#PIMM Android/Java Application Thrift API's


Requirement : JDK 1.8

This library is to be used for PIMM thrift API's only

##Thrift java library

If thrift is missing in the current project, you can download it here [Thrift Library](https://jar-download.com/artifacts/org.apache.thrift)

Make sure to import all the dependencies from the downloaded thrift archive

##Existing method calls

To call a specific method you must first authorize a user

```java
 import services.NamedServices;

 //e.g http://procuro.pimm.us
 String serverUrl;
 
 //e.g user credential base 64 format
 String userCredential;

 NamedServices namedServices = NamedServices.getInstance();
 namedServices.start(serverUrl, userCredential);
```

#### SiteSettingsProperties ....

```java
 String siteId
 ArrayList configSettings
 ArrayList users
```

SiteSettingsConfiguration

```java
 String name
 String value
```  

SiteSettingsUserRole

```java
 String userId
 String roleName
 String userName
 String email
 String phoneNumber
```

Client API Calls  

```java
 //e.g namedServices.siteSettingsClient.getSiteProperties(siteId);
 SiteSettingsProperties getSiteProperties(String siteId) throws TException;

 //e.g namedServices.siteSettingsClient.updateConfiguration(siteId, settings);
 boolean updateConfiguration(String siteId, ArrayList settings) throws TException;
```

#### SiteSettingsEquipment ....

SiteSettingsEquipment

```java
 String customerId
 String siteId
 String equipmentId
 String equipmentName
 String area
 boolean isDefault
 boolean inUse
```

Client API Calls  

```java
  
  //e.g namedServices.siteSettingsClient.getEquipment(customerId);
  ArrayList getEquipment(String customerId) throws TException;

  //e.g namedServices.siteSettingsClient.getSiteEquipment(siteId);
  ArrayList getSiteEquipment(String siteId) throws TException;

  //e.g namedServices.siteSettingsClient.updateSiteEquipment(editedEquipment);
  boolean updateSiteEquipment(ArrayList editedEquipment) throws TException;

```


Note : You can test the methods using the runnable class

### ...

### License 
 
Created by Ryan Madlangbayan on 03/04/2019.
Copyright © 2020