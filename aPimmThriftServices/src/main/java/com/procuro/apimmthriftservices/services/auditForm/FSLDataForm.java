package com.procuro.apimmthriftservices.services.auditForm;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.*;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class FSLDataForm {
    public String id, formId, userId, version, customerId, storeId;
    public long dateTimeStamp;
    public ArrayList items, notes, attachments;

    private boolean id_isset, formId_isset, userId_isset, version_isset,
                    dateTimeStamp_isset, customerId_isset, storeId_isset,
                    items_isset, notes_isset, attachments_isset;


    public FSLDataForm(TProtocol protocol) throws TException {
        read(protocol);
    }

    private void setItems(ArrayList items) {
        this.items = items;
        this.items_isset = true;
    }

    private void setNotes(ArrayList notes) {
        this.notes = notes;
        this.notes_isset = true;
    }

    private void setAttachments(ArrayList attachments) {
        this.attachments = attachments;
        this.attachments_isset = true;
    }

    private void setId(String id) {
        this.id = id;
        this.id_isset = true;
    }

    private void setFormId(String formId) {
        this.formId = formId;
        this.formId_isset = true;
    }

    private void setUserId(String userId) {
        this.userId = userId;
        this.userId_isset = true;
    }

    private void setVersion(String version) {
        this.version = version;
        this.version_isset = true;
    }

    private void setDateTimeStamp(long dateTimeStamp) {
        this.dateTimeStamp = dateTimeStamp;
        this.dateTimeStamp_isset = true;
    }

    private void setCustomerId(String customerId) {
        this.customerId = customerId;
        this.customerId_isset = true;
    }

    private void setStoreId(String storeId) {
        this.storeId = storeId;
        this.storeId_isset = true;
    }

    private void read(TProtocol protocol) throws TException {
        protocol.readStructBegin();

        while(true) {
            TField field = protocol.readFieldBegin();

            if(field.type == TType.STOP)
                break;

            switch (field.id) {
                case 1:
                    if(field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setId(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 2:
                    if(field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setFormId(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 3:
                    if(field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setUserId(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 4:
                    if(field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setVersion(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 5:
                    if(field.type == TType.I64) {
                         long fieldValue = protocol.readI64();
                         setDateTimeStamp(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 6:
                    if(field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setCustomerId(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 7:
                    if(field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setStoreId(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 8:
                    if(field.type == TType.LIST) {
                        TList list = protocol.readListBegin();
                        ArrayList fieldValue = new ArrayList();

                        for(int i=0; i<list.size; i++) {
                            FSLDataItem aci = new FSLDataItem(protocol);
                            fieldValue.add(aci);
                        }

                        protocol.readListEnd();
                        setItems(fieldValue);
                    }
                    else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 9:
                    if(field.type == TType.LIST) {
                        TList list = protocol.readListBegin();
                        ArrayList fieldValue = new ArrayList();

                        for(int i=0; i<list.size; i++) {
                            FSLDataNote note = new FSLDataNote(protocol);
                            fieldValue.add(note);
                        }

                        protocol.readListEnd();
                        setNotes(fieldValue);
                    }
                    else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 10:
                    if(field.type == TType.LIST) {
                        TList list = protocol.readListBegin();
                        ArrayList fieldValue = new ArrayList();

                        for(int i=0; i<list.size; i++) {
                            FSLDataAttachment aci = new FSLDataAttachment(protocol);
                            fieldValue.add(aci);
                        }

                        protocol.readListEnd();
                        setAttachments(fieldValue);
                    }
                    else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                default:
                    TProtocolUtil.skip(protocol, field.type);
                    break;

            }

            protocol.readFieldEnd();
        }

        protocol.readStructEnd();
    }

    public String description() {
        StringBuilder sb = new StringBuilder("FSLDataForm(");
        sb.append("id:");
        sb.append(String.format("\"%s\"", id));
        sb.append(",formId:");
        sb.append(String.format("\"%s\"", formId));
        sb.append(",userId:");
        sb.append(String.format("\"%s\"", userId));
        sb.append(",version:");
        sb.append(String.format("\"%s\"", version));
        sb.append(",dateTimeStamp:");
        sb.append(String.format("\"%s\"", dateTimeStamp));
        sb.append(",customerId:");
        sb.append(String.format("\"%s\"", customerId));
        sb.append(",storeId:");
        sb.append(String.format("\"%s\"", storeId));
        sb.append(",items:");
        sb.append(String.format("\"%s\"", items));
        sb.append(",notes:");
        sb.append(String.format("\"%s\"", notes));
        sb.append(",attachments:");
        sb.append(String.format("\"%s\"", attachments));
        sb.append(")");
        return sb.toString();
    }
}
