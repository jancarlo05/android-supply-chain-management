package com.procuro.apimmthriftservices.services.siteConfig;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TField;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolUtil;
import org.apache.thrift.protocol.TType;

public class SiteSettingsUserRole {

    public String userId, roleName, userName, email, phoneNumber;
    public boolean userId_isset, roleName_isset, userName_isset, email_isset, phoneNumber_isset;

    public SiteSettingsUserRole(TProtocol protocol) throws TException {
        read(protocol);
    }

    private void setUserId(String userId) {
        this.userId = userId;
        this.userId_isset = true;
    }

    private void setRoleName(String roleName) {
        this.roleName = roleName;
        this.roleName_isset = true;
    }

    private void setUserName(String userName) {
        this.userName = userName;
        this.userName_isset = true;
    }

    private void setEmail(String email) {
        this.email = email;
        this.email_isset = true;
    }

    private void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        this.phoneNumber_isset = true;
    }

    private void read(TProtocol protocol) throws TException {
        protocol.readStructBegin();

        while (true) {
            TField field = protocol.readFieldBegin();

            if(field.type == TType.STOP) {
                break;
            }

            switch (field.id) {
                case 1:
                    if (field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setUserId(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 2:
                    if (field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setRoleName(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 3:
                    if (field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setUserName(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 4:
                    if (field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setEmail(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 5:
                    if (field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setPhoneNumber(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                default:
                    TProtocolUtil.skip(protocol, field.type);
                    break;
            }

            protocol.readFieldEnd();
        }

        protocol.readStructEnd();
    }

    public String description() {
        StringBuilder sb = new StringBuilder("SiteSettingsUserRole(");
        sb.append("userId:");
        sb.append(String.format("\"%s\"", userId));
        sb.append(",roleName:");
        sb.append(String.format("\"%s\"", roleName));
        sb.append(",userName:");
        sb.append(String.format("\"%s\"", userName));
        sb.append(",email:");
        sb.append(String.format("\"%s\"", email));
        sb.append(",phoneNumber:");
        sb.append(String.format("\"%s\"", phoneNumber));
        sb.append(")");
        return sb.toString();
    }
}
