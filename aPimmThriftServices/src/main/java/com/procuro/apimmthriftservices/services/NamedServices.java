package com.procuro.apimmthriftservices.services;

import com.procuro.apimmthriftservices.services.auditForm.FSLDataFSLFormsClient;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsSiteConfigClient;
import com.procuro.apimmthriftservices.settings.Server;

import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TTransportException;

public class NamedServices {

    enum SType {
        SiteSettings,
        AuditForm,
        Calibration
    }

    private static NamedServices instance = null;
    private String credentials;
    public Server pimmServer;

    public SiteSettingsSiteConfigClient siteSettingsClient;
    public FSLDataFSLFormsClient fslFormsClient;

    public static NamedServices getInstance() throws TTransportException {

        if(instance == null)
        {
            synchronized (NamedServices.class) {
                instance = new NamedServices();
            }
        }

        return instance;
    }

    public void start(String webPimmUrl, String credentials) throws TTransportException {
        this.credentials = credentials;
        this.pimmServer = new Server(webPimmUrl, credentials);

        start();
    }

    private void start() throws TTransportException {
        siteSettingsClient = new SiteSettingsSiteConfigClient( protocol(SType.SiteSettings) );
        fslFormsClient = new FSLDataFSLFormsClient( protocol(SType.AuditForm) );
    }

    private TProtocol protocol(SType cserv) throws TTransportException {
        if(cserv == SType.AuditForm)
            return pimmServer.getProtocol("/WebPimm5/Services/PimmForm/Thrift/FSL/AuditForm");
        else if(cserv ==  SType.SiteSettings)
            return pimmServer.getProtocol("/WebPimm5/Services/SiteSettings");
        else //Calibration
            return pimmServer.getProtocol("/WebPimm5/Services/Calibration");
    }

}
