package com.procuro.apimmthriftservices.services.siteConfig;

import org.apache.thrift.TApplicationException;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.*;

import java.util.ArrayList;

public class SiteSettingsSiteConfigClient extends SiteSettingsSiteConfig {

    private TProtocol inProtocol, outProtocol;

    public SiteSettingsSiteConfigClient(TProtocol protocol) {
        this.inProtocol = protocol;
        this.outProtocol = protocol;
    }

    @Override
    public SiteSettingsProperties getSiteProperties(String siteId) throws TException {
        sendGetSiteProperties(siteId);
        return rcvGetSiteProperties();
    }

    private void sendGetSiteProperties(String siteId) throws TException {
        outProtocol.writeMessageBegin(new TMessage("GetSiteProperties", TMessageType.CALL, 0));
        outProtocol.writeStructBegin(new TStruct("GetSiteProperties_args"));

        if(siteId != null) {
            outProtocol.writeFieldBegin(new TField("siteId", TType.STRING, (short) 1));
            outProtocol.writeString(siteId);
            outProtocol.writeFieldEnd();
        }

        outProtocol.writeFieldStop();
        outProtocol.writeStructEnd();
        outProtocol.writeMessageEnd();
        outProtocol.getTransport().flush();

        System.out.println("______ EOF Send GetSiteProperties\n");
    }

    private SiteSettingsProperties rcvGetSiteProperties() throws TException {
        TMessage msg = inProtocol.readMessageBegin();

        if(msg.type == TMessageType.EXCEPTION) {
            System.out.println("Failed to receive site properties");
            TApplicationException x = TApplicationException.readFrom(inProtocol);
            inProtocol.readMessageEnd();
            throw x;
        }

        SiteSettingsGetSitePropertiesResult result = new SiteSettingsGetSitePropertiesResult(inProtocol);
        inProtocol.readMessageEnd();

        if(result.success_isset) {
            System.out.println("______ EOF Success Receive GetSiteProperties\n");
            return result.success;
        }

        throw new TApplicationException(TApplicationException.MISSING_RESULT,
                "GetSiteProperties failed: unknown result");
    }


    @Override
    public ArrayList getEquipment(String customerId) throws TException {
        sendGetEquipment(customerId);
        return rcvGetEquipment();
    }

    private void sendGetEquipment(String customerId) throws TException {
        outProtocol.writeMessageBegin(new TMessage("GetEquipmentForCustomer", TMessageType.CALL, 0));
        outProtocol.writeStructBegin(new TStruct("GetEquipmentForCustomer_args"));

        if(customerId != null) {
            outProtocol.writeFieldBegin(new TField("customerId", TType.STRING, (short) 1));
            outProtocol.writeString(customerId);
            outProtocol.writeFieldEnd();
        }

        outProtocol.writeFieldStop();
        outProtocol.writeStructEnd();
        outProtocol.writeMessageEnd();
        outProtocol.getTransport().flush();

        System.out.println("______ EOF Send GetEquipment\n");
    }

    private ArrayList rcvGetEquipment() throws TException {
        TMessage msg = inProtocol.readMessageBegin();

        if(msg.type == TMessageType.EXCEPTION) {
            System.out.println("Failed to receive equipment");
            TApplicationException x = TApplicationException.readFrom(inProtocol);
            inProtocol.readMessageEnd();
            throw x;
        }

        SiteSettingsGetEquipmentForCustomerResult result = new SiteSettingsGetEquipmentForCustomerResult(inProtocol);
        inProtocol.readMessageEnd();

        if(result.success_isset) {
            System.out.println("______ EOF Success Receive GetEquipment\n");
            return result.success;
        }

        throw new TApplicationException(TApplicationException.MISSING_RESULT,
                "GetEquipment failed: unknown result");
    }


    @Override
    public ArrayList getSiteEquipment(String siteId) throws TException {
        sendGetSiteEquipment(siteId);
        return rcvGetEquipment();
    }

    private void sendGetSiteEquipment(String siteId) throws TException {
        outProtocol.writeMessageBegin(new TMessage("GetSiteEquipment", TMessageType.CALL, 0));
        outProtocol.writeStructBegin(new TStruct("GetSiteEquipment_args"));

        if(siteId != null) {
            outProtocol.writeFieldBegin(new TField("siteId", TType.STRING, (short) 1));
            outProtocol.writeString(siteId);
            outProtocol.writeFieldEnd();
        }

        outProtocol.writeFieldStop();
        outProtocol.writeStructEnd();
        outProtocol.writeMessageEnd();
        outProtocol.getTransport().flush();

        System.out.println("______ EOF Send GetSiteEquipment\n");
    }

    private ArrayList rcvGetSiteEquipment() throws TException {
        TMessage msg = inProtocol.readMessageBegin();

        if(msg.type == TMessageType.EXCEPTION) {
            System.out.println("Failed to receive site equipments");
            TApplicationException x = TApplicationException.readFrom(inProtocol);
            inProtocol.readMessageEnd();
            throw x;
        }

        SiteSettingsGetSiteEquipmentResult result = new SiteSettingsGetSiteEquipmentResult(inProtocol);
        inProtocol.readMessageEnd();

        if(result.success_isset) {
            System.out.println("______ EOF Success Receive GetSiteEquipment\n");
            return result.success;
        }

        throw new TApplicationException(TApplicationException.MISSING_RESULT,
                "GetEquipment failed: unknown result");
    }

    @Override
    public boolean updateConfiguration(String siteId, ArrayList settings) throws TException {
        sendUpdateConfigurationForSite(siteId, settings);
        return recvUpdateConfigurationForSite();
    }

    private void sendUpdateConfigurationForSite(String siteId, ArrayList settings) throws TException {
        outProtocol.writeMessageBegin(new TMessage("UpdateConfigurationForSite", TMessageType.CALL, 0));
        outProtocol.writeStructBegin(new TStruct("UpdateConfigurationForSite_args"));

        if(siteId != null) {
            outProtocol.writeFieldBegin(new TField("siteId", TType.STRING, (short) 1));
            outProtocol.writeString(siteId);
            outProtocol.writeFieldEnd();
        }

        if(settings != null) {
            outProtocol.writeFieldBegin(new TField("settings", TType.LIST, (short) 2));
            {
                outProtocol.writeListBegin(new TList(TType.STRUCT, settings.size()));

                for (int i = 0; i < settings.size(); i++) {
                    SiteSettingsConfiguration conf = (SiteSettingsConfiguration)settings.get(i);
                    conf.write(outProtocol);
                }

                outProtocol.writeListEnd();
            }

            outProtocol.writeFieldEnd();
        }

        outProtocol.writeFieldStop();
        outProtocol.writeStructEnd();
        outProtocol.writeMessageEnd();
        outProtocol.getTransport().flush();

        System.out.println("______ EOF Send UpdateConfigurationForSite\n");
    }

    private boolean recvUpdateConfigurationForSite() throws TException {
        TMessage msg = inProtocol.readMessageBegin();

        if(msg.type == TMessageType.EXCEPTION) {
            System.out.println("Failed to update configuration");
            TApplicationException x = TApplicationException.readFrom(inProtocol);
            inProtocol.readMessageEnd();
            throw x;
        }

        SiteSettingsUpdateConfigurationForSiteResult result = new SiteSettingsUpdateConfigurationForSiteResult(inProtocol);
        inProtocol.readMessageEnd();

        if(result.success_isset) {
            System.out.println("______ EOF Success Receive UpdateConfigurationForSite\n");
            return result.success;
        }

        throw new TApplicationException(TApplicationException.MISSING_RESULT,
                "GetEquipment failed: unknown result");
    }

    @Override
    public boolean updateSiteEquipment(ArrayList editedEquipment) throws TException {
        sendUpdateSiteEquipment(editedEquipment);
        return rcvUpdateSiteEquipment();
    }

    private void sendUpdateSiteEquipment(ArrayList editedEquipment) throws TException {
        outProtocol.writeMessageBegin(new TMessage("UpdateSiteEquipment", TMessageType.CALL, 0));
        outProtocol.writeStructBegin(new TStruct("UpdateSiteEquipment_args"));

        if(editedEquipment != null) {
            outProtocol.writeFieldBegin(new TField("editedEquipment", TType.LIST, (short) 1));
            {
                outProtocol.writeListBegin(new TList(TType.STRUCT, editedEquipment.size()));

                for (int i = 0; i < editedEquipment.size(); i++) {
                    SiteSettingsEquipment eqp = (SiteSettingsEquipment)editedEquipment.get(i);
                    eqp.write(outProtocol);
                }

                outProtocol.writeListEnd();
            }

            outProtocol.writeFieldEnd();
        }

        outProtocol.writeFieldStop();
        outProtocol.writeStructEnd();
        outProtocol.writeMessageEnd();
        outProtocol.getTransport().flush();

        System.out.println("______ EOF Send UpdateSiteEquipment\n");
    }

    private boolean rcvUpdateSiteEquipment() throws TException {
        TMessage msg = inProtocol.readMessageBegin();

        if(msg.type == TMessageType.EXCEPTION) {
            System.out.println("Failed to update site equipment");
            TApplicationException x = TApplicationException.readFrom(inProtocol);
            inProtocol.readMessageEnd();
            throw x;
        }

        SiteSettingsUpdateSiteEquipmentResult result = new SiteSettingsUpdateSiteEquipmentResult(inProtocol);
        inProtocol.readMessageEnd();

        if(result.success_isset) {
            System.out.println("______ EOF Success Receive UpdateSiteEquipment\n");
            return result.success;
        }

        throw new TApplicationException(TApplicationException.MISSING_RESULT,
                "UpdateSiteEquipment failed: unknown result");
    }
}


class SiteSettingsUpdateSiteEquipmentResult {
    public boolean success;
    public boolean success_isset;

    public SiteSettingsUpdateSiteEquipmentResult(TProtocol protocol) throws TException {
        read(protocol);
    }

    private void setSuccess(boolean success) {
        this.success = success;
        this.success_isset = true;
    }

    public void read(TProtocol protocol) throws TException {
        protocol.readStructBegin();

        while (true) {
            TField field = protocol.readFieldBegin();

            if(field.type == TType.STOP) {
                break;
            }

            switch (field.id) {
                case 0:
                    if (field.type == TType.BOOL) {
                        boolean fieldValue = protocol.readBool();
                        setSuccess(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                default:
                    TProtocolUtil.skip(protocol, field.type);
                    break;
            }
            protocol.readFieldEnd();
        }

        protocol.readStructEnd();
    }
}

class SiteSettingsUpdateConfigurationForSiteResult {
    public boolean success;
    public boolean success_isset;

    public SiteSettingsUpdateConfigurationForSiteResult(TProtocol protocol) throws TException {
        read(protocol);
    }

    private void setSuccess(boolean success) {
        this.success = success;
        this.success_isset = true;
    }

    public void read(TProtocol protocol) throws TException {
        protocol.readStructBegin();

        while (true) {
            TField field = protocol.readFieldBegin();

            if(field.type == TType.STOP) {
                break;
            }

            switch (field.id) {
                case 0:
                    if (field.type == TType.BOOL) {
                        boolean fieldValue = protocol.readBool();
                        setSuccess(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                default:
                    TProtocolUtil.skip(protocol, field.type);
                    break;
            }
            protocol.readFieldEnd();
        }

        protocol.readStructEnd();
    }
}

class SiteSettingsGetEquipmentForCustomerResult {

    public ArrayList success;
    public boolean success_isset;

    public SiteSettingsGetEquipmentForCustomerResult(TProtocol protocol) throws TException {
        read(protocol);
    }

    private void setSuccess(ArrayList success) {
        this.success = success;
        this.success_isset = true;
    }

    public void read(TProtocol protocol) throws TException {
        protocol.readStructBegin();

        while(true) {
            TField field = protocol.readFieldBegin();

            if(field.type == TType.STOP)
                break;

            switch (field.id) {
                case 0:
                    if(field.type == TType.LIST) {
                        TList list = protocol.readListBegin();

                        ArrayList<SiteSettingsEquipment> fieldValue = new ArrayList<>();

                        for(int i=0; i<list.size; i++) {
                            SiteSettingsEquipment eqp = new SiteSettingsEquipment(protocol);
                            fieldValue.add(eqp);
                        }

                        protocol.readListEnd();
                        setSuccess(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                default:
                    TProtocolUtil.skip(protocol, field.type);
                    break;
            }

            protocol.readFieldEnd();
        }

        protocol.readStructEnd();
    }

}

class SiteSettingsGetSiteEquipmentResult {

    public ArrayList success;
    public boolean success_isset;


    public SiteSettingsGetSiteEquipmentResult(TProtocol protocol) throws TException {
        read(protocol);
    }

    private void setSuccess(ArrayList success) {
        this.success = success;
        this.success_isset = true;
    }

    public void read(TProtocol protocol) throws TException {
        protocol.readStructBegin();

        while(true) {
            TField field = protocol.readFieldBegin();

            if(field.type == TType.STOP)
                break;

            switch (field.id) {
                case 0:
                    if(field.type == TType.LIST) {
                        TList list = protocol.readListBegin();

                        ArrayList<SiteSettingsEquipment> fieldValue = new ArrayList<>();

                        for(int i=0; i<list.size; i++) {
                            SiteSettingsEquipment eqp = new SiteSettingsEquipment(protocol);
                            System.out.println(eqp.description());
                            fieldValue.add(eqp);
                        }

                        protocol.readListEnd();
                        setSuccess(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                default:
                    TProtocolUtil.skip(protocol, field.type);
                    break;
            }

            protocol.readFieldEnd();
        }

        protocol.readStructEnd();
    }

}

class SiteSettingsGetSitePropertiesResult {
    public SiteSettingsProperties success;
    public  boolean success_isset;


    private void setSuccess(SiteSettingsProperties success) {
        this.success = success;
        this.success_isset = true;
    }

    public SiteSettingsGetSitePropertiesResult(TProtocol protocol) throws TException {
        read(protocol);
    }

    public void read(TProtocol protocol) throws TException {
        while(true) {
            TField field = protocol.readFieldBegin();

            if (field.type == TType.STOP)
                break;

            switch (field.id) {

                case 0:
                    if (field.type == TType.STRUCT) {
                        SiteSettingsProperties fieldValue = new SiteSettingsProperties(protocol); //read
                        setSuccess(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                default:
                    TProtocolUtil.skip(protocol, field.type);
                    break;
            }

            protocol.readFieldEnd();
        }

        protocol.readStructEnd();
    }
}


