package com.procuro.apimmthriftservices.services.auditForm;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TField;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolUtil;
import org.apache.thrift.protocol.TType;

import java.nio.ByteBuffer;

public class FSLDataAttachment {

    public String attachmentName, userId;
    public long timestamp;
    public byte[] attachmentBinary;

    private boolean attachmentName_isset, userId_isset, timestamp_isset, attachmentBinary_isset;

    public FSLDataAttachment(TProtocol protocol) throws TException {
        read(protocol);
    }

    private void setAttachmentName(String attachmentName) {
        this.attachmentName = attachmentName;
        this.attachmentName_isset = true;
    }

    private void setUserId(String userId) {
        this.userId = userId;
        this.userId_isset = true;
    }

    private void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
        this.timestamp_isset = true;
    }

    private void setAttachmentBinary(byte[] attachmentBinary){
        this.attachmentBinary = attachmentBinary;
        this.attachmentName_isset = true;
    }

    private void read(TProtocol protocol) throws TException {
        protocol.readStructBegin();

        while(true) {
            TField field = protocol.readFieldBegin();

            if(field.type == TType.STOP)
                break;

            switch (field.id) {

                case 1:
                    if(field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setAttachmentName(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 2:
                    if(field.type == TType.I64) {
                        long fieldValue = protocol.readI64();
                        setTimestamp(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 3:
                    if(field.type == TType.STRING) {
                        ByteBuffer bf = protocol.readBinary();
                        byte[] bytes = new byte[bf.remaining()];
                        bf.get(bytes);

                        setAttachmentBinary(bytes);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 4:
                    if(field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setUserId(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                default:
                    TProtocolUtil.skip(protocol, field.type);
                    break;
            }

            protocol.readFieldEnd();
        }

        protocol.readStructEnd();
    }

    public String description() {
        StringBuilder sb = new StringBuilder("FSLDataAttachment(");
        sb.append("attachmentName:");
        sb.append(String.format("\"%s\"", attachmentName));
        sb.append(",timestamp:");
        sb.append(String.format("\"%s\"", timestamp));
        sb.append(",attachmentBinary:");
        sb.append(String.format("\"%s\"", attachmentBinary));
        sb.append(",userId:");
        sb.append(String.format("\"%s\"", userId));
        sb.append(",userId:");
        sb.append(String.format("\"%s\"", userId));
        sb.append(")");
        return sb.toString();
    }
}
