package com.procuro.apimmthriftservices.services.auditForm;

import org.apache.thrift.TException;

import java.util.ArrayList;

public abstract  class FSLDataFSLForms {
    public abstract FSLDataForm getForm(String customerId, String siteId, long unixFormDate) throws TException;
    public abstract ArrayList GetItemOverDateRange(String customerId, String siteId, long unixStartDate, long unixEndDate, String itemName) throws TException;
    public abstract ArrayList GetAttachmentsOverDateRange(String customerId, String siteId, long unixStartDate, long unixEndDate) throws TException;
    public abstract ArrayList GetNotesOverDateRange(String customerId, String siteId, long unixStartDate, long unixEndDate) throws TException;
}
