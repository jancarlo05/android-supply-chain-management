package com.procuro.apimmthriftservices.services.auditForm;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.*;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class FSLDataItem {
    public String itemName;
    public ArrayList readings;

    private boolean itemName_isset, readings_isset;

    public FSLDataItem(TProtocol protocol) throws TException {
        read(protocol);
    }

    private void setItemName(String itemName) {
        this.itemName = itemName;
        this.itemName_isset = true;
    }

    private void setReadings(ArrayList readings) {
        this.readings = readings;
        this.readings_isset = readings_isset;
    }

    private void read(TProtocol protocol) throws TException {
        protocol.readStructBegin();

        while(true) {
            TField field = protocol.readFieldBegin();

            if(field.type == TType.STOP)
                break;


            switch (field.id) {

                case 1:

                    if(field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setItemName(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 2:
                    if(field.type == TType.LIST) {
                        TList list = protocol.readListBegin();
                        ArrayList fieldValue = new ArrayList();

                        for(int i=0; i<list.size; i++) {
                            FSLDataReading aci = new FSLDataReading(protocol);
                            fieldValue.add(aci);
                        }

                        protocol.readListEnd();
                        setReadings(fieldValue);
                    }
                    break;

                default:
                    TProtocolUtil.skip(protocol, field.type);
                    break;

            }

            protocol.readFieldEnd();
        }


        protocol.readStructEnd();
    }

    public String description() {
        StringBuilder sb = new StringBuilder("FSLDataAttachment(");
        sb.append("itemName:");
        sb.append(String.format("\"%s\"", itemName));
        sb.append(",readings:");
        sb.append(String.format("\"%s\"", readings));
        sb.append(")");
        return sb.toString();
    }

}
