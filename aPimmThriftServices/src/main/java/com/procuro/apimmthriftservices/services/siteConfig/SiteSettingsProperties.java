package com.procuro.apimmthriftservices.services.siteConfig;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.*;

import java.util.ArrayList;

public class SiteSettingsProperties {

    public String siteId;
    public ArrayList configSettings, users;

    boolean siteId_isset, configSettings_isset, users_isset;

    public SiteSettingsProperties(TProtocol protocol) throws TException {
        read(protocol);
    }

    private void setSiteId(String siteId) {
        this.siteId = siteId;
        this.siteId_isset = true;
    }

    private void setConfigSettings(ArrayList<SiteSettingsConfiguration> configSettings) {
        this.configSettings = configSettings;
        this.configSettings_isset = true;
    }

    private void setUsers(ArrayList<SiteSettingsUserRole> users) {
        this.users = users;
        this.users_isset = true;
    }

    private void read(TProtocol protocol) throws TException {
        protocol.readStructBegin();

        while(true) {
            TField field = protocol.readFieldBegin();

            if(field.type == TType.STOP)
                break;

            switch (field.id) {
                case 1:
                    if(field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setSiteId(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 2:
                    if(field.type == TType.LIST) {
                        TList list = protocol.readListBegin();

                        ArrayList<SiteSettingsConfiguration> configs = new ArrayList<>();

                        for(int i=0; i<list.size; i++) {
                            SiteSettingsConfiguration siteConfig = new SiteSettingsConfiguration(protocol);
                            configs.add(siteConfig);
                        }

                        protocol.readListEnd();
                        setConfigSettings(configs);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 3:
                    if(field.type == TType.LIST) {
                        TList list = protocol.readListBegin();

                        ArrayList<SiteSettingsUserRole> userRoles = new ArrayList<>();

                        for(int i=0; i<list.size; i++) {
                            SiteSettingsUserRole userRole = new SiteSettingsUserRole(protocol);
                            userRoles.add(userRole);
                        }

                        protocol.readListEnd();
                        setUsers(userRoles);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                default:
                    TProtocolUtil.skip(protocol, field.type);
                    break;
            }

            protocol.readFieldEnd();
        }

        protocol.readStructEnd();
    }

    public String description() {
        StringBuilder sb = new StringBuilder("SiteSettingsProperties(");

        sb.append("siteId:");
        sb.append(String.format("\"%s\"", siteId));
        sb.append(",configSettings:");
        sb.append(String.format("\"%s\"", configSettings));
        sb.append(",users:");
        sb.append(String.format("\"%s\"", users));
        sb.append(")");
        return sb.toString();
    }
}
