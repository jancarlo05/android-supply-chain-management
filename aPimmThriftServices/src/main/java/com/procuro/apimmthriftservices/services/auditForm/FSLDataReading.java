package com.procuro.apimmthriftservices.services.auditForm;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.*;

import java.util.ArrayList;

public class FSLDataReading {

    public String itemName, textReading, userId, refId;
    public long dateTimeStamp;
    public double numericReading;
    public ArrayList actions;

    public boolean itemName_isset, textReading_isset, userId_isset, refId_isset,
                    dateTimeStamp_isset, numericReading_isset, actions_isset;

    public FSLDataReading(TProtocol protocol) throws TException {
        read(protocol);
    }

    private void setItemName(String itemName) {
        this.itemName = itemName;
        this.itemName_isset = true;
    }

    private void setTextReading(String textReading) {
        this.textReading = textReading;
        this.textReading_isset = true;
    }

    private void setUserId(String userId) {
        this.userId = userId;
        this.userId_isset = true;
    }

    private void setRefId(String refId) {
        this.refId = refId;
        this.refId_isset = true;
    }

    private void setDateTimeStamp(long dateTimeStamp) {
        this.dateTimeStamp = dateTimeStamp;
        this.dateTimeStamp_isset = true;
    }

    private void setNumericReading(double numericReading) {
        this.numericReading = numericReading;
        this.numericReading_isset = true;
    }

    private void setActions(ArrayList actions) {
        this.actions = actions;
        this.actions_isset = true;
    }

    private void read(TProtocol protocol) throws TException {
        protocol.readStructBegin();

        while(true) {
            TField field = protocol.readFieldBegin();

            if(field.type == TType.STOP)
                break;

            switch (field.id) {

                case 1:
                    if(field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setItemName(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 2:
                    if(field.type == TType.I64) {
                        long fieldValue = protocol.readI64();
                        setDateTimeStamp(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 3:
                    if(field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setTextReading(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 4:
                    if(field.type == TType.DOUBLE) {
                        double fieldValue = protocol.readDouble();
                        setNumericReading(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 5:
                    if(field.type == TType.LIST) {
                        TList list = protocol.readListBegin();
                        ArrayList fieldValue = new ArrayList();

                        for(int i=0; i<list.size; i++) {
                            FSLDataActionItem aci = new FSLDataActionItem(protocol);
                            fieldValue.add(aci);
                        }

                        protocol.readListEnd();
                        setActions(fieldValue);
                    }
                    else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 6:
                    if(field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setUserId(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 7:
                    if(field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setRefId(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                default:
                    TProtocolUtil.skip(protocol, field.type);
                    break;

            }


            protocol.readFieldEnd();
        }

        protocol.readStructEnd();
    }


    public String description() {
        StringBuilder sb = new StringBuilder("FSLDataReading(");
        sb.append("itemName:");
        sb.append(String.format("\"%s\"", itemName));
        sb.append(",dateTimeStamp:");
        sb.append(String.format("\"%s\"", dateTimeStamp));
        sb.append(",textReading:");
        sb.append(String.format("\"%s\"", textReading));
        sb.append(",numericReading:");
        sb.append(String.format("\"%s\"", numericReading));
        sb.append(",actions:");
        sb.append(String.format("\"%s\"", actions));
        sb.append(",userId:");
        sb.append(String.format("\"%s\"", userId));
        sb.append(",refId:");
        sb.append(String.format("\"%s\"", refId));
        sb.append(")");
        return sb.toString();
    }
}
