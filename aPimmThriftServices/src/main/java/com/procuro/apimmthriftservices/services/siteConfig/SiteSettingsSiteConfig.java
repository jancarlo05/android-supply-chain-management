package com.procuro.apimmthriftservices.services.siteConfig;

import org.apache.thrift.TException;

import java.util.ArrayList;

public abstract class SiteSettingsSiteConfig {

    public abstract SiteSettingsProperties getSiteProperties(String siteId) throws TException;
    public abstract ArrayList getEquipment(String customerId) throws TException;
    public abstract ArrayList getSiteEquipment(String siteId) throws TException;

    public abstract boolean updateConfiguration(String siteId, ArrayList settings) throws TException;
    public abstract boolean updateSiteEquipment(ArrayList editedEquipment) throws TException;


}
