package com.procuro.apimmthriftservices.services.siteConfig;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.*;

public class SiteSettingsConfiguration {

    public String name, value;
    private boolean name_isset, value_isset;

    private void setName(String name) {
        this.name = name;
        this.name_isset = true;
    }

    public void setValue(String value) {
        this.value = value;
        this.value_isset = true;
    }

    public SiteSettingsConfiguration(TProtocol protocol) throws TException {
        read(protocol);
    }

    public SiteSettingsConfiguration(String name, String value) {
        setName(name);
        setValue(value);
    }

    public void read(TProtocol protocol) throws TException {
        protocol.readStructBegin();

        while (true) {
            TField field = protocol.readFieldBegin();

            if(field.type == TType.STOP) {
                break;
            }

            switch (field.id) {
                case 1:
                    if (field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setName(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 2:
                    if (field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setValue(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                default:
                    TProtocolUtil.skip(protocol, field.type);
                    break;
            }
            protocol.readFieldEnd();
        }

        protocol.readStructEnd();
    }

    public void write(TProtocol outProtocol) throws TException {
        outProtocol.writeStructBegin(new TStruct("Configuration"));

        if(name_isset) {
            if(name != null) {
                outProtocol.writeFieldBegin(new TField("configName", TType.STRING, (short) 1));
                outProtocol.writeString(name);
                outProtocol.writeFieldEnd();
            }
        }

        if(value_isset) {
            if(value != null) {
                outProtocol.writeFieldBegin(new TField("configValue", TType.STRING, (short) 2));
                outProtocol.writeString(value);
                outProtocol.writeFieldEnd();
            }
        }

        outProtocol.writeFieldStop();
        outProtocol.writeStructEnd();
    }

    public String description() {
        StringBuilder sb = new StringBuilder("SiteSettingsConfiguration(");

        sb.append("name:");
        sb.append(String.format("\"%s\"", name));
        sb.append(",value:");
        sb.append(String.format("\"%s\"", value));
        sb.append(")");
        return sb.toString();
    }
}
