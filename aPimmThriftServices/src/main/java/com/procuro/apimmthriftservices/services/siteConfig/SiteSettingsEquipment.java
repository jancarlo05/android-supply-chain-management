package com.procuro.apimmthriftservices.services.siteConfig;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.*;

public class SiteSettingsEquipment {

    public String customerId, siteId, equipmentId, equipmentName, area;
    public boolean isDefault, inUse;

    public boolean customerId_isset, siteId_isset, equipmentId_isset, equipmentName_isset, area_isset, isDefault_isset, inUse_isset;

    public SiteSettingsEquipment(TProtocol protocol) throws TException {
        read(protocol);
    }

    private void setCustomerId(String customerId) {
        this.customerId = customerId;
        this.customerId_isset = true;
    }

    private void setSiteId(String siteId) {
        this.siteId = siteId;
        this.siteId_isset = true;
    }

    private void setEquipmentId(String equipmentId) {
        this.equipmentId = equipmentId;
        this.equipmentId_isset = true;
    }

    private void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName;
        this.equipmentName_isset = true;
    }

    private void setArea(String area) {
        this.area = area;
        this.area_isset = true;
    }

    private void setDefault(boolean isDefault) {
        this.isDefault = isDefault;
        this.isDefault_isset = true;
    }

    private void setInUse(boolean inUse) {
        this.inUse = inUse;
        this.inUse_isset = true;
    }

    public void setInUse(String siteId, boolean inUse) {
        this.inUse = inUse;
        this.inUse_isset = true;

        this.siteId = siteId;
        this.siteId_isset = true;
    }

    private void read(TProtocol protocol) throws TException {
        protocol.readStructBegin();

        while (true) {
            TField field = protocol.readFieldBegin();

            if(field.type == TType.STOP) {
                break;
            }

            switch (field.id) {
                case 1:
                    if (field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setCustomerId(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 2:
                    if (field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setSiteId(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 3:
                    if (field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setEquipmentId(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 4:
                    if (field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setEquipmentName(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 5:
                    if (field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setArea(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 6:
                    if (field.type == TType.BOOL) {
                        boolean fieldValue = protocol.readBool();
                        setDefault(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 7:
                    if (field.type == TType.BOOL) {
                        boolean fieldValue = protocol.readBool();
                        setInUse(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                default:
                    TProtocolUtil.skip(protocol, field.type);
                    break;
            }

            protocol.readFieldEnd();
        }

        protocol.readStructEnd();
    }

    public void write(TProtocol outProtocol) throws TException {
        outProtocol.writeStructBegin(new TStruct("Equipment"));

        if(customerId_isset) {
            if(customerId != null) {
                outProtocol.writeFieldBegin(new TField("customerId", TType.STRING, (short) 1));
                outProtocol.writeString(customerId);
                outProtocol.writeFieldEnd();
            }
        }

        if(siteId_isset) {
            if(siteId != null) {
                outProtocol.writeFieldBegin(new TField("siteId", TType.STRING, (short) 2));
                outProtocol.writeString(siteId);
                outProtocol.writeFieldEnd();
            }
        }


        if(equipmentId_isset) {
            if(equipmentId != null) {
                outProtocol.writeFieldBegin(new TField("equipmentId", TType.STRING, (short) 3));
                outProtocol.writeString(equipmentId);
                outProtocol.writeFieldEnd();
            }
        }

        if(equipmentName_isset) {
            if(equipmentName != null) {
                outProtocol.writeFieldBegin(new TField("equipmentName", TType.STRING, (short) 4));
                outProtocol.writeString(equipmentName);
                outProtocol.writeFieldEnd();
            }
        }

        if(area_isset) {
            if(area != null) {
                outProtocol.writeFieldBegin(new TField("area", TType.STRING, (short) 5));
                outProtocol.writeString(area);
                outProtocol.writeFieldEnd();
            }
        }

        if(isDefault_isset) {
            outProtocol.writeFieldBegin(new TField("isDefault", TType.BOOL, (short) 6));
            outProtocol.writeBool(isDefault);
            outProtocol.writeFieldEnd();
        }

        if(inUse_isset) {
            outProtocol.writeFieldBegin(new TField("inUse", TType.BOOL, (short) 7));
            outProtocol.writeBool(inUse);
            outProtocol.writeFieldEnd();
        }

        System.out.println("Writing eqp : " + description());

        outProtocol.writeFieldStop();
        outProtocol.writeStructEnd();
    }

    public String description() {

        StringBuilder sb = new StringBuilder("SiteSettingsEquipment(");
        sb.append("customerId:");
        sb.append(String.format("\"%s\"", customerId));
        sb.append(",siteId:");
        sb.append(String.format("\"%s\"", siteId));
        sb.append(",equipmentId:");
        sb.append(String.format("\"%s\"", equipmentId));
        sb.append(",equipmentName:");
        sb.append(String.format("\"%s\"", equipmentName));
        sb.append(",area:");
        sb.append(String.format("\"%s\"", area));
        sb.append(",isDefault:");
        sb.append(String.format("\"%s\"", isDefault ? "true" : "false"));
        sb.append(",inUse:");
        sb.append(String.format("\"%s\"", inUse ? "true" : "false"));
        sb.append(")");
        return sb.toString();
    }
}
