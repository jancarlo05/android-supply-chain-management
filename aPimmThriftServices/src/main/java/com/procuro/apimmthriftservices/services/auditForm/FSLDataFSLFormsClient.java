package com.procuro.apimmthriftservices.services.auditForm;

import org.apache.thrift.TApplicationException;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.*;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class FSLDataFSLFormsClient extends FSLDataFSLForms {

    private TProtocol inProtocol, outProtocol;

    public FSLDataFSLFormsClient(TProtocol protocol) {
        this.inProtocol = protocol;
        this.outProtocol = protocol;
    }

    @Override
    public FSLDataForm getForm(String customerId, String siteId, long unixFormDate) throws TException {
        sendGetForm(customerId, siteId, unixFormDate);
        return rcvGetForm();
    }

    private void sendGetForm(String customerId, String siteId, long unixFormDate) throws TException {
        outProtocol.writeMessageBegin(new TMessage("GetForm", TMessageType.CALL, 0));
        outProtocol.writeStructBegin(new TStruct("GetForm_args"));

        if (customerId != null) {
            outProtocol.writeFieldBegin(new TField("customerId", TType.STRING, (short) 1));
            outProtocol.writeString(customerId);
            outProtocol.writeFieldEnd();
        }

        if (siteId != null) {
            outProtocol.writeFieldBegin(new TField("storeId", TType.STRING, (short) 2));
            outProtocol.writeString(siteId);
            outProtocol.writeFieldEnd();
        }

        outProtocol.writeFieldBegin(new TField("reportDate", TType.I64, (short) 3));
        outProtocol.writeI64(unixFormDate);
        outProtocol.writeFieldEnd();

        outProtocol.writeFieldStop();
        outProtocol.writeStructEnd();
        outProtocol.writeMessageEnd();
        outProtocol.getTransport().flush();

        System.out.println("______ EOF Send GetForm\n");
    }

    private FSLDataForm rcvGetForm() throws TException {
        TMessage msg = inProtocol.readMessageBegin();

        if (msg.type == TMessageType.EXCEPTION) {
            System.out.println("Failed to receive get form");
            TApplicationException x = TApplicationException.readFrom(inProtocol);
            inProtocol.readMessageEnd();
            throw x;
        }

        FSLDataGetFormResult result = new FSLDataGetFormResult(inProtocol);
        inProtocol.readMessageEnd();

        if (result.success_isset) {
            System.out.println("______ EOF Success Receive GetForm\n");
            return result.success;
        }

        throw new TApplicationException(TApplicationException.MISSING_RESULT,
                "GetForm failed: unknown result");
    }

    @Override
    public ArrayList GetItemOverDateRange(String customerId, String siteId, long unixStartDate, long unixEndDate, String itemName) throws TException {
        sendGetItemOverDateRange(customerId, siteId, unixStartDate, unixEndDate, itemName);
        return rcvGetItemOverDateRange();
    }

    private void sendGetItemOverDateRange(String customerId, String siteId, long unixStartDate, long unixEndDate, String itemName) throws TException {
        outProtocol.writeMessageBegin(new TMessage("GetItemOverDateRange", TMessageType.CALL, 0));
        outProtocol.writeStructBegin(new TStruct("GetItemOverDateRange_args"));

        if (customerId != null) {
            outProtocol.writeFieldBegin(new TField("customerId", TType.STRING, (short) 1));
            outProtocol.writeString(customerId);
            outProtocol.writeFieldEnd();
        }

        if (siteId != null) {
            outProtocol.writeFieldBegin(new TField("storeId", TType.STRING, (short) 2));
            outProtocol.writeString(siteId);
            outProtocol.writeFieldEnd();
        }

        outProtocol.writeFieldBegin(new TField("startDate", TType.I64, (short) 3));
        outProtocol.writeI64(unixStartDate);
        outProtocol.writeFieldEnd();

        outProtocol.writeFieldBegin(new TField("endDate", TType.I64, (short) 4));
        outProtocol.writeI64(unixEndDate);
        outProtocol.writeFieldEnd();

        if (itemName != null) {
            outProtocol.writeFieldBegin(new TField("itemName", TType.STRING, (short) 5));
            outProtocol.writeString(itemName);
            outProtocol.writeFieldEnd();
        }

        outProtocol.writeFieldStop();
        outProtocol.writeStructEnd();
        outProtocol.writeMessageEnd();
        outProtocol.getTransport().flush();

        System.out.println("______ EOF Send GetItemOverDateRange\n");
    }

    private ArrayList rcvGetItemOverDateRange() throws TException {
        TMessage msg = inProtocol.readMessageBegin();

        if (msg.type == TMessageType.EXCEPTION) {
            System.out.println("Failed to receive get item over date range");
            TApplicationException x = TApplicationException.readFrom(inProtocol);
            inProtocol.readMessageEnd();
            throw x;
        }

        FSLDataGetItemOverDateRangeResult result = new FSLDataGetItemOverDateRangeResult(inProtocol);
        inProtocol.readMessageEnd();

        if (result.success_isset) {
            System.out.println("______ EOF Success Receive GetItemOverDateRange\n");
            return result.success;
        }

        throw new TApplicationException(TApplicationException.MISSING_RESULT,
                "GetItemOverDateRange failed: unknown result");
    }


    @Override
    public ArrayList GetAttachmentsOverDateRange(String customerId, String siteId, long unixStartDate, long unixEndDate) throws TException {
        sendGetAttachmentsOverDateRange(customerId, siteId, unixStartDate, unixEndDate);
        return rcvGetAttachmentsOverDateRange();
    }

    private void sendGetAttachmentsOverDateRange(String customerId, String siteId, long unixStartDate, long unixEndDate) throws TException {
        outProtocol.writeMessageBegin(new TMessage("GetAttachmentsOverDateRange", TMessageType.CALL, 0));
        outProtocol.writeStructBegin(new TStruct("GetAttachmentsOverDateRange_args"));

        if (customerId != null) {
            outProtocol.writeFieldBegin(new TField("customerId", TType.STRING, (short) 1));
            outProtocol.writeString(customerId);
            outProtocol.writeFieldEnd();
        }

        if (siteId != null) {
            outProtocol.writeFieldBegin(new TField("storeId", TType.STRING, (short) 2));
            outProtocol.writeString(siteId);
            outProtocol.writeFieldEnd();
        }

        outProtocol.writeFieldBegin(new TField("startDate", TType.I64, (short) 3));
        outProtocol.writeI64(unixStartDate);
        outProtocol.writeFieldEnd();

        outProtocol.writeFieldBegin(new TField("endDate", TType.I64, (short) 4));
        outProtocol.writeI64(unixEndDate);
        outProtocol.writeFieldEnd();

        outProtocol.writeFieldStop();
        outProtocol.writeStructEnd();
        outProtocol.writeMessageEnd();
        outProtocol.getTransport().flush();

        System.out.println("______ EOF Send GetAttachmentsOverDateRange\n");
    }

    private ArrayList rcvGetAttachmentsOverDateRange() throws TException {
        TMessage msg = inProtocol.readMessageBegin();

        if (msg.type == TMessageType.EXCEPTION) {
            System.out.println("Failed to receive get attachments over date range");
            TApplicationException x = TApplicationException.readFrom(inProtocol);
            inProtocol.readMessageEnd();
            throw x;
        }

        FSLDataGetAttachmentsOverDateRangeResult result = new FSLDataGetAttachmentsOverDateRangeResult(inProtocol);
        inProtocol.readMessageEnd();

        if (result.success_isset) {
            System.out.println("______ EOF Success Receive GetAttachmentsOverDateRange\n");
            return result.success;
        }

        throw new TApplicationException(TApplicationException.MISSING_RESULT,
                "GetAttachmentsOverDateRange failed: unknown result");
    }

    @Override
    public ArrayList GetNotesOverDateRange(String customerId, String siteId, long unixStartDate, long unixEndDate) throws TException {
        sendGetNotesOverDateRange(customerId, siteId, unixStartDate, unixEndDate);
        return rcvGetNotesOverDateRange();
    }

    private void sendGetNotesOverDateRange(String customerId, String siteId, long unixStartDate, long unixEndDate) throws TException {
        outProtocol.writeMessageBegin(new TMessage("GetNotesOverDateRange", TMessageType.CALL, 0));
        outProtocol.writeStructBegin(new TStruct("GetNotesOverDateRange_args"));

        if (customerId != null) {
            outProtocol.writeFieldBegin(new TField("customerId", TType.STRING, (short) 1));
            outProtocol.writeString(customerId);
            outProtocol.writeFieldEnd();
        }

        if (siteId != null) {
            outProtocol.writeFieldBegin(new TField("storeId", TType.STRING, (short) 2));
            outProtocol.writeString(siteId);
            outProtocol.writeFieldEnd();
        }

        outProtocol.writeFieldBegin(new TField("startDate", TType.I64, (short) 3));
        outProtocol.writeI64(unixStartDate);
        outProtocol.writeFieldEnd();

        outProtocol.writeFieldBegin(new TField("endDate", TType.I64, (short) 4));
        outProtocol.writeI64(unixEndDate);
        outProtocol.writeFieldEnd();

        outProtocol.writeFieldStop();
        outProtocol.writeStructEnd();
        outProtocol.writeMessageEnd();
        outProtocol.getTransport().flush();

        System.out.println("______ EOF Send GetNotesOverDateRange\n");
    }

    private ArrayList rcvGetNotesOverDateRange() throws TException {
        TMessage msg = inProtocol.readMessageBegin();

        if (msg.type == TMessageType.EXCEPTION) {
            System.out.println("Failed to receive get notes over date range");
            TApplicationException x = TApplicationException.readFrom(inProtocol);
            inProtocol.readMessageEnd();
            throw x;
        }

        FSLDataGetNotesOverDateRangeResult result = new FSLDataGetNotesOverDateRangeResult(inProtocol);
        inProtocol.readMessageEnd();

        if (result.success_isset) {
            System.out.println("______ EOF Success Receive GetNotesOverDateRange\n");
            return result.success;
        }

        throw new TApplicationException(TApplicationException.MISSING_RESULT,
                "GetNotesOverDateRange failed: unknown result");
    }
}

class FSLDataGetNotesOverDateRangeResult {
    public ArrayList success;
    public boolean success_isset;

    private void setSuccess(ArrayList success) {
        this.success = success;
        this.success_isset = true;
    }

    public FSLDataGetNotesOverDateRangeResult(TProtocol protocol) throws TException {
        read(protocol);
    }

    private void read(TProtocol protocol) throws TException {
        protocol.readStructBegin();

        while (true) {
            TField field = protocol.readFieldBegin();

            if (field.type == TType.STOP)
                break;

            switch (field.id) {
                case 0:
                    if (field.type == TType.LIST) {
                        TList list = protocol.readListBegin();

                        ArrayList fieldValue = new ArrayList<>();

                        for (int i = 0; i < list.size; i++) {
                            FSLDataNote note = new FSLDataNote(protocol);
                            fieldValue.add(note);
                        }

                        protocol.readListEnd();
                        setSuccess(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                default:
                    TProtocolUtil.skip(protocol, field.type);
                    break;
            }

            protocol.readFieldEnd();
        }

        protocol.readStructEnd();
    }
}

class FSLDataGetAttachmentsOverDateRangeResult {
    public ArrayList success;
    public boolean success_isset;

    private void setSuccess(ArrayList success) {
        this.success = success;
        this.success_isset = true;
    }

    public FSLDataGetAttachmentsOverDateRangeResult(TProtocol protocol) throws TException {
        read(protocol);
    }

    private void read(TProtocol protocol) throws TException {
        protocol.readStructBegin();

        while (true) {
            TField field = protocol.readFieldBegin();

            if (field.type == TType.STOP)
                break;

            switch (field.id) {
                case 0:
                    if (field.type == TType.LIST) {
                        TList list = protocol.readListBegin();

                        ArrayList fieldValue = new ArrayList<>();

                        for (int i = 0; i < list.size; i++) {
                            FSLDataAttachment attachment = new FSLDataAttachment(protocol);
                            fieldValue.add(attachment);
                        }

                        protocol.readListEnd();
                        setSuccess(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                default:
                    TProtocolUtil.skip(protocol, field.type);
                    break;
            }

            protocol.readFieldEnd();
        }

        protocol.readStructEnd();
    }
}

class FSLDataGetItemOverDateRangeResult {
    public ArrayList success;
    public boolean success_isset;


    private void setSuccess(ArrayList success) {
        this.success = success;
        this.success_isset = true;
    }

    public FSLDataGetItemOverDateRangeResult(TProtocol protocol) throws TException {
        read(protocol);
    }

    private void read(TProtocol protocol) throws TException {
        protocol.readStructBegin();

        while(true) {
            TField field = protocol.readFieldBegin();

            if (field.type == TType.STOP)
                break;



            switch (field.id) {
                case 0:
                    if(field.type == TType.LIST) {
                        TList list = protocol.readListBegin();
                        ArrayList fieldValue = new ArrayList<>();

                        for(int i=0; i<list.size; i++) {
                            FSLDataItem itm = new FSLDataItem(protocol);
                            fieldValue.add(itm);
                        }

                        protocol.readListEnd();
                        setSuccess(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                default:
                    TProtocolUtil.skip(protocol, field.type);
                    break;
            }

            protocol.readFieldEnd();
        }

        protocol.readStructEnd();
    }
}

class FSLDataGetFormResult {
    public FSLDataForm success;
    public boolean success_isset;

    private void setSuccess(FSLDataForm success) {
        this.success = success;
        this.success_isset = true;
    }

    public FSLDataGetFormResult(TProtocol protocol) throws TException {
        read(protocol);
    }

    private void read(TProtocol protocol) throws TException {
        protocol.readStructBegin();

        while(true) {
            TField field = protocol.readFieldBegin();

            if (field.type == TType.STOP)
                break;

            switch (field.id) {
                case 0:
                    if(field.type == TType.STRUCT) {
                        FSLDataForm fieldValue = new FSLDataForm(protocol);
                        setSuccess(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                default:
                    TProtocolUtil.skip(protocol, field.type);
                    break;
            }

            protocol.readFieldEnd();
        }

        protocol.readStructEnd();
    }
}
