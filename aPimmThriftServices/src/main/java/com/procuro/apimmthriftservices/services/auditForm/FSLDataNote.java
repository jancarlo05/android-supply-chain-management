package com.procuro.apimmthriftservices.services.auditForm;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.*;

import java.util.ArrayList;

public class FSLDataNote {

    public String formName, noteText, authorId;
    public long timestamp;

    private boolean formName_isset, noteText_isset, authorId_isset, timestamp_isset;

    public FSLDataNote(TProtocol protocol) throws TException {
        read(protocol);
    }

    private void setFormName(String formName) {
        this.formName = formName;
        this.formName_isset = true;
    }

    private void setNoteText(String noteText) {
        this.noteText = noteText;
        this.noteText_isset = true;
    }

    private void setAuthorId(String authorId) {
        this.authorId = authorId;
        this.authorId_isset = true;
    }

    private void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
        this.timestamp_isset = true;
    }

    private void read(TProtocol protocol) throws TException {
        protocol.readStructBegin();

        while(true) {
            TField field = protocol.readFieldBegin();

            if(field.type == TType.STOP)
                break;

            switch (field.id) {

                case 1:
                    if(field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setFormName(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 2:
                    if(field.type == TType.I64) {
                        long fieldValue = protocol.readI64();
                        setTimestamp(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 3:
                    if(field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setNoteText(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 4:
                    if(field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setAuthorId(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                default:
                    TProtocolUtil.skip(protocol, field.type);
                    break;
            }

            protocol.readFieldEnd();
        }

        protocol.readStructEnd();
    }

    public String description() {
        StringBuilder sb = new StringBuilder("FSLDataNote(");
        sb.append("formName:");
        sb.append(String.format("\"%s\"", formName));
        sb.append(",timestamp:");
        sb.append(String.format("\"%s\"", timestamp));
        sb.append(",noteText:");
        sb.append(String.format("\"%s\"", noteText));
        sb.append(",authorId:");
        sb.append(String.format("\"%s\"", authorId));
        sb.append(")");
        return sb.toString();
    }

}
