package com.procuro.apimmthriftservices.services.auditForm;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TField;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolUtil;
import org.apache.thrift.protocol.TType;

public class FSLDataActionItem {
    public String actionTaken, actionText;
    private boolean actionTaken_isset, actionText_isset;

    private void setActionTaken(String actionTaken) {
        this.actionTaken = actionTaken;
        this.actionTaken_isset = true;
    }

    private void setActionText(String actionText) {
        this.actionText = actionText;
        this.actionText_isset = true;
    }

    public FSLDataActionItem(TProtocol protocol) throws TException {
        read(protocol);
    }

    private void read(TProtocol protocol) throws TException {
        protocol.readStructBegin();

        while(true) {
            TField field = protocol.readFieldBegin();

            if(field.type == TType.STOP)
                break;

            switch (field.id) {
                case 1:
                    if(field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setActionTaken(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                case 2:
                    if(field.type == TType.STRING) {
                        String fieldValue = protocol.readString();
                        setActionText(fieldValue);
                    } else
                        TProtocolUtil.skip(protocol, field.type);
                    break;

                default:
                    TProtocolUtil.skip(protocol, field.type);
                    break;

            }

            protocol.readFieldEnd();
        }

        protocol.readStructEnd();
    }

    public String description() {
        StringBuilder sb = new StringBuilder("FSLDataReading(");
        sb.append("actionTaken:");
        sb.append(String.format("\"%s\"", actionTaken));
        sb.append(",actionText:");
        sb.append(String.format("\"%s\"", actionText));
        sb.append(")");
        return sb.toString();
    }
}
