package com.procuro.apimmthriftservices.settings;

public class Constants {
    public static final int serverTimeout = 0;
    public static final String protocolType = "binary";
    public static final String agent = "iPimm/Thrift";
}
