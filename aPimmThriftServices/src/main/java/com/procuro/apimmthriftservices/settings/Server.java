package com.procuro.apimmthriftservices.settings;

import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.THttpClient;
import org.apache.thrift.transport.TTransportException;

public class Server {
    public String serverUrl;
    public TProtocol serverProtocol;
    public String credentials;

    public Server(String serverUrl, String credentials) {
        this.serverUrl = serverUrl;
        this.credentials = credentials;
    }

    public TProtocol getProtocol(String url) throws TTransportException {
        String fullUrl = serverUrl+url;
        THttpClient tc = httpClient(fullUrl);
        TProtocol protocol = new TBinaryProtocol(tc);

        tc.close();
        return protocol;
    }

    private THttpClient httpClient(String url) throws TTransportException {
        THttpClient tc = new THttpClient(url);

        tc.setCustomHeader("Authorization", String.format("Basic %s", credentials));
        tc.setCustomHeader("X-Thrift-Protocol", Constants.protocolType);
        tc.setCustomHeader("User-Agent", Constants.agent);
        tc.open();

        return tc;
    }


}
