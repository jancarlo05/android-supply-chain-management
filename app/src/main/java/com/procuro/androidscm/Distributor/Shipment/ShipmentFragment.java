package com.procuro.androidscm.Distributor.Shipment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.WeeklySchedule.Dashboard_Weekdays_RecyclerViewAdapter;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.WeeklySchedule.Dashboard_Weekly_Sched_RecyclerViewAdapter;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.SMSPositioning;
import com.procuro.apimmdatamanagerlib.Site;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;


public class ShipmentFragment extends Fragment {

    TextView date;


    public ShipmentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.distributor_shipment_fragment, container, false);
        date = view.findViewById(R.id.date);


        DisplayData();

        return view;
    }

    private void DisplayData(){
       GenerateWeekDays();
    }

    private void GenerateWeekDays(){
        try {
            Calendar now = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            now.setTimeZone(TimeZone.getTimeZone("UTC"));

            ArrayList<Date> Weekdays = new ArrayList<>();
            int delta = -now.get(Calendar.DAY_OF_WEEK) + 2; //add 2 if your week start on monday
            now.add(Calendar.DAY_OF_MONTH, delta );
            for (int i = 0; i < 7; i++) {
                Weekdays.add(now.getTime());
                now.add(Calendar.DAY_OF_MONTH, 1);
            }

            SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            date.setText(format.format(Weekdays.get(0)));
            date.append(" to ");
            date.append(format.format(Weekdays.get(Weekdays.size()-1)));


        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
