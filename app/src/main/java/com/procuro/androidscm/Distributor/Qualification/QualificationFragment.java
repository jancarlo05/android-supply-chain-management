package com.procuro.androidscm.Distributor.Qualification;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.procuro.androidscm.HomePage;
import com.procuro.androidscm.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


public class QualificationFragment extends Fragment {

    TextView date;


    public QualificationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.distributor_qualification_fragment, container, false);
        date = view.findViewById(R.id.date);


        DisplayData();

        return view;
    }

    private void DisplayData(){
        SimpleDateFormat format = new SimpleDateFormat("EEEE, MMM d, yyyy");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        date.setText(format.format(new Date()));
    }



}
