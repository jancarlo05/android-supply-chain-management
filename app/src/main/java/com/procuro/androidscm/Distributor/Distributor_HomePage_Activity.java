package com.procuro.androidscm.Distributor;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Keep;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;
import com.procuro.androidscm.Distributor.Alerts.AlertsFragment;
import com.procuro.androidscm.Distributor.Header.HeaderFragment;
import com.procuro.androidscm.Distributor.More.MoreFragment;
import com.procuro.androidscm.Distributor.Qualification.QualificationFragment;
import com.procuro.androidscm.Distributor.Shipment.ShipmentFragment;
import com.procuro.androidscm.Distributor.Status.StatusFragment;
import com.procuro.androidscm.HomePage;
import com.procuro.androidscm.Logout_Confirmation_DialogFragment;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.AlertFragment.AlertFragment;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.Qualification.QualificationActivityFragment;
import com.procuro.androidscm.Restaurant.ReportsFragment.Reports_fragment;
import com.procuro.androidscm.Restaurant.Schema.Days;
import com.procuro.androidscm.Restaurant.Schema.Forms;
import com.procuro.androidscm.Restaurant.Schema.Schema;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.androidscm.UnderMaintenance_DialogFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

@Keep
public class Distributor_HomePage_Activity extends AppCompatActivity {
    private TextView mTextMessage;
    public static BottomNavigationView navView;
    public static  int NAV_QUALIFICATIONS = R.id.nav_qualifications;
    public static boolean custom;
    int PreviousItem = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.distributor_home_page);

        navView = findViewById(R.id.bottom_nav);
        navView.getMenu().clear();
        navView.getMenu().add(Menu.NONE, R.id.nav_qualifications, Menu.NONE, "Qualifications").setIcon(R.drawable.qualification_tab);
        navView.getMenu().add(Menu.NONE, R.id.nav_status, Menu.NONE, "Status").setIcon(R.drawable.status_tab_icon);
        navView.getMenu().add(Menu.NONE, R.id.nav_shipment, Menu.NONE, "Shipment").setIcon(R.drawable.shipment_tab);
        navView.getMenu().add(Menu.NONE, R.id.nav_alert, Menu.NONE, "Alerts").setIcon(R.drawable.alerts);
        navView.getMenu().add(Menu.NONE, R.id.nav_more, Menu.NONE, "More").setIcon(R.drawable.more);

        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navView.setSelectedItemId(R.id.nav_qualifications);

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(MenuItem item) {
            if (item.getItemId()!=PreviousItem){
                PreviousItem = item.getItemId();
                switch (item.getItemId()) {
                    case R.id.nav_qualifications:
                        DisplayQualificationFragment();
                        return true;
                    case R.id.nav_status:
                        StatusFragment();
                        return true;
                    case R.id.nav_alert:
                        AlertsFragment();
                        return true;
                    case R.id.nav_shipment:
                        ShipmentFragment();
                        return true;
                    case R.id.nav_more:
                        MoreFragment();
                        return true;
                }
            }

            return false;
        }
    };


    private void DisplayQualificationFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.distributor_fragment_container,
                new QualificationFragment()).commit();

        DisplayHeader();
    }

    private void StatusFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.distributor_fragment_container,
                new StatusFragment()).commit();

        DisplayHeader();
    }

    private void  ShipmentFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.distributor_fragment_container,
                new ShipmentFragment()).commit();

        DisplayHeader();
    }

    private void  AlertsFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.distributor_fragment_container,
                new AlertsFragment()).commit();

        DisplayHeader();
    }

    private void  MoreFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.distributor_fragment_container,
                new MoreFragment()).commit();

        DisplayHeader();
    }

    private void DisplayHeader() {
        getSupportFragmentManager().beginTransaction().replace(R.id.distributor_header_fragment_container,
                new HeaderFragment()).commit();
    }


    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(Distributor_HomePage_Activity.this)
                .setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(Distributor_HomePage_Activity.this, HomePage.class);
                        startActivity(intent);
                        finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    private void DisplayLogoutConfirmation() {
        DialogFragment newFragment = Logout_Confirmation_DialogFragment.newInstance();
        assert getFragmentManager() != null;
        newFragment.show(getSupportFragmentManager(), "dialog");
    }

    private void DisplayUnderMaintenanceMessage() {
        DialogFragment newFragment = UnderMaintenance_DialogFragment.newInstance();
        assert getFragmentManager() != null;
        newFragment.show(getSupportFragmentManager(), "dialog");
    }

}
