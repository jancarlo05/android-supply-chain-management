package com.procuro.androidscm.Supplier.Header;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.procuro.androidscm.HomePage;
import com.procuro.androidscm.R;


public class HeaderFragment extends Fragment {

    private ImageButton reward;
    private Button home;
    TextView title;

    public HeaderFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.distributor_main_header, container, false);
        home = view.findViewById(R.id.home);
        title = view.findViewById(R.id.username);

        SetupHeader();

        return view;

    }

    private void SetupHeader(){
        DisplayDefaultHeader();
    }

    private void DisplayDefaultHeader(){
        title.setText("aPIMM™ Suppliers");
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getContext())
                        .setMessage("Are you sure you want to exit?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent = new Intent(getActivity(), HomePage.class);
                                getActivity().startActivity(intent);
                                getActivity().finish();
                            }
                        })
                        .setNegativeButton("No", null)
                        .show();
            }
        });
    }

}
