package com.procuro.androidscm.Supplier.Alerts;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.procuro.androidscm.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


public class AlertsFragment extends Fragment {

    TextView date;


    public AlertsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.distributor_alerts_fragment, container, false);
        date = view.findViewById(R.id.date);

        DisplayData();

        return view;
    }

    private void DisplayData(){
        SimpleDateFormat format = new SimpleDateFormat("EEEE, MMM d, yyyy");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        date.setText(format.format(new Date()));
    }



}
