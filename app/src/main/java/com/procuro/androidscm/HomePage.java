package com.procuro.androidscm;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.procuro.androidscm.Distributor.Distributor_HomePage_Activity;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.Login.Login;
import com.procuro.androidscm.Restaurant.Schema.Days;
import com.procuro.androidscm.Restaurant.Schema.Forms;
import com.procuro.androidscm.Restaurant.Schema.Schema;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.StoreListFragment.SiteList;
import com.procuro.androidscm.Restaurant.Restaurant_home_page;
import com.procuro.androidscm.Supplier.Supplier_HomePage_Activity;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.PimmDevice;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.TimeZone;

public class HomePage extends AppCompatActivity {
    LinearLayout logout,restaurant,UserManagement,distributor,supplier;
    TextView username;
    ProgressDialog progressDialog;
    com.procuro.apimmdatamanagerlib.aPimmDataManager dataManager;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        logout = findViewById(R.id.logout);
        username = findViewById(R.id.username);
        restaurant = findViewById(R.id.restaurant);
        distributor = findViewById(R.id.distributor);
        supplier = findViewById(R.id.supplier);


        setUpOnclicks();

        username.setText(SCMDataManager.getInstance().getUsername());



    }

    private void setUpOnclicks(){
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayLogoutConfirmation();
            }
        });
        restaurant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayRestaurant();
            }
        });

        distributor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomePage.this, Distributor_HomePage_Activity.class);
                startActivity(intent);
                finish();
            }
        });

        supplier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomePage.this, Supplier_HomePage_Activity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    private void DisplayLogoutConfirmation() {
        DialogFragment newFragment = Logout_Confirmation_DialogFragment.newInstance();
        assert getFragmentManager() != null;
        newFragment.show(getSupportFragmentManager(), "dialog");
    }

    @Override
    public void onBackPressed() {
        DisplayLogoutConfirmation();
}


    private void DisplayRestaurant() {
        try {
            ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Please Wait");
            progressDialog.setMessage("Downloading Data");
            progressDialog.setCancelable(false);
            progressDialog.show();

            Login.Download_getSiteListForLoggedInUser(progressDialog,this,true);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void DisplayUnderMaintenanceMessage() {
        DialogFragment newFragment = UnderMaintenance_DialogFragment.newInstance();
        assert getFragmentManager() != null;
        newFragment.show(getSupportFragmentManager(), "dialog");
    }


}
