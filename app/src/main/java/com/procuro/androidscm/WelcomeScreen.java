package com.procuro.androidscm;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.gson.JsonObject;
import com.procuro.androidscm.Restaurant.Login.Login;
import com.procuro.androidscm.Restaurant.Restaurant_home_page;
import com.procuro.apimmdatamanagerlib.User;


public class WelcomeScreen extends AppCompatActivity {

    Button back, distribution,suppliers,restaurants,usermanagement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_screen);

        back = findViewById(R.id.back_button);
        suppliers = findViewById(R.id.suppliers);
        distribution = findViewById(R.id.distribution);
        restaurants = findViewById(R.id.restaurants);
        usermanagement = findViewById(R.id.user_management);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WelcomeScreen.this, Login.class);
                startActivity(intent);
                finish();
            }
        });

        restaurants.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WelcomeScreen.this, Restaurant_home_page.class);
                startActivity(intent);
                finish();
            }
        });


    }



}
