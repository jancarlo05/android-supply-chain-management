package com.procuro.androidscm;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;
import android.view.View;

import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_CleaningSlider.Daily.CleaningGuideList;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.DailyLabor.DailyLaborData;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.WeeklySchedule.WeeklyPickerDate;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Traning.Dashboard_TraningData;
import com.procuro.androidscm.Restaurant.EmployeeList.EmployeeFilter;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourcesChildList;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourcesGrandChildList;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourcesList;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.EquipmenList.EquipmentList;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Chain;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Qualification_Filter;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Regional;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.FormsList.FormListData;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.StoreProfileOptions.StoreProfileOptionsData;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.TeamAndContancts.ManagementTeam.ManagementTeamData;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsDateChildList;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsDateGrandChildList;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsParentList;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.StatusFilter;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.StoreListFragment.SiteList;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.TemperatureListFragment.Status_Journal_Parent_data;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.TemperatureListFragment.TemperatureList;
import com.procuro.androidscm.Restaurant.SOS.SOSData;
import com.procuro.apimmdatamanagerlib.ApplicationReport;
import com.procuro.apimmdatamanagerlib.CertificationDefinition;
import com.procuro.apimmdatamanagerlib.CorpStructure;
import com.procuro.apimmdatamanagerlib.DashboardKitchenData;
import com.procuro.apimmdatamanagerlib.DocumentDTO;
import com.procuro.apimmdatamanagerlib.HoursOfOperation;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.PimmForm;
import com.procuro.apimmdatamanagerlib.PimmInstance;
import com.procuro.apimmdatamanagerlib.SMSCleaningDaily;
import com.procuro.apimmdatamanagerlib.SMSCleaningMonthly;
import com.procuro.apimmdatamanagerlib.SMSCleaningWeekly;
import com.procuro.apimmdatamanagerlib.SMSCommunicationNotes;
import com.procuro.apimmdatamanagerlib.SMSDailyOpsPlan;
import com.procuro.apimmdatamanagerlib.SMSDailyOpsPlanCustomerSatisfactionItem;
import com.procuro.apimmdatamanagerlib.SMSDashboard;
import com.procuro.apimmdatamanagerlib.SMSPositioning;
import com.procuro.apimmdatamanagerlib.Schedule_Business_Site_Plan;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.SiteSales;
import com.procuro.apimmdatamanagerlib.SiteSettings;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;
import com.procuro.apimmthriftservices.services.NamedServices;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsConfiguration;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsEquipment;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsSiteConfig;

import org.apache.thrift.TException;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static com.procuro.apimmdatamanagerlib.OnCompleteListeners.*;

public class SCMDataManager {
    private static SCMDataManager instance = new SCMDataManager();

    private transient Activity activity;
    private String SPID;
    private String username;
    private String password;
    private boolean remembeMe;

    private User user;

    private ArrayList<ReportsParentList>reports_inspections;
    private ArrayList<ReportsParentList>reports_delivery;
    private ArrayList<ReportsParentList>reports_audits;
    private ArrayList<ReportsParentList>report_ratings;


    private ArrayList<ResourcesList>resourcesLists;
    private ArrayList<ResourcesList>TrainingVideos;

    private ArrayList<SiteList>siteLists;
    private ArrayList<CustomSite>customSites;

    private TemperatureList temperatureList;
    private CustomSite selectedSite;
    private String ReportLevel;
    private String ReportsSelectedSite;
    private PimmInstance SelectedPimmInstance;
    private boolean QualificationSelected;
    private ReportsDateGrandChildList selectedReportsDateGrandChild;
    private ReportsDateChildList seletedReportsDateChildList;
    private ApplicationReport selectedApplicationReport;



    private ArrayList<CleaningGuideList>CleaningGuideDailies;
    private ArrayList<CleaningGuideList>CleaningGuideWeekly;
    private ArrayList<CleaningGuideList>CleaningMonthly;

    private SMSDashboard smsDashboard ;

    private ArrayList<CertificationDefinition>certificationDefinitions;
    private boolean ProfileEditEnabled;

    private DashboardKitchenData dashboardKitchenData;
    private SMSPositioning positionForm;
    private SMSCleaningDaily cleaningDailyForm;
    private SMSCleaningMonthly cleaningMonthlyForm;
    private SMSCleaningWeekly cleaningWeeklyForm;
    private SMSDailyOpsPlan dailyOpsPlanForm;
    private ArrayList<Daypart>dayparts;


    private ArrayList<ResourcesGrandChildList>dashboardTaste;
    private ArrayList<ResourcesGrandChildList>dashboardFriendliness;
    private ArrayList<ResourcesGrandChildList>dashboardSpeed;
    private ArrayList<ResourcesGrandChildList>dashboardAccuracy;
    private ArrayList<ResourcesGrandChildList>dashboardCleanliness;
    private ArrayList<ResourcesGrandChildList>dashboardBackroom;
    private ArrayList<ResourcesGrandChildList>dashboardCustomerview;
    private ArrayList<ResourcesGrandChildList>dashboardProduction;
    private ArrayList<ResourcesGrandChildList>dashboardServices;
    private ArrayList<ResourcesChildList>SafetyDataSheets;
    private ArrayList<ResourcesChildList>vtas;


    private ArrayList<CorpStructure>corpStructures;
    private ArrayList<Corp_Chain>Ownershipview;
    private ArrayList<Corp_Chain>Corpview;
    private ArrayList<Corp_Regional>Siteview;


    private ArrayList<Qualification_Filter>qualificationFilterArrayList;

    private SOSData sosData;
    private ArrayList<SMSCommunicationNotes> smsCommunicationNotes;
    private ArrayList<Dashboard_TraningData>dashboardTraningData;
    private ArrayList<CustomUser>storeUsers;
    private ArrayList<StatusFilter> statusFilters;
    private ArrayList<EmployeeFilter> employeeFilters;

    private String[]countries;
    private String[]countrycode;

    private String FormpackCurrentVersion;

    private ArrayList<WeeklyPickerDate>weeklyPickerDates;
    private ArrayList<SMSDailyOpsPlanCustomerSatisfactionItem>dashboardDopHudlle;
    private SiteSettings siteSettings;
    private ArrayList<SiteSales>siteSales;
    private ArrayList<SiteSales>prevSiteSales;
    private SiteSales currentSiteSale;
    private ArrayList<DocumentDTO> documentDTOArrayList;
    private HoursOfOperation hoursOfOperation;

    private ArrayList<PimmForm> pimmFormArrayList;


    private ArrayList<SiteSettingsConfiguration>settingsConfigurations;
    private ArrayList<Schedule_Business_Site_Plan> dailySchedule;
    private ArrayList<Schedule_Business_Site_Plan> weeklySchedule;


    private ArrayList<DailyLaborData>dailyLaborDataArrayList;



    public static SCMDataManager getInstance() {
        if(instance == null) {
            instance = new SCMDataManager();
        }
        return instance;
    }

    public ArrayList<DailyLaborData> getDailyLaborDataArrayList() {
        return dailyLaborDataArrayList;
    }

    public void setDailyLaborDataArrayList(ArrayList<DailyLaborData> dailyLaborDataArrayList) {
        this.dailyLaborDataArrayList = dailyLaborDataArrayList;
    }

    public ArrayList<Schedule_Business_Site_Plan> getWeeklySchedule() {
        return weeklySchedule;
    }

    public void setWeeklySchedule(ArrayList<Schedule_Business_Site_Plan> weeklySchedule) {
        this.weeklySchedule = weeklySchedule;
    }

    public ArrayList<Schedule_Business_Site_Plan> getDailySchedule() {
        return dailySchedule;
    }

    public void setDailySchedule(ArrayList<Schedule_Business_Site_Plan> dailySchedule) {
        this.dailySchedule = dailySchedule;
    }

    public ArrayList<SiteSettingsConfiguration> getSettingsConfigurations() {
        return settingsConfigurations;
    }

    public void setSettingsConfigurations(ArrayList<SiteSettingsConfiguration> settingsConfigurations) {
        this.settingsConfigurations = settingsConfigurations;
    }

    public ArrayList<PimmForm> getPimmFormArrayList() {
        return pimmFormArrayList;
    }

    public void setPimmFormArrayList(ArrayList<PimmForm> pimmFormArrayList) {
        this.pimmFormArrayList = pimmFormArrayList;
    }

    public SiteSales getCurrentSiteSale() {
        return currentSiteSale;
    }

    public void setCurrentSiteSale(SiteSales currentSiteSale) {
        this.currentSiteSale = currentSiteSale;
    }

    public ArrayList<DocumentDTO> getDocumentDTOArrayList() {
        return documentDTOArrayList;
    }

    public void setDocumentDTOArrayList(ArrayList<DocumentDTO> documentDTOArrayList) {
        this.documentDTOArrayList = documentDTOArrayList;
    }

    public HoursOfOperation getHoursOfOperation() {
        return hoursOfOperation;
    }

    public void setHoursOfOperation(HoursOfOperation hoursOfOperation) {
        this.hoursOfOperation = hoursOfOperation;
    }

    public ArrayList<SiteSales> getSiteSales() {
        return siteSales;
    }

    public void setSiteSales(ArrayList<SiteSales> siteSales) {
        this.siteSales = siteSales;
    }

    public ArrayList<SiteSales> getPrevSiteSales() {
        return prevSiteSales;
    }

    public void setPrevSiteSales(ArrayList<SiteSales> prevSiteSales) {
        this.prevSiteSales = prevSiteSales;
    }

    public SiteSettings getSiteSettings() {
        return siteSettings;
    }

    public void setSiteSettings(SiteSettings siteSettings) {
        this.siteSettings = siteSettings;
    }

    public ArrayList<SMSDailyOpsPlanCustomerSatisfactionItem> getDashboardDopHudlle() {
        return dashboardDopHudlle;
    }

    public void setDashboardDopHudlle(ArrayList<SMSDailyOpsPlanCustomerSatisfactionItem> dashboardDopHudlle) {
        this.dashboardDopHudlle = dashboardDopHudlle;
    }

    public String getFormpackCurrentVersion() {
        return FormpackCurrentVersion;
    }

    public void setFormpackCurrentVersion(String formpackCurrentVersion) {
        FormpackCurrentVersion = formpackCurrentVersion;
    }

    public String[] getCountries() {
        return countries;
    }

    public void setCountries(String[] countries) {
        this.countries = countries;
    }

    public String[] getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(String[] countrycode) {
        this.countrycode = countrycode;
    }

    public ArrayList<EmployeeFilter> getEmployeeFilters() {
        return employeeFilters;
    }

    public void setEmployeeFilters(ArrayList<EmployeeFilter> employeeFilters) {
        this.employeeFilters = employeeFilters;
    }

    public ArrayList<StatusFilter> getStatusFilters() {
        return statusFilters;
    }

    public void setStatusFilters(ArrayList<StatusFilter> statusFilters) {
        this.statusFilters = statusFilters;
    }

    public ArrayList<CustomUser> getStoreUsers() {
        return storeUsers;
    }

    public void setStoreUsers(ArrayList<CustomUser> storeUsers) {
        this.storeUsers = storeUsers;
    }

    public ArrayList<Dashboard_TraningData> getDashboardTraningData() {
        return dashboardTraningData;
    }

    public void setDashboardTraningData(ArrayList<Dashboard_TraningData> dashboardTraningData) {
        this.dashboardTraningData = dashboardTraningData;
    }

    public ArrayList<ResourcesChildList> getVtas() {
        return vtas;
    }

    public void setVtas(ArrayList<ResourcesChildList> vtas) {
        this.vtas = vtas;
    }

    public ArrayList<ResourcesChildList> getSafetyDataSheets() {
        return SafetyDataSheets;
    }

    public void setSafetyDataSheets(ArrayList<ResourcesChildList> safetyDataSheets) {
        SafetyDataSheets = safetyDataSheets;
    }

    public ArrayList<SMSCommunicationNotes> getSmsCommunicationNotes() {
        return smsCommunicationNotes;
    }

    public void setSmsCommunicationNotes(ArrayList<SMSCommunicationNotes> smsCommunicationNotes) {
        this.smsCommunicationNotes = smsCommunicationNotes;
    }

    public SOSData getSosData() {
        return sosData;
    }

    public void setSosData(SOSData sosData) {
        this.sosData = sosData;
    }

    public ArrayList<Qualification_Filter> getQualificationFilterArrayList() {
        return qualificationFilterArrayList;
    }

    public void setQualificationFilterArrayList(ArrayList<Qualification_Filter> qualificationFilterArrayList) {
        this.qualificationFilterArrayList = qualificationFilterArrayList;
    }

    public ArrayList<Corp_Chain> getOwnershipview() {
        return Ownershipview;
    }

    public void setOwnershipview(ArrayList<Corp_Chain> ownershipview) {
        Ownershipview = ownershipview;
    }

    public ArrayList<Corp_Chain> getCorpview() {
        return Corpview;
    }

    public void setCorpview(ArrayList<Corp_Chain> corpview) {
        Corpview = corpview;
    }

    public ArrayList<Corp_Regional> getSiteview() {
        return Siteview;
    }

    public void setSiteview(ArrayList<Corp_Regional> siteview) {
        Siteview = siteview;
    }

    public ArrayList<CorpStructure> getCorpStructures() {
        return corpStructures;
    }



    public void setCorpStructures(ArrayList<CorpStructure> corpStructures) {
        this.corpStructures = corpStructures;
    }

    public ArrayList<ResourcesGrandChildList> getDashboardbackroom() {
        return dashboardBackroom;
    }

    public void setDashboardbackroom(ArrayList<ResourcesGrandChildList> dashboardbackroom) {
        this.dashboardBackroom = dashboardbackroom;
    }

    public ArrayList<ResourcesGrandChildList> getDashboardcustomerview() {
        return dashboardCustomerview;
    }

    public void setDashboardcustomerview(ArrayList<ResourcesGrandChildList> dashboardcustomerview) {
        this.dashboardCustomerview = dashboardcustomerview;
    }

    public ArrayList<ResourcesGrandChildList> getDashboardproduction() {
        return dashboardProduction;
    }

    public void setDashboardproduction(ArrayList<ResourcesGrandChildList> dashboardproduction) {
        this.dashboardProduction = dashboardproduction;
    }

    public ArrayList<ResourcesGrandChildList> getDashboardservices() {
        return dashboardServices;
    }

    public void setDashboardservices(ArrayList<ResourcesGrandChildList> dashboardservices) {
        this.dashboardServices = dashboardservices;
    }

    public ArrayList<ResourcesGrandChildList> getDashboardTaste() {
        return dashboardTaste;
    }

    public void setDashboardTaste(ArrayList<ResourcesGrandChildList> dashboardTaste) {
        this.dashboardTaste = dashboardTaste;
    }

    public ArrayList<ResourcesGrandChildList> getDashboardFriendliness() {
        return dashboardFriendliness;
    }

    public void setDashboardFriendliness(ArrayList<ResourcesGrandChildList> dashboardFriendliness) {
        this.dashboardFriendliness = dashboardFriendliness;
    }

    public ArrayList<ResourcesGrandChildList> getDashboardSpeed() {
        return dashboardSpeed;
    }

    public void setDashboardSpeed(ArrayList<ResourcesGrandChildList> dashboardSpeed) {
        this.dashboardSpeed = dashboardSpeed;
    }

    public ArrayList<ResourcesGrandChildList> getDashboardAccuracy() {
        return dashboardAccuracy;
    }

    public void setDashboardAccuracy(ArrayList<ResourcesGrandChildList> dashboardAccuracy) {
        this.dashboardAccuracy = dashboardAccuracy;
    }

    public ArrayList<ResourcesGrandChildList> getDashboardCleanliness() {
        return dashboardCleanliness;
    }

    public void setDashboardCleanliness(ArrayList<ResourcesGrandChildList> dashboardCleanliness) {
        this.dashboardCleanliness = dashboardCleanliness;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public ArrayList<Daypart> getDayparts() {
        return dayparts;
    }

    public void setDayparts(ArrayList<Daypart> dayparts) {
        this.dayparts = dayparts;
    }

    public SMSPositioning getPositionForm() {
        return positionForm;
    }

    public void setPositionForm(SMSPositioning positionForm) {
        this.positionForm = positionForm;
    }

    public SMSCleaningDaily getCleaningDailyForm() {
        return cleaningDailyForm;
    }

    public void setCleaningDailyForm(SMSCleaningDaily cleaningDailyForm) {
        this.cleaningDailyForm = cleaningDailyForm;
    }

    public SMSCleaningMonthly getCleaningMonthlyForm() {
        return cleaningMonthlyForm;
    }

    public void setCleaningMonthlyForm(SMSCleaningMonthly cleaningMonthlyForm) {
        this.cleaningMonthlyForm = cleaningMonthlyForm;
    }

    public SMSCleaningWeekly getCleaningWeeklyForm() {
        return cleaningWeeklyForm;
    }

    public void setCleaningWeeklyForm(SMSCleaningWeekly cleaningWeeklyForm) {
        this.cleaningWeeklyForm = cleaningWeeklyForm;
    }

    public SMSDailyOpsPlan getDailyOpsPlanForm() {
        return dailyOpsPlanForm;
    }

    public void setDailyOpsPlanForm(SMSDailyOpsPlan dailyOpsPlanForm) {
        this.dailyOpsPlanForm = dailyOpsPlanForm;
    }

    public DashboardKitchenData getDashboardKitchenData() {
        return dashboardKitchenData;
    }

    public void setDashboardKitchenData(DashboardKitchenData dashboardKitchenData) {
        this.dashboardKitchenData = dashboardKitchenData;
    }

    public ArrayList<CertificationDefinition> getCertificationDefinitions() {
        return certificationDefinitions;
    }

    public void setCertificationDefinitions(ArrayList<CertificationDefinition> certificationDefinitions) {
        this.certificationDefinitions = certificationDefinitions;
    }

    public boolean isProfileEditEnabled() {
        return ProfileEditEnabled;
    }

    public void setProfileEditEnabled(boolean profileEditEnabled) {
        ProfileEditEnabled = profileEditEnabled;
    }

    public SMSDashboard getSmsDashboard() {
        return smsDashboard;
    }

    public void setSmsDashboard(SMSDashboard smsDashboard) {
        this.smsDashboard = smsDashboard;
    }

    public ArrayList<CleaningGuideList> getCleaningGuideWeekly() {
        return CleaningGuideWeekly;
    }

    public void setCleaningGuideWeekly(ArrayList<CleaningGuideList> cleaningGuideWeekly) {
        CleaningGuideWeekly = cleaningGuideWeekly;
    }

    public ArrayList<CleaningGuideList> getCleaningMonthly() {
        return CleaningMonthly;
    }

    public void setCleaningMonthly(ArrayList<CleaningGuideList> cleaningMonthly) {
        CleaningMonthly = cleaningMonthly;
    }

    public ArrayList<CleaningGuideList> getCleaningGuideDailies() {
        return CleaningGuideDailies;
    }

    public void setCleaningGuideDailies(ArrayList<CleaningGuideList> cleaningGuideDailies) {
        CleaningGuideDailies = cleaningGuideDailies;
    }


    public ReportsDateChildList getSeletedReportsDateChildList() {
        return seletedReportsDateChildList;
    }

    public void setSeletedReportsDateChildList(ReportsDateChildList seletedReportsDateChildList) {
        this.seletedReportsDateChildList = seletedReportsDateChildList;
    }

    public ApplicationReport getSelectedApplicationReport() {
        return selectedApplicationReport;
    }

    public void setSelectedApplicationReport(ApplicationReport selectedApplicationReport) {
        this.selectedApplicationReport = selectedApplicationReport;
    }

    public ReportsDateGrandChildList getSelectedReportsDateGrandChild() {
        return selectedReportsDateGrandChild;
    }

    public void setSelectedReportsDateGrandChild(ReportsDateGrandChildList selectedReportsDateGrandChild) {
        this.selectedReportsDateGrandChild = selectedReportsDateGrandChild;
    }

    public boolean isQualificationSelected() {
        return QualificationSelected;
    }

    public void setQualificationSelected(boolean qualificationSelected) {
        QualificationSelected = qualificationSelected;
    }

    public PimmInstance getSelectedPimmInstance() {
        return SelectedPimmInstance;
    }

    public void setSelectedPimmInstance(PimmInstance selectedPimmInstance) {
        SelectedPimmInstance = selectedPimmInstance;
    }

    public String getReportsSelectedSite() {
        return ReportsSelectedSite;
    }

    public void setReportsSelectedSite(String reportsSelectedSite) {
        ReportsSelectedSite = reportsSelectedSite;
    }

    public ArrayList<ReportsParentList> getReport_ratings() {
        return report_ratings;
    }

    public void setReport_ratings(ArrayList<ReportsParentList> report_ratings) {
        this.report_ratings = report_ratings;
    }

    public String getReportLevel() {
        return ReportLevel;
    }

    public void setReportLevel(String reportLevel) {
        ReportLevel = reportLevel;
    }

    public ArrayList<ReportsParentList> getReports_audits() {
        return reports_audits;
    }

    public void setReports_audits(ArrayList<ReportsParentList> reports_audits) {
        this.reports_audits = reports_audits;
    }

    public ArrayList<ReportsParentList> getReports_delivery() {
        return reports_delivery;
    }

    public void setReports_delivery(ArrayList<ReportsParentList> reports_delivery) {
        this.reports_delivery = reports_delivery;
    }

    public ArrayList<ResourcesList> getTrainingVideos() {
        return TrainingVideos;
    }

    public void setTrainingVideos(ArrayList<ResourcesList> trainingVideos) {
        TrainingVideos = trainingVideos;
    }

    public CustomSite getSelectedSite() {
        return selectedSite;
    }

    public void setSelectedSite(CustomSite selectedSite) {
        this.selectedSite = selectedSite;
    }

    public TemperatureList getTemperatureList() {
        return temperatureList;
    }

    public void setTemperatureList(TemperatureList temperatureList) {
        this.temperatureList = temperatureList;
    }

    public static void setInstance(SCMDataManager instance) {
        SCMDataManager.instance = instance;
    }


    public ArrayList<CustomSite> getCustomSites() {
        return customSites;
    }

    public void setCustomSites(ArrayList<CustomSite> customSites) {
        this.customSites = customSites;
    }


    public ArrayList<SiteList> getSiteLists() {
        return siteLists;
    }

    public void setSiteLists(ArrayList<SiteList> siteLists) {
        this.siteLists = siteLists;
    }

    public boolean isRemembeMe() {
        return remembeMe;
    }

    public void setRemembeMe(boolean remembeMe) {
        this.remembeMe = remembeMe;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ArrayList<ResourcesList> getResourcesLists() {
        return resourcesLists;
    }

    public void setResourcesLists(ArrayList<ResourcesList> resourcesLists) {
        this.resourcesLists = resourcesLists;
    }

    public ArrayList<ReportsParentList> getReports_inspections() {
        return reports_inspections;
    }

    public void setReports_inspections(ArrayList<ReportsParentList> reports_inspections) {
        this.reports_inspections = reports_inspections;
    }

    public String getSPID() {
        return SPID;
    }

    public void setSPID(String SPID) {
        this.SPID = SPID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Daypart getCurrentDaypart(){
        Daypart currentdaypart = null;
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd MMM,yyyy hh:mm:ss a");

        if (getDayparts() !=null){
            for (Daypart daypart: getDayparts()){
                if (daypart.getStart_time().after(date) && date.before(daypart.getEnd_time())){
                    currentdaypart = daypart;
                    break;
                }else if (date.after(daypart.getStart_time()) && date.before(daypart.getStart_time())){
                    currentdaypart = daypart;
                    break;
                }
                else {
                    System.out.println("Start time : "+format.format(daypart.getStart_time()));
                    System.out.println("End time : "+format.format(daypart.getEnd_time()));
                    System.out.println("Currend Day: "+format.format(date));
                }

            }
        }

        return currentdaypart;
    }
    public Date setTimeZoneToSelectedStore(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        calendar.add(Calendar.HOUR,this.getSelectedSite().getSite().TimezoneId);

        return calendar.getTime();
    }

    public ArrayList<WeeklyPickerDate> getWeeklyPickerDates() {
        return weeklyPickerDates;
    }

    public void setWeeklyPickerDates(ArrayList<WeeklyPickerDate> weeklyPickerDates) {
        this.weeklyPickerDates = weeklyPickerDates;
    }

    public static void DownloadHoursOfOperationData(final String siteid) {
        aPimmDataManager dataManager = aPimmDataManager.getInstance();
        dataManager.getDocumentListForReferenceId(siteid, new getDocumentListForReferenceIdCallbackListener() {
            @Override
            public void getDocumentListForReferenceIdCallback(ArrayList<DocumentDTO> documentDTOArrayList, Error error) {
                if (error == null) {
                    SCMDataManager.getInstance().setDocumentDTOArrayList(documentDTOArrayList);
                    if (documentDTOArrayList != null) {
                        for (DocumentDTO documentDTO : documentDTOArrayList) {
                            System.out.println("CATEGORY : "+ documentDTO.category + " | "+documentDTO.documentId);
                            if (documentDTO.category.equalsIgnoreCase("Hours_Of_Operation")) {
                                getDocumentContentForDocumentId(documentDTO.documentId,documentDTO.category);
//                                DownloadPositionForm(siteid);
                                break;
                            }
                        }
                    }
                }
            }
        });
    }

    public static void DownloadSiteSettingsConfiguration(final Site site) {
        Log.i("Thrift ","DownloadSiteSettingsConfiguration start");
        ArrayList<SiteSettingsEquipment>equipments = new ArrayList<>();
        try {
            final String siteId = site.siteid;

            final NamedServices ns = NamedServices.getInstance();
            if (ns.pimmServer!=null){
                final SiteSettingsSiteConfig config = ns.siteSettingsClient;

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        ArrayList sitesettingconfig = null;
                        try {
                            sitesettingconfig = config.getSiteProperties(siteId).configSettings;
                            if (sitesettingconfig!=null){
                                SCMDataManager.getInstance().setSettingsConfigurations(sitesettingconfig);
                                sitesettingconfig.addAll(sitesettingconfig);
                            }
                        } catch (TException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();

            }else {
                Log.i("Thrift ","SERVER NULL ");
            }
            Log.i("Thrift ","DownloadSiteSettingsConfiguration End");

        }catch (TException e){
            e.printStackTrace();
        }
    }

    public static void DownloadSitePropertyValues(String siteID){
        aPimmDataManager dataManager = aPimmDataManager.getInstance();
        dataManager.getSiteSettingsForSiteId(siteID, new OnCompleteListeners.getSiteSettingsForSiteIdListener() {
            @Override
            public void getSiteSettingsForSiteId(SiteSettings siteSettings, Error error) {
                if (error == null){
                    SCMDataManager.getInstance().setSiteSettings(siteSettings);
                }
            }
        });
    }


    public static void DownloadPositionForm(final String siteid){
        aPimmDataManager dataManager = aPimmDataManager.getInstance();
        ArrayList<PimmForm>PimmFormList = SCMDataManager.getInstance().getPimmFormArrayList();
        if (PimmFormList == null){
            dataManager.getFormDefinitionListForAppId(new OnCompleteListeners.getFormDefinitionListForAppIdCallbackListener() {
                @Override
                public void getFormDefinitionListForAppIdCallback(ArrayList<PimmForm> pimmFormArrayList, Error error) {
                    if (error == null){
                        SCMDataManager.getInstance().setPimmFormArrayList(pimmFormArrayList);
                        for (final PimmForm form: pimmFormArrayList){
                            if (form.name.equalsIgnoreCase("SMS:Positioning")) {
                                Download_SMS_POSITIONING(siteid, form.formDefinitionID);
                                break;
                            }
                        }
                    }
                }
            });
        }
        else {
            for (final PimmForm form: PimmFormList){
                if (form.name.equalsIgnoreCase("SMS:Positioning")) {
                    Download_SMS_POSITIONING(siteid, form.formDefinitionID);
                    break;
                }
            }
        }
    }

    public static void Download_SMS_POSITIONING(String siteID,String formID){
        aPimmDataManager dataManager = aPimmDataManager.getInstance();
        dataManager.getLatestFormForReferenceId(siteID, formID, new OnCompleteListeners.getLatestFormForReferenceIdCallbackListener() {
            @Override
            public void getLatestFormForReferenceId(PimmForm pimmForm, Error error) {
                if (error == null){
                    try {
                        if (pimmForm!=null){
                            if (pimmForm.StringBody!=null){
                                JSONObject jsonObject = new JSONObject(pimmForm.StringBody);
                                SMSPositioning smsPositioning = new SMSPositioning();
                                smsPositioning.readFromJSONObject(jsonObject);
                                SCMDataManager.getInstance().setPositionForm(smsPositioning);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    public static void getDocumentContentForDocumentId(String documentID,String category){
        aPimmDataManager dataManager = aPimmDataManager.getInstance();
        dataManager.getDocumentContentForDocumentId(documentID, category, new getDocumentContentForDocumentIdCallbackListener() {
            @Override
            public void getDocumentContentForDocumentIdCallback(Object Object, Error error) {
                if (error ==null){
                    try {
                        JSONObject jsonObject = (JSONObject) Object;
                        HoursOfOperation hoursOfOperation = new HoursOfOperation();
                        hoursOfOperation.readFromJSONObject(jsonObject);
                        SCMDataManager.getInstance().setHoursOfOperation(hoursOfOperation);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public static void Download_EMPLOYEE_POSITION_DATA(String documentDtoID, String category){
        aPimmDataManager dataManager = aPimmDataManager.getInstance();
        dataManager.getDocumentContentForDocumentId(documentDtoID, category, new OnCompleteListeners.getDocumentContentForDocumentIdCallbackListener() {
            @Override
            public void getDocumentContentForDocumentIdCallback(Object obj, Error error) {
                if (error ==  null){
                    if (obj !=null){
                        JSONObject jsonObject = (JSONObject) obj;
                        DashboardKitchenData dashboardKitchenData = new DashboardKitchenData();
                        dashboardKitchenData.readFromJSONObject(jsonObject);
                        SCMDataManager.getInstance().setDashboardKitchenData(dashboardKitchenData);
                    }
                }
            }
        });
    }

}
