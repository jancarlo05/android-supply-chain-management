package com.procuro.androidscm.Restaurant.ReportsFragment.Inspection;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.procuro.androidscm.R;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Reports_right_pane_inspection_peformance_fragment extends Fragment {



    public Reports_right_pane_inspection_peformance_fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.reports_right_pane_inspection_performance, container, false);
        WebView webView = rootView.findViewById(R.id.webview);
        TextView date = rootView.findViewById(R.id.date);
        final SimpleDateFormat outputFormat = new SimpleDateFormat("MM/dd/yyyy, HH:mm a");
        date.setText(outputFormat.format(new Date()));


        return rootView;
    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();
    }
}