package com.procuro.androidscm.Restaurant.ReportsFragment.Inspection.FoodSafetySummary;

import com.procuro.apimmdatamanagerlib.CorpStructure;
import com.procuro.apimmdatamanagerlib.FSLAggregateData;
import com.procuro.apimmdatamanagerlib.FSLSite;

import java.util.ArrayList;

public class FSLDistrict {
    private CorpStructure corpStructure;
    private ArrayList<FSLSite>sites;

    private ArrayList<FSLAggregateData>fslAggregateData;

    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public ArrayList<FSLAggregateData> getFslAggregateData() {
        return fslAggregateData;
    }

    public void setFslAggregateData(ArrayList<FSLAggregateData> fslAggregateData) {
        this.fslAggregateData = fslAggregateData;
    }


    public CorpStructure getCorpStructure() {
        return corpStructure;
    }

    public void setCorpStructure(CorpStructure corpStructure) {
        this.corpStructure = corpStructure;
    }

    public ArrayList<FSLSite> getSites() {
        return sites;
    }

    public void setSites(ArrayList<FSLSite> sites) {
        this.sites = sites;
    }
}
