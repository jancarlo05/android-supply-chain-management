package com.procuro.androidscm.Restaurant.Status.StatusFragment.TemperatureListFragment;

import java.io.Serializable;
import java.util.Date;

public class Status_Journal_child_data implements Serializable {
    private String childname;
    private Date date;

    public Status_Journal_child_data(String childname, Date date) {
        this.childname = childname;
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getChildname() {
        return childname;
    }

    public void setChildname(String childname) {
        this.childname = childname;
    }
}
