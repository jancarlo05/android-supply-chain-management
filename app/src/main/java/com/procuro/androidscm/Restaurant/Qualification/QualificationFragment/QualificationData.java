package com.procuro.androidscm.Restaurant.Qualification.QualificationFragment;

import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.SOS.SOSData;
import com.procuro.apimmdatamanagerlib.SMSPositioning;

import java.util.ArrayList;

public class QualificationData {
    private static QualificationData instance = new QualificationData();

    private Corp_Regional regional;
    private String selectedItem;
    private boolean StateByStateViewEnabled;
    private boolean selectedSiteEnabled;
    private boolean OwnerShipViewEnabled;
    private CustomSite selectedSite;
    private ArrayList<CustomUser> users;
    private int allstore_count;
    private boolean allstore;
    private SOSData sosData;
    private SMSPositioning positionForm;

    public static QualificationData getInstance() {
        if(instance == null) {
            instance = new QualificationData();
        }
        return instance;
    }

    public SMSPositioning getPositionForm() {
        return positionForm;
    }

    public void setPositionForm(SMSPositioning positionForm) {
        this.positionForm = positionForm;
    }

    public SOSData getSosData() {
        return sosData;
    }

    public void setSosData(SOSData sosData) {
        this.sosData = sosData;
    }

    public boolean isAllstore() {
        return allstore;
    }

    public void setAllstore(boolean allstore) {
        this.allstore = allstore;
    }

    public ArrayList<CustomUser> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<CustomUser> users) {
        this.users = users;
    }

    public int getAllstore_count() {
        return allstore_count;
    }

    public void setAllstore_count(int allstore_count) {
        this.allstore_count = allstore_count;
    }

    public static void setInstance(QualificationData instance) {
        QualificationData.instance = instance;
    }

    public Corp_Regional getRegional() {
        return regional;
    }

    public void setRegional(Corp_Regional regional) {
        this.regional = regional;
    }

    public String getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(String selectedItem) {
        this.selectedItem = selectedItem;
    }

    public boolean isStateByStateViewEnabled() {
        return StateByStateViewEnabled;
    }

    public void setStateByStateViewEnabled(boolean stateByStateViewEnabled) {
        StateByStateViewEnabled = stateByStateViewEnabled;
    }

    public boolean isSelectedSiteEnabled() {
        return selectedSiteEnabled;
    }

    public void setSelectedSiteEnabled(boolean selectedSiteEnabled) {
        this.selectedSiteEnabled = selectedSiteEnabled;
    }

    public boolean isOwnerShipViewEnabled() {
        return OwnerShipViewEnabled;
    }

    public void setOwnerShipViewEnabled(boolean ownerShipViewEnabled) {
        OwnerShipViewEnabled = ownerShipViewEnabled;
    }

    public CustomSite getSelectedSite() {
        return selectedSite;
    }

    public void setSelectedSite(CustomSite selectedSite) {
        this.selectedSite = selectedSite;
    }
}
