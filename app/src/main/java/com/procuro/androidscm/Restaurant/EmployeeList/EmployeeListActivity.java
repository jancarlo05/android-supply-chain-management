package com.procuro.androidscm.Restaurant.EmployeeList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.EmployeeRerport.EmployeeReportActivity;
import com.procuro.androidscm.Restaurant.EmployeeRerport.EmployeeReportData;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.StatusFilter;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.Status_Filter_Listview_Adapter;
import com.procuro.androidscm.Restaurant.UserProfile.CustomRole;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileActivity;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileActivityOverAll;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileData;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.Qualification;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.User;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.TimeZone;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

public class EmployeeListActivity extends AppCompatActivity {

    public static int Employee_Schedule_Report = 0, Employee_Schedule_Summary = 1;
    public static RecyclerView recyclerView;
    public static EmployeeList_User_RecyclerViewAdapter adapter;
    public static Button back, add, filter, sort;
    public static TextView message;
    public static EditText search;
    public static ArrayList<CustomUser> searchUser = new ArrayList<>();
    private ImageView employee_sched_summary;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_list);
        recyclerView = findViewById(R.id.recyclerView);
        back = findViewById(R.id.home);
        message = findViewById(R.id.message);
        search = findViewById(R.id.search);
        filter = findViewById(R.id.filter);
        add = findViewById(R.id.add_employee);
        employee_sched_summary = findViewById(R.id.employee_sched_summary);

        setOnclicks();

        getCurrentUserRole();

        SelectPerspective(this,EmployeeListActivity.this);

        setUpEmployeeReport(null,employee_sched_summary,Employee_Schedule_Summary,this);

    }

    public static void setUpEmployeeReport(final CustomUser user, View view, final int Type, final FragmentActivity fragmentActivity){

        try {
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    QualificationData data = QualificationData.getInstance();
                    Site site = data.getSelectedSite().getSite();

                    Calendar currentDate = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                    currentDate = SCMTool.getDateWithDaypart(currentDate.getTime().getTime(),site.effectiveUTCOffset);

                    EmployeeReportData employeeReportData = new EmployeeReportData();
                    employeeReportData.setReportType(Type);
                    employeeReportData.setEmployee_Schedule_Report_Date(currentDate.getTime());
                    employeeReportData.setUser(user);
                    employeeReportData.setUsers(data.getUsers());
                    employeeReportData.setSite(site);
                    employeeReportData.setSPID(SCMDataManager.getInstance().getSPID());
                    EmployeeReportData.setInstance(employeeReportData);

                    Intent intent = new Intent(fragmentActivity, EmployeeReportActivity.class);
                    fragmentActivity.startActivity(intent);
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public static void SelectPerspective(Context context,FragmentActivity fragmentActivity){

        search.getText().clear();
        ArrayList<EmployeeFilter>filters = new ArrayList<>();

        if (SCMDataManager.getInstance().getEmployeeFilters()!=null){

            for (EmployeeFilter filter : SCMDataManager.getInstance().getEmployeeFilters()){
                if (filter.isSelected()){

                    if (filter.getName().equalsIgnoreCase("All")){
                        DisplayEmployee(QualificationData.getInstance().getUsers(),context,fragmentActivity);

                    }else if (filter.getName().equalsIgnoreCase("Active")){
                        ArrayList<CustomUser>users = Active();
                        DisplayEmployee(users,context,fragmentActivity);

                    }else if (filter.getName().equalsIgnoreCase("General Manager")){
                        ArrayList<CustomUser>users = GeneralManager();
                        DisplayEmployee(users,context,fragmentActivity);

                    }else if (filter.getName().equalsIgnoreCase("Inactive")){
                        ArrayList<CustomUser>users = Inactive();
                        DisplayEmployee(users,context,fragmentActivity);

                    }else if (filter.getName().equalsIgnoreCase("Crew")){
                        ArrayList<CustomUser>users = Employee();
                        DisplayEmployee(users,context,fragmentActivity);

                    }else if (filter.getName().equalsIgnoreCase("Supervisor")){
                        ArrayList<CustomUser>users = SUPERVISOR();
                        DisplayEmployee(users,context,fragmentActivity);

                    }else if (filter.getName().equalsIgnoreCase("Support Manager")){
                        ArrayList<CustomUser>users = Ops();
                        DisplayEmployee(users,context,fragmentActivity);
                    }
                }
            }

        }else {

            filters.add(new EmployeeFilter("All"));
            filters.add(new EmployeeFilter("Active",true));
            filters.add(new EmployeeFilter("Inactive"));
            filters.add(new EmployeeFilter("Crew"));
            filters.add(new EmployeeFilter("General Manager"));
            filters.add(new EmployeeFilter("Support Manager"));
            filters.add(new EmployeeFilter("Supervisor"));
            SCMDataManager.getInstance().setEmployeeFilters(filters);

            SelectPerspective(context,fragmentActivity);

        }
    }

    public static ArrayList<CustomUser>GeneralManager(){
        ArrayList<CustomUser> users = new ArrayList<>();

        if (QualificationData.getInstance().getUsers()!=null){
            for (CustomUser user : QualificationData.getInstance().getUsers()){

                if (user.getUser().roles!=null){
                    for (String role : user.getUser().roles){
                        if (role.equalsIgnoreCase("Gm")){
                            users.add(user);
                        }else if (role.equalsIgnoreCase("General Manager")){
                            users.add(user);
                        }
                    }
                }
            }
        }
        return users;
    }

    public static ArrayList<CustomUser>Ops(){
        ArrayList<CustomUser> users = new ArrayList<>();

        if (QualificationData.getInstance().getUsers()!=null){
            for (CustomUser user : QualificationData.getInstance().getUsers()){

                if (user.getUser().roles!=null){
                    for (String role : user.getUser().roles){
                        if (role.equalsIgnoreCase("Ops leader")){
                            users.add(user);
                        }
                    }
                }
            }
        }
        return users;
    }

    public static ArrayList<CustomUser>SUPERVISOR(){
        ArrayList<CustomUser> users = new ArrayList<>();
        if (QualificationData.getInstance().getUsers()!=null){
            for (CustomUser user : QualificationData.getInstance().getUsers()){

                if (user.getUser().roles!=null){
                    for (String role : user.getUser().roles){
                        if (role.equalsIgnoreCase("SUPERVISOR")){
                            users.add(user);
                        }
                    }
                }
            }
        }
        return users;
    }

    public static ArrayList<CustomUser>Employee(){
        ArrayList<CustomUser> users = new ArrayList<>();

        if (QualificationData.getInstance().getUsers()!=null){
            for (CustomUser user : QualificationData.getInstance().getUsers()){

                if (user.getUser().roles!=null){
                    for (String role : user.getUser().roles){
                        if (role.equalsIgnoreCase("Employee")){
                            users.add(user);
                        }
                    }
                }
            }
        }
        return users;
    }

    public static ArrayList<CustomUser>Active(){
        ArrayList<CustomUser> users = new ArrayList<>();

        if (QualificationData.getInstance().getUsers()!=null){
            for (CustomUser user : QualificationData.getInstance().getUsers()){
                if (user.getUser().active){
                    users.add(user);
                }
            }
        }
        return users;
    }

    public static ArrayList<CustomUser>Inactive(){
        ArrayList<CustomUser> users = new ArrayList<>();
        if (QualificationData.getInstance().getUsers()!=null){
            for (CustomUser user : QualificationData.getInstance().getUsers()){
                if (!user.getUser().active){
                    users.add(user);
                }
            }
        }
        return users;
    }

    public static void SearchFunction(final ArrayList<CustomUser>users, final Context context, final FragmentActivity fragmentActivity){

        if (users!=null){
            search.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    int textlength = s.length();
                    if (textlength > 0){
                        ArrayList<CustomUser> customUsers = new ArrayList<>();
                        for (CustomUser user : users){
                            StringBuilder name = new StringBuilder();
                            name.append(user.getUser().firstName);
                            name.append(" ");
                            name.append(user.getUser().lastName);
                            if (name.toString().toLowerCase().contains(s.toString().toLowerCase())){
                                customUsers.add(user);
                            }
                        }
                        adapter = new EmployeeList_User_RecyclerViewAdapter(context, customUsers,fragmentActivity,QualificationData.getInstance().getSelectedSite());
                        recyclerView.setLayoutManager(new GridLayoutManager(context,1));
                        recyclerView.setAdapter(adapter);

                        if (adapter.getItemCount()>0){
                            message.setVisibility(View.GONE);
                        }else {
                            message.setVisibility(View.VISIBLE);
                        }


                    }else {
                        adapter = new EmployeeList_User_RecyclerViewAdapter(context, users,fragmentActivity,QualificationData.getInstance().getSelectedSite());
                        recyclerView.setLayoutManager(new GridLayoutManager(context,1));
                        recyclerView.setAdapter(adapter);
                        message.setVisibility(View.GONE);

                    }
                }
            });
        }

    }

    private void setOnclicks(){

        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayFilterMenu(EmployeeListActivity.this,filter,SCMDataManager.getInstance().getEmployeeFilters(),recyclerView,EmployeeListActivity.this);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EmployeeListActivity.this, UserProfileActivityOverAll.class);
                UserProfileData data = new UserProfileData();
                data.setUserEditEnabled(true);
                CustomUser customUser = new CustomUser();
                customUser.setUser(new User());
                data.setCreate(true);
                data.setCustomUser(customUser);
                data.setCustomSite(QualificationData.getInstance().getSelectedSite());
                data.setSelectedSites(SCMDataManager.getInstance().getCustomSites());
                data.setFromQualification(true);
                data.setPrevActivity("EmployeeList");
                UserProfileData.setInstance(data);
                startActivity(intent);
            }
        });
    }

    public static void DisplayEmployee(ArrayList<CustomUser>users,Context context ,FragmentActivity fragmentActivity){

        try {
            if (users!=null){
                if (users.size()>0){

                    Collections.sort(users, new Comparator<CustomUser>() {
                        @Override
                        public int compare(CustomUser o1, CustomUser o2) {
                            return o1.getUser().firstName.compareToIgnoreCase(o2.getUser().firstName);
                        }
                    });

                    message.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    searchUser = users;
                    adapter = new EmployeeList_User_RecyclerViewAdapter(context, users,fragmentActivity,QualificationData.getInstance().getSelectedSite());
                    recyclerView.setLayoutManager(new GridLayoutManager(context,1));
                    recyclerView.setAdapter(adapter);
                    SearchFunction(users,context,fragmentActivity);
                }else {
                    message.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.INVISIBLE);
                }
            }else {
                message.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.INVISIBLE);
            }

        }catch (Exception e){
            e.printStackTrace();
        }


    }

    public static void DisplayFilterMenu(final Context context, View anchor, ArrayList<EmployeeFilter>filters,
                                  RecyclerView recyclerView,FragmentActivity fragmentActivity) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.BOTTOM)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .transparentOverlay(false)
                .arrowColor(ContextCompat.getColor(context, R.color.white))
                .contentView(R.layout.qualification_filter_popup)
                .focusable(true)
                .build();
        tooltip.show();


        ListView listView = tooltip.findViewById(R.id.listview);
        Employee_Filter_Listview_Adapter adapter = new Employee_Filter_Listview_Adapter(context,filters,recyclerView,fragmentActivity,tooltip,message,searchUser,search);
        listView.setAdapter(adapter);

    }

    private void getCurrentUserRole(){
        try {
            User currentUser = SCMDataManager.getInstance().getUser();
            if (currentUser!=null){
                if (currentUser.roles!=null){
                    ArrayList<CustomRole>customRoles = CustomRole.getAllCustomRole(currentUser.roles);
                    if (customRoles.size()>=1){
                        CustomRole customRole = customRoles.get(0);
                        if (customRole.getStatus()>4){
                            SCMTool.DisableView(add,.5f);
                        }
                    }
                }

            }

        }catch (Exception e){
            e.printStackTrace();
        }


    }


    @Override
    protected void onPostResume() {
        super.onPostResume();
        SelectPerspective(this,EmployeeListActivity.this);
    }
}
