package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_CleaningSlider.Daily;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;


import com.procuro.androidscm.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TimeZone;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;


public class Dashboard_Cleaning_Daily_ListviewAdapter extends BaseExpandableListAdapter {

    private final Context context ;
    private ArrayList<CleaningGuideList> parent_lists;
    private ExpandableListView expandableListView;


    public Dashboard_Cleaning_Daily_ListviewAdapter(Context context, ArrayList<CleaningGuideList> data , ExpandableListView expandableListView) {
        this.context = context;
        this.parent_lists = data;
        this.expandableListView = expandableListView;

    }


    @Override
    public int getGroupCount() {
        return parent_lists.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return parent_lists.get(i).getDailyPositions().size();
    }

    @Override
    public Object getGroup(int i) {
        return parent_lists.get(i);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return parent_lists.get(groupPosition).getDailyPositions().get(childPosition);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }


    @Override
    public View getGroupView(int position, boolean b, View contentView, ViewGroup parent) {

        final CleaningGuideList parent_data = parent_lists.get(position);
        contentView = LayoutInflater.from(context).inflate(R.layout.dashboard_cleaning_dialogfragment_parent_data, parent, false);

        TextView title = contentView.findViewById(R.id.title);
        TextView total = contentView.findViewById(R.id.total_completed);
        CheckBox completed = contentView.findViewById(R.id.completed);
        ImageView indicator = contentView.findViewById(R.id.indicator);
        LinearLayout root = contentView.findViewById(R.id.root);


        try {

            title.setText(String.valueOf(parent_data.getDaypartsShifting()));

            int complete_counter = 0;
            for (CleaningPosition cleaningPosition : parent_data.getDailyPositions()){
                if (cleaningPosition.isCompleted()){
                    complete_counter++;
                }
            }
            total.setText(complete_counter+"/"+parent_data.getDailyPositions().size());
            if (complete_counter == parent_data.getDailyPositions().size()){
                completed.setChecked(true);
            }else {
                completed.setChecked(false);
            }
            completed.setEnabled(false);

            if (parent_data.isSelected()){
                root.setBackgroundColor(Color.parseColor("#F68725"));
                indicator.setRotation(180f);
                expandableListView.expandGroup(position);
            }else {
                root.setBackgroundColor(Color.parseColor("#D8333333"));
                indicator.setRotation(270f);
                expandableListView.collapseGroup(position);
            }


        }catch (Exception e){
            e.printStackTrace();
        }


        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (parent_data.isSelected()){
                    parent_data.setSelected(false);
                }else {
                    parent_data.setSelected(true);
                }
                notifyDataSetChanged();
            }
        });

        return contentView;
    }


    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean b, View contentView, final ViewGroup parent) {
        final CleaningPosition child_data = parent_lists.get(groupPosition).getDailyPositions().get(childPosition);
        contentView = LayoutInflater.from(context).inflate(R.layout.dashboard_cleaning_dialogfragment_daily_child_data, parent, false);

        TextView position = contentView.findViewById(R.id.position);
        TextView tasks = contentView.findViewById(R.id.tasks);
        final TextView employee_name = contentView.findViewById(R.id.employee_name);
        CheckBox completed = contentView.findViewById(R.id.completed);
        final TextView assigned_Date = contentView.findViewById(R.id.assigned);

        if (child_data.isCompleted()){
            completed.setChecked(true);
        }else {
            completed.setChecked(false);
        }
        completed.setEnabled(false);

        completed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    child_data.setCompleted(true);
                }else {
                    child_data.setCompleted(false);
                }
                notifyDataSetChanged();
            }
        });

        if (!child_data.getEmployeename().equalsIgnoreCase("null")){
            employee_name.setText(child_data.getEmployeename());
        }

        if (child_data.getAssigned()!=null){
            SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy,h:mm a");
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            assigned_Date.setText(format.format(child_data.getAssigned()).replace(",","\n"));
        }

        position.setText(child_data.getPosition());
        tasks.setText(child_data.getTask());

        return contentView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }




}