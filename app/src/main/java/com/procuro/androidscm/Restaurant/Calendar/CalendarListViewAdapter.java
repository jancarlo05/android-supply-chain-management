package com.procuro.androidscm.Restaurant.Calendar;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

import com.procuro.androidscm.GlideApp;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.Connection_Error_DialogFragment;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.User;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


public class CalendarListViewAdapter extends BaseAdapter {

    // Declare Variables
    public Context mContext;
    private LayoutInflater inflater;
    private ArrayList<CalendarWeeks> arraylist;
    private int selected_month;
    private int selected_year;
    private TextView title;
    private FragmentActivity fragmentActivity;
    private Calendar SelectedMonth = Calendar.getInstance();
    private SimpleDateFormat formats = new SimpleDateFormat("MMMM yyyy");



    public CalendarListViewAdapter(Context context, ArrayList<CalendarWeeks> arraylist,
                                   int selected_month, int selected_year,TextView title,
                                   FragmentActivity fragmentActivity) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;
        this.selected_month = selected_month;
        this.selected_year = selected_year;
        this.fragmentActivity = fragmentActivity;
        this.title = title;

    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return arraylist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
            view = inflater.inflate(R.layout.calendar_cell, null);

            final ArrayList<CalendarDaily>dailies = arraylist.get(position).getDailies();

            ArrayList<TextView>dates = new ArrayList<>();
            ArrayList<LinearLayout>containers = new ArrayList<>();

            LinearLayout mon_container = null,tue_container= null,wed_container= null,thu_container = null,fri_container = null,
                    sat_container = null,sun_container = null;

            TextView weeknumber,mon = null,tue = null,wed = null,thu = null,fri = null,sat = null,sun = null;


            InitializeDatesCellContainer(view,containers,mon_container,tue_container,wed_container,
                    thu_container,fri_container,sat_container,sun_container);

            InitializeTextDate(view,dates,mon,tue,wed,thu,fri,sat,sun);


            weeknumber = view.findViewById(R.id.weeknumber);

            weeknumber.setText(String.valueOf(arraylist.get(position).getWeeknumber()));



            for (int i = 0; i <dailies.size() ; i++) {

             CalendarDaily daily = dailies.get(i);
             DisplayDates(i,daily,dates,containers);


        }

        return view;
    }


    private void InitializeDatesCellContainer(View view,ArrayList<LinearLayout>containers, LinearLayout mon_container,
                                              LinearLayout tue_container,LinearLayout wed_container,
                                              LinearLayout thu_container,LinearLayout fri_container,
                                              LinearLayout sat_container,LinearLayout sun_container){
        mon_container = view.findViewById(R.id.mon_container);
        tue_container = view.findViewById(R.id.tue_container);
        wed_container = view.findViewById(R.id.wed_container);
        thu_container = view.findViewById(R.id.thu_container);
        fri_container = view.findViewById(R.id.fri_container);
        sat_container = view.findViewById(R.id.sat_container);
        sun_container = view.findViewById(R.id.sun_container);

        containers.add(mon_container);
        containers.add(tue_container);
        containers.add(wed_container);
        containers.add(thu_container);
        containers.add(fri_container);
        containers.add(sat_container);
        containers.add(sun_container);
    }

    private void InitializeTextDate(View view,ArrayList<TextView>dates,TextView mon,TextView tue,TextView wed,
                                         TextView thu, TextView fri,TextView sat,TextView sun){

        mon = view.findViewById(R.id.mon);
        tue = view.findViewById(R.id.tue);
        wed = view.findViewById(R.id.wed);
        fri = view.findViewById(R.id.fri);
        thu = view.findViewById(R.id.thu);
        sat = view.findViewById(R.id.sat);
        sun = view.findViewById(R.id.sun);

        dates.add(mon);
        dates.add(tue);
        dates.add(wed);
        dates.add(thu);
        dates.add(fri);
        dates.add(sat);
        dates.add(sun);
    }


    private void DisplayDates(int i , CalendarDaily daily,
                              ArrayList<TextView> dates,
                              ArrayList<LinearLayout>containers){

        LinearLayout container = containers.get(i);
        TextView date = dates.get(i);

        final SimpleDateFormat format = new SimpleDateFormat("d");
        final SimpleDateFormat newformat = new SimpleDateFormat("dd/mm/");
        date.setText(format.format(daily.getDate()));

        Calendar cal1 = Calendar.getInstance();
        cal1.set(Calendar.YEAR,selected_year);
        cal1.set(Calendar.MONTH,selected_month);

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(daily.getDate());

        RemoveDateOutSideSelectedMonth(i,date,cal1,cal2,container);


    }



    private void RemoveDateOutSideSelectedMonth(int i ,TextView date,Calendar cal1 , Calendar cal2,LinearLayout container){

        Site site = SCMDataManager.getInstance().getSelectedSite().getSite();

        Calendar currentdate = Calendar.getInstance();
        currentdate.setTimeZone(TimeZone.getTimeZone("UTC"));
        currentdate.add(Calendar.MINUTE,site.effectiveUTCOffset);

        SimpleDateFormat format = new SimpleDateFormat("yyyy MM");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        //Current year and month
        if (format.format(cal1.getTime()).equalsIgnoreCase(format.format(cal2.getTime()))){

            getCurrentDate(date,cal2,currentdate);

            CheckEmployeeEvent(cal2.getTime(),container);



        }else {
            container.removeAllViews();
        }



    }

    private void setContainerClickLisneter(View view, final Date selectedDate , final ArrayList<CalendarEvent>events){

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayEventList(events,selectedDate);
            }
        });

    }

    private void getCurrentDate(TextView date ,Calendar cal2,Calendar currentdate){

        //Current Date
        if (cal2.get(Calendar.DATE) == currentdate.get(Calendar.DATE)
                && cal2.get(Calendar.YEAR) == currentdate.get(Calendar.YEAR)
                && cal2.get(Calendar.MONTH) == currentdate.get(Calendar.MONTH)){

            date.setBackgroundResource(R.drawable.red_circle);
            date.setTextColor(Color.WHITE);


        }else {
            date.setBackgroundColor(Color.TRANSPARENT);
            date.setTextColor(ContextCompat.getColor(mContext, R.color.black));
        }
    }

    private void CheckEmployeeEvent(Date selectedDate, LinearLayout container){

        ArrayList<CustomUser>users = SCMDataManager.getInstance().getStoreUsers();
        ArrayList<CalendarEvent> events = new ArrayList<>();

        try {
            if (users!=null){
                for (CustomUser user: users){
                    boolean ishired = CheckEmployeeAnniversary(user.getUser(),selectedDate,events,container);
                    if (ishired){
                        CheckEmployeeBirthday(user.getUser(),selectedDate,events,container);
                    }
                }

                if (events.size()>=4){
                    AddNote(events.size() -3,container);
                }
            }

            setContainerClickLisneter(container,selectedDate,events);


        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void CheckEmployeeBirthday(User user, Date selectedDate, ArrayList<CalendarEvent>events, LinearLayout container){
        SimpleDateFormat format = new SimpleDateFormat("dd/MM");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            if (user.DOB!=null){
                if (format.format(user.DOB).equalsIgnoreCase(format.format(selectedDate))){
                    int birthdateyear = SCMTool.getYearByDate(user.DOB);
                    int currentYear = SCMTool.getYearByDate(selectedDate);

                    if (currentYear - birthdateyear >=0){
                        events.add(new CalendarEvent(selectedDate,"birthday",user));
                        if (events.size()<4){
                            AddEventToCalendarCell(R.drawable.birthday,container,user);
                        }
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private boolean CheckEmployeeAnniversary(User user, Date selectedDate, ArrayList<CalendarEvent>events, LinearLayout container){
        SimpleDateFormat format = new SimpleDateFormat("dd/MM");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        boolean hired = false;

        try {
            if (user.hireDate!=null){
                if (format.format(user.hireDate).equalsIgnoreCase(format.format(selectedDate))){

                    int hiredateYear = SCMTool.getYearByDate(user.hireDate);
                    int currentYear = SCMTool.getYearByDate(selectedDate);

                    if (currentYear - hiredateYear >=0){
                        hired = true;
                        events.add(new CalendarEvent(selectedDate,"anniversary",user));
                        if (events.size()<4){
                            AddEventToCalendarCell(R.drawable.anniversary,container,user);

                        }
                    }
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return  hired;
    }

    private void AddEventToCalendarCell(int drawable, LinearLayout container,User user){

        try {
            View view = inflater.inflate(R.layout.calendar_even_icon,null);
            ImageView icon = view.findViewById(R.id.icon);
            TextView name = view.findViewById(R.id.name);

            name.setText(user.firstName);
            GlideApp.with(mContext).asDrawable().load(drawable).into(icon);
            container.addView(view);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void AddNote(int more, LinearLayout container){

        try {
            View view = inflater.inflate(R.layout.calendar_even_icon,null);
            TextView note = view.findViewById(R.id.name);
            ImageView icon = view.findViewById(R.id.icon);

            icon.setVisibility(View.GONE);

            note.setText("  +");
            note.append(String.valueOf(more));
            note.append(" more");
            note.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            container.addView(view);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void DisplayEventList(ArrayList<CalendarEvent>events ,Date date) {
        DialogFragment newFragment = CalendarEventListDialogFragment.newInstance(events,date);
        assert fragmentActivity.getFragmentManager() != null;
        newFragment.show(fragmentActivity.getSupportFragmentManager(), "dialog");
    }


}

