package com.procuro.androidscm.Restaurant.Calendar;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import java.util.List;

class CalendarViewPagerAdapter extends PagerAdapter {

    private List<View> views;
    private Context context;
    private ViewPager viewPager;
    public static int LOOPS_COUNT = 1000;

    public CalendarViewPagerAdapter(List<View> views, Context context, ViewPager viewPager) {
        this.views = views;
        this.context = context;
        this.viewPager = viewPager;

    }

    public View getView(int position) {
        return views.get(position);
    }

    @Override
    public int getCount() {
        return views.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(views.get(position));
    }

    @NonNull
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final View view = views.get(position);
        container.addView(view);
        return view;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        for (int index = 0; index < getCount(); index++) {
            if ((View) object == views.get(index)) {
                return index;
            }
        }
        return POSITION_NONE;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return views.get(position).getTag().toString();
    }


}