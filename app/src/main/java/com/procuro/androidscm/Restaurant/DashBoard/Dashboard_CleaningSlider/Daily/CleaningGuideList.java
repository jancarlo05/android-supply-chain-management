package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_CleaningSlider.Daily;

import java.util.ArrayList;

public class CleaningGuideList {

    private String DaypartsShifting;
    private boolean selected;
    private boolean completed;
    ArrayList<CleaningPosition>dailyPositions;

    public CleaningGuideList(String daypartsShifting, boolean selected, boolean completed, ArrayList<CleaningPosition> dailyPositions) {
        DaypartsShifting = daypartsShifting;
        this.selected = selected;
        this.completed = completed;
        this.dailyPositions = dailyPositions;
    }

    public CleaningGuideList(String daypartsShifting, boolean selected,  ArrayList<CleaningPosition> dailyPositions) {
        DaypartsShifting = daypartsShifting;
        this.selected = selected;
        this.dailyPositions = dailyPositions;
    }

    public String getDaypartsShifting() {
        return DaypartsShifting;
    }

    public void setDaypartsShifting(String daypartsShifting) {
        DaypartsShifting = daypartsShifting;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public ArrayList<CleaningPosition> getDailyPositions() {
        return dailyPositions;
    }

    public void setDailyPositions(ArrayList<CleaningPosition> dailyPositions) {
        this.dailyPositions = dailyPositions;
    }
}
