package com.procuro.androidscm.Restaurant.Calendar;

import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

public class CalendarEventListDialogFragment extends DialogFragment {

    private Button close;
    private RecyclerView recyclerView;
    private TextView eventDate,message;
    private CalendarEventListRecyclerViewAdapter adapter;

    public static ArrayList<CalendarEvent>events;
    public static Date date;


    public static CalendarEventListDialogFragment newInstance(ArrayList<CalendarEvent>EventObj,Date dateObj) {
        events = EventObj;
        date = dateObj;
        return new CalendarEventListDialogFragment();
    }
    @Override
        public int getTheme() {
        return R.style.slide_down_up;
        }

    @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.calendar_event_list_dialog_fragment, container, false);

//                    int height = (int)(getResources().getDisplayMetrics().heightPixels*0.90);
//                    int width = 320;
//
        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
        int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.60);


        Objects.requireNonNull(getDialog().getWindow()).setLayout(width, height);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.slide_down_up;
        view.setFocusableInTouchMode(true);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        close = view.findViewById(R.id.close);
        recyclerView = view.findViewById(R.id.recyclerView);
        eventDate = view.findViewById(R.id.eventDate);
        message = view.findViewById(R.id.message);

        SimpleDateFormat format = new SimpleDateFormat("MMMM d");

        eventDate.setText(format.format(date));
        eventDate.append(" Event");

        if (events.size()>0){
            adapter = new CalendarEventListRecyclerViewAdapter(getContext(),events,getActivity());
            recyclerView.setLayoutManager(new GridLayoutManager(getContext(),1));
            recyclerView.setAdapter(adapter);
            message.setVisibility(View.GONE);
        }

        setupOnclicks();


        return view;
    }


    private void setupOnclicks(){

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }


}
