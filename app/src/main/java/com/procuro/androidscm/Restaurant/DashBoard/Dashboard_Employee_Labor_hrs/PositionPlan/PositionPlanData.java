package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.PositionPlan;

import com.procuro.androidscm.Daypart;
import com.procuro.androidscm.Restaurant.Schema.Dayparts;

import java.util.Date;

public class PositionPlanData {

    private Dayparts daypart;
    private boolean selected;
    private Date startDate;
    private Date endDate;
    private int RequiredPosition;
    private Date curretDate;


    public PositionPlanData(Dayparts daypart, boolean selected, Date startDate, Date endDate, int requiredPosition, Date curretDate) {
        this.daypart = daypart;
        this.selected = selected;
        this.startDate = startDate;
        this.endDate = endDate;
        RequiredPosition = requiredPosition;
        this.curretDate = curretDate;
    }

    public PositionPlanData(Dayparts daypart, boolean selected, Date startDate, Date endDate, int requiredPosition) {
        this.daypart = daypart;
        this.selected = selected;
        this.startDate = startDate;
        this.endDate = endDate;
        RequiredPosition = requiredPosition;
    }

    public Dayparts getDaypart() {
        return daypart;
    }

    public void setDaypart(Dayparts daypart) {
        this.daypart = daypart;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getRequiredPosition() {
        return RequiredPosition;
    }

    public void setRequiredPosition(int requiredPosition) {
        RequiredPosition = requiredPosition;
    }

    public Date getCurretDate() {
        return curretDate;
    }

    public void setCurretDate(Date curretDate) {
        this.curretDate = curretDate;
    }
}
