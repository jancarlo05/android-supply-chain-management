package com.procuro.androidscm.Restaurant.Status.StatusFragment.StoreListFragment;

public class StoreList {
    private String name;
    private int icon;
    private  boolean seclected;

    public StoreList(String name, int icon) {
        this.name = name;
        this.icon = icon;
    }

    public StoreList(String name, int icon, boolean seclected) {
        this.name = name;
        this.icon = icon;
        this.seclected = seclected;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public boolean isSeclected() {
        return seclected;
    }

    public void setSeclected(boolean seclected) {
        this.seclected = seclected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}