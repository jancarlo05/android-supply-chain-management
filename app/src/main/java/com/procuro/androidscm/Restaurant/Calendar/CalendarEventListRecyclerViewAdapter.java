package com.procuro.androidscm.Restaurant.Calendar;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.GlideApp;
import com.procuro.androidscm.R;
import com.procuro.androidscm.SCMTool;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class CalendarEventListRecyclerViewAdapter extends RecyclerView.Adapter<CalendarEventListRecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<CalendarEvent> events;
    private FragmentActivity fragmentActivity;


    public CalendarEventListRecyclerViewAdapter(Context context, ArrayList<CalendarEvent> events,
                                                FragmentActivity fragmentActivity) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.events = events;
        this.fragmentActivity = fragmentActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.calendar_event_cardview,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final CalendarEvent event = events.get(position);

        holder.name.setText(event.getName());
        final int presentyear = SCMTool.getYearByDate(event.getDate());
        final int hiredate = SCMTool.getYearByDate(event.getUser().hireDate);
        int birthdate = SCMTool.getYearByDate(event.getUser().DOB);


        if (event.getType().equalsIgnoreCase("birthday")){
            GlideApp.with(mContext).asDrawable().load(R.drawable.birthday).into(holder.icon);
            holder.description.setText(getBirthdayDescriptionByYear(birthdate,presentyear));

        }if (event.getType().equalsIgnoreCase("Anniversary")){
            GlideApp.with(mContext).asDrawable().load(R.drawable.anniversary).into(holder.icon);
            holder.description.setText(getAnniversaryDescriptionByYear(hiredate,presentyear));
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventPopup(event,presentyear - hiredate);
            }
        });

    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name,description;
        ImageView icon;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            description = itemView.findViewById(R.id.description);
            cardView = itemView.findViewById(R.id.cardview_id);
            icon = itemView.findViewById(R.id.icon);
        }
    }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }


    private String getBirthdayDescriptionByYear(int original, int present){
        String description = "";
        int difference = present -original;

        if (difference == 0) {
            description = "Happy Birthday";
        }
        else if (difference == 1) {
            description = "Happy 1st Birthday";
        }
        else if (difference == 2) {
            description = "Happy 2nd Birthday";
        }
        else if (difference == 3) {
            description = "Happy 3rd Birthday";
        }
        else {
            description = "Happy "+difference+"th Birthday";
        }

        return  description;
    }

    private String getAnniversaryDescriptionByYear(int original, int present){

        String description = "";
        int difference = present-original;

        if (difference == 0) {
            description = "Welcome to the Company";
        }
        else if (difference == 1) {
            description = "Happy 1st Anniversary";
        }
        else if (difference == 2) {
            description = "Happy 2nd Anniversary";
        }
        else if (difference == 3) {
            description = "Happy 3rd Anniversary";
        }
        else {
            description = "Happy "+difference+"th Anniversary";
        }

        return  description;
    }

    private void EventPopup(CalendarEvent event,int years) {
        DialogFragment newFragment = CalendarEventDialogFragment.newInstance(event,years);
        assert fragmentActivity.getFragmentManager() != null;
        newFragment.show(fragmentActivity.getSupportFragmentManager(), "dialog");
    }



}
