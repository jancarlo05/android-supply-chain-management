package com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment;

import com.procuro.apimmdatamanagerlib.DocumentDTO;

public class ResourcesGrandChildList {

    String title;
    boolean selected;
    DocumentDTO documentDTO;

    public ResourcesGrandChildList(String title, boolean selected) {
        this.title = title;
        this.selected = selected;
    }

    public ResourcesGrandChildList(DocumentDTO documentDTO) {
        this.documentDTO = documentDTO;
    }

    public DocumentDTO getDocumentDTO() {
        return documentDTO;
    }

    public void setDocumentDTO(DocumentDTO documentDTO) {
        this.documentDTO = documentDTO;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
