package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.PositionPlan;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.Schema.Dayparts;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.SMSPositioningEmployeeScheduleAssignmentPositions;
import com.procuro.apimmdatamanagerlib.Schedule_Business_Site_Plan;
import com.procuro.apimmdatamanagerlib.Site;
import com.suke.widget.SwitchButton;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static com.procuro.androidscm.SCMTool.DaypartTimeToDate;


public class Dashboard_Position_plan_daypart_RecyclerViewAdapter extends RecyclerView.Adapter<Dashboard_Position_plan_daypart_RecyclerViewAdapter.MyViewHolder> {

    public static Context mContext;
    private LayoutInflater inflater;
    private ArrayList<PositionPlanData> arraylist;
    private Date calendar;
    private FragmentActivity fragmentActivity;
    private ArrayList<CustomUser>users;

    public Dashboard_Position_plan_daypart_RecyclerViewAdapter(Context context, ArrayList<PositionPlanData> arraylist, Date calendar,
                                                               FragmentActivity fragmentActivity,
                                                               ArrayList<CustomUser>users) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;
        this.calendar =calendar;
        this.fragmentActivity = fragmentActivity;
        this.users = users;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.position_plan_daypart_listview_item,parent,false);


        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final PositionPlanData positionPlanData = arraylist.get(position);

        Site site = SCMDataManager.getInstance().getSelectedSite().getSite();
        TimeZone UTC = TimeZone.getTimeZone("UTC");

        SimpleDateFormat dayformat = new SimpleDateFormat("EEE");
        SimpleDateFormat timeformat = new SimpleDateFormat("EEE dd,  hh:mm a");

        timeformat.setTimeZone(UTC);
        final Date currentDate = calendar;

        String daypartTitle = "D"+positionPlanData.getDaypart().name;
        int employeecount = getTotalCount(positionPlanData);

        try {
            holder.daypartname.setText(daypartTitle);
            holder.required_position.setText(String.valueOf(employeecount));
            holder.required_position.append("/");
            holder.required_position.append(String.valueOf(positionPlanData.getRequiredPosition()));


            if (positionPlanData.isSelected()){
                holder.daypartname.setBackgroundResource(R.drawable.rounded_orange_bg);
                holder.daypartname.setTextColor(ContextCompat.getColor(mContext,R.color.white));

                DisplaySelectedDaypart(positionPlanData.getDaypart(),positionPlanData.getRequiredPosition(),positionPlanData.getStartDate(),positionPlanData.getEndDate());

                Dashboard_PositionPlan_fragment.DisplayDaily(currentDate,positionPlanData.getRequiredPosition(),
                        mContext,fragmentActivity,positionPlanData,users);
            }else {
                holder.daypartname.setBackgroundResource(R.drawable.orange_border);
                holder.daypartname.setTextColor(ContextCompat.getColor(mContext,R.color.orange));
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        holder.daypartname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (PositionPlanData data : arraylist){
                    data.setSelected(false);
                }
                positionPlanData.setSelected(true);
                notifyDataSetChanged();
            }
        });


        setDinningRoom();

        setDROpen_Close();

    }

    private int getTotalCount(PositionPlanData positionPlanData){

        int counter = 0;

        final SimpleDateFormat dayformat = new SimpleDateFormat("EEE MM/dd/yy");
        dayformat.setTimeZone(TimeZone.getTimeZone("UTC"));

        final SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
        timeFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));


        Calendar start = DaypartTimeToDate(positionPlanData.getDaypart().start, positionPlanData.getCurretDate());
        Calendar end = DaypartTimeToDate(positionPlanData.getDaypart().end, positionPlanData.getCurretDate());

        end.add(Calendar.MINUTE,-1);
        if (positionPlanData.getDaypart().name.equalsIgnoreCase("6")) {
            end.add(Calendar.DATE, 1);
        }

        for (CustomUser user : users) {
            for (Schedule_Business_Site_Plan sched : user.getSchedules()) {

                if (dayformat.format(sched.date).equalsIgnoreCase(dayformat.format(positionPlanData.getCurretDate()))){

                    Calendar startTime = DaypartTimeToDate(timeFormatter.format(sched.startTime), positionPlanData.getCurretDate());
                    Calendar endTime = DaypartTimeToDate(timeFormatter.format(sched.endTime), positionPlanData.getCurretDate());

                    if (SCMTool.isBetween(startTime.getTime(),endTime.getTime(),start.getTime())){
                        counter++;

                    }else if (SCMTool.isBetween(startTime.getTime(),endTime.getTime(),end.getTime())){
                        counter++;
                    }
                }
            }
        }

        return counter;
    }


    @Override
    public int getItemCount() {
       return arraylist.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView daypartname ;
        TextView required_position ;


        public MyViewHolder(View itemView) {
            super(itemView);
            daypartname = itemView.findViewById(R.id.daypart_name);
            required_position = itemView.findViewById(R.id.position_required);
        }
    }

    private void DisplaySelectedDaypart(Dayparts daypart, int required_position, Date start, Date end){
        SimpleDateFormat timeformat = new SimpleDateFormat("hh:mm a");
        timeformat.setTimeZone(TimeZone.getTimeZone("UTC"));
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("DP");
        stringBuilder.append(daypart.name);
        stringBuilder.append(": ");
        stringBuilder.append(timeformat.format(start.getTime()));
        stringBuilder.append(" to ");
        stringBuilder.append(timeformat.format(end.getTime()));
        Dashboard_PositionPlan_fragment.daypart_name.setText(stringBuilder.toString());
        Dashboard_PositionPlan_fragment.required_position.setText(String.valueOf(required_position));

        if (daypart.name.equalsIgnoreCase("1")){
            DisableSegmentedButtons(false);
        }else {
            DisableSegmentedButtons(true);
        }
    }
    public static void DisableSegmentedButtons(boolean disableSegmented){
        if (disableSegmented){
            Dashboard_PositionPlan_fragment.infogroup.setVisibility(View.GONE);
            Dashboard_PositionPlan_fragment.daypart_name.setVisibility(View.VISIBLE);
        }else {
            Dashboard_PositionPlan_fragment.infogroup.setVisibility(View.VISIBLE);
            Dashboard_PositionPlan_fragment.daypart_name.setVisibility(View.GONE);
        }
    }

    public static void setupSwitchButton(boolean isChecked){
        if (isChecked){
            Dashboard_PositionPlan_fragment.switch_button_indicator_open.setVisibility(View.VISIBLE);
            Dashboard_PositionPlan_fragment.switch_button_indicator_closed.setVisibility(View.GONE);

        }else {
            Dashboard_PositionPlan_fragment.switch_button_indicator_open.setVisibility(View.GONE);
            Dashboard_PositionPlan_fragment.switch_button_indicator_closed.setVisibility(View.VISIBLE);

        }
    }

    private void setDinningRoom(){
        Dashboard_PositionPlan_fragment.dinningnRoom.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                setupSwitchButton(isChecked);
                notifyDataSetChanged();
            }
        });
    }

    private void setDROpen_Close(){
        Dashboard_PositionPlan_fragment.infogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                notifyDataSetChanged();
            }
        });
    }
}
