package com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.StorePrep;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.HoursOfOperation.HoursOfOpetationData;
import com.procuro.apimmdatamanagerlib.HoursOfOperation;
import com.procuro.apimmdatamanagerlib.StorePrepSchedule;
import com.procuro.apimmdatamanagerlib.StorePrepData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class StorePrepActivity extends AppCompatActivity {


    ListView listView;
    Button back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_prep);

        listView = findViewById(R.id.listView);
        back = findViewById(R.id.home);

        setupOnclicks();

        getData();
    }


    private void setupOnclicks(){
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }



    private void getData(){
        HoursOfOpetationData data = HoursOfOpetationData.getInstance();
        HoursOfOperation operation = data.getHoursOfOperation();

        ArrayList<CustomStorePrepData>customStorePrepData = new ArrayList<>();
        if (operation.storePrepSchedule!=null){

            if (operation.storePrepSchedule.breakfast!=null) {
                customStorePrepData.add(new CustomStorePrepData("Breakfast", operation.storePrepSchedule.breakfast));

                if (operation.storePrepSchedule.lunch != null) {
                    customStorePrepData.add(new CustomStorePrepData("Lunch", operation.storePrepSchedule.lunch));

                    customStorePrepData.add(new CustomStorePrepData("Total", CreateTotalStorePrepData(operation.storePrepSchedule.breakfast,operation.storePrepSchedule.lunch)));
                }
            }
        }else {
            try {
                operation.readFromJSONObject(CreateOfflineStorePrepData());

                System.out.println("storePrepSchedule : "+CreateOfflineStorePrepData().toString());

                if (operation.storePrepSchedule.breakfast!=null) {
                    customStorePrepData.add(new CustomStorePrepData("Breakfast", operation.storePrepSchedule.breakfast));

                    if (operation.storePrepSchedule.lunch != null) {
                        customStorePrepData.add(new CustomStorePrepData("Lunch", operation.storePrepSchedule.lunch));

                        customStorePrepData.add(new CustomStorePrepData("Total", CreateTotalStorePrepData(operation.storePrepSchedule.breakfast,operation.storePrepSchedule.lunch)));
                    }else {
                        System.out.println("storePrepSchedule NULL LUNCH");
                    }
                }else {
                    System.out.println("storePrepSchedule NULL BREAKFAST");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

        StorePrep_Listview_adapter adapter = new StorePrep_Listview_adapter(this,customStorePrepData);
        listView.setAdapter(adapter);

    }

    private JSONObject CreateOfflineStorePrepData() throws JSONException {
        String storePrepScheduleString = "{\"storePrepSchedule\":{\"lunch\":[{\"totalEmployees\":0,\"time\":\"05:30 AM\",\"positions\":null},{\"totalEmployees\":0,\"time\":\"06:00 AM\",\"positions\":null},{\"totalEmployees\":0,\"time\":\"06:30 AM\",\"positions\":null},{\"totalEmployees\":0,\"time\":\"07:00 AM\",\"positions\":null},{\"totalEmployees\":0,\"time\":\"08:00 AM\",\"positions\":null},{\"totalEmployees\":0,\"time\":\"09:00 AM\",\"positions\":null},{\"totalEmployees\":0,\"time\":\"10:00 AM\",\"positions\":null},{\"totalEmployees\":0,\"time\":\"10:30 AM\",\"positions\":null}],\"breakfast\":[{\"totalEmployees\":0,\"time\":\"05:30 AM\",\"positions\":null},{\"totalEmployees\":0,\"time\":\"06:00 AM\",\"positions\":null},{\"totalEmployees\":0,\"time\":\"06:30 AM\",\"positions\":null},{\"totalEmployees\":0,\"time\":\"07:00 AM\",\"positions\":null},{\"totalEmployees\":0,\"time\":\"08:00 AM\",\"positions\":null},{\"totalEmployees\":0,\"time\":\"09:00 AM\",\"positions\":null},{\"totalEmployees\":0,\"time\":\"10:00 AM\",\"positions\":null},{\"totalEmployees\":0,\"time\":\"10:30 AM\",\"positions\":null}]}}";
        return new JSONObject(storePrepScheduleString);
    }


    private ArrayList<StorePrepData> CreateTotalStorePrepData(ArrayList<StorePrepData>breakfast , ArrayList<StorePrepData>lunch){
        ArrayList<StorePrepData>total = new ArrayList<>();
        for (StorePrepData bf : breakfast){
            for (StorePrepData lu :lunch){
                if (bf.time.equalsIgnoreCase(lu.time)){
                    StorePrepData newdata = new StorePrepData();
                    newdata.time= bf.time;
                    newdata.totalEmployees = (bf.totalEmployees + lu.totalEmployees);
                    total.add(newdata);
                }
            }
        }
        return total;
    }

}
