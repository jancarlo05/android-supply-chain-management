package com.procuro.androidscm.Restaurant.DashBoard.KitchenMetric;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.apimmdatamanagerlib.PimmInstance;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class DashboardKitchenMetricActivity extends AppCompatActivity {

    Button back;
    public  static TextView cooler,freezer;
    ListView cooler_listview,freezer_listview;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_kitchen_metric);

        back = findViewById(R.id.home);
        cooler = findViewById(R.id.cooler);
        freezer = findViewById(R.id.freezer);
        cooler_listview = findViewById(R.id.coolers_listview);
        freezer_listview = findViewById(R.id.freezer_listview);

        setupOnclicks();

        setupData();
    }

    private void setupData(){
        HashMap<String,Object> coolersHashmap =  getTemperatureBYName("Walk-in Cooler","Cooler");
        HashMap<String,Object> feelersHashmap =  getTemperatureBYName("Walk-in Freezer","Freezer");

        PimmInstance cooler = (PimmInstance) coolersHashmap.get("instance");
        PimmInstance freezer = (PimmInstance) feelersHashmap.get("instance");

        ArrayList<Integer> coolerSeverites = (ArrayList<Integer>) coolersHashmap.get("severities");
        ArrayList<Integer> freezerSeverites = (ArrayList<Integer>) feelersHashmap.get("severities");

        ArrayList<PimmInstance> coolers =(ArrayList<PimmInstance>) coolersHashmap.get("instances");
        ArrayList<PimmInstance> freezers = (ArrayList<PimmInstance>) feelersHashmap.get("instances");
        if (cooler!=null){
            if (coolerSeverites.size()>0){
                setContainerSeverity(cooler_listview,coolerSeverites.get(coolerSeverites.size()-1));
            }
        }
        if (freezer!=null){
            if (freezerSeverites.size()>0){
                setContainerSeverity(freezer_listview,freezerSeverites.get(freezerSeverites.size()-1));
            }
        }

        DisplayListView(coolers,true);
        DisplayListView(freezers,false);
    }


    private void DisplayListView(ArrayList arrayList,boolean isCooler){
        if (arrayList!=null){
            Dashboard_kitchen_metrics_listviewAdapter adapter = new Dashboard_kitchen_metrics_listviewAdapter(this,arrayList);
            if (isCooler){
                cooler_listview.setAdapter(adapter);
            }else {
                freezer_listview.setAdapter(adapter);
            }
        }
    }

    private void setupOnclicks(){
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    private void setContainerSeverity(View value, int severity){
        try {
            if (severity == 0 ){
                value.setBackgroundResource(R.drawable.rounded_white_bg_blue_border);
            }
            else if (severity == 3 ){
                value.setBackgroundResource(R.drawable.rounded_white_bg_green_border);
            }
            else if (severity == 5 ){
                value.setBackgroundResource(R.drawable.rounded_white_bg_yellow_border);
            }
            else if (severity == 9 ){
                value.setBackgroundResource(R.drawable.rounded_white_bg_red_border);
            }
            else if (severity == 10 ){
                value.setBackgroundResource(R.drawable.rounded_white_bg_black_border);
            }
            else  {
                value.setBackgroundResource(R.drawable.rounded_white_bg_black_border);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private HashMap<String,Object> getTemperatureBYName(String name, String group) {
        HashMap<String,Object> hashMap = new HashMap<>();
        try {
            ArrayList<Integer> severities = new ArrayList<>();
            CustomSite site = SCMDataManager.getInstance().getSelectedSite();
            ArrayList<PimmInstance> instances = new ArrayList<>();
            ArrayList<PimmInstance> filtedInstances = new ArrayList<>();
            if (site.getPimmInstances()!=null){
                instances.addAll(site.getPimmInstances());
            }

            for (PimmInstance pimmInstance : instances){
                if (pimmInstance.description.equalsIgnoreCase(name)){
                    hashMap.put("instance",pimmInstance);
                }
                if (pimmInstance.description.toLowerCase().contains(group.toLowerCase())){
                    if (pimmInstance.description.contains("Battery Voltage") ||
                            pimmInstance.description.contains("Signal Strength")){
                    }else {
                        severities.add(pimmInstance.severity);
                        filtedInstances.add(pimmInstance);
                    }
                }
            }
            Collections.sort(severities);
            System.out.println( group + severities);
            Collections.sort(filtedInstances, new Comparator<PimmInstance>() {
                @Override
                public int compare(PimmInstance o1, PimmInstance o2) {
                    return o1.description.compareTo(o2.description);
                }
            });
            hashMap.put("severities",severities);
            hashMap.put("instances",filtedInstances);
        }catch (Exception e){
            e.printStackTrace();
        }

        return hashMap;
    }




}
