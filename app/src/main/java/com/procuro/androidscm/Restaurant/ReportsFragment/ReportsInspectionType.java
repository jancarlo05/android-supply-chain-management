package com.procuro.androidscm.Restaurant.ReportsFragment;

import com.procuro.androidscm.Restaurant.Schema.Days;

import java.util.ArrayList;
import java.util.Date;

public class ReportsInspectionType {
    private Date date;
    private ArrayList<Days>foodsafety;
    private ArrayList<Days>checklist;
    public ReportsInspectionType() {

    }

    public ReportsInspectionType(Date date, ArrayList<Days> foodsafety, ArrayList<Days> checklist) {
        this.date = date;
        this.foodsafety = foodsafety;
        this.checklist = checklist;
    }


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ArrayList<Days> getFoodsafety() {
        return foodsafety;
    }

    public void setFoodsafety(ArrayList<Days> foodsafety) {
        this.foodsafety = foodsafety;
    }

    public ArrayList<Days> getChecklist() {
        return checklist;
    }

    public void setChecklist(ArrayList<Days> checklist) {
        this.checklist = checklist;
    }
}
