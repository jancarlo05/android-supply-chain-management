package com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.FormsList;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.procuro.androidscm.GlideApp;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.Qualification.QualificationActivity;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Regional;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.EquipmenList.EquipmenlistData;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.HoursOfOperation.HoursOfOpetationData;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.StoreProfileOptions.StoreProfileOptionsData;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsParentList;
import com.procuro.androidscm.Restaurant.Schema.Days;
import com.procuro.androidscm.Restaurant.Schema.Forms;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.StoreListFragment.SiteList;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsConfiguration;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsEquipment;
import com.suke.widget.SwitchButton;

import java.util.ArrayList;

public class Formlist_Expandable_adapter extends BaseExpandableListAdapter implements ExpandableListAdapter {

    private Context context;
    private ArrayList<Days> days;
    private Button save;
    private boolean ifUserHasAccessToEdit = SCMTool.isUserHasAccessToEdit();

    public Formlist_Expandable_adapter(Context context, ArrayList<Days> days, Button save) {
        this.days = days;
        this.context = context;
        this.save = save;
    }

    @Override
    public int getGroupCount() {
        return days.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return days.get(groupPosition).getForms().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return days.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return days.get(groupPosition).getForms().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        Days day = days.get(groupPosition);

        View view = LayoutInflater.from(context).inflate(R.layout.status_journal_parent_data, parent, false);
        TextView textView = view.findViewById(R.id.rowParentText);


        if (!HoursOfOpetationData.getInstance().isBreakfastEnabled()){
            if (day.section.equalsIgnoreCase("Breakfast Food Safety Checks")){
                return SCMTool.getNullView(context);

            }  else if (day.section.equalsIgnoreCase("Breakfast Readiness Checklist")){
                return SCMTool.getNullView(context);
            }
            else if (day.section.equalsIgnoreCase("Lunch Transition Checklist")){
                return SCMTool.getNullView(context);
            }
        }


        try {
            textView.setText(day.section);
        }catch (Exception e){
            e.printStackTrace();
        }



        return view;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final Forms form = days.get(groupPosition).getForms().get(childPosition);
        Days day = days.get(groupPosition);

        View view = LayoutInflater.from(context).inflate(R.layout.equipment_list_data, parent, false);
        TextView name = view.findViewById(R.id.name);
        final com.suke.widget.SwitchButton switchButton = view.findViewById(R.id.switch_button);

        try {

            if (!HoursOfOpetationData.getInstance().isBreakfastEnabled()){
                if (day.section.equalsIgnoreCase("Breakfast Food Safety Checks")){
                    return SCMTool.getNullView(context);

                }  else if (day.section.equalsIgnoreCase("Breakfast Readiness Checklist")){
                    return SCMTool.getNullView(context);
                }
                else if (day.section.equalsIgnoreCase("Lunch Transition Checklist")){
                    return SCMTool.getNullView(context);
                }
            }

           if (form.title.contains("Critical Control Points")){
               name.setText("Critical Control Points");
               if (form.getConfig()!=null){
                   form.getConfig().getSiteSettingsConfiguration().name ="Config:autofill:ccp";
               }else {
                   String configname = "Config:autofill:ccp";
                   CustomSiteSettingsConfiguration config = new CustomSiteSettingsConfiguration(new SiteSettingsConfiguration(configname,"false"));
                   form.setConfig(config);
               }

           }else if (form.title.contains("Next Day")){
               name.setText(form.title);
               switchButton.setChecked(false);
               switchButton.setEnabled(false);
               switchButton.setAlpha(.5f);
           }else {
               name.setText(form.title);
           }

           if (form.getConfig()!=null){
               if (form.getConfig().getSiteSettingsConfiguration().value.equalsIgnoreCase("true")){
                   switchButton.setChecked(true);
               }else {
                   switchButton.setChecked(false);
               }
           }else {
               System.out.println("Form : "+form.title + " NULL ");
               switchButton.setChecked(false);
               setConfigifnull(form);
           }


       }catch (Exception e){
           e.printStackTrace();
       }

        switchButton.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                CustomSiteSettingsConfiguration config = form.getConfig();
                config.setUpdated(true);
                config.getSiteSettingsConfiguration().setValue(String.valueOf(isChecked));
                FormListData.getInstance().setUnsaveEdit(true);
                save.setAlpha(1);
                save.setEnabled(true);
            }
        });

        if (!ifUserHasAccessToEdit){
            SCMTool.DisableView(switchButton,.5f);
        }

        if (form.formName.equalsIgnoreCase("lto")){
            SCMTool.DisableView(view,.5f);
        }

        return view;
    }


    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }


//    private void setFormifCheck(final SiteSettingsConfiguration config , boolean isChecked,SwitchButton switchButton){
//
//        config.value = String.valueOf(isChecked);
//        final FormListData data = FormListData.getInstance();
//
//        if (data.getSiteSettingsConfigurationUpdates() == null){
//            data.setSiteSettingsConfigurationUpdates(new ArrayList<SiteSettingsConfiguration>());
//            data.getSiteSettingsConfigurationUpdates().add(config);
//
//        }else {
//            int counter = 0;
//            setSwithStatus(switchButton,false);
//            for (int i = 0; i <data.getSiteSettingsConfigurationUpdates().size() ; i++) {
//                counter++;
//                SiteSettingsConfiguration newConfig = data.getSiteSettingsConfigurationUpdates().get(i);
//
//                if (newConfig.name.equalsIgnoreCase(config.name)){
//                    newConfig.value = config.value;
//                }else {
//                    data.getSiteSettingsConfigurationUpdates().add(config);
//                }
//
//                if (counter == data.getSiteSettingsConfigurationUpdates().size()){
//                    setSwithStatus(switchButton,true);
//                }
//            }
//
//        }
//
//

//    }
//
    private void setConfigifnull(Forms forms){
        String configname = "Config:autofill:"+forms.formName;
        CustomSiteSettingsConfiguration config = new CustomSiteSettingsConfiguration(new SiteSettingsConfiguration(configname,"false"));
        forms.setConfig(config);

    }
//
//    private void setSwithStatus(final SwitchButton switchButton, final boolean enabled){
//        Handler handler = new Handler(Looper.getMainLooper());
//        handler.post(new Runnable() {
//            @Override
//            public void run() {
//                float alpha = 1;
//                if (!enabled){
//                    alpha = 0.5f;
//                }
//                switchButton.setAlpha(alpha);
//                switchButton.setEnabled(enabled);
//            }
//        });
//
//    }

}
