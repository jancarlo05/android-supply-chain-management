package com.procuro.androidscm.Restaurant;

import com.procuro.apimmdatamanagerlib.PimmDevice;
import com.procuro.apimmdatamanagerlib.PimmInstance;
import com.procuro.apimmdatamanagerlib.ScorecardDTO;
import com.procuro.apimmdatamanagerlib.Site;

import java.util.ArrayList;

public class CustomSite {
    private Site site;
    private PimmDevice pimmDevice;
    private ScorecardDTO scorecardDTO;
    private ArrayList<CustomSensor>customSensors;
    private ArrayList<PimmInstance>pimmInstances;
    private boolean selected;
    private boolean isMerged;


    public ArrayList<PimmInstance> getPimmInstances() {
        return pimmInstances;
    }

    public void setPimmInstances(ArrayList<PimmInstance> pimmInstances) {
        this.pimmInstances = pimmInstances;
    }

    public boolean isMerged() {
        return isMerged;
    }

    public void setMerged(boolean merged) {
        isMerged = merged;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public ArrayList<CustomSensor> getCustomSensors() {
        return customSensors;
    }

    public void setCustomSensors(ArrayList<CustomSensor> customSensors) {
        this.customSensors = customSensors;
    }

    public ScorecardDTO getScorecardDTO() {
        return scorecardDTO;
    }

    public void setScorecardDTO(ScorecardDTO scorecardDTO) {
        this.scorecardDTO = scorecardDTO;
    }

    public CustomSite(Site site) {
        this.site = site;
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    public PimmDevice getPimmDevice() {
        return pimmDevice;
    }

    public void setPimmDevice(PimmDevice pimmDevice) {
        this.pimmDevice = pimmDevice;
    }


}
