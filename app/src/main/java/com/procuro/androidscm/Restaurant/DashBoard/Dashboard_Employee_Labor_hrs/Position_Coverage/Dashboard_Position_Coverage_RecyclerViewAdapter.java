package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.Position_Coverage;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_SalePerformance.SalesPerformanceDaypartHeader;
import com.procuro.androidscm.Restaurant.Schema.Dayparts;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.HoursOfOperation;
import com.procuro.apimmdatamanagerlib.SMSPositioningEmployeeScheduleAssignmentPositions;
import com.procuro.apimmdatamanagerlib.Schedule_Business_Site_Plan;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.SiteSales;
import com.procuro.apimmdatamanagerlib.SiteSalesData;
import com.procuro.apimmdatamanagerlib.SiteSalesDaypart;
import com.procuro.apimmdatamanagerlib.SiteSalesForecast;
import com.procuro.apimmdatamanagerlib.StorePrepData;
import com.procuro.apimmdatamanagerlib.StorePrepSchedule;
import com.procuro.apimmdatamanagerlib.User;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.TimeZone;


public class Dashboard_Position_Coverage_RecyclerViewAdapter extends RecyclerView.Adapter<Dashboard_Position_Coverage_RecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<PositionCovered> arraylist;
    private ArrayList<SiteSales>siteSales;
    private ArrayList<CustomUser>users;
    private ArrayList<Date>dates;

    public Dashboard_Position_Coverage_RecyclerViewAdapter(Context context,
                                                           ArrayList<PositionCovered> arraylist,
                                                           ArrayList<SiteSales>siteSales,ArrayList<CustomUser>users,
                                                           ArrayList<Date>dates) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;
        this.siteSales = siteSales;
        this.users = users;
        this.dates = dates;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.position_covered_cardview,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final PositionCovered positionCovered = arraylist.get(position);

        Site site = SCMDataManager.getInstance().getSelectedSite().getSite();
        SimpleDateFormat format = new SimpleDateFormat("hh:mm a");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        Calendar start = SCMTool.StringTimeToDateNoOffset(positionCovered.getDaypart().start);
        Calendar end = SCMTool.StringTimeToDateNoOffset(positionCovered.getDaypart().end);

        holder.name.setText("DP"+positionCovered.getDaypart().name);
        holder.name.append(" : ");
        holder.name.append(format.format(start.getTime()));
        holder.name.append("-");
        holder.name.append(format.format(end.getTime()));


        Dashboard_Position_covered_item_RecyclerViewAdapter adapter = new Dashboard_Position_covered_item_RecyclerViewAdapter(
                mContext,positionCovered.getEmployeecount(),positionCovered.getRequired_position_count());
        holder.recyclerView.setLayoutManager(new GridLayoutManager(mContext,positionCovered.getEmployeecount().size()));
        holder.recyclerView.setAdapter(adapter);

        setupCollapseView(holder.child_continer,holder.indicator);


        setupChildView(positionCovered,holder.child_continer);

    }

    private void setupChildView(PositionCovered positionCovered,LinearLayout container){
        try {
            if (positionCovered.getDaypartHeaders()!=null){
                ArrayList<SalesPerformanceDaypartHeader>headers = positionCovered.getDaypartHeaders().get(0);
                for (int i = 0; i <headers.size()-1; i++) {
                    SalesPerformanceDaypartHeader header = headers.get(i);
                    CreateChildPosition(container,positionCovered.getDaypartHeaders(),header, positionCovered.getDaypart());
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void CreateChildPosition(LinearLayout container,ArrayList<ArrayList<SalesPerformanceDaypartHeader>>headers,
                                     SalesPerformanceDaypartHeader header,Dayparts selectedDaypart){
        try {
            View view = LayoutInflater.from(mContext).inflate(R.layout.dashboard_position_coverd_child_layout, null, false);
            TextView name = view.findViewById(R.id.name);
            RecyclerView recyclerView = view.findViewById(R.id.recyclerView);

            SimpleDateFormat format = new SimpleDateFormat("h:mm a");
            format.setTimeZone(TimeZone.getTimeZone("UTC"));

            final SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM/dd/yy");
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            name.setText(format.format(header.getStartDate()));
            name.append(" - ");
            name.append(format.format(header.getEndDate()));

            ArrayList<Integer>projectedHrs = new ArrayList<>();
            ArrayList<Integer>scheduledHrs = new ArrayList<>();

            for (Date date : dates){
                for (SiteSales sales : siteSales){
                    if (dateFormat.format(date).equalsIgnoreCase(dateFormat.format(sales.salesDate))){
                        for (ArrayList<SalesPerformanceDaypartHeader>daypartHeaders : headers){
                            for (SalesPerformanceDaypartHeader daypartHeader : daypartHeaders){
                                if (dateFormat.format(date).equalsIgnoreCase(dateFormat.format(daypartHeader.getStartDate()))){
                                    if (header.getName().equalsIgnoreCase(daypartHeader.getName())){
                                        projectedHrs.add(getProjectedHrPerDaypart(daypartHeader,sales,selectedDaypart,date));
                                        scheduledHrs.add(getScheduledHrs(daypartHeader,date,selectedDaypart));
                                    }
                                }
                            }
                        }
                    }
                }
            }

            Dashboard_Position_covered_item_RecyclerViewAdapter adapter = new Dashboard_Position_covered_item_RecyclerViewAdapter(
                    mContext,scheduledHrs,projectedHrs);
            recyclerView.setLayoutManager(new GridLayoutManager(mContext,7));
            recyclerView.setAdapter(adapter);

            container.addView(view);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private void setupCollapseView(final LinearLayout child_continer, final LinearLayout indicator){
        final boolean[] expanded = {false};
        indicator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (expanded[0]){
                    expanded[0] = false;
                    child_continer.setVisibility(View.GONE);
                    indicator.setRotation(0);
                }else {
                    expanded[0] = true;
                    child_continer.setVisibility(View.VISIBLE);
                    indicator.setRotation(180);

                }
            }
        });
        if (expanded[0]){
            child_continer.setVisibility(View.VISIBLE);
            indicator.setRotation(180);
        }else {
            child_continer.setVisibility(View.GONE);
            indicator.setRotation(0);
        }

    }


    private Integer getForcastPerDaypart(SiteSales siteSales,Dayparts selectedDaypart,SalesPerformanceDaypartHeader daypartHeader){
        double totalForecast = getForcast(siteSales);

        if (siteSales.data!=null){

            SiteSalesData data = siteSales.data;

            if (data.dayparts!=null){

                for (SiteSalesDaypart daypart : data.dayparts){

                    if (daypart.name.equalsIgnoreCase(selectedDaypart.name)){

                        BigDecimal percentage = new BigDecimal("0");

                        for (SiteSalesForecast forecast : daypart.forecast){

                            if (daypart.name.equalsIgnoreCase("1")|| daypart.name.equalsIgnoreCase("2")){

                                if (daypartHeader.getName().equalsIgnoreCase("10-11")){
                                    percentage = getDaypartTransitionPercentage(data.dayparts,data.isFromDefault,daypartHeader);
                                }else {
                                    if (data.isFromDefault){
                                        if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.startTime)){
                                            if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.endTime)){
                                                percentage = percentage.add(forecast.percentage);
                                            }
                                        }
                                    }else {
                                        if (isInsideDaypart(daypartHeader,forecast)){
                                            percentage = percentage.add(forecast.percentage);
                                        }
                                    }
                                }
                            }
                            else {
                                if (data.isFromDefault){
                                    if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.startTime)){
                                        if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.endTime)){
                                            percentage = percentage.add(forecast.percentage);
                                        }
                                    }
                                }else {
                                    if (isInsideDaypart(daypartHeader,forecast)){
                                        percentage = percentage.add(forecast.percentage);
                                    }
                                }
                            }
                        }

                        BigDecimal FORCASTRESULT = new BigDecimal(totalForecast).multiply(percentage);

                        return (int)Math.round(FORCASTRESULT.doubleValue());
                    }
                }
            }else {
                return 0;
            }
        }else {
            return 0;
        }
        return 0;
    }

    private Integer getScheduledHrs(SalesPerformanceDaypartHeader daypartHeader,Date date,Dayparts dayparts){

        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE hh:mm a");
        SimpleDateFormat format = new SimpleDateFormat("H:mm");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        final SimpleDateFormat dayformat = new SimpleDateFormat("EEE MMM/dd/yy");
        dayformat.setTimeZone(TimeZone.getTimeZone("UTC"));

        double total = 0;

        for (CustomUser user : users){


            long totalDiff = 0 ,totalHr  = 0, totalMinute =0;

            long total_hours = 0;
            long totalMinutes = 0;

            for (Schedule_Business_Site_Plan sched : user.getSchedules()){

                long  startDiff = 0;
                long  endDiff = 0 ;

                if (dayformat.format(sched.date).equalsIgnoreCase(dayformat.format(date))){
                    String stringName = format.format(sched.startTime);

                    Calendar daypartStart = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                    daypartStart.setTime(daypartHeader.getStartDate());

                    Calendar daypartEnd = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                    daypartEnd.setTime(daypartHeader.getEndDate());

                    Calendar schedStart = SCMTool.ISOTimeToDate(stringName,date);
                    Calendar schedEnd = SCMTool.ISOTimeToDate(format.format(sched.endTime),date);
                    schedEnd.add(Calendar.MINUTE,-1);

                    Calendar schedEndTime = SCMTool.ISOTimeToDate(format.format(sched.endTime),date);

                    if (SCMTool.isBetween(schedStart.getTime(),schedEnd.getTime(),daypartStart.getTime())
                            && SCMTool.isBetweenEqual(schedStart.getTime(),schedEndTime.getTime(),daypartEnd.getTime())){

                        if (schedEnd.getTime().getTime() - schedStart.getTime().getTime() >0){
                            startDiff += daypartStart.getTime().getTime() - schedStart.getTime().getTime();
                            endDiff += daypartEnd.getTime().getTime() - schedStart.getTime().getTime();
                        }


//                        System.out.println(" ALL "+user.getUser().getFullName() + " | "+dayparts.title + " "+
//                                dateFormat.format(schedStart.getTime())+" | "+dateFormat.format(schedEnd.getTime())+ " | "+dateFormat.format(daypartStart.getTime())+ " | "+dateFormat.format(daypartEnd.getTime()) +" | "+sched.position);
                    }

                    else if (SCMTool.isBetween(daypartStart.getTime(),daypartEnd.getTime(),schedStart.getTime())){
                        if (schedEnd.getTime().getTime() - schedStart.getTime().getTime() >0){
                            endDiff += schedStart.getTime().getTime() - daypartStart.getTime().getTime();
                        }
//                        System.out.println(" START "+user.getUser().getFullName() + " | "+dayparts.title + " "+
//                                dateFormat.format(schedStart.getTime())+" | "+dateFormat.format(schedEnd.getTime())+ " | "+dateFormat.format(daypartStart.getTime())+ " | "+dateFormat.format(daypartEnd.getTime())+" | "+sched.position);

                    }
                    else if (SCMTool.isBetween(daypartStart.getTime(),daypartEnd.getTime(),schedEnd.getTime())){
                        double difference = 0;
                        if (schedEnd.getTime().getTime() - schedStart.getTime().getTime() >0){
                            endDiff += daypartEnd.getTime().getTime() - schedEnd.getTime().getTime();

                        }
//                        System.out.println(" END "+user.getUser().getFullName() + " | "+dayparts.title + " "+
//                                dateFormat.format(schedStart.getTime())+" | "+dateFormat.format(schedEnd.getTime())+ " | "+dateFormat.format(daypartStart.getTime())+ " | "+dateFormat.format(daypartEnd.getTime())+" | "+sched.position + " | ");
                    }
                }

                totalDiff = endDiff - startDiff;
                int Minute = (int) totalDiff / (60 * 1000) % 60;
                totalHr = (int) (totalDiff / (60 * 60 * 1000));
                double finalminute = Minute/60.0;
                total += totalHr +finalminute;
            }

        }
        return (int)Math.round(total);
    }

    private int getCPH(double item) {

        if (item >= 0 && item <= 100) {
            return 4;
        } else if (item >= 100 && item <= 250) {
            return 5;
        } else if (item >= 250 && item <= 400) {
            return 6;
        } else if (item >= 400 && item <= 550) {
            return 7;
        } else if (item >= 550 && item <= 700) {
            return 8;
        } else if (item >= 700 && item <= 850) {
            return 9;
        } else if (item >= 850 && item <= 1000) {
            return 10;
        } else if (item >= 1000 && item <= 1150) {
            return 11;
        } else if (item >= 1150 && item <= 1300) {
            return 12;
        } else if (item >= 1300 && item <= 1450) {
            return 13;
        } else {
            return 14;
        }
    }

    private Integer getProjectedHrPerDaypart(SalesPerformanceDaypartHeader daypartHeader,SiteSales siteSales, Dayparts selectedDaypart,Date date){

        ArrayList<Double>projectedHrs = new ArrayList<>();
        try {
            int forecast = getForcastPerDaypart(siteSales,selectedDaypart,daypartHeader);
            HoursOfOperation hoursOfOperation = SCMDataManager.getInstance().getHoursOfOperation();
            if (hoursOfOperation!=null){
                StorePrepSchedule schedule = hoursOfOperation.storePrepSchedule;
                if (schedule!=null){
                    double total =0;
                    if (selectedDaypart.name.equalsIgnoreCase("1")){
                        if (schedule.breakfast!=null && schedule.breakfast.size()>0 && schedule.lunch!=null && schedule.lunch.size()>0){
                            ArrayList<StorePrepData>totalStorePrepData = CreateTotal(schedule.breakfast,schedule.lunch);
                            for (StorePrepData totalData : totalStorePrepData){
                                String time = totalData.time.replace(" AM","");
                                String[]hr  = daypartHeader.getStart().split(":");
                                if (time.contains(hr[0])){
                                    if (time.contains(":30")){
                                        total += totalData.totalEmployees;
                                    }else {
                                        total += totalData.totalEmployees;
                                    }
                                }
                            }
                        }
                    }
                    else if (selectedDaypart.name.equalsIgnoreCase("2")){
                        if (daypartHeader.getName().equalsIgnoreCase("10-11")){
                            if (schedule.breakfast!=null && schedule.breakfast.size()>0 && schedule.lunch!=null && schedule.lunch.size()>0){
                                ArrayList<StorePrepData>totalStorePrepData = CreateTotal(schedule.breakfast,schedule.lunch);
                                for (StorePrepData totalData : totalStorePrepData){
                                    String time = totalData.time.replace(" AM","");
                                    String[]hr  = daypartHeader.getStart().split(":");
                                    if (time.contains(hr[0])){
                                        if (time.contains(":30")){
                                            total += totalData.totalEmployees;
                                        }else {
                                            total += totalData.totalEmployees;
                                        }
                                    }
                                }
                            }
                        }else {
                            total = getCPH(forecast);
                        }
                    }else {
                        total = getCPH(forecast);
                    }
                    double result = 0;
                    if (total!=0){
                        if (daypartHeader.getStart().equalsIgnoreCase("6:00") || daypartHeader.getStart().equalsIgnoreCase("10:00")){
                            result = total/2;
                        }else {
                            result = total;
                        }
                    }
                    projectedHrs.add(result);
                }
            }
//            final SimpleDateFormat dateFormat = new SimpleDateFormat("EEE");
//            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            Collections.sort(projectedHrs);
//            System.out.println("PROJECTED : "+selectedDaypart.title + " : "+ dateFormat.format(date)+"| "+projectedHrs);
            if (projectedHrs.size()>0){
                double selectedNumber = projectedHrs.get(projectedHrs.size()-1);
                return (int)Math.round(selectedNumber);
            }else {
                return 4;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return 4;
    }

    private ArrayList<StorePrepData>CreateTotal(ArrayList<StorePrepData>breakfast,ArrayList<StorePrepData>lunch){
        ArrayList<StorePrepData>totaldata = new ArrayList<>();

        for (StorePrepData bfast : breakfast){
            for (StorePrepData lu : lunch){
                if (bfast.time.equalsIgnoreCase(lu.time)){
                    StorePrepData newData = new StorePrepData();
                    newData.time = bfast.time;
                    newData.totalEmployees = bfast.totalEmployees+lu.totalEmployees;
                    newData.positions = bfast.positions;
                    totaldata.add(newData);
                }
            }
        }

        return totaldata;
    }

    private boolean isInsideDaypart(SalesPerformanceDaypartHeader daypartHeader,SiteSalesForecast forecast){
        SimpleDateFormat format = new SimpleDateFormat("H:mm");
        String[] header = daypartHeader.getStart().split(":");
        String headerHr = header[0];
        String headerMinute = header[1];
        String header30Minutes = "30";

        String[] forecastName = format.format(forecast.startTime).split(":");
        String forecastHr = forecastName[0];
        String foreMinute = forecastName[1];

        if (headerHr.equalsIgnoreCase(forecastHr)){
            return foreMinute.equalsIgnoreCase(header30Minutes) || foreMinute.equalsIgnoreCase(headerMinute);
        }
        return false;
    }

    private BigDecimal getDaypartTransitionPercentage(ArrayList<SiteSalesDaypart>dayparts,boolean isFromDefault,SalesPerformanceDaypartHeader daypartHeader ){

        BigDecimal percentage = new BigDecimal("0");
        SimpleDateFormat format = new SimpleDateFormat("H:mm");
        for (SiteSalesDaypart daypart : dayparts){
            if (daypart.name.equalsIgnoreCase("1")){
                for (SiteSalesForecast forecast : daypart.forecast){
                    if (!isFromDefault){
                        if (format.format(forecast.startTime).equalsIgnoreCase("10:00")){
                            percentage = percentage.add(forecast.percentage);
                            break;
                        }
                    }else {
                        if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.startTime)){
                            if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.endTime)){
                                percentage = percentage.add(forecast.percentage);
                                break;
                            }
                        }
                    }

                }
            }else if (daypart.name.equalsIgnoreCase("2")){
                for (SiteSalesForecast forecast : daypart.forecast){
                    if (!isFromDefault){
                        if (format.format(forecast.startTime).equalsIgnoreCase("10:30")){
                            percentage = percentage.add(forecast.percentage);
                            break;
                        }
                    }else {
                        if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.startTime)){
                            if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.endTime)){
                                percentage = percentage.add(forecast.percentage);
                                break;
                            }
                        }
                    }
                }
            }
        }
        System.out.println("getDaypartTransitionPercentage : "+percentage);
        return percentage;
    }

    private double getForcast(SiteSales siteSales){
        try {
            if (siteSales.data!=null){
                SiteSalesData data = siteSales.data;
                double forcast =0;
                if (data.dss!=0) {
                    forcast = Double.parseDouble(String.valueOf(data.dss));
                }
                else if (data.projected!=0){
                    forcast = Double.parseDouble(String.valueOf(data.projected));
                }
                return forcast;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(hasStableIds);
    }

    @Override
    public int getItemCount() {
        return arraylist.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        RecyclerView recyclerView;
        CardView cardView;
        LinearLayout child_continer,indicator;

        public MyViewHolder(View itemView) {
            super(itemView);
             name = itemView.findViewById(R.id.name);
             recyclerView = itemView.findViewById(R.id.recyclerView);
             cardView = itemView.findViewById(R.id.cardview_id);
             child_continer = itemView.findViewById(R.id.lastview_container);
            indicator = itemView.findViewById(R.id.indicator);

        }
    }


}
