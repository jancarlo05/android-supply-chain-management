package com.procuro.androidscm.Restaurant.ReportsFragment.Delivery;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;


import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsDateChildList;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsDateGrandChildList;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsParentList;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.apimmdatamanagerlib.ApplicationReport;

import java.util.ArrayList;


public class Reports_delivery_list_adapter extends BaseExpandableListAdapter {

    final Context context ;
    ArrayList<ReportsParentList> parent_lists;
    FragmentActivity fragmentActivity;
    ExpandableListView listView;


    public Reports_delivery_list_adapter(Context context, ArrayList<ReportsParentList> data, FragmentActivity fragmentActivity, ExpandableListView listView) {
        this.context = context;
        this.parent_lists = new ArrayList<>();
        this.parent_lists = data;
        this.fragmentActivity = fragmentActivity;
        this.listView = listView;

    }
    @Override
    public void onGroupExpanded(int i) {

    }

    @Override
    public void onGroupCollapsed(int i){

    }

    @Override
    public boolean isEmpty() {
            return false;
    }


    @Override
    public int getGroupCount() {
        int size = 0;
        if (SCMDataManager.getInstance().getReportLevel().equalsIgnoreCase("site")){
            size = parent_lists.size();
        }else {
            size = 1;
        }
        return size;
    }

    @Override
    public int getChildrenCount(int i) {
        int size =0;

        if (i == 0){
            size = parent_lists.get(i).getApplicationReports().size();
        }else {
            size = parent_lists.get(i).getWeeklyArrayList().size();
        }
        return size;
    }

    @Override
    public Object getGroup(int i) {
        return parent_lists.get(i);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return parent_lists.get(groupPosition).getWeeklyArrayList().get(childPosition);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int position, boolean b, View contentView, ViewGroup parent) {

        ReportsParentList parent_data = parent_lists.get(position);
            contentView = LayoutInflater.from(context).inflate(R.layout.status_journal_parent_data, parent, false);
            TextView textView = contentView.findViewById(R.id.rowParentText);
            textView.setText(parent_data.getTitle());


            return contentView;
    }


    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean b, View contentView, final ViewGroup parent) {

        if (groupPosition ==0){
            final ApplicationReport applicationReport = parent_lists.get(groupPosition).getApplicationReports().get(childPosition);

            contentView= LayoutInflater.from(context).inflate(R.layout.reports_daily, parent, false);
            final TextView title = contentView.findViewById(R.id.title);
            title.setText(applicationReport.ReportName);
            contentView.setBackgroundResource(R.drawable.onpress_design);
            if (!applicationReport.UrlTemplate.equalsIgnoreCase("#")){
                contentView.setAlpha(1f);
                contentView.setEnabled(true);
            }else {
                contentView.setAlpha(.5f);
                contentView.setEnabled(false);
            }

            contentView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    notifyDataSetChanged();
                    listView.smoothScrollToPosition(groupPosition+1,100);

                }
            });

        }else {
            final ReportsDateChildList child_data = parent_lists.get(groupPosition).getWeeklyArrayList().get(childPosition);
            contentView = LayoutInflater.from(context).inflate(R.layout.reports_weekly, parent, false);
            final TextView textView = contentView.findViewById(R.id.title);
            final LinearLayout LastlevelContainer = contentView.findViewById(R.id.last_level_container);
            final ImageView indicator = contentView.findViewById(R.id.arrow_indicator);
            final ArrayList<View>last_view = new ArrayList<>();
            final ArrayList<ReportsDateGrandChildList>dailies = child_data.getDailyArrayList();
            textView.setText(child_data.getTitle());

            if (dailies.size()>0){
                for (int i = 0; i <dailies.size() ; i++) {
                    final ReportsDateGrandChildList daily = dailies.get(i);
                    View lastview = LayoutInflater.from(context).inflate(R.layout.reports_daily, parent, false);
                    final TextView title = lastview.findViewById(R.id.title);
                    title.setText(daily.getTitle());
                    lastview.setBackgroundResource(R.drawable.onpress_design);

                    lastview.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Toast.makeText(context, ""+title.getText().toString(), Toast.LENGTH_SHORT).show();
                        }
                    });
                    LastlevelContainer.addView(lastview);
                    last_view.add(lastview);
                }
            }
            if (child_data.isSelected()){
                LastlevelContainer.setVisibility(View.VISIBLE);
                indicator.setRotation(180f);
            }else {
                LastlevelContainer.setVisibility(View.GONE);
            }

            contentView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (child_data.isSelected()){
                        child_data.setSelected(false);
                    }else {
                        for (ReportsDateChildList childList:parent_lists.get(groupPosition).getWeeklyArrayList()){
                            childList.setSelected(false);
                        }
                        child_data.setSelected(true);
                    }
                    notifyDataSetChanged();
                    listView.smoothScrollToPosition(groupPosition+1,100);

                }
            });
        }
        return contentView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }


}