package com.procuro.androidscm.Restaurant.ReportsFragment.Ratings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import androidx.fragment.app.Fragment;

import com.diegocarloslima.fgelv.lib.FloatingGroupExpandableListView;
import com.diegocarloslima.fgelv.lib.WrapperExpandableListAdapter;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsParentList;
import com.procuro.androidscm.SCMDataManager;

import java.util.ArrayList;

public class Reports_ratings_fragment extends Fragment {

    public static WrapperExpandableListAdapter wrapperAdapter;
    public static ExpandableListView expandableListView;
    public static  Reports_ratings_list_adapter adapter;

    public Reports_ratings_fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.reports_left_pane_inspection, container, false);
        expandableListView =rootView.findViewById(R.id.expandible_listview);


        populate_ratings_list(SCMDataManager.getInstance().getReport_ratings());

        return rootView;
    }


    public void populate_ratings_list(ArrayList<ReportsParentList>ratings){

        adapter = new Reports_ratings_list_adapter(getContext(), ratings,getActivity(),expandableListView);
        expandableListView.setAdapter(adapter);
        for (int i = 0; i < adapter.getGroupCount(); i++) {
            expandableListView.expandGroup(i);
        }
        expandableListView.setDividerHeight(0);
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                return true; // This way the expander cannot be collapsed
            }
        });


    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}