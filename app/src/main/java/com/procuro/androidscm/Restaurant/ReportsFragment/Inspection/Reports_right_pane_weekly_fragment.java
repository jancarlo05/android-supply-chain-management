package com.procuro.androidscm.Restaurant.ReportsFragment.Inspection;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.Calendar.CalendarActivity;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.HoursOfOperation.HoursOfOpetationData;
import com.procuro.androidscm.Restaurant.QuickAccessMenu.QuickAccessMenu;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsDateChildList;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsDateGrandChildList;
import com.procuro.androidscm.Restaurant.ReportsFragment.Reports_fragment;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;

import java.util.Objects;

public class Reports_right_pane_weekly_fragment extends Fragment {


    public RadioButton breafast,foodTranspo,puw,managerObs,positionCleaning;
    info.hoang8f.android.segmented.SegmentedGroup infogroup;
    ReportsDateGrandChildList grandChildList;
    ReportsDateChildList childList;
    private Button back,quick_a_menu,Covid;
    private TextView name;

    public Reports_right_pane_weekly_fragment() {
        this.grandChildList = SCMDataManager.getInstance().getSelectedReportsDateGrandChild();
        this.childList = SCMDataManager.getInstance().getSeletedReportsDateChildList();
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.reports_right_pane_inspection_reports_weekly, container, false);

         back = rootView.findViewById(R.id.back_button);
        infogroup = rootView.findViewById(R.id.segmented2);
        breafast = rootView.findViewById(R.id.breakfast_summary);
        foodTranspo = rootView.findViewById(R.id.food_transportation);
        puw = rootView.findViewById(R.id.puw);
        managerObs = rootView.findViewById(R.id.managers_obs);
        positionCleaning = rootView.findViewById(R.id.position_cleaning);
        quick_a_menu = rootView.findViewById(R.id.quick_a_menu);
        name = rootView.findViewById(R.id.username);
        Covid = rootView.findViewById(R.id.covid);


        setupOnclicks();

        return rootView;
    }


    private void setupOnclicks(){
        infogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                try {

                    RadioButton button = radioGroup.findViewById(i);
                    name.setText(button.getText());
                    FindSelectedDay(i);

                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

        quick_a_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuickAccessMenu.DisplayQuickAMenu(getContext(),quick_a_menu,getActivity());
            }
        });


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                back();
            }
        });

        Covid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuickAccessMenu.DisplayCovidFragment(getActivity());
            }
        });

        CheckBrekfast();
    }

    private void CheckBrekfast (){
        if (SCMDataManager.getInstance().getHoursOfOperation()!=null){
            if (SCMDataManager.getInstance().getHoursOfOperation().isBreakfast){
                SCMTool.EnableView(breafast,1);
                breafast.setChecked(true);
            }else {
                SCMTool.DisableView(breafast,.5f);
                foodTranspo.setChecked(true);
            }
        }
    }

    private void FindSelectedDay(int id){
        if (positionCleaning.isChecked()){
            Positional_LoadWebview(grandChildList);
        }else {
            LoadWebview(id,grandChildList,childList);
        }
    }
    private void LoadWebview(int id , ReportsDateGrandChildList daily, ReportsDateChildList weekly) {
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.reports_weekly_fragment_container,
                new Reports_inspection_Load_Webview(id ,daily,weekly)).commit();
    }

    private void Positional_LoadWebview(ReportsDateGrandChildList daily) {
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.reports_weekly_fragment_container,
                new Reports_inspection_Positional_Load_Webview(daily)).commit();

    }

    private void back() {
        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_right,R.anim.slide_right_out)
                .replace(R.id.resturant_fragment_container,
                        new Reports_fragment()).commit();
    }

    private void DisplayCalendar() {
        Intent intent = new Intent(getActivity(), CalendarActivity.class);
        requireActivity().startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();
    }
}