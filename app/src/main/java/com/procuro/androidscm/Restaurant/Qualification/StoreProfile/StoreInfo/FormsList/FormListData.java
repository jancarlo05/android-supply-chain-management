package com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.FormsList;

import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsConfiguration;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsEquipment;

import java.util.ArrayList;

public class FormListData {

    private ArrayList<SiteSettingsConfiguration>siteSettingsConfigurations;
    private boolean UnsaveEdit;
    private ArrayList<SiteSettingsConfiguration>siteSettingsConfigurationUpdates;

    private static FormListData instance = new FormListData();

    public static FormListData getInstance() {
        if(instance == null) {
            instance = new FormListData();
        }
        return instance;
    }

    public boolean isUnsaveEdit() {
        return UnsaveEdit;
    }

    public void setUnsaveEdit(boolean unsaveEdit) {
        UnsaveEdit = unsaveEdit;
    }

    public ArrayList<SiteSettingsConfiguration> getSiteSettingsConfigurationUpdates() {
        return siteSettingsConfigurationUpdates;
    }

    public void setSiteSettingsConfigurationUpdates(ArrayList<SiteSettingsConfiguration> siteSettingsConfigurationUpdates) {
        this.siteSettingsConfigurationUpdates = siteSettingsConfigurationUpdates;
    }

    public ArrayList<SiteSettingsConfiguration> getSiteSettingsConfigurations() {
        return siteSettingsConfigurations;
    }

    public void setSiteSettingsConfigurations(ArrayList<SiteSettingsConfiguration> siteSettingsConfigurations) {
        this.siteSettingsConfigurations = siteSettingsConfigurations;
    }

    public static void setInstance(FormListData instance) {
        FormListData.instance = instance;
    }

    public SiteSettingsConfiguration getBlueToothProbe(){
        SiteSettingsConfiguration siteSettingsConfiguration = new SiteSettingsConfiguration("Config:bluetoothProbe","false");
        if (siteSettingsConfigurations!=null){
         for (SiteSettingsConfiguration config : siteSettingsConfigurations){
             if (config.name.equalsIgnoreCase("Config:bluetoothProbe")){
                 return config;
             }
         }
        }
        return  siteSettingsConfiguration;
    }
}
