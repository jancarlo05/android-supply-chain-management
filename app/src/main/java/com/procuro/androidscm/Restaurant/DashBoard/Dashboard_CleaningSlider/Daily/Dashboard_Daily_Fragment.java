package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_CleaningSlider.Daily;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import androidx.fragment.app.Fragment;

import com.diegocarloslima.fgelv.lib.FloatingGroupExpandableListView;
import com.diegocarloslima.fgelv.lib.WrapperExpandableListAdapter;
import com.procuro.androidscm.R;
import com.procuro.androidscm.SCMDataManager;

public class Dashboard_Daily_Fragment extends Fragment {


    private ExpandableListView expandableListView;

    public Dashboard_Daily_Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dashboard_cleaning_daily_fragment, container, false);

        try {

            expandableListView = view.findViewById(R.id.FloatingGroupExpandableListView);
            Dashboard_Cleaning_Daily_ListviewAdapter listviewAdapter = new Dashboard_Cleaning_Daily_ListviewAdapter(getContext(), SCMDataManager.getInstance().getCleaningGuideDailies(),expandableListView);
            WrapperExpandableListAdapter wrapper = new WrapperExpandableListAdapter(listviewAdapter);
            expandableListView.setAdapter(listviewAdapter);
            expandableListView.setDividerHeight(0);
            expandableListView.setGroupIndicator(null);

        }catch (Exception e){
            e.printStackTrace();
        }



        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

    }

}
