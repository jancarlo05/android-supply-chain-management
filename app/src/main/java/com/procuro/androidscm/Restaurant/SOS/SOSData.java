package com.procuro.androidscm.Restaurant.SOS;

import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.apimmdatamanagerlib.PropertyValue;
import com.procuro.apimmdatamanagerlib.Site;

import java.util.ArrayList;

public class SOSData {

    private static SOSData instance = new SOSData();

    private String fire_dept;
    private String police_dept;
    private String rescue_squad;
    private String hospital;
    private String franchise;
    private String district_manager;
    private String director_area_ops;
    private String loss_prevention;
    private String ems;
    private String acs;
    private String security_desk;

    private Site site;
    private ArrayList<PropertyValue> propertyValues;

    public static SOSData getInstance() {
        if(instance == null) {
            instance = new SOSData();
        }
        return instance;
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    public ArrayList<PropertyValue> getPropertyValues() {
        return propertyValues;
    }

    public void setPropertyValues(ArrayList<PropertyValue> propertyValues) {
        this.propertyValues = propertyValues;
    }

    public static void setInstance(SOSData instance) {
        SOSData.instance = instance;
    }

    public String getFire_dept() {
        return fire_dept;
    }

    public void setFire_dept(String fire_dept) {
        this.fire_dept = fire_dept;
    }

    public String getPolice_dept() {
        return police_dept;
    }

    public void setPolice_dept(String police_dept) {
        this.police_dept = police_dept;
    }

    public String getRescue_squad() {
        return rescue_squad;
    }

    public void setRescue_squad(String rescue_squad) {
        this.rescue_squad = rescue_squad;
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }

    public String getFranchise() {
        return franchise;
    }

    public void setFranchise(String franchise) {
        this.franchise = franchise;
    }

    public String getDistrict_manager() {
        return district_manager;
    }

    public void setDistrict_manager(String district_manager) {
        this.district_manager = district_manager;
    }

    public String getDirector_area_ops() {
        return director_area_ops;
    }

    public void setDirector_area_ops(String director_area_ops) {
        this.director_area_ops = director_area_ops;
    }

    public String getLoss_prevention() {
        return loss_prevention;
    }

    public void setLoss_prevention(String loss_prevention) {
        this.loss_prevention = loss_prevention;
    }

    public String getEms() {
        return ems;
    }

    public void setEms(String ems) {
        this.ems = ems;
    }

    public String getAcs() {
        return acs;
    }

    public void setAcs(String acs) {
        this.acs = acs;
    }

    public String getSecurity_desk() {
        return security_desk;
    }

    public void setSecurity_desk(String security_desk) {
        this.security_desk = security_desk;
    }
}
