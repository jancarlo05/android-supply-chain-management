package com.procuro.androidscm.Restaurant.DashBoard.DopHuddle;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.procuro.androidscm.GlideApp;
import com.procuro.androidscm.R;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.SMSDailyOpsPlanCustomerSatisfactionItem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;


public class Dashboard_dop_huddle_tasks_listviewAdapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<SMSDailyOpsPlanCustomerSatisfactionItem> arraylist;
    public FragmentActivity fragmentActivity;


    public Dashboard_dop_huddle_tasks_listviewAdapter(Context context, ArrayList<SMSDailyOpsPlanCustomerSatisfactionItem> arraylist) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;
    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return arraylist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.dashboard_dop_huddle_task_list_data, null);
            view.setTag(holder);

            SMSDailyOpsPlanCustomerSatisfactionItem item = arraylist.get(position);

            TextView title = view.findViewById(R.id.name);
            TextView task = view.findViewById(R.id.tasks);

            try {
                title.setText(item.position);
                task.setText(SCMTool.CheckString(item.task,"--"));
            }catch (Exception e){
                e.printStackTrace();
            }


            return view;
    }



}

