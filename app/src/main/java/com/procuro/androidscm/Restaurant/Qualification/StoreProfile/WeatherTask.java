package com.procuro.androidscm.Restaurant.Qualification.StoreProfile;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.solver.widgets.Rectangle;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.androdocs.httprequest.HttpRequest;
import com.procuro.androidscm.GlideApp;
import com.procuro.androidscm.R;
import com.procuro.apimmdatamanagerlib.Site;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import io.github.douglasjunior.androidSimpleTooltip.OverlayView;
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

import static com.google.android.gms.common.internal.service.Common.API;

public class WeatherTask extends AsyncTask<String, Void, String> {

    private static TextView weathers;
    private ImageView icon;
    private Context context;
    public static Site site;
    private String API = "8f87f9b66a84bc23b00a96fe775756fe";
    private TextView temperaturename;
    private LinearLayout weather_container;

    public WeatherTask(TextView weathers, ImageView icon , Context context, Site site,
                       TextView temperaturename, LinearLayout weather_container) {
        this.weathers = weathers;
        this.icon = icon;
        this.context = context;
        this.site = site;
        this.temperaturename = temperaturename;
        this.weather_container =weather_container;
    }

        @Override
        protected void onPreExecute() {
        super.onPreExecute();

        /* Showing the ProgressBar, Making the main design GONE */
        }

        protected String doInBackground(String... args) {
            return HttpRequest.excuteGet("https://api.openweathermap.org/data/2.5/weather?zip=" + site.Address.Postal + "&units=imperial&appid=" + API);
        }

        @Override
        protected void onPostExecute(final String result) {
        try {
            JSONObject jsonObj = null;
            jsonObj = new JSONObject(result);
            JSONObject main = jsonObj.getJSONObject("main");
            JSONObject sys = jsonObj.getJSONObject("sys");
            JSONObject wind = jsonObj.getJSONObject("wind");
            JSONObject weather = jsonObj.getJSONArray("weather").getJSONObject(0);
            Long updatedAt = jsonObj.getLong("dt");
            String updatedAtText = "Updated at: " + new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH).format(new Date(updatedAt * 1000));
            final String temp = main.getString("temp");
            String tempMin = "Min Temp: " + main.getString("temp_min") + "°C";
            String tempMax = "Max Temp: " + main.getString("temp_max") + "°C";
            String pressure = main.getString("pressure");
            String humidity = main.getString("humidity");
            Long sunrise = sys.getLong("sunrise");
            Long sunset = sys.getLong("sunset");
            String iconCode = weather.getString("icon");
            String windSpeed = wind.getString("speed");
            final String weatherDescription = weather.getString("description");
            String address = jsonObj.getString("name") + ", " + sys.getString("country");
        /* Populating extracted data into our views */


            System.out.println("WEATHER FORCAST: "+weatherDescription);
            System.out.println("TEMPERATURE FROM : "+tempMin +" TO " +tempMax);
            System.out.println("WIND: "+windSpeed);
            System.out.println("PRESSURE: "+pressure);
            System.out.println("HUMIDITY: "+humidity);

            System.out.println("MAIN OBJECT : "+main.toString());
            System.out.println("SYS OBJECT : "+sys.toString());
            System.out.println("WIND OBJECT : "+wind.toString());
            System.out.println("WEATHER OBJECT : "+weather.toString());
            System.out.println("RESULT : "+jsonObj.toString());

            try {

                double tempdouble = Double.parseDouble(temp);
                weathers.setText((int)tempdouble+"°");
                final String iconUrl = "http://openweathermap.org/img/w/" + iconCode + ".png";
                GlideApp.with(context).load(iconUrl).into(icon);
                temperaturename.setText(capitalizeString(weatherDescription));


                Handler handler = new Handler(Looper.getMainLooper());
                final JSONObject finalJsonObj = jsonObj;
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        weather_container.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                DisplayQuickAMenu(context,weather_container, finalJsonObj);

                            }
                        });
                    }
                });

            }catch (Exception e){
                e.printStackTrace();
            }




        } catch (JSONException e) {
        System.out.println("Error: "+e.getLocalizedMessage());
        System.out.println("Error: "+e.getMessage());
        }
    }

    public static String capitalizeString(String string) {
        char[] chars = string.toLowerCase().toCharArray();
        boolean found = false;
        for (int i = 0; i < chars.length; i++) {
            if (!found && Character.isLetter(chars[i])) {
                chars[i] = Character.toUpperCase(chars[i]);
                found = true;
            } else if (Character.isWhitespace(chars[i]) || chars[i]=='.' || chars[i]=='\'') { // You can add other chars here
                found = false;
            }
        }
        return String.valueOf(chars);
    }

    public static void DisplayQuickAMenu(final Context context, View anchor,JSONObject jsonObj) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.TOP)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(true)
                .showArrow(true)
                .arrowColor(ContextCompat.getColor(context, R.color.white))
                .transparentOverlay(false)
                .highlightShape(OverlayView.HIGHLIGHT_SHAPE_RECTANGULAR)
                .overlayOffset(5f)
                .contentView(R.layout.qualification_weather_forecast)
                .focusable(true)
                .build();
        tooltip.show();

        ImageView icon = tooltip.findViewById(R.id.icon);
        TextView temperature = tooltip.findViewById(R.id.temperature);
        TextView description = tooltip.findViewById(R.id.description);
        TextView siteaddress = tooltip.findViewById(R.id.address);
        TextView info = tooltip.findViewById(R.id.info);

        try {
            JSONObject main = jsonObj.getJSONObject("main");
            JSONObject sys = jsonObj.getJSONObject("sys");
            JSONObject wind = jsonObj.getJSONObject("wind");
            JSONObject clouds = jsonObj.getJSONObject("clouds");
            JSONObject weather = jsonObj.getJSONArray("weather").getJSONObject(0);
            Long updatedAt = jsonObj.getLong("dt");
            String updatedAtText = "Updated at: " + new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH).format(new Date(updatedAt * 1000));
            final String temp = main.getString("temp") + "°F";
            String tempMin =  main.getString("temp_min") ;
            String tempMax =  main.getString("temp_max") ;
            String pressure = main.getString("pressure");
            String humidity = main.getString("humidity");
            Long sunrise = sys.getLong("sunrise");
            Long sunset = sys.getLong("sunset");
            String iconCode = weather.getString("icon");
            String windSpeed = wind.getString("speed");
            String cloud = clouds.getString("all");
            final String weatherDescription = weather.getString("description");
            String address = jsonObj.getString("name") + ", " + sys.getString("country");
            /* Populating extracted data into our views */
            final String iconUrl = "http://openweathermap.org/img/w/" + iconCode + ".png";

            StringBuilder SiteAddress = new StringBuilder();
            SiteAddress.append(site.Address.City);
            SiteAddress.append(", ");
            SiteAddress.append(site.Address.Province);

            DecimalFormat decimalFormat = new DecimalFormat("0.00");
            double doubleTempMin = Double.parseDouble(tempMin);
            double doubleTempMax = Double.parseDouble(tempMax);

            String stringTemperature = "Temperature from " +
                    decimalFormat.format(doubleTempMin) + "°F" +
                    " to " +
                    decimalFormat.format(doubleTempMax) + "°F." +
                    "\n" +
                    "Wind " + windSpeed + "0000m/s, " +
                    "Clouds " + cloud + "%, " +
                    "\n" +
                    pressure + " HPA";
            info.setText(stringTemperature);
            siteaddress.setText(SiteAddress.toString());
            temperature.setText(temp);
            description.setText(capitalizeString(weatherDescription));
            GlideApp.with(context).load(iconUrl).into(icon);


        }catch (Exception e){
            e.printStackTrace();
        }



    }
}