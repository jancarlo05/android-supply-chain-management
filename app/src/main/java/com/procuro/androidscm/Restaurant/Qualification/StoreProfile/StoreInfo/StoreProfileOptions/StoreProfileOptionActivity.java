package com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.StoreProfileOptions;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.procuro.androidscm.GlideApp;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.DashboardKitchenData;
import com.procuro.apimmdatamanagerlib.DocumentDTO;
import com.procuro.apimmdatamanagerlib.KitchenDataItems;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;

import java.util.ArrayList;

import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import org.json.JSONObject;

public class StoreProfileOptionActivity extends AppCompatActivity {

    aPimmDataManager dataManager = aPimmDataManager.getInstance();

    ImageView kitchen_layout,pimmynews;
    RelativeLayout kitchenLayoutContainer;
    ProgressDialog progressDialog;
    private Button back;
    private TextView name,templatecode;
    private ListView listView;
    private   StoreProfileOptions_Listview_adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_profile_option);

        kitchen_layout = findViewById(R.id.kitchen_layout);
        kitchenLayoutContainer = findViewById(R.id.kitchen_layout_container);
        back = findViewById(R.id.home);
        templatecode = findViewById(R.id.template_code);
        name = findViewById(R.id.name);
        listView = findViewById(R.id.listView);

        getInstance();

        setKitchenCode();

        setupOnclicks();


    }

    private void getInstance(){
        StoreProfileOptionsData data = StoreProfileOptionsData.getInstance();
        QualificationData qualificationData = QualificationData.getInstance();
        if (data.getKitchenData()==null){
            Download_DocumentListForReferenceId();
        }else {
            name.setText(SCMTool.CheckString(qualificationData.getSelectedSite().getSite().sitename,"---"));
            setUPKitchenLayout(data.getKitchenData());
        }

    }

    private void setupOnclicks(){
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void Download_DocumentListForReferenceId(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please Wait");
        progressDialog.setMessage("Downloading Data");
        progressDialog.show();

        QualificationData data = QualificationData.getInstance();
        name.setText(SCMTool.CheckString(data.getSelectedSite().getSite().sitename,"---"));

        dataManager.getDocumentListForReferenceId(data.getSelectedSite().getSite().siteid, new OnCompleteListeners.getDocumentListForReferenceIdCallbackListener() {
            @Override
            public void getDocumentListForReferenceIdCallback(ArrayList<DocumentDTO> documentDTOArrayList, Error error) {
                if (error == null){
                    for (DocumentDTO documentDTO : documentDTOArrayList){
                        if (documentDTO.category.equalsIgnoreCase("Employee_Position_Optimization_Data")){
                            System.out.println("Employee_Position_Optimization_Data : START");
                            Download_EMPLOYEE_POSITION_DATA(documentDTO.documentId,documentDTO.category);

                        }
                    }
                }
            }
        });
    }

    private void Download_EMPLOYEE_POSITION_DATA(String documentDtoID, String category){


        dataManager.getDocumentContentForDocumentId(documentDtoID, category, new OnCompleteListeners.getDocumentContentForDocumentIdCallbackListener() {
            @Override
            public void getDocumentContentForDocumentIdCallback(Object obj, Error error) {
                if (error ==  null) {
                    if (obj != null) {
                        JSONObject jsonObject = (JSONObject) obj;

                        DashboardKitchenData dashboardKitchenData = new DashboardKitchenData();
                        System.out.println("getDocumentContentForDocumentId");
                        System.out.println(jsonObject);
                        dashboardKitchenData.readFromJSONObject(jsonObject);
                        StoreProfileOptionsData.getInstance().setKitchenData(dashboardKitchenData);
                        GlideApp.with(StoreProfileOptionActivity.this).load(dashboardKitchenData.kitchenImageData).into(kitchen_layout);
                        setUPKitchenLayout(dashboardKitchenData);

                    }
                }
            }
        });
    }

    private void setUPKitchenLayout(DashboardKitchenData dashboardKitchenData){

        boolean isFGPopulated = false;
        double multipler = SCMTool.multiplier(dashboardKitchenData.kitchenWidth,this);
        templatecode.setText(SCMTool.CheckString(dashboardKitchenData.kitchenTemplateCode,"---"));

        kitchenLayoutContainer.getLayoutParams().width = SCMTool.ApplyMultiplier(dashboardKitchenData.kitchenWidth,multipler);
        kitchenLayoutContainer.getLayoutParams().height = SCMTool.ApplyMultiplier(dashboardKitchenData.kitchenHeight,multipler);

        GlideApp.with(StoreProfileOptionActivity.this).asDrawable()
                .load(dashboardKitchenData.kitchenImageData)
                .override(SCMTool.ApplyMultiplier(dashboardKitchenData.kitchenWidth,multipler),
                        SCMTool.ApplyMultiplier(dashboardKitchenData.kitchenHeight,multipler))
                .into(kitchen_layout);


        for (KitchenDataItems kitchenDataItems : dashboardKitchenData.kitchenDataItems){

            if (!kitchenDataItems.imageType.equalsIgnoreCase("FREEZER")){
                ImageView imageView = new ImageView(this);
                imageView.setId(View.generateViewId());
                RelativeLayout.LayoutParams newParams = new RelativeLayout.LayoutParams(
                        SCMTool.ApplyMultiplier(kitchenDataItems.width,multipler), SCMTool.ApplyMultiplier(kitchenDataItems.height,multipler));
                newParams.setMarginStart(SCMTool.ApplyMultiplier(kitchenDataItems.xcoordinate,multipler));
                newParams.topMargin = SCMTool.ApplyMultiplier(kitchenDataItems.ycoordinate,multipler);
                imageView.setLayoutParams(newParams);

                int image = SCMTool.getDashboardPositionImage(kitchenDataItems.imageType,dashboardKitchenData.kitchenTemplateCode,isFGPopulated);
                GlideApp.with(StoreProfileOptionActivity.this).load(image).into(imageView);
                kitchenLayoutContainer.addView(imageView);
            }else {

                LayoutInflater inflater = getLayoutInflater();
                View myLayout = inflater.inflate(R.layout.dasboard_freezer, null, false);
                RelativeLayout.LayoutParams newParams = new RelativeLayout.LayoutParams(
                        SCMTool.ApplyMultiplier(kitchenDataItems.width,multipler), SCMTool.ApplyMultiplier(kitchenDataItems.height,multipler));
                newParams.setMarginStart(SCMTool.ApplyMultiplier(kitchenDataItems.xcoordinate,multipler));
                newParams.topMargin = SCMTool.ApplyMultiplier(kitchenDataItems.ycoordinate,multipler);
                myLayout.setLayoutParams(newParams);
                kitchenLayoutContainer.addView(myLayout);
            }

            if (progressDialog!=null){
                progressDialog.dismiss();
            }
        }
    }

    private void setKitchenCode(){

        StoreProfileOptionsData data = StoreProfileOptionsData.getInstance();

        if (data.getKitchenCriteria() == null) {

            ArrayList<KitchenCriteria> criteria = new ArrayList<>();
            ArrayList<KitchenDescription> des1 = new ArrayList<>();
            des1.add(new KitchenDescription("MO-", "Modern"));
            des1.add(new KitchenDescription("IM-", "Image"));
            des1.add(new KitchenDescription("SS-", "Special Site"));
            criteria.add(new KitchenCriteria("Kitchen Type", des1));

            ArrayList<KitchenDescription> des2 = new ArrayList<>();
            des2.add(new KitchenDescription("STD-", "Standard"));
            des2.add(new KitchenDescription("REV-", "Reverse"));
            des2.add(new KitchenDescription("LSC-", "L-Shaped Counter"));
            des2.add(new KitchenDescription("SLC-", "Straight-Line Counter"));
            des2.add(new KitchenDescription("IAC-", "Image Activation Counter"));
            des2.add(new KitchenDescription("MG-", "Middle Grill"));
            des2.add(new KitchenDescription("SM-", "Small"));
            des2.add(new KitchenDescription("TLINE-", "T-Line Counter"));
            des2.add(new KitchenDescription("NGK-", "New Generation Kitchen"));
            criteria.add(new KitchenCriteria("Building Set-up Type", des2));

            ArrayList<KitchenDescription> des3 = new ArrayList<>();
            des3.add(new KitchenDescription("NA-", "NA"));
            des3.add(new KitchenDescription("CCW-", "Counter Clockwise"));
            des3.add(new KitchenDescription("CW-", "Clockwise"));
            criteria.add(new KitchenCriteria("Separation of Order and Pay", des3));


            ArrayList<KitchenDescription> des4 = new ArrayList<>();
            des4.add(new KitchenDescription("NL-", "No Lane"));
            des4.add(new KitchenDescription("1L-", "Single Lane"));
            des4.add(new KitchenDescription("YL-", "Y-Lane"));
            criteria.add(new KitchenCriteria("Drive-thru Type", des4));


            ArrayList<KitchenDescription> des5 = new ArrayList<>();
            des5.add(new KitchenDescription("1DSG-", "1 DSG"));
            des5.add(new KitchenDescription("2DSG-", "2 DSGs"));
            des5.add(new KitchenDescription("1FG-", "1 Flat Grill"));
            des5.add(new KitchenDescription("2FG-", "2 Flat Grills"));
            criteria.add(new KitchenCriteria("Grill Type", des5));

            ArrayList<KitchenDescription> des6 = new ArrayList<>();
            des6.add(new KitchenDescription("NK-", "No Kiosk"));
            des6.add(new KitchenDescription("CK-", "Counter"));
            des6.add(new KitchenDescription("FSK-", "Free Standing"));
            des6.add(new KitchenDescription("2FG-", "2 Flat Grills"));
            criteria.add(new KitchenCriteria("Kiosk Type", des6));

            ArrayList<KitchenDescription> des7 = new ArrayList<>();
            des7.add(new KitchenDescription("-3-", "3 Front Registers"));
            des7.add(new KitchenDescription("-2-", "2 Front Registers"));
            des7.add(new KitchenDescription("-1-", "1 Front Register"));
            criteria.add(new KitchenCriteria("Front Registers (Qty)", des7));

            ArrayList<KitchenDescription> des9 = new ArrayList<>();
            des9.add(new KitchenDescription("-0-0-", "No PUW"));
            des9.add(new KitchenDescription("-1-1-", "1 PUW 1 Register"));
            des9.add(new KitchenDescription("-1-2-", "1 PUW 2 Registers"));
            des9.add(new KitchenDescription("-2-2-", "2 PUW 2 Registers"));
            des9.add(new KitchenDescription("-2-3-", "2 PUW 3 Registers"));
            criteria.add(new KitchenCriteria("Pick-Up Windows (Qty)", des9));

            ArrayList<KitchenDescription> des10 = new ArrayList<>();
            des10.add(new KitchenDescription("NSL-", "No Sandwich Line"));
            des10.add(new KitchenDescription("1SL-", "1 Sandwich Line"));
            des10.add(new KitchenDescription("2SL-", "2 Sandwich Lines"));
            criteria.add(new KitchenCriteria("Sandwich Lines (Qty)", des10));

            ArrayList<KitchenDescription> des11 = new ArrayList<>();
            des11.add(new KitchenDescription("CSD-", "Crew Serve Drinks"));
            des11.add(new KitchenDescription("SSD-", "Self Serve Drinks"));
            criteria.add(new KitchenCriteria("Fountain Drinks Type", des11));

            ArrayList<KitchenDescription> des12 = new ArrayList<>();
            des12.add(new KitchenDescription("NSLD-", "Off"));
            des12.add(new KitchenDescription("SLD-", "On"));
            criteria.add(new KitchenCriteria("Salad Station", des12, true));

            ArrayList<KitchenDescription> des13 = new ArrayList<>();
            des13.add(new KitchenDescription("DT1-", "HME"));
            des13.add(new KitchenDescription("DT2-", "Delphi"));
            criteria.add(new KitchenCriteria("Drive Thru Vendor", des13));

            ArrayList<KitchenDescription> des14 = new ArrayList<>();
            des14.add(new KitchenDescription("BO1-", "DUMAC"));
            des14.add(new KitchenDescription("BO2-", "RTI"));
            des14.add(new KitchenDescription("BO3-", "NCR"));
            des14.add(new KitchenDescription("BO4-", "Wendy’s"));
            criteria.add(new KitchenCriteria("Back Office Vendor", des14));

            ArrayList<KitchenDescription> des15 = new ArrayList<>();
            des15.add(new KitchenDescription("NTS-", "Off"));
            des15.add(new KitchenDescription("TS-", "On"));
            criteria.add(new KitchenCriteria("Training Store", des15, true));

            ArrayList<KitchenDescription> des16 = new ArrayList<>();

            des16.add(new KitchenDescription("00:00", "12:00 AM"));
            des16.add(new KitchenDescription("12:00_13:00", "12:00 PM to 1:00 PM"));
            criteria.add(new KitchenCriteria("Peak Hour Setting", des16));

            ArrayList<KitchenDescription> des17 = new ArrayList<>();
            des17.add(new KitchenDescription("NDRS-", "Off"));
            des17.add(new KitchenDescription("DRS-", "On"));
            criteria.add(new KitchenCriteria("Dining Room Service Hours", des17));


            ArrayList<KitchenDescription> des18 = new ArrayList<>();
            des18.add(new KitchenDescription("NDRH-", "Off"));
            des18.add(new KitchenDescription("DRH-", "On"));
            criteria.add(new KitchenCriteria("Dining Room Host", des18, true));

            ArrayList<KitchenDescription> des19 = new ArrayList<>();
            des19.add(new KitchenDescription("N24H-", "Off"));
            des19.add(new KitchenDescription("24H-", "On"));
            criteria.add(new KitchenCriteria("Open 24-Hrs", des19, true));

            ArrayList<KitchenDescription> des20 = new ArrayList<>();
            des20.add(new KitchenDescription("NBF", "Off"));
            des20.add(new KitchenDescription("BF", "On"));
            criteria.add(new KitchenCriteria("Breakfast", des20, true));

            data.setKitchenCriteria(criteria);
            DisplayTemplateCode(criteria);
        }else {
            DisplayTemplateCode(data.getKitchenCriteria());
        }

    }

    private void DisplayTemplateCode(ArrayList<KitchenCriteria>criteria){
        try {
            adapter = new StoreProfileOptions_Listview_adapter(this,criteria);
            listView.setAdapter(adapter);

        }catch (Exception e){
            e.printStackTrace();
        }

    }


}
