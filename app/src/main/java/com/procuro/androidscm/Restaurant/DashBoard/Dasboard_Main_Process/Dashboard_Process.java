package com.procuro.androidscm.Restaurant.DashBoard.Dasboard_Main_Process;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.bumptech.glide.Glide;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.DashBoard.DashboardActivity;
import com.procuro.androidscm.Restaurant.DashBoard.DopHuddle.Dashboard_dop_huddle_skills_and_position_listviewAdapter;
import com.procuro.androidscm.Restaurant.DashBoard.KitchenMetric.DashboardKitchenMetricActivity;
import com.procuro.androidscm.Restaurant.Schema.Dayparts;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.DashboardKitchenData;
import com.procuro.apimmdatamanagerlib.KitchenDataItems;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.PimmInstance;
import com.procuro.apimmdatamanagerlib.PositionDataItems;
import com.procuro.apimmdatamanagerlib.SMSDailyOpsPlanCustomerSatisfactionItem;
import com.procuro.apimmdatamanagerlib.SMSDashboard;
import com.procuro.apimmdatamanagerlib.SMSPositioning;
import com.procuro.apimmdatamanagerlib.Schedule_Business_Site_Plan;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import static com.procuro.androidscm.Restaurant.DashBoard.DashboardActivity.accurcacy;
import static com.procuro.androidscm.Restaurant.DashBoard.DashboardActivity.cleanliness;
import static com.procuro.androidscm.Restaurant.DashBoard.DashboardActivity.friendliness;
import static com.procuro.androidscm.Restaurant.DashBoard.DashboardActivity.taste;
import static com.procuro.androidscm.SCMTool.DaypartTimeToDate;
import static com.procuro.androidscm.SCMTool.getStringCurrentDaypart;

public class Dashboard_Process {

    public static void DisplayTextClock(final TextView currentDate,TextView daypart){
        try {
            final Site site = SCMDataManager.getInstance().getSelectedSite().getSite();
            final SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy hh:mm a");
            format.setTimeZone(TimeZone.getTimeZone("UTC"));

            new CountDownTimer(1000, 2000) {
                @Override
                public void onTick(long millisUntilFinished) {
                }
                @Override
                public void onFinish() {
                    Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                    calendar.add(Calendar.MINUTE,site.effectiveUTCOffset);
                    currentDate.setText(format.format(calendar.getTime()));
                    start();
                }
            }.start();

            daypart.setText(SCMTool.CheckString(SCMTool.getCurrentDaypart(),"--"));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void DisplayDopHuddle(ArrayList<SMSDailyOpsPlanCustomerSatisfactionItem> items, Context context, FragmentActivity fragmentActivity){
        try {
            if (items!=null){
                SCMDataManager.getInstance().setDashboardDopHudlle(items);
                Dashboard_dop_huddle_skills_and_position_listviewAdapter listviewAdapter = new Dashboard_dop_huddle_skills_and_position_listviewAdapter(context,items,fragmentActivity);
                DashboardActivity.listView.setAdapter(listviewAdapter);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void DisplayScores(SMSPositioning smsPositioning,Context context){
        SMSDashboard scores = SCMDataManager.getInstance().getSmsDashboard();
        try {
            DashboardActivity.cee.setText(SCMTool.CheckString(smsPositioning.customerExperience_CEE,"--"));
            DashboardActivity.fee.setText(SCMTool.CheckString(smsPositioning.customerExperience_FEE,"--"));
            DashboardActivity.number_of_cars.setText(SCMTool.CheckString(smsPositioning.storeRecord_NoOfCars,"0"));
        }catch (Exception e){
            e.printStackTrace();
        }
        try {
            if (scores!=null){
                DashboardActivity.number_of_cars.setText(SCMTool.CheckString(scores.totalCount+"","0"));
                DashboardActivity.osat.setText(SCMTool.CheckString(scores.osat+"%","--"));
                DashboardActivity.zod.setText(SCMTool.CheckString(scores.zod+"%","--"));
                SCMTool.setGradeColors(DashboardActivity.taste,SCMTool.CheckString(scores.tasteScore+"%","--"),context);
                SCMTool.setGradeColors(DashboardActivity.friendliness,SCMTool.CheckString(scores.friendlinessScore+"%","--"),context);
                SCMTool.setGradeColors(DashboardActivity.speed,SCMTool.CheckString(scores.speedScore+"%","--"),context);
                SCMTool.setGradeColors(DashboardActivity.accurcacy,SCMTool.CheckString(scores.accuracyScore+"%","--"),context);
                SCMTool.setGradeColors(DashboardActivity.cleanliness,SCMTool.CheckString(scores.cleanlinessScore+"%","--"),context);
            }
        }catch (Exception e){
                e.printStackTrace();
        }

    }

    public static void setUPKitchenLayout(DashboardKitchenData dashboardKitchenData,Context context,FragmentActivity fragmentActivity){

        boolean isFGPopulated = false;
        double multipler = SCMTool.multiplier(dashboardKitchenData.kitchenWidth,context);

        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        Site site = SCMDataManager.getInstance().getSelectedSite().getSite();

        String currentDaypart = SCMTool.getStringCurrentDaypart();


        Calendar CurrentDate = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        CurrentDate = SCMTool.getDateWithDaypart(CurrentDate.getTime().getTime(),site.effectiveUTCOffset);

        int totalPosition = SCMTool.getSiteSettingsConfigurationForCurrentDay(getStringCurrentDaypart(),dateFormat.format(CurrentDate.getTime()));


        DashboardActivity.kitchenLayoutContainer.getLayoutParams().width = SCMTool.ApplyMultiplier(dashboardKitchenData.kitchenWidth,multipler);
        DashboardActivity.kitchenLayoutContainer.getLayoutParams().height = SCMTool.ApplyMultiplier(dashboardKitchenData.kitchenHeight,multipler);

        Glide.with(context)
                .load(dashboardKitchenData.kitchenImageData)
                .override(SCMTool.ApplyMultiplier(dashboardKitchenData.kitchenWidth,multipler),
                        SCMTool.ApplyMultiplier(dashboardKitchenData.kitchenHeight,multipler))
                .into( DashboardActivity.kitchen_layout);

        DisplayKitchenComponents(dashboardKitchenData,context,fragmentActivity);

        DisplayAdditionalKitchenComponent(dashboardKitchenData,context,fragmentActivity);

        ArrayList<PositionDataItems>dashboard_position_data = getpositionDataItems(dashboardKitchenData,currentDaypart);

        DisplayPositions(dashboardKitchenData,dashboard_position_data,context,totalPosition);

        DashboardActivity.positions_covered_value.setText(String.valueOf(getPositionCovered(CurrentDate.getTime())));

        DashboardActivity.total_positions.setText("OUT OF "+totalPosition);

    }

    public static int getPositionCovered(Date date){

        Dayparts daypart = SCMTool.getActualCurrentDaypartStart();
        Calendar start = SCMTool.DaypartTimeToDate(daypart.start, date);
        Calendar end = SCMTool.DaypartTimeToDate(daypart.end, date);

        if (daypart.name.equalsIgnoreCase("6")) {
            end.add(Calendar.DATE, 1);
        }
        int employee = 0;


        final SimpleDateFormat timeFormat = new SimpleDateFormat("EEE hh:mm a");


        final SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM/dd/yy");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        ArrayList<CustomUser>users = SCMTool.getActiveCustomUser(SCMDataManager.getInstance().getStoreUsers());
        ArrayList<Schedule_Business_Site_Plan>schedules = SCMDataManager.getInstance().getDailySchedule();
        if (schedules!=null){
            for (CustomUser user : users){
                for (Schedule_Business_Site_Plan sched : schedules){
                    if (user.getUser().userId.equalsIgnoreCase(sched.userID)){
                        if (dateFormat.format(sched.date).equalsIgnoreCase(dateFormat.format(date))){

                            SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
                            timeFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));

                            Calendar startTime = DaypartTimeToDate(timeFormatter.format(sched.startTime), date);
                            Calendar endTime = DaypartTimeToDate(timeFormatter.format(sched.endTime), date);

                            if (SCMTool.isBetween(startTime.getTime(),endTime.getTime(),start.getTime())){
                                System.out.println("START : "+user.getUser().getFullName() + " | START "+timeFormat.format(sched.startTime) +" | "+timeFormat.format(sched.endTime)+" | "+timeFormat.format(start.getTime())+" | "+timeFormat.format(end.getTime()));
                                employee++;
                            }else if (SCMTool.isBetween(startTime.getTime(),endTime.getTime(),end.getTime())){
                                System.out.println("START : "+user.getUser().getFullName() + " | END "+timeFormat.format(sched.startTime) +" | "+timeFormat.format(sched.endTime)+" | "+timeFormat.format(start.getTime())+" | "+timeFormat.format(end.getTime()));

                                employee++;
                            }
                        }
                    }
                }
            }
        }
        return employee;
    }

    public static void DisplayPositions(DashboardKitchenData dashboardKitchenData,ArrayList<PositionDataItems> positionDataItems,Context context,int totalPosition ){

        double multipler = SCMTool.multiplier(dashboardKitchenData.kitchenWidth,context);

        for (PositionDataItems dataItems: positionDataItems){
            if (totalPosition == dataItems.totalEmployees){

                TextView imageView = new TextView(context);
                imageView.setId(View.generateViewId());
                RelativeLayout.LayoutParams newParams = new RelativeLayout.LayoutParams(
                        SCMTool.ApplyMultiplier(dataItems.width,multipler),  SCMTool.ApplyMultiplier(dataItems.height,multipler));
                newParams.setMarginStart( SCMTool.ApplyMultiplier(dataItems.xcoordinate,multipler));
                newParams.topMargin =  SCMTool.ApplyMultiplier(dataItems.ycoordinate,multipler);
                imageView.setLayoutParams(newParams);

                imageView.setText(String.valueOf(dataItems.position));
                imageView.setAllCaps(false);
                imageView.setGravity(Gravity.CENTER);
                imageView.setTextSize(9);
                imageView.setTextColor(Color.BLACK);

                DashboardActivity.kitchenLayoutContainer.addView(SCMTool.CreateDashboardPositionBackground(imageView,"#"+dataItems.color));
            }
        }
    }


    public static ArrayList<PositionDataItems> getpositionDataItems(DashboardKitchenData dashboardKitchenData,String CurrentDaypart){
        ArrayList<PositionDataItems>items = new ArrayList<>();

        if (CurrentDaypart.toLowerCase().equalsIgnoreCase("1")){
            if (SCMTool.getDiningRoomStatusPerDaypart(CurrentDaypart)){
                items = dashboardKitchenData.breakfastPositionDataItems_DROpen;
            }else {
                items = dashboardKitchenData.breakfastPositionDataItems_DRClosed;
            }
        }else {
            if (SCMTool.getDiningRoomStatusPerDaypart(CurrentDaypart)){
                items = dashboardKitchenData.positionDataItems;
            }else {
                items = dashboardKitchenData.lateNightPositionDataItems;
            }
        }

        return items;
    }


    public static void DisplayAdditionalKitchenComponent(DashboardKitchenData dashboardKitchenData, Context context, final FragmentActivity fragmentActivity){
        String templatecode = dashboardKitchenData.kitchenTemplateCode;
        double multipler = SCMTool.multiplier(dashboardKitchenData.kitchenWidth,context);

        if (templatecode.toUpperCase().contains("MO")){
            if (templatecode.toUpperCase().contains("STD")){

                SMSPositioning positionform = SCMDataManager.getInstance().getPositionForm();

                LayoutInflater inflater = fragmentActivity.getLayoutInflater();
                View myLayout = inflater.inflate(R.layout.dashboard_speed_of_service, null, false);
                TextView record = myLayout.findViewById(R.id.record);
                if (positionform!=null){
                    record.setText(SCMTool.CheckString(positionform.storeRecord_NoOfCars,"--"));
                }

                RelativeLayout.LayoutParams newParams = new RelativeLayout.LayoutParams(
                        SCMTool.ApplyMultiplier(95, multipler), SCMTool.ApplyMultiplier(95, multipler));
                newParams.setMarginStart(SCMTool.ApplyMultiplier(575, multipler));
                newParams.topMargin = SCMTool.ApplyMultiplier(160, multipler);

                myLayout.setLayoutParams(newParams);
                DashboardActivity.kitchenLayoutContainer.addView(myLayout);

            }
        }

    }

    public static void DisplayKitchenComponents(DashboardKitchenData dashboardKitchenData, Context context, final FragmentActivity fragmentActivity){

        double multipler = SCMTool.multiplier(dashboardKitchenData.kitchenWidth,context);
        for (KitchenDataItems kitchenDataItems : dashboardKitchenData.kitchenDataItems) {
            if (!kitchenDataItems.imageType.equalsIgnoreCase("FREEZER")) {
                ImageView imageView = new ImageView(context);
                imageView.setId(View.generateViewId());
                RelativeLayout.LayoutParams newParams = new RelativeLayout.LayoutParams(
                        SCMTool.ApplyMultiplier(kitchenDataItems.width, multipler), SCMTool.ApplyMultiplier(kitchenDataItems.height, multipler));
                newParams.setMarginStart(SCMTool.ApplyMultiplier(kitchenDataItems.xcoordinate, multipler));
                newParams.topMargin = SCMTool.ApplyMultiplier(kitchenDataItems.ycoordinate, multipler);
                imageView.setLayoutParams(newParams);

                int image = SCMTool.getDashboardPositionImage(kitchenDataItems.imageType, dashboardKitchenData.kitchenTemplateCode, false);
                Glide.with(context).load(image).into(imageView);
                DashboardActivity.kitchenLayoutContainer.addView(imageView);


            } else {

                LayoutInflater inflater = fragmentActivity.getLayoutInflater();
                View myLayout = inflater.inflate(R.layout.dasboard_freezer, null, false);
                TextView coolers = myLayout.findViewById(R.id.coolers);
                TextView freezers = myLayout.findViewById(R.id.freezers);
                LinearLayout freezers_container = myLayout.findViewById(R.id.freezers_container);
                LinearLayout coolers_container = myLayout.findViewById(R.id.coolers_container);

                HashMap<String,Object> coolersHashmap =  getTemperatureBYName("Walk-in Cooler","Cooler");
                HashMap<String,Object> feelersHashmap =  getTemperatureBYName("Walk-in Freezer","Freezer");

                PimmInstance cooler = (PimmInstance) coolersHashmap.get("instance");
                PimmInstance freezer = (PimmInstance) feelersHashmap.get("instance");

                ArrayList<Integer> coolerSeverites = (ArrayList<Integer>) coolersHashmap.get("severities");
                ArrayList<Integer> freezerSeverites = (ArrayList<Integer>) feelersHashmap.get("severities");

                if (cooler!=null){
                    double roundedCoolers = Math.round(Double.parseDouble(cooler.staticValue) * 10) / 10.0;
                    coolers.setText(roundedCoolers+"ºF");
                    setTextSeverity(coolers,context,cooler);

                    if (coolerSeverites.size()>0){
                        setContainerSeverity(coolers_container,coolerSeverites.get(coolerSeverites.size()-1));
                    }
                }

                if (freezer!=null){
                    double roundedFreezers = Math.round(Double.parseDouble(freezer.staticValue) * 10) / 10.0;
                    freezers.setText(roundedFreezers+"ºF");
                    setTextSeverity(freezers,context,freezer);

                    if (freezerSeverites.size()>0){
                        setContainerSeverity(freezers_container,freezerSeverites.get(freezerSeverites.size()-1));
                    }
                }

                myLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(fragmentActivity, DashboardKitchenMetricActivity.class);
                        DisplayIntent(intent,fragmentActivity);
                    }
                });

                RelativeLayout.LayoutParams newParams = new RelativeLayout.LayoutParams(
                        SCMTool.ApplyMultiplier(kitchenDataItems.width, multipler), SCMTool.ApplyMultiplier(kitchenDataItems.height, multipler));
                newParams.setMarginStart(SCMTool.ApplyMultiplier(kitchenDataItems.xcoordinate, multipler));
                newParams.topMargin = SCMTool.ApplyMultiplier(kitchenDataItems.ycoordinate, multipler);
                myLayout.setLayoutParams(newParams);
                DashboardActivity.kitchenLayoutContainer.addView(myLayout);
            }
        }
    }

    public static void setTextSeverity(TextView value, Context context, PimmInstance temperature){
        try {
            if (temperature.severity == 0 ){
                value.setTextColor(ContextCompat.getColor(context, R.color.blue));
            }
            else if (temperature.severity == 3 ){
                value.setTextColor(ContextCompat.getColor(context, R.color.green));
            }
            else if (temperature.severity == 5 ){
                value.setTextColor(ContextCompat.getColor(context, R.color.yellow));
            }
            else if (temperature.severity == 9 ){
                value.setTextColor(ContextCompat.getColor(context, R.color.red));
            }
            else  {
                value.setTextColor(ContextCompat.getColor(context, R.color.gray));
                value.setText("--");
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public static void setContainerSeverity(LinearLayout value, int severity){
        try {
            if (severity == 0 ){
                value.setBackgroundResource(R.drawable.rounded_white_bg_blue_border);
            }
            else if (severity == 3 ){
                value.setBackgroundResource(R.drawable.rounded_white_bg_green_border);
            }
            else if (severity == 5 ){
                value.setBackgroundResource(R.drawable.rounded_white_bg_yellow_border);
            }
            else if (severity == 9 ){
                value.setBackgroundResource(R.drawable.rounded_white_bg_red_border);
            }
            else if (severity == 10 ){
                value.setBackgroundResource(R.drawable.rounded_white_bg_black_border);
            }
            else  {
                value.setBackgroundResource(R.drawable.rounded_white_bg_black_border);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static HashMap<String,Object>  getTemperatureBYName(String name,String group) {
        HashMap<String,Object> hashMap = new HashMap<>();
        try {
            ArrayList<Integer>severities = new ArrayList<>();
            CustomSite site = SCMDataManager.getInstance().getSelectedSite();
            ArrayList<PimmInstance> instances = new ArrayList<>();
            if (site.getPimmInstances()!=null){
                instances.addAll(site.getPimmInstances());
            }

            for (PimmInstance pimmInstance : instances){
                if (pimmInstance.description.equalsIgnoreCase(name)){
                    hashMap.put("instance",pimmInstance);
                }
                if (pimmInstance.description.toLowerCase().contains(group.toLowerCase())){
                    severities.add(pimmInstance.severity);
                }
            }
            Collections.sort(severities);
            hashMap.put("severities",severities);
        }catch (Exception e){
            e.printStackTrace();
        }

        return hashMap;
    }

    public static void DisplayIntent(Intent intent,FragmentActivity fragmentActivity){
        fragmentActivity.startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(fragmentActivity).toBundle());
    }

}
