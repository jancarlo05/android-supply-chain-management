package com.procuro.androidscm.Restaurant.DashBoard;

public class SkillsAndPositionDescription {

    String description;
    boolean selected;

    public SkillsAndPositionDescription(String description, boolean selected) {
        this.description = description;
        this.selected = selected;
    }

    public String getDescription() {
        return description;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
