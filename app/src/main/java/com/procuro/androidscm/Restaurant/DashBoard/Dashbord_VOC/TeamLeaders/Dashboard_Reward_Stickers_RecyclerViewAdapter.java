package com.procuro.androidscm.Restaurant.DashBoard.Dashbord_VOC.TeamLeaders;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.procuro.androidscm.GlideApp;
import com.procuro.androidscm.R;


public class Dashboard_Reward_Stickers_RecyclerViewAdapter extends RecyclerView.Adapter<Dashboard_Reward_Stickers_RecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private int size;

    public Dashboard_Reward_Stickers_RecyclerViewAdapter(Context context, int size) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.size = size;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.reward_skill_layout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

            GlideApp.with(mContext).asDrawable().load(R.drawable.sticker)
                    .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(holder.icon);



    }

    @Override
    public int getItemCount() {
        return size;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView icon;

        public MyViewHolder(View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.icon);
        }
    }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

}
