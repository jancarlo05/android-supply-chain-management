package com.procuro.androidscm.Restaurant.Status.StatusFragment.StoreListFragment;

import java.util.ArrayList;

public class StoreParentList {

    String name;
    ArrayList<StoreList>storeLists;

    public StoreParentList(String name, ArrayList<StoreList> storeLists) {
        this.name = name;
        this.storeLists = storeLists;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<StoreList> getStoreLists() {
        return storeLists;
    }

    public void setStoreLists(ArrayList<StoreList> storeLists) {
        this.storeLists = storeLists;
    }
}
