package com.procuro.androidscm.Restaurant.Status.StatusFragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.apimmdatamanagerlib.CorpStructure;

import java.util.ArrayList;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;


public class Status_Filter_Listview_Adapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<StatusFilter> list;
    private ExpandableListView expandableListView;
    private FragmentActivity fragmentActivity;
    private SimpleTooltip tooltip;
    private ArrayList<CustomSite>customSites;
    private ArrayList<CorpStructure>corpStructures;

    public Status_Filter_Listview_Adapter(Context context, ArrayList<StatusFilter> list, ExpandableListView expandableListView,
                                          FragmentActivity fragmentActivity, SimpleTooltip tooltip,
                                          ArrayList<CorpStructure>corpStructures,ArrayList<CustomSite>customSites) {
        this.mContext = context;
        this.inflater = LayoutInflater.from(mContext);
        this.list = list;
        this.expandableListView = expandableListView;
        this.fragmentActivity = fragmentActivity;
        this.tooltip = tooltip;
        this.customSites = customSites;
        this.corpStructures = corpStructures;


    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
            view = inflater.inflate(R.layout.qualification_filter_listview, null);
            final StatusFilter filter = list.get(position);

            TextView name = view.findViewById(R.id.name);
            ImageView icon = view.findViewById(R.id.selected);
            name.setText(filter.getName());


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (StatusFilter filters : list){
                        filters.setSelected(false);
                    }
                    filter.setSelected(true);

                    StatusFragment.DisplaySelectedPerspective(mContext,fragmentActivity);


                    tooltip.dismiss();

                }
            });

        if (filter.isSelected()){
            icon.setVisibility(View.VISIBLE);
        }else {
            icon.setVisibility(View.INVISIBLE);
        }

        return view;
    }

}

