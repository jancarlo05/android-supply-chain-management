package com.procuro.androidscm.Restaurant.DashBoard.KitchenMetric;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.Employee;
import com.procuro.apimmdatamanagerlib.PimmInstance;

import java.util.ArrayList;

import static com.procuro.androidscm.Restaurant.DashBoard.KitchenMetric.DashboardKitchenMetricActivity.cooler;
import static com.procuro.androidscm.Restaurant.DashBoard.KitchenMetric.DashboardKitchenMetricActivity.freezer;


public class Dashboard_kitchen_metrics_listviewAdapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<PimmInstance> arraylist;


    public Dashboard_kitchen_metrics_listviewAdapter(Context context, ArrayList<PimmInstance> arraylist) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;

    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return arraylist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.dashboard_kitchen_metric_list_data, null);
            view.setTag(holder);
            final PimmInstance instance = arraylist.get(position);
            final TextView name = view.findViewById(R.id.name);
            final TextView value = view.findViewById(R.id.value);

            name.setText(instance.description);

            if (instance.staticValue!=null){
                if (!instance.staticValue.contains("--")){
                    double roundedInstance = Math.round(Double.parseDouble(instance.staticValue) * 10) / 10.0;
                    value.setText(roundedInstance+"ºF");

                    if (instance.description.equalsIgnoreCase("WALK-IN COOLER")){
                        cooler.setText(roundedInstance+"ºF");
                        setTextSeverity(cooler,mContext,instance);
                    }
                    else  if (instance.description.equalsIgnoreCase("WALK-IN FREEZER")){
                        freezer.setText(roundedInstance+"ºF");
                        setTextSeverity(freezer,mContext,instance);
                    }

                    setTextSeverity(value,mContext,instance);
                }
            }


        return view;
    }

    private void setTextSeverity(TextView value, Context context, PimmInstance temperature){
        try {
            if (temperature.severity == 0 ){
                value.setTextColor(ContextCompat.getColor(context, R.color.blue));
            }
            else if (temperature.severity == 3 ){
                value.setTextColor(ContextCompat.getColor(context, R.color.green));
            }
            else if (temperature.severity == 5 ){
                value.setTextColor(ContextCompat.getColor(context, R.color.yellow));
            }
            else if (temperature.severity == 9 ){
                value.setTextColor(ContextCompat.getColor(context, R.color.red));
            }
            else  {
                value.setTextColor(ContextCompat.getColor(context, R.color.gray));
                value.setText("--");
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }


}

