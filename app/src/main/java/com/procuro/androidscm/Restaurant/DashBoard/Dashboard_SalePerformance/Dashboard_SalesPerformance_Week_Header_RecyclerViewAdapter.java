package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_SalePerformance;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.R;

import java.util.ArrayList;


public class Dashboard_SalesPerformance_Week_Header_RecyclerViewAdapter extends RecyclerView.Adapter<Dashboard_SalesPerformance_Week_Header_RecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<String> arrayList;

    public Dashboard_SalesPerformance_Week_Header_RecyclerViewAdapter(Context context, ArrayList<String> arraylist) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arrayList = arraylist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.dashboard_weekly_sched_total_list_data,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final String string = arrayList.get(position);
        try {
            holder.name.setText(string);
            holder.name.setTextColor(ContextCompat.getColor(mContext,R.color.white));
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        public MyViewHolder(View itemView) {
            super(itemView);
             name = itemView.findViewById(R.id.name);

        }
    }




}
