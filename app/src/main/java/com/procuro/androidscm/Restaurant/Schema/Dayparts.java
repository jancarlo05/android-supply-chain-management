package com.procuro.androidscm.Restaurant.Schema;

import com.procuro.apimmdatamanagerlib.PimmBaseObject;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;

public class Dayparts extends PimmBaseObject {

    public String name;
    public String title;
    public String start;
    public String end;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);


    }


}
