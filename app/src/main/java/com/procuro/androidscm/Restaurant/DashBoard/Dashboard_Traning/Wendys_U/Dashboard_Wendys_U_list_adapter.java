package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Traning.Wendys_U;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

import com.procuro.androidscm.GlideApp;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Managers_Confirmation_DialogFragment;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Traning.Dashboard_TraningData;
import com.procuro.androidscm.Restaurant.DashBoard.VideoPlayerActivity;
import com.procuro.apimmdatamanagerlib.TrainingDataItems;

import java.util.ArrayList;


public class Dashboard_Wendys_U_list_adapter extends BaseExpandableListAdapter {

    private final Context context ;
    private ArrayList<Dashboard_TraningData> traningData;
    private FragmentActivity fragmentActivity;

    public Dashboard_Wendys_U_list_adapter(Context context, ArrayList<Dashboard_TraningData> traningData , FragmentActivity fragmentActivity) {
        this.context = context;
        this.traningData = traningData;
        this.fragmentActivity = fragmentActivity;

    }

    @Override
    public int getGroupCount() {
        return traningData.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return traningData.get(i).getTrainingDataItems().size();
    }

    @Override
    public Object getGroup(int i) {
        return traningData.get(i);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return traningData.get(groupPosition).getTrainingDataItems().get(childPosition);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int position, boolean b, View contentView, ViewGroup parent) {

        Dashboard_TraningData data = traningData.get(position);
        contentView = LayoutInflater.from(context).inflate(R.layout.dashboard_training_category_level, parent,false);

        TextView name = contentView.findViewById(R.id.name);
        TextView childcount = contentView.findViewById(R.id.childcount);

        name.setText(data.getCategory());

        if (data.getTrainingDataItems()!=null){
            childcount.setText(String.valueOf(data.getTrainingDataItems().size()));
        }else {
            childcount.setText("0");
        }

        return contentView;
    }


    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean b, View contentView, final ViewGroup parent) {
        final TrainingDataItems item = traningData.get(groupPosition).getTrainingDataItems().get(childPosition);
        contentView = LayoutInflater.from(context).inflate(R.layout.dashboard_training_wendys_u_child_data, parent, false);
        TextView name = contentView.findViewById(R.id.title);
        ImageView icon = contentView.findViewById(R.id.icon);

        GlideApp.with(context).asDrawable().load(R.drawable.video_icon).into(icon);
        name.setText(item.title);

        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DashboardTraningWendysU_With_VideoPlayerActivity.webView.setVisibility(View.GONE);
                DashboardTraningWendysU_With_VideoPlayerActivity.progressBar.setVisibility(View.VISIBLE);
                DashboardTraningWendysU_With_VideoPlayerActivity.webView.loadUrl(item.videoURL);



//                DisplaySiteListPopup(item.videoURL,item.title);
//                Intent intent = new Intent(context, VideoPlayerActivity.class);
//                intent.putExtra("Url",item.videoURL);
//                intent.putExtra("name",item.title);
//                fragmentActivity.startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(fragmentActivity).toBundle());
            }
        });


        return contentView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }


    private void DisplaySiteListPopup(String url, String title){
        DialogFragment newFragment = Dashboard_Managers_Confirmation_DialogFragment.newInstance(url,title);
        assert fragmentActivity.getFragmentManager() != null;
        newFragment.show(fragmentActivity.getSupportFragmentManager(), "dialog");
    }


}