package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.DailyLabor;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_SalePerformance.Dashboard_SalesPerformance_Week_Child_item_RecyclerViewAdapter;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_SalePerformance.SalesPerformanceDaypartHeader;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.HoursOfOperation;
import com.procuro.apimmdatamanagerlib.Schedule_Business_Site_Plan;
import com.procuro.apimmdatamanagerlib.SiteSales;
import com.procuro.apimmdatamanagerlib.SiteSalesActual;
import com.procuro.apimmdatamanagerlib.SiteSalesData;
import com.procuro.apimmdatamanagerlib.SiteSalesDaypart;
import com.procuro.apimmdatamanagerlib.SiteSalesForecast;
import com.procuro.apimmdatamanagerlib.StorePrepData;
import com.procuro.apimmdatamanagerlib.StorePrepSchedule;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.TimeZone;


public class Dashboard_New_DailyLabor_Daypart_item_RecyclerViewAdapter extends RecyclerView.Adapter<Dashboard_New_DailyLabor_Daypart_item_RecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<String> items;
    private DailyLaborData dailyLaborData;
    private DecimalFormat format = new DecimalFormat("0.00");

    public Dashboard_New_DailyLabor_Daypart_item_RecyclerViewAdapter(Context context,
                                                                     ArrayList<String> items,DailyLaborData dailyLaborData) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.items = items;
        this.dailyLaborData = dailyLaborData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.daily_labor_cardview,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final String string = items.get(position);

        holder.name.setText(string);

        ArrayList<String>items = new ArrayList<>();

        if (string.equalsIgnoreCase("Projected Sales")){
            getForcastPerDaypart(items);
        }
        else if (string.equalsIgnoreCase("Actual Sales")){
            getSalesPerDaypart(items);
        }
        else if (string.equalsIgnoreCase("Projected Hrs")){
            getProjectedHrPerDaypart(items);
        }
        else if (string.equalsIgnoreCase("Scheduled Hrs")){
            getScheduledHrs(items);
        }
        else if (string.equalsIgnoreCase("Optimum Hrs")){
            getOptimumHrs(items);
        }
        else if (string.equalsIgnoreCase("Labor Hrs +/-")){
            getLaborHrs(items);
        }
        else if (string.equalsIgnoreCase("Cumulative +/-")){
            getCommulative(items);
        }

        Dashboard_DailyLabor_Child_item_RecyclerViewAdapter adapter = new Dashboard_DailyLabor_Child_item_RecyclerViewAdapter(mContext,items,string,dailyLaborData.getDaypartHeaders().size());
        holder.recyclerView.setLayoutManager(new GridLayoutManager(mContext,dailyLaborData.getDaypartHeaders().size()));
        holder.recyclerView.setAdapter(adapter);

        if (position % 2 == 1) {
            holder.cardView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.light_orange2));
        } else {
            holder.cardView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.white));
        }
    }

    private void getForcastPerDaypart(ArrayList<String>arrayList){
        ArrayList<Double>forecasts = dailyLaborData.getForcasts();
        for (double forcast : forecasts){
             if (forcast != -0.1){
                if (forcast<0){
                    forcast = forcast*(-1);
                    arrayList.add("-$"+format.format(forcast));
                }else {
                    arrayList.add("$"+format.format(forcast));
                }
            }else {
                arrayList.add("--");
            }
        }
    }

    private void getSalesPerDaypart(ArrayList<String>arrayList){
        ArrayList<Double>forecasts = dailyLaborData.getSales();
        for (double item : forecasts){
            if (item != -0.1){
                if (item<0){
                    item = item*(-1);
                    arrayList.add("-$"+format.format(item));
                }else {
                    arrayList.add("$"+format.format(item));
                }
            }else {
                arrayList.add("--");
            }
        }
    }

    private void getProjectedHrPerDaypart(ArrayList<String>arrayList){
        ArrayList<Double>items = dailyLaborData.getProjectedHrs();
        for (double item : items){
            if (item != -0.1){
                if (item<=0){
                    if (item ==0){
                        arrayList.add("0");
                    }else {
                        arrayList.add(format.format(item));
                    }
                }
                else {
                    arrayList.add("+"+format.format(item));
                }
            }else {
                arrayList.add("--");
            }
        }
    }

    private void getScheduledHrs(ArrayList<String>arrayList){
        ArrayList<Double>items = dailyLaborData.getScheduledHrs();
        for (double item : items){
            if (item != -0.1){
                if (item<=0){
                    if (item ==0){
                        arrayList.add("0");
                    }else {
                        arrayList.add(format.format(item));
                    }
                }
                else {
                    arrayList.add("+"+format.format(item));
                }
            }else {
                arrayList.add("--");
            }
        }
    }

    private void getOptimumHrs(ArrayList<String>arrayList){
        ArrayList<Double>items = dailyLaborData.getOptimumHrs();
        for (double item : items){
            if (item != -0.1){
                if (item<=0){
                    if (item ==0){
                        arrayList.add(format.format(item));
                    }else {
                        arrayList.add(format.format(item));
                    }
                }
                else {
                    arrayList.add("+"+format.format(item));
                }
            }else {
                arrayList.add("--");
            }
        }
    }

    private void getLaborHrs(ArrayList<String>arrayList){
        ArrayList<Double>items = dailyLaborData.getLaborHrs();
        for (double item : items){
            if (item != -0.1){
                if (item<=0){
                    if (item ==0){
                        arrayList.add(format.format(item));
                    }else {
                        arrayList.add(format.format(item));
                    }
                }
                else {
                    arrayList.add("+"+format.format(item));
                }
            }else {
                arrayList.add("--");
            }
        }
    }

    private void getCommulative(ArrayList<String>arrayList){
        ArrayList<Double>items = dailyLaborData.getCommulativeHrs();
        for (double item : items){
            if (item != -0.1){
                if (item<=0){
                    if (item ==0){
                        arrayList.add(format.format(item));
                    }else {
                        arrayList.add(format.format(item));
                    }
                }
                else {
                    arrayList.add("+"+format.format(item));
                }
            }else {
                arrayList.add("--");
            }
        }
    }



    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        RecyclerView recyclerView;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
             name = itemView.findViewById(R.id.name);
             recyclerView = itemView.findViewById(R.id.recyclerView);
             cardView = itemView.findViewById(R.id.cardview_id);

        }
    }


}
