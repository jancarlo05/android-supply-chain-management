package com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.EquipmenList;

import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsEquipment;

public class CustomEquipment {

    private SiteSettingsEquipment equipment;
    private boolean updated;

    public CustomEquipment(SiteSettingsEquipment equipment) {
        this.equipment = equipment;
        this.updated = updated;
    }

    public SiteSettingsEquipment getEquipment() {
        return equipment;
    }

    public void setEquipment(SiteSettingsEquipment equipment) {
        this.equipment = equipment;
    }

    public boolean isUpdated() {
        return updated;
    }

    public void setUpdated(boolean updated) {
        this.updated = updated;
    }
}
