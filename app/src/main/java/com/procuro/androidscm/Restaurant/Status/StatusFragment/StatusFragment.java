package com.procuro.androidscm.Restaurant.Status.StatusFragment;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Keep;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.procuro.androidscm.Logout_Confirmation_DialogFragment;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.DashboardActivity;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Area;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Chain;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_District;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Division;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Regional;
import com.procuro.androidscm.Restaurant.CustomSensor;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.Restaurant.QuickAccessMenu.QuickAccessMenu;
import com.procuro.androidscm.Restaurant.SOS.SOSActivity;
import com.procuro.androidscm.Restaurant.SOS.SOSData;
import com.procuro.androidscm.Restaurant.Status.StatusActivityFragment;
import com.procuro.androidscm.Restaurant.Status.StatusData;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.StatusSearhAdapter.Status_Allstore_SearchAdapter;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.StatusSearhAdapter.Status_OwnerShipView_SearchAdapter;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.StatusSearhAdapter.Status_StateByState_SearchAdapter;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.StoreListFragment.SiteList;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.apimmdatamanagerlib.CorpStructure;
import com.procuro.apimmdatamanagerlib.PimmInstance;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

@Keep
public class StatusFragment extends Fragment {

    public static ExpandableListView listView;
    private CustomSite site;
    public static TextView Wait, storename, titleheader, username,allstore,no_data_message;
    public static ProgressBar progressBar;
    private aPimmDataManager dataManager;
    private Context mContext;
    private Button logout, dropdown, Dashboard, profile, quick_a_menu, sos,Covid;
    ;
    private LinearLayout dropdownContainer;
    private ArrayList<CustomSensor> Sensors;
    private ArrayList<PimmInstance> instances;
    public static AutoCompleteTextView search;


    public StatusFragment() {
        this.site = SCMDataManager.getInstance().getSelectedSite();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.status_temperature_fragment, container, false);

        listView = view.findViewById(R.id.listView);
        dropdownContainer = view.findViewById(R.id.dropdown_container);
        storename = view.findViewById(R.id.storename);
        logout = view.findViewById(R.id.home);
        progressBar = view.findViewById(R.id.progressBar2);
        Wait = view.findViewById(R.id.wait);
        dropdown = view.findViewById(R.id.dropdown);
        username = view.findViewById(R.id.username);
        Dashboard = view.findViewById(R.id.dashboard);
        quick_a_menu = view.findViewById(R.id.quick_a_menu);
        sos = view.findViewById(R.id.sos);
        allstore = view.findViewById(R.id.all_store);
        Covid = view.findViewById(R.id.covid);
        no_data_message = view.findViewById(R.id.no_data_message);


        search = view.findViewById(R.id.search);
        search.setDropDownVerticalOffset(10);
        search.setDropDownBackgroundResource(R.drawable.rounded_white_bg);


        DisplaySelectedPerspective(getContext(), getActivity());
        username.setText(SCMDataManager.getInstance().getUsername());

        SetupOnclicks();

        DisplayAllstoreCount();

        return view;
    }


    private void SetupOnclicks() {

        Covid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuickAccessMenu.DisplayCovidFragment(getActivity());
            }
        });

        dropdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayFilterMenu(getContext(), dropdown, SCMDataManager.getInstance().getStatusFilters(), listView);
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DisplayLogoutConfirmation();
            }
        });

        Dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DashboardActivity.class);
                requireActivity().startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
            }
        });
        quick_a_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuickAccessMenu.DisplayQuickAMenu(getContext(), quick_a_menu, getActivity());
            }
        });


        sos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SOSData data = new SOSData();
                data.setSite(SCMDataManager.getInstance().getSelectedSite().getSite());
                SOSData.setInstance(data);

                Intent intent = new Intent(getActivity(), SOSActivity.class);
                requireActivity().startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
            }
        });

        allstore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StatusData data = new StatusData();
                data.setAllstore(true);
                StatusData.setInstance(data);
//                Intent intent = new Intent(getActivity(), StatusActivity.class);
//                getActivity().startActivity(intent);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.resturant_fragment_container,
                        new StatusActivityFragment()).commit();

            }
        });



    }

    private void DisplayAllstoreCount(){
        if (QualificationData.getInstance().getAllstore_count()==0){
            int counter = 0;
            for (CustomSite customSite : SCMDataManager.getInstance().getCustomSites()){
                if (customSite.isMerged()){
                    counter++;
                }
            }
            allstore.setText("All Stores | ");
            allstore.append(counter + " Stores");
        }else {
            allstore.setText("All Stores | ");
            allstore.append(QualificationData.getInstance().getAllstore_count() + " Stores");
        }

    }

    public static void DisplaySelectedPerspective(Context context, FragmentActivity fragmentActivity) {

        StatusData data = new StatusData();
        StatusData.setInstance(data);

        if (SCMDataManager.getInstance().getStatusFilters() != null) {

            for (StatusFilter filter : SCMDataManager.getInstance().getStatusFilters()) {

                if (filter.isSelected()) {
                    if (filter.getName().equalsIgnoreCase("Ownership View")) {
                        DisplayOwnerShip(SCMDataManager.getInstance().getCustomSites(), SCMDataManager.getInstance().getCorpStructures(), context, fragmentActivity);
                        progressBar.setVisibility(View.GONE);
                        Wait.setVisibility(View.GONE);


                    } else if (filter.getName().equalsIgnoreCase("State By State View")) {
                        DisplaySiteView(context, fragmentActivity);
                        progressBar.setVisibility(View.GONE);
                        Wait.setVisibility(View.GONE);

                    } else {
                        DisplayAllstore(context, fragmentActivity);
                        progressBar.setVisibility(View.GONE);
                        Wait.setVisibility(View.GONE);
                    }

                }
            }
        } else {
            ArrayList<StatusFilter> filters = new ArrayList<>();
            filters.add(new StatusFilter("All Stores", true));
            filters.add(new StatusFilter("Ownership View", false));
            filters.add(new StatusFilter("State By State View", false));
            SCMDataManager.getInstance().setStatusFilters(filters);
            DisplayAllstore(context, fragmentActivity);
            progressBar.setVisibility(View.GONE);
            Wait.setVisibility(View.GONE);
        }

    }

    public static void DisplayAllstore(Context context, FragmentActivity fragmentActivity) {

        ArrayList<CustomSite> siteList = SCMDataManager.getInstance().getCustomSites();
        ArrayList<String> basestringchain = new ArrayList<>();
        ArrayList<StatusChain> chains = new ArrayList<>();

        for (CustomSite site : siteList) {
            if (site.isMerged()) {
                if (site.getSite().Chain == null || site.getSite().Chain.equalsIgnoreCase("null")) {
                    if (!basestringchain.contains(site.getSite().CustomerName)) {
                        basestringchain.add(site.getSite().CustomerName);
                        ArrayList<CustomSite> sites = new ArrayList<>();
                        sites.add(site);
                        chains.add(new StatusChain(site.getSite().CustomerName, sites));
                    } else {
                        for (StatusChain chain : chains) {
                            if (chain.getName().equalsIgnoreCase(site.getSite().CustomerName)) {
                                chain.getCustomSites().add(site);
                            }
                        }
                    }
                } else {
                    if (!basestringchain.contains(site.getSite().Chain)) {
                        basestringchain.add(site.getSite().Chain);
                        ArrayList<CustomSite> sites = new ArrayList<>();
                        sites.add(site);
                        chains.add(new StatusChain(site.getSite().Chain, sites));
                    } else {
                        for (StatusChain chain : chains) {
                            if (chain.getName().equalsIgnoreCase(site.getSite().Chain)) {
                                chain.getCustomSites().add(site);
                            }
                        }
                    }
                }
            }
        }

        try {
            if (chains.size() > 1) {
                Collections.sort(chains.subList(1, chains.size()), new Comparator<StatusChain>() {
                    @Override
                    public int compare(StatusChain o1, StatusChain o2) {
                        return o1.getName().compareToIgnoreCase(o2.getName());
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (chains.size()>0){
            Status_All_Store_List_adapter adapter = new Status_All_Store_List_adapter(context, chains, fragmentActivity,false);
            listView.setAdapter(adapter);


            for (int i = 0; i < adapter.getGroupCount(); i++) {
                listView.expandGroup(i);
            }

            Status_Allstore_SearchAdapter searchadapter = new Status_Allstore_SearchAdapter(context, chains, search, listView, fragmentActivity);
            search.setAdapter(searchadapter);
            no_data_message.setVisibility(View.GONE);
        }




    }

    public static ArrayList<Corp_Regional> SiteView() {

        ArrayList<Corp_Chain> qualificationChains = OwnerShipView(SCMDataManager.getInstance().getCustomSites(), SCMDataManager.getInstance().getCorpStructures());

        ArrayList<Corp_Regional> qualificationRegionals = new ArrayList<>();

        for (Corp_Chain qualificationChain : qualificationChains) {
            qualificationRegionals.addAll(qualificationChain.getQualificationRegionals());
        }

        ArrayList<Corp_Regional> NewCorpRegionals = new ArrayList<>();

        ArrayList<String> regional_string = new ArrayList<>();
        for (Corp_Regional qualificationRegional : qualificationRegionals) {

            if (!regional_string.contains(qualificationRegional.getCorpStructures().name)) {
                regional_string.add(qualificationRegional.getCorpStructures().name);

                ArrayList<String> province = new ArrayList<>();
                ArrayList<SiteList> siteLists = new ArrayList<>();

                for (Corp_Division qualificationDivision : qualificationRegional.getQualificationDivisions()) {
                    for (Corp_Area qualificationArea : qualificationDivision.getQualificationAreas()) {
                        for (Corp_District qualificationDistrict : qualificationArea.getQualificationDistricts()) {
                            for (CustomSite site : qualificationDistrict.getSites()) {
                                if (!province.contains(site.getSite().Address.Province)) {
                                    province.add(site.getSite().Address.Province);
                                    ArrayList<CustomSite> sites = new ArrayList<>();
                                    sites.add(site);
                                    siteLists.add(new SiteList(site.getSite().Address.Province, sites));


                                } else {
                                    for (SiteList siteList1 : siteLists) {
                                        if (siteList1.getProvince().equalsIgnoreCase(site.getSite().Address.Province)) {
                                            siteList1.getCustomSites().add(site);

                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                qualificationRegional.setSiteLists(siteLists);
                NewCorpRegionals.add(qualificationRegional);

            } else {

                for (Corp_Regional corp_regional : NewCorpRegionals) {
                    if (corp_regional.getCorpStructures().name.equalsIgnoreCase(qualificationRegional.getCorpStructures().name)) {


                        ArrayList<String> province = new ArrayList<>();

                        for (SiteList siteList : corp_regional.getSiteLists()) {
                            province.add(siteList.getProvince());

                        }

                        for (Corp_Division qualificationDivision : qualificationRegional.getQualificationDivisions()) {
                            for (Corp_Area qualificationArea : qualificationDivision.getQualificationAreas()) {
                                for (Corp_District qualificationDistrict : qualificationArea.getQualificationDistricts()) {

                                    for (CustomSite site : qualificationDistrict.getSites()) {
                                        if (!province.contains(site.getSite().Address.Province)) {
                                            province.add(site.getSite().Address.Province);
                                            ArrayList<CustomSite> sites = new ArrayList<>();
                                            sites.add(site);
                                            corp_regional.getSiteLists().add(new SiteList(site.getSite().Address.Province, sites));

                                        } else {
                                            for (SiteList siteList1 : corp_regional.getSiteLists()) {
                                                if (siteList1.getProvince().equalsIgnoreCase(site.getSite().Address.Province)) {
                                                    siteList1.getCustomSites().add(site);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        return NewCorpRegionals;
    }

    public static void DisplaySiteView(Context context, FragmentActivity fragmentActivity) {
        ArrayList<Corp_Regional> chains = SiteView();


        if (chains.size()>0){
            boolean readytoFilter = false;
            for (Corp_Regional corp_regional : chains) {
                if (corp_regional.getSiteLists().size()>1){
                    readytoFilter = true;
                    Collections.sort(corp_regional.getSiteLists(), new Comparator<SiteList>() {
                        @Override
                        public int compare(SiteList o1, SiteList o2) {
                            return o1.getProvince().compareToIgnoreCase(o2.getProvince());
                        }
                    });
                }
            }
            if (readytoFilter){
                chains.get(0).getSiteLists().get(0).setSelected(true);
            }

            Status_State_by_State_View_List_adapter adapter = new Status_State_by_State_View_List_adapter(context, chains, fragmentActivity,false);
            listView.setAdapter(adapter);

            for (int i = 0; i < adapter.getGroupCount(); i++) {
                listView.expandGroup(i);
            }
            listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                    return true;
                }
            });

            Status_StateByState_SearchAdapter searchadapter = new Status_StateByState_SearchAdapter(context, chains, search, listView, fragmentActivity);
            search.setAdapter(searchadapter);
            no_data_message.setVisibility(View.GONE);
        }
    }

    public static ArrayList<Corp_Chain> OwnerShipView(ArrayList<CustomSite> siteList, ArrayList<CorpStructure> corpStructures) {

        ArrayList<Corp_Chain> basechains = new ArrayList<>();
        ArrayList<String> basestringchain = new ArrayList<>();

        if (siteList!=null&& corpStructures!=null){

            for (CustomSite site : siteList) {
                if (site.getSite().Chain == null || site.getSite().Chain.equalsIgnoreCase("null")) {
                    if (!basestringchain.contains(site.getSite().CustomerName)) {
                        basestringchain.add(site.getSite().CustomerName);
                        ArrayList<CustomSite> sites = new ArrayList<>();
                        Corp_Chain qualificationChain = new Corp_Chain();
                        qualificationChain.setName(site.getSite().CustomerName);
                        qualificationChain.setSites(sites);
                        sites.add(site);
                        basechains.add(qualificationChain);

                    } else {
                        for (Corp_Chain qualificationChain : basechains) {
                            if (qualificationChain.getName().equalsIgnoreCase(site.getSite().CustomerName)) {
                                qualificationChain.getSites().add(site);
                            }
                        }
                    }
                } else {
                    if (!basestringchain.contains(site.getSite().Chain)) {
                        basestringchain.add(site.getSite().Chain);
                        ArrayList<CustomSite> sites = new ArrayList<>();
                        Corp_Chain qualificationChain = new Corp_Chain();
                        qualificationChain.setName(site.getSite().Chain);
                        qualificationChain.setSites(sites);
                        sites.add(site);
                        basechains.add(qualificationChain);
                    } else {
                        for (Corp_Chain qualificationChain : basechains) {
                            if (qualificationChain.getName().equalsIgnoreCase(site.getSite().Chain)) {
                                qualificationChain.getSites().add(site);
                            }
                        }
                    }
                }
            }

            Collections.sort(basechains.subList(1, basechains.size()), new Comparator<Corp_Chain>() {
                @Override
                public int compare(Corp_Chain o1, Corp_Chain o2) {
                    return o1.getName().compareToIgnoreCase(o2.getName());
                }
            });

            populateDistrict(basechains, corpStructures);

            if (basechains.get(0).getQualificationRegionals().size() >= 1) {
                basechains.get(0).getQualificationRegionals().get(0).setSelected(true);
                if (basechains.get(0).getQualificationRegionals().get(0).getQualificationDivisions().size() >= 1) {
                    basechains.get(0).getQualificationRegionals().get(0).getQualificationDivisions().get(0).setSelected(true);

                    if (basechains.get(0).getQualificationRegionals().get(0).getQualificationDivisions().get(0).getQualificationAreas().size() >= 1) {
                        basechains.get(0).getQualificationRegionals().get(0).getQualificationDivisions().get(0).getQualificationAreas().get(0).setSelected(true);

                        if (basechains.get(0).getQualificationRegionals().get(0).getQualificationDivisions().get(0).getQualificationAreas().get(0).getQualificationDistricts().size() >= 2) {
                            basechains.get(0).getQualificationRegionals().get(0).getQualificationDivisions().get(0).getQualificationAreas().get(0).getQualificationDistricts().get(0).setSelected(true);

                        }
                    }
                }
            }


        }

        return basechains;
    }

    public static void populateDistrict(ArrayList<Corp_Chain> basechains, ArrayList<CorpStructure> corpStructures) {

        for (Corp_Chain qualificationChain : basechains) {

            ArrayList<Corp_District> qualificationDistricts = new ArrayList<>();
            ArrayList<String> districs = new ArrayList<>();

            for (CustomSite site : qualificationChain.getSites()) {

                if (site.getSite().CorpStructureID != null) {
                    if (!site.getSite().CorpStructureID.equalsIgnoreCase("null")) {

                        if (!districs.contains(site.getSite().CorpStructureID)) {
                            districs.add(site.getSite().CorpStructureID);
                            ArrayList<CustomSite> sites = new ArrayList<>();
                            sites.add(site);
                            for (CorpStructure corpStructure : corpStructures) {
                                if (corpStructure.corpStructureID.equalsIgnoreCase(site.getSite().CorpStructureID)) {
                                    qualificationDistricts.add(new Corp_District(corpStructure, sites, corpStructure.parentID));

                                }
                            }

                        } else {
                            for (Corp_District qualificationDistrict : qualificationDistricts) {
                                if (qualificationDistrict.getCorpStructures().corpStructureID.equalsIgnoreCase(site.getSite().CorpStructureID)) {
                                    qualificationDistrict.getSites().add(site);

                                }
                            }
                        }
                    }
                }
            }

            Collections.sort(qualificationDistricts, new Comparator<Corp_District>() {
                @Override
                public int compare(Corp_District o1, Corp_District o2) {
                    return o1.getCorpStructures().name.compareToIgnoreCase(o2.getCorpStructures().name);
                }
            });


            populateArea(qualificationChain, corpStructures, qualificationDistricts);


        }
    }

    public static void populateArea(Corp_Chain qualificationChain, ArrayList<CorpStructure> corpStructures, ArrayList<Corp_District> qualificationDistricts) {

        ArrayList<Corp_Area> qualificationAreas = new ArrayList<>();
        ArrayList<String> arealist = new ArrayList<>();

        for (Corp_District qualificationDistrict : qualificationDistricts) {
            for (CorpStructure corpStructure : corpStructures) {

                if (qualificationDistrict.getCorpStructures().parentID.equalsIgnoreCase(corpStructure.corpStructureID)) {
                    if (!arealist.contains(corpStructure.corpStructureID)) {
                        arealist.add(corpStructure.corpStructureID);
                        ArrayList<Corp_District> districts1 = new ArrayList<>();
                        districts1.add(qualificationDistrict);

                        if (corpStructure.corpStructureID.equalsIgnoreCase(qualificationDistrict.getCorpStructures().parentID)) {
                            qualificationAreas.add(new Corp_Area(corpStructure, districts1));
                        }

                    } else {
                        for (Corp_Area qualificationArea : qualificationAreas) {
                            if (qualificationArea.getCorpStructures().corpStructureID.equalsIgnoreCase(corpStructure.corpStructureID)) {
                                qualificationArea.getQualificationDistricts().add(qualificationDistrict);
                            }
                        }
                    }
                }
            }
        }

        Collections.sort(qualificationAreas, new Comparator<Corp_Area>() {
            @Override
            public int compare(Corp_Area o1, Corp_Area o2) {
                return o1.getCorpStructures().name.compareToIgnoreCase(o2.getCorpStructures().name);
            }
        });

        populateDision(qualificationChain, corpStructures, qualificationAreas);

    }

    public static void populateDision(Corp_Chain qualificationChain, ArrayList<CorpStructure> corpStructures, ArrayList<Corp_Area> qualificationAreas) {

        ArrayList<Corp_Division> qualificationDivisions = new ArrayList<>();
        ArrayList<String> divisionlist = new ArrayList<>();
        for (Corp_Area qualificationArea : qualificationAreas) {
            for (CorpStructure corpStructure : corpStructures) {

                if (qualificationArea.getCorpStructures().parentID.equalsIgnoreCase(corpStructure.corpStructureID)) {

                    if (!divisionlist.contains(corpStructure.corpStructureID)) {
                        divisionlist.add(corpStructure.corpStructureID);
                        ArrayList<Corp_Area> areas1 = new ArrayList<>();
                        areas1.add(qualificationArea);

                        if (corpStructure.corpStructureID.equalsIgnoreCase(qualificationArea.getCorpStructures().parentID)) {
                            qualificationDivisions.add(new Corp_Division(corpStructure, areas1));
                        }
                    } else {
                        for (Corp_Division qualificationDivision : qualificationDivisions) {
                            if (qualificationDivision.getCorpStructures().corpStructureID.equalsIgnoreCase(corpStructure.corpStructureID)) {
                                qualificationDivision.getQualificationAreas().add(qualificationArea);
                            }
                        }
                    }
                }
            }
        }

        Collections.sort(qualificationDivisions, new Comparator<Corp_Division>() {
            @Override
            public int compare(Corp_Division o1, Corp_Division o2) {
                return o1.getCorpStructures().name.compareToIgnoreCase(o2.getCorpStructures().name);
            }
        });


        populateRegion(qualificationChain, corpStructures, qualificationDivisions);
    }

    public static void populateRegion(Corp_Chain qualificationChain, ArrayList<CorpStructure> corpStructures, ArrayList<Corp_Division> qualificationDivisions) {

        ArrayList<Corp_Regional> qualificationRegionals = new ArrayList<>();
        ArrayList<String> stringregionals = new ArrayList<>();
        for (Corp_Division qualificationDivision : qualificationDivisions) {

            for (CorpStructure corpStructure : corpStructures) {

                if (qualificationDivision.getCorpStructures().parentID.equalsIgnoreCase(corpStructure.corpStructureID)) {

                    if (!stringregionals.contains(corpStructure.corpStructureID)) {
                        stringregionals.add(corpStructure.corpStructureID);
                        ArrayList<Corp_Division> divisions1 = new ArrayList<>();
                        divisions1.add(qualificationDivision);

                        qualificationRegionals.add(new Corp_Regional(corpStructure, divisions1));

                    } else {
                        for (Corp_Regional qualificationRegional : qualificationRegionals) {
                            if (qualificationRegional.getCorpStructures().corpStructureID.equalsIgnoreCase(corpStructure.corpStructureID)) {
                                qualificationRegional.getQualificationDivisions().add(qualificationDivision);
                            }
                        }
                    }
                }
            }
        }

        Collections.sort(qualificationRegionals, new Comparator<Corp_Regional>() {
            @Override
            public int compare(Corp_Regional o1, Corp_Regional o2) {
                return o1.getCorpStructures().name.compareToIgnoreCase(o2.getCorpStructures().name);
            }
        });


        qualificationChain.setQualificationRegionals(qualificationRegionals);

    }

    public static void DisplayOwnerShip(ArrayList<CustomSite> siteList, ArrayList<CorpStructure> corpStructures, Context context, FragmentActivity fragmentActivity) {

        ArrayList<Corp_Chain> chains = OwnerShipView(siteList, corpStructures);

        if (chains.size()>0){
            Status_OwnershipView_List_adapter adapters = new Status_OwnershipView_List_adapter(context, chains, fragmentActivity,false);
            listView.setAdapter(adapters);

            for (int i = 0; i < adapters.getGroupCount(); i++) {
                listView.expandGroup(i);
            }
            listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                    return true;
                }
            });

            no_data_message.setVisibility(View.GONE);
            Status_OwnerShipView_SearchAdapter searchadapter = new Status_OwnerShipView_SearchAdapter(context, chains, search, listView, fragmentActivity);
            search.setAdapter(searchadapter);

        }
    }

    public void DisplayFilterMenu(final Context context, View anchor, ArrayList<StatusFilter> filters,
                                  ExpandableListView expandableListView) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.BOTTOM)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .transparentOverlay(true)
                .arrowColor(ContextCompat.getColor(context, R.color.white))
                .contentView(R.layout.status_filter_popup)
                .focusable(true)
                .build();
        tooltip.show();

        ListView listView = tooltip.findViewById(R.id.listview);
        Status_Filter_Listview_Adapter adapter = new Status_Filter_Listview_Adapter(context, filters, expandableListView, getActivity(), tooltip,
                SCMDataManager.getInstance().getCorpStructures(), SCMDataManager.getInstance().getCustomSites());
        listView.setAdapter(adapter);

    }


    private void DisplayLogoutConfirmation() {
        DialogFragment newFragment = Logout_Confirmation_DialogFragment.newInstance();
        assert getFragmentManager() != null;
        newFragment.show(getActivity().getSupportFragmentManager(), "dialog");
    }





}


