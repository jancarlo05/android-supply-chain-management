package com.procuro.androidscm.Restaurant.DashBoard.Dashbord_VOC;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.Dashbord_VOC.TeamLeaders.Dashboard_Reward_Fragment;
import com.procuro.androidscm.SCMDataManager;

public class Dashboard_VOC extends AppCompatActivity {
    TextView title;
    Button back;
    info.hoang8f.android.segmented.SegmentedGroup infogroup;
    RadioButton CustomerCare,Surveys,TeamLeaders;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_voc);

        infogroup = findViewById(R.id.segmented2);
        CustomerCare = findViewById(R.id.customer_care);
        Surveys = findViewById(R.id.survey);
        TeamLeaders = findViewById(R.id.team_leaders);
        title = findViewById(R.id.username);
        back = findViewById(R.id.home);

        title.setText(SCMDataManager.getInstance().getSelectedSite().getSite().SiteName);


        setUpOnclicks();
    }

    private void setUpOnclicks(){

        infogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if ( i == CustomerCare.getId()){
                    CustomerCare();
                }else if ( i == Surveys.getId()){
                    Surveys();
                }else if (i == TeamLeaders.getId()){
                    TeamLeader();
                }
            }
        });
        CustomerCare.setChecked(true);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void CustomerCare() {
        Bundle bundle = new Bundle();
        bundle.putInt("image", R.drawable.no_data_customer_care);
        Dashboard_VOC_NoData_Fragment fragobj = new Dashboard_VOC_NoData_Fragment();
        fragobj.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.training_fragment_container, fragobj).commit();
    }
    private void Surveys() {
        Bundle bundle = new Bundle();
        bundle.putInt("image", R.drawable.no_data_survey);
        Dashboard_VOC_NoData_Fragment fragobj = new Dashboard_VOC_NoData_Fragment();
        fragobj.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.training_fragment_container, fragobj).commit();
    }

    private void TeamLeader() {  Bundle bundle = new Bundle();
        bundle.putInt("image", R.drawable.no_data_team);
        Dashboard_VOC_NoData_Fragment fragobj = new Dashboard_VOC_NoData_Fragment();
//        Dashboard_Reward_Fragment fragobj = new Dashboard_Reward_Fragment();
        fragobj.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.training_fragment_container, fragobj).commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
