package com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.HoursOfOperation;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.EquipmenList.EquipmenlistData;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.DayOperationSchedule;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsEquipment;
import com.suke.widget.SwitchButton;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import io.github.douglasjunior.androidSimpleTooltip.OverlayView;
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;


public class HoursOfOperation_list_adapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<DayOperationSchedule> arraylist;

    public HoursOfOperation_list_adapter(Context context, ArrayList<DayOperationSchedule> arraylist) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;
    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {

        if (arraylist.size()>7){
            return 7;
        }
        return arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return arraylist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
        view = inflater.inflate(R.layout.hours_of_operation_list_day, null);

        final DayOperationSchedule operation = arraylist.get(position);
        TextView name = view.findViewById(R.id.name);
        final ConstraintLayout start_container,end_container;
        final TextView start,end;
        CheckBox checkBox = view.findViewById(R.id.isActive);
        start_container = view.findViewById(R.id.start_container);
        end_container = view.findViewById(R.id.end_container);

        start = view.findViewById(R.id.start);
        end = view.findViewById(R.id.end);

        SimpleDateFormat dateFormat = new SimpleDateFormat("h:mm a");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        name.setText(operation.day);
        checkBox.setChecked(operation.isActive);

        SCMTool.DisableView(checkBox,1);

        if (operation.openTime!=null){
            start.setText(dateFormat.format(operation.openTime));
        }

        if (operation.closeTime!=null){
            end.setText(dateFormat.format(operation.closeTime));
        }



//        start_container.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                DisplayTimePicker(mContext,start_container,start);
//            }
//        });
//
//        end_container.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                DisplayTimePicker(mContext,end_container,end);
//            }
//        });



        return view;
    }


    public void DisplayTimePicker(final Context context, View anchor,final TextView textView) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.BOTTOM)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .arrowColor(ContextCompat.getColor(context, R.color.white))
                .transparentOverlay(false)
                .contentView(R.layout.time_picker)
                .focusable(true)
                .overlayOffset(0)
                .highlightShape(OverlayView.HIGHLIGHT_SHAPE_RECTANGULAR)
                .build();
        tooltip.show();

        final TimePicker timePicker = tooltip.findViewById(R.id.datepicker);
        Button accept = tooltip.findViewById(R.id.apply);


        final SimpleDateFormat dateFormat = new SimpleDateFormat("h:mm a");

        timePicker.setIs24HourView(false);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar date = Calendar.getInstance();
                textView.setText(SCMTool.getTime(timePicker));
                tooltip.dismiss();
            }
        });

    }

}

