package com.procuro.androidscm.Restaurant.Qualification.QualificationFragment;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.procuro.androidscm.GlideApp;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.Qualification.QualificationActivity;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.Qualification.QualificationActivityFragment;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.StoreListFragment.SiteList;

import java.util.ArrayList;

public class Qualification_SiteView_List_adapter extends BaseExpandableListAdapter implements ExpandableListAdapter {

    private Context context;
    private ArrayList<Corp_Regional> qualificationRegionals;
    private FragmentActivity fragmentActivity;

    public Qualification_SiteView_List_adapter(Context context, ArrayList<Corp_Regional> qualificationRegionals, FragmentActivity fragmentActivity) {
        this.qualificationRegionals = qualificationRegionals;
        this.context = context;
        this.fragmentActivity = fragmentActivity;
    }

    @Override
    public int getGroupCount() {
        return qualificationRegionals.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return qualificationRegionals.get(groupPosition).getSiteLists().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return qualificationRegionals.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return qualificationRegionals.get(groupPosition).getSiteLists().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        final Corp_Regional qualificationRegional = qualificationRegionals.get(groupPosition);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.qualificaton_region_level, parent, false);

        TextView name = contentView.findViewById(R.id.name);
        ImageView icon   = contentView.findViewById(R.id.icon);
        LinearLayout linearLayout = contentView.findViewById(R.id.linearLayout);
        TextView count = contentView.findViewById(R.id.childcount);

        int childcount = 0;

        try {
            for (SiteList siteList : qualificationRegional.getSiteLists()){
                for (CustomSite site : siteList.getCustomSites()){
                    childcount++;
                }
            }

            name.setText(qualificationRegional.corpStructures.name);
            count.setText(String.valueOf(childcount));
            linearLayout.setRotation(180);


            if (qualificationRegional.getCorpStructures().name.toUpperCase().contains("UNITED STATES")){
                GlideApp.with(context).load(R.drawable.region_usa).into(icon);
            }
            else  if (qualificationRegional.getCorpStructures().name.toUpperCase().contains("PAFICIC")){
                GlideApp.with(context).load(R.drawable.region_paficic).into(icon);
            }
            else  if (qualificationRegional.getCorpStructures().name.toUpperCase().contains("CANADA")){
                GlideApp.with(context).load(R.drawable.region_canada).into(icon);
            }
            else  if (qualificationRegional.getCorpStructures().name.toUpperCase().contains("LATIN AMERICA")){
                GlideApp.with(context).load(R.drawable.region_latin_america).into(icon);
            }
            else  if (qualificationRegional.getCorpStructures().name.toUpperCase().contains("AMEA")){
                GlideApp.with(context).load(R.drawable.region_emea).into(icon);
            }


        }catch (Exception e){
            e.printStackTrace();
        }



        return contentView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final SiteList siteList = qualificationRegionals.get(groupPosition).getSiteLists().get(childPosition);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.qualification_province_level, parent, false);

        TextView name = contentView.findViewById(R.id.name);
        final LinearLayout contianer = contentView.findViewById(R.id.container);
        TextView count = contentView.findViewById(R.id.childcount);


        try {
            name.setText(siteList.getProvince());
            count.setText(String.valueOf(siteList.getCustomSites().size()));


            for (CustomSite site : siteList.getCustomSites()){
                getSiteView(contianer,site,parent);
            }


        }catch (Exception e){
            e.printStackTrace();
        }




        return contentView;
    }


    private void getSiteView(final LinearLayout container, final CustomSite site, ViewGroup parent){

        View contentView = LayoutInflater.from(context).inflate(R.layout.qualificaton_site_level, parent, false);
        TextView name = contentView.findViewById(R.id.name);
        ImageView icon   = contentView.findViewById(R.id.icon);
        TextView address = contentView.findViewById(R.id.address);
        contentView.setBackgroundResource(R.drawable.onpress_design);


        try {

            if (site.getSite()!=null){

                if (site.getSite().os.equalsIgnoreCase("Custom")){

                    GlideApp.with(context).load(R.drawable.store_light_blue).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);

                }else {
                    if (site.getSite().AlarmSeverity == 0 ){
                        GlideApp.with(context).load(R.drawable.store_blue).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
                    }
                    else if (site.getSite().AlarmSeverity == 3 ){
                        GlideApp.with(context).load(R.drawable.store_green).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
                    }
                    else if (site.getSite().AlarmSeverity == 5 ){
                        GlideApp.with(context).load(R.drawable.store_yellow).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
                    }
                    else if (site.getSite().AlarmSeverity == 9 ){
                        GlideApp.with(context).load(R.drawable.store_red).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
                    }
                    else {
                        GlideApp.with(context).load(R.drawable.store_gray).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
                    }
                }

            }else {
                GlideApp.with(context).load(R.drawable.store_gray).into(icon);
            }

            StringBuilder sitesaddress = new StringBuilder();
            if (site.getSite().Address.Address1 !=null){
                sitesaddress.append(site.getSite().Address.Address1);
                sitesaddress.append(", ");

            }
            if (site.getSite().Address.City!=null){
                sitesaddress.append(site.getSite().Address.City);
                sitesaddress.append(", ");
            }

            if (site.getSite().Address.Postal!=null){
                sitesaddress.append(site.getSite().Address.Postal);
            }

            address.setText(sitesaddress.toString());
            name.setText(site.getSite().SiteName);

            contentView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    setUpQualificationData(fragmentActivity,site);
                }
            });


        }catch (Exception e){
            e.printStackTrace();
        }




        container.addView(contentView);
    }

    private void setUpQualificationData(FragmentActivity fragmentActivity,CustomSite customSite) {

        try {
            QualificationData data = new QualificationData();
            data.setSelectedSite(customSite);
            data.setStateByStateViewEnabled(true);
            QualificationData.setInstance(data);
//            Intent intent = new Intent(fragmentActivity, QualificationActivity.class);
//            fragmentActivity.startActivity(intent);
            fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.resturant_fragment_container,
                    new QualificationActivityFragment()).commit();
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }


}
