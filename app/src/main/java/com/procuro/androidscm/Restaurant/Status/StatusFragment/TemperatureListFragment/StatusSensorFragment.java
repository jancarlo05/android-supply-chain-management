package com.procuro.androidscm.Restaurant.Status.StatusFragment.TemperatureListFragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Keep;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.AllStorePopup.Status_SiteList_DialogFragment;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.CustomSensor;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.StoreListFragment.StatusStoreListFragment;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.PimmInstance;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.util.ArrayList;
import java.util.Objects;
import static android.content.Context.MODE_PRIVATE;

@Keep
public class StatusSensorFragment extends Fragment {

    private Status_SensorList_Adapter adapter;
    private ListView listView;
    private CustomSite site;
    private TextView Wait,dropdown_name,storename,titleheader;
    private ProgressBar progressBar;
    private com.procuro.apimmdatamanagerlib.aPimmDataManager dataManager;
    private Context mContext;
    private Button back,dropdown;
    private LinearLayout dropdownContainer;
    private ArrayList<CustomSensor>Sensors;
    private ArrayList<PimmInstance>instances;


    public StatusSensorFragment() {
        this.site = SCMDataManager.getInstance().getSelectedSite();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.status_temperature_fragment, container, false);
        listView = view.findViewById(R.id.listView);
        dropdownContainer = view.findViewById(R.id.dropdown_container);
        titleheader = view.findViewById(R.id.username);
        storename = view.findViewById(R.id.storename);
        back = view.findViewById(R.id.home);
        progressBar = view.findViewById(R.id.progressBar2);
        Wait = view.findViewById(R.id.wait);
        dropdown_name = view.findViewById(R.id.dropdown_name);
        dropdown = view.findViewById(R.id.dropdown);

        dataManager = aPimmDataManager.getInstance();

        if (site.getPimmDevice()!=null){
            dataManager.getInstanceListForSiteId(site.getPimmDevice().deviceID, new OnCompleteListeners.getInstanceListForSiteIdCallbackListener() {
                @Override
                public void getInstanceListForSiteIdCallback(ArrayList<PimmInstance> pimmInstanceArrayList, Error error) {
                    if (error == null){
                        if (pimmInstanceArrayList.size()>0){
                            TemperatureList temperatureList = new TemperatureList(pimmInstanceArrayList);
                            SCMDataManager.getInstance().setTemperatureList(temperatureList);
                            progressBar.setVisibility(View.INVISIBLE);
                            Wait.setVisibility(View.INVISIBLE);
                            FilterObjectNames(pimmInstanceArrayList);
                        }else {
                            progressBar.setVisibility(View.INVISIBLE);
                            Wait.setText("No Data");
                        }
                    }
                }
            });
        }else {
            progressBar.setVisibility(View.INVISIBLE);
            Wait.setText("No Data");
        }

        dropdownContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DisplaySiteListPopup();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                back();
            }
        });


        dropdown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplaySensorFilterPopup();
            }
        });

        dropdown_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplaySensorFilterPopup();
            }
        });

        titleheader.setText(site.getSite().SiteName);
        storename.setText(site.getSite().SiteName);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        mContext = context;
        setSharedpref();
        super.onAttach(context);


    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void populateTemperatureList(ArrayList<CustomSensor>objects, ArrayList<PimmInstance>instances){
        for (CustomSensor object: objects){
            if (object.isSelected()){
                adapter = new Status_SensorList_Adapter(mContext, object,getActivity(),site,instances);
                listView.setAdapter(adapter);
                break;
            }
        }
    }

    private void back() {
        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_right,R.anim.slide_right_out)
                .replace(R.id.resturant_fragment_container,
                        new StatusStoreListFragment()).commit();

    }

    private void DisplaySiteListPopup(){
        DialogFragment newFragment = Status_SiteList_DialogFragment.newInstance("status");
        assert getFragmentManager() != null;
        newFragment.show(getActivity().getSupportFragmentManager(), "dialog");
    }

    private void setSharedpref(){
        SharedPreferences mPrefs = Objects.requireNonNull(getActivity()).getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.remove("SCMDataManager");
        Gson gson = new Gson();
        String json = gson.toJson(SCMDataManager.getInstance());
        prefsEditor.putString("SCMDataManager", json);
        prefsEditor.apply();
    }


    private void FilterObjectNames(ArrayList<PimmInstance> temperatureLists){
        ArrayList<String>objectname = new ArrayList<>();
        ArrayList<CustomSensor>objects=  new ArrayList<>();
        for (PimmInstance newObjects: temperatureLists){
            if (!objectname.contains(newObjects.objectName)){
                objectname.add(newObjects.objectName);
                objects.add(new CustomSensor(newObjects.objectName));
            }
        }

        PopulateInstanceByObjectName(temperatureLists,objects);
    }

    private void PopulateInstanceByObjectName(ArrayList<PimmInstance>pimmInstances,
                                              ArrayList<CustomSensor>newObjects){
        for (PimmInstance instance: pimmInstances){
            for (CustomSensor newObject: newObjects){
                if (instance.objectName.equalsIgnoreCase(newObject.getTitle())){
                    if (newObject.getObject() == null){
                        ArrayList<PimmInstance>newObs = new ArrayList<>();
                        newObs.add(instance);
                        newObject.setObject(newObs);
                    }else {
                        newObject.getObject().add(instance);
                    }

                }
                if (newObject.getTitle().equalsIgnoreCase("Temperature")){
                    newObject.setSelected(true);
                }
            }
        }

        this.Sensors = newObjects;
        this.instances =pimmInstances;
        populateTemperatureList(Sensors,instances);
    }


    private void DisplaySensorFilterPopup(){
        if (Sensors !=null){
            DialogFragment newFragment = Status_SensorFilter_DialogFragment.newInstance(Sensors,dropdown_name,instances,site,listView);
            assert getFragmentManager() != null;
            newFragment.show(getActivity().getSupportFragmentManager(), "dialog");
        }
    }
}


