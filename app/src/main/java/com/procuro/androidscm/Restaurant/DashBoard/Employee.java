package com.procuro.androidscm.Restaurant.DashBoard;

import java.util.ArrayList;
import java.util.Date;

public class Employee {

    String Firstname;
    String Lastname;
    String position;
    boolean selected;
    Date timeIn;
    Date timeOut;
    int stickerCount;

    String Username;
    String Password;
    String EmployeeID;
    String Email;
    String PersonalID;
    Date BirthDate;
    String Permission;
    String Street;
    String City;
    String State;
    String ZipCode;
    String MobileNumber;
    String Carrier;
    Date HireDate;
    Date ReviewDate;
    ArrayList<String>skills;
    ArrayList<String>sites;
    int ProfileImage;

    public Employee(String firstname, String lastname, boolean selected, int stickerCount, String email, ArrayList<String> skills, int profileImage) {
        Firstname = firstname;
        Lastname = lastname;
        this.selected = selected;
        this.stickerCount = stickerCount;
        Email = email;
        this.skills = skills;
        ProfileImage = profileImage;
    }

    public Employee(String firstname, String lastname, String position, boolean selected, Date timeIn, Date timeOut, int stickerCount, String username, String password, String employeeID, String email, String personalID, Date birthDate, String permission, String street, String city, String state, String zipCode, String mobileNumber, String carrier, Date hireDate, Date reviewDate, ArrayList<String> skills, ArrayList<String> sites, int profileImage) {
        Firstname = firstname;
        Lastname = lastname;
        this.position = position;
        this.selected = selected;
        this.timeIn = timeIn;
        this.timeOut = timeOut;
        this.stickerCount = stickerCount;
        Username = username;
        Password = password;
        EmployeeID = employeeID;
        Email = email;
        PersonalID = personalID;
        BirthDate = birthDate;
        Permission = permission;
        Street = street;
        City = city;
        State = state;
        ZipCode = zipCode;
        MobileNumber = mobileNumber;
        Carrier = carrier;
        HireDate = hireDate;
        ReviewDate = reviewDate;
        this.skills = skills;
        this.sites = sites;
        ProfileImage = profileImage;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getEmployeeID() {
        return EmployeeID;
    }

    public void setEmployeeID(String employeeID) {
        EmployeeID = employeeID;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPersonalID() {
        return PersonalID;
    }

    public void setPersonalID(String personalID) {
        PersonalID = personalID;
    }

    public Date getBirthDate() {
        return BirthDate;
    }

    public void setBirthDate(Date birthDate) {
        BirthDate = birthDate;
    }

    public String getPermission() {
        return Permission;
    }

    public void setPermission(String permission) {
        Permission = permission;
    }

    public String getStreet() {
        return Street;
    }

    public void setStreet(String street) {
        Street = street;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String zipCode) {
        ZipCode = zipCode;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getCarrier() {
        return Carrier;
    }

    public void setCarrier(String carrier) {
        Carrier = carrier;
    }

    public Date getHireDate() {
        return HireDate;
    }

    public void setHireDate(Date hireDate) {
        HireDate = hireDate;
    }

    public Date getReviewDate() {
        return ReviewDate;
    }

    public void setReviewDate(Date reviewDate) {
        ReviewDate = reviewDate;
    }

    public ArrayList<String> getSkills() {
        return skills;
    }

    public void setSkills(ArrayList<String> skills) {
        this.skills = skills;
    }

    public ArrayList<String> getSites() {
        return sites;
    }

    public void setSites(ArrayList<String> sites) {
        this.sites = sites;
    }

    public int getProfileImage() {
        return ProfileImage;
    }

    public void setProfileImage(int profileImage) {
        ProfileImage = profileImage;
    }


    public Employee(String firstname, String lastname, Date timeIn, Date timeOut, int stickerCount) {
        Firstname = firstname;
        Lastname = lastname;
        this.timeIn = timeIn;
        this.timeOut = timeOut;
        this.stickerCount = stickerCount;
    }

    public Employee(String firstname, String lastname, Date timeIn, Date timeOut, int stickerCount, int ProfileImage) {
        Firstname = firstname;
        Lastname = lastname;
        this.timeIn = timeIn;
        this.timeOut = timeOut;
        this.stickerCount = stickerCount;
        this.ProfileImage = ProfileImage;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getFirstname() {
        return Firstname;
    }

    public void setFirstname(String firstname) {
        Firstname = firstname;
    }

    public String getLastname() {
        return Lastname;
    }

    public void setLastname(String lastname) {
        Lastname = lastname;
    }

    public Date getTimeIn() {
        return timeIn;
    }

    public void setTimeIn(Date timeIn) {
        this.timeIn = timeIn;
    }

    public Date getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(Date timeOut) {
        this.timeOut = timeOut;
    }

    public int getStickerCount() {
        return stickerCount;
    }

    public void setStickerCount(int stickerCount) {
        this.stickerCount = stickerCount;
    }
}
