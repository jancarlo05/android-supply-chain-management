package com.procuro.androidscm.Restaurant.Calendar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.EmployeeList.EmployeeListActivity;
import com.procuro.androidscm.Restaurant.Guide.GuideActivity;
import com.procuro.androidscm.Restaurant.Guide.GuideData;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileActivityOverAll;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class CalendarActivity extends AppCompatActivity {

    public static int year;
    public static int months;
    int days;
    Button back,add,help;
    public static TextView title;
    private int currentposition;
    final ArrayList<View>views = new ArrayList<>();
    private LinearLayout root;
    ViewPager viewPager;
    ListView listView;
    com.procuro.apimmdatamanagerlib.aPimmDataManager dataManager = aPimmDataManager.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendar_container_fragment);

        back = findViewById(R.id.back_button);
        title = findViewById(R.id.name);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        root = findViewById(R.id.root);
        add = findViewById(R.id.add);
        help = findViewById(R.id.help);

        InitializeData();

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            @Override
            public void onPageSelected(int position) {

                if (position==0){
                    year--;
                    setupPreviewYear(year,viewPager,CalendarActivity.this);
                }else if (position == views.size()-1){
                    int last_positon = views.size()-1;
                    year++;
                    setupFutureYear(year,viewPager,CalendarActivity.this,last_positon);
                }
                currentposition = position;

                title.setText(views.get(viewPager.getCurrentItem()).getTag().toString());


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

        });



        setUponclicks();

    }

    private void setUponclicks(){
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back();
            }
        });

        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpGuideModule();
            }
        });
    }

//    private void ShowCalendar(TextView title) {
//           getSupportFragmentManager().beginTransaction().replace(R.id.calendar_container,
//                    new CalendarFragment(year,months,title)).commit();
//    }



    private void back() {
            finish();
    }

    private void setUpGuideModule(){
        GuideData data = GuideData.getInstance();
        data.setSelected(GuideActivity.GUIDE_CALENDAR);
        Intent intent = new Intent(this, GuideActivity.class);
        startActivity(intent);

    }

    private void setupReportsDelivery() {

        ArrayList<CalendarWeeks> weeklies = new ArrayList<>();
        java.util.Calendar cal = java.util.Calendar.getInstance();
        cal.set(java.util.Calendar.YEAR,year);
        cal.set(java.util.Calendar.MONTH,months);
        cal.setFirstDayOfWeek(Calendar.MONDAY);
        int maxWeeknumber = cal.getActualMaximum(java.util.Calendar.WEEK_OF_MONTH);

        weeklies.add(delivery_weekly());

        for (int j = 1; j < maxWeeknumber+1; j++) {
            cal.set(java.util.Calendar.WEEK_OF_MONTH, j);
            int weekOfYear = cal.get(java.util.Calendar.WEEK_OF_YEAR);
            cal.set(java.util.Calendar.DAY_OF_WEEK, java.util.Calendar.MONDAY);
            cal.set(java.util.Calendar.HOUR,0);
            cal.set(java.util.Calendar.MINUTE,0);
            ArrayList<CalendarDaily> dailies = new ArrayList<>();
            for (int k = 0; k < 7; k++) {

                java.util.Calendar daysCalendar = java.util.Calendar.getInstance();
                daysCalendar.setTime(cal.getTime());
                daysCalendar.add(java.util.Calendar.DATE, k);
                dailies.add(new CalendarDaily(daysCalendar.getTime()));
                SimpleDateFormat formats = new SimpleDateFormat("MMM dd , hh:mm a");


            }
            cal.add(java.util.Calendar.DATE, 6);
            weeklies.add(new CalendarWeeks(weekOfYear,dailies));


            CalendarListViewAdapter adapter = new CalendarListViewAdapter(this,weeklies,months,year,title,this);
            listView.setAdapter(adapter);
        }
    }

    private CalendarWeeks delivery_weekly(){

        CalendarWeeks delivery_weekly = null;
        java.util.Calendar Pcal = java.util.Calendar.getInstance();
        Pcal.set(java.util.Calendar.YEAR, year);
        Pcal.set(java.util.Calendar.MONTH, months);
        Pcal.set(java.util.Calendar.WEEK_OF_MONTH,1);
        Pcal.set(java.util.Calendar.DAY_OF_WEEK, java.util.Calendar.MONDAY);
        Pcal.add(java.util.Calendar.DATE , -7);
        Pcal.set(java.util.Calendar.HOUR,0);
        Pcal.set(java.util.Calendar.MINUTE,0);

        int weekOfYear = Pcal.get(java.util.Calendar.WEEK_OF_YEAR);
        ArrayList<CalendarDaily>previousdaily = new ArrayList<>();

        for (int k = 0; k <7 ; k++) {

            java.util.Calendar daysCalendar = java.util.Calendar.getInstance();
            daysCalendar.setTime(Pcal.getTime());
            daysCalendar.add(java.util.Calendar.DATE,+k);
            previousdaily.add(new CalendarDaily(daysCalendar.getTime()));
            SimpleDateFormat formats = new SimpleDateFormat("MMM dd , hh:mm a");


        }

        Pcal.add(java.util.Calendar.DATE, 6);
        delivery_weekly = new CalendarWeeks(weekOfYear,previousdaily);
        return delivery_weekly;

    }

    private void InitialDisplay(int year, ViewPager viewPager , ArrayList<Date>dates, Context context){

        for (int i = 0; i <12 ; i++) {
            View view = getLayoutInflater().inflate(R.layout.calendar_fragment,null);
            listView = view.findViewById(R.id.listview);
            final SimpleDateFormat format = new SimpleDateFormat("MMMM yyyy");

            java.util.Calendar SelectedMonth = java.util.Calendar.getInstance();
            SelectedMonth.setTime(new Date());
            months = i;
            SelectedMonth.set(java.util.Calendar.YEAR,year);
            SelectedMonth.set(java.util.Calendar.MONTH,months);
            setupReportsDelivery();
            dates.add(SelectedMonth.getTime());
            view.setTag(format.format(SelectedMonth.getTime()));
            views.add(view);
        }

        final SimpleDateFormat format = new SimpleDateFormat("MMMM yyyy");
        final CalendarViewPagerAdapter adapter = new CalendarViewPagerAdapter(views,context,viewPager);
        viewPager.setAdapter(adapter);

        for (int i = 0; i <dates.size() ; i++) {
            Date date = dates.get(i);
            java.util.Calendar current = java.util.Calendar.getInstance();
            current.setTime(new Date());
            java.util.Calendar selected = java.util.Calendar.getInstance();
            selected.setTime(date);

            if (format.format(current.getTime()).equalsIgnoreCase(format.format(selected.getTime()))){
                viewPager.setCurrentItem(i);
                title.setText(format.format(date));
                break;
            }
        }
    }

    private void setupPreviewYear(int year, ViewPager viewPager , Context context){

        for (int i = 0; i <12 ; i++) {
            View view = getLayoutInflater().inflate(R.layout.calendar_fragment,null);

            listView = view.findViewById(R.id.listview);
            final SimpleDateFormat format = new SimpleDateFormat("MMMM yyyy");

            java.util.Calendar SelectedMonth = java.util.Calendar.getInstance();
            SelectedMonth.setTime(new Date());
            months = i;
            SelectedMonth.set(java.util.Calendar.YEAR,year);
            SelectedMonth.set(java.util.Calendar.MONTH,months);
            setupReportsDelivery();
            view.setTag(format.format(SelectedMonth.getTime()));
            views.add(i,view);
        }

        final SimpleDateFormat format = new SimpleDateFormat("MMMM yyyy");
        final CalendarViewPagerAdapter adapter = new CalendarViewPagerAdapter(views,context,viewPager);
        viewPager.setAdapter(adapter);

        viewPager.setCurrentItem(12);
    }

    private void setupFutureYear(int year, ViewPager viewPager, Context context,int lastPosition){

        for (int i = 0; i <12 ; i++) {
            View view = getLayoutInflater().inflate(R.layout.calendar_fragment,null);
            listView = view.findViewById(R.id.listview);
            final SimpleDateFormat format = new SimpleDateFormat("MMMM yyyy");

            java.util.Calendar SelectedMonth = java.util.Calendar.getInstance();
            SelectedMonth.setTime(new Date());
            months = i;
            SelectedMonth.set(java.util.Calendar.YEAR,year);
            SelectedMonth.set(java.util.Calendar.MONTH,months);
            setupReportsDelivery();
            view.setTag(format.format(SelectedMonth.getTime()));
            views.add(view);
        }

        final SimpleDateFormat format = new SimpleDateFormat("MMMM yyyy");
        final CalendarViewPagerAdapter adapter = new CalendarViewPagerAdapter(views,context,viewPager);
        viewPager.setAdapter(adapter);

        viewPager.setCurrentItem(lastPosition);
        adapter.notifyDataSetChanged();
    }

    private void InitializeData(){
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please Wait");
        progressDialog.setMessage("Downloading Data");
        progressDialog.setCancelable(true);
        progressDialog.show();


        final ArrayList<Date> dates = new ArrayList<>();
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(new Date());
        year = calendar.get(java.util.Calendar.YEAR);


        Site site = SCMDataManager.getInstance().getSelectedSite().getSite();
        dataManager.getStoreUsers(site.siteid, new OnCompleteListeners.getStoreUsersCallbackListener() {
            @Override
            public void getStoreUsersCallback(ArrayList<User> storeUsers, Error error) {
                if (error == null){
                    ArrayList<CustomUser>users = new ArrayList<>();
                    if (storeUsers!=null){
                        for (User user : storeUsers){
                            if (user.active){
                                users.add(new CustomUser(user));
                            }
                        }
                    }
                    SCMDataManager.getInstance().setStoreUsers(users);
                    InitialDisplay(year,viewPager,dates,CalendarActivity.this);
                    progressDialog.dismiss();

                }else {
                    InitialDisplay(year,viewPager,dates,CalendarActivity.this);
                    DisplaySnackbar("Error Downloading Data");
                    progressDialog.dismiss();
                }
            }
        });
    }

    private void DisplaySnackbar(String title){

        try {
            Snackbar snackbar = Snackbar
                    .make(root, title, Snackbar.LENGTH_LONG);
            snackbar.show();

        }catch (Exception e){
            e.printStackTrace();
        }

    }

}
