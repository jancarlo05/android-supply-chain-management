package com.procuro.androidscm.Restaurant.Qualification.QualificationFragment;

import com.procuro.androidscm.Restaurant.CustomSite;

import java.util.ArrayList;

public class Corp_Chain {

    private String name;
    ArrayList<Corp_Regional> qualificationRegionals;
    ArrayList<CustomSite>sites;
    private ArrayList<Corp_Area>corp_areas;


    public Corp_Chain() {

    }

    public ArrayList<Corp_Area> getCorp_areas() {
        return corp_areas;
    }

    public void setCorp_areas(ArrayList<Corp_Area> corp_areas) {
        this.corp_areas = corp_areas;
    }

    public Corp_Chain(String name) {
        this.name = name;
    }

    public ArrayList<CustomSite> getSites() {
        return sites;
    }

    public void setSites(ArrayList<CustomSite> sites) {
        this.sites = sites;
    }

    public Corp_Chain(String name, ArrayList<Corp_Regional> qualificationRegionals) {
        this.name = name;
        this.qualificationRegionals = qualificationRegionals;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Corp_Regional> getQualificationRegionals() {
        return qualificationRegionals;
    }

    public void setQualificationRegionals(ArrayList<Corp_Regional> qualificationRegionals) {
        this.qualificationRegionals = qualificationRegionals;
    }
}
