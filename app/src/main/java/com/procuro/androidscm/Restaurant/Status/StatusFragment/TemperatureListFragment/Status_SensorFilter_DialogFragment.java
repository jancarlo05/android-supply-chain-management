package com.procuro.androidscm.Restaurant.Status.StatusFragment.TemperatureListFragment;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.CustomSensor;
import com.procuro.apimmdatamanagerlib.PimmInstance;

import java.util.ArrayList;
import java.util.Objects;

public class Status_SensorFilter_DialogFragment extends DialogFragment {

    public static ArrayList<CustomSensor> objects;
    public static ArrayList<PimmInstance>instances;
    public static CustomSite site;
    public static ListView tempListview;
    private Status_SensorFilter_adapter sensorTypeAdapter;
    public static TextView SelectedSensor;



    public static Status_SensorFilter_DialogFragment newInstance(ArrayList<CustomSensor> obj1, TextView obj2,
                                                                 ArrayList<PimmInstance>obj3, CustomSite obj4, ListView obj5) {
        objects = obj1;
        SelectedSensor = obj2;
        instances = obj3;
        site = obj4;
        tempListview = obj5;
        return new Status_SensorFilter_DialogFragment();
    }
    @Override
        public int getTheme() {
        return R.style.slide_down_up;
        }

    @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.status_temperature_filter_popup, container, false);

//                    int height = (int)(getResources().getDisplayMetrics().heightPixels*0.90);
//                    int width = 320;
//
        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.90);
        int height = (int)(getResources().getDisplayMetrics().heightPixels*0.60);


        Objects.requireNonNull(getDialog().getWindow()).setLayout(width, height);
        getDialog().setCanceledOnTouchOutside(true);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.slide_down_up;
        view.setFocusableInTouchMode(true);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        ListView listView = view.findViewById(R.id.listView);
        sensorTypeAdapter = new Status_SensorFilter_adapter(getContext(),objects,
                getDialog(),tempListview,instances,getActivity(),SelectedSensor,site);
        listView.setAdapter(sensorTypeAdapter);

        return view;
        }



    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.90);
        Window window = Objects.requireNonNull(getDialog()).getWindow();
        assert window != null;
        window.setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
    }
}
