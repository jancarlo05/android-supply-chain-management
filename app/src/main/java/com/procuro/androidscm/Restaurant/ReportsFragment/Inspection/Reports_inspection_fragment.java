package com.procuro.androidscm.Restaurant.ReportsFragment.Inspection;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import androidx.fragment.app.Fragment;

import com.diegocarloslima.fgelv.lib.FloatingGroupExpandableListView;
import com.diegocarloslima.fgelv.lib.WrapperExpandableListAdapter;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsData;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsParentList;
import com.procuro.androidscm.SCMDataManager;

import java.util.ArrayList;

public class Reports_inspection_fragment extends Fragment {


    public static WrapperExpandableListAdapter wrapperAdapter;
    public static ExpandableListView expandableListView;
    public static  Reports_inspection_list_adapter adapter;


    public Reports_inspection_fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.reports_left_pane_inspection, container, false);
        expandableListView = rootView.findViewById(R.id.expandible_listview);

        populate_Inspection_list(SCMDataManager.getInstance().getReports_inspections());

        return rootView;
    }


    public void populate_Inspection_list(ArrayList<ReportsParentList>inspections){

        adapter = new Reports_inspection_list_adapter(getContext(), inspections,getActivity(),expandableListView);
        expandableListView.setAdapter(adapter);
        for (int i = 0; i < adapter.getGroupCount(); i++) {
            expandableListView.expandGroup(i);
        }
        expandableListView.setDividerHeight(0);
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                return true; // This way the expander cannot be collapsed
            }
        });

        ReportsData data = ReportsData.getInstance();
        SCMDataManager scm = SCMDataManager.getInstance();

        if (scm.getReportLevel().equalsIgnoreCase("Site")){
            if (!data.isDisableDefaultView()){
                expandableListView.setStackFromBottom(true);
                expandableListView.setTranscriptMode(View.OVER_SCROLL_ALWAYS);
            }else {
                expandableListView.setStackFromBottom(false);
                expandableListView.setTranscriptMode(View.OVER_SCROLL_ALWAYS);
            }
        }
    }

    public void timerDelayRunForScroll(long time) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                try {
                    expandableListView.smoothScrollToPosition(1);
                } catch (Exception e) {}
            }
        }, time);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}