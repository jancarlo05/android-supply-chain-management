package com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.fragment.app.Fragment;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.MoreFragment.MoreFragment;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourceManualFragment.Resources_left_pane_resource_manual_fragment;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourceTrainingVideos.Resources_TrainingVideo_fragment;
import com.procuro.androidscm.Restaurant.QuickAccessMenu.QuickAccessMenu;

public class Resources_left_pane_fragment extends Fragment {

    private  RadioButton resourcemanual;
    private  RadioButton trainingvideos;
    private Button back,quick_a_menu,Covid;

    int id = 0;


    public  static  info.hoang8f.android.segmented.SegmentedGroup infogroup;

    public Resources_left_pane_fragment(int id) {
        this.id = id;
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.resources_fragment, container, false);

        resourcemanual = rootView.findViewById(R.id.resource_manual);
        trainingvideos = rootView.findViewById(R.id.training_videos);
        infogroup = rootView.findViewById(R.id.segmented2);
        back = rootView.findViewById(R.id.home);
        quick_a_menu = rootView.findViewById(R.id.quick_a_menu);
        Covid = rootView.findViewById(R.id.covid);

        setupOnclicks();


        if (id == resourcemanual.getId()){
            resourcemanual.setChecked(true);
        }else {
            trainingvideos.setChecked(true);
        }

        return rootView;
    }

    private void setupOnclicks(){

        Covid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuickAccessMenu.DisplayCovidFragment(getActivity());
            }
        });

        quick_a_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuickAccessMenu.DisplayQuickAMenu(getContext(),quick_a_menu,getActivity());
            }
        });

        infogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                if (i == resourcemanual.getId()) {
                    DisplayResourceManual();

                } else if (i == trainingvideos.getId()) {
                    DisplayTrainingVideos();
                }

            }

        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back();
            }
        });

    }

    public  void  DisplayResourceManual() {
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.resource_left_pane_Fragment_container,
                new Resources_left_pane_resource_manual_fragment()).commit();
    }

    public  void  DisplayTrainingVideos() {
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.resource_left_pane_Fragment_container,
                new Resources_TrainingVideo_fragment()).commit();
    }

    private void back() {
            getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_right,R.anim.slide_right_out)
                    .replace(R.id.resturant_fragment_container,
                            new MoreFragment()).commit();
        }
    }
