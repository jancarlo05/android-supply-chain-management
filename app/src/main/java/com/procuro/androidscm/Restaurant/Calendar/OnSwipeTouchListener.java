package com.procuro.androidscm.Restaurant.Calendar;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ListView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;

public class OnSwipeTouchListener implements OnTouchListener {
    private GestureDetector gestureDetector;
    private Context ctx;
    protected MotionEvent mLastOnDownEvent = null;
    private View view;

    public OnSwipeTouchListener(Context ctx){
        gestureDetector = new GestureDetector(ctx, new GestureListener());
        this.ctx = ctx;
    }

    public GestureDetector getDetector() {
        return gestureDetector;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        mLastOnDownEvent= event;
        if (gestureDetector==null){
            gestureDetector = new GestureDetector(ctx, new GestureListener());
        }
        return gestureDetector.onTouchEvent(event);
    }



    private final class GestureListener extends  GestureDetector.SimpleOnGestureListener {
        private static final int SWIPE_THRESHOLD = 100;
        private static final int SWIPE_VELOCITY_THRESHOLD = 100;

        @Override
        public boolean onDown(MotionEvent e) {
            mLastOnDownEvent = e;
            return true;
        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {

            try {
                if (e1==null){
                    e1 = mLastOnDownEvent;
                    e2 = mLastOnDownEvent;
                }
                float diffY = e2.getY() - e1.getY();
                float diffX = e2.getX() - e1.getX();
                if (Math.abs(diffX) > Math.abs(diffY)) {
                    if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffX > 0) {
                            onSwipeRight();
                            Toast.makeText(ctx, "onSwipeRight", Toast.LENGTH_SHORT).show();
                        } else {
                            onSwipeLeft();
                            Toast.makeText(ctx, "onSwipeLeft", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffY > 0) {
                        onSwipeBottom();
                    } else {
                        onSwipeTop();
                    }

                }
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            return true;
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            mLastOnDownEvent =e;
            onClick();
            return super.onSingleTapUp(e);
        }


    }


    public void onClick(){

    }

    public void onSwipeRight() {
    }
    public void onSwipeLeft() {
    }
    public void onSwipeTop() {
    }
    public void onSwipeBottom() {
    }
}