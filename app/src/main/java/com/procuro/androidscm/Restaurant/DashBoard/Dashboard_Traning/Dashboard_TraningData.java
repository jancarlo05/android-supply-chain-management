package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Traning;

import com.procuro.apimmdatamanagerlib.TrainingDataItems;

import java.util.ArrayList;

public class Dashboard_TraningData {

    private String category;
    private ArrayList<TrainingDataItems>trainingDataItems;
    private boolean selected;


    public Dashboard_TraningData(String category) {
        this.category = category;
    }

    public Dashboard_TraningData(String category, ArrayList<TrainingDataItems> trainingDataItems) {
        this.category = category;
        this.trainingDataItems = trainingDataItems;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public ArrayList<TrainingDataItems> getTrainingDataItems() {
        return trainingDataItems;
    }

    public void setTrainingDataItems(ArrayList<TrainingDataItems> trainingDataItems) {
        this.trainingDataItems = trainingDataItems;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
