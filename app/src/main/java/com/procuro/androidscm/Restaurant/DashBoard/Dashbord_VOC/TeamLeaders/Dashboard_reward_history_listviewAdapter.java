package com.procuro.androidscm.Restaurant.DashBoard.Dashbord_VOC.TeamLeaders;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.procuro.androidscm.GlideApp;
import com.procuro.androidscm.R;
import com.procuro.apimmdatamanagerlib.Award;

import java.text.SimpleDateFormat;
import java.util.ArrayList;


public class Dashboard_reward_history_listviewAdapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<Award> arraylist;


    public Dashboard_reward_history_listviewAdapter(Context context, ArrayList<Award> arraylist) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;
    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return arraylist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.dashboard_reward_history_list_data, null);
            view.setTag(holder);

            Award item = arraylist.get(position);
            ImageView icon = view.findViewById(R.id.icon);
            TextView comment = view.findViewById(R.id.comment);
            TextView date = view.findViewById(R.id.date);

            SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");

            if (item.timestamp!=null){
                date.setText(format.format(item.timestamp));
            }

            GlideApp.with(mContext).asDrawable().load(R.drawable.sticker).
                    diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(icon);

            if (item.comment!=null){
                if (!item.comment.equalsIgnoreCase("null")){
                    comment.setText(item.comment);
                }else {
                    comment.setText("--");
                }
            }else {
                comment.setText("--");
            }

        if (position % 2 == 1) {
            view.setBackgroundColor(Color.TRANSPARENT);
        } else {
            view.setBackgroundColor(ContextCompat.getColor(mContext, R.color.indicator));
        }


        return view;
    }



}

