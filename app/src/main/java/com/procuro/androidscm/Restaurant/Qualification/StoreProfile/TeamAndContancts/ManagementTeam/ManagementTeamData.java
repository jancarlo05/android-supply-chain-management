package com.procuro.androidscm.Restaurant.Qualification.StoreProfile.TeamAndContancts.ManagementTeam;

import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.StoreProfileOptions.KitchenCriteria;
import com.procuro.apimmdatamanagerlib.DashboardKitchenData;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsUserRole;

import java.util.ArrayList;

public class ManagementTeamData {
    private static ManagementTeamData instance = new ManagementTeamData();
    private ArrayList<SiteSettingsUserRole> userRoles;
    private ArrayList<CustomUser>thriftusers;

    public static ManagementTeamData getInstance() {
        if(instance == null) {
            instance = new ManagementTeamData();
        }
        return instance;
    }

    public ArrayList<CustomUser> getThriftusers() {
        return thriftusers;
    }

    public void setThriftusers(ArrayList<CustomUser> thriftusers) {
        this.thriftusers = thriftusers;
    }

    public static void setInstance(ManagementTeamData instance) {
        ManagementTeamData.instance = instance;
    }

    public ArrayList<SiteSettingsUserRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(ArrayList<SiteSettingsUserRole> userRoles) {
        this.userRoles = userRoles;
    }
}
