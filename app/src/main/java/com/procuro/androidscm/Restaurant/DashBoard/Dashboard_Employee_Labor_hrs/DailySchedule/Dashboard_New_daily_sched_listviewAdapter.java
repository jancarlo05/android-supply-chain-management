package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.DailySchedule;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.Dashboard_Sched_Skills_RecyclerViewAdapter;
import com.procuro.androidscm.Restaurant.EmployeeRerport.EmployeeReportActivity;
import com.procuro.androidscm.Restaurant.EmployeeRerport.EmployeeReportData;
import com.procuro.androidscm.Restaurant.Schema.Dayparts;
import com.procuro.androidscm.Restaurant.Schema.Schema;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.Certification;
import com.procuro.apimmdatamanagerlib.CertificationDefinition;
import com.procuro.apimmdatamanagerlib.DashboardKitchenData;
import com.procuro.apimmdatamanagerlib.PositionDataItems;
import com.procuro.apimmdatamanagerlib.SMSPositioningEmployeeScheduleAssignmentPositions;
import com.procuro.apimmdatamanagerlib.Schedule_Business_Site_Plan;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.User;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;


public class Dashboard_New_daily_sched_listviewAdapter extends BaseAdapter {

    // Declare Variables

    Context mContext;
    LayoutInflater inflater;
    private ArrayList<CustomUser> users;
    private int counter = 0;
    private Date selectedDate;
    private ArrayList<String>positionskills;
    private FragmentActivity fragmentActivity;


    public Dashboard_New_daily_sched_listviewAdapter(Context context, ArrayList<CustomUser> users,
                                                     FragmentActivity fragmentActivity,Date selectedDate) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.users = users;
        this.positionskills =getPositionSkills();
        this.fragmentActivity = fragmentActivity;
        this.selectedDate =selectedDate;
    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        holder = new ViewHolder();

        view = LayoutInflater.from(mContext).inflate(R.layout.daily_sched_parent_list_data, parent, false);
        TextView name = view.findViewById(R.id.name);
        RecyclerView skills = view.findViewById(R.id.recyclerView);
        RecyclerView positionsRecyclerView = view.findViewById(R.id.positions);
        TextView message = view.findViewById(R.id.message);

        TextView startTime = view.findViewById(R.id.startTime);
        TextView endTIme = view.findViewById(R.id.endTime);
        ImageView employee_report = view.findViewById(R.id.employee_report);

        final LinearLayout child_continer = view.findViewById(R.id.child_container);
        final LinearLayout indicator = view.findViewById(R.id.indicator);

        CustomUser user = users.get(position);

        try {
            name.setText(user.getUser().getFullName());
        }catch (Exception e){
            e.printStackTrace();
        }

        DisplaySkilss(user,skills,message);

        setupCollapseView(view,child_continer,indicator);

        ArrayList<String>StringPositions = DisplayPositionCovered(user,parent,child_continer,startTime,endTIme);

        DisplayPositionList(StringPositions,positionsRecyclerView);

        setUpEmployeeReport(user,employee_report,selectedDate);

        return view;
    }

    private ArrayList<String> DisplayPositionCovered(CustomUser user,ViewGroup parent,LinearLayout child_continer,TextView startTime,TextView endTIme){

        ArrayList<String>StringPositions = new ArrayList<>();
        if (user.getSchedules().size()>0){

            Collections.sort(user.getSchedules(), new Comparator<Schedule_Business_Site_Plan>() {
                @Override
                public int compare(Schedule_Business_Site_Plan o1, Schedule_Business_Site_Plan o2) {
                    return o1.startTime.compareTo(o2.startTime);
                }
            });

            ArrayList<Date>TimeIns = new ArrayList<>();
            ArrayList<Date>TimeOuts = new ArrayList<>();

            for (Schedule_Business_Site_Plan sched : user.getSchedules()){
                CreateChildPosition(parent,child_continer,sched,user.getUser());

                if (!StringPositions.contains(String.valueOf(sched.position))){
                    StringPositions.add(String.valueOf(sched.position));
                }
                if (sched.startTime !=null && sched.endTime !=null){
                    TimeIns.add(sched.startTime);
                    TimeOuts.add(sched.endTime);
                }
            }

            DisplayStart_EndTime(TimeIns,startTime,true);

            DisplayStart_EndTime(TimeOuts,endTIme,false);

        }else {
            CreateNullPOsition(child_continer);
        }

        return StringPositions;
    }

    private void setupCollapseView(View view, final LinearLayout child_continer, final LinearLayout indicator){
        final boolean[] expanded = {false};
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (expanded[0]){
                    expanded[0] = false;
                    child_continer.setVisibility(View.GONE);
                    indicator.setRotation(0);
                }else {
                    expanded[0] = true;
                    child_continer.setVisibility(View.VISIBLE);
                    indicator.setRotation(180);

                }
            }
        });
        if (expanded[0]){
            child_continer.setVisibility(View.VISIBLE);
            indicator.setRotation(180);
        }else {
            child_continer.setVisibility(View.GONE);
            indicator.setRotation(0);
        }

    }

    private void DisplaySkilss(CustomUser user,RecyclerView skills ,TextView message){
        try {
            if (user.getUser().certificationList !=null){
                if (user.getUser().certificationList.size()>0){

                    ArrayList<Certification>FilteredPositions = new ArrayList<>();
                    for (Certification certification : user.getUser().certificationList){
                        if (positionskills.contains(certification.Certification)){
                            FilteredPositions.add(certification);
                        }
                    }

                    if (FilteredPositions.size()>0){
                        Dashboard_Sched_Skills_RecyclerViewAdapter skill_adapter = new Dashboard_Sched_Skills_RecyclerViewAdapter(mContext,FilteredPositions);
                        skills.setLayoutManager(new GridLayoutManager(mContext,4));
                        skills.setAdapter(skill_adapter);
                        skills.setEnabled(false);
                        skills.setFocusable(false);
                        message.setVisibility(View.GONE);
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void DisplayPositionList(ArrayList<String>positions,RecyclerView recyclerView){
        try {
            Dashboard_DailySched_PositionRecyclerView_Adapter adapter = new Dashboard_DailySched_PositionRecyclerView_Adapter(mContext,positions);
            recyclerView.setLayoutManager(new GridLayoutManager(mContext,2));
            recyclerView.setAdapter(adapter);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void DisplayStart_EndTime(ArrayList<Date>dates,TextView textView,boolean startTime){
        try {
            SimpleDateFormat format = new SimpleDateFormat("h:mm a");
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = null;
            Collections.sort(dates);
            if (startTime){
                date = dates.get(0);
            }else {
                date = dates.get(dates.size()-1);
            }
            textView.setText(format.format(date));

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void CreateNullPOsition(LinearLayout container){

        TextView textView = new TextView(mContext);
        textView.setId(View.generateViewId());
        textView.setText("NO Position Data");
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textView.setLayoutParams(params);
        textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        textView.setPadding(SCMTool.dpToPx(10),SCMTool.dpToPx(10),SCMTool.dpToPx(10),SCMTool.dpToPx(10));
        textView.setTextColor(ContextCompat.getColor(mContext,R.color.black));
        container.addView(textView);
    }



    private void CreateChildPosition(ViewGroup parent, LinearLayout container, Schedule_Business_Site_Plan sched, User user){

        View view = LayoutInflater.from(mContext).inflate(R.layout.daily_sched_child_list_data, parent, false);
        TextView Txtposition = view.findViewById(R.id.position);
        TextView start_time = view.findViewById(R.id.start_time);
        TextView end_time = view.findViewById(R.id.end_time);
        TextView name = view.findViewById(R.id.name);
        TextView daypart = view.findViewById(R.id.daypart);

        SimpleDateFormat format = new SimpleDateFormat("hh:mm a");

        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        if (sched.startTime!=null){
            start_time.append(format.format(sched.startTime));
        }
        if (sched.endTime!=null){
            end_time.append(format.format(sched.endTime));
        }
        name.setText(SCMTool.CheckString(user.getFullName(),"--"));

        Txtposition.setText(SCMTool.CheckString(String.valueOf(sched.position),"--"));

        findPositonColorInKitchenLayout(Txtposition,sched.position);

//        daypart.setText(CheckDaypart(position.timeIn,false));
//        daypart.append(" - ");
//        daypart.append(CheckDaypart(position.timeOut,true));

        container.addView(view);

    }



    private void findPositonColorInKitchenLayout(TextView holder,int position){
        try {
            DashboardKitchenData dashboardKitchenData = SCMDataManager.getInstance().getDashboardKitchenData();
            ArrayList<PositionDataItems> positionDataItems = dashboardKitchenData.positionDataItems;
            if (positionDataItems!=null){

                for (PositionDataItems dataItems : positionDataItems){
                    if (position == dataItems.position){
                        CreatePositionBackground(holder,"#"+dataItems.color);
                        break;
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private TextView  CreatePositionBackground(TextView imageView, String backgroundColor) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setCornerRadii(new float[] { SCMTool.dpToPx(8), SCMTool.dpToPx(8), SCMTool.dpToPx(8), SCMTool.dpToPx(8), SCMTool.dpToPx(8), SCMTool.dpToPx(8), SCMTool.dpToPx(8), SCMTool.dpToPx(8) });
        shape.setColor(Color.parseColor(backgroundColor));
        imageView.setBackground(shape);
        return imageView;
    }

    private String CheckDaypart(Date selectedDate, boolean isEndtime){

        String daypartname = "UNIDENTIFIED";
        Schema schema = Schema.getInstance();
        Calendar currentDate = Calendar.getInstance();
        currentDate.clear();
        currentDate.setTimeZone(TimeZone.getTimeZone("UTC"));
        currentDate.setTime(selectedDate);
        currentDate.set(Calendar.SECOND,0);

        if (isEndtime){
            currentDate.add(Calendar.MINUTE,-1);
        }

        DateFormat format = new SimpleDateFormat("EEE MMM dd, yyyy hh:mm a");

        for(Dayparts daypart: schema.getDayparts()){

            Calendar start = SCMTool.setCalendarDaypart(currentDate.getTime(),daypart.start);
            start.set(Calendar.DATE,currentDate.get(Calendar.DATE));

            Calendar end = SCMTool.setCalendarDaypart(currentDate.getTime(),daypart.end);
            end.set(Calendar.DATE,currentDate.get(Calendar.DATE));
            end.add(Calendar.MINUTE,-1);

            if (daypart.name.equalsIgnoreCase("6")){
                end.add(Calendar.DATE,1);
            }

            System.out.println(daypart.title);
            System.out.println("START DATE  : "+format.format(start.getTime()));
            System.out.println("END DATE  : "+format.format(end.getTime()));

            if (daypart.name.equalsIgnoreCase("1")){
                //BEFORE TO CURRENT DATE
                if (currentDate.compareTo(start)<0){
                    return "Daypart 6";
                }
                // AFTER OR EQUAL TO CURRENT DATE
                else if (currentDate.compareTo(start)>=0){
                    //BEFORE OR EQUAL
                    if (currentDate.compareTo(end)<=0){
                        return "Daypart "+daypart.name;
                    }
                }
            }else {
                // AFTER OR EQUAL TO CURRENT DATE
                 if (currentDate.compareTo(start)>=0){
                    //BEFORE OR EQUAL TO CURRENT DATE
                    if (currentDate.compareTo(end)<=0){
                        return "Daypart "+daypart.name;
                    }
                }
            }
        }
        return daypartname;
    }


    public static boolean isBetween(final Calendar min, final Calendar max, final Calendar date){
        boolean isbetween =false;
        if (date.after(min) || date.equals(min)){
            if (date.before(max) || date.equals(max)){
                isbetween =true;
            }

        }
        return isbetween;
    }

    private ArrayList<String>getPositionSkills(){
        ArrayList<String>skills = new ArrayList<>();
        ArrayList<CertificationDefinition>certifications = SCMDataManager.getInstance().getCertificationDefinitions();
        if (certifications!=null){
            for (CertificationDefinition certification : certifications){
                if (certification.category.equalsIgnoreCase("0")){
                    skills.add(certification.certification);
                }
            }
        }
        return skills;
    }

    private void setUpEmployeeReport(final CustomUser user, View view, final Date date){
        try {
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int Employee_Schedule_Report = 0;
                    int Employee_Schedule_Summary = 1;

                    Site site = SCMDataManager.getInstance().getSelectedSite().getSite();
//
//                    Calendar currentDate = Calendar.getInstance();
//                    currentDate = SCMTool.getDateWithDaypart(currentDate.getTime().getTime(),site.effectiveUTCOffset);

                    EmployeeReportData employeeReportData = new EmployeeReportData();
                    employeeReportData.setReportType(Employee_Schedule_Report);
                    employeeReportData.setPositionForm(SCMDataManager.getInstance().getPositionForm());
                    employeeReportData.setEmployee_Schedule_Report_Date(date);
                    employeeReportData.setUser(user);
                    employeeReportData.setUsers(SCMDataManager.getInstance().getStoreUsers());
                    employeeReportData.setSite(site);
                    employeeReportData.setSPID(SCMDataManager.getInstance().getSPID());
                    EmployeeReportData.setInstance(employeeReportData);

                    Intent intent = new Intent(fragmentActivity, EmployeeReportActivity.class);
                    fragmentActivity.startActivity(intent);
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @Override
    public boolean hasStableIds() {
        return super.hasStableIds();
    }
}

