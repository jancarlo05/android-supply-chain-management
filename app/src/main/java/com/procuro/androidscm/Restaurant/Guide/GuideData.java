package com.procuro.androidscm.Restaurant.Guide;

public class GuideData {

    private static GuideData instance = new GuideData();

    private String title;
    private Integer selected;

    public static GuideData getInstance() {
        if(instance == null) {
            instance = new GuideData();
        }
        return instance;
    }

    public static void setInstance(GuideData instance) {
        GuideData.instance = instance;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getSelected() {
        return selected;
    }

    public void setSelected(Integer selected) {
        this.selected = selected;
    }
}
