package com.procuro.androidscm.Restaurant.DashBoard.Dashbord_VOC.TeamLeaders;

import android.content.Context;
import android.content.res.Resources;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;


import java.text.SimpleDateFormat;
import java.util.ArrayList;




public class Dashboard_Reward_RecyclerViewAdapter extends RecyclerView.Adapter<Dashboard_Reward_RecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<CustomUser> arraylist;
    private FragmentActivity fragmentActivity;
    private Dashboard_Reward_Skills_RecyclerViewAdapter skill_adapter;
    private Dashboard_Reward_Stickers_RecyclerViewAdapter sticker_adapter;

    public Dashboard_Reward_RecyclerViewAdapter(Context context, ArrayList<CustomUser> arraylist, FragmentActivity fragmentActivity) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;
        this.fragmentActivity = fragmentActivity;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.voc_rewards_cardview,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final CustomUser employee = arraylist.get(position);
        SimpleDateFormat format = new SimpleDateFormat("EEE MM/dd/yyyy hh:mm a");

        holder.name.setText(employee.getUser().firstName);
        holder.name.append(" ");
        holder.name.append(employee.getUser().lastName);
        holder.rank.setText(String.valueOf((position+1)));

        if (employee.getUser().awardList!=null){
            if (employee.getUser().awardList.size()>0){
                holder.last_reward.setText(format.format(employee.getUser().awardList.get(employee.getUser().awardList.size()-1).timestamp));
            }
        }

        if (employee.getUser().old_certificationList !=null){
            if (employee.getUser().old_certificationList.size()>0){
                skill_adapter = new Dashboard_Reward_Skills_RecyclerViewAdapter(mContext,employee.getUser().old_certificationList);
                holder.skills.setLayoutManager(new GridLayoutManager(mContext,9));
                holder.skills.setAdapter(skill_adapter);
                holder.skill_message.setVisibility(View.GONE);
            }
        }

        if (employee.getUser().awardList!=null){
            if (employee.getUser().awardList.size()>0){
                sticker_adapter = new Dashboard_Reward_Stickers_RecyclerViewAdapter(mContext,employee.getUser().awardList.size());
                holder.rewards.setLayoutManager(new GridLayoutManager(mContext,9));
                holder.rewards.setAdapter(sticker_adapter);
                holder.reward_message.setVisibility(View.GONE);
            }
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayHistoryMessage(employee);
            }
        });

        holder.skills.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayHistoryMessage(employee);
            }
        });

        holder.rewards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayHistoryMessage(employee);
            }
        });


    }

    @Override
    public int getItemCount() {
        return arraylist.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name,rank,last_reward,skill_message,reward_message;
        RecyclerView skills,rewards;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
             name = itemView.findViewById(R.id.name);
             rank = itemView.findViewById(R.id.rank);
             last_reward = itemView.findViewById(R.id.last_reward);
             skill_message = itemView.findViewById(R.id.skill_message);
             reward_message = itemView.findViewById(R.id.awards_message);;
             skills = itemView.findViewById(R.id.skills);
             rewards = itemView.findViewById(R.id.awards);
             cardView = itemView.findViewById(R.id.cardview_id);

        }
    }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    private void DisplayHistoryMessage(CustomUser user) {
        DialogFragment newFragment = Dashboard_Reward_History_DialogFragment.newInstance(user);
        assert fragmentActivity.getFragmentManager() != null;
        newFragment.show(fragmentActivity.getSupportFragmentManager(), "dialog");
    }



}
