package com.procuro.androidscm.Restaurant.Status.StatusFragment;

import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Area;

import java.util.ArrayList;

public class StatusChain {

    private String name ;
    private ArrayList<CustomSite>customSites;
    private ArrayList<Corp_Area>corp_areas;


    public StatusChain(String name) {
        this.name = name;
    }

    public StatusChain() {

    }


    public StatusChain(String name, ArrayList<CustomSite> customSites) {
        this.name = name;
        this.customSites = customSites;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<CustomSite> getCustomSites() {
        return customSites;
    }

    public void setCustomSites(ArrayList<CustomSite> customSites) {
        this.customSites = customSites;
    }
}
