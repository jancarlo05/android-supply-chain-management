package com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourceManualFragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourcesGrandChildList;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.Resources_left_pane_fragment;
import com.procuro.androidscm.Restaurant.QuickAccessMenu.QuickAccessMenu;
import com.procuro.androidscm.UnderMaintenance_DialogFragment;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import es.voghdev.pdfviewpager.library.RemotePDFViewPager;
import es.voghdev.pdfviewpager.library.adapter.PDFPagerAdapter;
import es.voghdev.pdfviewpager.library.remote.DownloadFile;
import es.voghdev.pdfviewpager.library.util.FileUtil;

public class Resources_right_pane_fragment extends Fragment  implements DownloadFile.Listener  {


    private ResourcesGrandChildList grandChildList;
    private ProgressBar progressBar;

    private TextView date , wait , title;
    private Button back,quick_a_menu,sos,Covid;
    private RemotePDFViewPager remotePDFViewPager;
    private PDFPagerAdapter adapter;
    private LinearLayout root;
    private ConstraintLayout pdf_container;

    com.procuro.apimmdatamanagerlib.aPimmDataManager dataManager;

    public Resources_right_pane_fragment(ResourcesGrandChildList grandChildList) {
        this.grandChildList = grandChildList;
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.resource_right_pane_fragment, container, false);

        title = rootView.findViewById(R.id.username);
        back = rootView.findViewById(R.id.home);
        title.setText(grandChildList.getDocumentDTO().name);

        root = (LinearLayout) rootView.findViewById(R.id.root);
        progressBar = rootView.findViewById(R.id.progressBar2);
        wait = rootView.findViewById(R.id.wait);
        pdf_container = rootView.findViewById(R.id.pdf_container);
        quick_a_menu = rootView.findViewById(R.id.quick_a_menu);
        Covid = rootView.findViewById(R.id.covid);



        SetupOnclicks();

        setDownloadButtonListener();

        return rootView;
    }



    protected void setDownloadButtonListener() {
        final Context ctx = getContext();
        final DownloadFile.Listener listener = this;

        dataManager = aPimmDataManager.getInstance();
        String[] strings = dataManager.getAuthHeaderValue().split("Basic ");

        StringBuilder urlString = new StringBuilder();
        urlString.append(dataManager.getBaseUrl());
        urlString.append("Document/Document/GetContent/");
        urlString.append("?documentId=");
        urlString.append(grandChildList.getDocumentDTO().documentId);
        urlString.append("&authorization=").append(strings[1]);

        System.out.println(urlString.toString());

        try {
            remotePDFViewPager = new RemotePDFViewPager(ctx, urlString.toString(), listener);
            remotePDFViewPager.setId(R.id.pdfViewPager);
        }catch (Exception e){
            progressBar.setVisibility(View.INVISIBLE);
            wait.setText("NO PREVIEW AVAILABLE");
        }



    }

    public static void open(Context context) {
        Intent i = new Intent(context, Resources_right_pane_fragment.class);
        context.startActivity(i);

    }

    public void updateLayout() {
        wait.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        pdf_container.addView(remotePDFViewPager,
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);


    }

    @Override
    public void onSuccess(String url, String destinationPath) {
        try {
            adapter = new PDFPagerAdapter(getContext(), FileUtil.extractFileNameFromURL(url));
            remotePDFViewPager.setAdapter(adapter);
            updateLayout();
        }catch (Exception e){
            progressBar.setVisibility(View.INVISIBLE);
            wait.setText("NO PREVIEW AVAILABLE");
        }
    }

    @Override
    public void onFailure(Exception e) {
        e.printStackTrace();
        progressBar.setVisibility(View.INVISIBLE);
        wait.setText("NO PREVIEW AVAILABLE");

    }

    @Override
    public void onProgressUpdate(int progress, int total) {
        System.out.println("PROGRESS: "+progress+"/"+total);

    }


    private void SetupOnclicks(){

        Covid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuickAccessMenu.DisplayCovidFragment(getActivity());
            }
        });

        quick_a_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuickAccessMenu.DisplayQuickAMenu(getContext(),quick_a_menu,getActivity());
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back();
            }
        });

    }

    private void Back() {
        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_right,R.anim.slide_right_out)
                .replace(R.id.resturant_fragment_container,
                        new Resources_left_pane_fragment(R.id.resource_manual)).commit();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (adapter != null) {
            adapter.close();
        }
    }

    private void DisplayUnderMaintenanceMessage() {
        DialogFragment newFragment = UnderMaintenance_DialogFragment.newInstance();
        assert getFragmentManager() != null;
        newFragment.show(getActivity().getSupportFragmentManager(), "dialog");
    }
    
}