package com.procuro.androidscm.Restaurant.Schema;

import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.FormsList.CustomSiteSettingsConfiguration;
import com.procuro.apimmdatamanagerlib.JSONDate;
import com.procuro.apimmdatamanagerlib.PimmBaseObject;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsConfiguration;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class Forms extends PimmBaseObject {
    public String formName;
    public String title;
    public ArrayList<FormItems>items;
    public ArrayList<String>dayparts;

    //Custom Property For Formslist
    public CustomSiteSettingsConfiguration config;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("items")){
            ArrayList<FormItems>formItems = new ArrayList<>();
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    JSONArray array = (JSONArray) value;
                        try {
                            for (int i = 0; i <array.length() ; i++) {
                                FormItems item = new FormItems();
                                item.readFromJSONObject((array.getJSONObject(i)));
                                formItems.add(item);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                }
            }
            this.items = formItems;
        }
    }

    public CustomSiteSettingsConfiguration getConfig() {
        return config;
    }

    public void setConfig(CustomSiteSettingsConfiguration config) {
        this.config = config;
    }

    public ArrayList<String> getDayparts() {
        return dayparts;
    }

    public void setDayparts(ArrayList<String> dayparts) {
        this.dayparts = dayparts;
    }
}

