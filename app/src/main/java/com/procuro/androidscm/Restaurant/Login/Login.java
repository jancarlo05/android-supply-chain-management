package com.procuro.androidscm.Restaurant.Login;

import androidx.annotation.Keep;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.procuro.androidscm.GlideApp;
import com.procuro.androidscm.HomePage;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.Connection_Error_DialogFragment;
import com.procuro.androidscm.Restaurant.Login_Error_DialogFragment;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.Schema.Actions;
import com.procuro.androidscm.Restaurant.Schema.Dayparts;
import com.procuro.androidscm.Restaurant.Schema.Days;
import com.procuro.androidscm.Restaurant.Schema.Forms;
import com.procuro.androidscm.Restaurant.Schema.Schema;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.StoreListFragment.SiteList;
import com.procuro.androidscm.Restaurant.Restaurant_home_page;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.CertificationDefinition;
import com.procuro.apimmdatamanagerlib.CorpStructure;
import com.procuro.apimmdatamanagerlib.FormPack;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.PimmDevice;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Objects;
import java.util.TimeZone;
import java.util.UUID;

@Keep
public class Login extends AppCompatActivity  {

    private ConstraintLayout loginBackground;
    private LinearLayout spidContainer;
    private EditText txtPassword, txtUsername, txtSPID;
    private Button btnLogin;
    private ImageView customerLogo,background_image,logo_up_right,emblem1,emblem2,emblem3,emblem4;
    private View.OnTouchListener onTouchListener;
    private View.OnClickListener onClickListener;
    public static com.procuro.apimmdatamanagerlib.aPimmDataManager dataManager;
    private ProgressDialog progressDialog = null;
    private CheckBox rememberme;


    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};

    private static String[] Camera = {
            Manifest.permission.CAMERA};



    public  static String SCM_FILE_FOLDER = ".SCMFiles";

    public static String root = Environment.getExternalStorageDirectory().toString();


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.login);

        spidContainer = findViewById(R.id.spid_container);
        customerLogo = findViewById(R.id.customerLogo);
        loginBackground = findViewById(R.id.root);
        txtSPID = findViewById(R.id.spid);
        txtUsername = findViewById(R.id.username);
        txtPassword = findViewById(R.id.password);
        btnLogin = findViewById(R.id.login);
        rememberme = findViewById(R.id.checkBox);
        background_image = findViewById(R.id.background_image);
        logo_up_right = findViewById(R.id.pimmlogo_up_right);

        emblem1 = findViewById(R.id.emblem1);
        emblem2 = findViewById(R.id.emblem2);
        emblem3 = findViewById(R.id.emblem3);
        emblem4 = findViewById(R.id.emblem4);

        DisplayLoginImages();

        setUpOnclicks();

        getSharedPref();

        verifyStoragePermissions(this);

    }


    @SuppressLint("ClickableViewAccessibility")
    private void setUpOnclicks(){
        this.setEventListeners();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        txtUsername.setOnTouchListener(onTouchListener);
        txtPassword.setOnTouchListener(onTouchListener);
        txtUsername.setOnClickListener(onClickListener);
        txtPassword.setOnClickListener(onClickListener);
        txtSPID.setOnTouchListener(onTouchListener);
        txtSPID.setOnTouchListener(onTouchListener);

        txtPassword.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    hide();
                    login();
                    return true;
                }
                return false;
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hide();
                login();

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void setEventListeners() {
        onTouchListener = new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                return false;
            }
        };

        onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            }
        };
    }

    public void login() {
        btnLogin.setEnabled(false);
        btnLogin.setAlpha(.5f);
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please Wait");
        progressDialog.setMessage("Checking Connectivity");
        progressDialog.setCancelable(false);
        progressDialog.show();

        if (isConnectedToInternet()) {
            progressDialog.setMessage("Logging-in");
            dataManager = aPimmDataManager.getInstance();
            dataManager.initializeWithSPID(txtSPID.getText().toString());
            dataManager.loginWithUsernameAndPassword(txtUsername.getText().toString(), txtPassword.getText().toString(), new OnCompleteListeners.OnLoginCompleteListener() {
                @Override
                public void onLoginComplete(final User user, Error error) {
                    if (error == null) {
                        SCMDataManager.getInstance().setUser(user);
                        SCMDataManager.getInstance().setSPID(txtSPID.getText().toString());
                        SCMDataManager.getInstance().setUsername(txtUsername.getText().toString());
                        SCMDataManager.getInstance().setPassword(txtPassword.getText().toString());
                        SCMDataManager.getInstance().setRemembeMe(rememberme.isChecked());
                        CheckUserAccount(user);

                    } else {
                        progressDialog.dismiss();
                        DisplayLoginError();
                    }
                }
            });
        } else {
            progressDialog.dismiss();
            btnLogin.setEnabled(true);
            btnLogin.setAlpha(1);
            DisplayInternetConnectionError();
        }
    }

    public void hide() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void setSharedpref(final ProgressDialog progressDialog , final Activity activity) {

        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(activity);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String scmdatamanager = gson.toJson(SCMDataManager.getInstance());
        String schema = gson.toJson(Schema.getInstance());
        prefsEditor.putString("SCMDataManager", scmdatamanager);
        prefsEditor.putString("Schema", schema);
        prefsEditor.apply();

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(activity, Restaurant_home_page.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                activity.startActivity(intent);
                progressDialog.dismiss();

            }
        });

    }

    private void getSharedPref() {
        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        Gson gson = new Gson();
        String dataManager = mPrefs.getString("SCMDataManager", "");
        String Schema = mPrefs.getString("Schema", "");

        Schema schemaObj = gson.fromJson(Schema, Schema.class);
        SCMDataManager obj = gson.fromJson(dataManager, SCMDataManager.class);
        SCMDataManager scmDataManager = new SCMDataManager();
        SCMDataManager.setInstance(scmDataManager);


        File destDir = new File(root, SCM_FILE_FOLDER);

        if (obj != null) {
            SCMDataManager.getInstance().setSPID(obj.getSPID());
            SCMDataManager.getInstance().setUsername(obj.getUsername());
            SCMDataManager.getInstance().setPassword(obj.getPassword());
            SCMDataManager.getInstance().setSelectedSite(obj.getSelectedSite());
            SCMDataManager.getInstance().setRemembeMe(obj.isRemembeMe());


            if (obj.getSPID() != null) {
               boolean Exist =  CheckFileIfExist(destDir);
               if (Exist){
                   SCMDataManager.getInstance().setFormpackCurrentVersion(obj.getFormpackCurrentVersion());
                   com.procuro.androidscm.Restaurant.Schema.Schema.setInstance(schemaObj);
               }

                spidContainer.setVisibility(View.GONE);
                if (SCMDataManager.getInstance().getSPID().equalsIgnoreCase("wendys")) {

                    btnLogin.setBackgroundResource(R.drawable.rounded_orange_bg);
                    txtSPID.setText(SCMDataManager.getInstance().getSPID());

                    GlideApp.with(this).asDrawable().fitCenter().load(R.drawable.wendys_new_conver).into(background_image);
                    GlideApp.with(this).asDrawable().load(R.drawable.wendys_delight_logo).fitCenter().into(customerLogo);


                }
                if (SCMDataManager.getInstance().isRemembeMe()) {

                    txtUsername.setText(SCMDataManager.getInstance().getUsername());
                    txtPassword.setText(SCMDataManager.getInstance().getPassword());
                    rememberme.setChecked(true);

                } else {
                    txtUsername.setText("");
                    txtPassword.setText("");
                    rememberme.setChecked(false);

                }
            }
            else {
                DeleteFileIFexist();
                GlideApp.with(this).asDrawable().load(R.drawable.new_master_bg).diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                        .skipMemoryCache(true).fitCenter().into(background_image);
                GlideApp.with(this).asDrawable().load(R.drawable.customer_logo_pimm).diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                        .skipMemoryCache(true).into(customerLogo);
                btnLogin.setBackgroundResource(R.drawable.rounded_black_bg);

            }
        } else {

            GlideApp.with(this).asDrawable().load(R.drawable.new_master_bg).diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .skipMemoryCache(true).fitCenter().into(background_image);
            GlideApp.with(this).asDrawable().load(R.drawable.customer_logo_pimm).diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                    .skipMemoryCache(true).into(customerLogo);
            btnLogin.setBackgroundResource(R.drawable.rounded_black_bg);

        }
    }

    public void DeleteFileIFexist() {
        File file = new File(root, SCM_FILE_FOLDER);
        if (file.exists()){
            SCMTool.purgeDirectory(file);
            file.delete();
        }
    }

    private boolean CheckFileIfExist(File file){
        boolean ifexist = false;
        if (file.exists()){
            if (file.list().length > 0){
                ifexist =true;
            }
        }
        return ifexist;
    }

    private void DisplayLoginImages() {

        GlideApp.with(this).asDrawable().load(R.drawable.pimm_new_logo_march).diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .skipMemoryCache(true).into(logo_up_right);

        GlideApp.with(this).asDrawable().load(R.drawable.login_scm_quality_emblem_a).diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .skipMemoryCache(true).into(emblem1);

        GlideApp.with(this).asDrawable().load(R.drawable.login_scm_quality_emblem_usda).diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .skipMemoryCache(true).into(emblem2);

        GlideApp.with(this).asDrawable().load(R.drawable.login_scm_quality_emblem_fda).diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .skipMemoryCache(true).into(emblem3);

        GlideApp.with(this).asDrawable().load(R.drawable.login_scm_quality_emblem_haccp).diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .skipMemoryCache(true).into(emblem4);

    }


    public static void PopulateCustomSite(final ArrayList<Site> sites, ArrayList<PimmDevice>
            pimmDevices,ProgressDialog progressDialog,Activity activity,boolean isAdmin) {
        ArrayList<CustomSite> customSites = new ArrayList<>();

        try {
            int counter = 0;
            for (Site site : sites) {
                counter++;
                customSites.add(new CustomSite(site));
                progressDialog.setMessage("Setup Site : "+counter+"/"+sites.size());
            }
        }catch (Exception e){
            e.printStackTrace();
        }

//        try {
//
//            for (CustomSite customSite : customSites) {
//                if (pimmDevices!=null){
//                for (PimmDevice pimmDevice : pimmDevices) {
//                    if (customSite.getSite().siteid.equalsIgnoreCase(pimmDevice.siteID)) {
//                        customSite.setPimmDevice(pimmDevice);
//                    }
//                }
//                }
//
//            }
//
//        }catch (Exception e){
//            e.printStackTrace();
//        }

        try {
            PopulateStoreList(customSites,progressDialog,activity,isAdmin);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void PopulateStoreList(final ArrayList<CustomSite> customSites,
                                         final ProgressDialog progressDialog,Activity activity,boolean isAdmin) throws Exception {

        Download_getWithStructure(progressDialog,activity,isAdmin);


        final ArrayList<SiteList> siteLists = new ArrayList<>();
        ArrayList<String> province = new ArrayList<>();
        try {
            System.out.println("PopulateStoreList >> siteLists ");
            for (CustomSite customSite : customSites) {

                if (SCMDataManager.getInstance().getUser().roles!=null){
                    if (SCMDataManager.getInstance().getUser().roles.contains(customSite.getSite().sitename)){
                        if (province.contains(customSite.getSite().Address.Province)) {
                            for (SiteList siteList1 : siteLists) {
                                if (siteList1.getProvince().equalsIgnoreCase(customSite.getSite().Address.Province)) {
                                    ArrayList<CustomSite> siteArrayList = siteList1.getCustomSites();
                                    siteArrayList.add(customSite);
                                }
                            }
                        }
                        else {
                            ArrayList<CustomSite> siteArrayList = new ArrayList<>();
                            province.add(customSite.getSite().Address.Province);
                            siteArrayList.add(customSite);
                            siteLists.add(new SiteList(customSite.getSite().Address.Province, siteArrayList));
                        }
                    }
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        try {
            System.out.println("Collections");
            Collections.sort(siteLists, new Comparator<SiteList>() {
                @Override
                public int compare(SiteList o1, SiteList o2) {
                    return o1.getProvince().compareToIgnoreCase(o2.getProvince());
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

        try {
            SCMDataManager.getInstance().setCustomSites(customSites);
            SCMDataManager.getInstance().setSiteLists(siteLists);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void DisplayLoginError() {
        DialogFragment newFragment = Login_Error_DialogFragment.newInstance(btnLogin);
        assert getFragmentManager() != null;
        newFragment.show(getSupportFragmentManager(), "dialog");
    }

    private void DisplayInternetConnectionError() {
        DialogFragment newFragment = Connection_Error_DialogFragment.newInstance(btnLogin);
        assert getFragmentManager() != null;
        newFragment.show(getSupportFragmentManager(), "dialog");
    }

    private boolean isConnectedToInternet() {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        if (Objects.requireNonNull(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }

        return connected;
    }
    

    private void CheckUserAccount(User user){

        Download_getCertificationDefinitionsWithCallback();

        if (user.roles.contains("CORP HQ")){
            Intent intent = new Intent(Login.this, HomePage.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            progressDialog.dismiss();
            finish();
        }else {

        progressDialog.setMessage("Downloading Site Information");
        try {
            Download_getSiteListForLoggedInUser(progressDialog,Login.this,false);

        }catch (Exception e){
            e.printStackTrace();
        }
//
        }
    }

    private void Download_getCertificationDefinitionsWithCallback(){
        dataManager.getCertificationDefinitionsWithCallback(new OnCompleteListeners.getCertificationDefinitionsWithCallbackListener() {
            @Override
            public void getCertificationDefinitionsWithCallback(ArrayList<CertificationDefinition> certificationDefinitions, Error error) {
                if (error == null){
                    SCMDataManager.getInstance().setCertificationDefinitions(certificationDefinitions);
                }else {
                    progressDialog.setTitle("Connection Error");
                    progressDialog.setMessage("Trying to reconnect");
                    Download_getCertificationDefinitionsWithCallback();
                }
            }
        });
    }

    public static void Download_getFacilityStatusForSiteClass(final ArrayList<Site>siteList,ProgressDialog progressDialog,
                                                              Activity activity,boolean isAdmin){
//        dataManager.getFacilityStatusForSiteClass(null, 50, null, new OnCompleteListeners.getFacilityStatusForSiteClassListener() {
//            @Override
//            public void getFacilityStatusForSiteClass(ArrayList<PimmDevice> pimmDeviceList, Error error) {
//                if (error == null) {
//                    PopulateCustomSite(siteList, pimmDeviceList);
//                } else {
//                    progressDialog.setTitle("Connection Error");
//                    progressDialog.setMessage("Trying to reconnect");
//                    PopulateCustomSite(siteList, pimmDeviceList);
//                }
//            }
//        });
        PopulateCustomSite(siteList, null,progressDialog,activity,isAdmin);
    }
    
    public static void Download_getSiteListForLoggedInUser(final ProgressDialog progressDialog,
                                                           final Activity activity, final boolean isAdmin){
        dataManager.getSiteListForLoggedInUser(new OnCompleteListeners.getSiteListForLoggedInUserWithCallbackListener() {
            @Override
            public void getSiteListForLoggedInUserWithCallback(final ArrayList<Site> siteList, Error error) {
                if (error == null) {
                    Download_getFacilityStatusForSiteClass(siteList,progressDialog,activity,isAdmin);
                } else {
                    progressDialog.setTitle("Connection Error");
                    progressDialog.setMessage("Trying to reconnect");
                    Download_getSiteListForLoggedInUser(progressDialog,activity,isAdmin);
                }
            }
        });
    }

    public static void Download_getWithStructure(final ProgressDialog progressDialog, final Activity activity,boolean isAdmin){
        if (!isAdmin){
            try {
                progressDialog.setTitle("Please Wait");
                progressDialog.setMessage("Preparing Application Settings");
                dataManager.getWithStructure(new OnCompleteListeners.getWithStructureCallbackListener() {
                    @Override
                    public void getWithStructureWithCallback(final ArrayList<CorpStructure> corpStructures, JSONArray siteObject, Error error) {
                        if (error == null){
                            SCMDataManager.getInstance().setCorpStructures(corpStructures);

                            for (CustomSite customSite : SCMDataManager.getInstance().getCustomSites()){
                                for (int i = 0; i < siteObject.length(); i++) {
                                    try {
                                        JSONObject jsonObject = siteObject.getJSONObject(i);
                                        String SiteID = jsonObject.getString("SiteID");

                                        if (customSite.getSite().siteid.equalsIgnoreCase(SiteID)) {
                                            customSite.getSite().readFromJSONObject(jsonObject);
                                            customSite.setMerged(true);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            CheckForLatestForm(progressDialog,activity);

                            new LoginBackGroundProcess(corpStructures,siteObject,activity).execute();

                        }else {
                            CheckForLatestForm(progressDialog,activity);
                            new LoginBackGroundProcess(corpStructures,siteObject,activity).execute();
                        }
                    }
                });
            }catch (Exception e){
                e.printStackTrace();
                CheckForLatestForm(progressDialog,activity);
                new LoginBackGroundProcess(null,null,activity).execute();
            }
        }else {
            CheckForLatestForm(progressDialog,activity);
            new LoginBackGroundProcess(null,null,activity).execute();
        }


    }
    public static void CheckForLatestForm(final ProgressDialog progressDialog, final Activity activity){

        //GET LIST OF FORM PACKS AND SORT ACCORDING TO THE LATEST VERSION

        final SCMDataManager data = SCMDataManager.getInstance();
        final String[] VersionName = {""};

        aPimmDataManager dataManager = aPimmDataManager.getInstance();
        dataManager.listAllFormpackWithName(data.getSPID(), new OnCompleteListeners.listAllFormpackWithNameListener() {
            @Override
            public void listAllFormpackWithNameCallback(ArrayList<FormPack> formPackslist, Error error) {
                if (error == null){
                    ArrayList<FormPack>newFormpack = new ArrayList<>();


                    for (FormPack formPack : formPackslist){
                        if (formPack.formpack.equalsIgnoreCase(data.getSPID())) {
                            if (formPack.startDate!=null){
                                newFormpack.add(formPack);
                            }
                        }
                    }
                    Collections.sort(newFormpack, new Comparator<FormPack>() {
                        @Override
                        public int compare(FormPack o1, FormPack o2) {
                            if (o1.startDate != null && o2.startDate != null)
                                return o1.startDate.compareTo(o2.startDate);
                            return (o1.startDate == null) ? 1 : -1;
                        }
                    });

                    if (newFormpack.size()>0){
                        VersionName[0] = newFormpack.get(0).fileName;
                        CheckFormIfExist(VersionName[0],progressDialog,activity);
                    }
                }
            }
        });

    }

    public static void CheckFormIfExist(String FormpackVersionName,ProgressDialog progressDialog,Activity activity){
        try {
            File destDir = new File(root, SCM_FILE_FOLDER);
            SCMDataManager data = SCMDataManager.getInstance();

            System.out.println("LATEST FORM : "+FormpackVersionName);
            System.out.println("OLD FORM : "+data.getFormpackCurrentVersion());

            //CHECK FORM PACK IF EXIST
            if (data.getFormpackCurrentVersion() == null) {
                if (!destDir.exists()) {
                    destDir.mkdir();
                    destDir.setWritable(true);
                    destDir.setReadable(true);
                    destDir.setExecutable(true);
                    data.setFormpackCurrentVersion(FormpackVersionName);
                    DownloadFormpack(progressDialog,activity);
                }else {
                    data.setFormpackCurrentVersion(FormpackVersionName);
                    DownloadFormpack(progressDialog,activity);
                    System.out.println("EXIST NEED TO DELETE FORM BEFORE DOWNLOADING");
                }
            }
            else {
                //CHECK FOR UPDATES
                System.out.println("LATEST FORM : "+FormpackVersionName);
                System.out.println("OLD FORM : "+data.getFormpackCurrentVersion());
                if (FormpackVersionName!=null){
                    if (data.getFormpackCurrentVersion()!=null){
                        if (!data.getFormpackCurrentVersion().equalsIgnoreCase(FormpackVersionName)){
                                data.setFormpackCurrentVersion(FormpackVersionName);
                            progressDialog.setMessage("Deleting Previous Forms");
                            DownloadFormpack(progressDialog,activity);

                        }else {
                            ParseSchema(progressDialog,activity);
                        }
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public static void FindSchema(ProgressDialog progressDialog , Activity activity){
        try {
            progressDialog.setMessage("Parsing Schema");
            File destDir = new File(root, SCM_FILE_FOLDER);

            if (destDir.exists()) {
                for (File file : Objects.requireNonNull(destDir.listFiles())){
                    if (file.isDirectory()){
                        for (File childfile : Objects.requireNonNull(file.listFiles())){
                            if (childfile.getName().contains("schema")){
                                try {
                                    InputStream inputStream = new FileInputStream(childfile);
                                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                                    int ctr;

                                    ctr = inputStream.read();
                                    while (ctr != -1) {
                                        byteArrayOutputStream.write(ctr);
                                        ctr = inputStream.read();
                                    }
                                    inputStream.close();


                                    JSONObject jObject = null;
                                    try {
                                        jObject = new JSONObject(
                                                byteArrayOutputStream.toString());
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    Schema.getInstance().setJsonSchema(byteArrayOutputStream.toString());

                                    ParseSchemaForms(jObject);

                                    ParseSchemaDayparts(jObject);

                                    ParseSchemaDays(jObject);

                                    ParseSchemaActions(jObject);

                                    setSharedpref(progressDialog,activity);

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                break;
                            }
                        }
                    }
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public static void ParseSchema(ProgressDialog progressDialog,Activity activity){
        try {

            JSONObject jsonObject = new JSONObject(Schema.getInstance().getJsonSchema());

            ParseSchemaForms(jsonObject);

            ParseSchemaDayparts(jsonObject);

            ParseSchemaDays(jsonObject);

            ParseSchemaActions(jsonObject);

            setSharedpref(progressDialog,activity);


        }catch (Exception e){

        }

    }

    public static void ParseSchemaForms(JSONObject jsonObject){

        try {
            ArrayList<Forms>forms = new ArrayList<>();
            JSONArray forms_aray = jsonObject.getJSONArray("forms");

            for (int i = 0; i <forms_aray.length() ; i++) {
                Forms form = new Forms();
                form.readFromJSONObject(forms_aray.getJSONObject(i));
                forms.add(form);
            }
            Schema.getInstance().setForms(forms);
        }catch (Exception e){
            e.printStackTrace();
        }



    }

    public static void ParseSchemaDayparts(JSONObject jsonObject) {

        try {
            ArrayList<Dayparts> dayparts = new ArrayList<>();
            JSONArray forms_aray = jsonObject.getJSONArray("dayparts");

            for (int i = 0; i < forms_aray.length(); i++) {
                Dayparts item = new Dayparts();
                item.readFromJSONObject(forms_aray.getJSONObject(i));

                dayparts.add(item);
            }
            Schema.getInstance().setDayparts(dayparts);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void ParseSchemaDays(JSONObject jsonObject){

        try {
            ArrayList<Days>days = new ArrayList<>();
            JSONArray forms_aray = jsonObject.getJSONArray("days");
            for (int i = 0; i <forms_aray.length() ; i++) {
                Days day = new Days();
                day.readFromJSONObject(forms_aray.getJSONObject(i));

                days.add(day);
            }
            Schema.getInstance().setDays(days);
            getFormsInCurrentDay();
        }catch (Exception e){
            e.printStackTrace();
        }



    }

    public static void ParseSchemaActions(JSONObject jsonObject){

        try {
            ArrayList<Actions>actions = new ArrayList<>();
            JSONArray forms_aray = jsonObject.getJSONArray("actions");

            for (int i = 0; i <forms_aray.length() ; i++) {
                Actions action = new Actions();
                action.readFromJSONObject(forms_aray.getJSONObject(i));
                actions.add(action);
            }
            Schema.getInstance().setActions(actions);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void getFormsInCurrentDay(){
        SimpleDateFormat format = new SimpleDateFormat("EEE");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        Calendar currentdate = Calendar.getInstance();
        if (SCMDataManager.getInstance().getSelectedSite()!=null){
            currentdate.add(Calendar.MINUTE,SCMDataManager.getInstance().getSelectedSite().getSite().effectiveUTCOffset);
        }

        ArrayList<String>sections = new ArrayList<>();
        ArrayList<Days> foodsafety = new ArrayList<>();
        ArrayList<Days> checklist = new ArrayList<>();

        for (Days days :Schema.getInstance().getDays()) {

            if (format.format(currentdate.getTime()).equalsIgnoreCase(days.day)){

                for (Forms form : Schema.getInstance().getForms()){

                    if (form.formName.equalsIgnoreCase(days.formName)){

                        if (days.section.contains("Cleaning Tasks")){

                        }else if (days.section.contains("Operations")){

                        }else if (days.section.contains("_Audit")){

                        }else if (days.section.contains("Checklist")){
                            if (!sections.contains(days.section)) {
                                sections.add(days.section);
                                days.setForms(new ArrayList<Forms>());
                                days.getForms().add(form);
                                checklist.add(days);
                                Schema.getInstance().setChecklist(checklist);
                            } else {
                                for (Days days1 : Schema.getInstance().getChecklist()){
                                    if (days.section.equalsIgnoreCase(days1.section)){
                                        days1.getForms().add(form);
                                    }
                                }
                            }

                        }else {
                            if (!sections.contains(days.section)) {
                                sections.add(days.section);
                                days.setForms(new ArrayList<Forms>());
                                days.getForms().add(form);
                                foodsafety.add(days);
                                System.out.println("DAY FORM: "+form.formName);
                                Schema.getInstance().setFoodsafety(foodsafety);
                            } else {
                                for (Days days1 : Schema.getInstance().getFoodsafety()){
                                    if (days.section.equalsIgnoreCase(days1.section)){
                                        days1.getForms().add(form);
                                        System.out.println("DAY FORM: "+form.formName);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static void DownloadFormpack(final ProgressDialog progressDialog, final Activity activity)throws Exception {

        progressDialog.setMessage("Downloading latest Forms");
        System.out.println("getLatestFormapack: start");
        String serialnumber = UUID.randomUUID().toString();
        dataManager.loadLatestFormpackWithName(SCMDataManager.getInstance().getSPID(), serialnumber, "", new OnCompleteListeners.getLatestFormpackListener() {
            @Override
            public void getLatestFormpackCallback(byte[] zip, Error error) {
                if (error == null) {
                    try {
                        progressDialog.setMessage("Unzipping Forms");
                        SCMTool.unzip(zip,activity);

                        FindSchema(progressDialog,activity);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                // Before navigating, I still check one more time the permission for good practice.
                verifyStoragePermissions(this);
                DeleteFileIFexist();

            }

        } else { // Case 2. Permission was refused
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                // Case 2.1. shouldShowRequest... returns true because the
                // permission was denied before. If it is the first time the app is running we will
                // end up in this part of the code. Because he need to deny at least once to get
                // to onRequestPermissionsResult.
                DisplayDialogForPermisison();
            } else {
                // Case 2.2. Permission was already denied and the user checked "Never ask again".
                // Navigate user to settings if he choose to allow this time.
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("SCM Permission");
                builder.setCancelable(false);
                builder.setMessage("SCM permission is required, Allow the permissions in order to use the Application")
                        .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                SCMTool.clearAppData(getApplicationContext());
                            }
                        });
                Dialog dialog = builder.create();
                dialog.show();
            }
        }
    }

    public  void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permissionWRITE = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permissionWRITE != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    private void DisplayDialogForPermisison(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("SCM Permission");
        builder.setCancelable(false);
        builder.setMessage("SCM permission is required, Denying permission will may cause the application less functional")
                .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        verifyStoragePermissions(Login.this);
                    }
                });
        Dialog dialog = builder.create();
        dialog.show();
    }




}