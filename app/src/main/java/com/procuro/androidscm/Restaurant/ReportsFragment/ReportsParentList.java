package com.procuro.androidscm.Restaurant.ReportsFragment;

import com.procuro.apimmdatamanagerlib.ApplicationReport;

import java.util.ArrayList;

public class ReportsParentList {

    String title;
    ArrayList<ReportsDateChildList>weeklyArrayList;
    ArrayList<ApplicationReport> applicationReports;

    public ReportsParentList(String title) {
        this.title = title;
    }

    public ArrayList<ApplicationReport> getApplicationReports() {
        return applicationReports;
    }

    public void setApplicationReports(ArrayList<ApplicationReport> applicationReports) {
        this.applicationReports = applicationReports;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<ReportsDateChildList> getWeeklyArrayList() {
        return weeklyArrayList;
    }

    public void setWeeklyArrayList(ArrayList<ReportsDateChildList> weeklyArrayList) {
        this.weeklyArrayList = weeklyArrayList;
    }
}
