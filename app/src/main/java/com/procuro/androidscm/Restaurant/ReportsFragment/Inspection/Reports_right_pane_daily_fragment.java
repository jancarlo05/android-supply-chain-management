package com.procuro.androidscm.Restaurant.ReportsFragment.Inspection;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.Calendar.CalendarActivity;
import com.procuro.androidscm.Restaurant.QuickAccessMenu.QuickAccessMenu;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsDateChildList;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsDateGrandChildList;
import com.procuro.androidscm.Restaurant.ReportsFragment.Reports_fragment;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;

import java.text.SimpleDateFormat;
import java.util.Objects;

public class Reports_right_pane_daily_fragment extends Fragment {

    ReportsDateGrandChildList selectedDay;
    public WebView webView;
    public RadioButton food_safety,closing_opening,daily_ops,rush_ready,breakfast_readiness;
    private info.hoang8f.android.segmented.SegmentedGroup infogroup;
    private ReportsDateGrandChildList grandChildList;
    private ReportsDateChildList childList;
    private Button quick_a_menu,back,Covid;
    private TextView name;

    public Reports_right_pane_daily_fragment() {
        this.grandChildList = SCMDataManager.getInstance().getSelectedReportsDateGrandChild();
        this.childList = SCMDataManager.getInstance().getSeletedReportsDateChildList();
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.reports_right_pane_inspection_reports_daily_fragment, container, false);

        webView = rootView.findViewById(R.id.webview);
        back = rootView.findViewById(R.id.back_button);
        infogroup = rootView.findViewById(R.id.segmented2);
        food_safety = rootView.findViewById(R.id.food_safety);
        closing_opening = rootView.findViewById(R.id.closing_opening);
        daily_ops = rootView.findViewById(R.id.dop);
        rush_ready = rootView.findViewById(R.id.rush_ready);
        quick_a_menu = rootView.findViewById(R.id.quick_a_menu);
        name = rootView.findViewById(R.id.username);
        Covid = rootView.findViewById(R.id.covid);
        breakfast_readiness = rootView.findViewById(R.id.breakfast_readiness);

        final SimpleDateFormat title = new SimpleDateFormat("EEEE, dd MMMM yyyy");

        CheckBrekfast();

        setupOnclicks();

        return rootView;
    }

    private void CheckBrekfast (){
        if (SCMDataManager.getInstance().getHoursOfOperation()!=null){
            if (SCMDataManager.getInstance().getHoursOfOperation().isBreakfast){
                SCMTool.EnableView(breakfast_readiness,1);
            }else {
                SCMTool.DisableView(breakfast_readiness,.5f);
            }
        }
    }


    private void setupOnclicks(){
        infogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                try {
                    RadioButton button = radioGroup.findViewById(i);
                    name.setText(button.getText());

                    FindSelectedDay(i);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                back();
            }
        });

        quick_a_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuickAccessMenu.DisplayQuickAMenu(getContext(),quick_a_menu,getActivity());
            }
        });

        Covid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuickAccessMenu.DisplayCovidFragment(getActivity());
            }
        });

        food_safety.setChecked(true);
    }

    private void FindSelectedDay(int id){
        LoadWebview(id,grandChildList,childList);
    }

    private void LoadWebview(int id , ReportsDateGrandChildList daily, ReportsDateChildList weekly) {
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.reports_daily_fragment_container,
                new Reports_inspection_Load_Webview(id ,daily,weekly)).commit();

    }

    private void back() {
            getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_right,R.anim.slide_right_out)
                    .replace(R.id.resturant_fragment_container,
                            new Reports_fragment()).commit();
    }

    private void DisplayCalendar() {
        Intent intent = new Intent(getActivity(), CalendarActivity.class);
        requireActivity().startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onStop() {
        super.onStop();
    }
}