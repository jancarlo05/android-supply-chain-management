package com.procuro.androidscm.Restaurant.Status.Chart;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSensor;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.Status.StatusActivity;
import com.procuro.androidscm.Restaurant.Status.StatusActivityFragment;
import com.procuro.androidscm.Restaurant.Status.StatusData;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.CustomPimmInstance;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.StatusSearhAdapter.Status_Allstore_SearchAdapter;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.StatusSearhAdapter.Status_OwnerShipView_SearchAdapter;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.StatusSearhAdapter.Status_StateByState_SearchAdapter;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.PimmDevice;
import com.procuro.apimmdatamanagerlib.PimmInstance;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class Status_Chart_Fragment extends Fragment {


    private View rootView;
    private WebView webView;
    private TextView message,sensorname;
    private ProgressBar progressBar;
    private com.procuro.apimmdatamanagerlib.aPimmDataManager dataManager;

    public Status_Chart_Fragment() {

    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
            rootView = inflater.inflate(R.layout.status_chart_fragment, container,false);
            webView = rootView.findViewById(R.id.webView);
            message = rootView.findViewById(R.id.message);
            progressBar = rootView.findViewById(R.id.progresbar);
            sensorname =rootView.findViewById(R.id.sensor_name);


            WebSettings settings = webView.getSettings();
            settings.setJavaScriptEnabled(true);
            settings.setDomStorageEnabled(true);
            settings.setSupportZoom(true);
            settings.supportZoom();
            settings.setDisplayZoomControls(false);
            settings.setBuiltInZoomControls(true);
            webView.setWebChromeClient(new Webcromeclient());
            webView.setWebViewClient(new mywebviewclient());
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setUseWideViewPort(true);


            CreateUrl();

        return rootView;

    }

    private void CreateUrl(){

        try {
            if (StatusData.getInstance().getSelectedSite()!=null){
                StatusActivityFragment.name.setText(StatusData.getInstance().getSelectedSite().getSite().sitename);

                if (StatusData.getInstance().getPimmInstance()!=null){
                    if (StatusData.getInstance().getPimmInstance()!=null){

                        sensorname.setText(StatusData.getInstance().getPimmInstance().description);
                        Site site = StatusData.getInstance().getSelectedSite().getSite();
                        PimmDevice device = StatusData.getInstance().getPimmDevice();
                        PimmInstance instance = StatusData.getInstance().getPimmInstance();

                        StringBuilder url = new StringBuilder();
                        url.append("https://");
                        url.append(SCMDataManager.getInstance().getSPID());
                        url.append(".pimm.us/");
                        url.append("WebPimm5/SignInRemote.aspx?");
                        url.append("remote=1&preserve=1");
                        url.append("&spid=");
                        url.append(SCMDataManager.getInstance().getSPID());
                        url.append("&email=");
                        url.append(SCMDataManager.getInstance().getUsername());
                        url.append("&pass=");
                        url.append(SCMDataManager.getInstance().getPassword());
                        url.append("&goto=");

                        url.append("/WebPimm5/SCM/reports/store-metrics/");
                        url.append(site.DeviceID);
                        url.append("/");
                        url.append(instance.objectName);
                        url.append("/");
                        url.append(instance.instanceId);
                        url.append("?SiteID=");
                        url.append(site.siteid);
                        url.append("&amp;hideTools=1");
                        System.out.println("URL ENCODE "+url.toString());
                        webView.loadUrl(url.toString());
                    }
                    else{
                        SensorCategoryView(StatusData.getInstance().getSelectedSite());
                    }
                }else if (StatusData.getInstance().getSelectedSite().getSite()!=null){

                    if (StatusData.getInstance().getPimmInstance()!=null){
                        sensorname.setText(StatusData.getInstance().getPimmInstance().description);
                        Site site = StatusData.getInstance().getSelectedSite().getSite();
                        PimmDevice device = StatusData.getInstance().getSelectedSite().getPimmDevice();
                        PimmInstance instance = StatusData.getInstance().getPimmInstance();

                        StringBuilder url = new StringBuilder();
                        url.append("https://");
                        url.append(SCMDataManager.getInstance().getSPID());
                        url.append(".pimm.us/");
                        url.append("WebPimm5/SignInRemote.aspx?");
                        url.append("remote=1&preserve=1");
                        url.append("&spid=");
                        url.append(SCMDataManager.getInstance().getSPID());
                        url.append("&email=");
                        url.append(SCMDataManager.getInstance().getUsername());
                        url.append("&pass=");
                        url.append(SCMDataManager.getInstance().getPassword());
                        url.append("&goto=");

                        url.append("/WebPimm5/SCM/reports/store-metrics/");
                        url.append(device.deviceID);
                        url.append("/");
                        url.append(instance.objectName);
                        url.append("/");
                        url.append(instance.instanceId);
                        url.append("?SiteID=");
                        url.append(site.siteid);
                        url.append("&amp;hideTools=1");
                        System.out.println("URL ENCODE "+url.toString());

                        webView.loadUrl(url.toString());

                    }else {
                        SensorCategoryView(StatusData.getInstance().getSelectedSite());
                    }

                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void SensorCategoryView(final CustomSite site){

        dataManager = aPimmDataManager.getInstance();
        if (site.getSite()!=null) {
            dataManager.getInstanceListForSiteId(site.getSite().DeviceID, new OnCompleteListeners.getInstanceListForSiteIdCallbackListener() {
                @Override
                public void getInstanceListForSiteIdCallback(ArrayList<PimmInstance> pimmInstanceArrayList, Error error) {
                    if (error == null) {
                        if (pimmInstanceArrayList.size() > 0) {
                            FilterObjectNames(pimmInstanceArrayList,site);
                        }else {
                            message.setVisibility(View.VISIBLE);
                            message.setText("No Metrics Collection Found ");
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    }
                }
            });
        }
    }

    private void FilterObjectNames(ArrayList<PimmInstance> temperatureLists,CustomSite customSite){
        ArrayList<String>objectname = new ArrayList<>();
        ArrayList<CustomSensor>objects=  new ArrayList<>();
        for (PimmInstance newObjects: temperatureLists){
            if (!objectname.contains(newObjects.objectName)){
                objectname.add(newObjects.objectName);
                objects.add(new CustomSensor(newObjects.objectName));
                System.out.println("Description: "+newObjects.description);
            }
        }

        PopulateInstanceByObjectName(temperatureLists,objects,customSite);
    }

    private void PopulateInstanceByObjectName(ArrayList<PimmInstance>pimmInstances,
                                              ArrayList<CustomSensor>newObjects,CustomSite customSite){
        for (PimmInstance instance: pimmInstances){
            for (CustomSensor newObject: newObjects){
                if (instance.objectName.equalsIgnoreCase(newObject.getTitle())){
                    if (newObject.getObject() == null){
                        ArrayList<PimmInstance>newObs = new ArrayList<>();
                        newObs.add(instance);
                        newObject.setObject(newObs);
                    }else {
                        newObject.getObject().add(instance);
                    }
                }
            }
        }
        PopulateInstanceByCoolsersAndFreezers(newObjects,customSite);
    }

    private void PopulateInstanceByCoolsersAndFreezers(ArrayList<CustomSensor>sensors,CustomSite customSite){

        for (CustomSensor sensor : sensors){
            if (sensor.getTitle().equalsIgnoreCase("Temperature")) {

                ArrayList<CustomPimmInstance> customPimmInstances = new ArrayList<>();
                ArrayList<String> titles = new ArrayList<>();
                if (sensor.getObject() != null) {
                    for (PimmInstance instance : sensor.getObject()) {
                        if (instance.description.toLowerCase().contains("cooler")) {
                            if (!titles.contains("Coolers")) {
                                titles.add("Coolers");
                                ArrayList<PimmInstance> instances = new ArrayList<>();
                                customPimmInstances.add(new CustomPimmInstance("Coolers", instances));

                            } else {
                                for (CustomPimmInstance pimmInstance : customPimmInstances) {
                                    if (pimmInstance.getTitle().equalsIgnoreCase("Coolers")) {
                                        pimmInstance.getInstances().add(instance);
                                    }
                                }
                            }
                        } else if (instance.description.toLowerCase().contains("freezer")) {

                            if (!titles.contains("Freezers")) {
                                titles.add("Freezers");
                                ArrayList<PimmInstance> instances = new ArrayList<>();
                                instances.add(instance);
                                customPimmInstances.add(new CustomPimmInstance("Freezers", instances));
                                System.out.println("Freezer: "+instance.description);

                            } else {
                                for (CustomPimmInstance pimmInstance : customPimmInstances) {
                                    if (pimmInstance.getTitle().equalsIgnoreCase("Freezers")) {
                                        pimmInstance.getInstances().add(instance);
                                        System.out.println("Freezer: "+instance.description);
                                    }
                                }
                            }
                        }
                    }
                }


                Collections.sort(customPimmInstances, new Comparator<CustomPimmInstance>() {
                    @Override
                    public int compare(CustomPimmInstance o1, CustomPimmInstance o2) {
                        return o1.getTitle().compareToIgnoreCase(o2.getTitle());
                    }
                });

                for (CustomPimmInstance pimmInstance : customPimmInstances){
                    getTemperatureLevel(pimmInstance,sensors,customSite);
                    break;
                }
            }
        }
    }

    private void getTemperatureLevel(final CustomPimmInstance pimmInstance, ArrayList<CustomSensor>customSensors,CustomSite customSite){

        if (pimmInstance.getInstances()!=null){
            for (PimmInstance sensors :  pimmInstance.getInstances()){
                setInstanceForTemperature(customSite,sensors);
                System.out.println("sensors: "+sensors.description);
                break;
            }
        }
    }

    private void setInstanceForTemperature(CustomSite site, PimmInstance pimmInstance){

        if (pimmInstance==null){
            message.setVisibility(View.VISIBLE);
            message.setText("No Metrics Collection Found ");
            progressBar.setVisibility(View.INVISIBLE);
        }else {
            StatusData.getInstance().setSelectedSite(site);
            StatusData.getInstance().setPimmDevice(site.getPimmDevice());
            StatusData.getInstance().setPimmInstance(pimmInstance);

            CreateUrl();
        }



    }

    private class Webcromeclient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);

        }
    }

    private class mywebviewclient extends WebViewClient {

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            message.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);

        }
    }


    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (Status_Allstore_SearchAdapter.progressDialog!=null){
            Status_Allstore_SearchAdapter.progressDialog.dismiss();
        }

        if (Status_StateByState_SearchAdapter.progressDialog!=null){
            Status_StateByState_SearchAdapter.progressDialog.dismiss();
        }

        if (Status_OwnerShipView_SearchAdapter.progressDialog!=null){
            Status_OwnerShipView_SearchAdapter.progressDialog.dismiss();
        }

    }
}