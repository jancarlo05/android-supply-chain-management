package com.procuro.androidscm.Restaurant.Qualification.StoreProfile;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.procuro.androidscm.GlideApp;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.Restaurant.SOS.SOSActivity;
import com.procuro.androidscm.Restaurant.SOS.SOSData;
import com.procuro.androidscm.Restaurant.Status.StatusActivity;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.PropertyValue;
import com.procuro.apimmdatamanagerlib.SMSDailyOpsPlanCustomerSatisfactionItem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Objects;

import static com.procuro.androidscm.Restaurant.Qualification.StoreProfile.TeamAndContancts.Qualification_TeamAndContancts_Fragment.wcsd;
import static com.procuro.androidscm.Restaurant.Qualification.StoreProfile.TeamAndContancts.Qualification_TeamAndContancts_Fragment.wcsd_Container;


public class StoreProfile_Emergency_listviewAdapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<PropertyValue> arraylist;
    public FragmentActivity fragmentActivity;


    public StoreProfile_Emergency_listviewAdapter(Context context, ArrayList<PropertyValue> arraylist,FragmentActivity fragmentActivity) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;
        this.fragmentActivity = fragmentActivity;
    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return arraylist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {

            PropertyValue propertyValue = arraylist.get(position);

            if (propertyValue!=null){
            if (propertyValue.property.equalsIgnoreCase("FM:EC:Police")){
                view =getContactsWithIcons(propertyValue);
            }
            else if (propertyValue.property.equalsIgnoreCase("FM:EC:Fire")){
                view =getContactsWithIcons(propertyValue);
            }
            else if (propertyValue.property.equalsIgnoreCase("FM:EC:RescueSquad")){
                view =getContactsWithIcons(propertyValue);
            }
            else if (propertyValue.property.equalsIgnoreCase("FM:EC:Hospital")){
                view =getContactsWithIcons(propertyValue);
            }
            else if (propertyValue.property.equalsIgnoreCase("Company Contacts")){
                view = getWendysContacts(propertyValue);
            }
            else if (propertyValue.property.equalsIgnoreCase("FM:EC:Franchise")) {
                view =  getContactsWithOutIcons(propertyValue);
            }
            else if (propertyValue.property.equalsIgnoreCase("FM:EC:DistrictManager")){
                view =  getContactsWithOutIcons(propertyValue);
            }
            else if (propertyValue.property.equalsIgnoreCase("FM:EC:DirectorAreaOps")) {
                view =  getContactsWithOutIcons(propertyValue);
            }
            else if (propertyValue.property.equalsIgnoreCase("FM:EC:LossPrevention")){
                view =  getContactsWithOutIcons(propertyValue);
            }
            else if (propertyValue.property.equalsIgnoreCase("FM:EC:EMSSafetyMgmt")){
                view =  getContactsWithOutIcons(propertyValue);
            }

            else if (propertyValue.property.equalsIgnoreCase("FM:EC:AutomatedCommSystem(ACS)")){
                view =  getContactsWithOutIcons(propertyValue);
            }

            else if (propertyValue.property.equalsIgnoreCase("FM:EC:SecurityDesk")){
              view =  getContactsWithOutIcons(propertyValue);
            }
        }
            return view;
    }


    private View getContactsWithOutIcons(PropertyValue propertyValue){
        View view = inflater.inflate(R.layout.emergency_contact_list_data, null);
        try {
            TextView title = view.findViewById(R.id.name);
            TextView contact = view.findViewById(R.id.contact);

            if (propertyValue.property.equalsIgnoreCase("FM:EC:Franchise")) {
                title.setText("Franchise/Operator/Area Office");
            }
            else if (propertyValue.property.equalsIgnoreCase("FM:EC:DistrictManager")){
                title.setText("District Manager");
            }
            else if (propertyValue.property.equalsIgnoreCase("FM:EC:DirectorAreaOps")) {
                title.setText("Director Area Ops");
            }
            else if (propertyValue.property.equalsIgnoreCase("FM:EC:LossPrevention")){
                title.setText("Loss Prevention / Security Mng");
            }
            else if (propertyValue.property.equalsIgnoreCase("FM:EC:EMSSafetyMgmt")){
                title.setText("EMS Safety Mng");
            }

            else if (propertyValue.property.equalsIgnoreCase("FM:EC:AutomatedCommSystem(ACS)")){
                title.setText("Automated Comm System (ACS)");
            }

            else if (propertyValue.property.equalsIgnoreCase("FM:EC:SecurityDesk")){
                title.setText("Security Desk");
            }

            contact.setText(SCMTool.CheckString(propertyValue.value,""));

        }catch (Exception e){
            e.printStackTrace();
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QualificationData data = QualificationData.getInstance();
                SOSData.setInstance(data.getSosData());
                Intent intent = new Intent(fragmentActivity, SOSActivity.class);
                Objects.requireNonNull(fragmentActivity).startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(fragmentActivity).toBundle());

            }
        });

        return view;

    }
    private View getContactsWithIcons(PropertyValue propertyValue){
        View view = inflater.inflate(R.layout.sos_contact_with_icon_list_data, null);

        try {
            TextView title = view.findViewById(R.id.name);
            TextView contact = view.findViewById(R.id.value);
            ImageView icon = view.findViewById(R.id.icon);

            if (propertyValue.property.equalsIgnoreCase("FM:EC:Police")){
                title.setText("Police Dept.");
                GlideApp.with(mContext).asDrawable().load(R.drawable.police_dept_no_bg).into(icon);

            }
            else if (propertyValue.property.equalsIgnoreCase("FM:EC:Fire")){
                title.setText("Fire Dept.");
                GlideApp.with(mContext).asDrawable().load(R.drawable.fire_dept_nobg).into(icon);
            }
            else if (propertyValue.property.equalsIgnoreCase("FM:EC:RescueSquad")){
                title.setText("Rescue Squad");
                GlideApp.with(mContext).asDrawable().load(R.drawable.rescue_dept_nobg).into(icon);
            }
            else if (propertyValue.property.equalsIgnoreCase("FM:EC:Hospital")){
                title.setText("Hospital / Clinic");
                GlideApp.with(mContext).asDrawable().load(R.drawable.hospital_dept_nobg).into(icon);
            }

            contact.setText(SCMTool.CheckString(propertyValue.value,""));

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    QualificationData data = QualificationData.getInstance();
                    SOSData.setInstance(data.getSosData());
                    Intent intent = new Intent(fragmentActivity, SOSActivity.class);
                    Objects.requireNonNull(fragmentActivity).startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(fragmentActivity).toBundle());

                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }

        return view;

    }

    private View getWendysContacts(PropertyValue propertyValue){
        View view = inflater.inflate(R.layout.status_journal_parent_data, null);
        TextView title = view.findViewById(R.id.rowParentText);
        try {
            title.setText(propertyValue.property);

        }catch (Exception e){
            e.printStackTrace();
        }

        return view;

    }

}

