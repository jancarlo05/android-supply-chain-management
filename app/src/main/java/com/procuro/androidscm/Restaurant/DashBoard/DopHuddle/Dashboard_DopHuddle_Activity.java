package com.procuro.androidscm.Restaurant.DashBoard.DopHuddle;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.SCMDataManager;

public class Dashboard_DopHuddle_Activity extends AppCompatActivity {

    Button back;
    TextView name,message;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_dop_huddle);

        listView = findViewById(R.id.listview);
        back = findViewById(R.id.home);
        name = findViewById(R.id.username);
        message = findViewById(R.id.message);


        setupONclicks();
        DisplayData();
    }


    private void setupONclicks(){
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void DisplayData(){
        SCMDataManager data = SCMDataManager.getInstance();
        try {
            name.setText(data.getSelectedSite().getSite().sitename);

            if (data.getDashboardDopHudlle()!=null){
                if (data.getDashboardDopHudlle().size()>0){
                    message.setVisibility(View.GONE);
                    Dashboard_dop_huddle_tasks_listviewAdapter listviewAdapter =
                            new Dashboard_dop_huddle_tasks_listviewAdapter(this,data.getDashboardDopHudlle());
                    listView.setAdapter(listviewAdapter);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }



    }
}
