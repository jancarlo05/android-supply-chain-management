package com.procuro.androidscm.Restaurant.DashBoard.Dashbord_VOC.TeamLeaders;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Managers_Confirmation_SelectEmployee_list_adapter;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Managers_Confirmation_SelectManager_list_adapter;
import com.procuro.androidscm.Restaurant.DashBoard.VideoPlayerActivity;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.apimmdatamanagerlib.Award;

import java.util.ArrayList;
import java.util.Objects;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

public class Dashboard_Reward_History_DialogFragment extends DialogFragment {


    private Button close ;
    private Dashboard_reward_history_listviewAdapter adapter;
    private ListView listView;
    public static CustomUser user;
    private TextView message,name;



    public static Dashboard_Reward_History_DialogFragment newInstance(CustomUser obj) {
        user = obj;

        return new Dashboard_Reward_History_DialogFragment();
    }
    @Override
        public int getTheme() {
        return R.style.slide_down_up;
        }

    @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.dashboard_reward_history_dialog_fragment, container, false);

//                    int height = (int)(getResources().getDisplayMetrics().heightPixels*0.90);
//                    int width = 320;
//
        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.95);
        int height = (int)(getResources().getDisplayMetrics().heightPixels*0.60);
        Objects.requireNonNull(getDialog().getWindow()).setLayout(width, height);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.slide_down_up;
        view.setFocusableInTouchMode(true);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        close = view.findViewById(R.id.back);
        listView = view.findViewById(R.id.listView);
        message = view.findViewById(R.id.message);
        name = view.findViewById(R.id.name);


        DisplayHistory();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });






        return view;

        }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }


    private void DisplayHistory(){
        ArrayList<Award>awards = user.getUser().awardList;
        try {
            if (awards!=null){
                if (awards.size()>0){
                    adapter = new Dashboard_reward_history_listviewAdapter(getContext(),awards);
                    listView.setAdapter(adapter);
                    message.setVisibility(View.GONE);
                }
            }
            name.setText(user.getUser().firstName);
            name.append(" ");
            name.append(user.getUser().lastName);


        }catch (Exception e){
            e.printStackTrace();
        }
    }



}
