package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_CustomerExperience;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.SMSPositioning;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Dashboard_CustomerExperience extends AppCompatActivity {

    TextView username,cee_value,fe_value;
    Button back;
    LinearLayout cee_container , fe_container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_customer_experience);

        username = findViewById(R.id.username);
        back = findViewById(R.id.home);
        cee_container = findViewById(R.id.cee_container);
        cee_value = findViewById(R.id.cee_value);
        fe_container = findViewById(R.id.fe_container);
        fe_value = findViewById(R.id.fe_value);

        setOnclicks();
        DisplayData();

    }
    private void DisplayData(){
        try {
            username.setText(SCMDataManager.getInstance().getSelectedSite().getSite().sitename);

            SMSPositioning smsPositioning = SCMDataManager.getInstance().getPositionForm();

            cee_value.setText(SCMTool.CheckString(smsPositioning.customerExperience_CEE,"--"));
            fe_value.setText(SCMTool.CheckString(smsPositioning.customerExperience_FEE,"--"));
            cee_container.setBackgroundResource(setGradeColors(SCMTool.CheckString(smsPositioning.customerExperience_CEE,"--")));
            fe_container.setBackgroundResource(setGradeColors(SCMTool.CheckString(smsPositioning.customerExperience_FEE,"--")));

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setOnclicks(){
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public static int setGradeColors(String value){
        double valuecolor = 0;
        boolean parsable = false;

        Pattern p = Pattern.compile("([0-9])");
        Matcher m = p.matcher(value);

        if(m.find()){
            parsable = true;
        }

        if (parsable){
            valuecolor = Double.parseDouble(value.replace("%",""));
        }else {
                return  R.drawable.rounded_orange_bg;

        }
        if(valuecolor > 0 && valuecolor < 65)
        {
            return  R.drawable.rounded_red_bg;

        }
        else if(valuecolor > 74)
        {
            return  R.drawable.rounded_green_bg;
        }
        else
        {
            return  R.drawable.rounded_red_bg;
        }
    }

}
