package com.procuro.androidscm.Restaurant.ScoreCard;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationFragment;
import com.procuro.androidscm.Restaurant.Status.StatusData;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.StoreListFragment.StatusStoreListFragment;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.ScorecardDTO;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;


public class ScoreCardFragment extends Fragment {

    private CustomSite site;
    private boolean result;
    private com.procuro.apimmdatamanagerlib.aPimmDataManager dataManager;
    private TextView greenflag,yellowflag,redflag,blackflag,facilty_overall,overallGrade,onTime,late,inComplete
            ,completed,generalMeets,totalAudits,praticeAudit,practiceAuditAvg,inspectionOverall,auditOverAll,storename,storeAddress;
    private ProgressDialog dialog;
    private ConstraintLayout overall_grade_container,internal_audits_grade_container,food_safety_grade_container,facility_monitoring_grade_Container,food_safety_inspection_grade_container;

    public ScoreCardFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.status_scorecard_fragment, container, false);

        storename = view.findViewById(R.id.storename);
        storeAddress = view.findViewById(R.id.storeaddress);
        greenflag = view.findViewById(R.id.green_flag);
        yellowflag = view.findViewById(R.id.yellow_flag);
        redflag = view.findViewById(R.id.red_flag);
        blackflag =view.findViewById(R.id.black_flag);
        facilty_overall = view.findViewById(R.id.facility_monitoring_grade);
        overallGrade = view.findViewById(R.id.overall_grade);
        onTime = view.findViewById(R.id.food_safety_ontime);
        late = view.findViewById(R.id.food_safety_late);
        inComplete = view.findViewById(R.id.food_safety_incomplete);
        completed = view.findViewById(R.id.food_safety_completed);
        generalMeets = view.findViewById(R.id.audit_general_meets);
        totalAudits = view.findViewById(R.id.total_audit);
        praticeAudit = view.findViewById(R.id.practice_audit);
        practiceAuditAvg = view.findViewById(R.id.practice_audit_avg);
        inspectionOverall = view.findViewById(R.id.food_safety_grade);
        auditOverAll = view.findViewById(R.id.internal_audits_grade);
        overall_grade_container = view.findViewById(R.id.overall_grade_container);
        internal_audits_grade_container = view.findViewById(R.id.internal_audits_grade_container);
        food_safety_grade_container = view.findViewById(R.id.food_safety_grade_container);
        facility_monitoring_grade_Container = view.findViewById(R.id.facility_monitoring_grade_Container);
        food_safety_inspection_grade_container = view.findViewById(R.id.food_safety_inspection_grade_container);

        getExtra();

        DownloadScoreCardInfo();

        return view;
    }


    private void getExtra(){
        QualificationData qualidata = QualificationData.getInstance();
        StatusData statdata = StatusData.getInstance();

        if (qualidata.getSelectedSite()!=null){
            this.site = qualidata.getSelectedSite();

        }else if (statdata.getSelectedSite()!=null){
            this.site = statdata.getSelectedSite();
        }
        else {
            this.site = SCMDataManager.getInstance().getSelectedSite();
        }

    }


    private void DownloadScoreCardInfo(){
        try {
            storename.append(site.getSite().sitename);

            storeAddress.append(SCMTool.getCompleteAddress(site.getSite()));

            dialog = new ProgressDialog(getContext());
            dialog.setTitle("Please Wait");
            dialog.setMessage("Downloading data");
            dialog.setCancelable(true);
//            dialog.show();

        }catch (Exception e){
            e.printStackTrace();
        }

        try {
            dataManager = aPimmDataManager.getInstance();
            dataManager.getScorecardForSiteId(site.getSite().siteid, "", new OnCompleteListeners.getScorecardForSiteIdCallbackListener() {
                @Override
                public void getScorecardForSiteIdCallback(ScorecardDTO scorecardDTO, Error error) {
                    if (error == null){
                        site.setScorecardDTO(scorecardDTO);
                        ComputeOverallGrade(scorecardDTO);
                        DisplayScores(scorecardDTO);
                        dialog.dismiss();
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void DisplayScoreCard(boolean ifqualification) {

        if (!ifqualification){
            getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_right,R.anim.slide_right_out)
                    .replace(R.id.resturant_fragment_container,
                            new StatusStoreListFragment()).commit();
        }else {
            getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_right,R.anim.slide_right_out)
                    .replace(R.id.resturant_fragment_container,
                            new QualificationFragment()).commit();
        }
    }

    private void ComputeOverallGrade(ScorecardDTO scorecardDTO){
        try {
            int totalItems = 0;
            int totalScore = 0;

            if (scorecardDTO.facilityScorecard!=null){
                if (scorecardDTO.facilityScorecard.grade>0){
                    totalScore+= (int)scorecardDTO.facilityScorecard.grade;
                    System.out.println("facilityScorecard: "+scorecardDTO.facilityScorecard.grade);
                    System.out.println("TOtal Score: "+totalScore);
                    totalItems++;

                }
            }
            if (scorecardDTO.supplierDeliveryPlantScorecard!=null){
                if (scorecardDTO.supplierDeliveryPlantScorecard.grade>0){
                    totalScore+= scorecardDTO.supplierDeliveryPlantScorecard.grade;
                    System.out.println("supplierDeliveryPlantScorecard: "+scorecardDTO.supplierDeliveryPlantScorecard.grade);
                    System.out.println("TOtal Score: "+totalScore);
                    totalItems++;
                }
            }
            if (scorecardDTO.supplierDeliveryDCScorecard!=null){
                if (scorecardDTO.supplierDeliveryDCScorecard.overallGrade>0){
                    totalScore+= scorecardDTO.supplierDeliveryDCScorecard.overallGrade;
                    System.out.println("supplierDeliveryDCScorecard: "+scorecardDTO.supplierDeliveryDCScorecard.overallGrade);
                    System.out.println("TOtal Score: "+totalScore);
                    totalItems++;
                }
            }if (
                    scorecardDTO.storeDeliveryScorecard!=null){
                if (scorecardDTO.storeDeliveryScorecard.otdGrade>0){
                    totalScore+= scorecardDTO.storeDeliveryScorecard.otdGrade;
                    System.out.println("storeDeliveryScorecard: "+scorecardDTO.storeDeliveryScorecard.otdGrade);
                    System.out.println("TOtal Score: "+totalScore);
                    totalItems++;
                }
            }

            if (totalItems>0){
                int average = (totalScore/totalItems);
                overallGrade.setText(ConvertGrades(average,overall_grade_container));
//                System.out.println("OverallGrade: "+ConvertGrades(average,overall_grade_container));
            }


        }catch (Exception e){
            e.printStackTrace();
        }


    }

    private String ConvertGrades(int intGradres,View view){

        String grade = "--";
        try {
            if (intGradres == 1){
                grade = "F";
                view.setBackgroundResource(R.drawable.rounded_black_bg);
            }else if (intGradres == 2){
                grade = "D";
                view.setBackgroundResource(R.drawable.rounded_red_bg);
            }
            else if (intGradres == 3){
                grade = "C";
                view.setBackgroundResource(R.drawable.rounded_orange_bg);
            }else if (intGradres == 4){
                grade = "B";
                view.setBackgroundResource(R.drawable.rounded_green_bg);
            }
            else if (intGradres == 5){
                grade = "A";
                view.setBackgroundResource(R.drawable.rounded_blue_bg);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return grade;
    }


    private void DisplayScores(ScorecardDTO scorecardDTO){

        try {
            if (scorecardDTO.facilityScorecard!=null){
                facilty_overall.setText(ConvertGrades(scorecardDTO.facilityScorecard.grade,facility_monitoring_grade_Container));
                greenflag.setText(String.valueOf(scorecardDTO.facilityScorecard.flagCountGreen));
                yellowflag.setText(String.valueOf(scorecardDTO.facilityScorecard.flagCountYellow));
                redflag.setText(String.valueOf(scorecardDTO.facilityScorecard.flagCountRed));
                blackflag.setText(String.valueOf((scorecardDTO.facilityScorecard.flagCountBlack)));

                if (scorecardDTO.facilityScorecard.inspectionScorecard!=null){
                    onTime.setText(String.valueOf(scorecardDTO.facilityScorecard.inspectionScorecard.onTimeCount));
                    late.setText(String.valueOf(scorecardDTO.facilityScorecard.inspectionScorecard.lateCount));
                    inComplete.setText(String.valueOf(scorecardDTO.facilityScorecard.inspectionScorecard.lastIncomplete));
                    completed.setText(String.valueOf(scorecardDTO.facilityScorecard.inspectionScorecard.lastComplete));
                    inspectionOverall.setText(ConvertGrades(scorecardDTO.facilityScorecard.inspectionScorecard.grade,food_safety_inspection_grade_container));

                }
                if (scorecardDTO.facilityScorecard.auditScorecard!=null){
                    generalMeets.setText(String.valueOf(scorecardDTO.facilityScorecard.auditScorecard.actualAuditCount));
                    totalAudits.setText(String.valueOf(scorecardDTO.facilityScorecard.auditScorecard.actualAuditCount));
                    praticeAudit.setText(String.valueOf(scorecardDTO.facilityScorecard.auditScorecard.practiceAuditCount));
                    practiceAuditAvg.setText(String.valueOf(scorecardDTO.facilityScorecard.auditScorecard.practicePassPercentage));
                    auditOverAll.setText(ConvertGrades(scorecardDTO.facilityScorecard.auditScorecard.grade,internal_audits_grade_container));
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
