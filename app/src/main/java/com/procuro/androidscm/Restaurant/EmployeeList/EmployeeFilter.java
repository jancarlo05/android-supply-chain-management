package com.procuro.androidscm.Restaurant.EmployeeList;

public class EmployeeFilter {
    String name;
    boolean selected;

    public EmployeeFilter(String name, boolean selected) {
        this.name = name;
        this.selected = selected;
    }

    public EmployeeFilter(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
