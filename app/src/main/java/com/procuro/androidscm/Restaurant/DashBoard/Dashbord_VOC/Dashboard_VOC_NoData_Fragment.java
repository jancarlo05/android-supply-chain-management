package com.procuro.androidscm.Restaurant.DashBoard.Dashbord_VOC;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import com.procuro.androidscm.GlideApp;
import com.procuro.androidscm.R;


public class Dashboard_VOC_NoData_Fragment extends Fragment {

    ImageView imageView;
    int image;

    public Dashboard_VOC_NoData_Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dashboard_voc_nodata_fragment, container, false);
        imageView = view.findViewById(R.id.imageview);

        assert getArguments() != null;
        this.image = getArguments().getInt("image");
        GlideApp.with(this).load(image).into(imageView);


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

    }

}
