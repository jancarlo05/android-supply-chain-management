package com.procuro.androidscm.Restaurant.ReportsFragment;

public class Comment {
   private String commentValue;
   private   String commentKey;

    public Comment(String commentKey) {
        this.commentKey = commentKey;
    }

    public Comment(String commentValue, String commentKey) {
        this.commentValue = commentValue;
        this.commentKey = commentKey;
    }

    public String getCommentValue() {
        return commentValue;
    }

    public void setCommentValue(String commentValue) {
        this.commentValue = commentValue;
    }

    public String getCommentKey() {
        return commentKey;
    }

    public void setCommentKey(String commentKey) {
        this.commentKey = commentKey;
    }
}
