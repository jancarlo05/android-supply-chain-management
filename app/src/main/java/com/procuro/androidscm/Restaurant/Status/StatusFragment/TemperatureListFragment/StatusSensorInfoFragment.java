package com.procuro.androidscm.Restaurant.Status.StatusFragment.TemperatureListFragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.Keep;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.diegocarloslima.fgelv.lib.FloatingGroupExpandableListView;
import com.diegocarloslima.fgelv.lib.WrapperExpandableListAdapter;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.UnderMaintenance_DialogFragment;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.PimmInstance;
import com.procuro.apimmdatamanagerlib.PimmThreshold;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

@Keep

public class StatusSensorInfoFragment extends Fragment {

    private PimmInstance temperatureList;
    private CustomSite site;
    private com.procuro.apimmdatamanagerlib.aPimmDataManager dataManager;
    private TextView upperRed,upperYellow,upperGreen,lowerRed,lowerYellow,lowerGreen,latestUpdate,chartIndicator;
    private ProgressBar progressBar;
    private WebView webView;
    private RadioButton today,day3,day7,day30,day365,daterange;
    private info.hoang8f.android.segmented.SegmentedGroup infogroup;
    private Button back;




    public StatusSensorInfoFragment() {
        this.temperatureList = SCMDataManager.getInstance().getSelectedPimmInstance();
        this.site = SCMDataManager.getInstance().getSelectedSite();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.status_temperature_info_fragment, container, false);
        TextView title_header = view.findViewById(R.id.temperature_name);
        title_header.setText(temperatureList.description);
        webView = view.findViewById(R.id.webView);
        progressBar = view.findViewById(R.id.progressbar);
        back = view.findViewById(R.id.home);
        upperRed = view.findViewById(R.id.red_upper);
        lowerRed = view.findViewById(R.id.red_lower);
        upperYellow = view.findViewById(R.id.yellow_upper);
        lowerYellow = view.findViewById(R.id.yellow_lower);
        upperGreen = view.findViewById(R.id.green_upper);
        lowerGreen = view.findViewById(R.id.green_lower);
        ConstraintLayout webContainer = view.findViewById(R.id.container);
        infogroup = view.findViewById(R.id.segmented2);
        today = view.findViewById(R.id.today);
        day3 = view.findViewById(R.id.day3);
        day7 = view.findViewById(R.id.day7);
        day30 = view.findViewById(R.id.day30);
        day365 = view.findViewById(R.id.day356);
        daterange = view.findViewById(R.id.date_range);
        latestUpdate = view.findViewById(R.id.latest_update);
        chartIndicator = view.findViewById(R.id.graphIndicator);

        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        settings.setSupportZoom(true);
        settings.supportZoom();
        settings.setDisplayZoomControls(false);
        settings.setBuiltInZoomControls(true);
        webView.setWebViewClient(new mywebviewclient());
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);

        DisplayUnderMaintenanceMessage();

        DisabledViews();

        setupOnclicks();

        LoadGraphData();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                back();
            }
        });




        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void LoadGraphData(){
        dataManager = aPimmDataManager.getInstance();
        dataManager.getThresholdForInstanceId(temperatureList.instanceId, new OnCompleteListeners.getThresholdForInstanceIdCallbackListener() {
            @Override
            public void getThresholdForInstanceIdCallback(PimmThreshold pimmThreshold, Error error) {
                if (error == null){
                    if (pimmThreshold!=null){
                        DisplayTreshold(pimmThreshold);
                        EnabledViews();
                        chartIndicator.setVisibility(View.GONE);
                    }
                }
            }
        });

    }



    private void setupOnclicks(){

        infogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                progressBar.setVisibility(View.VISIBLE);
                DisabledViews();
                if (checkedId == today.getId()){
                    webView.loadUrl(DisplayToday(temperatureList.instanceId));

                }else if (checkedId == day3.getId()){
                    webView.loadUrl(DisplayDay3(temperatureList.instanceId));

                }else if (checkedId == day7.getId()){
                    webView.loadUrl(DisplayDay7(temperatureList.instanceId));

                }else if (checkedId == day30.getId()){
                    webView.loadUrl(DisplayDay30(temperatureList.instanceId));

                }else if (checkedId == day365.getId()){
                    webView.loadUrl(DisplayDay365(temperatureList.instanceId));

                }else if (checkedId == daterange.getId()){
                    DisplayDateRangePicker();
                }
            }
        });
        today.setChecked(true);

    }

//    private void populateJournal(View rootView){
//
//        FloatingGroupExpandableListView list = (FloatingGroupExpandableListView) rootView.findViewById(R.id.sample_activity_list);
//        list.setChildDivider(new ColorDrawable(Color.BLACK));
//        Status_Journal_listiew_adapter adapter = new Status_Journal_listiew_adapter(rootView.getContext(),SCMDataManager.getInstance().getStatusJournal());
//        WrapperExpandableListAdapter wrapperAdapter = new WrapperExpandableListAdapter(adapter);
//        list.setAdapter(wrapperAdapter);
//        list.setChildDivider(null);
//        list.setDividerHeight(0);
//        for(int i = 0; i < wrapperAdapter.getGroupCount(); i++) {
//            list.expandGroup(i);
//        }
//
//        list.setOnScrollFloatingGroupListener(new FloatingGroupExpandableListView.OnScrollFloatingGroupListener() {
//            @Override
//            public void onScrollFloatingGroupListener(View view, int i) {
//                view.setBackgroundResource(R.drawable.rounded_top_black);
//            }
//        });
//    }

    private void back() {
        StatusSensorFragment fragment = new StatusSensorFragment();
        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_right,R.anim.slide_right_out)
                .replace(R.id.resturant_fragment_container,
                       fragment).commit();
    }

    @SuppressLint("SetTextI18n")
    private void DisplayTreshold(PimmThreshold threshold){
        if (threshold !=null){
            if (threshold.enabled){
                if (threshold.mode == 0){
                    upperRed.setText(threshold.red+"°F");
                    upperYellow.setText(threshold.yellow+"°F");
                    upperGreen.setText(threshold.green+"°F");
                    chartIndicator.setVisibility(View.GONE);

                } else if (threshold.mode == 1){
                    lowerRed.setText(threshold.redLow+"°F");
                    lowerYellow.setText(threshold.yellowLow+"°F");
                    lowerGreen.setText(threshold.greenLow+"°F");
                    chartIndicator.setVisibility(View.GONE);

                } else if (threshold.mode == 2){
                    upperRed.setText(threshold.red+"°F");
                    upperYellow.setText(threshold.yellow+"°F");
                    upperGreen.setText(threshold.green+"°F");
                    lowerRed.setText(threshold.redLow+"°F");
                    lowerYellow.setText(threshold.yellowLow+"°F");
                    lowerGreen.setText(threshold.greenLow+"°F");
                    chartIndicator.setVisibility(View.GONE);
                }else {
                    chartIndicator.setText("No Data");
                    chartIndicator.setVisibility(View.VISIBLE);
                    DisabledViews();
                }
            }
        }
    }

    private String DisplayToday(final String instanceID){

        final String[] link = {""};
        final SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmm");

        final Calendar start = Calendar.getInstance();
        start.setTime(new Date());
        start.set(Calendar.HOUR_OF_DAY, 1);
        start.set(Calendar.MINUTE,0);

        final Calendar end = Calendar.getInstance();
        end.setTime(new Date());
        end.set(Calendar.HOUR_OF_DAY,23);
        end.set(Calendar.MINUTE,59);
        int width  = 800;
        int height = 800;

        link[0] ="https://wendys.pimm.us/DesktopModules/EmailChart.aspx?" +
                "instanceid="+instanceID+
                "&username="+ SCMDataManager.getInstance().getUsername()+
                "&password="+SCMDataManager.getInstance().getPassword()+
                "&start="+format.format(start.getTime())+
                "&end="+format.format(end.getTime())+
                "width="+width+
                "&height="+height;
        return link[0];
    }

    private String DisplayDay3(final String instanceID){

        final String[] link = {""};
        final SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmm");

        final Calendar start = Calendar.getInstance();
        start.setTime(new Date());
        start.set(Calendar.HOUR_OF_DAY, 1);
        start.set(Calendar.MINUTE,0);
        start.add(Calendar.DATE,-2);

        final Calendar end = Calendar.getInstance();
        end.setTime(new Date());
        end.set(Calendar.HOUR_OF_DAY,23);
        end.set(Calendar.MINUTE,59);
        int width  = 800;
        int height = 800;

        link[0] ="https://wendys.pimm.us/DesktopModules/EmailChart.aspx?" +
                "instanceid="+instanceID+
                "&username="+ SCMDataManager.getInstance().getUsername()+
                "&password="+SCMDataManager.getInstance().getPassword()+
                "&start="+format.format(start.getTime())+
                "&end="+format.format(end.getTime())+
                "width="+width+
                "&height="+height;

        System.out.println("start: "+format.format(start.getTime()));
        System.out.println("end: "+format.format(end.getTime()));
        return link[0];
    }

    private String DisplayDay7(final String instanceID){

        final String[] link = {""};
        final SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmm");

        final Calendar start = Calendar.getInstance();
        start.setTime(new Date());
        start.set(Calendar.HOUR_OF_DAY, 1);
        start.set(Calendar.MINUTE,0);
        start.add(Calendar.DATE,-6);

        final Calendar end = Calendar.getInstance();
        end.setTime(new Date());
        end.set(Calendar.HOUR_OF_DAY,23);
        end.set(Calendar.MINUTE,59);
        int width  = 800;
        int height = 800;

        link[0] ="https://wendys.pimm.us/DesktopModules/EmailChart.aspx?" +
                "instanceid="+instanceID+
                "&username="+ SCMDataManager.getInstance().getUsername()+
                "&password="+SCMDataManager.getInstance().getPassword()+
                "&start="+format.format(start.getTime())+
                "&end="+format.format(end.getTime())+
                "width="+width+
                "&height="+height;

        return link[0];
    }

    private String DisplayDay30(final String instanceID){

        final String[] link = {""};
        final SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmm");

        final Calendar start = Calendar.getInstance();
        start.setTime(new Date());
        start.set(Calendar.HOUR_OF_DAY, 1);
        start.set(Calendar.MINUTE,0);
        start.add(Calendar.DATE,-29);

        final Calendar end = Calendar.getInstance();
        end.setTime(new Date());
        end.set(Calendar.HOUR_OF_DAY,23);
        end.set(Calendar.MINUTE,59);
        int width  = 800;
        int height = 800;

        link[0] ="https://wendys.pimm.us/DesktopModules/EmailChart.aspx?" +
                "instanceid="+instanceID+
                "&username="+ SCMDataManager.getInstance().getUsername()+
                "&password="+SCMDataManager.getInstance().getPassword()+
                "&start="+format.format(start.getTime())+
                "&end="+format.format(end.getTime())+
                "width="+width+
                "&height="+height;

        System.out.println("start: "+format.format(start.getTime()));
        System.out.println("end: "+format.format(end.getTime()));
        return link[0];
    }

    private String DisplayDay365(final String instanceID){

        final String[] link = {""};
        final SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmm");

        final Calendar start = Calendar.getInstance();
        start.setTime(new Date());
        start.set(Calendar.HOUR_OF_DAY, 1);
        start.set(Calendar.MINUTE,0);
        start.add(Calendar.YEAR,-1);
        start.add(Calendar.DATE,-1);

        final Calendar end = Calendar.getInstance();
        end.setTime(new Date());
        end.set(Calendar.HOUR_OF_DAY,23);
        end.set(Calendar.MINUTE,59);
        int width  = 800;
        int height = 800;

        link[0] ="https://wendys.pimm.us/DesktopModules/EmailChart.aspx?" +
                "instanceid="+instanceID+
                "&username="+ SCMDataManager.getInstance().getUsername()+
                "&password="+SCMDataManager.getInstance().getPassword()+
                "&start="+format.format(start.getTime())+
                "&end="+format.format(end.getTime())+
                "width="+width+
                "&height="+height;

        System.out.println("start: "+format.format(start.getTime()));
        System.out.println("end: "+format.format(end.getTime()));
        return link[0];
    }

    private class mywebviewclient extends WebViewClient{
        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);

            System.out.println("Error: "+error.getDescription());
            System.out.println(error.getErrorCode());
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            SimpleDateFormat format = new SimpleDateFormat("MM dd ,yyyy hh,mm a");
            progressBar.setVisibility(View.INVISIBLE);
            EnabledViews();
            latestUpdate.setText(format.format(new Date()));

        }
    }

    private void DisplayDateRangePicker(){
        DialogFragment newFragment = Status_DateRangePicker_DialogFragment.newInstance(webView,temperatureList);
        assert getFragmentManager() != null;
        newFragment.show(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), "dialog");
    }


    private void EnabledViews(){
        infogroup.setEnabled(true);
        infogroup.setAlpha(1);
        today.setEnabled(true);
        day3.setEnabled(true);
        day7.setEnabled(true);
        day30.setEnabled(true);
        day365.setEnabled(true);
        daterange.setEnabled(true);
    }

    private void DisabledViews(){
        infogroup.setEnabled(false);
        infogroup.setAlpha(.5f);
        today.setEnabled(false);
        day3.setEnabled(false);
        day7.setEnabled(false);
        day30.setEnabled(false);
        day365.setEnabled(false);
        daterange.setEnabled(false);
    }


    private void DisplayUnderMaintenanceMessage() {
        DialogFragment newFragment = UnderMaintenance_DialogFragment.newInstance();
        assert getFragmentManager() != null;
        newFragment.show(getActivity().getSupportFragmentManager(), "dialog");
    }
}
