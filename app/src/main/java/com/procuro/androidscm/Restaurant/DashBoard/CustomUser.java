package com.procuro.androidscm.Restaurant.DashBoard;

import com.procuro.apimmdatamanagerlib.SMSPositioningEmployeePositionData;
import com.procuro.apimmdatamanagerlib.SMSPositioningEmployeeScheduleAssignmentPositions;
import com.procuro.apimmdatamanagerlib.Schedule_Business_Site_Plan;
import com.procuro.apimmdatamanagerlib.User;

import java.util.ArrayList;

public class CustomUser {
    private User user ;
    private boolean selected;
    private SMSPositioningEmployeePositionData employeePositionData;
    private boolean selectedmanager;
    private ArrayList<SMSPositioningEmployeeScheduleAssignmentPositions>positions;
    private ArrayList<Schedule_Business_Site_Plan>schedules;

    public ArrayList<Schedule_Business_Site_Plan> getSchedules() {
        return schedules;
    }

    public void setSchedules(ArrayList<Schedule_Business_Site_Plan> schedules) {
        this.schedules = schedules;
    }

    public CustomUser() {
    }

    public CustomUser(User user) {
        this.user = user;
    }

    public SMSPositioningEmployeePositionData getEmployeePositionData() {
        return employeePositionData;
    }

    public void setEmployeePositionData(SMSPositioningEmployeePositionData employeePositionData) {
        this.employeePositionData = employeePositionData;
    }

    public ArrayList<SMSPositioningEmployeeScheduleAssignmentPositions> getPositions() {
        return positions;
    }

    public void setPositions(ArrayList<SMSPositioningEmployeeScheduleAssignmentPositions> positions) {
        this.positions = positions;
    }

    public boolean isSelectedmanager() {
        return selectedmanager;
    }

    public void setSelectedmanager(boolean selectedmanager) {
        this.selectedmanager = selectedmanager;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
