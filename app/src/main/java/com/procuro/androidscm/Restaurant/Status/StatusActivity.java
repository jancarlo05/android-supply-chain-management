package com.procuro.androidscm.Restaurant.Status;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.Calendar.CalendarActivity;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.DashBoard.DashboardActivity;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.Dashboard_Staffing_Activity;
import com.procuro.androidscm.Restaurant.PimmyNews.Pimmy_News_DialogFragment;
import com.procuro.androidscm.Restaurant.Qualification.QualificationActivity;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.Restaurant.QuickAccessMenu.QuickAccessMenu;
import com.procuro.androidscm.Restaurant.SOS.SOSActivity;
import com.procuro.androidscm.Restaurant.SOS.SOSData;
import com.procuro.androidscm.Restaurant.ScoreCard.ScoreCardFragment;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.Qualification_Store_Profile_Fragment;
import com.procuro.androidscm.Restaurant.Status.Chart.Status_Chart_Fragment;
import com.procuro.androidscm.Restaurant.Status.Map.Status_Map_Fragment;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileActivity;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileData;
import com.procuro.androidscm.SCMDataManager;

import java.util.Objects;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

public class StatusActivity extends AppCompatActivity {

    info.hoang8f.android.segmented.SegmentedGroup infogrp;
    public static RadioButton map,chart,storeOptions,scoreCard;
    Button back, Dashboard,profile,quick_a_menu,sos,Covid;
    public static TextView name;
    private com.procuro.apimmdatamanagerlib.aPimmDataManager dataManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.status_activity);

        infogrp =findViewById(R.id.segmented2);
        storeOptions = findViewById(R.id.store_options);
        scoreCard = findViewById(R.id.score_card);
        map = findViewById(R.id.map);
        chart = findViewById(R.id.chart);
        back = findViewById(R.id.home);
        name = findViewById(R.id.username);
        Dashboard = findViewById(R.id.dashboard);
        quick_a_menu = findViewById(R.id.quick_a_menu);
        sos  =findViewById(R.id.sos);
        Covid = findViewById(R.id.covid);
        setupOnclicks();

        getArgument();

    }


    private void getArgument(){


        StatusData data = StatusData.getInstance();
        if (data.isTemperatureViewEnabled()){
            chart.setChecked(true);
        }else {
            map.setChecked(true);
        }

    }

    private void setupOnclicks(){

        try {
            infogrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int id) {

                    if (id == map.getId()){
                        Display_facility_map();

                    }else if (id == storeOptions.getId()){
                        DisplayStoreProfile();

                    }else if (id == chart.getId()){
                        DisplayChart();

                    }else if (id == scoreCard.getId()){
                        DisplayScorecard();
                    }
                }
            });

            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });



            Dashboard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(StatusActivity.this, DashboardActivity.class);
                    Objects.requireNonNull(StatusActivity.this).startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(StatusActivity.this).toBundle());
                    finish();
                }
            });
            quick_a_menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DisplayQuickAMenu(StatusActivity.this,quick_a_menu,StatusActivity.this);
                }
            });


            sos.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SOSData data = new SOSData();
                    data.setSite(SCMDataManager.getInstance().getSelectedSite().getSite());
                    SOSData.setInstance(data);
                    Intent intent = new Intent(StatusActivity.this, SOSActivity.class);
                    Objects.requireNonNull(StatusActivity.this).startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(StatusActivity.this).toBundle());
                }
            });


            Covid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    QuickAccessMenu.DisplayCovidFragment(StatusActivity.this);
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }


    }
    private void Display_facility_map() {
        Fragment fragment = new Status_Map_Fragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.qualification_store_profile, fragment).commit();
    }

    private void DisplayStoreProfile() {
        Fragment fragment = new Qualification_Store_Profile_Fragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.qualification_store_profile,
                fragment).commit();
    }

    private void DisplayChart() {
        Fragment fragment = new Status_Chart_Fragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.qualification_store_profile,
                fragment).commit();
    }

    private void DisplayScorecard() {
        getSupportFragmentManager().beginTransaction().replace(R.id.qualification_store_profile,
                new ScoreCardFragment()).commit();

    }

    public void DisplayQuickAMenu(final Context context, View anchor, final FragmentActivity fragmentActivity) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.BOTTOM)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .transparentOverlay(false)
                .arrowColor(ContextCompat.getColor(context, R.color.black))
                .transparentOverlay(true)
                .contentView(R.layout.quick_access_menu)
                .focusable(true)
                .build();
        tooltip.show();

        ConstraintLayout calendar,dop,employee_sched,help,news,reports_portal,review_n_verify,skpe,sos,storeprofile,temp_check,user_profile;

        calendar = tooltip.findViewById(R.id.calendar);
        dop = tooltip.findViewById(R.id.dashboard);
        employee_sched = tooltip.findViewById(R.id.employee_schedule);
        help = tooltip.findViewById(R.id.menu_help);
        news = tooltip.findViewById(R.id.menu_news);
        reports_portal = tooltip.findViewById(R.id.menu_reports_portal);
        review_n_verify = tooltip.findViewById(R.id.menu_review_n_verify);
        skpe = tooltip.findViewById(R.id.menu_skype);
        sos = tooltip.findViewById(R.id.menu_sos);
        storeprofile = tooltip.findViewById(R.id.menu_store_profile);
        temp_check = tooltip.findViewById(R.id.menu_temp_check);
        user_profile = tooltip.findViewById(R.id.menu_user_profile);


        //remove
        temp_check.setVisibility(View.GONE);


        //disable
        reports_portal.setEnabled(false);
        reports_portal.setAlpha(.5f);


        help.setEnabled(false);
        help.setAlpha(.5f);

        employee_sched.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SCMDataManager.getInstance().setDashboardKitchenData(null);
                SCMDataManager.getInstance().setStoreUsers(null);
                SCMDataManager.getInstance().setPositionForm(null);
                Intent intent = new Intent(fragmentActivity, Dashboard_Staffing_Activity.class);
                fragmentActivity.startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(fragmentActivity).toBundle());
                tooltip.dismiss();
            }
        });


        user_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(fragmentActivity, UserProfileActivity.class);
                UserProfileData data = new UserProfileData();
                data.setUserEditEnabled(false);
                data.setCustomSite(SCMDataManager.getInstance().getSelectedSite());
                CustomUser user = new CustomUser(SCMDataManager.getInstance().getUser());
                data.setCustomUser(user);
                data.setFromQuickAccess(true);
                data.setIsupdate(true);
                UserProfileData.setInstance(data);
                fragmentActivity.startActivity(intent);
                tooltip.dismiss();
            }
        });

        dop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QualificationData data = new QualificationData();
                data.setSelectedSiteEnabled(true);
                QualificationData.setInstance(data);
                Intent intent = new Intent(fragmentActivity, DashboardActivity.class);
                Objects.requireNonNull(fragmentActivity).startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(fragmentActivity).toBundle());
                finish();
                tooltip.dismiss();
            }
        });

        storeprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(fragmentActivity, QualificationActivity.class);
                QualificationData data = new QualificationData();
                data.setSelectedSiteEnabled(true);
                data.setSelectedSite(SCMDataManager.getInstance().getSelectedSite());
                QualificationData.setInstance(data);
                Objects.requireNonNull(fragmentActivity).startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(fragmentActivity).toBundle());
                finish();
                tooltip.dismiss();
            }
        });

        sos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SOSData data = new SOSData();
                data.setSite(SCMDataManager.getInstance().getSelectedSite().getSite());
                SOSData.setInstance(data);
                Intent intent = new Intent(fragmentActivity, SOSActivity.class);
                Objects.requireNonNull(fragmentActivity).startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(fragmentActivity).toBundle());
                tooltip.dismiss();
            }
        });

        calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(fragmentActivity, CalendarActivity.class);
                Objects.requireNonNull(fragmentActivity).startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(fragmentActivity).toBundle());
                tooltip.dismiss();
            }
        });

        news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayPimmyNews(StatusActivity.this);
                tooltip.dismiss();

            }
        });

        skpe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initiateSkypeUri(StatusActivity.this,"");
                tooltip.dismiss();
            }
        });

    }


    private void DisplayPimmyNews(FragmentActivity activity){
        DialogFragment newFragment = Pimmy_News_DialogFragment.newInstance();
        assert getFragmentManager() != null;
        newFragment.show(activity.getSupportFragmentManager(), "dialog");
    }

    public void initiateSkypeUri(Activity activity, String mySkypeUri) {

        // Make sure the Skype for Android client is installed.
        if (!isSkypeClientInstalled(activity)) {
            goToMarket(activity);
        }else {
            Uri skypeUri = Uri.parse(mySkypeUri);
            Intent myIntent = new Intent(Intent.ACTION_VIEW, skypeUri);
            myIntent.setComponent(new ComponentName("com.skype.raider", "com.skype.raider.Main"));
            myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivity(myIntent);
        }
    }


    private boolean isSkypeClientInstalled(Activity activity) {

        PackageManager myPackageMgr = activity.getPackageManager();
        try {
            myPackageMgr.getPackageInfo("com.skype.raider", PackageManager.GET_ACTIVITIES);
        }
        catch (PackageManager.NameNotFoundException e) {
            return (false);
        }
        return (true);
    }

    private void goToMarket(Activity activity) {
        Uri marketUri = Uri.parse("market://details?id=com.skype.raider");
        Intent myIntent = new Intent(Intent.ACTION_VIEW, marketUri);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Objects.requireNonNull(activity).startActivity(myIntent);

    }

    @Override
    public void onStop() {
        super.onStop();
        StatusData data = new StatusData();
        StatusData.setInstance(data);
    }
}
