package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_SalePerformance;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.R;

import java.util.ArrayList;


public class Dashboard_SalesPerformance_Week_Child_item_RecyclerViewAdapter extends RecyclerView.Adapter<Dashboard_SalesPerformance_Week_Child_item_RecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<String> arrayList;
    String type;
    int size;


    public Dashboard_SalesPerformance_Week_Child_item_RecyclerViewAdapter(Context context,
                                                                          ArrayList<String> arraylist, String type, int size) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arrayList = arraylist;
        this.type = type;
        this.size = size;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.dashboard_daily_sales_list_data,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final String string = arrayList.get(position);
        try {
            if (string.contains(":")){
                String[]DisectedName = string.split(":");

                holder.indicator.setVisibility(View.VISIBLE);
                holder.indicator.setText(DisectedName[1]);
                holder.name.setText(DisectedName[0]);

                if (DisectedName[1].equalsIgnoreCase("Dss")){
                    holder.indicator.setTextColor(ContextCompat.getColor(mContext,R.color.orange));
                    holder.name.setTextColor(ContextCompat.getColor(mContext,R.color.orange));
                }
            }else {
                holder.name.setText(string);
            }
            if (string.contains("-")){
                try {
                    double value = Double.parseDouble(string);
                    if (value<0){
                        holder.name.setTextColor(ContextCompat.getColor(mContext,R.color.red));
                    }
                }catch (Exception ignored){

                }
            }


        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name,indicator;
        public MyViewHolder(View itemView) {
            super(itemView);
             name = itemView.findViewById(R.id.name);
            indicator = itemView.findViewById(R.id.indicator);
        }
    }




}
