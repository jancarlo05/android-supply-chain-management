package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_CleaningSlider.Daily;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.Employee;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;


public class Dashboard_cleaning_employee_listviewAdapter extends BaseAdapter {

    // Declare Variables

    Context mContext;
    LayoutInflater inflater;
    private ArrayList<Employee> arraylist;
    TextView textView,assigned;
    SimpleTooltip toolTip;
    CleaningPosition cleaningPosition;

    public Dashboard_cleaning_employee_listviewAdapter(Context context, ArrayList<Employee> arraylist, TextView textView,
                                                       SimpleTooltip toolTip, CleaningPosition cleaningPosition, TextView assigned) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;
        this.textView = textView;
        this.toolTip = toolTip;
        this.cleaningPosition = cleaningPosition;
        this.assigned = assigned;

    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return arraylist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.dashboad_employeelist_child_data, null);
            view.setTag(holder);

            final Employee employee = arraylist.get(position);

            try {

                holder.name = view.findViewById(R.id.name);
                holder.name.setText(employee.getFirstname());
                holder.name.append(" ");
                holder.name.append(employee.getLastname());

            }catch (Exception e){
                e.printStackTrace();
            }



        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    textView.setText(employee.getFirstname());
                    textView.append(" ");
                    textView.append(employee.getLastname());
                    cleaningPosition.setEmployee(employee);
                    cleaningPosition.setAssigned(new Date());
                    SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy : h:mm a");
                    assigned.setText(format.format(cleaningPosition.getAssigned()));
                    toolTip.dismiss();

                }catch (Exception e){
                    e.printStackTrace();
                }


            }
        });

        return view;
    }


}

