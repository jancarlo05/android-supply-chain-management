package com.procuro.androidscm.Restaurant.UserProfile;

import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.apimmdatamanagerlib.CertificationDefinition;
import com.procuro.apimmdatamanagerlib.User;

import java.util.ArrayList;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;


public class User_Skills_Fragment extends Fragment {

//    private ListView listView;
    private User_site_allowed_list_adapter adapter;
    private User_Skills_RecyclerViewAdapter recyclerViewAdapter;
    private RecyclerView recyclerView;
    private info.hoang8f.android.segmented.SegmentedGroup infgroup;
    private RadioButton opsSkill,positionSkill,productSkill,mgmtskill;
    private ConstraintLayout siteAllowed_Container;
    public static TextView siteAllowed;
    private  User user;
    private String siteTitle = "" ;
    boolean EnableEdit;

    public User_Skills_Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View  view = inflater.inflate(R.layout.user_skills_fragment, container, false);

       mgmtskill = view.findViewById(R.id.mgmtskill);
       recyclerView = view.findViewById(R.id.recyclerView);
       infgroup = view.findViewById(R.id.segmented2);
       opsSkill = view.findViewById(R.id.opsSkill);
       positionSkill = view.findViewById(R.id.position_skills);
       productSkill = view.findViewById(R.id.product_skills);
       siteAllowed_Container = view.findViewById(R.id.site_allowed_container);
       siteAllowed = view.findViewById(R.id.site_allowed);

      getArgument();

      setupOnclicks();

        return view;
    }


    private void getArgument(){
        try {
            UserProfileData data = UserProfileData.getInstance();
            if (data.isFromQuickAccess()){
                this.user =  SCMDataManager.getInstance().getUser();
                this.siteTitle = SCMDataManager.getInstance().getSelectedSite().getSite().sitename;
                siteAllowed.setText(SCMDataManager.getInstance().getSelectedSite().getSite().sitename);
            }
            else if (data.isFromQualification()){
                this.user = data.getCustomUser().getUser();

                if (data.getCustomSite()!=null){
                    if (data.getCustomSite().getSite()!=null){
                        this.siteTitle = data.getCustomSite().getSite().sitename;
                        setUpSitesAllowed();
                    }else {
                        DisplaySelectedSite();
                    }
                }
                else {
                    DisplaySelectedSite();
                }
            }

            if (data.isIsupdate()){
                if (this.user.active){
                    if (data.isUserEditEnabled()){

                        CheckCurrentUserRole();

                    }else {
                        EnableEdit = false;
                        DisableEDit();
                    }

                }else {
                    EnableEdit = false;
                    DisableEDit();
                }

            }
            else if (data.isCreate()){
                EnableEdit = true;
                InitializeSiteAllowedForCreate();
            }
            setupSkills();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void InitializeSiteAllowedForCreate(){
        try {
            UserProfileData data = UserProfileData.getInstance();
            QualificationData qualidata = QualificationData.getInstance();
            if (data.getUpdate_Siteallowed()==null){
                data.setUpdate_Siteallowed(new ArrayList<String>());
                data.getUpdate_Siteallowed().add(qualidata.getSelectedSite().getSite().sitename);
                siteAllowed.setText(qualidata.getSelectedSite().getSite().sitename);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private void setupOnclicks(){
        siteAllowed_Container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayQuickAMenu(getContext(),siteAllowed_Container);
            }
        });
    }


    private void setUpSitesAllowed(){
        try {
            siteAllowed.setText(siteTitle);
        }catch (Exception e){
            e.printStackTrace();}

    }

    public void DisplayQuickAMenu(final Context context, View anchor) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.BOTTOM)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .margin(R.dimen.simpletooltip_margin)
                .padding(R.dimen.simpletooltip_padding)
                .transparentOverlay(true)
                .arrowColor(ContextCompat.getColor(context, R.color.dirty_white))
                .contentView(R.layout.site_allowed_layout)
                .focusable(true)
                .build();
        tooltip.show();

        ListView listView = tooltip.findViewById(R.id.listView);
        Button button = tooltip.findViewById(R.id.apply);

        ArrayList<String>filtere_site = getAvailableSitesAllowed();

        adapter = new User_site_allowed_list_adapter(getContext(),filtere_site,EnableEdit);
        listView.setAdapter(adapter);

        try {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DisplaySelectedSite();
                    tooltip.dismiss();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private ArrayList<String> getAvailableSitesAllowed(){
        UserProfileData data = UserProfileData.getInstance();

        ArrayList<String>filtere_site = new ArrayList<>();
        try {
            ArrayList<CustomSite>customSites = SCMDataManager.getInstance().getCustomSites();
            ArrayList<String>roles = new ArrayList<>();

            if (!data.isCreate()){
                if (user.roles!=null){
                    roles = user.roles;
                }
            }
            if (UserProfileData.getInstance().getUpdate_Siteallowed()==null){

                for (CustomSite customSite : customSites){

                    if (roles.contains(customSite.getSite().sitename)){

                        filtere_site.add(customSite.getSite().sitename);

                        System.out.println("ADD: "+customSite.getSite().sitename);
                        if (UserProfileData.getInstance().getSelectedSites()==null){

                            UserProfileData.getInstance().setSelectedSites(new ArrayList<CustomSite>());
                            UserProfileData.getInstance().getSelectedSites().add(customSite);

                        }else {
                            UserProfileData.getInstance().getSelectedSites().add(customSite);
                        }
                    }
                }
                UserProfileData.getInstance().setUpdate_Siteallowed(filtere_site);
                UserProfileData.getInstance().setCurrent_Site_Allowed(new ArrayList<>(filtere_site));

            }else {
                filtere_site = UserProfileData.getInstance().getUpdate_Siteallowed();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return filtere_site;
    }

    private void setupSkills(){
        try {

            infgroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == opsSkill.getId()){
                        DisplaypOpsSkills();
                    }else if (checkedId == positionSkill.getId()){
                        DisplaypositionSkills();

                    }else if (checkedId == productSkill.getId()){
                        DisplaypProductSkills();
                    }
                    else if (checkedId == mgmtskill.getId()){
                        DisplayManagementSkills();
                    }
                }
            });

            positionSkill.setChecked(true);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void DisplaypositionSkills(){
        //CATEGORY 1
        DisplayData(getSKillsByCategory("0"));

    }

    private void DisplaypProductSkills(){
        //CATEGORY 1
        DisplayData(getSKillsByCategory("1"));
    }

    private void DisplaypOpsSkills(){
        //CATEGORY 2
        DisplayData(getSKillsByCategory("2"));

    }

    private void DisplayManagementSkills(){
        //CATEGORY 3
        DisplayData(getSKillsByCategory("3"));
    }

    private ArrayList<CertificationDefinition> getSKillsByCategory(String category){
        ArrayList<CertificationDefinition> skills = new ArrayList<>();
        ArrayList<CertificationDefinition>certificationDefinitions = SCMDataManager.getInstance().getCertificationDefinitions();

        for (CertificationDefinition certificationDefinition : certificationDefinitions){
            if (certificationDefinition.category.equalsIgnoreCase(category)){
                skills.add(certificationDefinition);
            }
        }
        return  skills;
    }

    private void DisplayData(ArrayList<CertificationDefinition> skills){
        recyclerViewAdapter  = new User_Skills_RecyclerViewAdapter(getContext(),skills,EnableEdit,user);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),3));
        recyclerView.setAdapter(recyclerViewAdapter);

    }

    private void DisableEDit(){
        siteAllowed_Container.setEnabled(false);
        siteAllowed_Container.setAlpha(.5f);
        recyclerView.setAlpha(.5f);
        recyclerView.setEnabled(false);
    }

    private void EnableEDit(){
        siteAllowed_Container.setEnabled(true);
        siteAllowed_Container.setAlpha(1);
        recyclerView.setAlpha(1);
        recyclerView.setEnabled(true);
    }

    private void DisplaySelectedSite(){
        UserProfileData data = UserProfileData.getInstance();
        if (data.isCreate()){
            if (data.getSelectedSites()!=null){
                if (data.getSelectedSites().size()>0){
                    siteAllowed.setText(data.getSelectedSites().get(0).getSite().sitename);
                }
            }
        }
    }

    private void CheckCurrentUserRole() {
        User user = SCMDataManager.getInstance().getUser();
        if (user != null) {
            if (user.roles != null) {
                ArrayList<CustomRole>customRoles = CustomRole.getAllCustomRole(user.roles);
                if (customRoles.size()>=1){
                    String strings = customRoles.get(0).getCode();

                    if (strings.equalsIgnoreCase("GM".toUpperCase())) {
                        EnableEDit();
                        EnableEdit = true;
                    }
                    else if (strings.equalsIgnoreCase("DM".toUpperCase())) {
                        EnableEDit();
                        EnableEdit = true;
                    }
                    else if (strings.equalsIgnoreCase("Store admin".toUpperCase())) {
                        EnableEDit();
                        EnableEdit = true;

                    } else if (strings.equalsIgnoreCase("Auditor".toUpperCase())) {
                        DisableEDit();
                    } else if (strings.equalsIgnoreCase("Ops leader".toUpperCase())) {
                        DisableEDit();
                    } else if (strings.equalsIgnoreCase("FSL".toUpperCase())) {
                        DisableEDit();
                    } else if (strings.equalsIgnoreCase("Employee".toUpperCase())) {
                        DisableEDit();
                    }
                    else if (strings.equalsIgnoreCase("RELIEF MGRr".toUpperCase())) {
                        DisableEDit();
                    }

                }

            }
        }
    }

}
