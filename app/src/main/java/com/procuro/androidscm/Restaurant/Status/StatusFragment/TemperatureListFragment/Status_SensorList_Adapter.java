package com.procuro.androidscm.Restaurant.Status.StatusFragment.TemperatureListFragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.procuro.androidscm.GlideApp;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.CustomSensor;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.apimmdatamanagerlib.PimmInstance;

import java.util.ArrayList;


public class Status_SensorList_Adapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private FragmentActivity fragmentActivity;
    private CustomSite site;
    private ArrayList<PimmInstance>instances;
    private CustomSensor temperatureList;


    public Status_SensorList_Adapter(Context context, CustomSensor temperatureList,
                                     FragmentActivity fragmentActivity, CustomSite site,
                                     ArrayList<PimmInstance>instances) {
        this.mContext = context;
        this.inflater = LayoutInflater.from(context);
        this.temperatureList = temperatureList;
        this.fragmentActivity = fragmentActivity;
        this.site = site;
        this.instances = instances;

    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return temperatureList.getObject().size();
    }

    @Override
    public Object getItem(int position) {
        return  temperatureList.getObject().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
            view = inflater.inflate(R.layout.status_temperture_list_data, null);
            final PimmInstance temperature = temperatureList.getObject().get(position);

            ImageView valueContainer = view.findViewById(R.id.value_container);
            TextView value = view.findViewById(R.id.value);
            TextView name = view.findViewById(R.id.name);
            TextView signal = view.findViewById(R.id.signal);
            TextView battery = view.findViewById(R.id.battery);



        if (temperature.severity == 0 ){
            GlideApp.with(mContext).load(R.drawable.empty_circle_blue).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(valueContainer);
            value.setTextColor(ContextCompat.getColor(mContext, R.color.blue));
        }else if (temperature.severity == 3 ){
            GlideApp.with(mContext).load(R.drawable.empty_circle_green).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(valueContainer);
            value.setTextColor(ContextCompat.getColor(mContext, R.color.green));
        }else if (temperature.severity == 5 ){
            GlideApp.with(mContext).load(R.drawable.empty_circle_yellow).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(valueContainer);
            value.setTextColor(ContextCompat.getColor(mContext, R.color.yellow));
        }else if (temperature.severity == 9 ){
            GlideApp.with(mContext).load(R.drawable.empty_circle_red).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(valueContainer);
            value.setTextColor(ContextCompat.getColor(mContext, R.color.red));
        }else {
            GlideApp.with(mContext).load(R.drawable.empty_circle_gray).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(valueContainer);
            value.setTextColor(ContextCompat.getColor(mContext, R.color.gray));
        }


        if (temperature.severity == 10){
            value.setText("--");
            signal.append("--");
            battery.append("--");
        }else {
            for (PimmInstance instance:temperatureList.getObject()){
                if (instance.description.contains(temperature.description)){
                    if (instance.description.contains("Signal")){
                        if (instance.staticValue != null){
                            if (Double.valueOf(instance.staticValue)<10){
                                String red = instance.staticValue;
                                SpannableString redSpannable= new SpannableString(red);
                                redSpannable.setSpan(new ForegroundColorSpan(Color.RED), 0, red.length(), 0);
                                signal.append(redSpannable);
                            }else {
                                signal.append(instance.staticValue);
                            }
                        }else {
                            signal.append("--");
                        }
                    }
                    else if (instance.description.contains("Battery")){
                        if (instance.staticValue!=null){
                            if (Double.valueOf(instance.staticValue)<2.6) {
                                String red = instance.staticValue+ "v";
                                SpannableString redSpannable= new SpannableString(red);
                                redSpannable.setSpan(new ForegroundColorSpan(Color.RED), 0, red.length(), 0);
                                battery.append(redSpannable);
                            }else {
                                battery.append(instance.staticValue);
                            }
                        }else {
                            battery.append("--");
                        }
                    }
                }
            }

            if (temperature.staticValue!=null){
                if (temperature.staticValue.length()>4){
                    value.setText(temperature.staticValue.substring(0,4));
                }else {
                    value.setText(temperature.staticValue);
                }
            }else {
                value.setText("--");
            }

        }
            name.setText(temperature.description);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SCMDataManager.getInstance().setSelectedPimmInstance(temperature);
                    DisplayTempInfo(fragmentActivity,temperature,site);

                }
            });

        return view;
    }


    private void DisplayTempInfo(FragmentActivity fragmentActivity, PimmInstance temperatureList, CustomSite site) {
        StatusSensorInfoFragment fragment = new StatusSensorInfoFragment();
        fragmentActivity.getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_left,R.anim.slide_left_out).replace(R.id.resturant_fragment_container,
                fragment).commit();
    }
}

