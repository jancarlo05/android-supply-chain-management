package com.procuro.androidscm.Restaurant.Qualification.UserProfile;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.apimmdatamanagerlib.Site;

import java.util.ArrayList;


public class Qualification_CustomSite_RecyclerViewAdapter extends RecyclerView.Adapter<Qualification_CustomSite_RecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<CustomSite> arraylist;
    private FragmentActivity fragmentActivity;
    private Bundle fragmentbundle;

    public Qualification_CustomSite_RecyclerViewAdapter(Context context, ArrayList<CustomSite> arraylist,
                                                        FragmentActivity fragmentActivity,
                                                        Bundle bundle) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;
        this.fragmentActivity = fragmentActivity;
        this.fragmentbundle = bundle;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.qualification_corp_cardview,parent,false);



        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final CustomSite customsite = arraylist.get(position);

        if (customsite.getSite().sitename!=null){
            holder.category_name.setText(customsite.getSite().sitename);
        }


        holder.corp_user.setText(Address(customsite.getSite()));

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QualificationData.getInstance().setSelectedItem(customsite.getSite().sitename);
                DisplayCorpview();

            }
        });

    }

    @Override
    public int getItemCount() {
        return arraylist.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView category_name,corp_user;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            category_name = itemView.findViewById(R.id.category_name);
            corp_user = itemView.findViewById(R.id.corp_user);
            cardView = itemView.findViewById(R.id.cardview_id);
        }
    }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }


    private void DisplayCorpview(){
        Fragment fragment = new Qualification_UserProfile_Regional_Child_level_Fragment_2();
        fragment.setArguments(fragmentbundle);
        fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.qualification_store_profile,
                fragment).commit();
    }

    private StringBuilder Address(Site site){
        StringBuilder address = new StringBuilder();

        if (site.Address.Address2 != null){
            if (!site.Address.Address2.equalsIgnoreCase("null")){
                if (!site.Address.Address2.equalsIgnoreCase("")){
                    address.append(site.Address.Address2);
                }
            }
        }


        if (site.Address.Address1 != null){
            if (!site.Address.Address1.equalsIgnoreCase("null")){
                if (!site.Address.Address1.equalsIgnoreCase("")){
                    address.append(site.Address.Address1);
                }
            }
        }


        return address;
    }

}
