package com.procuro.androidscm.Restaurant.Qualification.QualificationFragment;

public class Qualification_Filter {
    String name;
    boolean selected;

    public Qualification_Filter(String name, boolean selected) {
        this.name = name;
        this.selected = selected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
