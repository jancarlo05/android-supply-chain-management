package com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.StorePrep;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.StoreProfileOptions.KitchenCriteria;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.StoreProfileOptions.KitchenDescription;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.StoreProfileOptions.StoreProfileOptionsData;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.StorePrepSchedule;
import com.suke.widget.SwitchButton;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class StorePrep_Listview_adapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<CustomStorePrepData>arraylist ;
    private Site site;
    private Button button;

    public StorePrep_Listview_adapter(Context context, ArrayList<CustomStorePrepData> arraylist) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;
    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return arraylist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {

        view = inflater.inflate(R.layout.prep_hour_listview_parent_data, null);

        TextView name = view.findViewById(R.id.parent);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);

        final CustomStorePrepData storePrepData = arraylist.get(position);
        name.setText(storePrepData.StoreprepSchedule);

        StorePrep_Breakfast_RecyclerViewAdapter adapter =
                new StorePrep_Breakfast_RecyclerViewAdapter(mContext,storePrepData);
        recyclerView.setLayoutManager(new GridLayoutManager(mContext,8));
        recyclerView.setAdapter(adapter);


        return view;
    }


}

