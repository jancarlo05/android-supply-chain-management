package com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.StorePrep;

import com.procuro.apimmdatamanagerlib.StorePrepData;

import java.util.ArrayList;

public class CustomStorePrepData {

    String StoreprepSchedule ;
    ArrayList<StorePrepData> storePrepData;

    public CustomStorePrepData(String storeprepSchedule, ArrayList<StorePrepData> storePrepData) {
        StoreprepSchedule = storeprepSchedule;
        this.storePrepData = storePrepData;
    }

    public String getStoreprepSchedule() {
        return StoreprepSchedule;
    }

    public void setStoreprepSchedule(String storeprepSchedule) {
        StoreprepSchedule = storeprepSchedule;
    }

    public ArrayList<StorePrepData> getStorePrepData() {
        return storePrepData;
    }

    public void setStorePrepData(ArrayList<StorePrepData> storePrepData) {
        this.storePrepData = storePrepData;
    }
}
