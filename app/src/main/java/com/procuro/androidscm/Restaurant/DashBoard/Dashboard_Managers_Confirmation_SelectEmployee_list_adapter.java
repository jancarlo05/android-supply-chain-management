package com.procuro.androidscm.Restaurant.DashBoard;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import com.procuro.androidscm.R;

import java.util.ArrayList;


public class Dashboard_Managers_Confirmation_SelectEmployee_list_adapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<CustomUser> users;
    public FragmentActivity fragmentActivity;
    private LinearLayout numpad_container ;
    private boolean ManagerAvailble;


    public Dashboard_Managers_Confirmation_SelectEmployee_list_adapter(Context context, ArrayList<CustomUser> users,
                                                                       FragmentActivity fragmentActivity,LinearLayout numpad_container,
                                                                       boolean ManagerAvailble) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.users = users;
        this.fragmentActivity = fragmentActivity;
        this.numpad_container = numpad_container;
        this.ManagerAvailble = ManagerAvailble;
    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        holder = new ViewHolder();
        view = inflater.inflate(R.layout.managers_confirmation_users_list_data, null);
        view.setTag(holder);
        final CustomUser user = users.get(position);
        TextView name = view.findViewById(R.id.name);
        CheckBox checkBox = view.findViewById(R.id.checkbox);

        name.setText(user.getUser().firstName);
        name.append(" ");
        name.append(user.getUser().lastName);


        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                user.setSelected(isChecked);
                notifyDataSetChanged();
            }
        });

        disableEnableControls(numpad_container);

        if (user.isSelected()){
            checkBox.setChecked(true);
        }else {
            checkBox.setChecked(false);
        }
            return view;
    }


    private void disableEnableControls(ViewGroup vg) {
        if (ManagerAvailble) {
            boolean enable = false;
            for (CustomUser user : users){
                if (user.isSelected()){
                    enable =true;
                    break;
                }
            }
            if (enable){
                EnableNumpad(vg);
            }
            else {
                DisableNumpad(vg);
            }
        }
        else {
            DisableNumpad(vg);
        }

    }


    private void DisableNumpad(ViewGroup vg){
        try {
            vg.setAlpha(.5f);
            for (int i = 0; i < vg.getChildCount(); i++){
                View child = vg.getChildAt(i);
                child.setEnabled(false);
                if (child instanceof ViewGroup){
                    disableEnableControls((ViewGroup)child);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private void EnableNumpad(ViewGroup vg){
        try {
            vg.setAlpha(1);
            for (int i = 0; i < vg.getChildCount(); i++){
                View child = vg.getChildAt(i);
                child.setEnabled(true);
                if (child instanceof ViewGroup){
                    disableEnableControls((ViewGroup)child);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}

