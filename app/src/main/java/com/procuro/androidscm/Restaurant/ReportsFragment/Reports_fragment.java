package com.procuro.androidscm.Restaurant.ReportsFragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.procuro.androidscm.Logout_Confirmation_DialogFragment;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.EquipmenList.EquipmenlistData;
import com.procuro.androidscm.Restaurant.QuickAccessMenu.QuickAccessMenu;
import com.procuro.androidscm.Restaurant.ReportsFragment.Audits.Reports_audits_fragment;
import com.procuro.androidscm.Restaurant.ReportsFragment.Delivery.Reports_delivery_fragment;
import com.procuro.androidscm.Restaurant.ReportsFragment.Inspection.Reports_inspection_fragment;
import com.procuro.androidscm.Restaurant.ReportsFragment.Ratings.Reports_ratings_fragment;
import com.procuro.androidscm.Restaurant.AllStorePopup.Status_SiteList_DialogFragment;
import com.procuro.androidscm.Restaurant.Schema.FormItems;
import com.procuro.androidscm.Restaurant.Schema.Schema;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.androidscm.UnderMaintenance_DialogFragment;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.PropertyValue;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.SiteSettings;
import com.procuro.apimmthriftservices.services.NamedServices;
import com.procuro.apimmthriftservices.services.auditForm.FSLDataActionItem;
import com.procuro.apimmthriftservices.services.auditForm.FSLDataAttachment;
import com.procuro.apimmthriftservices.services.auditForm.FSLDataFSLFormsClient;
import com.procuro.apimmthriftservices.services.auditForm.FSLDataForm;
import com.procuro.apimmthriftservices.services.auditForm.FSLDataItem;
import com.procuro.apimmthriftservices.services.auditForm.FSLDataNote;
import com.procuro.apimmthriftservices.services.auditForm.FSLDataReading;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsEquipment;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsSiteConfig;

import org.apache.thrift.TException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import io.github.douglasjunior.androidSimpleTooltip.OverlayView;
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

public class Reports_fragment extends Fragment {


    private RadioButton audits;
    public static RadioButton delivery;
    public static RadioButton inspection;
    private RadioButton ratings;
    public  static  info.hoang8f.android.segmented.SegmentedGroup infogroup;
    private LinearLayout dropdownContainer,date_container;
    private TextView storeName, titleheader,date;
    private Button back,quick_a_menu;

    SimpleDateFormat dateFormat = new SimpleDateFormat("MMM d, yyyy");

    public Reports_fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the view for this fragment
        final View view = inflater.inflate(R.layout.reports_fragment, container, false);


        inspection = view.findViewById(R.id.inspection);
        delivery = view.findViewById(R.id.delivery);
        ratings = view.findViewById(R.id.ratings);
        audits = view.findViewById(R.id.audits);
        infogroup = view.findViewById(R.id.segmented2);
        back = view.findViewById(R.id.home);

        dropdownContainer = view.findViewById(R.id.dropdown_container);
        storeName = view.findViewById(R.id.storename);
        titleheader = view.findViewById(R.id.username);
        date = view.findViewById(R.id.date);
        date_container = view.findViewById(R.id.date_container);

        quick_a_menu = view.findViewById(R.id.quick_a_menu);

        setupOnclicks();

        DisplayStorename(storeName,titleheader);


        return view;
    }

    private void setupOnclicks(){

        infogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == inspection.getId()){
                    Display_inspection();
                }else if (i == delivery.getId()){
                    Display_delivery();
                }else if (i == ratings.getId()){
                    Display_ratings();
                }else if (i == audits.getId()){
                    Display_audits();
                }
            }
        });

        inspection.setChecked(true);

        dropdownContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DisplaySiteListPopup();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayLogoutConfirmation();
            }
        });

        quick_a_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuickAccessMenu.DisplayQuickAMenu(getContext(),quick_a_menu,getActivity());
            }
        });

    }

    public  void  Display_inspection() {
        InitializeDates();
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.inspection_food_safety_fragment_container,
                new Reports_inspection_fragment()).commit();
    }

    public  void  Display_ratings() {
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.inspection_food_safety_fragment_container,
                new Reports_ratings_fragment()).commit();
    }

    public  void  Display_delivery() {
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.inspection_food_safety_fragment_container,
                new Reports_delivery_fragment()).commit();
    }

    public  void  Display_audits() {
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.inspection_food_safety_fragment_container,
                new Reports_audits_fragment()).commit();
    }

    private void DisplaySiteListPopup(){
        DialogFragment newFragment = Status_SiteList_DialogFragment.newInstance("reports");
        assert getFragmentManager() != null;
        newFragment.show(getActivity().getSupportFragmentManager(), "dialog");
    }

    private void DisplayStorename(TextView textView , TextView title){
        if (SCMDataManager.getInstance().getReportsSelectedSite() == null){
            textView.setText(SCMDataManager.getInstance().getSelectedSite().getSite().SiteName);
            title.setText(SCMDataManager.getInstance().getSelectedSite().getSite().SiteName);
        }else {
            textView.setText(SCMDataManager.getInstance().getReportsSelectedSite());
            title.setText(SCMDataManager.getInstance().getReportsSelectedSite());
        }
    }


    private void InitializeDates(){
        try {
            ReportsData reportsData = ReportsData.getInstance();
            final ArrayList<ReportsDatePickerData>datePickerData = new ArrayList<>();


            if (reportsData.getStartDate()==null || reportsData.getEndDate()==null){

                Date startDate = getDate(true);
                Date endDate = getDate(false);
                reportsData.setDisableDefaultView(false);

                String selectedateTitle = dateFormat.format(startDate)+ " - "+dateFormat.format(endDate);
                date.setText(selectedateTitle);

                datePickerData.add(new ReportsDatePickerData(true,startDate,"Start"));
                datePickerData.add(new ReportsDatePickerData(false,endDate,"End"));

                ArrayList<ReportsDateChildList> reportsDateChildLists = CreateWeeklyData(startDate,endDate,false);
                if (inspection.isChecked()){
                    try {
                        reportsData.setStartDate(startDate);
                        reportsData.setEndDate(endDate);
                        reportsData.setDetailInspectionReport(reportsDateChildLists);
                        SCMDataManager.getInstance().getReports_inspections().get(1).setWeeklyArrayList(reportsDateChildLists);

                    }catch (Exception e){
                        e.printStackTrace();
                        InitializeDates();
                    }

                }
            }else {
                reportsData.setDisableDefaultView(false);
                ArrayList<ReportsDateChildList> reportsDateChildLists = CreateWeeklyData(reportsData.getStartDate(),reportsData.getEndDate(),true);
                String selectedateTitle = dateFormat.format(reportsData.getStartDate())+ " - "+dateFormat.format(reportsData.getEndDate());
                date.setText(selectedateTitle);

                datePickerData.add(new ReportsDatePickerData(true,reportsData.getStartDate(),"Start"));
                datePickerData.add(new ReportsDatePickerData(false,reportsData.getEndDate(),"End"));


                if (inspection.isChecked()){
                    reportsData.setStartDate(reportsData.getStartDate());
                    reportsData.setEndDate(reportsData.getEndDate());
                    reportsData.setDetailInspectionReport(reportsDateChildLists);
                    SCMDataManager.getInstance().getReports_inspections().get(1).setWeeklyArrayList(reportsDateChildLists);
                }
            }

            date_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DisplayDatePicker(getContext(),date_container,datePickerData);
                }
            });
        }catch (Exception e){
            e.printStackTrace();
            InitializeDates();
        }


    }

    public void DisplayDatePicker(final Context context, View anchor, final ArrayList<ReportsDatePickerData>datePickerData) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.BOTTOM)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .arrowColor(ContextCompat.getColor(context, R.color.gray))
                .transparentOverlay(true)
                .contentView(R.layout.reports_date_picker_popup)
                .focusable(true)
                .build();
        tooltip.show();

        ListView listView = tooltip.findViewById(R.id.listView);
        Button apply = tooltip.findViewById(R.id.apply);

        ReportsDatePicker_Listview_Adapter adapter = new ReportsDatePicker_Listview_Adapter(context,datePickerData);
        listView.setAdapter(adapter);

        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Date startDate = datePickerData.get(0).getDate();
                Date endDate = datePickerData.get(1).getDate();

                ArrayList<ReportsDateChildList> reportsDateChildLists = CreateWeeklyData(startDate,endDate,true);
                String selectedateTitle = dateFormat.format(startDate)+ " - "+dateFormat.format(endDate);
                date.setText(selectedateTitle);

                if (inspection.isChecked()){
                    ReportsData reportsData = ReportsData.getInstance();
                    reportsData.setStartDate(startDate);
                    reportsData.setEndDate(endDate);
                    reportsData.setDetailInspectionReport(reportsDateChildLists);
                    SCMDataManager.getInstance().getReports_inspections().get(1).setWeeklyArrayList(reportsDateChildLists);
                    reportsData.setDisableDefaultView(true);
                    Display_inspection();
                }
                tooltip.dismiss();
            }
        });
    }

    private Date getDate(boolean start){

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = 1;
        c.set(year, month, day);
        int numOfDaysInMonth = c.getActualMaximum(Calendar.DAY_OF_MONTH);

        if (start){
            System.out.println("First Day of month: " + c.getTime());
            return  c.getTime();
        }else {
            c.add(Calendar.DAY_OF_MONTH, numOfDaysInMonth-1);
            System.out.println("Last Day of month: " + c.getTime());
            return c.getTime();
        }
    }

    private ArrayList<ReportsDateChildList> CreateWeeklyData(Date start, Date end, boolean isFromDatePicker) {
        ArrayList<ReportsDateChildList> calendarDates = new ArrayList<>();

        Calendar cal = Calendar.getInstance();
        cal.clear();
        cal.setTimeZone(TimeZone.getTimeZone("UTC"));
        cal.setTime(start);
        cal.set(Calendar.HOUR,12);
        cal.set(Calendar.AM_PM,Calendar.AM);
        cal.set(Calendar.MINUTE,1);
        cal.set(Calendar.SECOND,0);
        cal.set(Calendar.MILLISECOND,0);


        SimpleDateFormat format  = new SimpleDateFormat("w - EEEE MMM/dd/yyyy");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        if (cal.get(Calendar.DAY_OF_WEEK)== Calendar.SUNDAY ||  cal.get(Calendar.DAY_OF_WEEK)== Calendar.SATURDAY){
            cal.add(Calendar.WEEK_OF_YEAR, -1);
        }else {
            cal.set(Calendar.DAY_OF_WEEK,Calendar.MONDAY);
        }

        while (cal.getTime().before(end)) {
            ReportsDateChildList childList = new ReportsDateChildList();
            childList.setDailyArrayList(GenerateWeekDays(cal.getTime(), childList));
            calendarDates.add(childList);
            cal.set(Calendar.DAY_OF_WEEK,Calendar.MONDAY);
            cal.add(Calendar.WEEK_OF_YEAR, 1);
        }

        return calendarDates;
    }

    private ArrayList<ReportsDateGrandChildList> GenerateWeekDays(Date date,ReportsDateChildList childList){
        ArrayList<ReportsDateGrandChildList> dailies = new ArrayList<>();

        try {
            Site site = SCMDataManager.getInstance().getSelectedSite().getSite();

            SimpleDateFormat format = new SimpleDateFormat("EEE MM/dd/yy");

            SimpleDateFormat fmtOutput = new SimpleDateFormat("EEEE, d MMMM yyyy");
            fmtOutput.setTimeZone(TimeZone.getTimeZone("UTC"));
            format.setTimeZone(TimeZone.getTimeZone("UTC"));

            Calendar now = Calendar.getInstance();
            now.setTime(date);
            now.setTimeZone(TimeZone.getTimeZone("UTC"));
            now.set(Calendar.HOUR,12);
            now.set(Calendar.AM_PM,Calendar.AM);
            now.set(Calendar.MINUTE,1);
            now.set(Calendar.SECOND,0);
            now.set(Calendar.MILLISECOND,0);
            now.add(Calendar.DATE,1);

            int weekOfYear = now.get(Calendar.WEEK_OF_YEAR);
            String daily = "Week " + weekOfYear + " Weekly Report";

            Calendar currentdate = SCMTool.getDateWithDaypart(new Date().getTime(),site.effectiveUTCOffset);

            if (now.compareTo(currentdate)<= 0){
                System.out.println(format.format(now.getTime())+ " |  "+daily);
                dailies.add(new ReportsDateGrandChildList(daily, date,false,true));

            } else if (now.get(Calendar.YEAR) <= currentdate.get(Calendar.YEAR)){

                if (now.get(Calendar.MONTH) <= currentdate.get(Calendar.MONTH)){

                    if (now.get(Calendar.WEEK_OF_YEAR)<= currentdate.get(Calendar.WEEK_OF_YEAR)){

                        if (now.get(Calendar.DATE)<= currentdate.get(Calendar.DATE)){

                            System.out.println(format.format(now.getTime())+ " |  "+daily);
                            dailies.add(new ReportsDateGrandChildList(daily, date,false,true));
                        } else {
                            dailies.add(new ReportsDateGrandChildList(daily, date,false,false));
                        }
                    }
                    else {
                        dailies.add(new ReportsDateGrandChildList(daily, date,false,false));
                    }
                } else {
                    dailies.add(new ReportsDateGrandChildList(daily, date,false,false));
                }

            }else {
                dailies.add(new ReportsDateGrandChildList(daily, date,false,false));
            }

            ArrayList<String>weeks = new ArrayList<>();
            ArrayList<Date>Weekdays = new ArrayList<>();
            int delta = -now.get(Calendar.DAY_OF_WEEK) + 2; //add 2 if your week start on monday
            now.add(Calendar.DAY_OF_MONTH, delta );

            for (int i = 0; i < 7; i++) {

                Weekdays.add(now.getTime());
                weeks.add(format.format(now.getTime()));

                String days = fmtOutput.format(now.getTime());

                if (now.compareTo(currentdate)<= 0){
                    dailies.add(new ReportsDateGrandChildList(days, now.getTime(),false,true));
                }
                else if (now.get(Calendar.YEAR)<=currentdate.get(Calendar.YEAR)){

                    if (now.get(Calendar.MONTH)<=currentdate.get(Calendar.MONTH)){

                        if (now.get(Calendar.DATE)<=currentdate.get(Calendar.DATE)){

                            dailies.add(new ReportsDateGrandChildList(days, now.getTime(),false,true));
                        }else {
                            dailies.add(new ReportsDateGrandChildList(days, now.getTime(),false,false));
                        }
                    }else {
                        dailies.add(new ReportsDateGrandChildList(days, now.getTime(),false,false));
                    }
                } else {
                    dailies.add(new ReportsDateGrandChildList(days, now.getTime(),false,false));
                }

                now.add(Calendar.DAY_OF_MONTH, 1);
            };
            System.out.println(weeks);

            fmtOutput = new SimpleDateFormat("MMM dd");

            String firstdate = fmtOutput.format(Weekdays.get(0));
            String lastdate = fmtOutput.format(Weekdays.get(Weekdays.size()-1));
            String parent_title = "Week " + weekOfYear + " (" + firstdate + " - " + lastdate + ")";

            childList.setDailyArrayList(dailies);
            childList.setTitle(parent_title);
            childList.setStartdate(Weekdays.get(0));
            childList.setEnddate(Weekdays.get(Weekdays.size()-1));
            childList.setWeekofyear(weekOfYear);

            boolean isSelected = false;
            for (Date day: Weekdays ){
                if (format.format(day).equalsIgnoreCase(format.format(currentdate.getTime()))){
                    isSelected = true;
                }
            }
            childList.setSelected(isSelected);


        }catch (Exception e){
            e.printStackTrace();
        }

        return dailies;
    }


    private void DisplayLogoutConfirmation() {
        DialogFragment newFragment = Logout_Confirmation_DialogFragment.newInstance();
        assert getFragmentManager() != null;
        newFragment.show(getActivity().getSupportFragmentManager(), "dialog");
    }

    private void DisplayUnderMaintenanceMessage() {
        DialogFragment newFragment = UnderMaintenance_DialogFragment.newInstance();
        assert getFragmentManager() != null;
        newFragment.show(getActivity().getSupportFragmentManager(), "dialog");
    }


    private void DownloadSitePropertyValues(final Site site){
        SCMTool.dataManager.getSiteSettingsForSiteId(site.siteid, new OnCompleteListeners.getSiteSettingsForSiteIdListener() {
            @Override
            public void getSiteSettingsForSiteId(SiteSettings siteSettings, Error error) {
                if (error == null){
                    System.out.println("settings ");
                    for (PropertyValue propertyValue : siteSettings.settings){
                        System.out.println("KEY: "+propertyValue.property);
                        System.out.println("VALUE: "+propertyValue.value);
                    }

                }
            }
        });
    }



}