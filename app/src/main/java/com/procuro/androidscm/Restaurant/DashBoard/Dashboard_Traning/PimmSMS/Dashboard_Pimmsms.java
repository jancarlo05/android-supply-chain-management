package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Traning.PimmSMS;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourceManualFragment.Resources_right_pane_fragment;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourcesChildList;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourcesGrandChildList;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.DocumentDTO;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.util.ArrayList;

import es.voghdev.pdfviewpager.library.RemotePDFViewPager;
import es.voghdev.pdfviewpager.library.adapter.PDFPagerAdapter;
import es.voghdev.pdfviewpager.library.remote.DownloadFile;
import es.voghdev.pdfviewpager.library.util.FileUtil;

public class Dashboard_Pimmsms extends AppCompatActivity implements  DownloadFile.Listener{


    private TextView title;
    private Button back;
    ExpandableListView expandableListView;
    EditText search;
    ArrayList<ResourcesChildList> array_sort = new ArrayList<>();


    //VIDEO REUIREMENTS
    public static WebView webView;
    public static ConstraintLayout fullscreen_Toggle,webview_container,video_pdf_container;
    public static LinearLayout header,list_container;
    public static boolean fullscreen = false;
    public static boolean visibleToggle = false;
    public static ImageView image_toggle;
    public static ProgressBar videoProgressbar;

    //PDF REQUIREMENTS
    public static ProgressBar pdfProgressbar;
    public static TextView date , wait;
    public static RemotePDFViewPager remotePDFViewPager;
    public static PDFPagerAdapter adapter;
    public static ConstraintLayout pdf_container;
    private com.procuro.apimmdatamanagerlib.aPimmDataManager dataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_traning_sms_implementation);

        title = findViewById(R.id.username);
        back = findViewById(R.id.home);
        expandableListView = findViewById(R.id.expandible_listview);
        search = findViewById(R.id.search);

        webView = findViewById(R.id.webView);
        fullscreen_Toggle = findViewById(R.id.fullscreen_Toggle);
        header = findViewById(R.id.header);
        image_toggle = findViewById(R.id.image_toggle);
        back = findViewById(R.id.home);
        videoProgressbar = findViewById(R.id.progresbar);
        list_container = findViewById(R.id.list_container);
        webview_container = findViewById(R.id.webview_container);

        video_pdf_container = findViewById(R.id.video_pdf_container);
        pdfProgressbar = findViewById(R.id.progressBar2);
        wait = findViewById(R.id.wait);
        pdf_container = findViewById(R.id.pdf_container);

        webView.setWebViewClient(new mywebviewclient());
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAllowFileAccess(true);
        webSettings.setAppCacheEnabled(true);
        webSettings.setUseWideViewPort(true);

        SetUpSiteInformation();

        setUPOnclicks();

        populateSafetyDataSheets();

        Searchfunction();


    }

    @SuppressLint("ClickableViewAccessibility")
    private void setUPOnclicks(){

        fullscreen_Toggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TooggleFullscreen();
            }
        });


        webView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ShowToggle();

                return false;
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void TooggleFullscreen(){
        if (!fullscreen){
            EnabledFUllscreen();
            fullscreen = true;
            image_toggle.setBackgroundResource(R.drawable.ic_fullscreen_exit_white_48dp);
            webview_container.getLayoutParams().height = LinearLayout.LayoutParams.MATCH_PARENT;
        }else {
            DisableFullscreen();
            fullscreen = false;
            image_toggle.setBackgroundResource(R.drawable.ic_fullscreen_white_48dp);
            webview_container.getLayoutParams().height = SCMTool.dpToPx(270);

        }
    }

    private void EnabledFUllscreen(){
//        SCREEN_ORIENTATION_LANDSCAPE
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        header.setVisibility(View.GONE);
        image_toggle.setBackgroundResource(R.drawable.ic_fullscreen_exit_white_48dp);
        fullscreen = true;
        list_container.setVisibility(View.GONE);
        webview_container.getLayoutParams().height = LinearLayout.LayoutParams.MATCH_PARENT;
        video_pdf_container.getLayoutParams().height = LinearLayout.LayoutParams.MATCH_PARENT;
    }

    private void DisableFullscreen(){
        //        SCREEN_ORIENTATION_PORTRAIT
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        header.setVisibility(View.VISIBLE);
        fullscreen = false;
        image_toggle.setBackgroundResource(R.drawable.ic_fullscreen_white_48dp);
        list_container.setVisibility(View.VISIBLE);
        webview_container.getLayoutParams().height = SCMTool.dpToPx(270);
        video_pdf_container.getLayoutParams().height = SCMTool.dpToPx(270);

    }

    private void ShowToggle(){
        if (!visibleToggle){
            visibleToggle = true;
            fullscreen_Toggle.setVisibility(View.VISIBLE);

            new CountDownTimer(4000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    fullscreen_Toggle.setVisibility(View.GONE);
                    visibleToggle = false;

                }
            }.start();
        }
    }

    private class mywebviewclient extends WebViewClient {

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);

            System.out.println("Error: "+error.getDescription());
            System.out.println(error.getErrorCode());
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            videoProgressbar.setVisibility(View.GONE);
            webView.setVisibility(View.VISIBLE);
            ShowToggle();
        }
    }


    @Override
    protected void onDestroy() {
        webView.destroy();
        super.onDestroy();

    }


    private void populateSafetyDataSheets(){

        ArrayList training = SCMDataManager.getInstance().getTrainingVideos().get(0).getChildLists();

        if (training!=null){
            if(training.size()>0){
                int counter = 0;
                Dashboard_PimmSMS_listview_adapter adapter = new Dashboard_PimmSMS_listview_adapter(this,training , Dashboard_Pimmsms.this);
                expandableListView.setAdapter(adapter);
                for (int i = 0; i <adapter.getGroupCount() ; i++) {
                    ResourcesChildList item = (ResourcesChildList) training.get(i);
                    if (item.getGrandChildLists()!=null){
                        if (item.getGrandChildLists().size()>0){
                            ResourcesGrandChildList grandChildList =item.getGrandChildLists().get(0);
                            if (counter==0){
                                counter++;
                                DisplayData(grandChildList.getDocumentDTO(),this,this);
                            }
                        }
                    }
                    if (i <= 1){
                        expandableListView.expandGroup(i);
                    }
                }
            }
        }




    }

    private void SetUpSiteInformation(){
        try {
            title.setText(SCMDataManager.getInstance().getSelectedSite().getSite().sitename);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void Searchfunction(){

        final ArrayList<ResourcesChildList>traningData = SCMDataManager.getInstance().getTrainingVideos().get(0).getChildLists();

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int textlength = search.getText().length();
                ArrayList<String>strings = new ArrayList<>();

                if (textlength>0){
                    array_sort.clear();

                    for (ResourcesChildList dashboardTraningData : traningData){
                        boolean isparentCreated = false;
                        ArrayList<ResourcesGrandChildList> trainingVideos = dashboardTraningData.getGrandChildLists();
                        ArrayList<ResourcesGrandChildList> sortedvideo = new ArrayList<>();

                        if (textlength <= dashboardTraningData.getAppString().length()){
                            if (dashboardTraningData.getAppString().toLowerCase().trim().contains(
                                    search.getText().toString().toLowerCase().trim())){
                                if (!strings.contains(dashboardTraningData.getAppString())){
                                    strings.add(dashboardTraningData.getAppString());
                                    array_sort.add(new ResourcesChildList(dashboardTraningData.getAppString(), sortedvideo));
                                }
                            }
                        }

                        for (ResourcesGrandChildList trainingVideo: trainingVideos){
                            if (textlength <= trainingVideo.getDocumentDTO().name.length()){
                                if (trainingVideo.getDocumentDTO().name.toLowerCase().trim().contains(
                                        search.getText().toString().toLowerCase().trim())) {
                                    if (!strings.contains(dashboardTraningData.getAppString())) {
                                        strings.add(dashboardTraningData.getAppString());
                                        sortedvideo.add(trainingVideo);
                                        array_sort.add(new ResourcesChildList(dashboardTraningData.getAppString(), sortedvideo));

                                    }
                                    else {
                                        sortedvideo.add(trainingVideo);
                                    }
                                }
                            }
                        }
                    }
                    Dashboard_PimmSMS_listview_adapter adapter = new Dashboard_PimmSMS_listview_adapter(Dashboard_Pimmsms.this,array_sort, Dashboard_Pimmsms.this);
                    expandableListView.setAdapter(adapter);
                    for (int i = 0; i <adapter.getGroupCount() ; i++) {
                        expandableListView.expandGroup(i);
                    }
                }else {
                    Dashboard_PimmSMS_listview_adapter adapter = new Dashboard_PimmSMS_listview_adapter(Dashboard_Pimmsms.this,traningData, Dashboard_Pimmsms.this);
                    expandableListView.setAdapter(adapter);
                    for (int i = 0; i <adapter.getGroupCount() ; i++) {
                        if (i <= 1){
                            expandableListView.expandGroup(i);
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public static void DisplayData(DocumentDTO documentDTO,Context context,Activity activity){
        if (documentDTO.docType.toLowerCase().contains("pdf")){
            setupPDFViewer(documentDTO,context,activity);
        }else {
            setupVideoPlayer(activity);
            webView.loadUrl(documentDTO.url);
        }
    }


    public static void setupVideoPlayer(Activity activity){
        pdf_container.setVisibility(View.GONE);
        webview_container.setVisibility(View.VISIBLE);
        int orientation = activity.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            header.setVisibility(View.GONE);
            image_toggle.setBackgroundResource(R.drawable.ic_fullscreen_exit_white_48dp);
            fullscreen = true;
            webview_container.getLayoutParams().height = LinearLayout.LayoutParams.MATCH_PARENT;
            video_pdf_container.getLayoutParams().height = LinearLayout.LayoutParams.MATCH_PARENT;


        } else {
            header.setVisibility(View.VISIBLE);
            fullscreen = false;
            image_toggle.setBackgroundResource(R.drawable.ic_fullscreen_white_48dp);
            webview_container.getLayoutParams().height = SCMTool.dpToPx(270);
            video_pdf_container.getLayoutParams().height = SCMTool.dpToPx(270);
        }
    }

    public static void setupPDFViewer(DocumentDTO documentDTO, Context context,Activity activity){
        pdf_container.setVisibility(View.VISIBLE);
        webview_container.setVisibility(View.GONE);
        DownloadPDF(documentDTO,context,activity);

    }

    public static void DownloadPDF(DocumentDTO documentDTO, final Context context,Activity activity) {
        pdfProgressbar.setVisibility(View.VISIBLE);
        wait.setVisibility(View.VISIBLE);
        wait.setText("Please Wait");

        DownloadFile.Listener listener = (DownloadFile.Listener) activity;

        aPimmDataManager dataManager = aPimmDataManager.getInstance();
        String[] strings = dataManager.getAuthHeaderValue().split("Basic ");

        StringBuilder urlString = new StringBuilder();
        urlString.append(dataManager.getBaseUrl());
        urlString.append("Document/Document/GetContent/");
        urlString.append("?documentId=");
        urlString.append(documentDTO.documentId);
        urlString.append("&authorization=").append(strings[1]);

        System.out.println(urlString.toString());

        try {
            remotePDFViewPager = new RemotePDFViewPager(context, urlString.toString(), listener);
            remotePDFViewPager.setId(R.id.pdfViewPager);
        }catch (Exception e){
            pdfProgressbar.setVisibility(View.INVISIBLE);
            wait.setText("NO PREVIEW AVAILABLE");
        }

    }

    public static void open(Context context) {
        Intent i = new Intent(context, Resources_right_pane_fragment.class);
        context.startActivity(i);

    }

    public static void updateLayout() {
        wait.setVisibility(View.GONE);
        pdfProgressbar.setVisibility(View.GONE);
        remotePDFViewPager.setVisibility(View.VISIBLE);
        pdf_container.addView(remotePDFViewPager);
    }

    @Override
    public void onSuccess(String url, String destinationPath) {

        try {
            adapter = new PDFPagerAdapter(this, FileUtil.extractFileNameFromURL(url));
            remotePDFViewPager.setAdapter(adapter);
            updateLayout();

        }catch (Exception e){
            pdfProgressbar.setVisibility(View.INVISIBLE);
            wait.setText("NO PREVIEW AVAILABLE");
        }
    }

    @Override
    public void onFailure(Exception e) {
        e.printStackTrace();
        pdfProgressbar.setVisibility(View.INVISIBLE);
        wait.setText("NO PREVIEW AVAILABLE");

    }

    @Override
    public void onProgressUpdate(int progress, int total) {
        System.out.println("PROGRESS: "+progress+"/"+total);

    }


}