package com.procuro.androidscm.Restaurant.Qualification.UserProfile;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Division;
import com.procuro.apimmdatamanagerlib.CorpUser;

import java.util.ArrayList;


public class Qualification_DIvision_RecyclerViewAdapter extends RecyclerView.Adapter<Qualification_DIvision_RecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<Corp_Division> arraylist;
    private FragmentActivity fragmentActivity;

    public Qualification_DIvision_RecyclerViewAdapter(Context context, ArrayList<Corp_Division> arraylist,
                                                      FragmentActivity fragmentActivity) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;
        this.fragmentActivity = fragmentActivity;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.qualification_corp_cardview,parent,false);



        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final Corp_Division division = arraylist.get(position);

        if (division.getCorpStructures().name!=null){
            holder.category_name.setText(division.getCorpStructures().name);
        }



        CorpUser corpUser = division.getCorpStructures().corpUser;

        if (corpUser!=null){
            if (corpUser.firstName!=null){
                if (corpUser.lastName!=null){
                    holder.corp_user.setText(corpUser.firstName);
                    holder.corp_user.append(" ");
                    holder.corp_user.append(corpUser.lastName);
                }
            }
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QualificationData.getInstance().setSelectedItem(division.getCorpStructures().name);
                DisplayCorpview();
            }
        });

    }

    @Override
    public int getItemCount() {
        return arraylist.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView category_name,corp_user;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            category_name = itemView.findViewById(R.id.category_name);
            corp_user = itemView.findViewById(R.id.corp_user);
            cardView = itemView.findViewById(R.id.cardview_id);
        }
    }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }


    private void DisplayCorpview(){
        Fragment fragment = new Qualification_UserProfile_Regional_Child_level_Fragment_2();
        fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.qualification_store_profile,
                fragment).commit();
    }

}
