package com.procuro.androidscm.Restaurant.Login;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourcesChildList;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourcesGrandChildList;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourcesList;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsDateChildList;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsDateGrandChildList;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsParentList;
import com.procuro.androidscm.Restaurant.SOS.SOSData;
import com.procuro.androidscm.Restaurant.Schema.Actions;
import com.procuro.androidscm.Restaurant.Schema.Dayparts;
import com.procuro.androidscm.Restaurant.Schema.Days;
import com.procuro.androidscm.Restaurant.Schema.Forms;
import com.procuro.androidscm.Restaurant.Schema.Schema;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.ApplicationReport;
import com.procuro.apimmdatamanagerlib.CorpStructure;
import com.procuro.apimmdatamanagerlib.DocumentDTO;
import com.procuro.apimmdatamanagerlib.FormPack;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.PimmForm;
import com.procuro.apimmdatamanagerlib.SiteSettings;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;
import com.procuro.apimmthriftservices.services.NamedServices;

import org.apache.thrift.transport.TTransportException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;


public class LoginBackGroundProcess extends AsyncTask<String, Void, Boolean> {
    private ArrayList<CorpStructure>corpStructures;
    private JSONArray siteObject;
    private Activity activity;
    private aPimmDataManager dataManager = aPimmDataManager.getInstance();
    private ArrayList<ResourcesList> resourcesLists = new ArrayList<>();
    private ArrayList<ResourcesList> TrainingVideos = new ArrayList<>();


    public LoginBackGroundProcess(ArrayList<CorpStructure>corpStructures , JSONArray siteObject,Activity activity) {
        this.corpStructures = corpStructures;
        this.siteObject = siteObject;
        this.activity = activity;
    }

    @Override
    protected Boolean doInBackground(String... strings) {

        try {

            InitializedThriftAPI();

            Download_getFormDefinitionListForAppId();

            StartCleaningThread();

            StartReportThread();

            ParseJson();


        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    public void ParseJson(){

        if (SCMDataManager.getInstance().getCountries()==null){

            InputStream inputStream = activity.getResources().openRawResource(R.raw.countries);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            int ctr;
            try {
                ctr = inputStream.read();
                while (ctr != -1) {
                    byteArrayOutputStream.write(ctr);
                    ctr = inputStream.read();
                }
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                JSONArray jsonArray = new JSONArray(byteArrayOutputStream.toString());
                String[]countries = new String[jsonArray.length()];
                String[]code = new String[jsonArray.length()];

                for (int i = 0; i <jsonArray.length() ; i++) {
                    countries[i] = jsonArray.getJSONObject(i).getString("name");
                    code[i] = jsonArray.getJSONObject(i).getString("abbreviation");
                }

                SCMDataManager.getInstance().setCountries(countries);
                SCMDataManager.getInstance().setCountrycode(code);

            } catch (Exception e) {
                e.printStackTrace();
            }

        }



    }

    private void InitializedThriftAPI() {
        try {
            Log.i("Thrift ","INITIALIZING SERVER START");
            NamedServices services = NamedServices.getInstance();
            aPimmDataManager dataManager = aPimmDataManager.getInstance();
            SCMDataManager scmDataManager = SCMDataManager.getInstance();

            StringBuilder baseUrl = new StringBuilder();
            baseUrl.append("http://");
            baseUrl.append(scmDataManager.getSPID());
            baseUrl.append(".pimm.us");

            services.start(baseUrl.toString(), dataManager.getAuthHeaderValue().replace("Basic ",""));
            Log.i("Thrift ","INITIALIZING SERVER END");


        }catch (TTransportException e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
        System.out.println(Arrays.toString(values));
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
    }

    private void StartReportThread(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                Download_getReportListforLoggedInUser();
            }
        }).start();
    }

    private void Download_getReportListforLoggedInUser(){
        try {
            dataManager.getReportListforLoggedInUser(new OnCompleteListeners.getReportListforLoggedInUserCallbackListener() {
                @Override
                public void getReportListforLoggedInUserCallback(ArrayList<ApplicationReport> applicationReports, Error error) {
                    if (error == null) {
                        SCMDataManager.getInstance().setReportLevel("site");
                        setupReportsInspection(applicationReports);

                    }else {
                        Download_getReportListforLoggedInUser();
                    }
                }
            });

        }catch (Exception e){
            e.printStackTrace();

        }
    }

    private void setupReportsDelivery(ArrayList<ApplicationReport> applicationReports) {

        ArrayList<ReportsParentList> delivery = new ArrayList<>();
        delivery.add(new ReportsParentList("Delivery Performance"));
        delivery.add(new ReportsParentList("Delivert Inspections Reports"));
        for (int i = 0; i < delivery.size(); i++) {
            ArrayList<ApplicationReport> applicationReportArrayList = new ArrayList<>();
            if (i == 0) {
                for (ApplicationReport report : applicationReports) {
                    if (report.Application.equalsIgnoreCase("SCM")){
                        if(report.Filter.equalsIgnoreCase("Delivery")){
                            if (report.Category.equalsIgnoreCase("Delivery Performance")){
                                if (report.ReportLevel.equalsIgnoreCase(SCMDataManager.getInstance().getReportLevel())){
                                    applicationReportArrayList.add(report);
                                }
                            }
                        }
                    }
                }
                delivery.get(i).setApplicationReports(applicationReportArrayList);

            } else {
                delivery.get(i).setWeeklyArrayList(GenerateDateReports());
            }
        }
        SCMDataManager.getInstance().setReports_delivery(delivery);
        setupReportsRatings(applicationReports);

    }

    private void setupReportsRatings(ArrayList<ApplicationReport>applicationReports) {

        ArrayList<ReportsParentList>ratings = new ArrayList<>();
        ReportsParentList rating = null;
        ArrayList<String>stringArrayList = new ArrayList<>();

        try {
            for (ApplicationReport applicationReport : applicationReports){
                if (applicationReport.Application.equalsIgnoreCase("SCM")){
                    if(applicationReport.Filter.equalsIgnoreCase("Ratings")){
                        if (applicationReport.ReportLevel.equalsIgnoreCase(SCMDataManager.getInstance().getReportLevel())){
                            if (stringArrayList.contains(applicationReport.Category)){
                                for (ReportsParentList parentList: ratings){
                                    if (parentList.getTitle().equalsIgnoreCase(applicationReport.Category)){
                                        parentList.getApplicationReports().add(applicationReport);
                                    }
                                }
                            }else {
                                ArrayList<ApplicationReport>reports = new ArrayList<>();
                                reports.add(applicationReport);
                                stringArrayList.add(applicationReport.Category);
                                rating = new ReportsParentList(applicationReport.Category);
                                rating.setApplicationReports(reports);
                                ratings.add(rating);
                            }

                        }
                    }
                }
            }


        }catch (Exception e){
            e.printStackTrace();
        }

        SCMDataManager.getInstance().setReport_ratings(ratings);
        setupReportsAudit(applicationReports);
    }

    private void setupReportsAudit(ArrayList<ApplicationReport>applicationReports) {
        ArrayList<ReportsParentList>AuditReports = new ArrayList<>();
        AuditReports.add(new ReportsParentList("Completed Audits"));
        AuditReports.add(new ReportsParentList("Audit Performance"));
        AuditReports.add(new ReportsParentList("Store Audits - Offical"));
        AuditReports.add(new ReportsParentList("Store Audits - Practice"));

        for (ApplicationReport applicationReport : applicationReports){
            if (applicationReport.Application.equalsIgnoreCase("SCM")){
                if(applicationReport.Filter.equalsIgnoreCase("Audit")){
                    if (applicationReport.ReportLevel.equalsIgnoreCase(SCMDataManager.getInstance().getReportLevel())){
                        for (ReportsParentList parentList: AuditReports){
                            if (applicationReport.Category.equalsIgnoreCase(parentList.getTitle())){
                                ArrayList<String>title = new ArrayList<>();
                                title.add(parentList.getTitle());
                                if (parentList.getApplicationReports() == null){
                                    ArrayList<ApplicationReport>reports = new ArrayList<>();
                                    reports.add(applicationReport);
                                    parentList.setApplicationReports(reports);
                                }else {
                                    if (!title.contains(applicationReport.ReportName)){
                                        parentList.getApplicationReports().add(applicationReport);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        SCMDataManager.getInstance().setReports_audits(AuditReports);


    }

    private void setupReportsInspection(ArrayList<ApplicationReport> applicationReports) {
        ArrayList<ReportsParentList> inspections = new ArrayList<>();
        inspections.add(new ReportsParentList("Inspection Performance"));
        inspections.add(new ReportsParentList("Detail Inspections Reports"));
        try {
            for (int i = 0; i < inspections.size(); i++) {
                ArrayList<ApplicationReport> applicationReportArrayList = new ArrayList<>();
                if (i == 0) {
                    for (ApplicationReport report : applicationReports) {
                        if (report.Application.equalsIgnoreCase("SCM")) {
                            if (report.Filter.equalsIgnoreCase("Inspections")) {
                                if (report.Category.equalsIgnoreCase("Inspections Performance")) {
                                    if (report.ReportLevel.equalsIgnoreCase(SCMDataManager.getInstance().getReportLevel())) {
                                        applicationReportArrayList.add(report);
                                    }
                                }
                            }
                        }
                    }

                    inspections.get(i).setApplicationReports(applicationReportArrayList);

                } else {
                    inspections.get(i).setWeeklyArrayList(GenerateDateReports());
                }
            }


        }catch (Exception e){
            e.printStackTrace();
        }

        SCMDataManager.getInstance().setReports_inspections(inspections);
        setupReportsDelivery(applicationReports);


    }

    private ArrayList<ReportsDateChildList> GenerateDateReports() {

        ArrayList<ReportsDateChildList> reportsDateChildLists = new ArrayList<>();
        Calendar localCalendar = Calendar.getInstance();

        SimpleDateFormat monthformat = new SimpleDateFormat("MMMM yyyy");
        Calendar cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getTimeZone("UTC"));
        int maxWeeknumber = cal.getActualMaximum(Calendar.WEEK_OF_MONTH);

        for (int j = 0; j < maxWeeknumber; j++) {
            cal.set(Calendar.WEEK_OF_MONTH, j + 1);
            int weekOfYear = cal.get(Calendar.WEEK_OF_YEAR);
            cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
            cal.add(Calendar.DATE, 1);
            cal.set(Calendar.AM_PM,Calendar.AM);
            cal.set(Calendar.HOUR,12);
            cal.set(Calendar.MINUTE,1);
            cal.set(Calendar.SECOND,0);
            cal.set(Calendar.MILLISECOND,0);

            Date startDate = cal.getTime();
            ArrayList<ReportsDateGrandChildList> dailies = new ArrayList<>();
            String daily = "Week " + weekOfYear + " Weekly Report";

            Calendar current = Calendar.getInstance();
            current.setTimeZone(TimeZone.getTimeZone("UTC"));

            if (current.get(Calendar.WEEK_OF_YEAR)>= weekOfYear){

                dailies.add(new ReportsDateGrandChildList(daily, startDate,false,true));
            }else {
                dailies.add(new ReportsDateGrandChildList(daily, startDate,false,false));
            }

            for (int k = 0; k < 7; k++) {
                SimpleDateFormat fmtOutput = new SimpleDateFormat("EEEE, d MMMM yyyy");
                Calendar daysCalendar = Calendar.getInstance();
                daysCalendar.setTimeZone(TimeZone.getTimeZone("UTC"));
                daysCalendar.setTime(cal.getTime());
                daysCalendar.add(Calendar.DATE, k);
                daysCalendar.set(Calendar.AM_PM,Calendar.AM);
                daysCalendar.set(Calendar.HOUR,12);
                daysCalendar.set(Calendar.MINUTE,1);
                daysCalendar.set(Calendar.SECOND,0);
                daysCalendar.set(Calendar.MILLISECOND,0);

                String days = fmtOutput.format(daysCalendar.getTime());
                Calendar currentdate = Calendar.getInstance();

                if (daysCalendar.compareTo(currentdate)<= 0){
                    dailies.add(new ReportsDateGrandChildList(days, daysCalendar.getTime(),false,true));

                }else if (daysCalendar.get(Calendar.YEAR)<=currentdate.get(Calendar.YEAR)){

                    if (daysCalendar.get(Calendar.MONTH)<=currentdate.get(Calendar.MONTH)){

                        if (daysCalendar.get(Calendar.DATE)<=currentdate.get(Calendar.DATE)){

                            dailies.add(new ReportsDateGrandChildList(days, daysCalendar.getTime(),false,true));
                        }else {
                            dailies.add(new ReportsDateGrandChildList(days, daysCalendar.getTime(),false,false));
                        }
                    }else {
                        dailies.add(new ReportsDateGrandChildList(days, daysCalendar.getTime(),false,false));
                    }
                } else {
                    dailies.add(new ReportsDateGrandChildList(days, daysCalendar.getTime(),false,false));

                }

            }
            cal.add(Calendar.DATE, 6);
            Date endDate = cal.getTime();
            SimpleDateFormat fmtOutput = new SimpleDateFormat("MMM dd");

            String firstdate = fmtOutput.format(startDate);
            String lastdate = fmtOutput.format(endDate);
            String parent_title = "Week " + weekOfYear + " (" + firstdate + " - " + lastdate + ")";

            Calendar currentdate = Calendar.getInstance();
            currentdate.setTimeZone(TimeZone.getTimeZone("UTC"));

            if (weekOfYear == current.get(Calendar.WEEK_OF_YEAR)){
                reportsDateChildLists.add(new ReportsDateChildList(parent_title, startDate, endDate, weekOfYear, dailies, true));
            }else {
                reportsDateChildLists.add(new ReportsDateChildList(parent_title, startDate, endDate, weekOfYear, dailies, false));
            }
        }
        return reportsDateChildLists;

    }


    private void StartCleaningThread(){

        new Thread(new Runnable() {
            @Override
            public void run() {
                Download_getDocumentListForReferenceId();
            }
        }).start();

    }

    private void Download_getDocumentListForReferenceId(){

        if (SCMDataManager.getInstance().getSelectedSite() != null){
            dataManager.getDocumentListForReferenceId(SCMDataManager.getInstance().getSelectedSite().getSite().CustomerId, new OnCompleteListeners.getDocumentListForReferenceIdCallbackListener() {
                @Override
                public void getDocumentListForReferenceIdCallback(ArrayList<DocumentDTO> documentDTOArrayList, Error error) {
                    if (error == null) {
                        FilterCleaningGuides(documentDTOArrayList);
                    }
                }
            });
        }else {
            dataManager.getDocumentListForReferenceId(SCMDataManager.getInstance().getCustomSites().get(0).getSite().CustomerId, new OnCompleteListeners.getDocumentListForReferenceIdCallbackListener() {
                @Override
                public void getDocumentListForReferenceIdCallback(ArrayList<DocumentDTO> documentDTOArrayList, Error error) {
                    if (error == null) {
                        FilterCleaningGuides(documentDTOArrayList);
                    }
                }
            });
        }
    }

    private void FilterCleaningGuides(final ArrayList<DocumentDTO>documentDTOS){
        ArrayList<ResourcesGrandChildList>backroom = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>CustomerView = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>Production = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>Service = new ArrayList<>();

        for (DocumentDTO dto : documentDTOS){
            if (dto.appString.contains("Backroom_")){
                backroom.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("Customer_View")){
                CustomerView.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("Production")){
                Production.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("Service")){
                Service.add(new ResourcesGrandChildList(dto));
            }
        }

        SCMDataManager.getInstance().setDashboardbackroom(backroom);
        SCMDataManager.getInstance().setDashboardcustomerview(CustomerView);
        SCMDataManager.getInstance().setDashboardproduction(Production);
        SCMDataManager.getInstance().setDashboardservices(Service);


        ArrayList<ResourcesChildList>CleaningGuides = new ArrayList<>();
        CleaningGuides.add(new ResourcesChildList("Backroom",backroom));
        CleaningGuides.add(new ResourcesChildList("Customer View",CustomerView));
        CleaningGuides.add(new ResourcesChildList("Production",Production));
        CleaningGuides.add(new ResourcesChildList("Service",Service));

        resourcesLists.add(new ResourcesList("Cleaning Guides",CleaningGuides));

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {

                FilterFoodSafety(documentDTOS);
            }
        });
    }

    private void FilterFoodSafety(final ArrayList<DocumentDTO>documentDTOS){

        ArrayList<ResourcesGrandChildList>instruction = new ArrayList<>();

        for (DocumentDTO dto : documentDTOS){
            if (dto.category.contains("Resources")){
                instruction.add(new ResourcesGrandChildList(dto));
            }
        }

        Collections.sort(instruction, new Comparator<ResourcesGrandChildList>() {
            @Override
            public int compare(ResourcesGrandChildList o1, ResourcesGrandChildList o2) {
                return o1.getDocumentDTO().name.compareToIgnoreCase(o2.getDocumentDTO().name);
            }
        });


        ArrayList<ResourcesChildList>FoodSafety = new ArrayList<>();
        FoodSafety.add(new ResourcesChildList("Instruction Manuals",instruction));

        resourcesLists.add(new ResourcesList("Food Safety",FoodSafety));


        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                FilterPlaybook(documentDTOS);
            }
        });

    }

    private void FilterPlaybook(final ArrayList<DocumentDTO>documentDTOS){

        ArrayList<ResourcesGrandChildList>introduction = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>taste = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>friendliness = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>speed = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>accuracy = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>cleanliness = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>core = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>welearn = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>instructions = new ArrayList<>();

        for (DocumentDTO dto : documentDTOS){
            if (dto.appString.contains("Introduction")){
                introduction.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("Taste")){
                taste.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("Friendliness")){
                friendliness.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("Speed")){
                speed.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("Accuracy")){
                accuracy.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("Cleanliness")){
                cleanliness.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("Core_Operations")){
                core.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("We_Learn")){
                welearn.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("Instructions_And_Forms")){
                instructions.add(new ResourcesGrandChildList(dto));
            }
        }

        SCMDataManager.getInstance().setDashboardTaste(taste);
        SCMDataManager.getInstance().setDashboardFriendliness(friendliness);
        SCMDataManager.getInstance().setDashboardSpeed(speed);
        SCMDataManager.getInstance().setDashboardAccuracy(accuracy);
        SCMDataManager.getInstance().setDashboardCleanliness(cleanliness);

        ArrayList<ResourcesChildList>PlayBook = new ArrayList<>();
        PlayBook.add(new ResourcesChildList("Introduction",introduction));
        PlayBook.add(new ResourcesChildList("Taste",taste));
        PlayBook.add(new ResourcesChildList("Friendliness",friendliness));
        PlayBook.add(new ResourcesChildList("Speed",speed));
        PlayBook.add(new ResourcesChildList("Accuracy",accuracy));
        PlayBook.add(new ResourcesChildList("Cleanliness",cleanliness));
        PlayBook.add(new ResourcesChildList("Core Operations",core));
        PlayBook.add(new ResourcesChildList("We Learn",welearn));
        PlayBook.add(new ResourcesChildList("Instructions And Forms",instructions));

        resourcesLists.add(new ResourcesList("PlayBook",PlayBook));

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {

                FilterSafetyDataSheets(documentDTOS);
            }
        });
    }

    private void FilterSafetyDataSheets(final ArrayList<DocumentDTO>documentDTOS){

        ArrayList<ResourcesGrandChildList>airCarbonGasses = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>FireExtinguishants = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>CleaningProducts = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>Disinfectants = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>Emergency_Procedures = new ArrayList<>();

        for (DocumentDTO dto : documentDTOS){
            if (dto.appString.contains("Air_Carbon_Gasses")){
                airCarbonGasses.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("Fire_Extinguishants")){
                FireExtinguishants.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("Cleaning_Products")){
                CleaningProducts.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("Disinfectants")){
                Disinfectants.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("Emergency_Procedures")){
                Emergency_Procedures.add(new ResourcesGrandChildList(dto));
            }

            if (dto.category.equalsIgnoreCase("Training_Data")){
                System.out.println("appString: "+dto.appString);
                System.out.println("name: "+dto.name);
                System.out.println("category: "+dto.category);
            }
        }


        Collections.sort(airCarbonGasses, new Comparator<ResourcesGrandChildList>() {
            @Override
            public int compare(ResourcesGrandChildList o1, ResourcesGrandChildList o2) {
                return o1.getDocumentDTO().name.compareToIgnoreCase(o2.getDocumentDTO().name);
            }
        });

        Collections.sort(CleaningProducts, new Comparator<ResourcesGrandChildList>() {
            @Override
            public int compare(ResourcesGrandChildList o1, ResourcesGrandChildList o2) {
                return o1.getDocumentDTO().name.compareToIgnoreCase(o2.getDocumentDTO().name);
            }
        });

        Collections.sort(Disinfectants, new Comparator<ResourcesGrandChildList>() {
            @Override
            public int compare(ResourcesGrandChildList o1, ResourcesGrandChildList o2) {
                return o1.getDocumentDTO().name.compareToIgnoreCase(o2.getDocumentDTO().name);
            }
        });

        Collections.sort(Emergency_Procedures, new Comparator<ResourcesGrandChildList>() {
            @Override
            public int compare(ResourcesGrandChildList o1, ResourcesGrandChildList o2) {
                return o1.getDocumentDTO().name.compareToIgnoreCase(o2.getDocumentDTO().name);
            }
        });

        ArrayList<ResourcesChildList>SafetyDataSheets = new ArrayList<>();

        SafetyDataSheets.add(new ResourcesChildList("Air Carbon Gasses",airCarbonGasses));
        SafetyDataSheets.add(new ResourcesChildList("Fire Extinguishants",FireExtinguishants));
        SafetyDataSheets.add(new ResourcesChildList("Cleaning Products",CleaningProducts));
        SafetyDataSheets.add(new ResourcesChildList("Disinfectants",Disinfectants));
        SafetyDataSheets.add(new ResourcesChildList("Emergency Procedures",Emergency_Procedures));
        resourcesLists.add(new ResourcesList("Safety Data Sheets",SafetyDataSheets));
        SCMDataManager.getInstance().setSafetyDataSheets(SafetyDataSheets);

        SCMDataManager.getInstance().setResourcesLists(resourcesLists);

        FilterVTA(documentDTOS);
    }

    private void FilterVTA(final ArrayList<DocumentDTO>documentDTOS){

        ArrayList<ResourcesChildList>vta = new ArrayList<>();
        ArrayList<String>appstring = new ArrayList<>();

        for (DocumentDTO dto : documentDTOS){
            if (dto.category.contains("Visual_Training_Aids")){
                if (!appstring.contains(dto.appString)){
                    ArrayList<ResourcesGrandChildList>child = new ArrayList<>();
                    appstring.add(dto.appString);
                    child.add(new ResourcesGrandChildList(dto));
                    vta.add(new ResourcesChildList(dto.appString,child));
                }else {
                    for (ResourcesChildList childList : vta){
                        if (childList.getAppString().equalsIgnoreCase(dto.appString)){
                            childList.getGrandChildLists().add(new ResourcesGrandChildList(dto));
                        }
                    }
                }
            }
        }

        Collections.sort(vta, new Comparator<ResourcesChildList>() {
            @Override
            public int compare(ResourcesChildList o1, ResourcesChildList o2) {
                return o1.getAppString().compareToIgnoreCase(o2.getAppString());
            }
        });


        for (ResourcesChildList childList : vta){

            Collections.sort(childList.getGrandChildLists(), new Comparator<ResourcesGrandChildList>() {
                @Override
                public int compare(ResourcesGrandChildList o1, ResourcesGrandChildList o2) {
                    return o1.getDocumentDTO().name.compareToIgnoreCase(o2.getDocumentDTO().name);
                }
            });

        }
        SCMDataManager.getInstance().setVtas(vta);
        FilterByInspectionModule(documentDTOS);

    }

    private void FilterByInspectionModule(final ArrayList<DocumentDTO>documentDTOS){

        ArrayList<String>categories= new ArrayList<>();
        ArrayList<ResourcesChildList>PIMM_SMS = new ArrayList<>();

        for (DocumentDTO dto : documentDTOS){
            if (dto.category.equalsIgnoreCase("PIMM_SMS")){
                String appstring = dto.appString.replace("_"," ");
                if (!categories.contains(appstring)){
                 categories.add(appstring);
                 ArrayList<ResourcesGrandChildList>grandChildLists = new ArrayList<>();
                 grandChildLists.add(new ResourcesGrandChildList(dto));
                 PIMM_SMS.add(new ResourcesChildList(appstring,grandChildLists));
                }else {
                    for (ResourcesChildList childList : PIMM_SMS){
                        if (childList.getAppString().equalsIgnoreCase(appstring)){
                            childList.getGrandChildLists().add(new ResourcesGrandChildList(dto));
                        }
                    }
                }
            }
        }


        Collections.sort(PIMM_SMS, new Comparator<ResourcesChildList>() {
            @Override
            public int compare(ResourcesChildList o1, ResourcesChildList o2) {
                return o1.getAppString().compareTo(o2.getAppString());
            }

        });

        for (ResourcesChildList childList : PIMM_SMS){
            Collections.sort(childList.getGrandChildLists(), new Comparator<ResourcesGrandChildList>() {
                @Override
                public int compare(ResourcesGrandChildList o1, ResourcesGrandChildList o2) {
                    return o1.getDocumentDTO().name.compareToIgnoreCase(o2.getDocumentDTO().name);
                }
            });
        }

        TrainingVideos.add(new ResourcesList("PIMM SMS",PIMM_SMS));
        SCMDataManager.getInstance().setTrainingVideos(TrainingVideos);

    }

    private void Download_getFormDefinitionListForAppId(){
        dataManager.getFormDefinitionListForAppId(new OnCompleteListeners.getFormDefinitionListForAppIdCallbackListener() {
            @Override
            public void getFormDefinitionListForAppIdCallback(ArrayList<PimmForm> pimmFormArrayList, Error error) {
                if (error == null){
                    SCMDataManager.getInstance().setPimmFormArrayList(pimmFormArrayList);
                }
            }
        });
    }

}
