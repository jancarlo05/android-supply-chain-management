package com.procuro.androidscm.Restaurant.ReportsFragment;

import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Regional;
import com.procuro.androidscm.Restaurant.ReportsFragment.Inspection.FoodSafetySummary.FSLRegion;
import com.procuro.androidscm.Restaurant.SOS.SOSData;
import com.procuro.apimmdatamanagerlib.FSLDaySummary;
import com.procuro.apimmthriftservices.services.auditForm.FSLDataAttachment;
import com.procuro.apimmthriftservices.services.auditForm.FSLDataForm;
import com.procuro.apimmthriftservices.services.auditForm.FSLDataItem;
import com.procuro.apimmthriftservices.services.auditForm.FSLDataNote;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsConfiguration;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsEquipment;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsSiteConfig;

import java.util.ArrayList;
import java.util.Date;

public class ReportsData {
    private static ReportsData instance = new ReportsData();

    private CustomSite site;
    private FSLDataForm form;
    private ArrayList<SiteSettingsEquipment>equipment;
    private ArrayList<SiteSettingsEquipment>enabledEquipments;
    private ArrayList<SiteSettingsConfiguration>settingsSiteConfigs;

    private ArrayList<FSLDataItem>overall_items;
    private ArrayList<FSLDataAttachment>overall_attachments;
    private ArrayList<FSLDataNote>overll_notes;
    private ArrayList<FSLDataItem>inspection;
    private ArrayList<ReportsInspectionType> reportsInspectionTypes;

    private ArrayList<ReportsDateChildList>DetailInspectionReport;

    private Date startDate;
    private Date endDate;

    private boolean disableDefaultView;

    private FSLDaySummary fslDaySummary;
    private ArrayList<Object>navigations ;
    private Object selectedCorpStructure;
    private ArrayList<FSLRegion> regions;

    private ReportsInspectionType currentInspectionType;


    public ArrayList<SiteSettingsConfiguration> getSettingsSiteConfigs() {
        return settingsSiteConfigs;
    }

    public void setSettingsSiteConfigs(ArrayList<SiteSettingsConfiguration> settingsSiteConfigs) {
        this.settingsSiteConfigs = settingsSiteConfigs;
    }

    public ReportsInspectionType getCurrentInspectionType() {
        return currentInspectionType;
    }

    public void setCurrentInspectionType(ReportsInspectionType currentInspectionType) {
        this.currentInspectionType = currentInspectionType;
    }

    public ArrayList<FSLRegion> getRegions() {
        return regions;
    }

    public void setRegions(ArrayList<FSLRegion> regions) {
        this.regions = regions;
    }

    public static ReportsData getInstance() {
        if (instance == null) {
            instance = new ReportsData();
        }
        return instance;
    }

    public Object getSelectedCorpStructure() {
        return selectedCorpStructure;
    }

    public void setSelectedCorpStructure(Object selectedCorpStructure) {
        this.selectedCorpStructure = selectedCorpStructure;
    }

    public ArrayList<Object> getNavigations() {
        return navigations;
    }

    public void setNavigations(ArrayList<Object> navigations) {
        this.navigations = navigations;
    }

    public FSLDaySummary getFslDaySummary() {
        return fslDaySummary;
    }

    public void setFslDaySummary(FSLDaySummary fslDaySummary) {
        this.fslDaySummary = fslDaySummary;
    }

    public CustomSite getSite() {
        return site;
    }

    public void setSite(CustomSite site) {
        this.site = site;
    }

    public boolean isDisableDefaultView() {
        return disableDefaultView;
    }

    public void setDisableDefaultView(boolean disableDefaultView) {
        this.disableDefaultView = disableDefaultView;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public ArrayList<ReportsDateChildList> getDetailInspectionReport() {
        return DetailInspectionReport;
    }

    public void setDetailInspectionReport(ArrayList<ReportsDateChildList> detailInspectionReport) {
        DetailInspectionReport = detailInspectionReport;
    }

    public ArrayList<SiteSettingsEquipment> getEnabledEquipments() {
        return enabledEquipments;
    }

    public void setEnabledEquipments(ArrayList<SiteSettingsEquipment> enabledEquipments) {
        this.enabledEquipments = enabledEquipments;
    }

    public ArrayList<ReportsInspectionType> getReportsInspectionTypes() {
        return reportsInspectionTypes;
    }

    public void setReportsInspectionTypes(ArrayList<ReportsInspectionType> reportsInspectionTypes) {
        this.reportsInspectionTypes = reportsInspectionTypes;
    }

    public ArrayList<FSLDataItem> getInspection() {
        return inspection;
    }

    public void setInspection(ArrayList<FSLDataItem> inspection) {
        this.inspection = inspection;
    }

    public ArrayList<FSLDataItem> getOverall_items() {
        return overall_items;
    }

    public void setOverall_items(ArrayList<FSLDataItem> overall_items) {
        this.overall_items = overall_items;
    }

    public ArrayList<FSLDataAttachment> getOverall_attachments() {
        return overall_attachments;
    }

    public void setOverall_attachments(ArrayList<FSLDataAttachment> overall_attachments) {
        this.overall_attachments = overall_attachments;
    }

    public ArrayList<FSLDataNote> getOverll_notes() {
        return overll_notes;
    }

    public void setOverll_notes(ArrayList<FSLDataNote> overll_notes) {
        this.overll_notes = overll_notes;
    }

    public ArrayList<SiteSettingsEquipment> getEquipment() {
        return equipment;
    }

    public void setEquipment(ArrayList<SiteSettingsEquipment> equipment) {
        this.equipment = equipment;
    }

    public static void setInstance(ReportsData instance) {
        ReportsData.instance = instance;
    }

    public FSLDataForm getForm() {
        return form;
    }

    public void setForm(FSLDataForm form) {
        this.form = form;
    }
}
