package com.procuro.androidscm.Restaurant.ReportsFragment.Delivery;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.diegocarloslima.fgelv.lib.FloatingGroupExpandableListView;
import com.diegocarloslima.fgelv.lib.WrapperExpandableListAdapter;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsParentList;
import com.procuro.androidscm.SCMDataManager;


import java.util.ArrayList;

public class Reports_delivery_fragment extends Fragment {


    public static WrapperExpandableListAdapter wrapperAdapter;
    public static ExpandableListView expandableListView;
    private TextView current_date;
    public static Button review_verify;
    public static  Reports_delivery_list_adapter adapter;
    public static  boolean isFoodSafetyshowing = false;

    public Reports_delivery_fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.reports_left_pane_inspection, container, false);
        expandableListView = rootView.findViewById(R.id.expandible_listview);

        populate_delivery_list(SCMDataManager.getInstance().getReports_delivery());
        return rootView;


    }

    public void populate_delivery_list(ArrayList<ReportsParentList>deliveryLists){

        adapter = new Reports_delivery_list_adapter(getContext(), deliveryLists,getActivity(),expandableListView);
        expandableListView.setAdapter(adapter);
        for (int i = 0; i < adapter.getGroupCount(); i++) {
            expandableListView.expandGroup(i);
        }
        expandableListView.setDividerHeight(0);

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                return true; // This way the expander cannot be collapsed
            }
        });


    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}