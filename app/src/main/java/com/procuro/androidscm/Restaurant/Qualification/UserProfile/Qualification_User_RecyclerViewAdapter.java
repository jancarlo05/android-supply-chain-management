package com.procuro.androidscm.Restaurant.Qualification.UserProfile;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileActivity;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileActivityOverAll;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileData;

import java.util.ArrayList;


public class Qualification_User_RecyclerViewAdapter extends RecyclerView.Adapter<Qualification_User_RecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<CustomUser> arraylist;
    private FragmentActivity fragmentActivity;
    private Bundle fragmentbundle;
    private CustomSite customSite;

    public Qualification_User_RecyclerViewAdapter(Context context, ArrayList<CustomUser> arraylist,
                                                  FragmentActivity fragmentActivity,CustomSite customSite) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;
        this.fragmentActivity = fragmentActivity;
        this.customSite = customSite;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.qualification_employee_cardview,parent,false);



        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final CustomUser user = arraylist.get(position);

        holder.name.setText(user.getUser().firstName);
        holder.name.append(" ");
        holder.name.append(user.getUser().lastName);

        if (user.getUser().roles!=null){
            holder.position.setText(Permission(user.getUser().roles));
        }


        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(fragmentActivity, UserProfileActivityOverAll.class);
                UserProfileData data = new UserProfileData();
                data.setUserEditEnabled(false);
                data.setCustomSite(customSite);
                data.setCustomUser(user);
                data.setIsupdate(true);
                data.setFromQualification(true);
                data.setPrevActivity("UserProfile");
                UserProfileData.setInstance(data);
                fragmentActivity.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return arraylist.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name,position;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            cardView = itemView.findViewById(R.id.cardview_id);
            position = itemView.findViewById(R.id.position);
        }
    }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    private String Permission(ArrayList<String>stringss){
        String permission = "";
        ArrayList<String>roles = new ArrayList<>();

        for (String strings: stringss){
            if (strings.equalsIgnoreCase("GM".toUpperCase())){
                roles.add(strings);
                permission = "General Manager";
            }
            else if (strings.equalsIgnoreCase("Store admin".toUpperCase())){
                permission = "Store admin";
                roles.add(strings);
            }
            else if (strings.equalsIgnoreCase("Auditor".toUpperCase())){
                permission = "Auditor";
                roles.add(strings);
            }
            else if (strings.equalsIgnoreCase("Ops leader".toUpperCase())){
                permission = "Support Manager";
                roles.add(strings);
            }
            else if (strings.equalsIgnoreCase("FSL".toUpperCase())) {
                permission = "Inspector";
                roles.add(strings);
            }
            else if (strings.equalsIgnoreCase("Employee".toUpperCase())) {
                permission = "Employee";
                roles.add(strings);

            }else if (strings.equalsIgnoreCase("DM".toUpperCase())) {
                permission = "District Manager";
                roles.add(strings);

            }else if (strings.equalsIgnoreCase("RELIEF CREW".toUpperCase())) {
                permission = "Relief Crew";
                roles.add(strings);
            }
            else if (strings.equalsIgnoreCase("RELIEF MGR".toUpperCase())) {
                permission = "Relief Manager";
                roles.add(strings);
            }
            else if (strings.equalsIgnoreCase("SUPERVISOR".toUpperCase())) {
                permission = "Supervisor";
                roles.add(strings);
            }

        }

        return permission;
    }




}
