package com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.EquipmenList;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.FormsList.FormListData;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.Qualification_StoreInfo_Fragment;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmthriftservices.services.NamedServices;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsConfiguration;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsEquipment;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsSiteConfig;
import com.suke.widget.SwitchButton;

import org.apache.thrift.TException;

import java.util.ArrayList;

public class EquipmentListActivity extends AppCompatActivity {

    private CustomSite site;
    private info.hoang8f.android.segmented.SegmentedGroup infogrp;
    private RadioButton grills_and_cookers, coolers_and_freezer,other;
    private Button back,save;
    private ListView listView ;
    private EquipmentList_Listview_Adapter adapter;
    private TextView message;
    private LinearLayout root;
    private com.suke.widget.SwitchButton switchButton;
    ArrayList<CustomEquipment> equipments;
    private SiteSettingsConfiguration blueToothProbe = FormListData.getInstance().getBlueToothProbe();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qualification_equipment_list_fragment);

        back = findViewById(R.id.home);
        grills_and_cookers = findViewById(R.id.grills_and_cookers);
        coolers_and_freezer = findViewById(R.id.coolers_and_freezers);
        other = findViewById(R.id.others);
        infogrp = findViewById(R.id.segmented2);
        listView = findViewById(R.id.listView);
        message = findViewById(R.id.message);
        save = findViewById(R.id.save);
        root = findViewById(R.id.root);
        switchButton =findViewById(R.id.switch_button);

        setupOnclicks();

        DisplaySiteInfo();

        setUpBluetoothProbe();
    }

    private void setUpBluetoothProbe(){

        if (blueToothProbe.value.equalsIgnoreCase("true")){
           switchButton.setChecked(true);
        }else {
            switchButton.setChecked(false);
        }
        switchButton.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                blueToothProbe.setValue(String.valueOf(isChecked));
                EquipmenlistData.getInstance().setProbeUpdated(true);
                SCMTool.EnableView(save,1);
            }
        });

        if (!SCMTool.isUserHasAccessToEdit()){
            SCMTool.DisableView(switchButton,.5f);
        }

    }

    private void DisplaySiteInfo(){

        if (QualificationData.getInstance().getSelectedSite()!=null){
            this.site = QualificationData.getInstance().getSelectedSite();
        }
        else {
            this.site = SCMDataManager.getInstance().getSelectedSite();
        }

        System.out.println("Selected Site ID: "+this.site.getSite().siteid);


        EquipmenlistData data = EquipmenlistData.getInstance();
        if (data.isUnsaveEdit()){
            save.setAlpha(1);
            save.setEnabled(true);

        }else {
            save.setAlpha(.5f);
            save.setEnabled(false);
        }

        coolers_and_freezer.setChecked(true);
    }

    private void setupOnclicks(){

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Qualification_StoreInfo_Fragment.DisplayStoreOptions();
                finish();
            }
        });

        infogrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                if (i == grills_and_cookers.getId()){
                    DisplayEquipmentBy("Cooker");

                }else if (i == coolers_and_freezer.getId()){
                    DisplayEquipmentBy("Cooler");

                }
                else if (i == other.getId()){
                    DisplayEquipmentBy("Other");
                }
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final ProgressDialog progressDialog = new ProgressDialog(EquipmentListActivity.this);
                progressDialog.setTitle("Please Wait");
                progressDialog.setMessage("Updating Data");
                progressDialog.setCancelable(false);
                progressDialog.show();

                if (equipments!=null){
                    if (equipments.size()>0){
                        saveDefaultSiteSettingEquipment(equipments,progressDialog);
                    }
                }

            }
        });

    }

    private void DisplayEquipmentBy(String area){
         equipments = EquipmenlistData.getInstance().getEquipmentByArea(area);
        if (equipments.size()>0){
            adapter = new EquipmentList_Listview_Adapter(this,equipments,site.getSite(),save);
            listView.setAdapter(adapter);
            message.setVisibility(View.GONE);
        }else {
            message.setVisibility(View.VISIBLE);
        }
    }



    private void saveDefaultSiteSettingEquipment(final ArrayList arrayList, final ProgressDialog progressDialog) {

        Log.i("Thrift ","saveDefaultSiteSettingEquipment start");
        try {
            final NamedServices ns = NamedServices.getInstance();
            if (ns.pimmServer!=null){
                final SiteSettingsSiteConfig config = ns.siteSettingsClient;

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            ArrayList<SiteSettingsEquipment>equipments = new ArrayList<>();

                            ArrayList<CustomEquipment>customEquipments = (ArrayList<CustomEquipment>)arrayList;
                            for (CustomEquipment customEquipment : customEquipments){
                                if (customEquipment.isUpdated()){
                                    equipments.add(customEquipment.getEquipment());
                                    customEquipment.setUpdated(false);
                                }
                            }

                            boolean success = config.updateSiteEquipment(equipments);
                            if (success &&  UpdateProbe(config,site.getSite())){

                                SCMTool.DismissDialogInThrift(progressDialog,save);
                                SCMTool.DisplaySnackbar("Update Successfully",root);
                                EquipmenlistData.getInstance().setUnsaveEdit(false);
                                EquipmenlistData.getInstance().setSiteSettingsEquipmentsUpdates(null);


                            }else {
                                EquipmenlistData.getInstance().setUnsaveEdit(true);
                                SCMTool.DismissDialogInThrift(progressDialog,null);
                                SCMTool.DisplaySnackbar("Update Failed : Please Check Internet Connection",root);

                            }

                        } catch (TException e) {
                            e.printStackTrace();
                            SCMTool.DisplaySnackbar("Update Failed : Please Check Internet Connection",root);
                            SCMTool.DismissDialogInThrift(progressDialog,null);
                        }
                    }
                }).start();

            }else {
                Log.i("Thrift ","SERVER NULL ");
                SCMTool.DismissDialogInThrift(progressDialog,null);
                SCMTool.DisplaySnackbar("Update Failed : Please Check Internet Connection",root);

            }

            Log.i("Thrift ","saveDefaultSiteSettingEquipment End");

        }catch (TException e){
            e.printStackTrace();
            SCMTool.DismissDialogInThrift(progressDialog,null);
            SCMTool.DisplaySnackbar("Update Failed : Please Check Internet Connection",root);
        }
    }

    private boolean UpdateProbe(SiteSettingsSiteConfig config,Site site){
        EquipmenlistData data = EquipmenlistData.getInstance();
        ArrayList arrayList = new ArrayList();
        arrayList.add(blueToothProbe);

        if (data.isProbeUpdated()){
            try {
                return config.updateConfiguration(site.siteid,arrayList);

            } catch (TException e) {
                e.printStackTrace();
                return false;
            }
        }else {
            return true;
        }
    }
}
