package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.Position_Coverage;

import com.procuro.androidscm.Daypart;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_SalePerformance.SalesPerformanceDaypartHeader;
import com.procuro.androidscm.Restaurant.Schema.Dayparts;
import com.procuro.apimmdatamanagerlib.SiteSalesData;

import java.util.ArrayList;

public class PositionCovered {
    private Dayparts daypart;
    private ArrayList<Integer>employeecount;
    private ArrayList<Integer>required_position_count;
    private ArrayList<ArrayList<SalesPerformanceDaypartHeader>> daypartHeaders;


    public ArrayList<ArrayList<SalesPerformanceDaypartHeader>> getDaypartHeaders() {
        return daypartHeaders;
    }

    public void setDaypartHeaders(ArrayList<ArrayList<SalesPerformanceDaypartHeader>> daypartHeaders) {
        this.daypartHeaders = daypartHeaders;
    }

    public PositionCovered(){

    }
    public PositionCovered(Dayparts daypart, ArrayList<Integer> employeecount, ArrayList<Integer> required_position_count) {
        this.daypart = daypart;
        this.employeecount = employeecount;
        this.required_position_count = required_position_count;
    }

    public Dayparts getDaypart() {
        return daypart;
    }

    public void setDaypart(Dayparts daypart) {
        this.daypart = daypart;
    }

    public ArrayList<Integer> getEmployeecount() {
        return employeecount;
    }

    public void setEmployeecount(ArrayList<Integer> employeecount) {
        this.employeecount = employeecount;
    }

    public ArrayList<Integer> getRequired_position_count() {
        return required_position_count;
    }

    public void setRequired_position_count(ArrayList<Integer> required_position_count) {
        this.required_position_count = required_position_count;
    }
}
