package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.Position_Coverage;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.Daypart;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.WeeklySchedule.Dashboard_Weekdays_RecyclerViewAdapter;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.WeeklySchedule.Dashboard_Weekly_Sched_Total_RecyclerViewAdapter;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.WeeklySchedule.WeeklyPickerDate;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_SalePerformance.SalesPerformanceDaypartHeader;
import com.procuro.androidscm.Restaurant.Schema.Dayparts;
import com.procuro.androidscm.Restaurant.Schema.Schema;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.HoursOfOperation;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.SMSPositioning;
import com.procuro.apimmdatamanagerlib.SMSPositioningEmployeeScheduleAssignment;
import com.procuro.apimmdatamanagerlib.SMSPositioningEmployeeScheduleAssignmentPositions;
import com.procuro.apimmdatamanagerlib.Schedule_Buisness;
import com.procuro.apimmdatamanagerlib.Schedule_Business_Site_Plan;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.SiteSales;
import com.procuro.apimmdatamanagerlib.SiteSalesActual;
import com.procuro.apimmdatamanagerlib.SiteSalesData;
import com.procuro.apimmdatamanagerlib.SiteSalesDaypart;
import com.procuro.apimmdatamanagerlib.SiteSalesDefault;
import com.procuro.apimmdatamanagerlib.SiteSalesForecast;
import com.procuro.apimmdatamanagerlib.StorePrepData;
import com.procuro.apimmdatamanagerlib.StorePrepSchedule;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

import static com.procuro.androidscm.Restaurant.DashBoard.Dashboard_SalePerformance.Dashboard_SalesPerformance.ConvertDefaultToSales;
import static com.procuro.androidscm.Restaurant.DashBoard.Dashboard_SalePerformance.Dashboard_SalesPerformance.getPrevSiteSalesInCurrentWeek;
import static com.procuro.androidscm.SCMTool.DaypartTimeToDate;
import static com.procuro.androidscm.SCMTool.getStringCurrentDaypart;


public class Dashboard_posiiton_coverage_fragment extends Fragment {

    private Dashboard_Weekdays_RecyclerViewAdapter weekdays_adapter;
    private RecyclerView listView;
    private RecyclerView weekdays,total_employees,total_hours_list;
    private TextView date,message;
    private ConstraintLayout datepicker;
    private ProgressBar progressBar;
    private Thread thread;

    public Dashboard_posiiton_coverage_fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dashboard_position_covered_fragment, container, false);

        listView = view.findViewById(R.id.listview);
        weekdays = view.findViewById(R.id.weekdays);
        date = view.findViewById(R.id.date);
        datepicker = view.findViewById(R.id.date_container);
        total_hours_list = view.findViewById(R.id.total_hours);
        total_employees = view.findViewById(R.id.total_employees);
        message = view.findViewById(R.id.message);
        progressBar = view.findViewById(R.id.progresbar);

        Site site = SCMDataManager.getInstance().getSelectedSite().getSite();
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar = SCMTool.getDateWithDaypart(calendar.getTime().getTime(),site.effectiveUTCOffset);


        GenerateWeekDays(calendar.getTime(),true);

        datepicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayPicker(getContext(),datepicker);
            }
        });

        return view;
    }



    @Override
    public void onStart() {
        super.onStart();
    }

    private ArrayList<Date> getWeekdays(Date date){

        Calendar now = Calendar.getInstance();
        now.setTimeZone(TimeZone.getTimeZone("UTC"));
        now.setTime(date);
        if (now.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
            now.add(Calendar.WEEK_OF_YEAR,-1);
        }

        ArrayList<Date>Weekdays = new ArrayList<>();
        int delta = -now.get(Calendar.DAY_OF_WEEK) + 2; //add 2 if your week start on monday
        now.add(Calendar.DAY_OF_MONTH, delta );
        for (int i = 0; i < 7; i++) {
            Weekdays.add(now.getTime());
            now.add(Calendar.DAY_OF_MONTH, 1);
        }

        Collections.sort(Weekdays);
        return Weekdays;
    }

    private void GenerateWeekDays(Date selectedDate,boolean isDefault){
        try {
            ShowLoading(true);

            ArrayList<Date>Weekdays = getWeekdays(selectedDate);

            SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
            format.setTimeZone(TimeZone.getTimeZone("UTC"));

            date.setText(format.format(Weekdays.get(0)));
            date.append(" to ");
            date.append(format.format(Weekdays.get(Weekdays.size()-1)));

            weekdays_adapter = new Dashboard_Weekdays_RecyclerViewAdapter(getContext(),Weekdays);
            weekdays.setLayoutManager(new GridLayoutManager(getContext(),7));
            weekdays.setAdapter(weekdays_adapter);


            if (isDefault){
                CheckDefaultSched(Weekdays);
            }else {
                DownloadSchedule(Weekdays,isDefault);
            }

        }catch (Exception e){
         e.printStackTrace();
        }
    }

    private void ShowLoading(boolean show){
        if (show){
            listView.setVisibility(View.INVISIBLE);
            progressBar.setVisibility(View.VISIBLE);
            message.setVisibility(View.VISIBLE);
        }else {
            listView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            message.setVisibility(View.GONE);
        }
    }

    private void CheckDefaultSched(ArrayList<Date>Weekdays){
        SCMDataManager scmDataManager = SCMDataManager.getInstance();
        if (scmDataManager.getWeeklySchedule() == null){
            DownloadSchedule(Weekdays,true);
        }else {
            setUpDailySalesPerformance(Weekdays.get(Weekdays.size()/2),SCMDataManager.getInstance().getWeeklySchedule(),true);
        }
    }

    private void DownloadSchedule(final ArrayList<Date>Weekdays, final boolean isDefault){
        aPimmDataManager dataManager = aPimmDataManager.getInstance();
        Site site = SCMDataManager.getInstance().getSelectedSite().getSite();

        dataManager.getSchedulesByDate(site.siteid, Weekdays.get(0), Weekdays.get(Weekdays.size()-1), new OnCompleteListeners.getEmployeeScheduleListener() {
            @Override
            public void getEmployeeScheduleCallback(ArrayList<Schedule_Business_Site_Plan> schedules, Error error) {
                if (isDefault){
                    SCMDataManager.getInstance().setWeeklySchedule(schedules);
                }
                setUpDailySalesPerformance(Weekdays.get(Weekdays.size()/2),schedules,isDefault);
            }
        });
    }

    private ArrayList<CustomUser>poPulatePositionInUsers(ArrayList<Schedule_Business_Site_Plan>schedules){

        ArrayList<CustomUser>customUsers = SCMTool.getActiveCustomUser(SCMDataManager.getInstance().getStoreUsers());
        try {
            Collections.sort(customUsers, new Comparator<CustomUser>() {
                @Override
                public int compare(CustomUser o1, CustomUser o2) {
                    return o1.getUser().firstName.compareToIgnoreCase(o2.getUser().firstName);
                }
            });
            for (CustomUser user : customUsers){
                ArrayList<Schedule_Business_Site_Plan>FilteredSchedules = new ArrayList<>();
                if (schedules!=null){
                    for (Schedule_Business_Site_Plan sched : schedules){
                        if (sched.userID.equalsIgnoreCase(user.getUser().userId)){
                            FilteredSchedules.add(sched);
                        }
                    }
                }
                user.setSchedules(FilteredSchedules);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return customUsers;
    }

    private void setUpPositionData(final ArrayList<Date>dates,
                                   final ArrayList<Schedule_Business_Site_Plan>schedules, final ArrayList<SiteSales>siteSales){
        try {

            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    final ArrayList<CustomUser>customUsers = poPulatePositionInUsers(schedules);
                    final ArrayList<PositionCovered>positionCovered = new ArrayList<>();
                    final ArrayList<Dayparts>dayparts = Schema.getInstance().getDayparts();

                    final SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM/dd/yy");
                    dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                    final SimpleDateFormat dayFormat = new SimpleDateFormat("EEE");
                    dayFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

                    final SimpleDateFormat timeFormat = new SimpleDateFormat("EEE hh:mm a");
                    timeFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

                    for (Dayparts daypart : dayparts){

                        Calendar start = null;
                        Calendar end = null;

                        ArrayList<Integer>employee_count = new ArrayList<>();
                        ArrayList<Integer>required_position = new ArrayList<>();
                        ArrayList<ArrayList<SalesPerformanceDaypartHeader>> headers = new ArrayList<>();

                        for (Date selectedDate : dates) {

                            start = DaypartTimeToDate(daypart.start, selectedDate);
                            end = DaypartTimeToDate(daypart.end, selectedDate);
                            end.add(Calendar.MINUTE,-1);

                            if (daypart.name.equalsIgnoreCase("6")) {
                                end.add(Calendar.DATE, 1);
                            }

                            int employee = 0;
                            for (CustomUser user : customUsers) {
                                for (Schedule_Business_Site_Plan sched : user.getSchedules()) {
                                    if (dateFormat.format(sched.date).equalsIgnoreCase(dateFormat.format(selectedDate))) {

                                        final SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
                                        timeFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));

                                        Calendar startTime = DaypartTimeToDate(timeFormatter.format(sched.startTime), selectedDate);
                                        Calendar endTime = DaypartTimeToDate(timeFormatter.format(sched.endTime), selectedDate);

                                        if (SCMTool.isBetween(startTime.getTime(),endTime.getTime(),start.getTime())){
//                                            System.out.println("SCHED: "+daypart.title+" | "+user.getUser().getFullName()+" | START "+timeFormat.format(startTime.getTime())+" - "+timeFormat.format(end.getTime()));
                                            employee++;
                                        }else if (SCMTool.isBetween(startTime.getTime(),endTime.getTime(),end.getTime())){
//                                            System.out.println("SCHED: "+daypart.title+" | "+user.getUser().getFullName()+" | END "+timeFormat.format(startTime.getTime())+" - "+timeFormat.format(end.getTime()));

                                            employee++;
                                        }
                                    }
                                }
                            }

                            ArrayList<SalesPerformanceDaypartHeader> daypartHeaders = getDaypartItems(daypart, selectedDate);
                            required_position.add(getRequiredPositions(daypartHeaders,siteSales,daypart,selectedDate));
                            headers.add(daypartHeaders);
                            employee_count.add(employee);

                        }
                        PositionCovered posCov = new PositionCovered();
                        posCov.setDaypart(daypart);
                        posCov.setEmployeecount(employee_count);
                        posCov.setRequired_position_count(required_position);
                        posCov.setDaypartHeaders(headers);
                        positionCovered.add(posCov);

                    }

                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                ShowLoading(false);
                                DisplayPositionCovered(positionCovered,listView,siteSales,customUsers,dates);
                                GenerateTotals(dates,customUsers);
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                }
            };
            thread = new Thread(runnable);
            thread.start();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            if (thread!=null && thread.isAlive()){
                thread.interrupt();
            }
            Looper.getMainLooper().getThread().interrupt();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void GenerateTotals(ArrayList<Date>dates, ArrayList<CustomUser>users){

        ArrayList<String>total_employee = new ArrayList<>();
        ArrayList<String>total_hours = new ArrayList<>();

        SMSPositioning smsPositionings = SCMDataManager.getInstance().getPositionForm();
        ArrayList<SMSPositioningEmployeeScheduleAssignment> assignments = smsPositionings.employeeScheduleAssignmentList;

        SimpleDateFormat timeformat = new SimpleDateFormat("hh:mm");
        SimpleDateFormat dateFormat= new SimpleDateFormat("EEE MMM/dd/yy");
        timeformat.setTimeZone(TimeZone.getTimeZone("UTC"));
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));


        for (Date selectedDate : dates){
            long totalStart = 0;
            long total_end = 0;
            int totalEmployee = 0;

            for (CustomUser user : users){
                boolean hasSchedule = false;
                if (user.getSchedules()!=null){
                    for (Schedule_Business_Site_Plan sched : user.getSchedules()){
                        if (dateFormat.format(selectedDate).equalsIgnoreCase(dateFormat.format(sched.date))){

                            final SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
                            timeFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));

                            Calendar startTime = DaypartTimeToDate(timeFormatter.format(sched.startTime), selectedDate);
                            Calendar endTime = DaypartTimeToDate(timeFormatter.format(sched.endTime), selectedDate);

                            totalStart += startTime.getTime().getTime();
                            total_end += endTime.getTime().getTime();

                            hasSchedule = true;
                        }
                    }
                }
                if (hasSchedule){
                    totalEmployee++;
                }
            }

            long diff = total_end - totalStart;
            int diffhours = (int) (diff / (60 * 60 * 1000));
            int diffmin = (int) (diff / (60 * 1000)% 60);

            total_hours.add(String.format("%02d", diffhours)+":"+String.format("%02d", diffmin));
            total_employee.add(String.valueOf(totalEmployee));
        }

        DispayTotal(total_employee,total_employees);
        DispayTotal(total_hours,total_hours_list);
    }

    private Integer getRequiredPositions(ArrayList<SalesPerformanceDaypartHeader> header,ArrayList<SiteSales> siteSales, Dayparts selectedDaypart,Date date){
        final SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM/dd/yy");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        if (siteSales!=null){
            for (SiteSales sales : siteSales){
                if (dateFormat.format(sales.salesDate).equalsIgnoreCase(dateFormat.format(date))){
                    return getProjectedHrPerDaypart(header,sales,selectedDaypart,date);
                }
            }
        }
        return 0;
    }

    private ArrayList<Double> getForcastPerDaypart(SiteSales siteSales,Dayparts selectedDaypart,ArrayList<SalesPerformanceDaypartHeader>header){
        ArrayList<Double>forecasts = new ArrayList<>();
        double totalForecast = getForcast(siteSales);

        if (siteSales.data!=null){

            SiteSalesData data = siteSales.data;

            if (data.dayparts!=null){

                for (SiteSalesDaypart daypart : data.dayparts){

                    if (daypart.name.equalsIgnoreCase(selectedDaypart.name)){
                        for (int i = 0; i <header.size()-1 ; i++) {
                            SalesPerformanceDaypartHeader daypartHeader  =  header.get(i);

                            BigDecimal percentage = new BigDecimal("0");

                            for (SiteSalesForecast forecast : daypart.forecast){

                                if (daypart.name.equalsIgnoreCase("1")|| daypart.name.equalsIgnoreCase("2")){

                                    if (daypartHeader.getName().equalsIgnoreCase("10-11")){
                                        percentage = getDaypartTransitionPercentage(data.dayparts,data.isFromDefault,daypartHeader);
                                    }else {
                                        if (data.isFromDefault){
                                            if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.startTime)){
                                                if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.endTime)){
                                                    percentage = percentage.add(forecast.percentage);
                                                }
                                            }
                                        }else {
                                            if (isInsideDaypart(daypartHeader,forecast)){
                                                percentage = percentage.add(forecast.percentage);
                                            }
                                        }
                                    }
                                }
                                else {
                                    if (data.isFromDefault){
                                        if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.startTime)){
                                            if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.endTime)){
                                                percentage = percentage.add(forecast.percentage);
                                            }
                                        }
                                    }else {
                                        if (isInsideDaypart(daypartHeader,forecast)){
                                            percentage = percentage.add(forecast.percentage);
                                        }
                                    }
                                }
                            }

                            BigDecimal FORCASTRESULT = new BigDecimal(totalForecast).multiply(percentage);
                            forecasts.add(FORCASTRESULT.doubleValue());
                        }
                        break;
                    }
                }
            }else {
                for (int i = 0; i <header.size()-1 ; i++) {
                    forecasts.add(0.0);
                }
            }
        }else {
            for (int i = 0; i <header.size()-1; i++) {
                forecasts.add(0.0);
            }
        }
        final SimpleDateFormat dateFormat = new SimpleDateFormat("EEE");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        return forecasts;
    }

    private int getCPH(double item) {

        if (item >= 0 && item <= 100) {
            return 4;
        } else if (item >= 100 && item <= 250) {
            return 5;
        } else if (item >= 250 && item <= 400) {
            return 6;
        } else if (item >= 400 && item <= 550) {
            return 7;
        } else if (item >= 550 && item <= 700) {
            return 8;
        } else if (item >= 700 && item <= 850) {
            return 9;
        } else if (item >= 850 && item <= 1000) {
            return 10;
        } else if (item >= 1000 && item <= 1150) {
            return 11;
        } else if (item >= 1150 && item <= 1300) {
            return 12;
        } else if (item >= 1300 && item <= 1450) {
            return 13;
        } else {
            return 14;
        }
    }

    private Integer getProjectedHrPerDaypart(ArrayList<SalesPerformanceDaypartHeader> header,SiteSales siteSales, Dayparts selectedDaypart,Date date){

        ArrayList<Double>projectedHrs = new ArrayList<>();
        try {
            ArrayList<Double>forecasts = getForcastPerDaypart(siteSales,selectedDaypart,header);
            HoursOfOperation hoursOfOperation = SCMDataManager.getInstance().getHoursOfOperation();
            for (int i = 0; i <header.size()-1 ; i++) {
                SalesPerformanceDaypartHeader daypartHeader = header.get(i);
                if (hoursOfOperation!=null){
                    StorePrepSchedule schedule = hoursOfOperation.storePrepSchedule;
                    if (schedule!=null){
                        double total =0;
                        if (selectedDaypart.name.equalsIgnoreCase("1")){
                            if (schedule.breakfast!=null && schedule.breakfast.size()>0 && schedule.lunch!=null && schedule.lunch.size()>0){
                                ArrayList<StorePrepData>totalStorePrepData = CreateTotal(schedule.breakfast,schedule.lunch);
                                for (StorePrepData totalData : totalStorePrepData){
                                    String time = totalData.time.replace(" AM","");
                                    String[]hr  = daypartHeader.getStart().split(":");
                                    if (time.contains(hr[0])){
                                        if (time.contains(":30")){
                                            total += totalData.totalEmployees;
                                        }else {
                                            total += totalData.totalEmployees;
                                        }
                                    }
                                }
                            }
                        }else if (selectedDaypart.name.equalsIgnoreCase("2")){
                            if (daypartHeader.getName().equalsIgnoreCase("10-11")){
                                if (schedule.breakfast!=null && schedule.breakfast.size()>0 && schedule.lunch!=null && schedule.lunch.size()>0){
                                    ArrayList<StorePrepData>totalStorePrepData = CreateTotal(schedule.breakfast,schedule.lunch);
                                    for (StorePrepData totalData : totalStorePrepData){
                                        String time = totalData.time.replace(" AM","");
                                        String[]hr  = daypartHeader.getStart().split(":");
                                        if (time.contains(hr[0])){
                                            if (time.contains(":30")){
                                                total += totalData.totalEmployees;
                                            }else {
                                                total += totalData.totalEmployees;
                                            }
                                        }
                                    }
                                }
                            }else {
                                total = getCPH(forecasts.get(i));
                            }
                        }else {
                            total = getCPH(forecasts.get(i));
                        }
                        double result = 0;
                        if (total!=0){
                            if (daypartHeader.getStart().equalsIgnoreCase("6:00") || daypartHeader.getStart().equalsIgnoreCase("10:00")){
                                result = total/2;
                            }else {
                                result = total;
                            }
                        }
                        projectedHrs.add(result);
                    }
                }
            }
            Collections.sort(projectedHrs);
            if (projectedHrs.size()>0){
                double selectedNumber = projectedHrs.get(projectedHrs.size()-1);
                return (int)Math.round(selectedNumber);
            }else {
                return 4;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return 4;
    }

    private ArrayList<StorePrepData>CreateTotal(ArrayList<StorePrepData>breakfast,ArrayList<StorePrepData>lunch){
        ArrayList<StorePrepData>totaldata = new ArrayList<>();

        for (StorePrepData bfast : breakfast){
            for (StorePrepData lu : lunch){
                if (bfast.time.equalsIgnoreCase(lu.time)){
                    StorePrepData newData = new StorePrepData();
                    newData.time = bfast.time;
                    newData.totalEmployees = bfast.totalEmployees+lu.totalEmployees;
                    newData.positions = bfast.positions;
                    totaldata.add(newData);
                }
            }
        }

        return totaldata;
    }

    private boolean isInsideDaypart(SalesPerformanceDaypartHeader daypartHeader,SiteSalesForecast forecast){
        SimpleDateFormat format = new SimpleDateFormat("H:mm");
        String[] header = daypartHeader.getStart().split(":");
        String headerHr = header[0];
        String headerMinute = header[1];
        String header30Minutes = "30";

        String[] forecastName = format.format(forecast.startTime).split(":");
        String forecastHr = forecastName[0];
        String foreMinute = forecastName[1];

        if (headerHr.equalsIgnoreCase(forecastHr)){
            return foreMinute.equalsIgnoreCase(header30Minutes) || foreMinute.equalsIgnoreCase(headerMinute);
        }
        return false;
    }

    private BigDecimal getDaypartTransitionPercentage(ArrayList<SiteSalesDaypart>dayparts,boolean isFromDefault,SalesPerformanceDaypartHeader daypartHeader ){

        BigDecimal percentage = new BigDecimal("0");
        SimpleDateFormat format = new SimpleDateFormat("H:mm");
        for (SiteSalesDaypart daypart : dayparts){
            if (daypart.name.equalsIgnoreCase("1")){
                for (SiteSalesForecast forecast : daypart.forecast){
                    if (!isFromDefault){
                        if (format.format(forecast.startTime).equalsIgnoreCase("10:00")){
                            percentage = percentage.add(forecast.percentage);
                            break;
                        }
                    }else {
                        if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.startTime)){
                            if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.endTime)){
                                percentage = percentage.add(forecast.percentage);
                                break;
                            }
                        }
                    }

                }
            }else if (daypart.name.equalsIgnoreCase("2")){
                for (SiteSalesForecast forecast : daypart.forecast){
                    if (!isFromDefault){
                        if (format.format(forecast.startTime).equalsIgnoreCase("10:30")){
                            percentage = percentage.add(forecast.percentage);
                            break;
                        }
                    }else {
                        if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.startTime)){
                            if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.endTime)){
                                percentage = percentage.add(forecast.percentage);
                                break;
                            }
                        }
                    }
                }
            }
        }
        System.out.println("getDaypartTransitionPercentage : "+percentage);
        return percentage;
    }

    private double getForcast(SiteSales siteSales){
        try {
            if (siteSales.data!=null){
                SiteSalesData data = siteSales.data;
                double forcast =0;
                if (data.dss!=0) {
                    forcast = Double.parseDouble(String.valueOf(data.dss));
                }
                else if (data.projected!=0){
                    forcast = Double.parseDouble(String.valueOf(data.projected));
                }
                return forcast;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    private ArrayList<SalesPerformanceDaypartHeader> getDaypartItems(Dayparts daypart,Date date){
        if (daypart.name.equalsIgnoreCase("1")){
            return CreateDaypart1(date);
        }
        else if (daypart.name.equalsIgnoreCase("2")){
            return CreateDaypart2(date);
        }
        else if (daypart.name.equalsIgnoreCase("3")){
            return CreateDaypart3(date);
        }
        else if (daypart.name.equalsIgnoreCase("4")){
            return CreateDaypart4(date);
        }
        else if (daypart.name.equalsIgnoreCase("5")){
            return CreateDaypart5(date);
        }else{
            return CreateDaypart6(date);
        }
    }

    private ArrayList<SalesPerformanceDaypartHeader> CreateDaypart1(Date date){
        ArrayList<SalesPerformanceDaypartHeader>times = new ArrayList<>();

        Calendar start = SCMTool.ISOTimeToDate("4:00",date);
        Calendar end = SCMTool.ISOTimeToDate("5:00",date);

        times.add(new SalesPerformanceDaypartHeader("4-5","4:00","5:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("5:00",date);
        end = SCMTool.ISOTimeToDate("6:00",date);

        times.add(new SalesPerformanceDaypartHeader("5-6","5:00","6:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("6:00",date);
        end = SCMTool.ISOTimeToDate("7:00",date);
        times.add(new SalesPerformanceDaypartHeader("6-7","6:00","7:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("7:00",date);
        end = SCMTool.ISOTimeToDate("8:00",date);
        times.add(new SalesPerformanceDaypartHeader("7-8","7:00","8:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("8:00",date);
        end = SCMTool.ISOTimeToDate("9:00",date);
        times.add(new SalesPerformanceDaypartHeader("8-9","8:00","9:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("9:00",date);
        end = SCMTool.ISOTimeToDate("10:00",date);
        times.add(new SalesPerformanceDaypartHeader("9-10","9:00","10:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("10:00",date);
        end = SCMTool.ISOTimeToDate("11:00",date);
        times.add(new SalesPerformanceDaypartHeader("10-11",true,"10:00","11:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("4:00",date);
        end = SCMTool.ISOTimeToDate("11:00",date);
        times.add(new SalesPerformanceDaypartHeader("Total","4:00","11:00",start.getTime(),end.getTime()));

        return times;
    }

    private ArrayList<SalesPerformanceDaypartHeader>  CreateDaypart2(Date date){
        ArrayList<SalesPerformanceDaypartHeader>times = new ArrayList<>();

        Calendar start = SCMTool.ISOTimeToDate("10:00",date);
        Calendar end = SCMTool.ISOTimeToDate("11:00",date);
        times.add(new SalesPerformanceDaypartHeader("10-11",true,"10:00","11:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("11:00",date);
        end = SCMTool.ISOTimeToDate("12:00",date);
        times.add(new SalesPerformanceDaypartHeader("11-12","11:00","12:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("12:00",date);
        end = SCMTool.ISOTimeToDate("13:00",date);
        times.add(new SalesPerformanceDaypartHeader("12-1","12:00","13:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("13:00",date);
        end = SCMTool.ISOTimeToDate("14:00",date);
        times.add(new SalesPerformanceDaypartHeader("1-2","13:00","14:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("10:00",date);
        end = SCMTool.ISOTimeToDate("14:00",date);
        times.add(new SalesPerformanceDaypartHeader("Total","10:00","14:00",start.getTime(),end.getTime()));

        return times;
    }

    private ArrayList<SalesPerformanceDaypartHeader>  CreateDaypart3(Date date){

        ArrayList<SalesPerformanceDaypartHeader>times = new ArrayList<>();

        Calendar start = SCMTool.ISOTimeToDate("14:00",date);
        Calendar end = SCMTool.ISOTimeToDate("15:00",date);
        times.add(new SalesPerformanceDaypartHeader("2-3","14:00","15:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("15:00",date);
        end = SCMTool.ISOTimeToDate("16:00",date);
        times.add(new SalesPerformanceDaypartHeader("3-4","15:00","16:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("16:00",date);
        end = SCMTool.ISOTimeToDate("17:00",date);
        times.add(new SalesPerformanceDaypartHeader("4-5","16:00","17:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("14:00",date);
        end = SCMTool.ISOTimeToDate("17:00",date);
        times.add(new SalesPerformanceDaypartHeader("Total","14:00","17:00",start.getTime(),end.getTime()));

        return times;

    }

    private ArrayList<SalesPerformanceDaypartHeader>  CreateDaypart4(Date date){
        ArrayList<SalesPerformanceDaypartHeader>times = new ArrayList<>();

        Calendar start = SCMTool.ISOTimeToDate("17:00",date);
        Calendar end = SCMTool.ISOTimeToDate("18:00",date);
        times.add(new SalesPerformanceDaypartHeader("5-6","17:00","18:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("18:00",date);
        end = SCMTool.ISOTimeToDate("19:00",date);
        times.add(new SalesPerformanceDaypartHeader("6-7","18:00","19:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("19:00",date);
        end = SCMTool.ISOTimeToDate("20:00",date);
        times.add(new SalesPerformanceDaypartHeader("7-8","19:00","20:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("17:00",date);
        end = SCMTool.ISOTimeToDate("20:00",date);
        times.add(new SalesPerformanceDaypartHeader("Total","17:00","20:00",start.getTime(),end.getTime()));

        return times;
    }

    private ArrayList<SalesPerformanceDaypartHeader>  CreateDaypart5(Date date){
        ArrayList<SalesPerformanceDaypartHeader>times = new ArrayList<>();

        Calendar start = SCMTool.ISOTimeToDate("20:00",date);
        Calendar end = SCMTool.ISOTimeToDate("21:00",date);
        times.add(new SalesPerformanceDaypartHeader("8-9","20:00","21:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("21:00",date);
        end = SCMTool.ISOTimeToDate("22:00",date);
        times.add(new SalesPerformanceDaypartHeader("9-10","21:00","22:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("20:00",date);
        end = SCMTool.ISOTimeToDate("22:00",date);
        times.add(new SalesPerformanceDaypartHeader("Total","20:00","22:00",start.getTime(),end.getTime()));


        return times;

    }

    private ArrayList<SalesPerformanceDaypartHeader>  CreateDaypart6(Date date){
        ArrayList<SalesPerformanceDaypartHeader>times = new ArrayList<>();

        Calendar start = SCMTool.ISOTimeToDate("22:00",date);
        Calendar end = SCMTool.ISOTimeToDate("23:00",date);
        times.add(new SalesPerformanceDaypartHeader("10-11","22:00","23:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("23:00",date);
        end = SCMTool.ISOTimeToDatePlus1("0:00",date);
        times.add(new SalesPerformanceDaypartHeader("11-12","23:00","0:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDatePlus1("0:00",date);
        end = SCMTool.ISOTimeToDatePlus1("1:00",date);
        times.add(new SalesPerformanceDaypartHeader("12-1","0:00","1:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDatePlus1("1:00",date);
        end = SCMTool.ISOTimeToDatePlus1("2:00",date);
        times.add(new SalesPerformanceDaypartHeader("1-2","1:00","2:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDatePlus1("2:00",date);
        end = SCMTool.ISOTimeToDatePlus1("3:00",date);
        times.add(new SalesPerformanceDaypartHeader("2-3","2:00","3:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDatePlus1("3:00",date);
        end = SCMTool.ISOTimeToDatePlus1("4:00",date);
        times.add(new SalesPerformanceDaypartHeader("3-4","3:00","4:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("22:00",date);
        end = SCMTool.ISOTimeToDatePlus1("4:00",date);
        times.add(new SalesPerformanceDaypartHeader("Total","22:00","4:00",start.getTime(),end.getTime()));

        return times;

    }

    public boolean isBetween(final Date min, final Date max, final Date date){
        boolean isbetween = false;

        //AFTER OR EQUAL TO SELECTED DATE
        if (date.compareTo(min)>=0){
            //BEFORE OR EQUAL
            if (date.compareTo(max)<=0){
                isbetween = true;
            }
        }

        return isbetween;
    }

    private void DispayTotal(ArrayList<String>total,RecyclerView recyclerView){
        Dashboard_Weekly_Sched_Total_RecyclerViewAdapter adapter = new Dashboard_Weekly_Sched_Total_RecyclerViewAdapter(getContext(),total);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),7));
        recyclerView.setAdapter(adapter);
    }

    private void DisplayPositionCovered(ArrayList<PositionCovered>positionCovereds,RecyclerView recyclerView,ArrayList<SiteSales>siteSales,
                                        ArrayList<CustomUser>users,ArrayList<Date>dates){

        Dashboard_Position_Coverage_RecyclerViewAdapter adapter = new Dashboard_Position_Coverage_RecyclerViewAdapter(getContext(),positionCovereds,siteSales,users,dates);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),1));
        recyclerView.setAdapter(adapter);
    }

    public void DisplayPicker(final Context context, View anchor) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.BOTTOM)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .arrowColor(ContextCompat.getColor(context,R.color.white))
                .transparentOverlay(true)
                .contentView(R.layout.weekly_picker)
                .focusable(true)
                .build();
        tooltip.show();

        final NumberPicker picker =tooltip.findViewById(R.id.picker);
        Button accept = tooltip.findViewById(R.id.apply);

        String[] values= null;
        final SCMDataManager data = SCMDataManager.getInstance();
        ArrayList<WeeklyPickerDate>weeklyPickerDates = data.getWeeklyPickerDates();
        int currentWeek = Calendar.getInstance().get(Calendar.WEEK_OF_YEAR);
        int currenyYear = Calendar.getInstance().get(Calendar.YEAR);

        if (weeklyPickerDates!=null){
             values = new String[weeklyPickerDates.size()];

            for (int i = 0; i < weeklyPickerDates.size(); i++) {
                WeeklyPickerDate weekdate = weeklyPickerDates.get(i);
                values[i] = weekdate.getName();

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(weekdate.getWeekdays().get(0));
                int year = calendar.get(Calendar.YEAR);
                if (year== currenyYear){
                    if (weekdate.getWeeknumber() == currentWeek){
                        currentWeek = i;
                    }
                }
            }

            picker.setDisplayedValues(values);
            picker.setMinValue(0);
            picker.setMaxValue(values.length-1);
            picker.setValue(currentWeek);

        }else {
            Toast.makeText(context, "NULL", Toast.LENGTH_SHORT).show();
        }

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<Date>Weekdays = data.getWeeklyPickerDates().get(picker.getValue()).getWeekdays();
                GenerateWeekDays(Weekdays.get(Weekdays.size()/2),false);
                tooltip.dismiss();
            }
        });
    }

    private void setUpDailySalesPerformance(Date selectedDate, ArrayList<Schedule_Business_Site_Plan>schedules,boolean isDefault){
        try {
            SCMDataManager scm = SCMDataManager.getInstance();
            final ArrayList<Date> Weekdays = getWeekdays(selectedDate);
            if (isDefault){
                if (scm.getSiteSales() == null && scm.getPrevSiteSales() == null){
                    DownloadSalesData(getPrevWeek(selectedDate),Weekdays,isDefault,schedules);
                }else {
                    setUpPositionData(Weekdays,schedules,scm.getSiteSales());
                }
            }else {
                DownloadSalesData(getPrevWeek(selectedDate),Weekdays,isDefault,schedules);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void DownloadSalesData(final ArrayList<Date> prevdates, final ArrayList<Date>dates, final boolean isDefault, final ArrayList<Schedule_Business_Site_Plan>Schedules){

        aPimmDataManager dataManager = aPimmDataManager.getInstance();
        Site site = SCMDataManager.getInstance().getSelectedSite().getSite();

        final SimpleDateFormat format = new SimpleDateFormat("EEE MM-dd-yyyy");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        final ArrayList<SiteSales>prevsiteSalesList = new ArrayList<>();

        final int[] Prev_counter = {0};

        for (final Date date : prevdates){
            System.out.println("PREV DATES : "+format.format(date));
            dataManager.getSiteSalesByDate(site.siteid, date, new OnCompleteListeners.getSiteSalesByDateListener() {
                @Override
                public void getSiteSalesByDateCallback(SiteSales siteSales, Error error) {
                    Prev_counter[0]++;
                    if (error == null){
                        prevsiteSalesList.add(siteSales);
                        if (Prev_counter[0] == prevdates.size()){
                            DownloadCurrentSalesData(dates,prevsiteSalesList,Schedules,isDefault);
                        }
                    }else {
                        SiteSales siteSale = new SiteSales();
                        siteSale.salesDate = date;
                        prevsiteSalesList.add(siteSale);
                        if (Prev_counter[0] == prevdates.size()){
                            DownloadCurrentSalesData(dates,prevsiteSalesList,Schedules,isDefault);
                        }
                    }
                }
            });
        }

    }

    private void DownloadCurrentSalesData(final ArrayList<Date>dates, final ArrayList<SiteSales>prevsiteSalesList, final ArrayList<Schedule_Business_Site_Plan>Schedules,
                                          final boolean isDefault){

        final SimpleDateFormat format = new SimpleDateFormat("EEE MM-dd-yyyy");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        aPimmDataManager dataManager = aPimmDataManager.getInstance();
        Site site = SCMDataManager.getInstance().getSelectedSite().getSite();
        final ArrayList<SiteSales>siteSalesList = new ArrayList<>();
        final int[] counter = {0};
        if (isDefault){
            for (final Date date : dates){
                dataManager.getSiteSalesByDate(site.siteid, date, new OnCompleteListeners.getSiteSalesByDateListener() {
                    @Override
                    public void getSiteSalesByDateCallback(SiteSales siteSales, Error error) {
                        counter[0]++;
                        if (error == null){
                            if (siteSales!=null && siteSales.data!=null){
                                System.out.println("SITE SALES ADDED: "+format.format(siteSales.salesDate));
                                if (siteSales.data.dss!=0){
                                    siteSalesList.add(siteSales);
                                    if (counter[0] == dates.size()){
                                        SCMDataManager.getInstance().setSiteSales(siteSalesList);
                                        SCMDataManager.getInstance().setPrevSiteSales(prevsiteSalesList);
                                        setUpPositionData(dates,Schedules,siteSalesList);
                                    }
                                }
                                else if (siteSales.data.projected!=0){
                                    siteSalesList.add(siteSales);
                                    if (counter[0] == dates.size()){
                                        SCMDataManager.getInstance().setSiteSales(siteSalesList);
                                        SCMDataManager.getInstance().setPrevSiteSales(prevsiteSalesList);
                                        setUpPositionData(dates,Schedules,siteSalesList);
                                    }
                                }
                                else if (siteSales.data.sales!=null && siteSales.data.sales.size()>0){
                                    siteSalesList.add(siteSales);
                                    if (counter[0] == dates.size()){
                                        SCMDataManager.getInstance().setSiteSales(siteSalesList);
                                        SCMDataManager.getInstance().setPrevSiteSales(prevsiteSalesList);
                                        setUpPositionData(dates,Schedules,siteSalesList);
                                    }
                                }else {
                                    SiteSales sales = getPrevSiteSalesInCurrentWeek(siteSalesList,prevsiteSalesList,date);
                                    if (sales.data!=null){
                                        if (sales.data.sales!=null && sales.data.sales.size()>0){
                                            double amount = 0;
                                            for (SiteSalesActual actual : sales.data.sales){
                                                amount +=actual.amount;
                                            }
                                             siteSales.data.projected= (int)amount;
                                        }
                                        else if (sales.data.dss !=0){
                                            siteSales.data.projected = sales.data.dss;
                                        }
                                        else if (sales.data.projected !=0){
                                            siteSales.data.projected = sales.data.projected;
                                        }
                                    }
                                    siteSalesList.add(siteSales);
                                    setUpPositionData(dates,Schedules,siteSalesList);
                                }
                            }
                            else {
                                SiteSales siteSale = new SiteSales();
                                siteSale.salesDate = date;
                                getSiteDefault(siteSalesList,prevsiteSalesList,date,siteSale,counter[0],dates.size(),isDefault,Schedules);
                            }
                        }
                        else {
                            SiteSales siteSale = new SiteSales();
                            siteSale.salesDate = date;
                            getSiteDefault(siteSalesList,prevsiteSalesList,date,siteSale,counter[0],dates.size(),isDefault,Schedules);
                        }
                    }
                });
            }
        }else {
            for (final Date date : dates){
                dataManager.getSiteSalesByDate(site.siteid, date, new OnCompleteListeners.getSiteSalesByDateListener() {
                    @Override
                    public void getSiteSalesByDateCallback(SiteSales siteSales, Error error) {
                        counter[0]++;
                        if (error == null){
                            if (siteSales!=null && siteSales.data!=null){
                                System.out.println("SITE SALES ADDED: "+format.format(siteSales.salesDate));

                                if (siteSales.data.dss!=0){
                                    siteSalesList.add(siteSales);
                                    if (counter[0] == dates.size()){
                                        setUpPositionData(dates,Schedules,siteSalesList);
                                    }
                                }
                                else if (siteSales.data.projected!=0){
                                    siteSalesList.add(siteSales);
                                    if (counter[0] == dates.size()){
                                        setUpPositionData(dates,Schedules,siteSalesList);
                                    }
                                }
                                else if (siteSales.data.sales!=null && siteSales.data.sales.size()>0){
                                    siteSalesList.add(siteSales);
                                    if (counter[0] == dates.size()){
                                        setUpPositionData(dates,Schedules,siteSalesList);
                                    }
                                }else {
                                    SiteSales sales = getPrevSiteSalesInCurrentWeek(siteSalesList,prevsiteSalesList,date);
                                    if (sales.data!=null){
                                        if (sales.data.sales!=null && sales.data.sales.size()>0){
                                            double amount = 0;
                                            for (SiteSalesActual actual : sales.data.sales){
                                                amount +=actual.amount;
                                            }
                                            siteSales.data.projected= (int)amount;
                                        }
                                        else if (sales.data.dss !=0){
                                            siteSales.data.projected = sales.data.dss;
                                        }
                                        else if (sales.data.projected !=0){
                                            siteSales.data.projected = sales.data.projected;
                                        }
                                    }
                                    siteSalesList.add(siteSales);
                                    setUpPositionData(dates,Schedules,siteSalesList);
                                }
                            }else {
                                SiteSales siteSale = new SiteSales();
                                siteSale.salesDate = date;
                                getSiteDefault(siteSalesList,prevsiteSalesList,date,siteSale,counter[0],dates.size(),isDefault,Schedules);
                            }
                        }
                        else {
                            SiteSales siteSale = new SiteSales();
                            siteSale.salesDate = date;
                            getSiteDefault(siteSalesList,prevsiteSalesList,date,siteSale,counter[0],dates.size(),isDefault,Schedules);
                        }
                    }
                });
            }
        }

    }

    private  void getSiteDefault(final ArrayList<SiteSales>siteSales,
                                 final ArrayList<SiteSales>prevSiteSales,
                                 final Date date, final SiteSales sales,
                                 final int DownloadedItems, final int TotalItems,
                                 final boolean isDefault,final ArrayList<Schedule_Business_Site_Plan>Schedules){

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.setTime(date);
        SimpleDateFormat format = new SimpleDateFormat("EEE");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        String StringDayOfWeek = format.format(date);
        int dayOfWeek = 0;

        if (StringDayOfWeek.equalsIgnoreCase("Tue")){
            dayOfWeek = 1;
        }else if (StringDayOfWeek.equalsIgnoreCase("Wed")){
            dayOfWeek = 2;
        }else if (StringDayOfWeek.equalsIgnoreCase("Thu")){
            dayOfWeek = 3;
        }else if (StringDayOfWeek.equalsIgnoreCase("Fri")){
            dayOfWeek = 4;
        }else if (StringDayOfWeek.equalsIgnoreCase("Sat")){
            dayOfWeek = 5;
        }else if (StringDayOfWeek.equalsIgnoreCase("Sun")){
            dayOfWeek = 6;
        }

        aPimmDataManager dataManager = aPimmDataManager.getInstance();
        dataManager.getDefaultSiteSalesForStoreType(dayOfWeek, 1, new OnCompleteListeners.getSiteSalesDefaultListener() {
            @Override
            public void getSiteSalesDefaultCallback(final SiteSalesDefault salesDefault, Error error) {
                if (error == null){
                    sales.data = ConvertDefaultToSales(salesDefault,date,siteSales,prevSiteSales);
                    siteSales.add(sales);
                    if (DownloadedItems == TotalItems){
                        if (isDefault){
                            SCMDataManager.getInstance().setSiteSales(siteSales);
                            SCMDataManager.getInstance().setPrevSiteSales(prevSiteSales);
                            setUpPositionData(getWeekdays(date),Schedules,siteSales);
                        }else {
                            setUpPositionData(getWeekdays(date),Schedules,siteSales);
                        }
                    }
                }else {
                    getSiteDefault(siteSales,prevSiteSales,date,sales,DownloadedItems,TotalItems,isDefault,Schedules);
                }
            }
        });
    }

    private ArrayList<Date> getPrevWeek(Date currentDate){
        ArrayList<Date> Weekdays = new ArrayList<>();
        try {
            Calendar now = Calendar.getInstance();
            now.setTimeZone(TimeZone.getTimeZone("UTC"));
            now.setTime(currentDate);
            now.add(Calendar.WEEK_OF_YEAR, -1);

            Weekdays = getWeekdays(now.getTime());
            Collections.reverse(Weekdays);

        }catch (Exception e){
            e.printStackTrace();
        }

        return Weekdays;
    }

}
