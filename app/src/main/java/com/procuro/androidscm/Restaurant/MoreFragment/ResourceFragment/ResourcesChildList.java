package com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment;

import java.util.ArrayList;

public class ResourcesChildList {

    String title;
    boolean selected;
    String appString;
    ArrayList<ResourcesGrandChildList>grandChildLists;


    public ResourcesChildList(String title, boolean selected, ArrayList<ResourcesGrandChildList> grandChildLists) {
        this.title = title;
        this.selected = selected;
        this.grandChildLists = grandChildLists;
    }

    public ResourcesChildList(String appString, ArrayList<ResourcesGrandChildList> grandChildLists) {
        this.appString = appString;
        this.grandChildLists = grandChildLists;
    }

    public String getAppString() {
        return appString;
    }

    public void setAppString(String appString) {
        this.appString = appString;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<ResourcesGrandChildList> getGrandChildLists() {
        return grandChildLists;
    }

    public void setGrandChildLists(ArrayList<ResourcesGrandChildList> grandChildLists) {
        this.grandChildLists = grandChildLists;
    }
}
