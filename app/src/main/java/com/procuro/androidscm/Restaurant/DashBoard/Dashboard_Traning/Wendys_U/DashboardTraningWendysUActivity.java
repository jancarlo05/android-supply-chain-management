package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Traning.Wendys_U;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Traning.Dashboard_TraningData;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.apimmdatamanagerlib.TrainingDataItems;

import java.util.ArrayList;

public class DashboardTraningWendysUActivity extends AppCompatActivity {
    TextView title;
    Button back;
    ExpandableListView expandableListView;
    EditText search;
    ArrayList<Dashboard_TraningData>array_sort = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_traning_wendys_u_activity);

        title = findViewById(R.id.username);
        back = findViewById(R.id.home);
        expandableListView =findViewById(R.id.expandible_listview);
        search = findViewById(R.id.search);


        title.setText("Wendy's University");
        setupOnclicks();
        setupTraning();
        Searchfunction();

    }


    private void setupOnclicks(){

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void setupTraning(){
        expandableListView.setDividerHeight(2);
        expandableListView.setGroupIndicator(null);
        Dashboard_Wendys_U_list_adapter adapter = new Dashboard_Wendys_U_list_adapter(this,SCMDataManager.getInstance().getDashboardTraningData(),this);
        expandableListView.setAdapter(adapter);

        for (int i = 0; i <adapter.getGroupCount() ; i++) {
            expandableListView.expandGroup(i);
        }

    }


    private void Searchfunction(){

        final ArrayList<Dashboard_TraningData>traningData = SCMDataManager.getInstance().getDashboardTraningData();

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int textlength = search.getText().length();
                ArrayList<String>strings = new ArrayList<>();

                if (textlength>0){
                    array_sort.clear();

                    for (Dashboard_TraningData dashboardTraningData : traningData){
                        boolean isparentCreated = false;
                        ArrayList<TrainingDataItems> trainingVideos = dashboardTraningData.getTrainingDataItems();
                        ArrayList<TrainingDataItems> sortedvideo = new ArrayList<>();

                        if (textlength <= dashboardTraningData.getCategory().length()){
                            if (dashboardTraningData.getCategory().toLowerCase().trim().contains(
                                    search.getText().toString().toLowerCase().trim())){
                                if (!strings.contains(dashboardTraningData.getCategory())){
                                    strings.add(dashboardTraningData.getCategory());
                                    array_sort.add(new Dashboard_TraningData(dashboardTraningData.getCategory(), sortedvideo));
                                }
                            }
                        }

                        for (TrainingDataItems trainingVideo: trainingVideos){
                            if (textlength <= trainingVideo.title.length()){
                                if (trainingVideo.title.toLowerCase().trim().contains(
                                        search.getText().toString().toLowerCase().trim())) {
                                    if (!strings.contains(dashboardTraningData.getCategory())) {
                                        strings.add(dashboardTraningData.getCategory());
                                        sortedvideo.add(trainingVideo);
                                        array_sort.add(new Dashboard_TraningData(dashboardTraningData.getCategory(), sortedvideo));

                                    }
                                    else {
                                        sortedvideo.add(trainingVideo);
                                    }
                                }

                            }
                        }
                    }
                    Dashboard_Wendys_U_list_adapter adapter = new Dashboard_Wendys_U_list_adapter(DashboardTraningWendysUActivity.this,array_sort,DashboardTraningWendysUActivity.this);
                    expandableListView.setAdapter(adapter);
                    for (int i = 0; i <adapter.getGroupCount() ; i++) {
                        expandableListView.expandGroup(i);
                    }
                }else {
                    Dashboard_Wendys_U_list_adapter adapter = new Dashboard_Wendys_U_list_adapter(DashboardTraningWendysUActivity.this,traningData,DashboardTraningWendysUActivity.this);
                    expandableListView.setAdapter(adapter);
                    for (int i = 0; i <adapter.getGroupCount() ; i++) {
                        expandableListView.expandGroup(i);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



    }


}
