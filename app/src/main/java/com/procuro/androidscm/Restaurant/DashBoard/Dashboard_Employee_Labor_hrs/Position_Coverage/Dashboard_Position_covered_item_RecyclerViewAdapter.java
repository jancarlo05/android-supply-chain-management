package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.Position_Coverage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.Schema.Dayparts;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.SMSPositioning;
import com.procuro.apimmdatamanagerlib.SMSPositioningEmployeeScheduleAssignment;
import com.procuro.apimmdatamanagerlib.SMSPositioningEmployeeScheduleAssignmentPositions;
import com.procuro.apimmdatamanagerlib.Site;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


public class Dashboard_Position_covered_item_RecyclerViewAdapter extends RecyclerView.Adapter<Dashboard_Position_covered_item_RecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    ArrayList<Integer>employee_count;
    ArrayList<Integer>required_position;


    public Dashboard_Position_covered_item_RecyclerViewAdapter(Context context,
                                                               ArrayList<Integer>employee_count,ArrayList<Integer>required_position) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.employee_count = employee_count;
        this.required_position = required_position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.dashboard_weekly_sched_total_list_data,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        int employees = employee_count.get(position);
        int required = required_position.get(position);

        if (employees<required){
            holder.name.setTextColor(ContextCompat.getColor(mContext,R.color.red));
        }

        StringBuilder builder = new StringBuilder();
        builder.append(employees);
        builder.append("/");
        builder.append(required);

        try {
            holder.name.setText(builder.toString());
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return required_position.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        public MyViewHolder(View itemView) {
            super(itemView);
             name = itemView.findViewById(R.id.name);

        }
    }

}
