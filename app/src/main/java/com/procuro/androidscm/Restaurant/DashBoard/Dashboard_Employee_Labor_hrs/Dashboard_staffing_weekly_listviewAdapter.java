package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.Employee;
import java.util.ArrayList;


public class Dashboard_staffing_weekly_listviewAdapter extends BaseAdapter {

    // Declare Variables

    Context mContext;
    LayoutInflater inflater;
    private ArrayList<Employee> arraylist;


    public Dashboard_staffing_weekly_listviewAdapter(Context context, ArrayList<Employee> arraylist) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;

    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return arraylist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.dashboard_header_staffing_weekly_list_data, null);
            view.setTag(holder);
            final Employee employee = arraylist.get(position);
            final TextView name = view.findViewById(R.id.name);

            name.setText(employee.getFirstname());
            name.append(" ");
            name.append(employee.getLastname());

        return view;
    }


}

