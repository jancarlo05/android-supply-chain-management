package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Traning.VTAs;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

import com.procuro.androidscm.GlideApp;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_PdfView_DialogFragment;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourcesChildList;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourcesGrandChildList;
import com.procuro.apimmdatamanagerlib.DocumentDTO;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.util.ArrayList;

import es.voghdev.pdfviewpager.library.RemotePDFViewPager;
import es.voghdev.pdfviewpager.library.adapter.PDFPagerAdapter;
import es.voghdev.pdfviewpager.library.remote.DownloadFile;
import es.voghdev.pdfviewpager.library.util.FileUtil;

import static com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Traning.VTAs.Dashboard_Vtas_WITH_PDFVIER_Activity.updateLayout;
import static com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Traning.VTAs.Dashboard_Vtas_WITH_PDFVIER_Activity.wait;


public class Dashboard_Vta_listview_adapter extends BaseExpandableListAdapter implements  DownloadFile.Listener {

    private final Context context ;
    private ArrayList<ResourcesChildList> safetydata;
    private FragmentActivity fragmentActivity;


    public Dashboard_Vta_listview_adapter(Context context, ArrayList<ResourcesChildList> safetydata , FragmentActivity fragmentActivity) {
        this.context = context;
        this.safetydata = safetydata;
        this.fragmentActivity = fragmentActivity;

    }

    @Override
    public int getGroupCount() {
        return safetydata.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return safetydata.get(i).getGrandChildLists().size();
    }

    @Override
    public Object getGroup(int i) {
        return safetydata.get(i);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return safetydata.get(groupPosition).getGrandChildLists().get(childPosition);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int position, boolean b, View contentView, ViewGroup parent) {

        ResourcesChildList data = safetydata.get(position);
        contentView = LayoutInflater.from(context).inflate(R.layout.dashboard_training_category_level, parent,false);

        TextView name = contentView.findViewById(R.id.name);
        TextView childcount = contentView.findViewById(R.id.childcount);

        name.setText(data.getAppString());

        if (data.getGrandChildLists()!=null){
            childcount.setText(String.valueOf(data.getGrandChildLists().size()));
        }else {
            childcount.setText("0");
        }

        return contentView;
    }


    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean b, View contentView, final ViewGroup parent) {
        final ResourcesGrandChildList item = safetydata.get(groupPosition).getGrandChildLists().get(childPosition);
        contentView = LayoutInflater.from(context).inflate(R.layout.dashboard_safety_datasheets_child_data, parent, false);
        TextView name = contentView.findViewById(R.id.title);
        ImageView icon = contentView.findViewById(R.id.icon);

        name.setText(item.getDocumentDTO().name);

        if (item.getDocumentDTO().docType.toLowerCase().contains("pdf")){
            GlideApp.with(context).asDrawable().load(R.drawable.pdf_icon).into(icon);
        }else {
            GlideApp.with(context).asDrawable().load(R.drawable.video_player).into(icon);
        }

        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setDownloadButtonListener(item.getDocumentDTO(),context);
            }
        });

        return contentView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }


    @Override
    public void onSuccess(String url, String destinationPath) {

        try {
            Dashboard_Vtas_WITH_PDFVIER_Activity.adapter = new PDFPagerAdapter(fragmentActivity, FileUtil.extractFileNameFromURL(url));
            Dashboard_Vtas_WITH_PDFVIER_Activity.remotePDFViewPager.setAdapter(Dashboard_Vtas_WITH_PDFVIER_Activity.adapter);
            Dashboard_Vtas_WITH_PDFVIER_Activity.updateLayout();

        }catch (Exception e){
            Dashboard_Vtas_WITH_PDFVIER_Activity.progressBar.setVisibility(View.INVISIBLE);
            Dashboard_Vtas_WITH_PDFVIER_Activity.wait.setText("NO PREVIEW AVAILABLE");
        }
    }

    @Override
    public void onFailure(Exception e) {
        e.printStackTrace();
        Dashboard_Vtas_WITH_PDFVIER_Activity.progressBar.setVisibility(View.INVISIBLE);
        Dashboard_Vtas_WITH_PDFVIER_Activity.wait.setText("NO PREVIEW AVAILABLE");

    }

    @Override
    public void onProgressUpdate(int progress, int total) {
        System.out.println("PROGRESS: "+progress+"/"+total);

    }
    public void setDownloadButtonListener(DocumentDTO documentDTO, final Context context) {
        Dashboard_Vtas_WITH_PDFVIER_Activity.progressBar.setVisibility(View.VISIBLE);
        Dashboard_Vtas_WITH_PDFVIER_Activity.wait.setVisibility(View.VISIBLE);
        Dashboard_Vtas_WITH_PDFVIER_Activity.wait.setText("Please Wait");
        Dashboard_Vtas_WITH_PDFVIER_Activity.remotePDFViewPager.setVisibility(View.GONE);
        DownloadFile.Listener listener = this;

        aPimmDataManager dataManager = aPimmDataManager.getInstance();
        String[] strings = dataManager.getAuthHeaderValue().split("Basic ");

        StringBuilder urlString = new StringBuilder();
        urlString.append(dataManager.getBaseUrl());
        urlString.append("Document/Document/GetContent/");
        urlString.append("?documentId=");
        urlString.append(documentDTO.documentId);
        urlString.append("&authorization=").append(strings[1]);

        System.out.println(urlString.toString());

        try {
            Dashboard_Vtas_WITH_PDFVIER_Activity.remotePDFViewPager = new RemotePDFViewPager(context, urlString.toString(), listener);
            Dashboard_Vtas_WITH_PDFVIER_Activity.remotePDFViewPager.setId(R.id.pdfViewPager);
        }catch (Exception e){
            Dashboard_Vtas_WITH_PDFVIER_Activity.progressBar.setVisibility(View.INVISIBLE);
            Dashboard_Vtas_WITH_PDFVIER_Activity.wait.setText("NO PREVIEW AVAILABLE");
        }

    }


}