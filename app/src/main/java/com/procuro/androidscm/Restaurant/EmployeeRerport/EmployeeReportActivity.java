package com.procuro.androidscm.Restaurant.EmployeeRerport;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.WeeklySchedule.Dashboard_Weekdays_RecyclerViewAdapter;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.WeeklySchedule.Dashboard_Weekly_Sched_RecyclerViewAdapter;
import com.procuro.androidscm.Restaurant.ReportsFragment.Inspection.Reports_inspection_Load_Webview;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsData;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsDateChildList;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsDateGrandChildList;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.SMSPositioning;
import com.procuro.apimmdatamanagerlib.SMSPositioningEmployeeScheduleAssignment;
import com.procuro.apimmdatamanagerlib.SMSPositioningEmployeeScheduleAssignmentPositions;
import com.procuro.apimmdatamanagerlib.Schedule_Business_Site_Plan;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;

public class EmployeeReportActivity extends AppCompatActivity {

    public static int Employee_Schedule_Report = 0;
    public static int Employee_Schedule_Summary = 1;

    public static ProgressBar progressBar;
    public WebView webView;
    public static TextView date , wait,name;
    private Button back;


    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_report);

        webView = findViewById(R.id.webview);
        progressBar = findViewById(R.id.progressBar2);
        wait = findViewById(R.id.wait);
        back = findViewById(R.id.back_button);
        name = findViewById(R.id.username);

        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        settings.setSupportZoom(true);
        settings.supportZoom();
        settings.setBuiltInZoomControls(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.addJavascriptInterface(new JavaScriptInterface(this,webView),"AndroidApp");
        webView.setWebViewClient(new mywebviewclient());
        webView.setWebChromeClient(new Webcromeclient());

        CheckReportDetails();

        setupOnclicks();
    }
    private void CheckReportDetails(){
        EmployeeReportData data = EmployeeReportData.getInstance();

        if (data.getReportType() == Employee_Schedule_Report){
            Download_Employee_Schedule_Report_Data(data);
        }else {
            Download_Employee_Schedule_Summary_Data(data);
        }
    }

    public void Download_Employee_Schedule_Report_Data(final EmployeeReportData data){

        ArrayList<Date>totalDays = new ArrayList<>();
        ArrayList<Date>dates = new ArrayList<>();
        try {
            Calendar selectedDate = Calendar.getInstance();
            selectedDate.setTime(data.getEmployee_Schedule_Report_Date());

            if (selectedDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
                selectedDate.add(Calendar.WEEK_OF_YEAR,-1);
            }

            for (int i = 0; i <3 ; i++) {
                dates.add(selectedDate.getTime());
                selectedDate.add(Calendar.WEEK_OF_YEAR,1);
            }

            Collections.sort(dates);

        }catch (Exception e){
            e.printStackTrace();
        }

        for (Date date :dates){
            Calendar now = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            now.setTime(date);
            ArrayList<Date>Weekdays = getWeekDates(now);
            totalDays.addAll(Weekdays);
        }
        Collections.sort(totalDays);
        try {
            aPimmDataManager dataManager = aPimmDataManager.getInstance();
            dataManager.getEmployeeSchedule(data.getSite().siteid, totalDays.get(0), totalDays.get(totalDays.size() - 1), data.getUser().getUser().userId, new OnCompleteListeners.getEmployeeScheduleListener() {
                @Override
                public void getEmployeeScheduleCallback(ArrayList<Schedule_Business_Site_Plan> schedules, Error error) {
                    if (error == null){
                        data.getUser().setSchedules(schedules);
                    }
                    webView.loadUrl("file:///android_asset/EmployeeScheduleReport/index.html");
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    private void Download_Employee_Schedule_Summary_Data(final EmployeeReportData data){

        try {
            Calendar selectedDate = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            selectedDate.setTime(data.getEmployee_Schedule_Report_Date());
            if (selectedDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
                selectedDate.add(Calendar.WEEK_OF_YEAR,-1);
            }
            ArrayList<Date>Weekdays = getWeekDates(selectedDate);
            Collections.sort(Weekdays);

            try {
                aPimmDataManager dataManager = aPimmDataManager.getInstance();
                dataManager.getSchedulesByDate(data.getSite().siteid, Weekdays.get(0), Weekdays.get(Weekdays.size() - 1), new OnCompleteListeners.getEmployeeScheduleListener() {
                    @Override
                    public void getEmployeeScheduleCallback(ArrayList<Schedule_Business_Site_Plan> schedules, Error error) {
                        if (error == null){
                            if (data.getUsers()!=null){
                                for (CustomUser user : data.getUsers()){
                                    ArrayList<Schedule_Business_Site_Plan> filteredSchedules = new ArrayList<>();
                                    if (schedules!=null){
                                        for (Schedule_Business_Site_Plan sched : schedules){
                                            if (user.getUser().userId.equalsIgnoreCase(sched.userID)){
                                                filteredSchedules.add(sched);
                                            }
                                        }
                                    }
                                    user.setSchedules(filteredSchedules);
                                }
                            }
                        }
                        webView.loadUrl("file:///android_asset/EmployeeScheduleReport/index.html");
                    }
                });
            }catch (Exception e){
                e.printStackTrace();
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void setupOnclicks(){
        try {
            final EmployeeReportData reportData = EmployeeReportData.getInstance();
            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    wait.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.VISIBLE);
                    if (reportData.getReportType() == Employee_Schedule_Report){
                        reportData.setReportType(Employee_Schedule_Summary);
                        Download_Employee_Schedule_Summary_Data(reportData);
                    }else {
                        finish();
                    }
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private class Webcromeclient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            webView.setVisibility(View.INVISIBLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                progressBar.setProgress(newProgress, true);
            }else {
                progressBar.setProgressTintList(ColorStateList.valueOf(Color.parseColor("#F68725")));
                progressBar.setProgress(newProgress);
            }
            if(newProgress == 100) {
                webView.setVisibility(View.VISIBLE);
                wait.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);

            }
        }
    }



    private class mywebviewclient extends WebViewClient {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            setUpEmployeeReport();
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            System.out.println("ERROR"+error.getDescription());
            System.out.println("ERROR"+error.toString());
        }
    }

    private void setUpEmployeeReport(){
        EmployeeReportData reportData  = EmployeeReportData.getInstance();

        if (reportData.getReportType() == Employee_Schedule_Report){
            Generate_Employee_Schedule_Report(reportData);
        }else {
            Generate_Employee_Schedule_Summary_Report(reportData);
        }

        name.setText(reportData.getTitle());
    }

    private void Generate_Employee_Schedule_Summary_Report(EmployeeReportData reportData){
        String params = Create_Employee_Schedule_Summary_Params(reportData);
        StringBuilder sbldr = new StringBuilder();
        sbldr.append("loadReport(");
        sbldr.append(params);
        sbldr.append(")");
        System.out.println(sbldr);
        webView.evaluateJavascript(sbldr.toString(), null);
    }

    private String Create_Employee_Schedule_Summary_Params(EmployeeReportData reportData) {
        String param = "";
        try {
            JSONObject dataJObj = new JSONObject();
            JSONObject header = getHeaderDays(reportData);

            dataJObj.put("headerDays", header.getJSONArray("headerDays"));
            dataJObj.put("storeName", reportData.getSite().sitename);
            dataJObj.put("spid", reportData.getSPID());
            dataJObj.put("title", reportData.getTitle());
            dataJObj.put("template", reportData.getTemplate());
            dataJObj.put("date", header.getString("date"));
            dataJObj.put("employees", Create_Employee_Schedule_Summary_Report_Employee_Items(reportData));

            param = dataJObj.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return param;
    }

    private JSONArray Create_Employee_Schedule_Summary_Report_Employee_Items(EmployeeReportData reportData){

        JSONArray jsonArray = new JSONArray();
        ArrayList<CustomUser>users = SCMTool.getActiveCustomUser(reportData.getUsers());

        Collections.sort(users, new Comparator<CustomUser>() {
          @Override
          public int compare(CustomUser o1, CustomUser o2) {
              return o1.getUser().firstName.compareTo(o2.getUser().firstName);
          }
        });


        Calendar selectedDate = SCMTool.getDateWithDaypart(Calendar.getInstance().getTime().getTime(),reportData.getSite().effectiveUTCOffset);
        if (selectedDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
            selectedDate.add(Calendar.WEEK_OF_YEAR,-1);
        }

        try {

            ArrayList<Date>Weekdays = getWeekDates(selectedDate);

            for (CustomUser user : users){

                try {
                    JSONObject EmployeeObject = new JSONObject();

                    JSONArray schedules = new JSONArray();
                    for (Date date : Weekdays){
                        schedules.put(getEmployeeSched(date,reportData,user));
                    }
                    EmployeeObject.put("name",user.getUser().getFullName());
                    EmployeeObject.put("userId",user.getUser().userId);
                    EmployeeObject.put("schedules",schedules);

                    jsonArray.put(EmployeeObject);

                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return jsonArray;
    }

    private void Generate_Employee_Schedule_Report(EmployeeReportData reportData){
        String params = Create_Employee_Schedule_Report_Params(reportData);
        StringBuilder sbldr = new StringBuilder();
        sbldr.append("loadReport(");
        sbldr.append(params);
        sbldr.append(")");
        System.out.println(sbldr);
        webView.evaluateJavascript(sbldr.toString(), null);
    }

    private String Create_Employee_Schedule_Report_Params(EmployeeReportData reportData) {
        String param = "";
        try {
            JSONObject dataJObj = new JSONObject();
            dataJObj.put("storeName", reportData.getName());
            dataJObj.put("spid", reportData.getSPID());
            dataJObj.put("title", reportData.getTitle());
            dataJObj.put("template", reportData.getTemplate());
            dataJObj.put("date", getWeekDatesSummary(reportData));
            dataJObj.put("schedules", Create_Employee_Schedule_Report_Schedules(reportData));
            param = dataJObj.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return param;
    }

    private JSONObject Create_Employee_Schedule_Report_Schedules_Item(EmployeeReportData reportData,Date selectedDate){
        JSONObject jsonObject = new JSONObject();
        try {
            TimeZone timeZone = TimeZone.getTimeZone("UTC");
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");
            SimpleDateFormat weekFormat = new SimpleDateFormat("w");
            SimpleDateFormat dayformat = new SimpleDateFormat("EEEE");
            SimpleDateFormat dateTitleFormat = new SimpleDateFormat(" M/dd/yy");

            dayformat.setTimeZone(timeZone);
            dateFormat.setTimeZone(timeZone);
            weekFormat.setTimeZone(timeZone);
            dateTitleFormat.setTimeZone(timeZone);

            JSONArray scheduleHeader = new JSONArray();
            JSONArray schedules = new JSONArray();

            Calendar now = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            now.setTime(selectedDate);
            ArrayList<Date>Weekdays = getWeekDates(now);

            for (Date date : Weekdays){

                JSONObject headerItem = new JSONObject();
                headerItem.put("name",dayformat.format(date));
                headerItem.put("date",dateFormat.format(date));
                scheduleHeader.put(headerItem);
                schedules.put(getEmployeeSched(date,reportData,reportData.getUser()));
            }

            Date start = Weekdays.get(0);
            Date middle  = Weekdays.get(Weekdays.size()/2);
            Date end  = Weekdays.get(Weekdays.size()-1);

            StringBuilder title = new StringBuilder();
            title.append("Week ");
            title.append(weekFormat.format(middle));
            title.append(" (");
            title.append(dateTitleFormat.format(start));
            title.append(" - ");
            title.append(dateTitleFormat.format(end));
            title.append(")");

            jsonObject.put("title",title.toString());
            jsonObject.put("headerDays",scheduleHeader);
            jsonObject.put("schedules",schedules);


        }catch (Exception e){
            e.printStackTrace();
        }

        return jsonObject;
    }

    private JSONObject getEmployeeSched(Date selectedDate,EmployeeReportData reportData,CustomUser user){
        JSONObject jsonObject = new JSONObject();
        SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a");
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM/dd/yy");

        TimeZone timeZone = TimeZone.getTimeZone("UTC");
        dateFormat.setTimeZone(timeZone);
        timeFormat.setTimeZone(timeZone);

        ArrayList<Date>startDates = new ArrayList<>();
        ArrayList<Date>endDates = new ArrayList<>();

        try {
            if (user.getSchedules()!=null){
                for (Schedule_Business_Site_Plan sched : user.getSchedules()){
                    if (sched.userID.equalsIgnoreCase(user.getUser().userId)){
                        if (dateFormat.format(selectedDate).equalsIgnoreCase(dateFormat.format(sched.date))){
                            startDates.add(sched.startTime);
                            endDates.add(sched.endTime);
                        }
                    }
                }
            }
            Collections.sort(startDates);
            Collections.sort(endDates);

            if (startDates.size()>0){
                jsonObject.put("timeIn",timeFormat.format(startDates.get(0)));
            }else {
                jsonObject.put("timeIn","--");
            }

            if (endDates.size()>0){
                jsonObject.put("timeOut",timeFormat.format(endDates.get(endDates.size()-1)));
            }else {
                jsonObject.put("timeOut","--");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return jsonObject;
    }

    private ArrayList<Date> getWeekDates(Calendar now){
        ArrayList<Date>Weekdays = new ArrayList<>();
        int delta = -now.get(Calendar.DAY_OF_WEEK) + 2; //add 2 if your week start on monday
        now.add(Calendar.DAY_OF_MONTH, delta );

        for (int i = 0; i < 7; i++) {
            Weekdays.add(now.getTime());
            now.add(Calendar.DAY_OF_MONTH, 1);
        }

        return Weekdays;
    }

    private String getWeekDatesSummary(EmployeeReportData reportData){
        SimpleDateFormat dateFormat = new SimpleDateFormat("M/dd/yy");
        SimpleDateFormat weekformat = new SimpleDateFormat("w");
        weekformat.setTimeZone(TimeZone.getTimeZone("UTC"));
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Calendar selectedDate = SCMTool.getDateWithDaypart(Calendar.getInstance().getTime().getTime(),reportData.getSite().effectiveUTCOffset);

        ArrayList<Date>dates = new ArrayList<>();
        if (selectedDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
            selectedDate.add(Calendar.WEEK_OF_YEAR,-1);
        }

        for (int i = 0; i <3 ; i++) {
            dates.add(selectedDate.getTime());
            selectedDate.add(Calendar.WEEK_OF_YEAR,1);

        }
        Collections.sort(dates);

        StringBuilder dateTitle = new StringBuilder();

        dateTitle.append("Week ");
        dateTitle.append(weekformat.format(dates.get(0)));
        dateTitle.append(" - ");
        dateTitle.append(weekformat.format(dates.get(dates.size()-1)));

        return dateTitle.toString();

    }

    private JSONArray Create_Employee_Schedule_Report_Schedules(EmployeeReportData reportData){
        JSONArray jsonArray  = new JSONArray();

        SimpleDateFormat format = new SimpleDateFormat("w");

        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        ArrayList<Date>dates = new ArrayList<>();

        try {
            Calendar selectedDate = SCMTool.getDateWithDaypart(Calendar.getInstance().getTime().getTime(),reportData.getSite().effectiveUTCOffset);

            if (selectedDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
                selectedDate.add(Calendar.WEEK_OF_YEAR,-1);
            }

            for (int i = 0; i <3 ; i++) {
                dates.add(selectedDate.getTime());
                selectedDate.add(Calendar.WEEK_OF_YEAR,1);

            }
            Collections.sort(dates);

        }catch (Exception e){
            e.printStackTrace();
        }


        for (Date date :dates){
            jsonArray.put(Create_Employee_Schedule_Report_Schedules_Item(reportData,date));
        }

        return jsonArray;
    }

    private JSONObject getHeaderDays(EmployeeReportData reportData){

        JSONObject jsonObject = new JSONObject();

        try {
            TimeZone timeZone = TimeZone.getTimeZone("UTC");
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");
            SimpleDateFormat dayformat = new SimpleDateFormat("EEEE");

            dayformat.setTimeZone(timeZone);
            dateFormat.setTimeZone(timeZone);

            JSONArray headerDays = new JSONArray();

            Calendar selectedDate = SCMTool.getDateWithDaypart(Calendar.getInstance().getTime().getTime(),reportData.getSite().effectiveUTCOffset);
            if (selectedDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
                selectedDate.add(Calendar.WEEK_OF_YEAR,-1);
            }

            ArrayList<Date>Weekdays = getWeekDates(selectedDate);
            for (Date date : Weekdays){
                JSONObject headerItem = new JSONObject();
                headerItem.put("name",dayformat.format(date));
                headerItem.put("date",dateFormat.format(date));
                headerDays.put(headerItem);
            }
            StringBuilder dateTitle = new StringBuilder();

            dateTitle.append(dateFormat.format(Weekdays.get(0)));
            dateTitle.append(" - ");
            dateTitle.append(dateFormat.format(Weekdays.get(Weekdays.size()-1)));


            jsonObject.put("headerDays",headerDays);
            jsonObject.put("date",dateTitle);


        }catch (Exception e){
            e.printStackTrace();
        }

        return jsonObject;
    }

     class  JavaScriptInterface {

        Context context;
        WebView webView;

        JavaScriptInterface(Context c, WebView w) {
            context = c;
            webView = w;
        }

        @JavascriptInterface
        public void onClickEmployee(String userId) {
            final EmployeeReportData reportData = EmployeeReportData.getInstance();
            reportData.setReportType(Employee_Schedule_Report);

            reportData.setUser(getEmployeeByUserId(reportData,userId));


            Handler handler = new Handler(Looper.getMainLooper());

            handler.post(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.VISIBLE);
                    wait.setVisibility(View.VISIBLE);
                    Download_Employee_Schedule_Report_Data(reportData);;
                }
            });

        }

        private CustomUser getEmployeeByUserId(EmployeeReportData reportsData,String userID){
            ArrayList<CustomUser>users = reportsData.getUsers();
            try {
                for (CustomUser user :users){
                    if (user.getUser().userId.equalsIgnoreCase(userID)){
                        return user;
                    }
                }

            }catch (Exception e){
                e.printStackTrace();
            }
            return users.get(0);
        }

    }


}
