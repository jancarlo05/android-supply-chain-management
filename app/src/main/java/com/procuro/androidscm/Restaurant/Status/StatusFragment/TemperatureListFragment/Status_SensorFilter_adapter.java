package com.procuro.androidscm.Restaurant.Status.StatusFragment.TemperatureListFragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.procuro.androidscm.GlideApp;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.CustomSensor;
import com.procuro.apimmdatamanagerlib.PimmInstance;

import java.util.ArrayList;

import static android.view.View.OnClickListener;


public class Status_SensorFilter_adapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<CustomSensor>sensors;
    private Dialog dialog;
    private ListView listView;
    private Status_SensorList_Adapter adapter;
    private ArrayList<PimmInstance>instances;
    private FragmentActivity fragmentActivity;
    private TextView dropdown_title;
    private CustomSite site;


    public Status_SensorFilter_adapter(Context context, ArrayList<CustomSensor> sensors,
                                       Dialog dialog, ListView listView,
                                       ArrayList<PimmInstance>instances,
                                       FragmentActivity fragmentActivity, TextView dropdown_title,
                                       CustomSite site) {
        this.mContext = context;
        this.inflater = LayoutInflater.from(context);
        this.sensors = sensors;
        this.dialog = dialog;
        this.listView = listView;
        this.instances = instances;
        this.fragmentActivity = fragmentActivity;
        this.dropdown_title = dropdown_title;
        this.site = site;


    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return sensors.size();
    }

    @Override
    public Object getItem(int position) {
        return  sensors.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
            view = inflater.inflate(R.layout.status_sensor_list_data, null);
            final CustomSensor sensor = sensors.get(position);
            ImageView indicator = view.findViewById(R.id.indicator);
            TextView title = view.findViewById(R.id.title);
            view.setBackgroundResource(R.drawable.onpress_design);

            title.setText(sensor.getTitle());
            if (sensor.isSelected()){
                GlideApp.with(mContext).load(R.drawable.blue_check).into(indicator);
                title.setTextColor(ContextCompat.getColor(mContext, R.color.orange));
            }else {
                title.setTextColor(ContextCompat.getColor(mContext, R.color.blue));
                indicator.setBackgroundColor(Color.TRANSPARENT);
            }

            view.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (CustomSensor list: sensors){
                        list.setSelected(false);
                    }
                    sensor.setSelected(true);
                    dropdown_title.setText(sensor.getTitle());
                    adapter = new Status_SensorList_Adapter(mContext,sensor,fragmentActivity,site,instances);
                    listView.setAdapter(adapter);
                    dialog.dismiss();
                }
            });


        return view;
    }


}

