package com.procuro.androidscm.Restaurant.ReportsFragment.Inspection;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsDateGrandChildList;
import com.procuro.androidscm.SCMDataManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import static com.procuro.androidscm.Restaurant.Login.Login.SCM_FILE_FOLDER;

public class Reports_inspection_Positional_Load_Webview extends Fragment {

    public WebView webView;
    ReportsDateGrandChildList daily;
    public ProgressBar progressBar;
    RadioGroup radioGroup;
    private RadioButton dailyReport ,weeklyReport,monthlyReport;
    private TextView wait;
    private String Filefound;

    public Reports_inspection_Positional_Load_Webview(ReportsDateGrandChildList daily) {
        // Required empty public constructor
        this.daily = daily;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.reports_right_pane_inspection_reports_daily_rush_ready_load_webview, container, false);

        webView = rootView.findViewById(R.id.webview);
        progressBar = rootView.findViewById(R.id.progressBar2);
        radioGroup = rootView.findViewById(R.id.radio_grp);
        dailyReport = rootView.findViewById(R.id.daily);
        monthlyReport = rootView.findViewById(R.id.monthly);
        weeklyReport = rootView.findViewById(R.id.weekly);
        wait = rootView.findViewById(R.id.wait);

        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        settings.setSupportZoom(true);
        settings.supportZoom();
        settings.setAllowFileAccess(true);
        settings.setAllowFileAccessFromFileURLs(true);
        settings.setDisplayZoomControls(false);
        settings.setBuiltInZoomControls(true);
        settings.setAppCacheEnabled(true);
        settings.setAllowContentAccess(true);
        webView.setWebChromeClient(new Webcromeclient());
        webView.setWebViewClient(new mywebviewclient());
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);



        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
          @Override
          public void onCheckedChanged(RadioGroup radioGroup, int i) {
              if (dailyReport.getId() == i){
                  dailyReport.setTextColor(Color.WHITE);
              }else {
                  dailyReport.setTextColor(Color.parseColor("#F87700"));
              }
              if (monthlyReport.getId() == i){
                  monthlyReport.setTextColor(Color.WHITE);
              }else {
                  monthlyReport.setTextColor(Color.parseColor("#F87700"));
              }
              if (weeklyReport.getId() == i){
                  weeklyReport.setTextColor(Color.WHITE);
              }else {
                  weeklyReport.setTextColor(Color.parseColor("#F87700"));
              }

              loadFileInExternalStore("index.html","CleaningReport");

          }
      });

        dailyReport.setChecked(true);
        return rootView;
    }

    private void FindSelectedDay(){
        if (dailyReport.isChecked()) {
            GenerateCleaningDailyReport(daily);

        }
        else if (weeklyReport.isChecked()){
            GenerateCleaningWeeklyReport(daily);
        }
        else if (monthlyReport.isChecked()){
            GenerateCleaningMonhtlyReport(daily);

        }
    }
    private void GenerateCleaningDailyReport(ReportsDateGrandChildList daily){
        String params = CreateCleaningDailyParams(daily);
        StringBuilder sbldr = new StringBuilder();
        sbldr.append("loadReport(");
        sbldr.append(params);
        sbldr.append(",");
        sbldr.append("'positioncleaning'");
        sbldr.append(")");
        System.out.println(sbldr);
        webView.evaluateJavascript(sbldr.toString(), null);

    }


    private String CreateCleaningDailyParams(ReportsDateGrandChildList daily) {
        String param = "";
        try {
            JSONObject dataJObj = new JSONObject();
            dataJObj.put("storeName", SCMDataManager.getInstance().getSelectedSite().getSite().sitename);
            dataJObj.put("spid", "wendys.png");
            param = dataJObj.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return param;

    }


    private void GenerateCleaningWeeklyReport(ReportsDateGrandChildList daily){
        String params = CreateCleaningWeeklyParams(daily);
        StringBuilder sbldr = new StringBuilder();
        sbldr.append("loadReport(");
        sbldr.append(params);
        sbldr.append(",");
        sbldr.append("'weeklycleaning'");
        sbldr.append(")");
        System.out.println(sbldr);
        webView.evaluateJavascript(sbldr.toString(), null);

    }


    private String CreateCleaningWeeklyParams(ReportsDateGrandChildList daily) {
        String param = "";
        try {
            JSONObject dataJObj = new JSONObject();
            dataJObj.put("storeName", SCMDataManager.getInstance().getSelectedSite().getSite().sitename);
            dataJObj.put("spid", "wendys.png");
            param = dataJObj.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return param;

    }

    private void GenerateCleaningMonhtlyReport(ReportsDateGrandChildList daily){
        String params = CreateCleaningMonthlyParams(daily);
        StringBuilder sbldr = new StringBuilder();
        sbldr.append("loadReport(");
        sbldr.append(params);
        sbldr.append(",");
        sbldr.append("'monthlycleaning'");
        sbldr.append(")");
        System.out.println(sbldr);
        webView.evaluateJavascript(sbldr.toString(), null);

    }


    private String CreateCleaningMonthlyParams(ReportsDateGrandChildList daily) {
        String param = "";
        try {
            JSONObject dataJObj = new JSONObject();
            dataJObj.put("storeName", SCMDataManager.getInstance().getSelectedSite().getSite().sitename);
            dataJObj.put("spid", "wendys.png");
            param = dataJObj.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return param;

    }



    private class Webcromeclient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                progressBar.setProgress(newProgress, true);
            }else {
                progressBar.setProgressTintList(ColorStateList.valueOf(Color.parseColor("#F68725")));
                progressBar.setProgress(newProgress);
            }
            if(newProgress == 100) {
                wait.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
        }
    }


    private class mywebviewclient extends WebViewClient {

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            FindSelectedDay();

        }
    }

    public  void listData(File dir, String filename,String rootFolder) {
        for (File file: dir.listFiles()) {
            if (file.isDirectory()){
                listData(file,filename,rootFolder);
            }else {
                if (file.getParentFile().getName().equalsIgnoreCase(rootFolder)){
                    if (file.getName().equalsIgnoreCase(filename)){
                        Filefound = "file://"+file.getAbsolutePath();
                        System.out.println("FILE PATH: "+Filefound);
                        break;
                    }
                }
            }
        }
    }

    public  void loadFileInExternalStore(String filename,String rootFolder){
        String root = Environment.getExternalStorageDirectory().toString();
        File destDir = new File(root, SCM_FILE_FOLDER);
        if(destDir.exists()){
            listData(destDir,filename,rootFolder);
            webView.loadUrl(Filefound);
        }
    }
}