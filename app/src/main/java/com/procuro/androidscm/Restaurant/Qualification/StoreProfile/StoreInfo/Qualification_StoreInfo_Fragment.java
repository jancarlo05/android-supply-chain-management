package com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.EmployeeList.EmployeeListActivity;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.EquipmenList.EquipmenlistData;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.EquipmenList.EquipmentListActivity;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.FormsList.FormListData;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.FormsList.FormListActivity;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.HoursOfOperation.HoursOfOperationActivity;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.HoursOfOperation.HoursOfOpetationData;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.StoreProfileOptions.StoreProfileOptionActivity;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.StoreProfileOptions.StoreProfileOptionsData;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.WeatherTask;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.DayOperationSchedule;
import com.procuro.apimmdatamanagerlib.HolidaySchedules;
import com.procuro.apimmdatamanagerlib.HoursOfOperation;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsConfiguration;

import java.text.SimpleDateFormat;
import java.util.Objects;
import java.util.TimeZone;


public class Qualification_StoreInfo_Fragment extends Fragment {


    private CustomSite storeList;
    private Site site;
    String API = "8118ed6ee68db2debfaaa5a44c832918";
    public static TextView street1,street2,city,state,country,phone,zip,
            weathers,equipment_number,employeelist,formlist,store_profile_options,hours_of_operation,temp_description;
    private ImageView icon;
    private ConstraintLayout equipment_list_container,employeelist_container,
            form_list_container,store_profile_option_container,hours_of_operation_container;
    private ListView emergency_list;
    private LinearLayout weather_container;

    public Qualification_StoreInfo_Fragment() {

        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.qualification_storeinfo_fragment, container, false);

        equipment_list_container = view.findViewById(R.id.equipment_list_container);
        equipment_number = view.findViewById(R.id.equipment_list);
        street1 = view.findViewById(R.id.street1);
        street2 = view.findViewById(R.id.street2);
        city = view.findViewById(R.id.city);
        state = view.findViewById(R.id.state);
        country = view.findViewById(R.id.country);
        phone = view.findViewById(R.id.phone);
        zip = view.findViewById(R.id.zip);
        weathers = view.findViewById(R.id.weather);
        icon = view.findViewById(R.id.icon);
        employeelist = view.findViewById(R.id.employee_list);
        employeelist_container = view.findViewById(R.id.employeelist_container);
        form_list_container = view.findViewById(R.id.form_list_container);
        formlist = view.findViewById(R.id.autofill);
        store_profile_option_container = view.findViewById(R.id.store_profile_option_container);
        store_profile_options = view.findViewById(R.id.store_profile_options);
        hours_of_operation_container = view.findViewById(R.id.hours_of_operation_container);
        hours_of_operation = view.findViewById(R.id.hours_of_operation);
        temp_description = view.findViewById(R.id.temp_description);
        weather_container =view.findViewById(R.id.weather_container);

        getSelectedStore();

        new WeatherTask(weathers,icon,getContext(),site,temp_description,weather_container).execute();

        setUpOncliscks();

        DisplayStoreInformation();

        DisplayStoreOptions();


        return view;
    }

    private void getSelectedStore(){
        try {
            if (QualificationData.getInstance().getSelectedSite()!=null){
                this.storeList = QualificationData.getInstance().getSelectedSite();
                this.site = storeList.getSite();
            }
            else {
                this.storeList = SCMDataManager.getInstance().getSelectedSite();
                this.site = storeList.getSite();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setUpOncliscks(){
        try {

            equipment_list_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), EquipmentListActivity.class);
                    requireActivity().startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
                }
            });

            employeelist_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), EmployeeListActivity.class);
                    requireActivity().startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());

                }
            });

            form_list_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), FormListActivity.class);
                    requireActivity().startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());

                }
            });

            store_profile_option_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), StoreProfileOptionActivity.class);
                    requireActivity().startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());

                }
            });

            hours_of_operation_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), HoursOfOperationActivity.class);
                    requireActivity().startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());

                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }


    }

    private void DisplayStoreInformation(){

        try {
            street1.setText(SCMTool.CheckString(site.Address.Address1,"--"));
            street2.setText(SCMTool.CheckString(site.Address.Address2,"--"));

            city.setText(SCMTool.CheckString(site.Address.City,"--"));
            state.setText(SCMTool.CheckString(site.Address.Province,"--"));
            country.setText(SCMTool.CheckString(site.Address.Country,"--"));
            phone.setText(SCMTool.CheckString(site.Contact.PhoneNumber,"--"));
            zip.setText(SCMTool.CheckString(site.Address.Postal,"--"));



        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public static String getKitchenTypeUsingTemplateCode(){
        String kitchentype = "---";
        StoreProfileOptionsData data = StoreProfileOptionsData.getInstance();
        if (data.getKitchentype()!=null){
            if (data.getKitchentype().contains("MO")){
                kitchentype = "Modern";

            }else if (data.getKitchentype().contains("IM")){
                kitchentype = "Image";
            }
            else if (data.getKitchentype().contains("SS")){
                kitchentype = "Special Site";
            }
        }
        return kitchentype;
    }

    public static String getAutofill(){
        String autofill = "Off";
        FormListData data = FormListData.getInstance();

        for (SiteSettingsConfiguration config : data.getSiteSettingsConfigurations()){
            if (config.name.equalsIgnoreCase("Config:autoFill")){
                autofill = "On";
                break;
            }
        }
        return  autofill;
    }

    public static void DisplayStoreOptions(){
        try {
            int employeelistCount = SCMTool.getActiveORinUseCount(QualificationData.getInstance().getUsers());
            employeelist.setText(String.valueOf(employeelistCount));

            int equipmentCount = SCMTool.getActiveORinUseCount(EquipmenlistData.getInstance().getSiteSettingsEquipments());
            equipment_number.setText(String.valueOf(equipmentCount));

            formlist.setText("Auto Fill ");
            formlist.append(getAutofill());

            store_profile_options.setText(getKitchenTypeUsingTemplateCode());

            hours_of_operation.setText(getStoreHour());

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static String getStoreHour() {
        String newline = " \n";
        HoursOfOpetationData data = HoursOfOpetationData.getInstance();
        if (data.getHoursOfOperation() != null) {
            HoursOfOperation operation = data.getHoursOfOperation();
            if (operation!=null){
                return Check7dayswithOpenTime(operation);
            }
        }

        return "--";
    }

    public static String Check7dayswithOpenTime(HoursOfOperation operation){
        SimpleDateFormat dateFormat = new SimpleDateFormat("h:mm a");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String newline = " \n";
        StringBuilder storehour = new StringBuilder();
        try {
            if (operation.openTime!=null && operation.closeTime!=null){
                if (operation.isOpen7Days){
                    storehour.append("7 days");
                    storehour.append(newline);
                    storehour.append(dateFormat.format(operation.openTime));
                    storehour.append(" - ");
                    storehour.append(dateFormat.format(operation.closeTime));
                    return storehour.toString();
                }
                else if (operation.dayOperationSchedules!=null){
                    if (operation.dayOperationSchedules.size()>=7){
                        storehour.append("S - S");
                        storehour.append(newline);
                        storehour.append(dateFormat.format(operation.openTime));
                        storehour.append(" - ");
                        storehour.append(dateFormat.format(operation.closeTime));
                        return storehour.toString();
                    }else {
                        String days ="Sunday Monday Tuesday Wednesday Thursday Friday Saturday";
                        int counter = 0;

                        for (DayOperationSchedule sched : operation.dayOperationSchedules){
                            if (sched.day.contains(days)){
                                char first = sched.day.charAt(0);
                                if (counter == 0){
                                }else {
                                    storehour.append(",");
                                    storehour.append(first);
                                }
                                counter++;
                            }
                        }

                        storehour.append(newline);
                        storehour.append(dateFormat.format(operation.openTime));
                        storehour.append(" - ");
                        storehour.append(dateFormat.format(operation.closeTime));
                        return storehour.toString();

                    }
                }
            }

            else if (operation.isOpen7Days){

                if (operation.isOpen24Hours){
                    storehour.append("7 days");
                    storehour.append(newline);
                    storehour.append("24 hours");
                    return storehour.toString();
                }

            }

            else if (operation.dayOperationSchedules!=null){
                if (operation.dayOperationSchedules.size()==7){
                    storehour.append("S - S");
                    storehour.append(newline);
                    storehour.append(dateFormat.format(operation.dayOperationSchedules.get(0).openTime));
                    storehour.append(" - ");
                    storehour.append(dateFormat.format(operation.dayOperationSchedules.get(0).closeTime));
                    return storehour.toString();
                }else {
                    String days ="Sunday Monday Tuesday Wednesday Thursday Friday Saturday";
                    int counter = 0;

                    for (DayOperationSchedule sched : operation.dayOperationSchedules){
                        if (sched.day.contains(days)){
                            char first = sched.day.charAt(0);
                            if (counter == 0){
                            }else {
                                storehour.append(",");
                                storehour.append(first);
                            }
                            counter++;
                        }
                    }

                    storehour.append(newline);
                    storehour.append(dateFormat.format(operation.dayOperationSchedules.get(0).openTime));
                    storehour.append(" - ");
                    storehour.append(dateFormat.format(operation.dayOperationSchedules.get(0).closeTime));
                    return storehour.toString();

                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }


        return "--";
    }

}
