package com.procuro.androidscm.Restaurant.UserProfile;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.SCMDataManager;

import java.util.ArrayList;

import static com.procuro.androidscm.Restaurant.UserProfile.User_Skills_Fragment.siteAllowed;


public class User_site_allowed_list_adapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<String> arraylist;
    private ArrayList<CustomSite>customSites = SCMDataManager.getInstance().getCustomSites();
    private boolean EnableEdit ;


    public User_site_allowed_list_adapter(Context context, ArrayList<String> arraylist, boolean EnableEdit) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;
        this.EnableEdit =  EnableEdit;

    }

    public class ViewHolder {
        TextView name;
        CheckBox checkBox;
    }

    @Override
    public int getCount() {
        return customSites.size();
    }

    @Override
    public Object getItem(int position) {
        return customSites.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        holder = new ViewHolder();
        view = inflater.inflate(R.layout.user_sitelist_data, null);
        view.setTag(holder);

        final CustomSite customSite = customSites.get(position);

        holder.checkBox = view.findViewById(R.id.checkbox);
        holder.name = view.findViewById(R.id.name);

        holder.name.setText(customSite.getSite().sitename);


        if (arraylist.contains(customSite.getSite().sitename)){

            holder.checkBox.setChecked(true);
        }else {
            holder.checkBox.setChecked(false);
        }


        if (!EnableEdit){
            holder.checkBox.setEnabled(false);
            view.setEnabled(false);
        }else {
            view.setEnabled(true);
            holder.checkBox.setEnabled(true);
        }


        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked){
                    AddRoleToTemp(customSite);
                }
                else {
                    RemoveToTemp(customSite);
                }

                CheckSelectedSiteAllowed();
                System.out.println(UserProfileData.getInstance().getUpdate_Siteallowed());
            }
        });

        return view;
    }

    private void AddRoleToTemp(CustomSite customSite){

        boolean selected =false;

        UserProfileData data = UserProfileData.getInstance();
        if (!data.getUpdate_Siteallowed().contains(customSite.getSite().sitename)){
            data.getUpdate_Siteallowed().add(customSite.getSite().sitename);
        }

        if (data.getSelectedSites()!=null){
            for (CustomSite site : data.getSelectedSites()){
                if (site.getSite().sitename.equalsIgnoreCase(customSite.getSite().sitename)){
                    selected = true;
                    break;
                }
            }
        }else {
            data.setSelectedSites(new ArrayList<CustomSite>());
        }

        if (!selected){
            data.getSelectedSites().add(customSite);
        }
    }

    private void RemoveToTemp(CustomSite customSite){
        UserProfileData data = UserProfileData.getInstance();
        for (int i = 0; i <data.getUpdate_Siteallowed().size() ; i++) {
            String site =data.getUpdate_Siteallowed().get(i);
            if (site.equalsIgnoreCase(customSite.getSite().sitename)){
                data.getUpdate_Siteallowed().remove(i);
                arraylist = data.getUpdate_Siteallowed();
                notifyDataSetChanged();
                break;
            }
        }
    }

    public static void CheckSelectedSiteAllowed(){
        try {
            QualificationData data = QualificationData.getInstance();
            UserProfileData userdata = UserProfileData.getInstance();
            boolean Exist = false;

            if (userdata.getUpdate_Siteallowed().size()>0){
                for (String role : userdata.getUpdate_Siteallowed()){
                    if (role.equalsIgnoreCase(data.getSelectedSite().getSite().sitename)){
                        siteAllowed.setText(role);
                        Exist = true;
                    }
                }
                if (!Exist){
                    siteAllowed.setText(userdata.getUpdate_Siteallowed().get(0));
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}

