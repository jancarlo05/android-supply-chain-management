package com.procuro.androidscm.Restaurant.Qualification;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.Calendar.CalendarActivity;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.DashBoard.DashboardActivity;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.Dashboard_Staffing_Activity;
import com.procuro.androidscm.Restaurant.EmployeeList.EmployeeListActivity;
import com.procuro.androidscm.Restaurant.Guide.GuideActivity;
import com.procuro.androidscm.Restaurant.PimmyNews.Pimmy_News_DialogFragment;
import com.procuro.androidscm.Restaurant.Qualification.Map.Map_Fragment;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Area;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_District;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Division;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Regional;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationFragment;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.Qualification_Store_Profile_Fragment;
import com.procuro.androidscm.Restaurant.Qualification.UserProfile.Qualification_UserProfile_Regional_Child_level_Fragment_2;
import com.procuro.androidscm.Restaurant.QuickAccessMenu.QuickAccessMenu;
import com.procuro.androidscm.Restaurant.SOS.SOSActivity;
import com.procuro.androidscm.Restaurant.SOS.SOSData;
import com.procuro.androidscm.Restaurant.ScoreCard.ScoreCardFragment;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileActivity;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileActivityOverAll;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileData;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.PimmForm;
import com.procuro.apimmdatamanagerlib.SMSPositioning;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;


public class QualificationActivityFragment extends Fragment {


    info.hoang8f.android.segmented.SegmentedGroup infogrp;
    public static RadioButton map , storeProfile,userProfile,scoreCard;
    Button back,Dashboard,profile,quick_a_menu,sos,Covid;
    public static TextView name;
    private com.procuro.apimmdatamanagerlib.aPimmDataManager dataManager;
    private Site site = SCMDataManager.getInstance().getSelectedSite().getSite();
    private ProgressDialog progressDialog;
    private LinearLayout root;
    private Corp_Regional regional;
    private Corp_Division division;
    private Corp_District district;
    private Corp_Area area;
    private CustomSite customSite;
    private Bundle fragmentbundle;

    public QualificationActivityFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.store_profile_activity, container, false);

        infogrp = view.findViewById(R.id.segmented2);
        map =  view.findViewById(R.id.map);
        storeProfile = view.findViewById(R.id.store_profile);
        userProfile = view.findViewById(R.id.user_profile);
        scoreCard = view.findViewById(R.id.score_card);
        back = view.findViewById(R.id.home);
        name = view.findViewById(R.id.username);
        root = view.findViewById(R.id.root);
        Dashboard = view.findViewById(R.id.dashboard);
        quick_a_menu = view.findViewById(R.id.quick_a_menu);
        sos  = view.findViewById(R.id.sos);
        Covid = view.findViewById(R.id.covid);

        CheckSMSPositionForm();
        setupOnclicks();

        return view;
    }

    private void CheckSMSPositionForm(){
        QualificationData data = QualificationData.getInstance();
        if (!data.isAllstore()){
            try {
                DownloadPositionForm(data.getSelectedSite().getSite().siteid);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private void setupOnclicks(){
        try {
            infogrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int id) {

                    if (id == map.getId()){
                        Display_facility_map();

                    }else if (id == storeProfile.getId()){
                        DisplayStoreProfile();

                    }else if (id == userProfile.getId()){
                        DisplayUserProfile();

                    }else if (id == scoreCard.getId()){
                        DisplayScorecard();
                    }
                }
            });

            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.resturant_fragment_container,
                            new QualificationFragment()).commit();
                }
            });

            Dashboard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), DashboardActivity.class);
                    requireContext().startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());

                }
            });
            quick_a_menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DisplayQuickAMenu(getContext(),quick_a_menu,getActivity());
                }
            });


            sos.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SOSData data = new SOSData();
                    data.setSite(SCMDataManager.getInstance().getSelectedSite().getSite());
                    SOSData.setInstance(data);
                    Intent intent = new Intent(getActivity(), SOSActivity.class);
                    requireActivity().startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
                }
            });


            QualificationData data = QualificationData.getInstance();
            if (data.isSelectedSiteEnabled()){
                storeProfile.setChecked(true);
            }else {
                map.setChecked(true);
            }

            Covid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    QuickAccessMenu.DisplayCovidFragment(getActivity());
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }


    }
    private void Display_facility_map() {
        Fragment fragment = new Map_Fragment();
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.qualification_store_profile, fragment).commit();
    }

    private void DisplayStoreProfile() {
        Fragment fragment = new Qualification_Store_Profile_Fragment();
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.qualification_store_profile,
                fragment).commit();
    }

    private void DisplayUserProfile() {
        Fragment fragment = new Qualification_UserProfile_Regional_Child_level_Fragment_2();
        fragment.setArguments(fragmentbundle);
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.qualification_store_profile,
                fragment).commit();
    }

    private void DisplayScorecard() {
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.qualification_store_profile,
                new ScoreCardFragment()).commit();

    }

    public void DisplayQuickAMenu(final Context context, View anchor, final FragmentActivity fragmentActivity) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.BOTTOM)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .transparentOverlay(false)
                .arrowColor(ContextCompat.getColor(context, R.color.black))
                .transparentOverlay(true)
                .contentView(R.layout.quick_access_menu)
                .focusable(true)
                .build();
        tooltip.show();

        ConstraintLayout calendar,dop,employee_sched,help,news,reports_portal,review_n_verify,skpe,sos,storeprofile,temp_check,user_profile,employee_list;

        calendar = tooltip.findViewById(R.id.calendar);
        dop = tooltip.findViewById(R.id.dashboard);
        employee_sched = tooltip.findViewById(R.id.employee_schedule);
        help = tooltip.findViewById(R.id.menu_help);
        news = tooltip.findViewById(R.id.menu_news);
        reports_portal = tooltip.findViewById(R.id.menu_reports_portal);
        review_n_verify = tooltip.findViewById(R.id.menu_review_n_verify);
        skpe = tooltip.findViewById(R.id.menu_skype);
        sos = tooltip.findViewById(R.id.menu_sos);
        storeprofile = tooltip.findViewById(R.id.menu_store_profile);
        temp_check = tooltip.findViewById(R.id.menu_temp_check);
        user_profile = tooltip.findViewById(R.id.menu_user_profile);
        employee_list = tooltip.findViewById(R.id.employee_list);

        //remove
        temp_check.setVisibility(View.GONE);
        employee_list.setVisibility(View.GONE);

        //disable
        reports_portal.setEnabled(false);
        reports_portal.setAlpha(.5f);

        employee_sched.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SCMDataManager.getInstance().setDashboardKitchenData(null);
                SCMDataManager.getInstance().setStoreUsers(null);
                SCMDataManager.getInstance().setPositionForm(null);
                Intent intent = new Intent(fragmentActivity, Dashboard_Staffing_Activity.class);
                fragmentActivity.startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(fragmentActivity).toBundle());
                tooltip.dismiss();
            }
        });


        user_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(fragmentActivity, UserProfileActivityOverAll.class);
                UserProfileData data = new UserProfileData();
                data.setUserEditEnabled(false);
                data.setCustomSite(SCMDataManager.getInstance().getSelectedSite());
                CustomUser user = new CustomUser(SCMDataManager.getInstance().getUser());
                data.setCustomUser(user);
                data.setFromQuickAccess(true);
                data.setIsupdate(true);
                UserProfileData.setInstance(data);
                fragmentActivity.startActivity(intent);
                tooltip.dismiss();
            }
        });

        dop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(fragmentActivity, DashboardActivity.class);
                Objects.requireNonNull(fragmentActivity).startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(fragmentActivity).toBundle());
                tooltip.dismiss();
            }
        });

        storeprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(fragmentActivity, QualificationActivity.class);
                QualificationData data = new QualificationData();
                data.setSelectedSiteEnabled(true);
                data.setSelectedSite(SCMDataManager.getInstance().getSelectedSite());
                QualificationData.setInstance(data);

                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.resturant_fragment_container,
                        new QualificationActivityFragment()).commit();

//                Objects.requireNonNull(fragmentActivity).startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(fragmentActivity).toBundle());
                tooltip.dismiss();
            }
        });

        sos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SOSData data = new SOSData();
                data.setSite(SCMDataManager.getInstance().getSelectedSite().getSite());
                SOSData.setInstance(data);
                Intent intent = new Intent(fragmentActivity, SOSActivity.class);
                Objects.requireNonNull(fragmentActivity).startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(fragmentActivity).toBundle());
                tooltip.dismiss();
            }
        });

        calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(fragmentActivity, CalendarActivity.class);
                Objects.requireNonNull(fragmentActivity).startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(fragmentActivity).toBundle());
                tooltip.dismiss();
            }
        });

        news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayPimmyNews(getActivity());
                tooltip.dismiss();

            }
        });

        skpe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initiateSkypeUri(getActivity(),"");
                tooltip.dismiss();
            }
        });

        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(fragmentActivity, GuideActivity.class);
                Objects.requireNonNull(fragmentActivity).startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(fragmentActivity).toBundle());
                tooltip.dismiss();
            }
        });

        employee_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.resturant_fragment_container,
                        new QualificationFragment()).commit();

                QualificationData data = new QualificationData();
                data.setSelectedSiteEnabled(true);
                data.setSelectedSite(SCMDataManager.getInstance().getSelectedSite());
                data.setUsers(SCMDataManager.getInstance().getStoreUsers());
                QualificationData.setInstance(data);

                Intent intent = new Intent(fragmentActivity, EmployeeListActivity.class);
                Objects.requireNonNull(fragmentActivity).startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(fragmentActivity).toBundle());


                tooltip.dismiss();
            }
        });


    }

    private void DisplayPimmyNews(FragmentActivity activity){
        DialogFragment newFragment = Pimmy_News_DialogFragment.newInstance();
        assert getFragmentManager() != null;
        newFragment.show(activity.getSupportFragmentManager(), "dialog");
    }

    public void initiateSkypeUri(Activity activity, String mySkypeUri) {

        // Make sure the Skype for Android client is installed.
        if (!isSkypeClientInstalled(activity)) {
            goToMarket(activity);
        }else {
            Uri skypeUri = Uri.parse(mySkypeUri);
            Intent myIntent = new Intent(Intent.ACTION_VIEW, skypeUri);
            myIntent.setComponent(new ComponentName("com.skype.raider", "com.skype.raider.Main"));
            myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivity(myIntent);
        }
    }

    private boolean isSkypeClientInstalled(Activity activity) {

        PackageManager myPackageMgr = activity.getPackageManager();
        try {
            myPackageMgr.getPackageInfo("com.skype.raider", PackageManager.GET_ACTIVITIES);
        }
        catch (PackageManager.NameNotFoundException e) {
            return (false);
        }
        return (true);
    }

    private void goToMarket(Activity activity) {
        Uri marketUri = Uri.parse("market://details?id=com.skype.raider");
        Intent myIntent = new Intent(Intent.ACTION_VIEW, marketUri);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        requireActivity().startActivity(myIntent);

    }

    @Override
    public void onDetach() {
        super.onDetach();
//        QualificationData data = new QualificationData();
//        QualificationData.setInstance(data);
    }

    public static void DownloadPositionForm(final String siteid){
        aPimmDataManager dataManager = aPimmDataManager.getInstance();
        ArrayList<PimmForm>PimmFormList = SCMDataManager.getInstance().getPimmFormArrayList();
        if (PimmFormList == null){
            dataManager.getFormDefinitionListForAppId(new OnCompleteListeners.getFormDefinitionListForAppIdCallbackListener() {
                @Override
                public void getFormDefinitionListForAppIdCallback(ArrayList<PimmForm> pimmFormArrayList, Error error) {
                    if (error == null){
                        for (final PimmForm form: pimmFormArrayList){
                            if (form.name.equalsIgnoreCase("SMS:Positioning")) {
                                Download_SMS_POSITIONING(siteid, form.formDefinitionID);
                                break;
                            }
                        }
                    }
                }
            });
        }
        else {
            for (final PimmForm form: PimmFormList){
                if (form.name.equalsIgnoreCase("SMS:Positioning")) {
                    Download_SMS_POSITIONING(siteid, form.formDefinitionID);
                    break;
                }
            }
        }
    }

    public static void Download_SMS_POSITIONING(String siteID,String formID){
        aPimmDataManager dataManager = aPimmDataManager.getInstance();
        dataManager.getLatestFormForReferenceId(siteID, formID, new OnCompleteListeners.getLatestFormForReferenceIdCallbackListener() {
            @Override
            public void getLatestFormForReferenceId(PimmForm pimmForm, Error error) {
                if (error == null){
                    try {
                        if (pimmForm!=null){
                            if (pimmForm.StringBody!=null){
                                JSONObject jsonObject = new JSONObject(pimmForm.StringBody);
                                SMSPositioning smsPositioning = new SMSPositioning();
                                smsPositioning.readFromJSONObject(jsonObject);
                                QualificationData qualificationData = QualificationData.getInstance();
                                qualificationData.setPositionForm(smsPositioning);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }



}


