package com.procuro.androidscm.Restaurant.Status.StatusFragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.procuro.androidscm.GlideApp;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSensor;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationFragment;
import com.procuro.androidscm.Restaurant.Status.StatusActivity;
import com.procuro.androidscm.Restaurant.Status.StatusActivityFragment;
import com.procuro.androidscm.Restaurant.Status.StatusData;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.PimmInstance;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Status_All_Store_List_adapter extends BaseExpandableListAdapter implements ExpandableListAdapter {

    private Context context;
    private ArrayList<StatusChain> chains;
    private FragmentActivity fragmentActivity;
    private com.procuro.apimmdatamanagerlib.aPimmDataManager dataManager;
    private boolean isFromSearch;

    public Status_All_Store_List_adapter(Context context, ArrayList<StatusChain> chains,
                                         FragmentActivity fragmentActivity,boolean isFromSearch) {
        this.chains = chains;
        this.context = context;
        this.fragmentActivity = fragmentActivity;
        this.isFromSearch = isFromSearch;
    }

    @Override
    public int getGroupCount() {
        return chains.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return chains.get(groupPosition).getCustomSites().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return chains.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return chains.get(groupPosition).getCustomSites().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        StatusChain chain = chains.get(groupPosition);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.qualification_chain_level, parent, false);

        TextView name = contentView.findViewById(R.id.name);
        TextView count = contentView.findViewById(R.id.childcount);

        if (chain.getCustomSites()!=null){
            count.setText(String.valueOf(chain.getCustomSites().size()));
        }

        name.setText(chain.getName());
        return contentView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, final ViewGroup parent) {
        final CustomSite site = chains.get(groupPosition).getCustomSites().get(childPosition);
        View contentView = LayoutInflater.from(context).inflate(R.layout.status_site_level, parent, false);

        TextView name = contentView.findViewById(R.id.name);
        ImageView icon   = contentView.findViewById(R.id.icon);
        final LinearLayout arrow_container = contentView.findViewById(R.id.arrow_container);
        final LinearLayout contianer = contentView.findViewById(R.id.container);
        TextView address = contentView.findViewById(R.id.address);

        site.setSelected(false);

        if (site.getSite()!=null){

            if (site.getSite().os.equalsIgnoreCase("Custom")){

                GlideApp.with(context).load(R.drawable.store_light_blue).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);

            }else {
                if (site.getSite().AlarmSeverity == 0 ){
                    GlideApp.with(context).load(R.drawable.store_blue).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
                }
                else if (site.getSite().AlarmSeverity == 3 ){
                    GlideApp.with(context).load(R.drawable.store_green).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
                }
                else if (site.getSite().AlarmSeverity == 5 ){
                    GlideApp.with(context).load(R.drawable.store_yellow).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
                }
                else if (site.getSite().AlarmSeverity == 9 ){
                    GlideApp.with(context).load(R.drawable.store_red).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
                }
                else {
                    GlideApp.with(context).load(R.drawable.store_gray).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
                }
            }

        }else {
            GlideApp.with(context).load(R.drawable.store_gray).into(icon);
        }

        address.setText(SCMTool.getCompleteAddress(site.getSite()));
        name.setText(site.getSite().sitename);


        arrow_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (site.isSelected()){
                    site.setSelected(false);
                    contianer.setVisibility(View.GONE);
                    contianer.removeAllViews();
                    arrow_container.setRotation(0);
                }else {
                    site.setSelected(true);
                    contianer.setVisibility(View.VISIBLE);
                    SensorCategoryView(contianer,site,parent);
                    arrow_container.setRotation(180);
                }
            }
        });


        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                isFromSearch = true;
//                setInstanceForStatus(site);
                site.setSelected(true);
                contianer.setVisibility(View.VISIBLE);
                arrow_container.setRotation(180);
                SensorCategoryView(contianer,site,parent);
            }
        });

        if (isFromSearch){
            site.setSelected(true);
        }

        if (site.isSelected()){
            contianer.setVisibility(View.VISIBLE);
            arrow_container.setRotation(180);
            SensorCategoryView(contianer,site,parent);
        }else {
            contianer.setVisibility(View.GONE);
            arrow_container.setRotation(0);
        }



        return contentView;
    }

    private void SensorCategoryView(final LinearLayout container, final CustomSite site, final ViewGroup parent){

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View contentView = inflater.inflate(R.layout.status_no_metric_collection, parent, false);

        final TextView message = contentView.findViewById(R.id.message);
        final ProgressBar progressBar = contentView.findViewById(R.id.progresbar);
        message.setVisibility(View.GONE);
        container.addView(contentView);

        dataManager = aPimmDataManager.getInstance();
        if (site.getSite()!=null) {
            dataManager.getInstanceListForSiteId(site.getSite().DeviceID, new OnCompleteListeners.getInstanceListForSiteIdCallbackListener() {
                @Override
                public void getInstanceListForSiteIdCallback(ArrayList<PimmInstance> pimmInstanceArrayList, Error error) {
                    if (error == null) {
                        if (pimmInstanceArrayList.size() > 0) {
                            container.removeAllViews();
                            FilterObjectNames(pimmInstanceArrayList,container,parent,site);
                        }else {
                            progressBar.setVisibility(View.GONE);
                            message.setVisibility(View.VISIBLE);
                        }
                    }else {
                        progressBar.setVisibility(View.GONE);
                        message.setVisibility(View.VISIBLE);
                    }
                }
            });
        }else {
            progressBar.setVisibility(View.GONE);
            message.setVisibility(View.VISIBLE);
        }


    }

    private void FilterObjectNames(ArrayList<PimmInstance> temperatureLists,LinearLayout container,ViewGroup parent,CustomSite customSite){
        ArrayList<String>objectname = new ArrayList<>();
        ArrayList<CustomSensor>objects=  new ArrayList<>();
        for (PimmInstance newObjects: temperatureLists){
            if (!objectname.contains(newObjects.objectName)){
                objectname.add(newObjects.objectName);
                objects.add(new CustomSensor(newObjects.objectName));
                System.out.println("Description: "+newObjects.description);
            }
        }

        PopulateInstanceByObjectName(temperatureLists,objects,container,parent,customSite);
    }

    private void PopulateInstanceByObjectName(ArrayList<PimmInstance>pimmInstances,
                                              ArrayList<CustomSensor>newObjects,LinearLayout container,ViewGroup parent,CustomSite customSite){
        for (PimmInstance instance: pimmInstances){
            for (CustomSensor newObject: newObjects){
                if (instance.objectName.equalsIgnoreCase(newObject.getTitle())){
                    if (newObject.getObject() == null){
                        ArrayList<PimmInstance>newObs = new ArrayList<>();
                        newObs.add(instance);
                        newObject.setObject(newObs);
                    }else {
                        newObject.getObject().add(instance);
                    }
                }
            }
        }
        PopulateInstanceByCoolsersAndFreezers(newObjects,container,parent,customSite);
    }

    private void PopulateInstanceByCoolsersAndFreezers(ArrayList<CustomSensor>sensors,LinearLayout container,ViewGroup parent,CustomSite customSite){

        for (CustomSensor sensor : sensors){
            if (sensor.getTitle().equalsIgnoreCase("Temperature")) {
                ArrayList<CustomPimmInstance> customPimmInstances = new ArrayList<>();
                ArrayList<String> titles = new ArrayList<>();
                if (sensor.getObject() != null) {
                    for (PimmInstance instance : sensor.getObject()) {
                        if (instance.description.toLowerCase().contains("cooler")) {
                            if (!titles.contains("Coolers")) {
                                titles.add("Coolers");
                                ArrayList<PimmInstance> instances = new ArrayList<>();
                                instances.add(instance);
                                customPimmInstances.add(new CustomPimmInstance("Coolers", instances));

                            } else {
                                for (CustomPimmInstance pimmInstance : customPimmInstances) {
                                    if (pimmInstance.getTitle().equalsIgnoreCase("Coolers")) {
                                        pimmInstance.getInstances().add(instance);
                                    }
                                }
                            }
                        } else if (instance.description.toLowerCase().contains("freezer")) {

                            if (!titles.contains("Freezers")) {
                                titles.add("Freezers");
                                ArrayList<PimmInstance> instances = new ArrayList<>();
                                instances.add(instance);
                                customPimmInstances.add(new CustomPimmInstance("Freezers", instances));
                                System.out.println("Freezer: "+instance.description);

                            } else {
                                for (CustomPimmInstance pimmInstance : customPimmInstances) {
                                    if (pimmInstance.getTitle().equalsIgnoreCase("Freezers")) {
                                        pimmInstance.getInstances().add(instance);
                                        System.out.println("Freezer: "+instance.description);
                                    }
                                }
                            }
                        }
                    }
                }


                Collections.sort(customPimmInstances, new Comparator<CustomPimmInstance>() {
                    @Override
                    public int compare(CustomPimmInstance o1, CustomPimmInstance o2) {
                        return o1.getTitle().compareToIgnoreCase(o2.getTitle());
                    }
                });

                for (CustomPimmInstance pimmInstance : customPimmInstances){
                    getTemperatureLevel(container,pimmInstance,parent,sensors,customSite);
                }

            }
            else if (sensor.getTitle().equalsIgnoreCase("Product")){
                if (sensor.getObject()!=null){
                    getProductLevel(container,sensor,parent,sensors,customSite);
                }
            }
        }
    }

    private void getTemperatureLevel(LinearLayout container, final CustomPimmInstance pimmInstance, ViewGroup parent,ArrayList<CustomSensor>customSensors,CustomSite customSite){

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.status_sensor_category_level, parent, false);
        TextView name = contentView.findViewById(R.id.name);
        TextView count = contentView.findViewById(R.id.childcount);
        LinearLayout contianer = contentView.findViewById(R.id.container);

        if (pimmInstance.getInstances()!=null){
            count.setText(String.valueOf(pimmInstance.getInstances().size()));

            int counter= 0;
            if (isFromSearch){
                if (pimmInstance.getInstances()!=null && pimmInstance.getInstances().size()>0){
                    setInstanceForTemperature(customSite,pimmInstance.getInstances().get(0));
                }
            }else {
                for (PimmInstance sensors :  pimmInstance.getInstances()){
                    getFilteredSensors(contianer,sensors,parent,customSensors,customSite);
                }
            }
        }
        name.setText(pimmInstance.getTitle());
        contentView.setEnabled(false);

        container.addView(contentView);
    }

    private void getProductLevel(LinearLayout container, final CustomSensor pimmInstance, ViewGroup parent,ArrayList<CustomSensor>customSensors,CustomSite customSite){

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.status_sensor_category_level, parent, false);
        TextView name = contentView.findViewById(R.id.name);
        TextView count = contentView.findViewById(R.id.childcount);
        LinearLayout contianer = contentView.findViewById(R.id.container);

        if (pimmInstance.getTitle()!=null){
            count.setText(String.valueOf(pimmInstance.getObject().size()));

            for (PimmInstance sensors :  pimmInstance.getObject()){
                getFilteredSensors(contianer,sensors,parent,customSensors,customSite);
            }

        }

        name.setText(pimmInstance.getTitle());

        contentView.setEnabled(false);

        container.addView(contentView);
    }

    private void getFilteredSensors(LinearLayout container, final PimmInstance temperature, ViewGroup parent, ArrayList<CustomSensor>customSensors, final CustomSite customSite){

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.status_temperture_list_data, parent, false);

        ImageView valueContainer = view.findViewById(R.id.value_container);
        TextView value = view.findViewById(R.id.value);
        TextView name = view.findViewById(R.id.name);
        TextView signal = view.findViewById(R.id.signal);
        TextView battery = view.findViewById(R.id.battery);
        boolean hasDiagnostics = false;

        name.setText(temperature.description);

        if (temperature.severity == 0 ){
            GlideApp.with(context).load(R.drawable.empty_circle_blue).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(valueContainer);
            value.setTextColor(ContextCompat.getColor(context, R.color.blue));
        }
        else if (temperature.severity == 3 ){
            GlideApp.with(context).load(R.drawable.empty_circle_green).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(valueContainer);
            value.setTextColor(ContextCompat.getColor(context, R.color.green));
        }
        else if (temperature.severity == 5 ){
            GlideApp.with(context).load(R.drawable.empty_circle_yellow).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(valueContainer);
            value.setTextColor(ContextCompat.getColor(context, R.color.yellow));
        }
        else if (temperature.severity == 9 ){
            GlideApp.with(context).load(R.drawable.empty_circle_red).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(valueContainer);
            value.setTextColor(ContextCompat.getColor(context, R.color.red));
        }
        else  {
            GlideApp.with(context).load(R.drawable.empty_circle_gray).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(valueContainer);
            value.setTextColor(ContextCompat.getColor(context, R.color.gray));
            value.setText("--");
            signal.append("--");
            battery.append("--");
        }

        if (temperature.severity == 10){
            value.setText("--");
            signal.append("--");
            battery.append("--");

        } else {
            if (customSensors!=null){
                for (CustomSensor sensor : customSensors){
                    if (sensor.getTitle().equalsIgnoreCase("Diagnostics")){
                        hasDiagnostics = true;
                        for (PimmInstance instance: sensor.getObject()){

                            if (instance.description.contains(temperature.description)){

                                if (instance.description.contains("Signal")){

                                    if (instance.staticValue != null){

                                        if (!instance.staticValue.equalsIgnoreCase("")){

                                            if (Double.valueOf(instance.staticValue)<= 14.9){
                                                String red = "Poor";
                                                SpannableString redSpannable= new SpannableString(red);
                                                redSpannable.setSpan(new ForegroundColorSpan(Color.RED), 0, red.length(), 0);
                                                signal.append(redSpannable);

                                            }else if (Double.valueOf(instance.staticValue)>= 15 &&Double.valueOf(instance.staticValue)<= 59.9){
                                                signal.append("Good");

                                            }
                                            else {
                                                signal.append("Excellent");

                                            }
                                        }else {
                                            signal.append("--");
                                        }

                                    }else {
                                        signal.append("--");
                                    }

                                    System.out.println("STATIC VALUE :  "+instance.description + " "+instance.staticValue);
                                }
                                else if (instance.description.contains("Battery")){
                                    if (instance.staticValue!=null){
                                        if (!instance.staticValue.equalsIgnoreCase("")){
                                            if (Double.valueOf(instance.staticValue)<=2.6) {
                                                String red = "Replace";
                                                SpannableString redSpannable= new SpannableString(red);
                                                redSpannable.setSpan(new ForegroundColorSpan(Color.RED), 0, red.length(), 0);
                                                battery.append(redSpannable);
                                            }
                                            else if (Double.valueOf(instance.staticValue) == 2.7){
                                                battery.append("Order New");

                                            }else if (Double.valueOf(instance.staticValue) >= 2.8){
                                                battery.append("Good");

                                            }
                                        }else {
                                            battery.append("--");
                                        }

                                    }else {
                                        battery.append("--");
                                    }
                                }else {
                                    battery.append("--");
                                    signal.append("--");
                                }
                            }
                        }
                    }
                }
            }

            if (!hasDiagnostics){
                System.out.println("NO DIAGNOSTICS");
                battery.append("--");
                signal.append("--");
            }

            if (temperature.staticValue!=null){
                if (temperature.staticValue.length()>4){
                    value.setText(temperature.staticValue.substring(0,4));
                }else {
                    value.setText(temperature.staticValue);
                }
            }else {
                value.setText("--");
            }

        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setInstanceForTemperature(customSite,temperature);
            }
        });



        container.addView(view);
    }

    private void setInstanceForTemperature(CustomSite site, PimmInstance pimmInstance){
        isFromSearch = false;
        StatusData data = new StatusData();
        data.setSelectedSite(site);
        data.setAllStoreEnabled(true);
        data.setSelectedSite(site);
        data.setPimmDevice(site.getPimmDevice());
        data.setPimmInstance(pimmInstance);
        data.setTemperatureViewEnabled(true);
        StatusData.setInstance(data);
//        Intent intent = new Intent(fragmentActivity, StatusActivity.class);
//        fragmentActivity.startActivity(intent);
        fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.resturant_fragment_container,
                new StatusActivityFragment()).commit();



    }

    private void setInstanceForStatus(CustomSite site ){

        StatusData data = new StatusData();
        data.setSelectedSite(site);
        data.setAllStoreEnabled(true);
        data.setSelectedSiteEnabled(false);
        data.setStateEnabled(false);
        data.setOwnerShipViewEnabled(false);
        StatusData.setInstance(data);
//        Intent intent = new Intent(fragmentActivity, StatusActivity.class);
//        fragmentActivity.startActivity(intent);
        fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.resturant_fragment_container,
                new StatusActivityFragment()).commit();
    }


    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }



}
