package com.procuro.androidscm.Restaurant.Status.StatusFragment.StoreListFragment;

import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.apimmdatamanagerlib.Site;

import java.util.ArrayList;

public class SiteList {
    String Province;
    ArrayList<Site>sites;
    ArrayList<CustomSite>customSites;
    boolean selected;

    public SiteList() {

    }

    public SiteList(String province, ArrayList<CustomSite> customSites) {
        Province = province;
        this.customSites = customSites;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public ArrayList<CustomSite> getCustomSites() {
        return customSites;
    }

    public void setCustomSites(ArrayList<CustomSite> customSites) {
        this.customSites = customSites;
    }

    public String getProvince() {
        return Province;
    }

    public void setProvince(String province) {
        Province = province;
    }

    public ArrayList<Site> getSites() {
        return sites;
    }

    public void setSites(ArrayList<Site> sites) {
        this.sites = sites;
    }
}
