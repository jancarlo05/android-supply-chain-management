package com.procuro.androidscm.Restaurant.Schema;

import com.procuro.apimmdatamanagerlib.PimmBaseObject;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class Days extends PimmBaseObject {

    public String day ;
    public String formName ;
    public String section;
    public ArrayList<String>dayparts;
    public String start;
    public String end;

    //For CustomPurpose
    ArrayList<Forms>forms;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("dayparts")){
            ArrayList<String>items = new ArrayList<>();
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    JSONArray array = (JSONArray) value;
                    try {
                        for (int i = 0; i <array.length() ; i++) {
                            items.add(array.getString(i));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            this.dayparts = items;
        }
    }

    public ArrayList<Forms> getForms() {
        return forms;
    }

    public void setForms(ArrayList<Forms> forms) {
        this.forms = forms;
    }
}
