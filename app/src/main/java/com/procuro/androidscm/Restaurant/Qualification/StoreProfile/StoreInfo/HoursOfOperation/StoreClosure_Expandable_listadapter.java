package com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.HoursOfOperation;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ExpandableListAdapter;
import android.widget.TextView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.Calendar.CalendarEvent;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.FormsList.FormListData;
import com.procuro.androidscm.Restaurant.Schema.Days;
import com.procuro.androidscm.Restaurant.Schema.Forms;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsConfiguration;
import com.suke.widget.SwitchButton;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

public class StoreClosure_Expandable_listadapter extends BaseExpandableListAdapter implements ExpandableListAdapter {

    private Context context;
    private ArrayList<StoreClosureEvent> events;
    private TextView description;
    private android.app.AlertDialog tooltip;
    TextView start, end;
    CheckBox allday;

    public StoreClosure_Expandable_listadapter(Context context, ArrayList<StoreClosureEvent> events,
                                               TextView description,android.app.AlertDialog tooltip,
                                               TextView start,TextView end,CheckBox allday) {
        this.events = events;
        this.context = context;
        this.description = description;
        this.tooltip = tooltip;
        this.start = start;
        this.end = end;
        this.allday = allday;
    }

    @Override
    public int getGroupCount() {
        return events.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return events.get(groupPosition).getEvents().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return events.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return events.get(groupPosition).getEvents().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        StoreClosureEvent event = events.get(groupPosition);

        View view = LayoutInflater.from(context).inflate(R.layout.store_closure_description_list_group_data, parent, false);
        TextView textView = view.findViewById(R.id.name);

        try {
            textView.setText(event.getName());

        }catch (Exception e){
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final CalendarEvent calendarEvent = events.get(groupPosition).getEvents().get(childPosition);
        View view = LayoutInflater.from(context).inflate(R.layout.store_closure_description_list_data, parent, false);

        TextView name = view.findViewById(R.id.name);
        final TextView date = view.findViewById(R.id.date);
        final SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");

        try {
            name.setText(calendarEvent.getName());

            if (calendarEvent.getDate()!=null){
                date.setText(dateFormat.format(calendarEvent.getDate()));

            }else {
                date.setVisibility(View.GONE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HoursOfOpetationData data = HoursOfOpetationData.getInstance();
                description.setText(calendarEvent.getName());

                if (calendarEvent.getDate()!=null){
                    calendarEvent.setAllday(true);
                    calendarEvent.setStartTime(setStartTime(calendarEvent.getDate()));
                    calendarEvent.setEndTime(setEndTime(calendarEvent.getDate()));
                    data.setNewEvent(calendarEvent);
                    allday.setChecked(true);

                }else {

                    calendarEvent.setDate(Calendar.getInstance().getTime());
                    calendarEvent.setStartTime(setStartTime(calendarEvent.getDate()));
                    calendarEvent.setEndTime(setEndTime(calendarEvent.getDate()));
                    calendarEvent.setAllday(false);

                    data.setNewEvent(calendarEvent);
                    start.setText("");
                    end.setText("");
                    allday.setChecked(false);
                }

                tooltip.dismiss();
            }
        });

        return view;
    }

    private Date  setStartTime(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR,0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.MILLISECOND,0);
        calendar.set(Calendar.AM_PM,Calendar.AM);

        return calendar.getTime();
    }

    private Date  setEndTime(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR,11);
        calendar.set(Calendar.MINUTE,59);
        calendar.set(Calendar.MILLISECOND,59);
        calendar.set(Calendar.AM_PM,Calendar.PM);
        return calendar.getTime();
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }



}
