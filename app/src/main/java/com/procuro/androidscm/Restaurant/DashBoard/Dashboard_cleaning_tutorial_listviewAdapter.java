package com.procuro.androidscm.Restaurant.DashBoard;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

import com.procuro.androidscm.R;
import com.procuro.apimmdatamanagerlib.DocumentDTO;

import java.util.ArrayList;


public class Dashboard_cleaning_tutorial_listviewAdapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<TutorialList> arraylist;
    private String titles;
    private FragmentActivity fragmentActivity;

    public Dashboard_cleaning_tutorial_listviewAdapter(Context context, ArrayList<TutorialList> arraylist,
                                                       String title, FragmentActivity fragmentActivity) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;
        this.titles = title;
        this.fragmentActivity = fragmentActivity;

    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return arraylist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.dashboard_cleaning_tutorial_list_data, null);
            view.setTag(holder);
            final TutorialList tutorial = arraylist.get(position);

            TextView id = view.findViewById(R.id.id);
            final TextView title = view.findViewById(R.id.title);

            id.setText(tutorial.getId());
            title.setText(tutorial.getTitle());

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    DisplaySiteListPopup(tutorial.getDocumentDTO());
                }
            });

        return view;
    }


    private void DisplaySiteListPopup(DocumentDTO dto){
        DialogFragment newFragment = Dashboard_PdfView_DialogFragment.newInstance(dto);
        assert fragmentActivity.getFragmentManager() != null;
        newFragment.show(fragmentActivity.getSupportFragmentManager(), "dialog");
    }

}

