package com.procuro.androidscm.Restaurant.Schema;


import com.procuro.apimmdatamanagerlib.PimmBaseObject;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsConfiguration;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class FormItems extends PimmBaseObject {
    public String itemName;
    public String type;
    public String title;
    public String subtitle;
    public String standard;
    public boolean spotcheck;
    public ArrayList<String> dayparts;
    public ArrayList<String>  days;
    public double standardMin;
    public double standardMax;
    public double controlMin;
    public double controlMax;
    public ArrayList<String>  actions;




    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("dayparts")){
            ArrayList<String>items = new ArrayList<>();
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    JSONArray array = (JSONArray) value;
                    try {
                        for (int i = 0; i <array.length() ; i++) {
                            items.add(array.getString(i));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            this.dayparts = items;
        }

        if (key.equalsIgnoreCase("days")){
            ArrayList<String>items = new ArrayList<>();
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    JSONArray array = (JSONArray) value;
                    try {
                        for (int i = 0; i <array.length() ; i++) {
                            items.add(array.getString(i));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            this.days = items;

        }

        if (key.equalsIgnoreCase("actions")){
            ArrayList<String>items = new ArrayList<>();
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    JSONArray array = (JSONArray) value;
                    try {
                        for (int i = 0; i <array.length() ; i++) {
                            items.add(array.getString(i));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            this.actions = items;
        }
    }


}

