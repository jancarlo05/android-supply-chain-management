package com.procuro.androidscm.Restaurant.AllStorePopup;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

import com.diegocarloslima.fgelv.lib.FloatingGroupExpandableListView;
import com.diegocarloslima.fgelv.lib.WrapperExpandableListAdapter;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.ReportsFragment.Reports_fragment;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.StoreListFragment.SiteList;
import com.procuro.androidscm.SCMDataManager;

import java.util.ArrayList;
import java.util.Objects;

public class Status_SiteList_DialogFragment extends DialogFragment {

    public static String fragments;
    private Status_StoreListDialogFragment_adapter adapters;
    private ExpandableListView expandableListView;
    private ArrayList<SiteList> storeParentLists = SCMDataManager.getInstance().getSiteLists();
    private WrapperExpandableListAdapter wrapperExpandableListAdapter;

    public static Status_SiteList_DialogFragment newInstance(String fragment) {
        fragments = fragment;
        return new Status_SiteList_DialogFragment();
    }
    @Override
        public int getTheme() {
        return R.style.slide_down_up;
        }

    @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.status_storelist_popup, container, false);

//                    int height = (int)(getResources().getDisplayMetrics().heightPixels*0.90);
//                    int width = 320;
//
        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.90);
        int height = (int)(getResources().getDisplayMetrics().heightPixels*0.60);


        Objects.requireNonNull(getDialog().getWindow()).setLayout(width, height);
        getDialog().setCanceledOnTouchOutside(true);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.slide_down_up;
        view.setFocusableInTouchMode(true);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        LinearLayout allstore = view.findViewById(R.id.allstore_container);
        LinearLayout search_container = view.findViewById(R.id.search_container);
        final EditText search = view.findViewById(R.id.search);
        expandableListView = view.findViewById(R.id.expandible_listview);
        search.setFocusableInTouchMode(true);
        search_container.setFocusableInTouchMode(true);
        allstore.setBackgroundResource(R.drawable.onpress_design);

        if (!fragments.equalsIgnoreCase("reports")){
            allstore.setVisibility(View.GONE);
        }else {
            allstore.setVisibility(View.VISIBLE);
        }

        adapters = new Status_StoreListDialogFragment_adapter(getContext(),storeParentLists,getActivity(),getDialog(),fragments);

         expandableListView.setAdapter(adapters);

         for (int i = 0; i < adapters.getGroupCount(); i++) {
            expandableListView.expandGroup(i);
         }

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                return true; // This way the expander cannot be collapsed
            }
        });

         allstore.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 SCMDataManager.getInstance().setReportLevel("corp");
                 SCMDataManager.getInstance().setReportsSelectedSite("All Store");
                 DisplayAllStore(Objects.requireNonNull(getActivity()));
             }
         });

        SearchFunctions(search);

        return view;

        }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    private void SearchFunctions(final EditText search){
        final ArrayList<SiteList> array_sort = new ArrayList<>();
        search.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int textlength = search.getText().length();
                array_sort.clear();
                for (int i = 0; i < storeParentLists.size(); i++) {
                    boolean isparentCreated = false;
                    SiteList parentList = storeParentLists.get(i);
                    ArrayList<CustomSite> storeLists = parentList.getCustomSites();
                    ArrayList<CustomSite> sortedStorelist = new ArrayList<>();
                    for (int j = 0; j < storeLists.size(); j++) {
                        if (textlength <= storeLists.get(j).getSite().sitename.length()) {
                            if (storeLists.get(j).getSite().sitename.toLowerCase().trim().contains(
                                    search.getText().toString().toLowerCase().trim())) {
                                if (!isparentCreated) {
                                    sortedStorelist.add(storeLists.get(j));
                                    array_sort.add(new SiteList(parentList.getProvince(), sortedStorelist));
                                    isparentCreated = true;
                                } else {
                                    sortedStorelist.add(storeLists.get(j));
                                }
                            }
                        }
                    }
                }
                adapters = new Status_StoreListDialogFragment_adapter(getContext(), array_sort,getActivity(),getDialog(),fragments);
                expandableListView.setAdapter(adapters);

                for (int i = 0; i < adapters.getGroupCount(); i++) {
                    expandableListView.expandGroup(i);
                }

                expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                    @Override
                    public boolean onGroupClick(ExpandableListView parent, View v,
                                                int groupPosition, long id) {
                        return true; // This way the expander cannot be collapsed
                    }
                });

            }
        });

    }

    private void DisplayAllStore(FragmentActivity fragmentActivity) {
            fragmentActivity.getSupportFragmentManager().beginTransaction()
                    .replace(R.id.resturant_fragment_container,
                            new Reports_fragment()).commit();
            dismiss();

    }


}
