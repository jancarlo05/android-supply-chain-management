package com.procuro.androidscm.Restaurant.ReportsFragment.Inspection;

import android.app.ProgressDialog;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.print.PrintAttributes;
import android.provider.DocumentsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.FormsList.FormListData;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.StoreProfileOptions.StoreProfileOptionsData;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.TeamAndContancts.ManagementTeam.ManagementTeamData;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsData;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsDateChildList;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsDateGrandChildList;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsInspectionType;
import com.procuro.androidscm.Restaurant.Schema.Dayparts;
import com.procuro.androidscm.Restaurant.Schema.Days;
import com.procuro.androidscm.Restaurant.Schema.FormItems;
import com.procuro.androidscm.Restaurant.Schema.Forms;
import com.procuro.androidscm.Restaurant.Schema.Schema;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.IFTA_ECM;
import com.procuro.apimmdatamanagerlib.PropertyValue;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.SiteSalesData;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmthriftservices.services.NamedServices;
import com.procuro.apimmthriftservices.services.auditForm.FSLDataActionItem;
import com.procuro.apimmthriftservices.services.auditForm.FSLDataAttachment;
import com.procuro.apimmthriftservices.services.auditForm.FSLDataFSLFormsClient;
import com.procuro.apimmthriftservices.services.auditForm.FSLDataForm;
import com.procuro.apimmthriftservices.services.auditForm.FSLDataItem;
import com.procuro.apimmthriftservices.services.auditForm.FSLDataNote;
import com.procuro.apimmthriftservices.services.auditForm.FSLDataReading;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsConfiguration;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsEquipment;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsSiteConfig;

import org.apache.thrift.TException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;
import java.util.TimeZone;
import java.util.regex.Pattern;


public class Reports_inspection_Load_Webview extends Fragment {

    public ProgressBar progressBar;
    public WebView webView;
    private TextView date , wait;

    private int id;
    private ReportsDateGrandChildList daily;
    private ReportsDateChildList weekly;
    private String Filefound;
    private File SelectedFile;

    public  static String SCM_FILE_FOLDER = ".SCMFiles";
    public  static String root = Environment.getExternalStorageDirectory().toString();
    private Site site = ReportsData.getInstance().getSite().getSite();

    public Reports_inspection_Load_Webview(int id , ReportsDateGrandChildList daily, ReportsDateChildList weekly) {
        // Required empty public constructor
        this.id = id;
        this.daily = daily;
        this.weekly = weekly;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.reports_right_pane_inspection_reports_daily_load_webview, container, false);

        webView = rootView.findViewById(R.id.webview);
        progressBar = rootView.findViewById(R.id.progressBar2);
        wait = rootView.findViewById(R.id.wait);

        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        settings.setSupportZoom(true);
        settings.supportZoom();
        settings.setAllowFileAccess(true);
        settings.setAllowFileAccessFromFileURLs(true);
        settings.setDisplayZoomControls(false);
        settings.setBuiltInZoomControls(true);
        settings.setAppCacheEnabled(true);
        settings.setAllowContentAccess(true);
        settings.setSupportZoom(true);
        settings.setDisplayZoomControls(true);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        webView.setWebViewClient(new mywebviewclient());
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);



        if (id == R.id.closing_opening){
            if (HasDailyData()){
                loadFileInExternalStore("wendys.html");
            }else {
                getFSLDataForm(daily.getDate());
            }
//            webView.loadUrl("file:///storage/emulated/0/SCMFiles/wendys.2.0.0/reports/wendys.html");

        }else if (id == R.id.food_safety){
            if (HasDailyData()){
                loadFileInExternalStore("wendys.html");
            }else {
                getFSLDataForm(daily.getDate());
            }
        }else if (id == R.id.dop){
            webView.setWebChromeClient(new Webcromeclient());
            webView.loadUrl("file:///android_asset/DOPTemp/dop.html");

        }else if ( id == R.id.rush_ready){
            if (HasDailyData()){
                loadFileInExternalStore("wendys.html");
            }else {
                getFSLDataForm(daily.getDate());
            }

        }else if ( id == R.id.breakfast_summary){
            if (HasWeeklyData()){
                loadFileInExternalStore("wendys.html");
            }else {
                GetItemOverDateRange(weekly.getStartdate(),weekly.getEnddate(),id);
            }
        }else if (id == R.id.food_transportation){
            loadFileInExternalStore("FoodTransportationWeeklyReport.html");

        } else if (id == R.id.puw){
            loadFileInExternalStore("wendys.html");

        }else if (id == R.id.managers_obs){
            if (HasWeeklyData()){
                loadFileInExternalStore("npo-grilledchicken.html");
            }else {
                GetItemOverDateRange(weekly.getStartdate(),weekly.getEnddate(),id);
            }
        }else if (id == R.id.breakfast_readiness){
            if (HasDailyData()){
                webView.setWebChromeClient(new Webcromeclient());
                webView.loadUrl("file:///storage/emulated/0/.SCMFiles/wendys.2.0.0/reports/CheckListBreakfast/index.html");
            }else {
                getFSLDataForm(daily.getDate());
            }
        }


        return rootView;
    }

    private boolean HasWeeklyData(){
        ReportsData data = ReportsData.getInstance();
        if (data.getOverall_items()!=null){
            if (data.getOverall_attachments()!=null){
                if (data.getOverll_notes()!=null){
                    return true;
                }else {
                    return false;
                }
            }else {
                return false;
            }
        }else {
            return false;
        }
    }

    private boolean HasDailyData(){
        ReportsData data = ReportsData.getInstance();
        if (data.getForm()!=null){
            if (data.getEquipment()!=null){
                return true;
            }else {
                return false;
            }
        }else {
            return false;
        }
    }

    private void FindSelectedDay(){

        if (id == R.id.food_safety) {
            GenerateFoodSafetyReport(daily);
        }
        else if (id == R.id.closing_opening){
            GenerateClosingOpeningReport(daily);
        }
        else if (id == R.id.dop){
            GenerateDOPReport(daily);
        }
        else if (id == R.id.rush_ready){
            GenerateRushReadyReport(daily);
        }
        else  if (id == R.id.breakfast_summary){
            GenerateBreakfastSummaryReport(daily,weekly);
        }
        else if (id == R.id.food_transportation){
            GenerateFoodTranspoReport(weekly);
        }
        else if (id == R.id.puw){
            GeneratePUWReport(weekly);
        }else if (id == R.id.managers_obs){
            GenerateManagerObsReport(weekly);
        }
        else if (id == R.id.breakfast_readiness){
            GenerateBreakFastReadiness(daily);
        }
    }

    // DAILY REPORTS

    private void GenerateFoodSafetyReport(ReportsDateGrandChildList daily){
        final SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE");
        String params = CreateFoodSafety_RushReady_And_ClosingOpeningParams(daily);
        StringBuilder sbldr = new StringBuilder();
        sbldr.append("loadReport(");
        sbldr.append(params);
        sbldr.append(",");
        sbldr.append("'"+dateFormat.format(daily.getDate()).toLowerCase()+"'");
        sbldr.append(")");
        System.out.println(sbldr);
        webView.evaluateJavascript(sbldr.toString(), null);

    }

    private void GenerateClosingOpeningReport(ReportsDateGrandChildList daily){
        String params = CreateFoodSafety_RushReady_And_ClosingOpeningParams(daily);
        StringBuilder sbldr = new StringBuilder();
        sbldr.append("loadReport(");
        sbldr.append(params);
        sbldr.append(",");
        sbldr.append("'cochecklist'");
        sbldr.append(")");
        System.out.println(sbldr);
        webView.evaluateJavascript(sbldr.toString(), null);

    }

    private String CreateFoodSafety_RushReady_And_ClosingOpeningParams(ReportsDateGrandChildList daily) {

        final SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, dd MMMM, yyyy");
        Site site = ReportsData.getInstance().getSite().getSite();
        User user = SCMDataManager.getInstance().getUser();
        ReportsData data = ReportsData.getInstance();

        String grilltype = "Config:grillType";
        String open24Hours = "Config:open24Hours";
        String drivethru = "Config:autofill:pickUpWin";

        String param = "";
        try {
            JSONObject dataJObj = new JSONObject();
            dataJObj.put("author",user.firstName+" "+user.lastName);
            dataJObj.put("leaderName", user.username);
            dataJObj.put("CCPEquipment", getCCPEQUIPMENT());
            dataJObj.put("open24hrs", getSitePropertyValueByName(open24Hours));
            dataJObj.put("date", dateFormat.format(daily.getDate()));
            dataJObj.put("grillType", getSitePropertyValueByName(grilltype));
            dataJObj.put("storeName", site.sitename);
            dataJObj.put("driveThru", getSitePropertyValueByName(drivethru));

            dataJObj.put("rnvcomments", getRNVComments());
            dataJObj.put("comments", getComments());
            dataJObj.put("items", getDailyItems());
            dataJObj.put("initials", getInitalItems());
            dataJObj.put("dayparts", getDayparts());

            JSONObject gmanager = new JSONObject();
            gmanager.put("username", "");
            gmanager.put("signature", "");
            dataJObj.put("gmmanager", gmanager);
            param = dataJObj.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return param;

    }

    private void GenerateDOPReport(ReportsDateGrandChildList daily){
        String params = CreateDop_And_RushReadyParams(daily);
        StringBuilder sbldr = new StringBuilder();
        sbldr.append("loadReport(");
        sbldr.append(params);
        sbldr.append(",");
        sbldr.append("'dop'");
        sbldr.append(")");
        System.out.println(sbldr);
        webView.evaluateJavascript(sbldr.toString(), null);

    }

    private void GenerateRushReadyReport(ReportsDateGrandChildList daily){
        String params = CreateFoodSafety_RushReady_And_ClosingOpeningParams(daily);
        StringBuilder sbldr = new StringBuilder();
        sbldr.append("loadReport(");
        sbldr.append(params);
        sbldr.append(",");
        sbldr.append("'rushreadychecklist'");
        sbldr.append(")");
        System.out.println(sbldr);
        webView.evaluateJavascript(sbldr.toString(), null);

    }

    private String CreateDop_And_RushReadyParams(ReportsDateGrandChildList daily) {
        String param = "";
        final SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, dd MMMM, yyyy");

        try {
            JSONObject dataJObj = new JSONObject();
            dataJObj.put("storeName", ReportsData.getInstance().getSite().getSite().sitename);
            dataJObj.put("date", dateFormat.format(daily.getDate()));
            dataJObj.put("title", "Daily Operations Plan");
            dataJObj.put("items", new JSONArray());
            dataJObj.put("logo", "wendys");
            param = dataJObj.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return param;
    }


    //WEEKLY REPORT


    private void GenerateBreakfastSummaryReport(final ReportsDateGrandChildList daily, final ReportsDateChildList weekly){

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {

                String params = CreateBreakfastParams(daily, weekly).replace("\n","");
                StringBuilder sbldr = new StringBuilder();
                sbldr.append("loadReport(");
                sbldr.append(params);
                sbldr.append(",");
                sbldr.append("'breakfast'");
                sbldr.append(")");
                System.out.println(sbldr);
                webView.evaluateJavascript(sbldr.toString(), null);
                
            }
        });

    }


    private String CreateBreakfastParams(ReportsDateGrandChildList daily , ReportsDateChildList weekly) {

        String param = "";
        final SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, dd MMMM, yyyy");

        SCMDataManager scmData = SCMDataManager.getInstance();
        User user = scmData.getUser();

        JSONObject WeeklyComments = getWeeklyComments();

        try {
            JSONObject dataJObj = new JSONObject();
            dataJObj.put("storeName", ReportsData.getInstance().getSite().getSite().sitename);
            dataJObj.put("author",user.getFullName());
            dataJObj.put("breakfastDays", breakfastDays(weekly.getDailyArrayList()));
            dataJObj.put("date", dateFormat.format(weekly.getEnddate()));
            dataJObj.put("items", getWeeklyItems());
            dataJObj.put("breakfastNotes", WeeklyComments.get("breakfastNotes"));
            dataJObj.put("productTempsNotes", WeeklyComments.get("productTempsNotes"));
            dataJObj.put("sanitationNotes", WeeklyComments.get("sanitationNotes"));
            dataJObj.put("equipmentTempsNotes", WeeklyComments.get("equipmentTempsNotes"));
            dataJObj.put("ccpNotes", WeeklyComments.get("ccpNotes"));
            dataJObj.put("breakfastInitialItem", getBreakfastInitialItems());
            dataJObj.put("breakfastOpsItem", getbreakfastOpsItems());
            dataJObj.put("leaderName", user.username);
            dataJObj.put("breakfastGMItem", getbreakfastGMItem());

            param = dataJObj.toString();


        } catch (JSONException e) {
            e.printStackTrace();
        }


        return param;

    }


    private void GenerateFoodTranspoReport(ReportsDateChildList weekly){
        String params = CreateFoodTranspo_And_PuwParams(weekly);
        StringBuilder sbldr = new StringBuilder();
        sbldr.append("loadReport(");
        sbldr.append(params);
        sbldr.append(")");
        System.out.println(sbldr);
        webView.evaluateJavascript(sbldr.toString(), null);

    }

    private void GeneratePUWReport(ReportsDateChildList weekly){
        String params = CreateFoodTranspo_And_PuwParams(weekly);
        StringBuilder sbldr = new StringBuilder();
        sbldr.append("loadReport(");
        sbldr.append(params);
        sbldr.append(",");
        sbldr.append("'dop'");
        sbldr.append(")");
        System.out.println(sbldr);
        webView.evaluateJavascript(sbldr.toString(), null);

    }

    private String CreateFoodTranspo_And_PuwParams(ReportsDateChildList weekly) {
        String param = "";
        final SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");
        final SimpleDateFormat breakfast_format = new SimpleDateFormat(" MM/dd/yy");

        try {
            JSONObject dataJObj = new JSONObject();
            JSONArray jdays = new JSONArray();
            ArrayList<ReportsDateGrandChildList>dailies = weekly.getDailyArrayList();
            for (int i = 1; i <dailies.size() ; i++) {
                ReportsDateGrandChildList daily1 =dailies.get(i);
                jdays.put(breakfast_format.format(daily1.getDate()));
            }
            dataJObj.put("spid", SCMDataManager.getInstance().getSPID());
            dataJObj.put("dayparts", new JSONArray());
            dataJObj.put("notes", "");
            dataJObj.put("date", dateFormat.format(weekly.getStartdate())+" - "+dateFormat.format(weekly.getEnddate()));
            JSONArray npoGMItems = new JSONArray();
            JSONObject items = new JSONObject();
            items.put("wed3_gm", new JSONArray());
            items.put("wed4_gm", new JSONArray());
            items.put("wed5_gm", new JSONArray());
            items.put("wed6_gm", new JSONArray());
            items.put("wed7_gm", new JSONArray());
            items.put("wed1_gm", new JSONArray());
            items.put("wed2_gm", new JSONArray());
            npoGMItems.put(items);
            dataJObj.put("npoGMItems", npoGMItems);
            dataJObj.put("storeName", ReportsData.getInstance().getSite().getSite().sitename);

            param = dataJObj.toString();


        } catch (JSONException e) {
            e.printStackTrace();
        }


        return param;

    }

    private void GenerateManagerObsReport(ReportsDateChildList weekly){
        String params = CreateManagerObsParams(weekly);
        StringBuilder sbldr = new StringBuilder();
        sbldr.append("loadReport(");
        sbldr.append(params);
        sbldr.append(",");
        sbldr.append("'moc'");
        sbldr.append(")");
        System.out.println(sbldr);
        webView.evaluateJavascript(sbldr.toString(), null);
    }

    private void GenerateBreakFastReadiness(ReportsDateGrandChildList daily){
        String params = GenerateBreakFastReadinessParams(daily);
        StringBuilder sbldr = new StringBuilder();
        sbldr.append("loadReport(");
        sbldr.append(params);
        sbldr.append(")");
        System.out.println(sbldr);
        webView.evaluateJavascript(sbldr.toString(), null);
    }

    private String GenerateBreakFastReadinessParams(ReportsDateGrandChildList daily) {
        String param = "";
        final SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, MMMM dd, yyyy");

        try {
            JSONObject dataJObj = new JSONObject();
            dataJObj.put("storeName", ReportsData.getInstance().getSite().getSite().sitename);
            dataJObj.put("spid", SCMDataManager.getInstance().getSPID());
            dataJObj.put("title", " Breakfast Readiness Checklist");
            dataJObj.put("items",getBreafastReadinessItems());
            dataJObj.put("date", dateFormat.format(daily.getDate()));
            param = dataJObj.toString();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return param;
    }

    private JSONArray getBreafastReadinessItems(){
        ReportsData reportsData = ReportsData.getInstance();
        JSONArray jsonArray = new JSONArray();
        JSONObject BRCObject = new JSONObject();
        JSONObject LunchObject = new JSONObject();

        try {
            JSONArray BreakfastForms = new JSONArray();
            JSONArray LunchForms = new JSONArray();

            if (ReportsData.getInstance().getCurrentInspectionType()!=null){
                for (Days days : ReportsData.getInstance().getCurrentInspectionType().getChecklist()){
                    if (days.section.equalsIgnoreCase("Breakfast Readiness Checklist")){
                        if (days.getForms()!=null){
                            for (Forms forms : days.getForms()){
                                BreakfastForms.put(getBreafastReadinessItemForms(forms));
                            }
                        }

                    }else if (days.section.equalsIgnoreCase("Lunch Transition Checklist")){
                        if (days.getForms()!=null){
                            for (Forms forms : days.getForms()){
                                LunchForms.put(getBreafastReadinessItemForms(forms));
                            }
                        }
                    }
                }
            }

            BRCObject.put("title","Breakfast Readiness");
            BRCObject.put("forms",BreakfastForms);
            LunchObject.put("title","Lunch Transition");
            LunchObject.put("forms",LunchForms);

            jsonArray.put(BRCObject);
            jsonArray.put(LunchObject);

        }catch (Exception e){
            e.printStackTrace();
        }

        return  jsonArray;
    }
    private JSONObject BFRInitialItem(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type",0);
            jsonObject.put("description","Description");
        }catch (Exception e){
            e.printStackTrace();
        }
        return  jsonObject;
    }

    private JSONObject getBreafastReadinessItemForms(Forms forms){
        JSONObject jsonObject = new JSONObject();
        JSONArray formItems = new JSONArray();
        formItems.put(BFRInitialItem());
        try {
            jsonObject.put("title",forms.title);
            if (forms.items!=null){
                for (FormItems formItem : forms.items){
                    formItems.put(getBreafastReadinessFormsItems(formItem));
                }
            }
            jsonObject.put("items",formItems);
        }catch (Exception e){
            e.printStackTrace();
        }
        return jsonObject;
    }
    private JSONObject getBreafastReadinessFormsItems(FormItems formItem){
        ReportsData reportsData = ReportsData.getInstance();
        JSONObject jsonObject = new JSONObject();
        SimpleDateFormat format = new SimpleDateFormat("M/d/yy, h:mm a");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            jsonObject.put("inputType",formItem.type);
            jsonObject.put("subDescriptions",new JSONArray());
            jsonObject.put("description",formItem.title);
            jsonObject.put("type",1);

            if (ReportsData.getInstance().getForm()!=null){
                ArrayList<FSLDataItem>items = ReportsData.getInstance().getForm().items;
                if (items!=null){
                    for (FSLDataItem item : items){
                        if (item.itemName.contains(formItem.itemName)){
                            jsonObject.put("itemName",item.itemName);
                            if (item.readings!=null){
                                ArrayList<FSLDataReading>readings = item.readings;
                                if (readings!=null){
                                    for (FSLDataReading reading : readings){
                                        if (item.itemName.contains(reading.itemName)){
                                            if (reading.dateTimeStamp!=0){
                                                Calendar date = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                                                date.setTime(new Date(reading.dateTimeStamp));
                                                date.add(Calendar.MINUTE,site.effectiveUTCOffset);
                                                jsonObject.put("lastStatus",format.format(date.getTime()));
                                                System.out.println("TIME STAMP : "+item.itemName + " DATE : "+reading.dateTimeStamp);
                                            }
                                            if (reading.textReading.toLowerCase().contains("text")) {
                                                String[] text = reading.textReading.split("_");
                                                jsonObject.put("textReading", text[1]);
                                            }
                                            else {
                                                jsonObject.put("textReading", getReading2(reading));
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                            break;
                        }
                    }
                }
            }


        }catch (Exception e){
            e.printStackTrace();
        }
        return  jsonObject;
    }

    private String CreateManagerObsParams(ReportsDateChildList weekly) {
        String param = "";
        final SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, MMMM dd, yyyy");

        try {
            JSONObject dataJObj = new JSONObject();
            dataJObj.put("storeName", ReportsData.getInstance().getSite().getSite().sitename);
            dataJObj.put("spid", SCMDataManager.getInstance().getSPID());

            JSONArray signatures = new JSONArray();
            JSONObject signature = new JSONObject();
            for (int i = 0; i <7 ; i++) {
                signature.put("name","");
                signature.put("signb64","");
            }
            signatures.put(signature);
            dataJObj.put("signatures", getWeeklySignatures(weekly.getDailyArrayList()));
            dataJObj.put("week", getWeeks(weekly.getDailyArrayList()));

            dataJObj.put("inspectionTypes", getinspectionTypes());
            dataJObj.put("date", dateFormat.format(weekly.getStartdate())+" - "+dateFormat.format(weekly.getEnddate()));
            param = dataJObj.toString();


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return param;
    }


    private class Webcromeclient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                progressBar.setProgress(newProgress, true);
            }else {
                progressBar.setProgressTintList(ColorStateList.valueOf(Color.parseColor("#F68725")));
                progressBar.setProgress(newProgress);
            }
            if(newProgress == 100) {
                wait.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);

            }
        }
    }

    private class mywebviewclient extends WebViewClient{

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            FindSelectedDay();
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            System.out.println("ERROR"+error.getDescription());
            System.out.println("ERROR"+error.toString());
        }
    }



    public  void listData(File dir,String filename) {
        for (File file: dir.listFiles()) {
            if (file.isDirectory()){
                listData(file,filename);
            }else {
                if (file.getName().equalsIgnoreCase(filename)){
                    Filefound = "file://"+file.getAbsolutePath();
                    System.out.println("FILE PATH: "+Filefound);
                    SelectedFile = file;
                    break;
                }
            }
        }
    }

    public  void listData(File dir,String filename,String parent) {
        System.out.println("PARENT SEARCHING STARTED : ");
        for (File file: dir.listFiles()) {
            if (file.isDirectory()){
                if (file.getName().equalsIgnoreCase(parent)){
                    System.out.println("FILE PARENT FOUND : "+file.getName());
                    listData(file,filename);

                    break;
                }else {
                    listData(file,filename);
                }
            }else {
                if (file.getParentFile()!=null){
                    if (file.getName().equalsIgnoreCase(filename) && file.getParentFile().getName().equalsIgnoreCase(parent)){
                        if (file.getParentFile()!=null){
                            Filefound = "file://"+file.getAbsolutePath();
                            System.out.println("FILE PATH: "+Filefound);
                            SelectedFile = file;
                            break;
                        }
                    }
                }
            }
        }
    }

    public  void loadFileInExternalStore(final String filename){
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                webView.setWebChromeClient(new Webcromeclient());
                File destDir = new File(root, SCM_FILE_FOLDER);
                if(destDir.exists()){
                    listData(destDir,filename);
                    webView.loadUrl(Filefound);
                }
            }
        });

    }

    public  void loadFileInExternalStore(final String filename, final String Parent){
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                webView.setWebChromeClient(new Webcromeclient());
                File destDir = new File(root, SCM_FILE_FOLDER);
                if(destDir.exists()){
                    listData(destDir,filename,Parent);
                    webView.loadUrl(Filefound);
                }
            }
        });

    }

    private String getRNVComments(){
        StringBuilder comment = new StringBuilder();
        ReportsData data = ReportsData.getInstance();
        ArrayList<String> strings = new ArrayList<>();
        if (data.getForm().notes!=null){
            ArrayList<FSLDataNote>notes = data.getForm().notes;
            for (FSLDataNote note : notes){
                if (note.formName.equalsIgnoreCase("FSLVerification_99")){
                   strings.add("EOD : "+note.noteText);
                }
                else if (note.formName.equalsIgnoreCase("FSLVerification_0")){
                    strings.add(0,"Daypart 1 : "+note.noteText);
                }
                else if (note.formName.equalsIgnoreCase("FSLVerification_1")){
                    strings.add(1,"Daypart 2 : "+note.noteText);
                }
                else if (note.formName.equalsIgnoreCase("FSLVerification_2")){
                    strings.add(2,"Daypart 3 : "+note.noteText);
                }
                else if (note.formName.equalsIgnoreCase("FSLVerification_3")){
                    strings.add(3,"Daypart 4 : "+note.noteText);
                }
                else if (note.formName.equalsIgnoreCase("FSLVerification_4")){
                    strings.add(4,"Daypart 5 : "+note.noteText);
                }
                else if (note.formName.equalsIgnoreCase("FSLVerification_5")){
                    strings.add(5,"Daypart 6 : "+note.noteText);
                }
            }
        }


        for (int j = 0; j <strings.size() ; j++) {
            if (j== strings.size()-1){
                comment.append(strings.get(j));
            }else {
                comment.append(strings.get(j));
                comment.append("; ");
            }
        }

      return  comment.toString();
    }

    private JSONArray getComments(){
        JSONArray jsonArray = new JSONArray();
        ReportsData data = ReportsData.getInstance();
        ArrayList notes = data.getForm().notes;

        ArrayList<FSLDataNote>ccps_note = new ArrayList<>();
        ArrayList<FSLDataNote>sanitation_notes = new ArrayList<>();
        ArrayList<FSLDataNote>grillCookers_note = new ArrayList<>();
        ArrayList<FSLDataNote>hotHolds_note = new ArrayList<>();

        if (notes!=null){
            for (Object object : notes){
                FSLDataNote note = (FSLDataNote) object;
                if (note.formName.toLowerCase().contains("ccp")){
                    ccps_note.add(note);
                    System.out.println("CCP: "+note.noteText);
                }
                else if (note.formName.toLowerCase().contains("sanitation")){
                    sanitation_notes.add(note);
                    System.out.println("sanitation: "+note.noteText);
                }
                else if (note.formName.toLowerCase().contains("grillcookers") || note.formName.toLowerCase().contains("coolersfreezers") ){
                    grillCookers_note.add(note);
                    System.out.println("grillcookers: "+note.noteText);
                }
                else if (note.formName.toLowerCase().contains("hothold") || note.formName.toLowerCase().contains("coldhold")){
                    hotHolds_note.add(note);
                    System.out.println("hotHold: "+note.noteText);
                }
            }
        }

        jsonArray.put(createNotesBytitle(ccps_note,"ccps_note"));
        jsonArray.put(createNotesBytitle(sanitation_notes,"sanitation_notes"));
        jsonArray.put(createNotesBytitle(grillCookers_note,"grillCookers_note"));
        jsonArray.put(createNotesBytitle(hotHolds_note,"hotHolds_note"));

        return jsonArray;
    }

    private JSONArray getDailyItems(){
        JSONArray jsonArray = new JSONArray();
        ReportsData data = ReportsData.getInstance();
        ArrayList items = data.getForm().items;
        ArrayList<SiteSettingsEquipment> enabledEquipments = data.getEnabledEquipments();
        SimpleDateFormat format = new SimpleDateFormat("M/d/yy, h:mm a");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        String degree= "&deg;F";

        try {
            if (items!=null){
                System.out.println("------- items ->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                for (Object itemObject : items){
                    FSLDataItem item  = (FSLDataItem) itemObject;
                    if (item.readings!=null){
                        for (Object object : item.readings){
                            FSLDataReading reading = (FSLDataReading) object;
                            JSONObject jsonObject = new JSONObject();

                            if (!reading.itemName.equalsIgnoreCase("inspection")){
                                CreateReading(reading,jsonObject);
                                jsonArray.put(jsonObject);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonArray;
    }


    private void CreateReading(FSLDataReading reading, JSONObject jsonObject){
        try {
            SimpleDateFormat format = new SimpleDateFormat("M/d/yy, h:mm a");
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = new Date();
            date.setTime(reading.dateTimeStamp);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            calendar.setTime(date);
            calendar.add(Calendar.MINUTE, site.effectiveUTCOffset);

            jsonObject.put("itemName", reading.itemName);
            jsonObject.put("dateTaken", format.format(calendar.getTime()));


            if (reading.textReading.toLowerCase().contains("text")) {
                String[] text = reading.textReading.split("_");
                jsonObject.put("textReading", text[0] + "," + text[1]);
                jsonObject.put("reading", text[1]);
            } else {
                jsonObject.put("textReading", reading.textReading);
                jsonObject.put("reading", getReading(reading));
            }

            if (reading.actions != null) {
                if (reading.actions.size() > 0) {
                    jsonObject.put("correctiveAction", getActions(reading.actions));
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private String getReading(FSLDataReading reading){
        String degree= "&deg;F";
        try {

            if (reading.textReading.toLowerCase().contains("verify")){
                String[]text = reading.textReading.split("_");
                return text[1];
            }
            else if (reading.textReading.toLowerCase().contains("temperature")){
                double roundOff = Math.round(reading.numericReading * 10) / 10.0;
                return roundOff+degree;
            }
            else if (reading.textReading.toLowerCase().contains("date")){
                String[]dates = reading.textReading.split("_");
                return dates[1];

            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return +reading.numericReading+degree;
    }

    private String getReading2(FSLDataReading reading){
        String degree= "&deg;F";
        try {

            if (reading.textReading.toLowerCase().contains("verify")){
                String[]text = reading.textReading.split("_");
                if (text[1].toLowerCase().contains("y")){
                    return "Yes";
                }else {
                    return "No";
                }

            }
            else if (reading.textReading.toLowerCase().contains("temperature")){
                double roundOff = Math.round(reading.numericReading * 10) / 10.0;
                return roundOff+degree;
            }
            else if (reading.textReading.toLowerCase().contains("date")){
                String[]dates = reading.textReading.split("_");
                return dates[1];

            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return +reading.numericReading+degree;
    }


    private String getActions (ArrayList arrayList){
        int counter = 0;
        StringBuilder actions = new StringBuilder();
        for (Object action: arrayList){
            FSLDataActionItem actionItem = (FSLDataActionItem) action;
            if (counter==0){
                counter++;
                actions.append(actionItem.actionText);
            }else {
                actions.append(", ");
                actions.append(actionItem.actionText);
            }
        }

        return actions.toString();
    }

    private JSONArray getDayparts(){
        JSONArray jsonArray = new JSONArray();
        ReportsData data = ReportsData.getInstance();
        ArrayList<Dayparts>dayparts = Schema.getInstance().getDayparts();

        for (Dayparts daypart : dayparts){
            JSONObject jsonObject = new JSONObject();
            String signature = "";
            String username = "";
            try {
                if (data.getForm().attachments!=null){
                    for (Object  object : data.getForm().attachments){
                        FSLDataAttachment attachment = (FSLDataAttachment) object;

                        String[]username_arry = attachment.attachmentName.split("_");
                        int Daypart = 0;
                        try {
                            Daypart = Integer.parseInt(username_arry[username_arry.length-1]);
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        if (daypart.name.equalsIgnoreCase(String.valueOf(Daypart+1))){
                            if (attachment.attachmentBinary!=null){
                                signature = new String(attachment.attachmentBinary);
                                username = username_arry[0];

                            }
                        }
                    }
                }

                jsonObject.put("username",username);
                jsonObject.put("signature",signature);
                jsonObject.put("dpName",daypart.title);

                jsonArray.put(jsonObject);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jsonArray;
    }

    private JSONArray getInitalItems(){
        JSONArray jsonArray = new JSONArray();
        ReportsData data = ReportsData.getInstance();
        ArrayList items = data.getForm().items;
        SimpleDateFormat format = new SimpleDateFormat("M/d/yy, h:mm a");
        String degree= "&deg;F";
        try {
            if (items!=null){
                for (Object itemObject : items){
                    FSLDataItem item  = (FSLDataItem) itemObject;
                    if (item.readings!=null){
                        for (Object object : item.readings){
                            FSLDataReading reading = (FSLDataReading) object;
                            JSONObject jsonObject = new JSONObject();

                            if (reading.itemName.equalsIgnoreCase("inspection")){
                                String[] initialValue = reading.textReading.split(Pattern.quote("$"));
                                String[] key = initialValue[0].split("_");

                                String initialKey = "dp" +
                                        key[key.length - 1] +
                                        "$" +
                                        key[0] +
                                        "_inspection";

                                jsonObject.put("initialValue",initialValue[1]);
                                jsonObject.put("initialKey", initialKey);

                                jsonArray.put(jsonObject);
                            }
                        }
                    }
                }
            }



        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    private JSONObject createNotesBytitle(ArrayList<FSLDataNote> notes,String title){
        JSONObject jsonObject = new JSONObject();
        StringBuilder stringBuilder = new StringBuilder();

        for (int j = 0; j <notes.size() ; j++) {
            FSLDataNote note = notes.get(j);
            if (j==notes.size()-1){
                stringBuilder.append(note.noteText);
            }else {
                stringBuilder.append(note.noteText);
                stringBuilder.append("; ") ;
            }
        }
        try {
            jsonObject.put("commentValue",stringBuilder);
            jsonObject.put("commentKey",title);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    private String getSitePropertyValueByName(String key){
        String value = "";
        Site site = ReportsData.getInstance().getSite().getSite();

        if (site.PropertiesList!=null){
            for (PropertyValue propertyValue : site.PropertiesList){
                if (propertyValue.property.equalsIgnoreCase(key)){
                    return propertyValue.value;
                }
            }
        }

        return value;
    }

    private String getCCPEQUIPMENT(){

        StringBuilder stringBuilder = new StringBuilder();
        ReportsData data = ReportsData.getInstance();
        int counter = 0;
        if (data.getEquipment()!=null){
            for (SiteSettingsEquipment equipment : data.getEquipment()){
                if (equipment.equipmentName.toLowerCase().contains("dsg") || equipment.equipmentName.contains("grill")){
                    if (equipment.inUse){
                        if (counter==0){
                            counter++;
                            stringBuilder.append(equipment.equipmentName);
                        }else {
                            stringBuilder.append(", ").append(equipment.equipmentName);
                        }
                    }
                }
            }
        }
        return stringBuilder.toString();
    }

    private JSONArray breakfastDays(ArrayList<ReportsDateGrandChildList>dalies){
        final SimpleDateFormat breakfast_format = new SimpleDateFormat(" MM/dd/yy");
        JSONArray jsonArray = new JSONArray();
        if (dalies!=null) {
            for (int j = 1; j < dalies.size(); j++) {
                ReportsDateGrandChildList daily = dalies.get(j);
                jsonArray.put(breakfast_format.format(daily.getDate()));

            }
        }
        return jsonArray;
    }

    private JSONArray getWeeks(ArrayList<ReportsDateGrandChildList>dalies){
        final SimpleDateFormat dateFormat = new SimpleDateFormat(" MM/dd/yy");
        final SimpleDateFormat dayformat = new SimpleDateFormat("EEEE");
        JSONArray jsonArray = new JSONArray();
        try {
            if (dalies!=null) {
                for (int j = 1; j < dalies.size(); j++) {
                    ReportsDateGrandChildList daily = dalies.get(j);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("weekDay",dayformat.format(daily.getDate()));
                    jsonObject.put("date",dateFormat.format(daily.getDate()));
                    jsonArray.put(jsonObject);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return jsonArray;
    }

    private JSONObject getWeeklyComments(){
        JSONObject jsonObject = new JSONObject();
        ReportsData data = ReportsData.getInstance();
        ArrayList notes = data.getOverll_notes();
        ArrayList<FSLDataNote>breakfastNotes = new ArrayList<>();
        ArrayList<FSLDataNote>ccpNotes = new ArrayList<>();
        ArrayList<FSLDataNote>sanitationNotes = new ArrayList<>();
        ArrayList<FSLDataNote>equipmentTempsNotes = new ArrayList<>();
        ArrayList<FSLDataNote>productTempsNotes = new ArrayList<>();

        try {

            if (notes!=null){
                for (Object object : notes){
                    FSLDataNote note = (FSLDataNote) object;
                    if (note.formName.toLowerCase().contains("ccp")) {
                        ccpNotes.add(note);
                    }
                    else if (note.formName.toLowerCase().contains("sanitation")){
                        sanitationNotes.add(note);

                    }
                    else if (note.formName.toLowerCase().contains("grillcookers") || note.formName.toLowerCase().contains("coolersfreezers") ){
                        equipmentTempsNotes.add(note);

                    }
                    else if (note.formName.toLowerCase().contains("hothold") || note.formName.toLowerCase().contains("coldhold")){
                        productTempsNotes.add(note);

                    }
                    else if (note.formName.equalsIgnoreCase("breakfast")){
                        breakfastNotes.add(note);
                    }
                }
            }

            jsonObject.put("breakfastNotes", CreateWeeklyNotes(breakfastNotes));
            jsonObject.put("ccpNotes", CreateWeeklyNotes(ccpNotes));
            jsonObject.put("sanitationNotes", CreateWeeklyNotes(sanitationNotes));
            jsonObject.put("equipmentTempsNotes", CreateWeeklyNotes(equipmentTempsNotes));
            jsonObject.put("productTempsNotes", CreateWeeklyNotes(productTempsNotes));

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jsonObject;

    }

    private JSONArray getWeeklySignatures(ArrayList<ReportsDateGrandChildList>dates){
        JSONArray jsonArray = new JSONArray();
        ReportsData data = ReportsData.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("EEE M/d/yy");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        try {
            for (int i = 1; i <dates.size() ; i++) {
                ReportsDateGrandChildList date =dates.get(i);
                JSONObject jsonObject = new JSONObject();

                String name = "";
                String signb64 = "";

                if (data.getOverall_attachments()!=null){
                    for (Object object : data.getOverall_attachments()){
                        FSLDataAttachment attachment = (FSLDataAttachment) object;
                        String[]attachmentname = attachment.attachmentName.split("_");
                        if (format.format(date.getDate()).equalsIgnoreCase(format.format(new Date(attachment.timestamp *1000)))){
                            if (attachmentname.length>0){
                                name = attachmentname[0];
                            }

                            if (attachment.attachmentBinary!=null){
                                signb64 = new String(attachment.attachmentBinary);
                            }
                        }

                    }
                }
                jsonObject.put("name",name);
                jsonObject.put("signb64",signb64);
                jsonArray.put(jsonObject);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return jsonArray;
    }

    private JSONArray CreateWeeklyNotes(ArrayList arrayList){
        final SimpleDateFormat dateFormat = new SimpleDateFormat(" M/d/yy");
        JSONArray jsonArray = new JSONArray();
        ArrayList<String>dates = new ArrayList<>();
        if (arrayList!=null){
            for (Object object : arrayList){
                FSLDataNote note = (FSLDataNote) object;
                Date newDate = new Date(note.timestamp);
                String date = dateFormat.format(newDate);
                if (!dates.contains(date)){
                    dates.add(date);
                }
            }
        }

        try {
            for (String selectedDate : dates){
                JSONObject jsonObject = new JSONObject();
                StringBuilder stringnotes = new StringBuilder();

                if (arrayList!=null){
                    for (Object object : arrayList) {
                        FSLDataNote note = (FSLDataNote) object;
                        Date newDate = new Date(note.timestamp);
                        String date = dateFormat.format(newDate);
                        if (date.equalsIgnoreCase(selectedDate)) {
                            stringnotes.append(note.noteText);
                            stringnotes.append("; ");
                        }
                    }
                }
                jsonObject.put("date",selectedDate);
                jsonObject.put("note",stringnotes);
                jsonArray.put(jsonObject);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return jsonArray;
    }

    private JSONArray getWeeklyItems(){
        JSONArray jsonArray = new JSONArray();
        ReportsData data = ReportsData.getInstance();
        ArrayList items = data.getOverall_items();

        Site site = ReportsData.getInstance().getSite().getSite();
        SimpleDateFormat format = new SimpleDateFormat("M/d/yy, h:mm a");
        SimpleDateFormat dayformat = new SimpleDateFormat("EEEE");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        dayformat.setTimeZone(TimeZone.getTimeZone("UTC"));

        String degree= "&deg;F";

        try {
            if (items!=null){

                for (Object itemObject : items){
                    FSLDataItem item  = (FSLDataItem) itemObject;
                    if (item.readings!=null){

                        for (Object object : item.readings){
                            FSLDataReading reading = (FSLDataReading) object;
                            JSONObject jsonObject = new JSONObject();

                            if (!reading.itemName.equalsIgnoreCase("inspection")){
                                if (!reading.itemName.equalsIgnoreCase("OpsLeaderAcknowledgement")){

                                    Calendar calendar = Calendar.getInstance();
                                    calendar.setTime(new Date(reading.dateTimeStamp));
                                    calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
                                    calendar.add(Calendar.MINUTE,site.effectiveUTCOffset);

                                    Schema schema = Schema.getInstance();
                                    Calendar currentDate = Calendar.getInstance();
                                    currentDate.setTime(calendar.getTime());
                                    currentDate.setTimeZone(TimeZone.getTimeZone("UTC"));

                                    Dayparts daypart = schema.getDayparts().get(0);
                                    Calendar start = SCMTool.setCalendarDaypart(currentDate.getTime(),daypart.start);


                                    if (currentDate.getTime().compareTo(start.getTime())<0) {
                                        calendar.add(Calendar.DATE,-1);
                                    }


                                    String day = dayformat.format(calendar.getTime()).toLowerCase()+"_";
                                    String stringDate = format.format(calendar.getTime());

                                    System.out.println("CURRENT DATE : "+stringDate + " |  "+format.format(start.getTime()));

                                    jsonObject.put("itemName",day+reading.itemName);
                                    System.out.println("STRING DATE : "+stringDate + " |  "+reading.dateTimeStamp);
                                    jsonObject.put("dateTaken",stringDate);


                                    if (reading.textReading.toLowerCase().contains("text")){
                                        String[]text = reading.textReading.split("_");
                                        jsonObject.put("textReading",text[0]+","+text[1]);
                                        jsonObject.put("reading",text[1]);
                                    }
                                    else {
                                        jsonObject.put("textReading",reading.textReading);
                                        jsonObject.put("reading",getReading(reading));
                                    }

                                    if (reading.actions!=null){
                                        if (reading.actions.size()>0){
                                            jsonObject.put("correctiveAction",getActions(reading.actions));
                                        }
                                    }

                                    jsonArray.put(jsonObject);
                                }
                            }
                        }
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println("JSON ARRY: "+jsonArray.toString().replaceAll("\\\\",""));

        return jsonArray;
    }

    private JSONArray getBreakfastInitialItems(){
        JSONArray jsonArray = new JSONArray();
        ReportsData data = ReportsData.getInstance();
        ArrayList items = data.getOverall_items();
        SimpleDateFormat format = new SimpleDateFormat("EEEE");
        String degree= "&deg;F";

        ArrayList<String>categories = new ArrayList<>();
        try {
            if (items!=null){
                for (Object itemObject : items){
                    FSLDataItem item  = (FSLDataItem) itemObject;
                    if (item.readings!=null){
                        for (Object object : item.readings){
                            FSLDataReading reading = (FSLDataReading) object;
                            JSONObject jsonObject = new JSONObject();

                            if (reading.itemName.equalsIgnoreCase("inspection")){

                                if (reading.textReading.contains("breakfast")){
                                    String[] opsLeader = reading.textReading.split(Pattern.quote("$"));

                                    Date date = getDateWithDaypart(reading.dateTimeStamp);
                                    String itemName = format.format(date).toLowerCase()+"_inspection";
                                    System.out.println("getBreakfastInitialItems >>> " +reading.description());
                                    jsonObject.put("initial",opsLeader[1]);
                                    jsonObject.put("itemName", itemName);
                                    jsonArray.put(jsonObject);
                                }
                            }
                        }
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonArray;
    }

    private JSONArray getbreakfastOpsItems(){
        JSONArray jsonArray = new JSONArray();
        ReportsData data = ReportsData.getInstance();
        ArrayList items = data.getOverall_items();
        SimpleDateFormat format = new SimpleDateFormat("EEEE");
        String degree= "&deg;F";
        try {
            if (items!=null){
                for (Object itemObject : items){
                    FSLDataItem item  = (FSLDataItem) itemObject;
                    if (item.readings!=null){
                        for (Object object : item.readings){
                            FSLDataReading reading = (FSLDataReading) object;
                            JSONObject jsonObject = new JSONObject();

                            if (reading.itemName.equalsIgnoreCase("OpsLeaderAcknowledgement" )){
                                if (reading.textReading.contains("breakfast")|| reading.textReading.contains("Unverified")){
                                    String[] opsLeader = reading.textReading.split(Pattern.quote(":"));
                                    Date date = new Date(reading.dateTimeStamp);
                                    String itemName = format.format(date).toLowerCase()+"_ack";
//                                    System.out.println("getbreakfastOpsItems >>>: " +reading.textReading);
                                    jsonObject.put("opsLeader",opsLeader[1].replace("Category",""));
                                    jsonObject.put("itemName", itemName);
                                    jsonArray.put(jsonObject);
                                }
                            }
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonArray;
    }

    private JSONArray getbreakfastGMItem(){
        JSONArray jsonArray = new JSONArray();
        ReportsData data = ReportsData.getInstance();
        ArrayList items = data.getOverall_attachments();
        SimpleDateFormat format = new SimpleDateFormat("EEEE");
        try {
            if (items!=null){
                for (Object itemObject : items){
                    FSLDataAttachment attachment  = (FSLDataAttachment) itemObject;
                    String signature = "";

                    JSONObject jsonObject = new JSONObject();

                    if (attachment.attachmentName.contains("GMgr")){
                        String[] managerName = attachment.attachmentName.split(Pattern.quote("_"));
                        Date date = new Date(attachment.timestamp);
                        String itemName = format.format(date).toLowerCase()+"_GM";

                        if (attachment.attachmentBinary!=null){
                            signature = new String(attachment.attachmentBinary);
                        }

                        jsonObject.put("managerName",managerName[0]);
                        jsonObject.put("managerSignature",signature);
                        jsonObject.put("itemName", itemName);

                        jsonArray.put(jsonObject);
                    }

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonArray;
    }

    //DOWNLOAD DATA

    private void GetItemOverDateRange(final Date startDate, final Date endDate, final int id){

        final Site site = ReportsData.getInstance().getSite().getSite();

        Log.i("Thrift ","GetItemOverDateRange start");
        try {
            final NamedServices ns = NamedServices.getInstance();
            if (ns.pimmServer!=null){

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Calendar start = Calendar.getInstance();
                            start.setTimeZone(TimeZone.getTimeZone("UTC"));
                            start.setTime(startDate);
                            start.set(Calendar.HOUR,0);
                            start.set(Calendar.MINUTE,1);
                            start.set(Calendar.AM_PM,Calendar.AM);
                            start.clear(Calendar.SECOND);
                            start.clear(Calendar.MILLISECOND);

                            Calendar end = Calendar.getInstance();
                            end.setTimeZone(TimeZone.getTimeZone("UTC"));
                            end.setTime(endDate);
                            end.set(Calendar.HOUR,0);
                            end.set(Calendar.MINUTE,1);
                            end.set(Calendar.AM_PM,Calendar.AM);
                            end.clear(Calendar.SECOND);
                            end.clear(Calendar.MILLISECOND);


                            FSLDataFSLFormsClient fslForms = ns.fslFormsClient;

                            long startTimestamp = Long.parseLong(String.valueOf(start.getTime().getTime()));
                            long endTimestamp = Long.parseLong(String.valueOf(end.getTime().getTime()));

                            System.out.println("Site ID: "+site.siteid);
                            System.out.println("CUSTOMER ID: "+site.CustomerId);
                            System.out.println("start  : "+startTimestamp);
                            System.out.println("end : "+endTimestamp);

                            SimpleDateFormat format = new SimpleDateFormat("EEEE, MMMM d, yyyy hh:mm a");
                            format.setTimeZone(TimeZone.getTimeZone("UTC"));

                            ArrayList overallitems = new ArrayList();
                            overallitems.addAll(getBreakfastItems(site,fslForms,startTimestamp,endTimestamp));

                            ArrayList attachments = new ArrayList();
                            attachments.addAll(fslForms.GetAttachmentsOverDateRange(site.CustomerId, site.siteid,
                                    startTimestamp,endTimestamp));

                            ArrayList notes = new ArrayList();
                            notes.addAll( fslForms.GetNotesOverDateRange(site.CustomerId, site.siteid,
                                    startTimestamp,endTimestamp));

                            ReportsData reportsData = ReportsData.getInstance();
                            reportsData.setOverall_attachments(attachments);
                            reportsData.setOverll_notes(notes);
                            reportsData.setOverall_items(overallitems);

                            if (attachments!=null){
                                for (Object object : attachments){
                                    FSLDataAttachment attachment = (FSLDataAttachment) object;{
                                        Date date = new Date(attachment.timestamp);
                                        date.setTime(attachment.timestamp);
                                        System.out.println(format.format(date));
                                    }
                                }
                            }

                            if (id ==R.id.breakfast_summary ){
                                loadFileInExternalStore("wendys.html");
                            }
                            else if (id == R.id.managers_obs){
                                loadFileInExternalStore("npo-grilledchicken.html");
                            }
                        } catch (TException e) {
                            e.printStackTrace();

                        }
                    }
                }).start();

            }else {
                Log.i("Thrift ","SERVER NULL ");
            }
            Log.i("Thrift ","GetItemOverDateRange End");
        }catch (TException e){
            e.printStackTrace();

        }

    }
    private JSONArray  getinspectionTypes() {
        JSONArray jsonArray = new JSONArray();
        ReportsData reportsData = ReportsData.getInstance();


        try {
            ArrayList<String>foodsafety = new ArrayList<>();
            ArrayList<String>checklist = new ArrayList<>();

            for (ReportsInspectionType inspectionType : reportsData.getReportsInspectionTypes()){
                for (Days days : inspectionType.getFoodsafety()){
                    if (!foodsafety.contains(days.section)){
                        foodsafety.add(days.section);
                    }
                }

                for (Days days : inspectionType.getChecklist()){
                    if (!checklist.contains(days.section)){
                        checklist.add(days.section);
                        System.out.println("CHECKLIST : "+days.section);
                    }
                }
            }
            Collections.sort(foodsafety);
            Collections.sort(checklist);

            jsonArray.put(CheckSchemaFormsByName(foodsafety,"Food Safety"));
            jsonArray.put(CheckSchemaFormsByName(checklist,"Checklist"));


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonArray;

    }

    private JSONObject CheckSchemaFormsByName(ArrayList<String>arrayList, String title)throws  JSONException{

        ReportsData reportsData = ReportsData.getInstance();

        JSONObject jsonObject = new JSONObject();

        SimpleDateFormat format = new SimpleDateFormat("EEE");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        if (title.equalsIgnoreCase("Food Safety")){

            jsonObject.put("title","Food Safety");
        }
        else {
            jsonObject.put("title","Checklist");
        }

        JSONArray jsonformarry = new JSONArray();

        for (String section : arrayList) {

            JSONObject jsonformObj = new JSONObject();
            JSONArray statuses = new JSONArray();

            for (ReportsInspectionType inspectionType : reportsData.getReportsInspectionTypes()) {

                ArrayList<Days> SchemeForms = new ArrayList<>();

                if (title.equalsIgnoreCase("Food Safety")) {
                    SchemeForms = inspectionType.getFoodsafety();
                } else {
                    SchemeForms = inspectionType.getChecklist();
                }

                ArrayList<Integer>finalStatus = new ArrayList<>();

                for (Days days : SchemeForms) {
                    if (days.section.equalsIgnoreCase(section)){
                        if (format.format(inspectionType.getDate()).equalsIgnoreCase(days.day)){
                            if (section.equalsIgnoreCase(days.section)) {
                                finalStatus.addAll(getStatus(days,section));
                            }
                        }
                    }
                }

                Collections.sort(finalStatus);


                if (finalStatus.size() > 0) {
                    statuses.put(finalStatus.get(0));
                } else {
                    statuses.put(-1);
                }
            }

            jsonformObj.put("title", section);
            jsonformObj.put("statuses", statuses);

            jsonformarry.put(jsonformObj);
        }


        jsonObject.put("forms",jsonformarry);

        return jsonObject;
    }

    private ArrayList<Integer> getStatus(Days days,String section){

        ArrayList<Integer>statusPattern = new ArrayList<>();
        ReportsData reportsData = ReportsData.getInstance();
        Site site = ReportsData.getInstance().getSite().getSite();

        SimpleDateFormat format = new SimpleDateFormat("EEE");
        SimpleDateFormat dateformat = new SimpleDateFormat("EEE MM/dd/yy hh:mm a");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        dateformat.setTimeZone(TimeZone.getTimeZone("UTC"));

        ArrayList<String>readings = new ArrayList<>();

        ArrayList inspection = reportsData.getInspection();
        try {
            if (inspection!=null){

                for (Object object : inspection){
                    FSLDataItem item = (FSLDataItem) object;

                    if (item.readings!=null){

                        for (Object readingObj : item.readings){
                            FSLDataReading reading = (FSLDataReading) readingObj;

                            Calendar date = Calendar.getInstance();
                            date.setTime(new Date(reading.dateTimeStamp));
                            date.setTimeZone(TimeZone.getTimeZone("UTC"));
                            date.set(Calendar.HOUR,0);
                            date.set(Calendar.MINUTE,1);
                            date.set(Calendar.AM_PM,Calendar.AM);
                            date.add(Calendar.MINUTE,site.effectiveUTCOffset);

                            if (days.day.equalsIgnoreCase(format.format(date.getTime()))){
                                if (reading.itemName.equalsIgnoreCase("inspection")){

                                    readings.add(reading.textReading);
                                }
                            }
                        }
                    }
                }
            }

         statusPattern = FilterReading(readings,days,section);

        }catch (Exception e){
            e.printStackTrace();
        }

        return statusPattern;
    }

    private ArrayList<Integer> FilterReading(ArrayList<String>reading,Days days,String section){

        ArrayList<Integer>statuses = new ArrayList<>();
        SimpleDateFormat format = new SimpleDateFormat("EEE");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));


        int total_form = days.getForms().size();
        ArrayList<String>filteredForms = new ArrayList<>();

        for (Forms form: days.getForms()){

            boolean populated = false;

            ArrayList<String>daypartChecker = new ArrayList<>();

            for (String read : reading){

                String[] removed$ = read.split(Pattern.quote("$"));
                String[] removed_ = removed$[0].split("_");

                String formName = removed_[0];
                String status = removed_[1];
                int daypart = 1;

                if (form.formName.equalsIgnoreCase(formName)){

                    System.out.println("SECTION : "+section +" | "+ read + " | "+ days.day+ " | Daypart : "+form.getDayparts() + " | "+form.title);

                    if (removed_.length>=3){
                        daypart = Integer.parseInt(removed_[2])+1;
                    }

                    if (!filteredForms.contains(formName)){
                        filteredForms.add(formName);
                    }

                    if (form.getDayparts().contains(String.valueOf(daypart))){

                        ArrayList<String>dayparts = form.getDayparts();

                        if (dayparts.size()>1){
                            for (String dp : dayparts){
                                if (dp.equalsIgnoreCase(String.valueOf(daypart))){
                                    statuses.add(Integer.parseInt(status));
                                    System.out.println("------SUCCESS WITH MULTIPLE DAYPART : "+ days.day);

                                    if (!daypartChecker.contains(dp)){
                                        daypartChecker.add(dp);

                                        if (daypartChecker.containsAll(dayparts)){
                                            populated = true;
                                        }
                                    }
                                }
                            }
                        }else {
                            populated = true;
                            daypartChecker.add(String.valueOf(daypart));
                            System.out.println("-------SUCCESS SOLO DAYPART "+ days.day);
                            statuses.add(Integer.parseInt(status));
                        }
                    }else {
                        if (!populated){
                            System.out.print("---------FAILED "+ days.day);
                            statuses.add(-1);
                        }
                    }
                }
            }

            if (!daypartChecker.containsAll(form.getDayparts())){
                statuses.add(-1);
                System.out.println("checker : "+daypartChecker + " | "+ "ORIGINAL : "+form.getDayparts() + "FAILED  TO POPULATE ALL "+ form.formName);

            }else {
                System.out.println("checker : "+daypartChecker + " | "+ "ORIGINAL : "+form.getDayparts() + "SUCCESS  TO POPULATE ALL "+ form.formName);
            }
        }

        if (filteredForms.size()!=total_form){
            statuses.add(-1);
        }
        System.out.println("F : "+ filteredForms.size() + " | "+ " T: "+total_form + " | "+ statuses);



        return statuses;
    }


    private ArrayList getBreakfastItems(Site site,FSLDataFSLFormsClient fslForms,long startTimestamp,long endTimestamp) throws TException{
        ArrayList overallitems = new ArrayList();

        Schema schema = Schema.getInstance();

        DownloadinspectionItems(site,fslForms,startTimestamp,endTimestamp);

        ArrayList OpsLeaderAcknowledgement = fslForms.GetItemOverDateRange(site.CustomerId, site.siteid,
                startTimestamp,endTimestamp,"OpsLeaderAcknowledgement");

        overallitems.addAll(OpsLeaderAcknowledgement);

        ArrayList inspection = fslForms.GetItemOverDateRange(site.CustomerId, site.siteid,
                startTimestamp,endTimestamp,"inspection");

        overallitems.addAll(inspection);

        if (schema.getForms()!=null){

            for (Forms form : schema.getForms()){

                if (form.formName.equalsIgnoreCase("breakfast")){

                    if (form.items!=null){

                        for (FormItems item: form.items){

                            String itemname = "dp1$"+item.itemName;

                            ArrayList items = fslForms.GetItemOverDateRange(site.CustomerId, site.siteid,
                                    startTimestamp,endTimestamp,itemname);

                            if (items!=null){
                                if (items.size()>0){
                                    overallitems.addAll(items);
                                }
                            }
                        }
                    }
                    break;
                }
            }
        }
        return overallitems;
    }


    private void DownloadinspectionItems(Site site,FSLDataFSLFormsClient fslForms,long startTimestamp,long endTimestamp) throws TException{

        ReportsData reportsData = ReportsData.getInstance();

        ArrayList inspection = fslForms.GetItemOverDateRange(site.CustomerId, site.siteid,
                startTimestamp,endTimestamp,"inspection");
        reportsData.setInspection(inspection);

    }


    private void getFSLDataForm(final Date daily) {
        final Site site = ReportsData.getInstance().getSite().getSite();
        Log.i("Thrift ","getFSLDataForm start");
        try {
            final NamedServices ns = NamedServices.getInstance();
            if (ns.pimmServer!=null){

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(daily);
                            calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
                            calendar.add(Calendar.MINUTE,site.effectiveUTCOffset);
                            calendar.set(Calendar.HOUR,0);
                            calendar.set(Calendar.MINUTE,1);
                            calendar.set(Calendar.AM_PM,Calendar.AM);
                            calendar.clear(Calendar.SECOND);
                            calendar.clear(Calendar.MILLISECOND);

                            FSLDataFSLFormsClient fslForms = ns.fslFormsClient;
                            long timestamp = Long.parseLong(String.valueOf(calendar.getTime().getTime()));

                            System.out.println("Site ID: "+site.siteid);
                            System.out.println("CUSTOMER ID: "+site.CustomerId);
                            System.out.println("TIME STAMP : "+timestamp);

                            SimpleDateFormat format = new SimpleDateFormat("EEEE, MMMM d, yyyy hh:mm a");
                            format.setTimeZone(TimeZone.getTimeZone("UTC"));
                            System.out.println("form.dateTimeStamp : "+format.format(calendar.getTime()));

//                            long timestamp = 1585699260000l; //Long.parseLong("1585699260000");
                            FSLDataForm form = fslForms.getForm(site.CustomerId, site.siteid, timestamp);
                            ReportsData data = ReportsData.getInstance();
                            data.setForm(form);

                            startSiteSettingsEquipement(site);

                        } catch (TException e) {
                            e.printStackTrace();

                        }
                    }
                }).start();

            }else {
                Log.i("Thrift ","SERVER NULL ");

            }

            Log.i("Thrift ","saveDefaultSiteSettingEquipment End");

        }catch (TException e){
            e.printStackTrace();

        }

    }

    private void startSiteSettingsEquipement(final Site site) {
        Log.i("Thrift ","startSiteSettingsEquipement start");
        try {
            final String siteId = site.siteid;
            final NamedServices ns = NamedServices.getInstance();
            if (ns.pimmServer!=null){
                final SiteSettingsSiteConfig config = ns.siteSettingsClient;

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        ArrayList equipments = null;
                        try {
                            equipments = config.getSiteEquipment(siteId);
                            ArrayList<SiteSettingsEquipment>newEquipment = new ArrayList<>();
                            ArrayList<SiteSettingsEquipment>EnabledEquipments = new ArrayList<>();
                            if (equipments!=null){
                                if (equipments.size()>0){
                                    for (Object object : equipments){
                                        SiteSettingsEquipment settings = (SiteSettingsEquipment) object;
                                        if (settings.inUse){
                                            EnabledEquipments.add(settings);
                                        }
                                    }
                                }
                                newEquipment.addAll(equipments);
                            }

                            ReportsData data = ReportsData.getInstance();
                            data.setEquipment(newEquipment);
                            data.setEnabledEquipments(EnabledEquipments);

                            if (id == R.id.food_safety){
                                loadFileInExternalStore("wendys.html");
                            }
                            else if (id == R.id.closing_opening){
                                loadFileInExternalStore("wendys.html");
                            }
                            else if (id == R.id.rush_ready){
                                loadFileInExternalStore("wendys.html");

                            }else if (id == R.id.breakfast_readiness){
                                webView.setWebChromeClient(new Webcromeclient());
                                webView.loadUrl("file:///storage/emulated/0/.SCMFiles/wendys.2.0.0/reports/CheckListBreakfast/index.html");
                            }

                        } catch (TException e) {
                            e.printStackTrace();

                        }
                    }
                }).start();

            }else {
                Log.i("Thrift ","SERVER NULL ");

            }
            Log.i("Thrift ","startSiteSettingsEquipement End");
        }catch (TException e){
            e.printStackTrace();

        }
    }

    private Date getDateWithDaypart(long reading){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(reading));
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        calendar.add(Calendar.MINUTE,site.effectiveUTCOffset);

        Schema schema = Schema.getInstance();
        Calendar currentDate = Calendar.getInstance();
        currentDate.setTime(calendar.getTime());
        currentDate.setTimeZone(TimeZone.getTimeZone("UTC"));

        Dayparts daypart = schema.getDayparts().get(0);
        Calendar start = SCMTool.setCalendarDaypart(currentDate.getTime(),daypart.start);


        if (currentDate.getTime().compareTo(start.getTime())<0) {
            calendar.add(Calendar.DATE,-1);
        }

        return calendar.getTime();
    }

    private void getSiteSettingConfiguration(final Site site) {
        Log.i("Thrift ","getSiteSettingConfiguration start");
        try {
            final String siteId = site.siteid;

            final NamedServices ns = NamedServices.getInstance();
            if (ns.pimmServer!=null){
                final SiteSettingsSiteConfig config = ns.siteSettingsClient;

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        ArrayList sitesettingconfig = new ArrayList();
                        ArrayList users = null;
                        try {
                            sitesettingconfig = (config.getSiteProperties(siteId).configSettings);
                            ReportsData.getInstance().setSettingsSiteConfigs(sitesettingconfig);
                        } catch (TException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();

            }else {
                Log.i("Thrift ","SERVER NULL ");
            }

            Log.i("Thrift ","startSiteSettingsEquipement End");

        }catch (TException e){
            e.printStackTrace();
        }
    }


}