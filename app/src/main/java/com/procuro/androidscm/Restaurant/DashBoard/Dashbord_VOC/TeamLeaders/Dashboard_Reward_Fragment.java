package com.procuro.androidscm.Restaurant.DashBoard.Dashbord_VOC.TeamLeaders;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.SCMDataManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class Dashboard_Reward_Fragment extends Fragment {

    Dashboard_Reward_RecyclerViewAdapter adapter;
    RecyclerView employeelistview;


    public Dashboard_Reward_Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dashboard_voc_team_leader_fragment, container, false);
        employeelistview = view.findViewById(R.id.employee_listview);

        ArrayList<CustomUser> users = SCMDataManager.getInstance().getStoreUsers();

        Collections.sort(users, new Comparator<CustomUser>() {
            @Override
            public int compare(CustomUser o1, CustomUser o2) {

                Integer r1 = o1.getUser().awardList.size();
                Integer r2 = o2.getUser().awardList.size();

                Integer s1 = o1.getUser().old_certificationList.size();
                Integer s2 = o2.getUser().old_certificationList.size();

                int rCompare = r2.compareTo(r1);
                if (rCompare == 0){
                    return s2.compareTo(s1);
                }

                return  rCompare;
            }
        });



        adapter = new Dashboard_Reward_RecyclerViewAdapter(getContext(),users ,getActivity());
        employeelistview.setLayoutManager(new GridLayoutManager(getContext(),1));
        employeelistview.setAdapter(adapter);


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

    }

}
