package com.procuro.androidscm.Restaurant;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;
import com.procuro.androidscm.Logout_Confirmation_DialogFragment;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.AlertFragment.AlertFragment;
import com.procuro.androidscm.Restaurant.MoreFragment.MoreFragment;
import com.procuro.androidscm.Restaurant.Qualification.QualificationActivityFragment;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationFragment;
import com.procuro.androidscm.Restaurant.ReportsFragment.Reports_fragment;
import com.procuro.androidscm.Restaurant.Schema.Days;
import com.procuro.androidscm.Restaurant.Schema.Forms;
import com.procuro.androidscm.Restaurant.Schema.Schema;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.StatusFragment;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.androidscm.UnderMaintenance_DialogFragment;
import com.procuro.apimmdatamanagerlib.Site;

import androidx.annotation.Keep;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

@Keep
public class Restaurant_home_page extends AppCompatActivity {
    private TextView mTextMessage;
    public static BottomNavigationView navView;
    public static  int NAV_QUALIFICATIONS = R.id.nav_qualifications;
    public static boolean custom;
    int PreviousItem = 0;

    public boolean isCustom() {
        return custom;
    }
    public static void setCustom(boolean parameter) {
        custom = parameter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.restaurant_home_page);
        navView = findViewById(R.id.btm_nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        InitializeSelectedSite();

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(MenuItem item) {


            if (item.getItemId()!=PreviousItem){
                PreviousItem = item.getItemId();
                switch (item.getItemId()) {
                    case R.id.nav_qualifications:
                        DisplayQualificationFragment();
                        return true;
                    case R.id.nav_status:
                        displayStatusFragment();
                        return true;
                    case R.id.nav_alert:
                        DisplayAlertsFragment();
                        return true;
                    case R.id.nav_report:
                        DisplayReportsFragment();
                        return true;
                    case R.id.nav_more:
                        DisplayMoreFragment();
                        return true;
                }
            }

            return false;
        }
    };

    public void displayStatusFragment() {

        StatusFragment fragment = new StatusFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.resturant_fragment_container,
                fragment).commit();
    }

    private void DisplayQualificationFragment() {
        if (!custom){
            getSupportFragmentManager().beginTransaction().replace(R.id.resturant_fragment_container,
                    new QualificationFragment()).commit();
        }else {
            if(!isFinishing()) {
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.add(R.id.resturant_fragment_container, new QualificationActivityFragment());
                ft.commitAllowingStateLoss();
                custom =false;
            }
            DisplayQualificationFragment();
        }
    }

    private void DisplayAlertsFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.resturant_fragment_container,
                new AlertFragment()).commit();
    }

    private void DisplayReportsFragment() {
        getSupportFragmentManager().beginTransaction().replace(R.id.resturant_fragment_container,
                new Reports_fragment()).commit();
    }

    private void DisplayMoreFragment() {
            getSupportFragmentManager().beginTransaction().replace(R.id.resturant_fragment_container,
                    new MoreFragment()).commit();
        }

    private void setSharedpref(){
        //Select The 1st Item In The List As Selected Selected Site
        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(SCMDataManager.getInstance());
        prefsEditor.putString("SCMDataManager", json);
        prefsEditor.commit();

        navView.setSelectedItemId(R.id.nav_status);

    }

    private void InitializeSelectedSite(){
        if (SCMDataManager.getInstance().getSelectedSite() == null) {
            CustomSite site = SCMDataManager.getInstance().getCustomSites().get(0);
            SCMDataManager.getInstance().setSelectedSite(site);
            DownloadInitialData(site.getSite(),null);
            setSharedpref();

        }else {
            boolean found = false;
            for (CustomSite customSite : SCMDataManager.getInstance().getCustomSites()){
                if (customSite.getSite().sitename.equalsIgnoreCase(SCMDataManager.getInstance().getSelectedSite().getSite().sitename)){
                    found = true;
                    DownloadInitialData(customSite.getSite(),null);
                    setSharedpref();
                    break;
                }
            }
            if (!found){
                CustomSite site = SCMDataManager.getInstance().getCustomSites().get(0);
                SCMDataManager.getInstance().setSelectedSite(site);
                DownloadInitialData(site.getSite(),null);
                setSharedpref();
            }
        }
    }

    public static void DownloadInitialData(Site site, ProgressDialog progressDialog){
        SCMDataManager.DownloadSitePropertyValues(site.siteid);
        SCMDataManager.DownloadSiteSettingsConfiguration(site);
        SCMDataManager.DownloadHoursOfOperationData(site.siteid);
        SCMTool.getStoreUsers(site,progressDialog);
    }

    public static void getFormsInCurrentDay(){
        SimpleDateFormat format = new SimpleDateFormat("EEE");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        Calendar currentdate = Calendar.getInstance();

        ArrayList<String> sections = new ArrayList<>();
        ArrayList<Days> foodsafety = new ArrayList<>();
        ArrayList<Days> checklist = new ArrayList<>();

        for (Days days : Schema.getInstance().getDays()) {

            if (format.format(currentdate.getTime()).equalsIgnoreCase(days.day)){

                for (Forms form : Schema.getInstance().getForms()){

                    if (form.formName.equalsIgnoreCase(days.formName)){

                        if (days.section.contains("Cleaning Tasks")){

                        }else if (days.section.contains("Operations")){

                        }else if (days.section.contains("_Audit")){

                        }else if (days.section.contains("Checklist")){
                            if (!sections.contains(days.section)) {
                                sections.add(days.section);
                                days.setForms(new ArrayList<Forms>());
                                days.getForms().add(form);
                                checklist.add(days);
                                Schema.getInstance().setChecklist(checklist);
                            } else {
                                for (Days days1 : Schema.getInstance().getChecklist()){
                                    if (days.section.equalsIgnoreCase(days1.section)){
                                        days1.getForms().add(form);
                                    }
                                }
                            }

                        }else {
                            if (!sections.contains(days.section)) {
                                sections.add(days.section);
                                days.setForms(new ArrayList<Forms>());
                                days.getForms().add(form);
                                foodsafety.add(days);
                                Schema.getInstance().setFoodsafety(foodsafety);
                            } else {
                                for (Days days1 : Schema.getInstance().getFoodsafety()){
                                    if (days.section.equalsIgnoreCase(days1.section)){
                                        days1.getForms().add(form);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    @Override
    public void onBackPressed() {
        DisplayLogoutConfirmation();

    }

    private void DisplayLogoutConfirmation() {
        DialogFragment newFragment = Logout_Confirmation_DialogFragment.newInstance();
        assert getFragmentManager() != null;
        newFragment.show(getSupportFragmentManager(), "dialog");
    }

    private void DisplayUnderMaintenanceMessage() {
        DialogFragment newFragment = UnderMaintenance_DialogFragment.newInstance();
        assert getFragmentManager() != null;
        newFragment.show(getSupportFragmentManager(), "dialog");
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (custom){
            getSupportFragmentManager().beginTransaction().replace(R.id.resturant_fragment_container,
                    new QualificationActivityFragment()).commit();
        }
    }
}
