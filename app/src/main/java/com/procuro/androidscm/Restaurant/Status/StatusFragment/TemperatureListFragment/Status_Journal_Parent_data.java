package com.procuro.androidscm.Restaurant.Status.StatusFragment.TemperatureListFragment;

import java.io.Serializable;
import java.util.ArrayList;

public class Status_Journal_Parent_data implements Serializable {
    private String parent_name;
    private ArrayList<Status_Journal_child_data> statusJournal_child_data;

    public Status_Journal_Parent_data(String parent_name, ArrayList<Status_Journal_child_data> statusJournal_child_data) {
        this.parent_name = parent_name;
        this.statusJournal_child_data = statusJournal_child_data;
    }

    public String getParent_name() {
        return parent_name;
    }

    public void setParent_name(String parent_name) {
        this.parent_name = parent_name;
    }

    public ArrayList<Status_Journal_child_data> getStatusJournal_child_data() {
        return statusJournal_child_data;
    }

    public void setStatusJournal_child_data(ArrayList<Status_Journal_child_data> statusJournal_child_data) {
        this.statusJournal_child_data = statusJournal_child_data;
    }
}
