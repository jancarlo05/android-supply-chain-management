package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_CommunicationNotes;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.procuro.androidscm.R;
import com.procuro.apimmdatamanagerlib.SMSCommunicationNotes;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TimeZone;


public class DashboardCommunicatonNotesListAdapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<SMSCommunicationNotes> smsCommunicationNotes;


    public DashboardCommunicatonNotesListAdapter(Context context, ArrayList<SMSCommunicationNotes> smsCommunicationNotes) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.smsCommunicationNotes = smsCommunicationNotes;

    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return smsCommunicationNotes.size();
    }

    @Override
    public Object getItem(int position) {
        return smsCommunicationNotes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {

        SMSCommunicationNotes note = smsCommunicationNotes.get(position);
        view = inflater.inflate(R.layout.dashboard_communicationnote_list_data, null);

        TextView name,message,date;
        name = view.findViewById(R.id.name);
        date = view.findViewById(R.id.date);
        message = view.findViewById(R.id.message);

        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yy, h:mm a");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        name.setText(note.userName);
        date.setText(format.format(note.timestamp).replace(",","\n"));

        message.setText(note.message.replace("%20"," ").replace("%0A"," ").replace("\n"," "));

        return view;
    }



}

