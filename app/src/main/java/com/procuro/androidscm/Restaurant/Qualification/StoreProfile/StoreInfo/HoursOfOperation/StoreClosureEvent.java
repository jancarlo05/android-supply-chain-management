package com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.HoursOfOperation;

import com.procuro.androidscm.Restaurant.Calendar.CalendarEvent;

import java.util.ArrayList;

public class StoreClosureEvent {

    String name;
    ArrayList<CalendarEvent>events;

    public StoreClosureEvent() {
    }

    public StoreClosureEvent(String name, ArrayList<CalendarEvent> events) {
        this.name = name;
        this.events = events;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<CalendarEvent> getEvents() {
        return events;
    }

    public void setEvents(ArrayList<CalendarEvent> events) {
        this.events = events;
    }
}
