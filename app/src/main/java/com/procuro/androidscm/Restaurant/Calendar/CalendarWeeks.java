package com.procuro.androidscm.Restaurant.Calendar;

import java.util.ArrayList;

public class CalendarWeeks {
    int weeknumber;
    ArrayList<CalendarDaily>dailies;

    public CalendarWeeks(int weeknumber, ArrayList<CalendarDaily> dailies) {
        this.weeknumber = weeknumber;
        this.dailies = dailies;
    }

    public int getWeeknumber() {
        return weeknumber;
    }

    public void setWeeknumber(int weeknumber) {
        this.weeknumber = weeknumber;
    }

    public ArrayList<CalendarDaily> getDailies() {
        return dailies;
    }

    public void setDailies(ArrayList<CalendarDaily> dailies) {
        this.dailies = dailies;
    }
}
