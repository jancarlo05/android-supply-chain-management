package com.procuro.androidscm.Restaurant.DashBoard;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.procuro.androidscm.R;

import java.util.ArrayList;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;


public class Dashboard_Managers_Confirmation_SelectManager_list_adapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<CustomUser> users;
    private TextView title;
    private SimpleTooltip tooltip;


    public Dashboard_Managers_Confirmation_SelectManager_list_adapter(Context context, ArrayList<CustomUser> users,TextView title,
                                                                      SimpleTooltip tooltip) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.users = users;
        this.title = title;
        this.tooltip = tooltip;
    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public Object getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        holder = new ViewHolder();
        view = inflater.inflate(R.layout.managers_confirmation_users_list_data, null);
        view.setTag(holder);
        final CustomUser user = users.get(position);
        TextView name = view.findViewById(R.id.name);
        CheckBox checkBox = view.findViewById(R.id.checkbox);

        name.setText(user.getUser().firstName);
        name.append(" ");
        name.append(user.getUser().lastName);


        if (user.isSelectedmanager()){
            checkBox.setChecked(true);
        }else {
            checkBox.setChecked(false);
        }

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked){
                    for(CustomUser customUser : users){
                        customUser.setSelectedmanager(false);
                    }
                    user.setSelectedmanager(true);
                    title.setText(user.getUser().firstName);
                    title.append(" ");
                    title.append(user.getUser().lastName);
                    tooltip.dismiss();

                }

            }
        });




            return view;
    }

}

