package com.procuro.androidscm.Restaurant.Calendar;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.GlideApp;
import com.procuro.androidscm.R;
import com.procuro.androidscm.SCMTool;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

public class CalendarEventDialogFragment extends DialogFragment {

    private TextView name,year;
    private ImageView icon;

    public static CalendarEvent event;
    public static int years;

    public static CalendarEventDialogFragment newInstance(CalendarEvent eventObj,int yearsOBj) {
        years = yearsOBj;
        event = eventObj;
        return new CalendarEventDialogFragment();
    }
    @Override
        public int getTheme() {
        return R.style.slide_down_up;
        }

    @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        setDialogSettings();

        View view = null;

        if (event.getType().equalsIgnoreCase("Birthday")){

            view = inflater.inflate(R.layout.calendar_birthday_popup, container, false);
            view.setFocusableInTouchMode(true);

            name = view.findViewById(R.id.name);
            icon = view.findViewById(R.id.icon);

            name.setText(event.getName());
            GlideApp.with(getContext()).asGif().load(R.drawable.birthday_gif).fitCenter().into(icon);

        }else if (event.getType().equalsIgnoreCase("Anniversary")){

            view = inflater.inflate(R.layout.calendar_anniversary_popup, container, false);
            view.setFocusableInTouchMode(true);

            name = view.findViewById(R.id.name);
            icon = view.findViewById(R.id.icon);
            year = view.findViewById(R.id.year);

            if (years >0){
                year.setText(String.valueOf(years));
            }else {
                year.setVisibility(View.INVISIBLE);
            }
            name.setText(event.getName());

            GlideApp.with(getContext()).asGif().load(R.drawable.wendy_anniversary_animation).fitCenter().into(icon);

        }




        setupOnclicks(view);
        return view;
    }


    private void setupOnclicks(View view){

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

    }

    private void setDialogSettings(){
        //   int height = (int)(getResources().getDisplayMetrics().heightPixels*0.90);
//           int width = 320;
//
        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
        int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.60);

        Objects.requireNonNull(getDialog().getWindow()).setLayout(width, height);
        getDialog().setCanceledOnTouchOutside(true);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.slide_down_up;
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


    }


}
