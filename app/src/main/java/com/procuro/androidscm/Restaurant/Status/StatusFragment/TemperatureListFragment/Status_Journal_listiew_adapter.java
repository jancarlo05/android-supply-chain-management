package com.procuro.androidscm.Restaurant.Status.StatusFragment.TemperatureListFragment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.procuro.androidscm.R;

import java.util.ArrayList;


public class Status_Journal_listiew_adapter extends BaseExpandableListAdapter {
    Context context = null;
    ArrayList<Status_Journal_Parent_data> originalAssignedrouteList;
    int completed_Counter = 0;

    public Status_Journal_listiew_adapter(Context context, ArrayList<Status_Journal_Parent_data> data) {
        this.context = context;
        this.originalAssignedrouteList = new ArrayList<>();
        this.originalAssignedrouteList.addAll(data);

    }

    @Override
    public int getGroupCount() {
        return originalAssignedrouteList.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return originalAssignedrouteList.get(i).getStatusJournal_child_data().size();
    }

    @Override
    public Status_Journal_Parent_data getGroup(int i) {
        return originalAssignedrouteList.get(i);
    }

    @Override
    public Status_Journal_child_data getChild(int groupPosition, int childPosition) {
        return originalAssignedrouteList.get(groupPosition).getStatusJournal_child_data().get(childPosition);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int position, boolean b, View contentView, ViewGroup parent) {
        Status_Journal_Parent_data parent_data = originalAssignedrouteList.get(position);
        if(position == 0){
            contentView = LayoutInflater.from(context).inflate(R.layout.status_journal_parent_data_rounded_top, parent, false);
            TextView textView = contentView.findViewById(R.id.rowParentText);
            textView.setText(parent_data.getParent_name());
        }else {
            contentView = LayoutInflater.from(context).inflate(R.layout.status_journal_parent_data, parent, false);
            TextView textView = contentView.findViewById(R.id.rowParentText);
            textView.setText(parent_data.getParent_name());
        }

            return contentView;
    }


    @Override
    public View getChildView(int groupPosition, int childPosition, boolean b, View contentView, ViewGroup parent) {
        final Status_Journal_child_data child_data = originalAssignedrouteList.get(groupPosition).getStatusJournal_child_data().get(childPosition);
                contentView = LayoutInflater.from(context).inflate(R.layout.status_journal_child_notification_data, parent, false);
                return contentView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }




}