package com.procuro.androidscm.Restaurant.Qualification.QualificationFragment;

import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.apimmdatamanagerlib.CorpStructure;

import java.util.ArrayList;

public class Corp_District {

    CorpStructure corpStructures;
    private ArrayList<CustomSite>sites;
    String parentid;
    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public Corp_District( ) {
    }

    public Corp_District(CorpStructure corpStructures, ArrayList<CustomSite> sites, String parentid) {
        this.corpStructures = corpStructures;
        this.sites = sites;
        this.parentid = parentid;
    }

    public Corp_District(String corpid, ArrayList<CustomSite> sites) {
        this.parentid = corpid;
        this.sites = sites;
    }

    public Corp_District(CorpStructure corpStructures) {
        this.corpStructures = corpStructures;
    }

    public CorpStructure getCorpStructures() {
        return corpStructures;
    }

    public void setCorpStructures(CorpStructure corpStructures) {
        this.corpStructures = corpStructures;
    }

    public ArrayList<CustomSite> getSites() {
        return sites;
    }

    public void setSites(ArrayList<CustomSite> sites) {
        this.sites = sites;
    }
}
