package com.procuro.androidscm.Restaurant.Status;

import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Regional;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.apimmdatamanagerlib.PimmDevice;
import com.procuro.apimmdatamanagerlib.PimmInstance;

public class StatusData {
    private static StatusData instance = new StatusData();

    private Corp_Regional regional;
    private String selectedItem;
    private int employeeSize;
    private boolean StateByStateViewEnabled;
    private boolean selectedSiteEnabled;
    private boolean allStoreEnabled;
    private boolean OwnerShipViewEnabled;
    private boolean temperatureViewEnabled;
    private PimmDevice pimmDevice;
    private CustomSite selectedSite;
    private PimmInstance pimmInstance;
    private boolean allstore;



    public static StatusData getInstance() {
        if(instance == null) {
            instance = new StatusData();
        }
        return instance;
    }

    public boolean isAllstore() {
        return allstore;
    }

    public void setAllstore(boolean allstore) {
        this.allstore = allstore;
    }

    public boolean isTemperatureViewEnabled() {
        return temperatureViewEnabled;
    }

    public void setTemperatureViewEnabled(boolean temperatureViewEnabled) {
        this.temperatureViewEnabled = temperatureViewEnabled;
    }

    public PimmInstance getPimmInstance() {
        return pimmInstance;
    }

    public void setPimmInstance(PimmInstance pimmInstance) {
        this.pimmInstance = pimmInstance;
    }

    public PimmDevice getPimmDevice() {
        return pimmDevice;
    }

    public void setPimmDevice(PimmDevice pimmDevice) {
        this.pimmDevice = pimmDevice;
    }

    public boolean isOwnerShipViewEnabled() {
        return OwnerShipViewEnabled;
    }

    public void setOwnerShipViewEnabled(boolean ownerShipViewEnabled) {
        OwnerShipViewEnabled = ownerShipViewEnabled;
    }

    public boolean isStateByStateViewEnabled() {
        return StateByStateViewEnabled;
    }

    public void setStateByStateViewEnabled(boolean stateByStateViewEnabled) {
        StateByStateViewEnabled = stateByStateViewEnabled;
    }

    public CustomSite getSelectedSite() {
        return selectedSite;
    }

    public void setSelectedSite(CustomSite selectedSite) {
        this.selectedSite = selectedSite;
    }

    public static void setInstance(StatusData instance) {
        StatusData.instance = instance;
    }

    public boolean isAllStoreEnabled() {
        return allStoreEnabled;
    }

    public void setAllStoreEnabled(boolean allStoreEnabled) {
        this.allStoreEnabled = allStoreEnabled;
    }

    public Corp_Regional getRegional() {
        return regional;
    }

    public void setRegional(Corp_Regional regional) {
        this.regional = regional;
    }

    public String getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(String selectedItem) {
        this.selectedItem = selectedItem;
    }

    public int getEmployeeSize() {
        return employeeSize;
    }

    public void setEmployeeSize(int employeeSize) {
        this.employeeSize = employeeSize;
    }


    public boolean isStateEnabled() {
        return StateByStateViewEnabled;
    }

    public void setStateEnabled(boolean stateEnabled) {
        this.StateByStateViewEnabled = stateEnabled;
    }

    public boolean isSelectedSiteEnabled() {
        return selectedSiteEnabled;
    }

    public void setSelectedSiteEnabled(boolean selectedSiteEnabled) {
        this.selectedSiteEnabled = selectedSiteEnabled;
    }
}
