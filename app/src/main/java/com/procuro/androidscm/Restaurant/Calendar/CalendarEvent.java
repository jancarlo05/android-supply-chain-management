package com.procuro.androidscm.Restaurant.Calendar;

import com.procuro.apimmdatamanagerlib.User;

import java.util.Date;

public class CalendarEvent {

    private Date date;
    private String name;
    private String type;
    private User user;
    private Date startTime;
    private Date EndTime;
    private boolean allday;

    public CalendarEvent() {

    }


    public boolean isAllday() {
        return allday;
    }

    public void setAllday(boolean allday) {
        this.allday = allday;
    }

    public CalendarEvent(String name, String type) {
        this.name = name;
        this.type = type;
    }


    public CalendarEvent(Date date, String name, String type) {
        this.date = date;
        this.name = name;
        this.type = type;
    }

    public CalendarEvent(Date date, String type, User user) {
        this.date = date;
        this.type = type;
        this.user = user;
        this.name = user.firstName+" "+user.lastName;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return EndTime;
    }

    public void setEndTime(Date endTime) {
        EndTime = endTime;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
