package com.procuro.androidscm.Restaurant.Qualification.StoreProfile.TeamAndContancts;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreProfile_Emergency_listviewAdapter;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.TeamAndContancts.ManagementTeam.ManagementTeamData;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.TeamAndContancts.ManagementTeam.Management_Team_ListAdapter;
import com.procuro.androidscm.Restaurant.SOS.SOSActivity;
import com.procuro.androidscm.Restaurant.SOS.SOSData;
import com.procuro.androidscm.Restaurant.UserProfile.CustomRole;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileActivity;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileData;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.androidscm.UnderMaintenance_DialogFragment;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.PropertyValue;
import com.procuro.apimmdatamanagerlib.Qualification;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.SiteSettings;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.util.ArrayList;
import java.util.Objects;


public class Qualification_TeamAndContancts_Fragment extends Fragment {


    private CustomSite storeList;
    private Site site;
    private TextView fire_dept,police_dept,rescue_squad,hospital,management_list_message;
    private ListView emergency_list,managementteam_list;
    private aPimmDataManager dataManager;
    public  static TextView wcsd,district_manager;
    public  static ConstraintLayout wcsd_Container,district_manager_container;

    public Qualification_TeamAndContancts_Fragment() {

        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.qualification_management_and_team_fragment, container, false);

        fire_dept = view.findViewById(R.id.fire_dept);
        police_dept = view.findViewById(R.id.police_dept);
        rescue_squad = view.findViewById(R.id.rescue_squad);
        hospital = view.findViewById(R.id.hospital);
        emergency_list = view.findViewById(R.id.emergency);
        managementteam_list = view.findViewById(R.id.listView);
        management_list_message = view.findViewById(R.id.management_list_message);
        wcsd= view.findViewById(R.id.wcsd);
        wcsd_Container = view.findViewById(R.id.wcsd_Container);
        district_manager_container = view.findViewById(R.id.district_manager_container);
        district_manager = view.findViewById(R.id.district_manager);

        getSelectedStore();

        CheckSitePropertyValues();

        setupManagementTeamList();

        return view;

    }

    private void setupManagementTeamList(){
        try {
            ManagementTeamData data = ManagementTeamData.getInstance();
            if (data.getThriftusers()!=null){
                if (data.getThriftusers().size()>0){
                   FindDistrictManager(data.getThriftusers());
                    Management_Team_ListAdapter adapter = new Management_Team_ListAdapter(getContext(),data.getThriftusers(),getActivity());
                    managementteam_list.setAdapter(adapter);
                    management_list_message.setVisibility(View.GONE);
                }
            }else {
                management_list_message.setVisibility(View.VISIBLE);
                management_list_message.setText("Please Wait");
                new CountDownTimer(1000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                    }
                    @Override
                    public void onFinish() {
                        setupManagementTeamList();
                    }
                }.start();

            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private void FindDistrictManager(ArrayList<CustomUser>customUsers){
        try {
            for (final CustomUser user : customUsers){
                String roleName = "--";
                if (user.getUser().roles!=null) {
                    ArrayList<CustomRole> customRoles = CustomRole.getAllCustomRole(user.getUser().roles);
                    if (customRoles.size() >= 1) {
                        roleName = (customRoles.get(0).getName());
                    }

                    if (roleName.equalsIgnoreCase("District Manager")){
                        district_manager.setText(SCMTool.CheckString(user.getUser().dayPhone,"--"));

                        district_manager_container.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                QualificationData qualificationData = QualificationData.getInstance();
                                Intent intent = new Intent(getActivity(), UserProfileActivity.class);
                                UserProfileData profileData = new UserProfileData();
                                profileData.setUserEditEnabled(false);
                                profileData.setCustomSite(qualificationData.getSelectedSite());
                                profileData.setCustomUser(user);
                                profileData.setFromQualification(true);
                                profileData.setIsupdate(true);
                                profileData.setPrevActivity("MT");
                                UserProfileData.setInstance(profileData);
                                getActivity().startActivity(intent);
                            }
                        });
                        break;
                    }
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void getSelectedStore(){
        try {
            if (QualificationData.getInstance().getSelectedSite()!=null){
                this.storeList = QualificationData.getInstance().getSelectedSite();
                this.site = storeList.getSite();
            }
            else {
                this.storeList = SCMDataManager.getInstance().getSelectedSite();
                this.site = storeList.getSite();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void DownloadSitePropertyValues(String siteID){
        dataManager = aPimmDataManager.getInstance();
        dataManager.getSiteSettingsForSiteId(siteID, new OnCompleteListeners.getSiteSettingsForSiteIdListener() {
            @Override
            public void getSiteSettingsForSiteId(SiteSettings siteSettings, Error error) {
                if (error == null){
                    DisplayEmergencyContact(siteSettings.settings);
                }else {
                    DisplayEmergencyContact(siteSettings.settings);
                }
            }
        });
    }

    private void CheckSitePropertyValues(){
        QualificationData qualificationData = QualificationData.getInstance();
        if (qualificationData.getSosData() == null){
            SOSData sosData = new SOSData();
            sosData.setSite(qualificationData.getSelectedSite().getSite());
            SOSData.setInstance(sosData);
            qualificationData.setSosData(sosData);
            DownloadSitePropertyValues(qualificationData.getSelectedSite().getSite().siteid);
        }else {
            DisplayEmergencyContact(qualificationData.getSosData().getPropertyValues());
        }
    }

    private void DisplayEmergencyContact(ArrayList<PropertyValue> propertyValues){
        ArrayList<PropertyValue>contancts = new ArrayList<>();
        PropertyValue Police = new PropertyValue(),
                Fire = new PropertyValue(),
                RescueSquad = new PropertyValue(),
                Hospital = new PropertyValue(),
                WendysContacts = new PropertyValue(),
                Franchise = new PropertyValue(),
                DistrictManager = new PropertyValue(),
                DirectorAreaOps = new PropertyValue(),
                EMSSafetyMgmt = new PropertyValue(),
                LossPrevention = new PropertyValue(),
                AutomatedCommSystem = new PropertyValue(),
                SecurityDesk = new PropertyValue();


        WendysContacts.property = "Company Contacts";
        Fire.property = "FM:EC:Fire";
        Police.property = "FM:EC:Police";
        RescueSquad.property = "FM:EC:RescueSquad";
        Hospital.property = "FM:EC:Hospital";
        Franchise.property = "FM:EC:Franchise";
        DistrictManager.property = "FM:EC:DistrictManager";
        DirectorAreaOps.property = "FM:EC:DirectorAreaOps";
        EMSSafetyMgmt.property = "FM:EC:EMSSafetyMgmt";
        LossPrevention.property = "FM:EC:LossPrevention";
        AutomatedCommSystem.property = "FM:EC:AutomatedCommSystem(ACS)";
        SecurityDesk.property = "FM:EC:SecurityDesk";


        try {
            if (propertyValues!=null){

               for (PropertyValue propertyValue : propertyValues){
                   if (propertyValue!=null){
                       if (propertyValue.property.equalsIgnoreCase("FM:EC:Police")){
                           Police = propertyValue;
                       }
                       else if (propertyValue.property.equalsIgnoreCase("FM:EC:Fire")){
                           Fire = propertyValue;
                       }
                       else if (propertyValue.property.equalsIgnoreCase("FM:EC:RescueSquad")){
                           RescueSquad = propertyValue;
                       }
                       else if (propertyValue.property.equalsIgnoreCase("FM:EC:Hospital")){
                           Hospital = propertyValue;
                       }
                       else if (propertyValue.property.equalsIgnoreCase("FM:EC:Franchise")) {
                           Franchise = propertyValue;
                       }

                       else if (propertyValue.property.equalsIgnoreCase("FM:EC:DistrictManager")){
                           DistrictManager = propertyValue;
                       }

                       else if (propertyValue.property.equalsIgnoreCase("FM:EC:DirectorAreaOps")) {
                           DirectorAreaOps = propertyValue;
                       }

                       else if (propertyValue.property.equalsIgnoreCase("FM:EC:LossPrevention")){
                           LossPrevention = propertyValue;
                       }

                       else if (propertyValue.property.equalsIgnoreCase("FM:EC:EMSSafetyMgmt")){
                           EMSSafetyMgmt = propertyValue;
                       }

                       else if (propertyValue.property.equalsIgnoreCase("FM:EC:AutomatedCommSystem(ACS)")){
                           AutomatedCommSystem = propertyValue;
                       }

                       else if (propertyValue.property.equalsIgnoreCase("FM:EC:SecurityDesk")){
                           SecurityDesk = propertyValue;
                       }
                   }
               }
               contancts.add(Fire);
               contancts.add(Police);
               contancts.add(RescueSquad);
               contancts.add(Hospital);
               contancts.add(WendysContacts);
               contancts.add(Franchise);
               contancts.add(DistrictManager);
               contancts.add(DirectorAreaOps);
               contancts.add(EMSSafetyMgmt);
               contancts.add(LossPrevention);
               contancts.add(AutomatedCommSystem);
               contancts.add(SecurityDesk);


                wcsd.setText(SCMTool.CheckString(SecurityDesk.value,"--"));
                wcsd_Container.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        QualificationData data = QualificationData.getInstance();
                        SOSData.setInstance(data.getSosData());
                        Intent intent = new Intent(getActivity(), SOSActivity.class);
                        Objects.requireNonNull(getActivity()).startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());

                    }
                });

               QualificationData.getInstance().getSosData().setPropertyValues(contancts);
               StoreProfile_Emergency_listviewAdapter adapter = new StoreProfile_Emergency_listviewAdapter(getContext(),contancts,getActivity());
               emergency_list.setAdapter(adapter);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            CheckSitePropertyValues();
            setupManagementTeamList();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void DisplayUnderMaintenanceMessage() {
        DialogFragment newFragment = UnderMaintenance_DialogFragment.newInstance();
        assert getFragmentManager() != null;
        newFragment.show(getActivity().getSupportFragmentManager(), "dialog");
    }

}
