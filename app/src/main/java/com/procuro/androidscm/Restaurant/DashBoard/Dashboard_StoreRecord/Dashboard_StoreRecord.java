package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_StoreRecord;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.procuro.androidscm.R;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.apimmdatamanagerlib.SMSDailyOpsPlan;
import com.procuro.apimmdatamanagerlib.SMSDailyOpsPlanSpeedOfServiceItem;
import com.procuro.apimmdatamanagerlib.SpeedOfServiceItemDaypartItems;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Dashboard_StoreRecord extends AppCompatActivity {
    Dashboard_StoreRecord_listviewAdapter adapter;
    ListView listView ;
    Button back;
    TextView title,date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard__store_record);

        back = findViewById(R.id.home);
        listView = findViewById(R.id.listView);
        title = findViewById(R.id.username);
        date = findViewById(R.id.date);


        setUpspeedOfService();
        setUpOnlick();

    }

    private void setUpOnlick(){
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    private void setUpspeedOfService(){

        ArrayList<SMSDailyOpsPlanSpeedOfServiceItem> speedOfServiceItems = SCMDataManager.getInstance().getDailyOpsPlanForm().speedOfService.speedOfServiceItems;



        try {
            if (speedOfServiceItems != null) {
                SimpleDateFormat format = new SimpleDateFormat("MM/dd/yy");
                date.setText(format.format(new Date()));
                title.setText(SCMDataManager.getInstance().getSelectedSite().getSite().sitename);
                adapter = new Dashboard_StoreRecord_listviewAdapter(this,speedOfServiceItems);
                listView.setAdapter(adapter);
            }else {
                ArrayList<SMSDailyOpsPlanSpeedOfServiceItem> items = new ArrayList<>();

                SMSDailyOpsPlanSpeedOfServiceItem greet = new SMSDailyOpsPlanSpeedOfServiceItem();
                greet.item = "Greet";
                greet.daypartItems = new ArrayList<>();
                items.add(greet);

                SMSDailyOpsPlanSpeedOfServiceItem window = new SMSDailyOpsPlanSpeedOfServiceItem();
                window.item = "Window";
                window.daypartItems = new ArrayList<>();
                items.add(window);

                SMSDailyOpsPlanSpeedOfServiceItem total_time = new SMSDailyOpsPlanSpeedOfServiceItem();
                total_time.item = "Total Time";
                total_time.daypartItems = new ArrayList<>();
                items.add(total_time);

                SMSDailyOpsPlanSpeedOfServiceItem cars = new SMSDailyOpsPlanSpeedOfServiceItem();
                cars.item = "# Cars";
                cars.daypartItems = new ArrayList<>();
                items.add(cars);

                SimpleDateFormat format = new SimpleDateFormat("MM/dd/yy");
                date.setText(format.format(new Date()));
                title.setText(SCMDataManager.getInstance().getSelectedSite().getSite().sitename);
                adapter = new Dashboard_StoreRecord_listviewAdapter(this,items);
                listView.setAdapter(adapter);

            }


        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
