package com.procuro.androidscm.Restaurant.DashBoard.Dashbord_VOC.CustomerCare;

import android.graphics.Bitmap;

import java.util.Date;

public class CustomerCareListData {
    int caseNumber;
    String caseType;
    String category;
    String categoryType;
    Date  date;
    Bitmap picture;
    String Status;
    String contactName;
    String contactNumber;
    String contactEmail;
    String contactMethod;
    String contactTime;
    String description;

    public CustomerCareListData(int caseNumber, String caseType, String category, String categoryType, Date date, Bitmap picture, String status, String contactName, String contactNumber, String contactEmail,
                                String contactMethod, String contactTime, String description) {
        this.caseNumber = caseNumber;
        this.caseType = caseType;
        this.category = category;
        this.categoryType = categoryType;
        this.date = date;
        this.picture = picture;
        Status = status;
        this.contactName = contactName;
        this.contactNumber = contactNumber;
        this.contactEmail = contactEmail;
        this.contactMethod = contactMethod;
        this.contactTime = contactTime;
        this.description = description;
    }

    public int getCaseNumber() {
        return caseNumber;
    }

    public void setCaseNumber(int caseNumber) {
        this.caseNumber = caseNumber;
    }

    public String getCaseType() {
        return caseType;
    }

    public void setCaseType(String caseType) {
        this.caseType = caseType;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(String categoryType) {
        this.categoryType = categoryType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Bitmap getPicture() {
        return picture;
    }

    public void setPicture(Bitmap picture) {
        this.picture = picture;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getContactMethod() {
        return contactMethod;
    }

    public void setContactMethod(String contactMethod) {
        this.contactMethod = contactMethod;
    }

    public String getContactTime() {
        return contactTime;
    }

    public void setContactTime(String contactTime) {
        this.contactTime = contactTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
