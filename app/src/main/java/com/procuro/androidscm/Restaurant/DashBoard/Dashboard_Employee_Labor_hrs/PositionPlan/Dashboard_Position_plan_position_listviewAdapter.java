package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.PositionPlan;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.DashBoard.Employee;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.DashboardKitchenData;
import com.procuro.apimmdatamanagerlib.PositionDataItems;
import com.procuro.apimmdatamanagerlib.SMSPositioningEmployeeScheduleAssignmentPositions;
import com.procuro.apimmdatamanagerlib.Schedule_Business_Site_Plan;
import com.procuro.apimmdatamanagerlib.User;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static com.procuro.androidscm.SCMTool.DaypartTimeToDate;


public class Dashboard_Position_plan_position_listviewAdapter extends BaseAdapter {

    // Declare Variables

    Context mContext;
    LayoutInflater inflater;
    private ArrayList<PositionDataItems> arraylist;
    private ArrayList<CustomUser>users;
    private PositionPlanData positionPlanData;

    public Dashboard_Position_plan_position_listviewAdapter(Context context, ArrayList<PositionDataItems> arraylist,ArrayList<CustomUser> users,
                                                            PositionPlanData positionPlanData) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;
        this.users = users;
        this.positionPlanData = positionPlanData;
    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return arraylist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {

        view = inflater.inflate(R.layout.position_plan_list_data, null);

        final PositionDataItems item = arraylist.get(position);

        TextView item_position = view.findViewById(R.id.position);
        TextView name = view.findViewById(R.id.name);
        final LinearLayout container = view.findViewById(R.id.container);
        final LinearLayout indicator = view.findViewById(R.id.indicator);


        SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd, yyyy hh:mm a");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        ArrayList<SMSPositioningEmployeeScheduleAssignmentPositions>filterdPosition = new ArrayList<>();

        Calendar start = DaypartTimeToDate(positionPlanData.getDaypart().start, positionPlanData.getCurretDate());
        Calendar end = DaypartTimeToDate(positionPlanData.getDaypart().end, positionPlanData.getCurretDate());

        end.add(Calendar.MINUTE,-1);
        if (positionPlanData.getDaypart().name.equalsIgnoreCase("6")) {
            end.add(Calendar.DATE, 1);
        }

        for (CustomUser user : users) {
            for (Schedule_Business_Site_Plan sched : user.getSchedules()) {

                if (item.position == sched.position){

                    final SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
                    timeFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));

                    Calendar startTime = DaypartTimeToDate(timeFormatter.format(sched.startTime), positionPlanData.getCurretDate());
                    Calendar endTime = DaypartTimeToDate(timeFormatter.format(sched.endTime), positionPlanData.getCurretDate());

                    if (SCMTool.isBetween(startTime.getTime(),endTime.getTime(),start.getTime())){
                        filterdPosition.add(getPositionPlanOfUser(user.getUser(),sched));

                    }else if (SCMTool.isBetween(startTime.getTime(),endTime.getTime(),end.getTime())){
                        filterdPosition.add(getPositionPlanOfUser(user.getUser(),sched));
                    }
                }
            }
        }


        try {
            item_position.setText(String.valueOf(item.position));
            SCMTool.CreateDashboardPositionBackground(item_position,"#"+item.color);

            final boolean[] expanded = {false};
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (expanded[0]){
                        expanded[0] = false;
                        container.setVisibility(View.GONE);
                        indicator.setRotation(0);
                    }else {
                        expanded[0] = true;
                        container.setVisibility(View.VISIBLE);
                        indicator.setRotation(180f);

                    }
                }
            });
            if (expanded[0]){
                container.setVisibility(View.VISIBLE);
                indicator.setRotation(180f);
            }else {
                container.setVisibility(View.GONE);
                indicator.setRotation(0);
            }

            if (filterdPosition.size()>0){
                name.setText(filterdPosition.get(0).employee);
                for (SMSPositioningEmployeeScheduleAssignmentPositions filteredposition :filterdPosition){
                    CreateChildPosition(parent,container,filteredposition);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return view;
    }

    private SMSPositioningEmployeeScheduleAssignmentPositions getPositionPlanOfUser(User user ,Schedule_Business_Site_Plan sched){

        SMSPositioningEmployeeScheduleAssignmentPositions position = new SMSPositioningEmployeeScheduleAssignmentPositions();
        position.employee = user.getFullName();
        position.position = sched.position;
        position.timeIn = sched.startTime;
        position.timeOut = sched.endTime;

        return position;

    }


    private void CreateChildPosition(ViewGroup parent, LinearLayout container,
                                     SMSPositioningEmployeeScheduleAssignmentPositions position){

        View view = LayoutInflater.from(mContext).inflate(R.layout.position_plan_position_child_list_data, parent, false);
        TextView time = view.findViewById(R.id.time);
        TextView name = view.findViewById(R.id.name);

        SimpleDateFormat format = new SimpleDateFormat("hh:mm a");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        StringBuilder timebuilder = new StringBuilder();
        timebuilder.append(format.format(position.timeIn));
        timebuilder.append(" - ");
        timebuilder.append(format.format(position.timeOut));

        try {
            name.setText(position.employee);
            time.setText(timebuilder.toString());

        }catch (Exception e){
            e.printStackTrace();
        }


        container.addView(view);
    }

    private void findPositonColorInKitchenLayout(TextView holder,int position){
        try {
            DashboardKitchenData dashboardKitchenData = SCMDataManager.getInstance().getDashboardKitchenData();
            ArrayList<PositionDataItems> positionDataItems = dashboardKitchenData.positionDataItems;
            if (positionDataItems!=null){

                for (PositionDataItems dataItems : positionDataItems){
                    if (position == dataItems.position){
                        SCMTool.CreateDashboardPositionBackground(holder,"#"+dataItems.color);
                        break;
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }


}

