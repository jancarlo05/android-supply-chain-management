package com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.HoursOfOperation;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.Calendar.CalendarEvent;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.StorePrep.StorePrepActivity;
import com.procuro.androidscm.Restaurant.Status.StatusActivity;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileData;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.DayOperationSchedule;
import com.procuro.apimmdatamanagerlib.HolidaySchedules;
import com.procuro.apimmdatamanagerlib.HoursOfOperation;
import com.procuro.apimmdatamanagerlib.Qualification;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.StorePrepData;
import com.procuro.apimmdatamanagerlib.StorePrepSchedule;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;

import io.github.douglasjunior.androidSimpleTooltip.OverlayView;
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

public class HoursOfOperationActivity extends AppCompatActivity {

    ListView listView,store_closure_list;
    HoursOfOperation_list_adapter adapter;
    private Button back,add_store_closure,edit_store_prep;
    private TextView name, checklist, fsl_report,open,close,store_closure_message,breakfast_start,breakfast_labor,lunch_start,lunch_labor;
    private ConstraintLayout open_container,close_container;
    private CheckBox isBreakfast,isOpen24Hours,isOpen7Days,isRestaurant,isTestKitchen;
    private LinearLayout root;


    public Date openTime;
    public Date closeTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hours_of_operation);

        listView = findViewById(R.id.day_listview);
        back = findViewById(R.id.home);
        name = findViewById(R.id.username);
        checklist = findViewById(R.id.checklist);
        fsl_report = findViewById(R.id.fsl_report);
        open_container = findViewById(R.id.open_container);
        open = findViewById(R.id.open);
        close_container = findViewById(R.id.close_container);
        close = findViewById(R.id.close);
        add_store_closure = findViewById(R.id.add);
        store_closure_list = findViewById(R.id.store_closure_list);
        store_closure_message = findViewById(R.id.store_closure_message);

        isBreakfast = findViewById(R.id.isBreakfast);
        isOpen24Hours = findViewById(R.id.isOpen24Hours);
        isOpen7Days = findViewById(R.id.isOpen7Days);
        isRestaurant = findViewById(R.id.isRestaurant);
        isTestKitchen = findViewById(R.id.isTestKitchen);
        root = findViewById(R.id.root);
        edit_store_prep = findViewById(R.id.edit_store_prep);
        breakfast_start = findViewById(R.id.breakfast_start);
        breakfast_labor = findViewById(R.id.breakfast_labor);
        lunch_start = findViewById(R.id.lunch_start);
        lunch_labor = findViewById(R.id.lunch_labor);


        setupOncclicks();

        getSiteInfo();

        getStoreClosureEvent();

        setupStoreClosureList();

        DisplayStoreOperationData();

        DisplayStorePrepData();

    }

    private void DisplayStorePrepData(){
        HoursOfOpetationData data = HoursOfOpetationData.getInstance();
        HoursOfOperation hoursOfOperation = data.getHoursOfOperation();

        StorePrepSchedule storePrepSchedule  =hoursOfOperation.storePrepSchedule;
        if (storePrepSchedule!=null){
            if (storePrepSchedule.breakfast!=null){
                for (StorePrepData prepData : storePrepSchedule.breakfast){
                    if (prepData.totalEmployees!=0){
                        breakfast_start.setText(prepData.time);
                        break;
                    }
                }
                for (StorePrepData prepData : storePrepSchedule.breakfast){
                    if (prepData.totalEmployees!=0){
                        breakfast_labor.setText(String.valueOf(prepData.totalEmployees));
                    }
                }
            }
        }

        if (storePrepSchedule!=null){
            if (storePrepSchedule.lunch!=null){
                for (StorePrepData prepData : storePrepSchedule.lunch){
                    if (prepData.totalEmployees!=0){
                        lunch_start.setText(prepData.time);
                        break;
                    }
                }
                for (StorePrepData prepData : storePrepSchedule.lunch){
                    if (prepData.totalEmployees!=0){
                        lunch_labor.setText(String.valueOf(prepData.totalEmployees));
                    }
                }
            }
        }
    }

    private void DisplayStoreOperationData(){
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("h:mm a");
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            HoursOfOpetationData data = HoursOfOpetationData.getInstance();
            HoursOfOperation hoursOfOperation = data.getHoursOfOperation();

            isBreakfast.setChecked(hoursOfOperation.isBreakfast);
            isOpen24Hours.setChecked(hoursOfOperation.isOpen24Hours);
            isOpen7Days.setChecked(hoursOfOperation.isOpen7Days);
            isRestaurant.setChecked(hoursOfOperation.isRestaurant);
            isTestKitchen.setChecked(hoursOfOperation.isTestKitchen);

            SCMTool.DisableView(isBreakfast,1);
            SCMTool.DisableView(isOpen24Hours,1);
            SCMTool.DisableView(isOpen7Days,1);
            SCMTool.DisableView(isRestaurant,1);
            SCMTool.DisableView(isTestKitchen,1);

            if (hoursOfOperation.openTime!=null){
                open.setText(dateFormat.format(hoursOfOperation.openTime));
                close.setText(dateFormat.format(hoursOfOperation.closeTime));
            }

            if (hoursOfOperation.closeTime!=null){
                close.setText(dateFormat.format(hoursOfOperation.closeTime));
            }

            if (hoursOfOperation.dayOperationSchedules!=null){
                setupListView(hoursOfOperation.dayOperationSchedules);
            }

            if (data.getEvents()==null){
                if (hoursOfOperation.holidaySchedules!=null){
                    ConvertHolidaysToCalendar(hoursOfOperation.holidaySchedules);
                }
            }else {
                setupStoreClosureList();
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setupListView(ArrayList<DayOperationSchedule>dayOperationSchedules) {
        try {
            adapter = new HoursOfOperation_list_adapter(this, dayOperationSchedules);
            listView.setAdapter(adapter);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void ConvertHolidaysToCalendar(ArrayList<HolidaySchedules>holidaySchedules){
        HoursOfOpetationData data = HoursOfOpetationData.getInstance();
        ArrayList<CalendarEvent> events = new ArrayList<>();
        if (holidaySchedules!=null){
            for (HolidaySchedules schedule : holidaySchedules){
                CalendarEvent event = new CalendarEvent();
                event.setName(schedule.holiday);
                event.setDate(schedule.date);
                event.setStartTime(schedule.openTime);
                event.setEndTime(schedule.closeTime);
                event.setAllday(schedule.isAllDay);
                events.add(event);
            }
        }
        data.setEvents(events);

        setupStoreClosureList();
    }

    private void setupStoreClosureList(){
        HoursOfOpetationData data = HoursOfOpetationData.getInstance();
        if (data.getEvents()!=null){
            if (data.getEvents().size()>0){
                StoreClosure_list_adapter adapter = new StoreClosure_list_adapter(HoursOfOperationActivity.this,data.getEvents());
                store_closure_list.setAdapter(adapter);
                store_closure_message.setVisibility(View.GONE);
            }else {
                store_closure_message.setVisibility(View.VISIBLE);
            }
        }else {
            store_closure_message.setVisibility(View.VISIBLE);
        }
    }

    private void getSiteInfo() {
        try {
            QualificationData data = QualificationData.getInstance();
            Site site = data.getSelectedSite().getSite();
            name.setText(site.sitename);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupOncclicks() {

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        fsl_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Display_numpad(getApplicationContext(), fsl_report, fsl_report);
            }
        });

        checklist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Display_numpad(getApplicationContext(), checklist, checklist);
            }
        });

        edit_store_prep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HoursOfOperationActivity.this, StorePrepActivity.class);
                Objects.requireNonNull(HoursOfOperationActivity.this).startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(HoursOfOperationActivity.this).toBundle());
            }
        });

//        open_container.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                DisplayTimePicker(getApplicationContext(),open_container,open);
//            }
//        });
//
//        close_container.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                DisplayTimePicker(getApplicationContext(),close_container,close);
//            }
//        });


        add_store_closure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayAddStoreClosureDialog(HoursOfOperationActivity.this);
            }
        });

    }



    public static void Display_numpad(final Context context, View anchor, final TextView textView) {

        LayoutInflater li = (LayoutInflater) LayoutInflater.from(context);
        View view = li.inflate(R.layout.dashboard_num_pad, null);

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.TOP)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .overlayOffset(0)
                .transparentOverlay(false)
                .arrowColor(Color.WHITE)
                .contentView(R.layout.dashboard_num_pad)
                .focusable(true)
                .highlightShape(OverlayView.HIGHLIGHT_SHAPE_RECTANGULAR)
                .build();
        tooltip.show();

        final Button save = tooltip.findViewById(R.id.temp_pad_save);
        final EditText temp_value = tooltip.findViewById(R.id.temp_value);
        Button button1 = tooltip.findViewById(R.id.button_1);
        Button button2 = tooltip.findViewById(R.id.button_2);
        Button button3 = tooltip.findViewById(R.id.button_3);
        Button button4 = tooltip.findViewById(R.id.button_4);
        Button button5 = tooltip.findViewById(R.id.button_5);
        Button button6 = tooltip.findViewById(R.id.button_6);
        Button button7 = tooltip.findViewById(R.id.button_7);
        Button button8 = tooltip.findViewById(R.id.button_8);
        Button button9 = tooltip.findViewById(R.id.button_9);
        Button button0 = tooltip.findViewById(R.id.button_0);
        final Button button_delete = tooltip.findViewById(R.id.textView7);
        temp_value.setCursorVisible(false);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (temp_value.getText().length() < 8) {
                    temp_value.append("1");
                } else {
                    Toast.makeText(context, "You have reached the maximum value", Toast.LENGTH_SHORT).show();
                }

            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (temp_value.getText().length() < 8) {
                    temp_value.append("2");
                } else {
                    Toast.makeText(context, "You have reached the maximum value", Toast.LENGTH_SHORT).show();
                }
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (temp_value.getText().length() < 8) {
                    temp_value.append("3");
                } else {
                    Toast.makeText(context, "You have reached the maximum value", Toast.LENGTH_SHORT).show();
                }
            }
        });
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (temp_value.getText().length() < 8) {
                    temp_value.append("4");
                } else {
                    Toast.makeText(context, "You have reached the maximum value", Toast.LENGTH_SHORT).show();
                }
            }
        });
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (temp_value.getText().length() < 8) {
                    temp_value.append("5");
                } else {
                    Toast.makeText(context, "You have reached the maximum value", Toast.LENGTH_SHORT).show();
                }
            }
        });

        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (temp_value.getText().length() < 8) {
                    temp_value.append("6");
                } else {
                    Toast.makeText(context, "You have reached the maximum value", Toast.LENGTH_SHORT).show();
                }
            }
        });

        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (temp_value.getText().length() < 8) {
                    temp_value.append("7");
                } else {
                    Toast.makeText(context, "You have reached the maximum value", Toast.LENGTH_SHORT).show();
                }
            }
        });

        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (temp_value.getText().length() < 8) {
                    temp_value.append("8");
                } else {
                    Toast.makeText(context, "You have reached the maximum value", Toast.LENGTH_SHORT).show();
                }
            }
        });

        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (temp_value.getText().length() < 8) {
                    temp_value.append("9");
                } else {
                    Toast.makeText(context, "You have reached the maximum value", Toast.LENGTH_SHORT).show();
                }
            }
        });

        button0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (temp_value.getText().length() < 8) {
                    temp_value.append("0");
                } else {
                    Toast.makeText(context, "You have reached the maximum value", Toast.LENGTH_SHORT).show();
                }
            }
        });

        button_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (temp_value.getText().length() > 0) {
                    String text = temp_value.getText().toString();
                    temp_value.setText(text.substring(0, text.length() - 1));
                }
            }
        });


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (temp_value.length()>0){
                    int value = Integer.parseInt(temp_value.getText().toString());
                    textView.setText(String.valueOf(value));
                }
                tooltip.dismiss();

            }
        });
    }

    public void DisplayTimePicker(final Context context, View anchor,final TextView textView) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.BOTTOM)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .arrowColor(ContextCompat.getColor(context, R.color.white))
                .transparentOverlay(false)
                .contentView(R.layout.time_picker)
                .focusable(true)
                .overlayOffset(0)
                .highlightShape(OverlayView.HIGHLIGHT_SHAPE_RECTANGULAR)
                .build();
        tooltip.show();


        final TimePicker timePicker = tooltip.findViewById(R.id.datepicker);
        Button accept = tooltip.findViewById(R.id.apply);


        final SimpleDateFormat dateFormat = new SimpleDateFormat("h:mm a");

        timePicker.setIs24HourView(false);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar date = Calendar.getInstance();
                textView.setText(SCMTool.getTime(timePicker));
                tooltip.dismiss();
            }
        });

    }

    public void DisplayAddStoreClosureDialog(final Context context) {

        final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(context);
        LayoutInflater li = LayoutInflater.from(context);
        View view = li.inflate(R.layout.add_store_closure_popup, null);
        alertDialogBuilder.setView(view);

        final android.app.AlertDialog dialog = alertDialogBuilder.create();

        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.80);
        int height = (int)(getResources().getDisplayMetrics().heightPixels*0.60);

        Objects.requireNonNull(dialog.getWindow()).setLayout(width,height);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(true);
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        dialog.show();


        final ConstraintLayout descriptionContainer = view.findViewById(R.id.description_container);
        final TextView description = view.findViewById(R.id.description);
        final CheckBox allday = view.findViewById(R.id.allday);
        final TextView start = view.findViewById(R.id.start);
        final TextView end = view.findViewById(R.id.end);
        Button add = view.findViewById(R.id.add);

        final HoursOfOpetationData data = HoursOfOpetationData.getInstance();
        final CalendarEvent event = data.getNewEvent();

        final SimpleDateFormat Dateformat = new SimpleDateFormat("MMM dd, yyyy");
        final SimpleDateFormat DateTimeformat = new SimpleDateFormat("MMM dd, yyyy h:mm a");
        try {


            description.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int startcount, int before, int count) {
                    if (s.length()>0){
                        EnableView(allday);
                        EnableView(start);
                        EnableView(end);
                    }else {
                        DisableView(allday);
                        DisableView(start);
                        DisableView(end);
                    }
                }
                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            allday.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    HoursOfOpetationData data1 = HoursOfOpetationData.getInstance();
                    CalendarEvent event1 = data1.getNewEvent();

                    if (event1!=null){
                        event1.setAllday(isChecked);
                        CheckAllDayEvent(start,end,event1,Dateformat,DateTimeformat,allday);
                    }
                }
            });

            descriptionContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DisplayStoreCLosureEventPopup(context,description,start,end,allday);
                }
            });

            if (event!=null){
                description.setText(event.getName());
                CheckAllDayEvent(start,end,event,Dateformat,DateTimeformat,allday);
            }else {
                DisableView(allday);
                DisableView(start);
                DisableView(end);
            }


            add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final HoursOfOpetationData data = HoursOfOpetationData.getInstance();
                    if (data.getNewEvent()!=null){
                        if (data.getEvents()==null){
                            data.setEvents(new ArrayList<CalendarEvent>());
                            data.getEvents().add(data.getNewEvent());
                        }else {
                            data.getEvents().add(data.getNewEvent());
                        }
                        data.setNewEvent(null);
                        setupStoreClosureList();
                    }

                    dialog.dismiss();
                }
            });

            start.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DisplayDateTimePicker(HoursOfOperationActivity.this,start);
                }
            });

            end.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DisplayDateTimePicker(HoursOfOperationActivity.this,end);
                }
            });



        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private void CheckAllDayEvent(TextView start,TextView end,CalendarEvent event,
                                  SimpleDateFormat Dateformat,
                                  SimpleDateFormat DateTimeformat,
                                  CheckBox allday){
        if (event!=null){
            if (event.isAllday()){
                allday.setChecked(true);
                if (event.getDate()!=null){
                    start.setText(Dateformat.format(event.getDate()));
                    end.setText(Dateformat.format(event.getDate()));
                }
            }else {
                allday.setChecked(false);
                if (event.getStartTime()!=null){
                    start.setText(DateTimeformat.format(event.getStartTime()));
                }else {
                    start.setText("");
                }

                if (event.getEndTime()!=null){
                    end.setText(DateTimeformat.format(event.getEndTime()));
                }else {
                    end.setText("");
                }
            }
        }
    }

    private void DisableView(View view){
        view.setAlpha(.5f);
        view.setEnabled(false);
    }



    private void EnableView(View view){
        view.setAlpha(1);
        view.setEnabled(true);

    }

    public void DisplayStoreCLosureEventPopup(final Context context,final TextView description,TextView start,TextView end,CheckBox allday) {

        final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(context);
        LayoutInflater li = LayoutInflater.from(context);
        View view = li.inflate(R.layout.store_closure_description_list_popup, null);
        alertDialogBuilder.setView(view);

        android.app.AlertDialog dialog = alertDialogBuilder.create();

        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.80);
        int height = (int)(getResources().getDisplayMetrics().heightPixels*0.60);

        Objects.requireNonNull(dialog.getWindow()).setLayout(width,height);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(true);
        dialog.getWindow().getAttributes().gravity = Gravity.TOP;
        dialog.show();


        try {
            ExpandableListView listView = view.findViewById(R.id.expandible_listview);
            StoreClosure_Expandable_listadapter adapter = new StoreClosure_Expandable_listadapter(this,
                    HoursOfOpetationData.getInstance().getStoreClosureEvents(), description, dialog,start,end,allday);

            listView.setDividerHeight(1);
            listView.setAdapter(adapter);

            SCMTool.ExpandListview(adapter.getGroupCount(),listView);

        }catch (Exception e){
            e.printStackTrace();
        }


    }



    public void DisplayDateTimePicker(final Context context,final TextView textView) {

        final android.app.AlertDialog.Builder alertDialogBuilder = new android.app.AlertDialog.Builder(context);
        LayoutInflater li = LayoutInflater.from(context);
        View view = li.inflate(R.layout.date_time_picker, null);
        alertDialogBuilder.setView(view);

        final android.app.AlertDialog dialog = alertDialogBuilder.create();

        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.80);
        int height = (int)(getResources().getDisplayMetrics().heightPixels*0.60);

        Objects.requireNonNull(dialog.getWindow()).setLayout(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(true);
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        dialog.show();

        final TimePicker timePicker = view.findViewById(R.id.timepicker);
        final Button accept = view.findViewById(R.id.apply);
        final DatePicker datePicker = view.findViewById(R.id.datepicker);

        final SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy h:mm a");

        timePicker.setDescendantFocusability(TimePicker.FOCUS_BLOCK_DESCENDANTS);
        datePicker.setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);

        final HoursOfOpetationData data = HoursOfOpetationData.getInstance();

        if(textView.getId() == R.id.end){
            datePicker.setMinDate(data.getNewEvent().getStartTime().getTime());
        }

        timePicker.setIs24HourView(false);


        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());

                calendar.set(Calendar.MINUTE, timePicker.getMinute());
                if (timePicker.getHour()<12){
                    calendar.set(Calendar.AM_PM, Calendar.AM);
                    calendar.set(Calendar.HOUR, timePicker.getHour());
                }else {
                    calendar.set(Calendar.AM_PM, Calendar.PM);
                    calendar.set(Calendar.HOUR, timePicker.getHour()-12);
                }

                if (textView.getId() == R.id.start){

                    if (data.getNewEvent().getEndTime().after(calendar.getTime()) ||
                            data.getNewEvent().getStartTime().equals(calendar.getTime())){

                        data.getNewEvent().setStartTime(calendar.getTime());
                        textView.setText(dateFormat.format(data.getNewEvent().getStartTime()));
                        dialog.dismiss();

                    }else {
                        SCMTool.DisplaySnackbar("Choose Date Before End Date",root);
                    }

                }else {

                    if (data.getNewEvent().getStartTime().before(calendar.getTime()) ||
                            data.getNewEvent().getStartTime().equals(calendar.getTime())){

                        data.getNewEvent().setEndTime(calendar.getTime());
                        textView.setText(dateFormat.format(data.getNewEvent().getEndTime()));

                        dialog.dismiss();

                    }else {
                        SCMTool.DisplaySnackbar("Choose Date Before End Date",root);
                    }
                }

            }
        });

    }

    private void getStoreClosureEvent(){
        HoursOfOpetationData data = HoursOfOpetationData.getInstance();
        if (data.getStoreClosureEvents()==null){
            data.setStoreClosureEvents(createStaticEvents());
        }
    }

    private ArrayList<StoreClosureEvent> createStaticEvents(){

            Calendar calendar = Calendar.getInstance();
            ArrayList<StoreClosureEvent>static_events = new ArrayList<>();
            ArrayList<CalendarEvent>holidays = new ArrayList<>();
            calendar.set(Calendar.DATE,1);
            calendar.set(Calendar.MONTH,0);
            holidays.add(new CalendarEvent(calendar.getTime(),"New Year’s Day","Holidays"));

            calendar.set(Calendar.DATE,20);
            calendar.set(Calendar.MONTH,Calendar.JANUARY);
            holidays.add(new CalendarEvent(calendar.getTime(),"Birthday of Martin Luther King, Jry","Holidays"));

            calendar.set(Calendar.DATE,17);
            calendar.set(Calendar.MONTH,Calendar.FEBRUARY);
            holidays.add(new CalendarEvent(calendar.getTime(),"Washington’s Birthday","Holidays"));

            calendar.set(Calendar.DATE,25);
            calendar.set(Calendar.MONTH,Calendar.MAY);
            holidays.add(new CalendarEvent(calendar.getTime(),"Memorial Day","Holidays"));

            calendar.set(Calendar.DATE,3);
            calendar.set(Calendar.MONTH,Calendar.JULY);
            holidays.add(new CalendarEvent(calendar.getTime(),"Independence Day","Holidays"));


            calendar.set(Calendar.DATE,7);
            calendar.set(Calendar.MONTH,Calendar.SEPTEMBER);
            holidays.add(new CalendarEvent(calendar.getTime(),"Labor Day","Holidays"));

            calendar.set(Calendar.DATE,12);
            calendar.set(Calendar.MONTH,Calendar.OCTOBER);
            holidays.add(new CalendarEvent(calendar.getTime(),"Columbus Day","Holidays"));

            calendar.set(Calendar.DATE,11);
            calendar.set(Calendar.MONTH,Calendar.NOVEMBER);
            holidays.add(new CalendarEvent(calendar.getTime(),"Veterans Day","Holidays"));


            calendar.set(Calendar.DATE,26);
            calendar.set(Calendar.MONTH,Calendar.NOVEMBER);
            holidays.add(new CalendarEvent(calendar.getTime(),"Thanksgiving Day","Holidays"));

            calendar.set(Calendar.DATE,25);
            calendar.set(Calendar.MONTH,Calendar.DECEMBER);
            holidays.add(new CalendarEvent(calendar.getTime(),"Christmas Day","Holidays"));

            static_events.add(new StoreClosureEvent("Holidays",holidays));

            ArrayList<CalendarEvent>StoreClosure = new ArrayList<>();
            StoreClosure.add(new CalendarEvent("Building Repair / Remodeling","StoreClosure"));
            StoreClosure.add(new CalendarEvent("Health And Safety","StoreClosure"));
            StoreClosure.add(new CalendarEvent("Holiday","StoreClosure"));
            StoreClosure.add(new CalendarEvent("Weather","StoreClosure"));
            StoreClosure.add(new CalendarEvent("Other","StoreClosure"));
            static_events.add(new StoreClosureEvent("Store Closures",StoreClosure));


   return static_events;
    }

}
