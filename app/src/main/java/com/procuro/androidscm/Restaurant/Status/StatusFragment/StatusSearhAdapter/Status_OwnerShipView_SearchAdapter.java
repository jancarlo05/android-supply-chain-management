package com.procuro.androidscm.Restaurant.Status.StatusFragment.StatusSearhAdapter;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ExpandableListView;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSensor;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Area;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Chain;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_District;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Division;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Regional;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Qualification_Owner_Corporate_List_adapter;
import com.procuro.androidscm.Restaurant.Qualification.QualificationSearchAdapters.SearchDetails;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.Status.StatusActivity;
import com.procuro.androidscm.Restaurant.Status.StatusActivityFragment;
import com.procuro.androidscm.Restaurant.Status.StatusData;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.CustomPimmInstance;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.StatusFragment;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.Status_OwnershipView_List_adapter;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.PimmInstance;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Status_OwnerShipView_SearchAdapter extends ArrayAdapter<SearchDetails> {

    private ArrayList<Corp_Chain> corpchain ;
    private AutoCompleteTextView autoCompleteTextView;
    private ExpandableListView expandableListView;
    private FragmentActivity fragmentActivity;
    private ArrayList<Corp_Chain>SelectedCorpchain = new ArrayList<>();
    private boolean populated = false;
    public static ProgressDialog progressDialog ;
    private com.procuro.apimmdatamanagerlib.aPimmDataManager dataManager;

    public Status_OwnerShipView_SearchAdapter(@NonNull Context context, @NonNull ArrayList<Corp_Chain> corp_chains,
                                              AutoCompleteTextView autoCompleteTextView,
                                              ExpandableListView expandableListView, FragmentActivity fragmentActivity) {
        super(context, 0, new ArrayList<SearchDetails>());
        this.corpchain = new ArrayList<>(corp_chains);
        this.autoCompleteTextView = autoCompleteTextView;
        this.expandableListView = expandableListView;
        this.fragmentActivity = fragmentActivity;
        progressDialog = new  ProgressDialog(getContext());

    }

    @Nullable
    @Override
    public SearchDetails getItem(int position) {
        return super.getItem(position);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return ChainFilter;
    }

    @SuppressLint("ViewHolder")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        convertView = LayoutInflater.from(getContext()).inflate(
                R.layout.drop_down_view, parent, false);

        final SearchDetails searchDetails = getItem(position);
        TextView category_name = convertView.findViewById(R.id.category_name);
        TextView description = convertView.findViewById(R.id.description);
        String title = "";


        try {

            category_name.setText(searchDetails.getName());
            description.setText(searchDetails.getDescription());


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    populated = true;
                    hide();
                    autoCompleteTextView.setText(searchDetails.getName());
                    autoCompleteTextView.dismissDropDown();
                    Status_OwnershipView_List_adapter adapter = new Status_OwnershipView_List_adapter(getContext(),getSelectedChain(searchDetails),fragmentActivity,false);
                    expandableListView.setAdapter(adapter);

                    for (int i = 0; i <adapter.getGroupCount() ; i++) {
                        expandableListView.expandGroup(i);
                    }


                }
            });



        }catch (Exception e){
            e.printStackTrace();
        }


        return convertView;
    }



    @Override
    public int getPosition(@Nullable SearchDetails item) {
        return super.getPosition(item);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    private Filter ChainFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();
            ArrayList<SearchDetails> searchDetails = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {

                if (populated){
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Qualification_Owner_Corporate_List_adapter adapter = new Qualification_Owner_Corporate_List_adapter(getContext(),corpchain,fragmentActivity);
                            expandableListView.setAdapter(adapter);

                            for (int i = 0; i <adapter.getGroupCount() ; i++) {
                                expandableListView.expandGroup(i);
                            }
                        }
                    });
                    populated = false;
                }

            } else {

                boolean isContain = false;

                String filterPattern = constraint.toString().toLowerCase().trim();
                for (Corp_Chain item : corpchain) {
                    if (item.getName().toLowerCase().contains(filterPattern)) {
                        for (SearchDetails details : searchDetails){
                            if (details.getName().equalsIgnoreCase(item.getName())){
                                isContain = true;
                                break;
                            }
                        }
                        if (!isContain){
                            searchDetails.add(new SearchDetails(item.getName(),"Chain"));
                        }

                        SearchRegion(item.getQualificationRegionals(),searchDetails,filterPattern);
                    }else {
                        SearchRegion(item.getQualificationRegionals(),searchDetails,filterPattern);
                    }
                }
            }

            results.values = searchDetails;
            results.count = searchDetails.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            addAll((ArrayList<SearchDetails>) results.values);
            notifyDataSetChanged();
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return super.convertResultToString(resultValue);
        }


    };


    private void SearchRegion (ArrayList<Corp_Regional> regionals,ArrayList<SearchDetails>searchDetails,String filterpattern){
        boolean isContain = false;
        if (regionals!=null){
            for (Corp_Regional regional : regionals){
                SearchDivision(regional.getQualificationDivisions(),searchDetails,filterpattern);
            }
        }
    }

    private void SearchDivision (ArrayList<Corp_Division>divisions ,ArrayList<SearchDetails>searchDetails,String filterpattern){
        boolean isContain = false;
        if (divisions!=null){
            for (Corp_Division division : divisions){
                SearchArea(division.getQualificationAreas(),searchDetails,filterpattern);
            }
        }
    }

    private void SearchArea (ArrayList<Corp_Area>areas ,ArrayList<SearchDetails>searchDetails,String filterpattern){
        boolean isContain = false;
        if (areas!=null){
            for (Corp_Area area : areas){
                if (area.getCorpStructures().name.toLowerCase().contains(filterpattern)){

                    for (SearchDetails details : searchDetails){
                        if (details.getName().equalsIgnoreCase(area.getCorpStructures().name)){
                            isContain = true;
                            break;
                        }
                    }
                    if (!isContain){
                        searchDetails.add(new SearchDetails(area.getCorpStructures().name,"Area"));
                    }

                    SearchDistrict(area.getQualificationDistricts(),searchDetails,filterpattern);
                }else {
                    SearchDistrict(area.getQualificationDistricts(),searchDetails,filterpattern);

                }
            }

        }
    }

    private void SearchDistrict (ArrayList<Corp_District>districts ,ArrayList<SearchDetails>searchDetails,String filterpattern){
        boolean isContain = false;
        if (districts!=null){
            for (Corp_District district : districts){
                if (district.getCorpStructures().name.toLowerCase().contains(filterpattern)){

                    for (SearchDetails details : searchDetails){
                        if (details.getName().equalsIgnoreCase(district.getCorpStructures().name)){
                            isContain = true;
                            break;
                        }
                    }
                    if (!isContain){
                        searchDetails.add(new SearchDetails(district.getCorpStructures().name,"District"));
                    }

                    SearchSite(district.getSites(),searchDetails,filterpattern);
                }else {
                    SearchSite(district.getSites(),searchDetails,filterpattern);
                }
            }

        }
    }

    private void SearchSite (ArrayList<CustomSite>sites ,ArrayList<SearchDetails>searchDetails,String filterpattern){
        boolean isContain = false;
        if (sites!=null){
            for (CustomSite site : sites) {
                if (site.getSite().sitename.toLowerCase().contains(filterpattern)) {

                    for (SearchDetails details : searchDetails){
                        if (details.getName().equalsIgnoreCase(site.getSite().sitename)){
                            isContain = true;
                            break;
                        }
                    }

                    if (!isContain){
                        searchDetails.add(new SearchDetails(site.getSite().sitename, "Store"));
                    }
                }
            }
        }
    }

    private ArrayList<Corp_Chain> getSelectedChain(SearchDetails details){

        SelectedCorpchain = new ArrayList<>();

        if (corpchain!=null){

            for (Corp_Chain corp_chain : corpchain){
                if (corp_chain.getName().equalsIgnoreCase(details.getName())){

                    if (corp_chain.getQualificationRegionals()!=null){
                        for (Corp_Regional regional : corp_chain.getQualificationRegionals()){
                            regional.setSelected(true);

                            if (regional.getQualificationDivisions()!=null){
                                for (Corp_Division division : regional.getQualificationDivisions()){

                                    if (regional.getQualificationDivisions().size()==1){
                                        division.setSelected(true);
                                    }else {
                                        division.setSelected(false);
                                    }

                                    if (division.getQualificationAreas()!=null){

                                        for (Corp_Area area : division.getQualificationAreas()){

                                            if (division.getQualificationAreas().size() == 1){
                                                area.setSelected(true);
                                            }else {
                                                area.setSelected(false);
                                            }

                                            if (area.getQualificationDistricts()!=null){
                                                for (Corp_District district : area.getQualificationDistricts()){
                                                    if (area.getQualificationDistricts().size()== 1){
                                                        district.setSelected(true);
                                                    }else {
                                                        district.setSelected(false);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    SelectedCorpchain.add(corp_chain);
                }else {
                    Corp_Chain corp_chain1 = new Corp_Chain();
                    corp_chain1.setName(corp_chain.getName());
                    corp_chain1.setSites(corp_chain.getSites());
                    corp_chain1.setQualificationRegionals(getSelectedRegion(corp_chain1,corp_chain.getQualificationRegionals(),details));
                }
            }
        }

        return SelectedCorpchain;
    }

    private ArrayList<Corp_Regional> getSelectedRegion(Corp_Chain chain,ArrayList<Corp_Regional>regionals,SearchDetails details){

        ArrayList<Corp_Regional>SelectedRegion = new ArrayList<>();
        ArrayList<Corp_Regional>corpRegionals = new ArrayList<>(regionals);

        try {
            for (Corp_Regional regional : corpRegionals){
                if (regional.getCorpStructures().name.equalsIgnoreCase(details.getName())){
                    regional.setSelected(true);

                    if (regional.getQualificationDivisions()!=null){

                        for (Corp_Division division : regional.getQualificationDivisions()){

                            if (regional.getQualificationDivisions().size()==1){
                                division.setSelected(true);
                            }else {
                                division.setSelected(false);
                            }

                            if (division.getQualificationAreas()!=null){

                                for (Corp_Area area : division.getQualificationAreas()){

                                    if (division.getQualificationAreas().size() == 1){
                                        area.setSelected(true);
                                    }else {
                                        area.setSelected(false);
                                    }

                                    if (area.getQualificationDistricts()!=null){
                                        for (Corp_District district : area.getQualificationDistricts()){
                                            if (area.getQualificationDistricts().size()== 1){
                                                district.setSelected(true);
                                            }else {
                                                district.setSelected(false);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    SelectedRegion.add(regional);
                    SelectedCorpchain.add(chain);
                }else {

                    Corp_Regional regional1 = new Corp_Regional();
                    regional1.setCorpStructures(regional.getCorpStructures());
                    regional1.setSelected(true);
                    regional1.setSiteLists(regional.getSiteLists());
                    regional1.setQualificationDivisions(getSelectedDivision(chain,regional.getQualificationDivisions(),details));
                    if (regional1.getQualificationDivisions().size()>0){
                        SelectedRegion.add(regional1);
                    }

                }
            }


        }catch (Exception e){
            e.printStackTrace();
        }


        return SelectedRegion;
    }

    private ArrayList<Corp_Division> getSelectedDivision(Corp_Chain chain,ArrayList<Corp_Division>divisions,SearchDetails details){
        ArrayList<Corp_Division>SelectedDivision = new ArrayList<>();
        ArrayList<Corp_Division>Corp_Division = new ArrayList<>(divisions);
        try {
            for (Corp_Division division : Corp_Division){
                if (division.getCorpStructures().name.equalsIgnoreCase(details.getName())){
                    division.setSelected(true);

                    if (division.getQualificationAreas()!=null){

                        for (Corp_Area area : division.getQualificationAreas()){

                            if (division.getQualificationAreas().size() == 1){
                                area.setSelected(true);
                            }else {
                                area.setSelected(false);
                            }

                            if (area.getQualificationDistricts()!=null){
                                for (Corp_District district : area.getQualificationDistricts()){
                                    if (area.getQualificationDistricts().size()== 1){
                                        district.setSelected(true);
                                    }else {
                                        district.setSelected(false);
                                    }
                                }
                            }
                        }
                    }

                    SelectedDivision.add(division);
                    SelectedCorpchain.add(chain);
                }else {

                    Corp_Division division1 = new Corp_Division();
                    division1.setSelected(true);
                    division1.setCorpStructures(division.getCorpStructures());
                    division1.setQualificationAreas(getSelectedArea(chain,division.getQualificationAreas(),details));
                    if (division1.getQualificationAreas().size()>0){
                        SelectedDivision.add(division1);
                    }

                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return SelectedDivision;
    }


    private ArrayList<Corp_Area> getSelectedArea(Corp_Chain chain,ArrayList<Corp_Area>areas,SearchDetails details){

        ArrayList<Corp_Area>SelectedArea = new ArrayList<>();
        ArrayList<Corp_Area>Corp_Area = new ArrayList<>(areas);
        try {
            for (Corp_Area area : Corp_Area){
                if (area.getCorpStructures().name.equalsIgnoreCase(details.getName())){
                    area.setSelected(true);

                    if (area.getQualificationDistricts()!=null){
                        for (Corp_District district : area.getQualificationDistricts()){
                            if (area.getQualificationDistricts().size()== 1){
                                district.setSelected(true);
                            }else {
                                district.setSelected(false);
                            }
                        }
                    }

                    SelectedArea.add(area);
                    SelectedCorpchain.add(chain);
                }else {

                    Corp_Area area1 = new Corp_Area();
                    area1.setSelected(true);
                    area1.setCorpStructures(area.getCorpStructures());
                    area1.setQualificationDistricts(getSelectedDistrict(chain,area.getQualificationDistricts(),details));
                    if (area1.getQualificationDistricts().size()>0){
                        SelectedArea.add(area1);
                    }

                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return SelectedArea;
    }


    private ArrayList<Corp_District> getSelectedDistrict(Corp_Chain chain,ArrayList<Corp_District>districts,SearchDetails details){
        ArrayList<Corp_District>SelectedDistrict = new ArrayList<>();
        ArrayList<Corp_District>Corp_District = new ArrayList<>(districts);
        try {
            for (Corp_District district : Corp_District){
                if (district.getCorpStructures().name.equalsIgnoreCase(details.getName())){
                    district.setSelected(true);

                    SelectedDistrict.add(district);
                    SelectedCorpchain.add(chain);
                }else {
                    Corp_District district1 = new Corp_District();
                    district1.setSelected(true);
                    district1.setCorpStructures(district.getCorpStructures());
                    district1.setSites(getSelectedStore(chain,district.getSites(),details));
                    if (district1.getSites().size()>0){
                        SelectedDistrict.add(district1);
                        SelectedCorpchain.add(chain);
                    }
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return SelectedDistrict;
    }


    private ArrayList<CustomSite> getSelectedStore(Corp_Chain chain,ArrayList<CustomSite>sites,SearchDetails details){
        ArrayList<CustomSite>SelectedSite = new ArrayList<>();
        ArrayList<CustomSite>CustomSite = new ArrayList<>(sites);
        try {
            for (CustomSite site : CustomSite){
                if (site.getSite().sitename.equalsIgnoreCase(details.getName())){
//                    SensorCategoryView(site);
                    SelectedSite.add(site);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return SelectedSite;
    }

    private void SensorCategoryView(final CustomSite site){

        progressDialog.setTitle("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Downloading Data ");
        progressDialog.show();

        dataManager = aPimmDataManager.getInstance();
        if (site.getPimmDevice()!=null) {
            dataManager.getInstanceListForSiteId(site.getSite().DeviceID, new OnCompleteListeners.getInstanceListForSiteIdCallbackListener() {
                @Override
                public void getInstanceListForSiteIdCallback(ArrayList<PimmInstance> pimmInstanceArrayList, Error error) {
                    if (error == null) {
                        if (pimmInstanceArrayList.size() > 0) {
                            FilterObjectNames(pimmInstanceArrayList,site);

                        }else {
                            setInstanceForNoTemperature(site);
                        }
                    }
                }
            });
        }
    }

    private void FilterObjectNames(ArrayList<PimmInstance> temperatureLists,CustomSite customSite){
        ArrayList<String>objectname = new ArrayList<>();
        ArrayList<CustomSensor>objects=  new ArrayList<>();
        for (PimmInstance newObjects: temperatureLists){
            if (!objectname.contains(newObjects.objectName)){
                objectname.add(newObjects.objectName);
                objects.add(new CustomSensor(newObjects.objectName));
                System.out.println("Description: "+newObjects.description);
            }
        }

        PopulateInstanceByObjectName(temperatureLists,objects,customSite);
    }

    private void PopulateInstanceByObjectName(ArrayList<PimmInstance>pimmInstances,
                                              ArrayList<CustomSensor>newObjects,CustomSite customSite){
        for (PimmInstance instance: pimmInstances){
            for (CustomSensor newObject: newObjects){
                if (instance.objectName.equalsIgnoreCase(newObject.getTitle())){
                    if (newObject.getObject() == null){
                        ArrayList<PimmInstance>newObs = new ArrayList<>();
                        newObs.add(instance);
                        newObject.setObject(newObs);
                    }else {
                        newObject.getObject().add(instance);
                    }
                }
            }
        }
        PopulateInstanceByCoolsersAndFreezers(newObjects,customSite);
    }

    private void PopulateInstanceByCoolsersAndFreezers(ArrayList<CustomSensor>sensors,CustomSite customSite){

        for (CustomSensor sensor : sensors){
            if (sensor.getTitle().equalsIgnoreCase("Temperature")) {

                ArrayList<CustomPimmInstance> customPimmInstances = new ArrayList<>();
                ArrayList<String> titles = new ArrayList<>();
                if (sensor.getObject() != null) {
                    for (PimmInstance instance : sensor.getObject()) {
                        if (instance.description.toLowerCase().contains("cooler")) {
                            if (!titles.contains("Coolers")) {
                                titles.add("Coolers");
                                ArrayList<PimmInstance> instances = new ArrayList<>();
                                instances.add(instance);
                                customPimmInstances.add(new CustomPimmInstance("Coolers", instances));

                            } else {
                                for (CustomPimmInstance pimmInstance : customPimmInstances) {
                                    if (pimmInstance.getTitle().equalsIgnoreCase("Coolers")) {
                                        pimmInstance.getInstances().add(instance);
                                    }
                                }
                            }
                        } else if (instance.description.toLowerCase().contains("freezer")) {

                            if (!titles.contains("Freezers")) {
                                titles.add("Freezers");
                                ArrayList<PimmInstance> instances = new ArrayList<>();
                                instances.add(instance);
                                customPimmInstances.add(new CustomPimmInstance("Freezers", instances));
                                System.out.println("Freezer: "+instance.description);

                            } else {
                                for (CustomPimmInstance pimmInstance : customPimmInstances) {
                                    if (pimmInstance.getTitle().equalsIgnoreCase("Freezers")) {
                                        pimmInstance.getInstances().add(instance);
                                        System.out.println("Freezer: "+instance.description);
                                    }
                                }
                            }
                        }
                    }
                }


                Collections.sort(customPimmInstances, new Comparator<CustomPimmInstance>() {
                    @Override
                    public int compare(CustomPimmInstance o1, CustomPimmInstance o2) {
                        return o1.getTitle().compareToIgnoreCase(o2.getTitle());
                    }
                });

                for (CustomPimmInstance pimmInstance : customPimmInstances){
                    getTemperatureLevel(pimmInstance,sensors,customSite);
                    break;
                }
            }
        }
    }

    private void getTemperatureLevel(final CustomPimmInstance pimmInstance, ArrayList<CustomSensor>customSensors,CustomSite customSite){

        if (pimmInstance.getInstances()!=null){
            for (PimmInstance sensors :  pimmInstance.getInstances()){
                setInstanceForTemperature(customSite,sensors);
                break;
            }
        }
    }

    private void setInstanceForTemperature(CustomSite site, PimmInstance pimmInstance){

        StatusData data = new StatusData();
        data.setSelectedSite(site);
        data.setAllStoreEnabled(true);
        data.setSelectedSite(site);
        data.setPimmDevice(site.getPimmDevice());
        data.setPimmInstance(pimmInstance);
        data.setTemperatureViewEnabled(true);
        StatusData.setInstance(data);
//        Intent intent = new Intent(fragmentActivity, StatusActivity.class);
//        fragmentActivity.startActivity(intent);
        fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.resturant_fragment_container,
                new StatusActivityFragment()).commit();

    }

    private void setInstanceForNoTemperature(CustomSite site){

        StatusData data = new StatusData();
        data.setSelectedSite(site);
        data.setAllStoreEnabled(true);
        data.setSelectedSite(site);
        data.setPimmDevice(site.getPimmDevice());
        data.setTemperatureViewEnabled(true);
        StatusData.setInstance(data);
//        Intent intent = new Intent(fragmentActivity, StatusActivity.class);
//        fragmentActivity.startActivity(intent);
        fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.resturant_fragment_container,
                new StatusActivityFragment()).commit();

    }

    public void hide() {
        View view = fragmentActivity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) fragmentActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
