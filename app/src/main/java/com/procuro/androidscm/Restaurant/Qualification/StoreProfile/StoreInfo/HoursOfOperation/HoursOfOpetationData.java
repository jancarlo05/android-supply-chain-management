package com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.HoursOfOperation;

import com.procuro.androidscm.Restaurant.Calendar.CalendarEvent;
import com.procuro.apimmdatamanagerlib.HoursOfOperation;
import com.procuro.apimmdatamanagerlib.JSONDate;
import com.procuro.apimmdatamanagerlib.PimmBaseObject;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsConfiguration;

import java.util.ArrayList;
import java.util.Date;

public class HoursOfOpetationData  {


    private static HoursOfOpetationData instance = new HoursOfOpetationData();

    private ArrayList<StoreClosureEvent> storeClosureEvents;

    private CalendarEvent NewEvent;

    private ArrayList<CalendarEvent> events;

    private HoursOfOperation hoursOfOperation;


    public static HoursOfOpetationData getInstance() {
        if(instance == null) {
            instance = new HoursOfOpetationData();
        }
        return instance;
    }

    public HoursOfOperation getHoursOfOperation() {
        return hoursOfOperation;
    }

    public void setHoursOfOperation(HoursOfOperation hoursOfOperation) {
        this.hoursOfOperation = hoursOfOperation;
    }

    public ArrayList<CalendarEvent> getEvents() {
        return events;
    }

    public void setEvents(ArrayList<CalendarEvent> events) {
        this.events = events;
    }

    public static void setInstance(HoursOfOpetationData instance) {
        HoursOfOpetationData.instance = instance;
    }

    public CalendarEvent getNewEvent() {
        return NewEvent;
    }

    public void setNewEvent(CalendarEvent newEvent) {
        NewEvent = newEvent;
    }

    public ArrayList<StoreClosureEvent> getStoreClosureEvents() {
        return storeClosureEvents;
    }

    public void setStoreClosureEvents(ArrayList<StoreClosureEvent> storeClosureEvents) {
        this.storeClosureEvents = storeClosureEvents;
    }

    public boolean isBreakfastEnabled(){
        return hoursOfOperation.isBreakfast;
    }
}
