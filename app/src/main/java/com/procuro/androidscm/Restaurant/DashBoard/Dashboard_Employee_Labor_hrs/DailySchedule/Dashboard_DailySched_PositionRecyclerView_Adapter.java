package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.DailySchedule;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.apimmdatamanagerlib.DashboardKitchenData;
import com.procuro.apimmdatamanagerlib.PositionDataItems;
import com.procuro.apimmdatamanagerlib.SMSPositioning;
import com.procuro.apimmdatamanagerlib.SMSPositioningEmployeeScheduleAssignment;
import com.procuro.apimmdatamanagerlib.SMSPositioningEmployeeScheduleAssignmentPositions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;


public class Dashboard_DailySched_PositionRecyclerView_Adapter extends RecyclerView.Adapter<Dashboard_DailySched_PositionRecyclerView_Adapter.MyViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<String> arraylist;


    public Dashboard_DailySched_PositionRecyclerView_Adapter(Context context, ArrayList<String> arraylist) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.daily_sched_position_list_data,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        if (arraylist.size()>0){
            try {

                final String StringPosition = arraylist.get(position);
                int IntPosition = Integer.parseInt(StringPosition);
                holder.position.setText(StringPosition);
                findPositonColorInKitchenLayout(holder.position,IntPosition);

            }catch (Exception e){
                e.printStackTrace();
            }

        }

    }

    @Override
    public int getItemCount() {
        if (arraylist.size()>0){
            return Math.min(arraylist.size(), 2);
        }else {
            return 1;
        }
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView  position;


        public MyViewHolder(View itemView) {
            super(itemView);
            position = itemView.findViewById(R.id.position);

        }
    }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    private TextView  CreatePositionBackground(TextView imageView, String backgroundColor) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setCornerRadii(new float[] { dpToPx(5), dpToPx(5), dpToPx(5), dpToPx(5), dpToPx(5), dpToPx(5), dpToPx(5), dpToPx(5) });
        shape.setColor(Color.parseColor(backgroundColor));
        imageView.setBackground(shape);
        return imageView;
    }


    private void findPositonColorInKitchenLayout(TextView holder,int position){
        try {
            DashboardKitchenData dashboardKitchenData = SCMDataManager.getInstance().getDashboardKitchenData();
            ArrayList<PositionDataItems> positionDataItems = dashboardKitchenData.positionDataItems;
            if (positionDataItems!=null){

                for (PositionDataItems dataItems : positionDataItems){
                    if (position == dataItems.position){
                        CreatePositionBackground(holder,"#"+dataItems.color);
                        break;
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }


}
