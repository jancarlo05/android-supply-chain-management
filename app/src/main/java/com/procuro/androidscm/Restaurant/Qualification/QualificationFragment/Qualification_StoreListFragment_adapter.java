package com.procuro.androidscm.Restaurant.Qualification.QualificationFragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.google.gson.Gson;
import com.procuro.androidscm.GlideApp;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.Qualification_Store_Profile_Fragment;
import com.procuro.androidscm.Restaurant.ScoreCard.ScoreCardFragment;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.StoreListFragment.SiteList;
import com.procuro.androidscm.SCMDataManager;

import java.util.ArrayList;


public class Qualification_StoreListFragment_adapter extends BaseExpandableListAdapter {

    final Context context ;
    ArrayList<SiteList> parent_lists;
    TextView storeName;
    FragmentActivity fragmentActivity;

    public Qualification_StoreListFragment_adapter(Context context, ArrayList<SiteList> data , FragmentActivity fragmentActivity) {
        this.context = context;
        this.parent_lists = data;
        this.fragmentActivity = fragmentActivity;

    }

    @Override
    public int getGroupCount() {
        return parent_lists.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return parent_lists.get(i).getCustomSites().size();
    }

    @Override
    public Object getGroup(int i) {
        return parent_lists.get(i);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return parent_lists.get(groupPosition).getSites().get(childPosition);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int position, boolean b, View contentView, ViewGroup parent) {
        SiteList parentList = parent_lists.get(position);
        contentView = LayoutInflater.from(context).inflate(R.layout.status_storelist_parent_data, parent,false);

        TextView name = contentView.findViewById(R.id.name);
        name.setText(parentList.getProvince());

        return contentView;
    }


    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean b, View contentView, final ViewGroup parent) {
        final CustomSite childlist = parent_lists.get(groupPosition).getCustomSites().get(childPosition);
        contentView = LayoutInflater.from(context).inflate(R.layout.status_storelist_child_data_with_scorecard, parent, false);

        ImageView icon = contentView.findViewById(R.id.icon);
        ImageView scorecardContainer = contentView.findViewById(R.id.score_container);
        TextView score = contentView.findViewById(R.id.score);
        TextView name = contentView.findViewById(R.id.name);
        contentView.setBackgroundResource(R.drawable.onpress_design);

        if (childlist.getPimmDevice()!=null){
            if (childlist.getPimmDevice().severity == 0 ){
                GlideApp.with(context).load(R.drawable.store_blue).into(icon);
            }else if (childlist.getPimmDevice().severity == 3 ){
                GlideApp.with(context).load(R.drawable.store_green).into(icon);
            }else if (childlist.getPimmDevice().severity == 5 ){
                GlideApp.with(context).load(R.drawable.store_yellow).into(icon);
            }else if (childlist.getPimmDevice().severity == 9 ){
                GlideApp.with(context).load(R.drawable.store_red).into(icon);
            }else {
                GlideApp.with(context).load(R.drawable.store_gray).into(icon);
                icon.setAlpha(.5f);
            }
        }else {
            GlideApp.with(context).load(R.drawable.store_gray).into(icon);
            icon.setAlpha(.5f);
        }

        GlideApp.with(context).load(R.drawable.empty_circle_orange).into(scorecardContainer);
        score.setText("A");
        name.setText(childlist.getSite().SiteName);

        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SCMDataManager.getInstance().setSelectedSite(childlist);
                setSharedpref(fragmentActivity);
                SCMDataManager.getInstance().setReportsSelectedSite(null);
                SCMDataManager.getInstance().setReportLevel("Site");
                displayStoreTemperature(fragmentActivity,childlist);
            }
        });

        scorecardContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DisplayScoreCard(fragmentActivity);
            }
        });


        return contentView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }


    private void displayStoreTemperature(FragmentActivity fragmentActivity, CustomSite storeList) {
        fragmentActivity.getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_left,R.anim.slide_left_out)
                .replace(R.id.resturant_fragment_container,
                new Qualification_Store_Profile_Fragment()).commit();
    }

    private void DisplayScoreCard(FragmentActivity fragmentActivity) {
        SCMDataManager.getInstance().setQualificationSelected(true);
        fragmentActivity.getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_left,R.anim.slide_left_out)
                .replace(R.id.resturant_fragment_container,
                new ScoreCardFragment()).commit();
    }

    private void setSharedpref(FragmentActivity fragmentActivity){
        SharedPreferences mypref = PreferenceManager.getDefaultSharedPreferences(fragmentActivity);
        SharedPreferences.Editor prefsEditor = mypref.edit();
        Gson gson = new Gson();
        String json = gson.toJson(SCMDataManager.getInstance());
        prefsEditor.putString("SCMDataManager", json);
        prefsEditor.commit();
    }
}