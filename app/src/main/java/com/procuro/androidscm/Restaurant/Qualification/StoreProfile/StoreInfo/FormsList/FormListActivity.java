package com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.FormsList;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.EquipmenList.EquipmenlistData;
import com.procuro.androidscm.Restaurant.Schema.Days;
import com.procuro.androidscm.Restaurant.Schema.Forms;
import com.procuro.androidscm.Restaurant.Schema.Schema;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.ApplicationReport;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmthriftservices.services.NamedServices;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsConfiguration;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsSiteConfig;

import org.apache.thrift.TException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class FormListActivity extends AppCompatActivity {

    private CustomSite site;
    private info.hoang8f.android.segmented.SegmentedGroup infogrp;
    private RadioButton food_safety, checklist;
    private Button back,save;
    private ExpandableListView listView ;
    private Formlist_Expandable_adapter adapter;
    private TextView message;
    private LinearLayout root;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qualification_formlist_activity);

        back = findViewById(R.id.home);
        food_safety = findViewById(R.id.food_safety);
        checklist = findViewById(R.id.checklist);
        infogrp = findViewById(R.id.segmented2);
        listView = findViewById(R.id.listView);
        message = findViewById(R.id.message);
        save = findViewById(R.id.save);
        root = findViewById(R.id.root);

        setupOnclicks();


        food_safety.setChecked(true);


        for (SiteSettingsConfiguration configuration : FormListData.getInstance().getSiteSettingsConfigurations()){
            if (configuration.name.toLowerCase().contains("Config:autofill".toLowerCase())){
                System.out.println(configuration.description());
            }
        }
    }


    private void setupOnclicks(){

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        infogrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                if (i == food_safety.getId()){
                    ArrayList<Days>foodSafety = new ArrayList<>();
                    foodSafety.addAll(Schema.getInstance().getFoodsafety());
                    populateSiteConfigtoForms(foodSafety);
                    DisplayFormList(foodSafety);

                }else if (i == checklist.getId()){
                    ArrayList<Days>checklist = new ArrayList<>();
                    checklist.addAll(Schema.getInstance().getChecklist());
                    populateSiteConfigtoForms(checklist);
                    DisplayFormList(checklist);
                }
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Save();
            }
        });
    }

    private void DisplayFormList(ArrayList<Days>formslist){
        try {
            if (formslist.size()>0){
                adapter = new Formlist_Expandable_adapter(this,formslist,save);
                listView.setAdapter(adapter);
                SCMTool.ExpandListview(formslist.size(),listView);
                message.setVisibility(View.GONE);
            }else {
                message.setVisibility(View.INVISIBLE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void populateSiteConfigtoForms(ArrayList<Days> arrayList){
        try {
            if (arrayList!=null){
                for (Days days :arrayList){
                    for (Forms form : days.getForms()){
                        FormListData data = FormListData.getInstance();
                        if (data.getSiteSettingsConfigurations()!=null){
                            for (SiteSettingsConfiguration config : data.getSiteSettingsConfigurations()){
                                if (config.name.contains("Config:autofill:")){
                                    if (form.formName.toLowerCase().contains("ccp")){
                                        if (config.name.equalsIgnoreCase("Config:autofill:ccp")){
                                            form.setConfig(new CustomSiteSettingsConfiguration(config));
                                        }
                                    }else{
                                        String[] formname = config.name.split(":");
                                        if (formname[2].equalsIgnoreCase(form.formName)){
                                            if (config.name.toLowerCase().contains(form.formName.toLowerCase())){
                                                form.setConfig(new CustomSiteSettingsConfiguration(config));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                for (Days days :arrayList){
                    boolean isLtoPopulated = false;
                    if (days.section.equalsIgnoreCase("Product Temps")){
                        if (days.getForms()!=null) {
                            for (Forms form : days.getForms()) {
                                if (form.formName.equalsIgnoreCase("lto")) {
                                    isLtoPopulated = true;
                                    break;
                                }
                            }
                            if (!isLtoPopulated){
                                days.getForms().add(AddCustomFormLto());
                            }
                            break;
                        }
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private ArrayList getAllUpdatedSiteCOnfig(){
        ArrayList<SiteSettingsConfiguration> siteSettingsConfigurations = new ArrayList<>();
        ArrayList<Days> foodsafety =  Schema.getInstance().getFoodsafety();
        try {
            if (foodsafety!=null){
                for (Days days :foodsafety){
                    for (Forms form : days.getForms()){
                        if (form.getConfig()!=null){
                            if (form.getConfig().isUpdated()){
                                siteSettingsConfigurations.add(form.getConfig().getSiteSettingsConfiguration());
                                form.getConfig().setUpdated(false);
                            }
                        }
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        ArrayList<Days> checklist =  Schema.getInstance().getChecklist();
        try {
            if (checklist!=null){
                for (Days days :checklist){
                    for (Forms form : days.getForms()){
                        if (form.getConfig()!=null){
                            if (form.getConfig().isUpdated()){
                                siteSettingsConfigurations.add(form.getConfig().getSiteSettingsConfiguration());
                                form.getConfig().setUpdated(false);
                            }
                        }
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return siteSettingsConfigurations;
    }

    private void Save(){

        final ProgressDialog progressDialog = new ProgressDialog(FormListActivity.this);
        progressDialog.setTitle("Please Wait");
        progressDialog.setMessage("Updating Data");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Site site =  QualificationData.getInstance().getSelectedSite().getSite();
        ArrayList siteconfig = getAllUpdatedSiteCOnfig();

        if (siteconfig.size()>0){
            saveDefaultSiteSettingConfiguration(siteconfig,progressDialog,site);
        }
    }

    private Forms AddCustomFormLto(){
        Forms form = new Forms();
        form.formName = "lto";
        form.title = "Limited Time Offers";
        return form;
    }

    private void saveDefaultSiteSettingConfiguration(final ArrayList arrayList, final ProgressDialog progressDialog, final Site site) {

        Log.i("Thrift ","saveDefaultSiteSettingConfiguration Start");
        try {
            final NamedServices ns = NamedServices.getInstance();
            if (ns.pimmServer!=null){
                final SiteSettingsSiteConfig config = ns.siteSettingsClient;

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            for (int i = 0; i <arrayList.size() ; i++) {
                                SiteSettingsConfiguration configuration = (SiteSettingsConfiguration)arrayList.get(i);
                                System.out.println("UPDATED SITE CONFIG  : "+configuration.description());
                            }

                            boolean success = config.updateConfiguration(site.siteid,arrayList);

                            if (success){
                                SCMTool.DismissDialogInThrift(progressDialog,save);
//                                SCMTool.DisplaySnackbar("Updates Saved",root);
                                FormListData.getInstance().setUnsaveEdit(false);
                                Handler handler = new Handler(Looper.getMainLooper());
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        finish();
                                    }
                                });

                            }else {
                                EquipmenlistData.getInstance().setUnsaveEdit(true);
                                SCMTool.DismissDialogInThrift(progressDialog,null);
                                SCMTool.DisplaySnackbar("Update Failed : Please Check Internet Connection",root);

                            }

                        } catch (TException e) {
                            e.printStackTrace();
                            SCMTool.DisplaySnackbar("Update Failed : Please Check Internet Connection",root);
                            SCMTool.DismissDialogInThrift(progressDialog,null);
                        }
                    }
                }).start();

            }else {
                Log.i("Thrift ","SERVER NULL ");
                SCMTool.DismissDialogInThrift(progressDialog,null);
                SCMTool.DisplaySnackbar("Update Failed : Please Check Internet Connection",root);
            }

            Log.i("Thrift ","saveDefaultSiteSettingEquipment End");

        }catch (TException e){
            e.printStackTrace();
            SCMTool.DismissDialogInThrift(progressDialog,null);
            SCMTool.DisplaySnackbar("Update Failed : Please Check Internet Connection",root);
        }
    }

}
