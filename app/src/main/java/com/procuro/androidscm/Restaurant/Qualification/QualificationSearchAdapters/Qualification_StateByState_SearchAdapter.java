package com.procuro.androidscm.Restaurant.Qualification.QualificationSearchAdapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ExpandableListView;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Regional;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Qualification_SiteView_List_adapter;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.StoreListFragment.SiteList;

import java.util.ArrayList;

public class Qualification_StateByState_SearchAdapter extends ArrayAdapter<SearchDetails> {


    private AutoCompleteTextView autoCompleteTextView;
    private ExpandableListView expandableListView;
    private FragmentActivity fragmentActivity;

    private ArrayList<Corp_Regional> regionals ;
    private ArrayList<Corp_Regional>SelectedRegional = new ArrayList<>();
    private boolean populated = false;

    public Qualification_StateByState_SearchAdapter(@NonNull Context context, @NonNull ArrayList<Corp_Regional> regionals,
                                                    AutoCompleteTextView autoCompleteTextView,
                                                    ExpandableListView expandableListView, FragmentActivity fragmentActivity) {
        super(context, 0, new ArrayList<SearchDetails>());
        this.regionals = new ArrayList<>(regionals);
        this.autoCompleteTextView = autoCompleteTextView;
        this.expandableListView = expandableListView;
        this.fragmentActivity = fragmentActivity;

    }


    @Nullable
    @Override
    public SearchDetails getItem(int position) {
        return super.getItem(position);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return ChainFilter;
    }

    @SuppressLint("ViewHolder")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        convertView = LayoutInflater.from(getContext()).inflate(
                R.layout.drop_down_view, parent, false);

        final SearchDetails searchDetails = getItem(position);
        TextView category_name = convertView.findViewById(R.id.category_name);
        TextView description = convertView.findViewById(R.id.description);
        String title = "";


        category_name.setText(searchDetails.getName());
        description.setText(searchDetails.getDescription());


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                populated = true;
                hide();
                autoCompleteTextView.setText(searchDetails.getName());
                autoCompleteTextView.dismissDropDown();
                Qualification_SiteView_List_adapter adapter = new Qualification_SiteView_List_adapter(getContext(),getSelectedRegion(searchDetails),fragmentActivity);
                expandableListView.setAdapter(adapter);

                for (int i = 0; i <adapter.getGroupCount() ; i++) {
                    expandableListView.expandGroup(i);
                }
            }
        });

        return convertView;
    }



    @Override
    public int getPosition(@Nullable SearchDetails item) {
        return super.getPosition(item);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    private Filter ChainFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();
            ArrayList<SearchDetails> searchDetails = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {

                if (populated){
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Qualification_SiteView_List_adapter adapter = new Qualification_SiteView_List_adapter(getContext(),regionals,fragmentActivity);
                            expandableListView.setAdapter(adapter);

                            for (int i = 0; i <adapter.getGroupCount() ; i++) {
                                expandableListView.expandGroup(i);
                            }
                        }
                    });
                    populated = false;
                }


            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                SearchRegion(regionals,searchDetails,filterPattern);
            }

            results.values = searchDetails;
            results.count = searchDetails.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            addAll((ArrayList<SearchDetails>) results.values);
            notifyDataSetChanged();
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return super.convertResultToString(resultValue);
        }


    };


    private void SearchRegion (ArrayList<Corp_Regional> regionals,ArrayList<SearchDetails>searchDetails,String filterpattern){
        boolean isContain = false;
        if (regionals!=null){
            for (Corp_Regional regional : regionals){

                if (regional.getCorpStructures().name.toLowerCase().contains(filterpattern)){
                    for (SearchDetails details : searchDetails){
                        if (details.getName().equalsIgnoreCase(regional.getCorpStructures().name)){
                            isContain = true;
                            break;
                        }
                    }
                    if (!isContain){
                        searchDetails.add(new SearchDetails(regional.getCorpStructures().name,"Region"));
                    }

                    SearchProvince(regional.getSiteLists(),searchDetails,filterpattern);
                }else {

                    SearchProvince(regional.getSiteLists(),searchDetails,filterpattern);
                }
            }
        }
    }

    private void SearchProvince (ArrayList<SiteList>provinces , ArrayList<SearchDetails>searchDetails, String filterpattern){
        boolean isContain = false;
        if (provinces!=null){
            for (SiteList province : provinces){

                if (province.getProvince().toLowerCase().contains(filterpattern)){
                    for (SearchDetails details : searchDetails){
                        if (details.getName().equalsIgnoreCase(province.getProvince())){
                            isContain = true;
                            break;
                        }
                    }
                    if (!isContain){
                        searchDetails.add(new SearchDetails(province.getProvince(),"Province"));
                    }
                    SearchStore(province.getCustomSites(),searchDetails,filterpattern);
                }else {
                    SearchStore(province.getCustomSites(),searchDetails,filterpattern);
                }
            }
        }
    }

    private void SearchStore (ArrayList<CustomSite>sites , ArrayList<SearchDetails>searchDetails, String filterpattern){
        boolean isContain = false;
        if (sites!=null){
            for (CustomSite site : sites){

                if (site.getSite().sitename.toLowerCase().contains(filterpattern)) {
                    for (SearchDetails details : searchDetails) {
                        if (details.getName().equalsIgnoreCase(site.getSite().sitename)) {
                            isContain = true;
                            break;
                        }
                    }
                    if (!isContain) {
                        searchDetails.add(new SearchDetails(site.getSite().sitename, "Store"));
                    }
                }
            }
        }
    }

    private ArrayList<Corp_Regional> getSelectedRegion(SearchDetails details){

        SelectedRegional = new ArrayList<>();
        ArrayList<Corp_Regional>corpRegionals = new ArrayList<>(regionals);

        try {
            for (Corp_Regional regional : corpRegionals){
                if (regional.getCorpStructures().name.equalsIgnoreCase(details.getName())){
                    regional.setSelected(true);

                    if (regional.getSiteLists()!=null){
                        for (SiteList province : regional.getSiteLists()){
                            province.setSelected(true);
                        }
                    }
                    SelectedRegional.add(regional);
                }else {

                    Corp_Regional regional1 = new Corp_Regional();
                    regional1.setCorpStructures(regional.getCorpStructures());
                    regional1.setSelected(true);
                    regional1.setSiteLists(getProvinces(regional.getSiteLists(),details));

                    if (regional1.getSiteLists().size()>0){
                        SelectedRegional.add(regional1);
                    }
                }
            }


        }catch (Exception e){
            e.printStackTrace();
        }


        return SelectedRegional;
    }


    private ArrayList<SiteList> getProvinces(ArrayList<SiteList>provinces,SearchDetails details){

        ArrayList<SiteList>SuggestedSitelist = new ArrayList<>();
        ArrayList<SiteList>siteLists = new ArrayList<>(provinces);

        try {
            for (SiteList province : siteLists){
                if (province.getProvince().equalsIgnoreCase(details.getName())){
                    province.setSelected(true);
                    SuggestedSitelist.add(province);
                }else {

                    SiteList province1 = new SiteList();
                    province1.setSelected(true);
                    province1.setProvince(province.getProvince());
                    province1.setCustomSites(getSelectedStore(province.getCustomSites(),details));
                    if (province1.getCustomSites().size()>0){
                        SuggestedSitelist.add(province1);
                    }
                }
            }


        }catch (Exception e){
            e.printStackTrace();
        }


        return SuggestedSitelist;
    }

    private ArrayList<CustomSite> getSelectedStore(ArrayList<CustomSite>sites,SearchDetails details){
        ArrayList<CustomSite>SelectedSite = new ArrayList<>();
        ArrayList<CustomSite>CustomSite = new ArrayList<>(sites);
        try {
            for (CustomSite site : CustomSite){
                if (site.getSite().sitename.equalsIgnoreCase(details.getName())){
                    SelectedSite.add(site);
                    System.out.println("SITE NAME : "+site.getSite().sitename);

                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return SelectedSite;
    }

    public void hide() {
        View view = fragmentActivity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) fragmentActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


}
