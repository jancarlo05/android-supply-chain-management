package com.procuro.androidscm.Restaurant.PimmyNews;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;

import com.procuro.androidscm.R;

import java.util.Objects;

public class Pimmy_News_DialogFragment extends DialogFragment {


    private Button close ;
    private ProgressBar progressBar;
    private TextView date , wait , title;
    private WebView webView;

    public static Pimmy_News_DialogFragment newInstance() {
        return new Pimmy_News_DialogFragment();
    }
    @Override
        public int getTheme() {
        return R.style.slide_down_up;
        }

    @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.pimmy_news_fragment, container, false);

//                    int height = (int)(getResources().getDisplayMetrics().heightPixels*0.90);
//                    int width = 320;
//
        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.99);
        int height = (int)(getResources().getDisplayMetrics().heightPixels*.90);


        Objects.requireNonNull(getDialog().getWindow()).setLayout(width, height);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.slide_down_up;
        view.setFocusableInTouchMode(true);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        webView = view.findViewById(R.id.webview);
        close = view.findViewById(R.id.back_button);
        progressBar = view.findViewById(R.id.progressBar2);
        wait = view.findViewById(R.id.wait);

        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        webView.setWebChromeClient(new Webcromeclient());
        webView.setWebViewClient(new mywebviewclient());
        webView.loadUrl("https://procuro.com/pimmy/news/");

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });



        return view;

        }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }


    private class Webcromeclient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);


        }
    }

    private class mywebviewclient extends WebViewClient {

        @Override
        public void onPageFinished(WebView view, String url) {
            wait.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            super.onPageFinished(view, url);

        }
    }




}
