package com.procuro.androidscm.Restaurant.ReportsFragment.Inspection;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.gson.JsonArray;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.QuickAccessMenu.QuickAccessMenu;
import com.procuro.androidscm.Restaurant.ReportsFragment.Inspection.FoodSafetySummary.FSLArea;
import com.procuro.androidscm.Restaurant.ReportsFragment.Inspection.FoodSafetySummary.FSLDistrict;
import com.procuro.androidscm.Restaurant.ReportsFragment.Inspection.FoodSafetySummary.FSLDivision;
import com.procuro.androidscm.Restaurant.ReportsFragment.Inspection.FoodSafetySummary.FSLRegion;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsChildList;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsData;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsDateChildList;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsDateGrandChildList;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsParentList;
import com.procuro.androidscm.Restaurant.ReportsFragment.Reports_fragment;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.ApplicationReport;
import com.procuro.apimmdatamanagerlib.CorpStructure;
import com.procuro.apimmdatamanagerlib.FSLAggregateData;
import com.procuro.apimmdatamanagerlib.FSLAggregateSiteData;
import com.procuro.apimmdatamanagerlib.FSLCompliance;
import com.procuro.apimmdatamanagerlib.FSLDaySummary;
import com.procuro.apimmdatamanagerlib.FSLDaypartSummaryDTO;
import com.procuro.apimmdatamanagerlib.FSLSite;
import com.procuro.apimmdatamanagerlib.FSLSummary;
import com.procuro.apimmdatamanagerlib.FSLTemperature;
import com.procuro.apimmdatamanagerlib.FSLTemperaturePerformace;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;

import static com.procuro.androidscm.Restaurant.ReportsFragment.Inspection.Reports_inspection_list_adapter.ClearReportsInstance;
import static com.procuro.androidscm.Restaurant.ReportsFragment.Inspection.Reports_inspection_list_adapter.DisplayDaily;
import static com.procuro.androidscm.SCMTool.dpToPx;

public class Reports_Inspection_RuReport extends Fragment {

    com.procuro.apimmdatamanagerlib.aPimmDataManager dataManager = aPimmDataManager.getInstance();
    private info.hoang8f.android.segmented.SegmentedGroup infogroup;
    private WebView webView ;
    private ProgressBar progressBar;
    private User users ;
    private Site sites;
    private ApplicationReport applicationReport;
    private TextView title;
    private Button back,quick_a_menu;
    private String Params;

    //FOODSAFETY SUMMARY

    public FSLDaypartSummaryDTO currentFSLDaypartSummary  = new FSLDaypartSummaryDTO();
    public static LinearLayout nav_container,root,navigation_container;
    private RadioButton foodsafety,checklist;
    private String LayerInidator = "";
    private String LayerName = "";

    public Reports_Inspection_RuReport() {
        this.applicationReport = SCMDataManager.getInstance().getSelectedApplicationReport();
        this. users = SCMDataManager.getInstance().getUser();
        this.sites = SCMDataManager.getInstance().getSelectedSite().getSite();
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.reports_ru_report, container, false);

        infogroup = view.findViewById(R.id.segmented2);
        webView = view.findViewById(R.id.webview);
        progressBar = view.findViewById(R.id.progresbar);
        nav_container = view.findViewById(R.id.action_container);
        infogroup = view.findViewById(R.id.segmented2);
        foodsafety = view.findViewById(R.id.radio1);
        checklist = view.findViewById(R.id.radio2);
        title = view.findViewById(R.id.username);
        back = view.findViewById(R.id.home);
        quick_a_menu = view.findViewById(R.id.quick_a_menu);
        navigation_container = view.findViewById(R.id.navigation_container);

        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        settings.setSupportZoom(true);
        settings.supportZoom();
        settings.setDisplayZoomControls(false);
        settings.setBuiltInZoomControls(true);

        webView.setWebChromeClient(new Webcromeclient());
        webView.setWebViewClient(new mywebviewclient());
        webView.addJavascriptInterface(new JavaScriptInterface(getContext(),webView),"AndroidApp");
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(true);

        title.setText(applicationReport.ReportName);


        ReportsData.getInstance().setSelectedCorpStructure(null);
        ReportsData.getInstance().setNavigations(null);


        if (applicationReport.ReportName.equalsIgnoreCase("Food Safety Summary")){
            PopuulateFoodSafetySumamary();
            setupOnclicks();


        }else if (applicationReport.ReportName.equalsIgnoreCase("PUW Speed Of Service Summary")){
            navigation_container.setVisibility(View.GONE);
            webView.loadUrl("file:///android_asset/RUReport/index.html");
            setupOnclicks();


        }else if (applicationReport.ReportName.equalsIgnoreCase("Shift Performance Summary")){
            foodsafety.setText("Inspection");
            checklist.setText("Equipment");
            setupOnclicks();
            foodsafety.setChecked(true);
        }

        return view;
    }

    private void setupOnclicks(){
        quick_a_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuickAccessMenu.DisplayQuickAMenu(getContext(),quick_a_menu,getActivity());
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Back();
            }
        });

        infogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                ClearViewGroup();
                progressBar.setVisibility(View.VISIBLE);
                webView.loadUrl("file:///android_asset/RUReport/index.html");
            }
        });
        SCMTool.DisableView(foodsafety,0.5f);
        SCMTool.DisableView(checklist,0.5f);
    }

    class  JavaScriptInterface {

        Context context;
        WebView webView;

        JavaScriptInterface(Context c, WebView w) {
            context = c;
            webView = w;
        }

        @JavascriptInterface
        public void callbackAction(String id, final String corpId, String title) {
            System.out.println("ID :"+id + " | "+corpId+ " | "+title);
            if (applicationReport.ReportName.equalsIgnoreCase("Food Safety Summary")){

                try {
                    SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
                    format.setTimeZone(TimeZone.getTimeZone("UTC"));
                    Date date = format.parse(title);

                    ArrayList<ReportsParentList>parentLists = SCMDataManager.getInstance().getReports_inspections();
                    for (ReportsParentList parentList : parentLists){
                        if (parentList.getTitle().equalsIgnoreCase("Detail Inspections Reports")){
                            ArrayList<ReportsDateChildList>childLists = parentList.getWeeklyArrayList();
                            for (ReportsDateChildList child : childLists){
                                if (child.getDailyArrayList()!=null){
                                    for (ReportsDateGrandChildList dailies : child.getDailyArrayList()){
                                        if (format.format(dailies.getDate()).equalsIgnoreCase(title)){

                                            SCMDataManager.getInstance().setSelectedReportsDateGrandChild(dailies);
                                            SCMDataManager.getInstance().setSeletedReportsDateChildList(child);

                                            if (SCMDataManager.getInstance().getReportLevel().equalsIgnoreCase("site")){
                                                ClearReportsInstance(SCMDataManager.getInstance().getSelectedSite());
                                            }else {

                                                FSLSite site = (FSLSite)ReportsData.getInstance().getSelectedCorpStructure();
                                                ClearReportsInstance(getSite(site.siteID));
                                            }
                                            DisplayDaily(getActivity(),dailies,child);
                                            break;
                                        }
                                    }
                                }
                            }
                            break;
                        }
                    }

                }catch (Exception e){
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            ClearViewGroup();
                            SearchSelectedItem(corpId);
                            webView.loadUrl("file:///android_asset/RUReport/index.html");
                        }
                    });
                }
            }
        }
    }

    private CustomSite getSite(String siteID){
        for (CustomSite customSite : SCMDataManager.getInstance().getCustomSites()){
            if (customSite.getSite().siteid.equalsIgnoreCase(siteID)){
                return customSite;
            }
        }
        return SCMDataManager.getInstance().getSelectedSite();
    }

    public void SearchSelectedItem(String id){
        ReportsData reportsData = ReportsData.getInstance();
        ArrayList<Object>navigations = new ArrayList<>();
        if (reportsData.getRegions()!=null){
            for (FSLRegion region : reportsData.getRegions()){
                if (region.getCorpStructure().corpStructureID.equalsIgnoreCase(id)){
                    ReportsData.getInstance().setSelectedCorpStructure(region);
                    System.out.println("FOUND : " + region.getCorpStructure().name);
                    navigations.add(region);
                    break;
                }else {
                    if (FindInDivision(region,navigations,id)){
                        break;
                    }
                }
            }
        }
        reportsData.setNavigations(navigations);
    }

    public boolean FindInDivision(FSLRegion region,ArrayList<Object>navigations,String id){
        if (region.getDivisions()!=null){
            for (FSLDivision division : region.getDivisions()){
                if (division.getCorpStructure().corpStructureID.equalsIgnoreCase(id)){
                    ReportsData.getInstance().setSelectedCorpStructure(division);
                    System.out.println("FOUND : " + region.getCorpStructure().name+ " | "+division.getCorpStructure().name);
                    navigations.add(region);
                    navigations.add(division);
                    return true;
                }else {
                    if (FindInArea(region,division,navigations,id)){
                        break;
                    }
                }
            }
        }
        return false;
    }

    public boolean FindInArea(FSLRegion region,FSLDivision division,ArrayList<Object>navigations,String id){
        if (division.getAreas()!=null){
            for (FSLArea area : division.getAreas()){
                if (area.getCorpStructure().corpStructureID.equalsIgnoreCase(id)){
                    ReportsData.getInstance().setSelectedCorpStructure(area);
                    System.out.println("FOUND : " + region.getCorpStructure().name+ " | "+division.getCorpStructure().name +" | "+area.getCorpStructure().name);
                    navigations.add(region);
                    navigations.add(division);
                    navigations.add(area);
                    return true;
                }else {
                    if (FindInDistrict(region,division,area,navigations,id)){
                        break;
                    }
                }
            }
        }
        return false;
    }

    public boolean FindInDistrict(FSLRegion region,FSLDivision division,FSLArea area,ArrayList<Object>navigations,String id){
        if (area.getDistricts()!=null){
            for (FSLDistrict district : area.getDistricts()){
                if (district.getCorpStructure().corpStructureID.equalsIgnoreCase(id)){
                    ReportsData.getInstance().setSelectedCorpStructure(district);
                    System.out.println("FOUND : " + region.getCorpStructure().name+ " | "+division.getCorpStructure().name +" | "+
                            area.getCorpStructure().name+" | "+district.getCorpStructure().name);

                    navigations.add(region);
                    navigations.add(division);
                    navigations.add(area);
                    navigations.add(district);
                    return true;
                }else {
                    if (FindInSite(region,division,area,district,navigations,id)){
                        break;
                    }
                }
            }
        }
        return false;
    }

    public boolean FindInSite(FSLRegion region,FSLDivision division,FSLArea area,FSLDistrict district,ArrayList<Object>navigations,String id){
        if (district.getSites()!=null){
            for (FSLSite site : district.getSites()){
                if (site.siteID.equalsIgnoreCase(id)){
                    ReportsData.getInstance().setSelectedCorpStructure(site);
                    System.out.println("FOUND : " + region.getCorpStructure().name+ " | "+division.getCorpStructure().name +" | "+
                            area.getCorpStructure().name+" | "+district.getCorpStructure().name+" | "+site.siteName);

                    navigations.add(region);
                    navigations.add(division);
                    navigations.add(area);
                    navigations.add(district);
                    navigations.add(site);

                    DownloadFSLDaypartSUmmary(site.siteID);

                    return true;
                }
            }
        }
        return false;
    }



    private void DownloadFSLDaypartSUmmary(String siteID){
        progressBar.setVisibility(View.VISIBLE);
        ReportsData data = ReportsData.getInstance();
        dataManager.getDaypartSummaryForSiteID(siteID, data.getStartDate(), new Date(), new OnCompleteListeners.getDaypartSummaryForSiteIDListener() {
            @Override
            public void getDaypartSummaryForSiteIDCallback(FSLDaypartSummaryDTO daypartSummary, Error error) {
                if (error == null){
                    currentFSLDaypartSummary = daypartSummary;
                    SCMTool.EnableView(foodsafety,1);
                    SCMTool.EnableView(checklist,1);
                    foodsafety.setChecked(true);
                }
            }
        });
    }

    private void PopuulateFoodSafetySumamary(){
        DownloadFSLDaySummary();
    }

    private void DownloadFSLDaySummary(){
        final SCMDataManager data = SCMDataManager.getInstance();
        final ReportsData reportsData = ReportsData.getInstance();
        dataManager = aPimmDataManager.getInstance();
        if (!data.getReportLevel().equalsIgnoreCase("site")){
            if (reportsData.getRegions()==null){
                dataManager.getDaySummaryForSiteID(sites.siteid, reportsData.getStartDate(), new Date(), new OnCompleteListeners.getDaySummaryForSiteIDCallbackListener() {
                    @Override
                    public void getDaySummaryForSiteIDrCallback(FSLDaySummary fslDaySummary, Error error) {
                        if (error == null){

                            reportsData.setFslDaySummary(fslDaySummary);
                            FilterBYDistrict(reportsData.getFslDaySummary());


                            SCMTool.EnableView(foodsafety,1);
                            SCMTool.EnableView(checklist,1);
                            foodsafety.setChecked(true);
                        }
                    }
                });
            }else {
                FilterBYDistrict(reportsData.getFslDaySummary());
                reportsData.setFslDaySummary(reportsData.getFslDaySummary());

                SCMTool.EnableView(foodsafety,1);
                SCMTool.EnableView(checklist,1);
                foodsafety.setChecked(true);
            }

        }else {
            DownloadFSLDaypartSUmmary(data.getSelectedSite().getSite().siteid);
        }
    }

    private void FilterBYDistrict(FSLDaySummary fslDaySummary){

        ArrayList<FSLDistrict> districts = new ArrayList<>();
        ArrayList<String>DistricNames = new ArrayList<>();
        if (fslDaySummary.siteList !=null && fslDaySummary.corpStructure!=null){
            for (CorpStructure corpStructure : fslDaySummary.corpStructure){
                for (FSLSite site : fslDaySummary.siteList){
                    if (site.corpStructureID.equalsIgnoreCase(corpStructure.corpStructureID)){
                        if (!DistricNames.contains(corpStructure.name)){
                            DistricNames.add(corpStructure.name);
                            FSLDistrict district = new FSLDistrict();
                            district.setCorpStructure(corpStructure);
                            district.setSites(new ArrayList<FSLSite>());
                            district.getSites().add(site);
                            districts.add(district);
                        }else {
                            for (FSLDistrict fslDistrict : districts){
                                if (fslDistrict.getCorpStructure().corpStructureID.equalsIgnoreCase(corpStructure.corpStructureID)){
                                    fslDistrict.getSites().add(site);
                                }
                            }
                        }
                    }
                }
            }
        }
        FilterBYArea(districts,fslDaySummary);
    }

    private void FilterBYArea(ArrayList<FSLDistrict>districts,FSLDaySummary fslDaySummary){
        ArrayList<FSLArea> areas = new ArrayList<>();
        ArrayList<String>AreaNames = new ArrayList<>();
        if (districts!=null && fslDaySummary.corpStructure!=null){
            for (CorpStructure corpStructure : fslDaySummary.corpStructure){
                for (FSLDistrict district : districts){
                    if (district.getCorpStructure().parentID.equalsIgnoreCase(corpStructure.corpStructureID)){
                        if (!AreaNames.contains(corpStructure.name)){
                            AreaNames.add(corpStructure.name);
                            FSLArea area = new FSLArea();
                            area.setCorpStructure(corpStructure);
                            area.setDistricts(new ArrayList<FSLDistrict>());
                            area.getDistricts().add(district);
                            areas.add(area);
                        }else {
                            for (FSLArea fslArea : areas){
                                if (fslArea.getCorpStructure().corpStructureID.equalsIgnoreCase(corpStructure.corpStructureID)){
                                    fslArea.getDistricts().add(district);
                                }
                            }
                        }
                    }
                }
            }
        }
        FilterBYDivision(areas,fslDaySummary);
    }

    private void FilterBYDivision(ArrayList<FSLArea>areas, FSLDaySummary fslDaySummary){
        ArrayList<FSLDivision> divisions = new ArrayList<>();
        ArrayList<String>DivisionName = new ArrayList<>();
        if (areas!=null && fslDaySummary.corpStructure!=null){
            for (CorpStructure corpStructure : fslDaySummary.corpStructure){
                for (FSLArea area : areas){
                    if (area.getCorpStructure().parentID.equalsIgnoreCase(corpStructure.corpStructureID)){
                        if (!DivisionName.contains(corpStructure.name)){
                            DivisionName.add(corpStructure.name);
                            FSLDivision division = new FSLDivision();
                            division.setCorpStructure(corpStructure);
                            division.setAreas(new ArrayList<FSLArea>());
                            division.getAreas().add(area);
                            divisions.add(division);
                        }else {
                            for (FSLDivision fslDivision : divisions){
                                if (fslDivision.getCorpStructure().corpStructureID.equalsIgnoreCase(corpStructure.corpStructureID)){
                                    fslDivision.getAreas().add(area);
                                }
                            }
                        }
                    }
                }
            }
        }
        FilterBYRegion(divisions,fslDaySummary);
    }

    private void FilterBYRegion(ArrayList<FSLDivision>divisions, FSLDaySummary fslDaySummary){
        ArrayList<String>RegionName = new ArrayList<>();
        ArrayList<FSLRegion>regions = new ArrayList<>();
        if (divisions!=null && fslDaySummary.corpStructure!=null){
            for (CorpStructure corpStructure : fslDaySummary.corpStructure){
                for (FSLDivision division : divisions){
                    if (division.getCorpStructure().parentID.equalsIgnoreCase(corpStructure.corpStructureID)){
                        if (!RegionName.contains(corpStructure.name)){
                            RegionName.add(corpStructure.name);
                            FSLRegion region = new FSLRegion();
                            region.setCorpStructure(corpStructure);
                            region.setDivisions(new ArrayList<FSLDivision>());
                            region.getDivisions().add(division);
                            regions.add(region);
                        }else {
                            for (FSLRegion fslRegion : regions){
                                if (fslRegion.getCorpStructure().corpStructureID.equalsIgnoreCase(corpStructure.corpStructureID)){
                                    fslRegion.getDivisions().add(division);
                                }
                            }
                        }
                    }
                }
            }
        }
        ReportsData.getInstance().setRegions(regions);
    }

    private String CreateFoodSafetyParams(){
        JSONObject jsonObject = new JSONObject();
        boolean isSite = false;
        final SCMDataManager data = SCMDataManager.getInstance();
        if (!data.getReportLevel().equalsIgnoreCase("site")) {
            try {
                Object selectedCorpStructure =ReportsData.getInstance().getSelectedCorpStructure();
                CorpStructure corpStructure = new CorpStructure();
                if (selectedCorpStructure != null) {
                    if (selectedCorpStructure instanceof FSLRegion) {
                        FSLRegion region = (FSLRegion) selectedCorpStructure;
                        corpStructure = region.getCorpStructure();
                    } else if (selectedCorpStructure instanceof FSLDivision) {
                        FSLDivision division = (FSLDivision) selectedCorpStructure;
                        corpStructure = division.getCorpStructure();
                    } else if (selectedCorpStructure instanceof FSLArea) {
                        FSLArea area = (FSLArea) selectedCorpStructure;
                        corpStructure = area.getCorpStructure();
                    } else if (selectedCorpStructure instanceof FSLDistrict) {
                        FSLDistrict district = (FSLDistrict) selectedCorpStructure;
                        corpStructure = district.getCorpStructure();
                    } else if (selectedCorpStructure instanceof FSLSite) {
                        FSLSite site = (FSLSite) selectedCorpStructure;
                        corpStructure = new CorpStructure();
                        corpStructure.corpStructureID = site.siteID;
                        corpStructure.layerName = site.siteName;
                        corpStructure.layer = 6;
                        isSite = true;
                    }
                } else {
                    corpStructure = new CorpStructure();
                    corpStructure.layerName = "Region";
                    corpStructure.layer = 1;
                }
                if (!isSite) {
                    JSONObject items = items(ReportsData.getInstance().getFslDaySummary());
                    jsonObject.put("headers", getFoodSafetyHeader());
                    jsonObject.put("siteId", "87340814-8fc7-4aa0-b321-b004ecdd02d3");
                    jsonObject.put("subtitle", LayerName);
                    jsonObject.put("spid", "Wendy's International Inc.");
                    jsonObject.put("totalStores", ReportsData.getInstance().getFslDaySummary().siteList.size());
                    jsonObject.put("headerTitles", getHeaderTitles(true));
                    jsonObject.put("title", "PIMM&trade; Food Safety Summary");
                    jsonObject.put("header", LayerInidator);
                    jsonObject.put("address", "4049 E. LIVINGSTON AVE., COLUMBUS, OH");
                    jsonObject.put("nextLvl", corpStructure.layer);
                    jsonObject.put("items", items.getJSONArray("items"));
                    jsonObject.put("average", items.getJSONArray("average"));
                } else {
                    jsonObject.put("headers", getFoodSafetyHeader());
                    jsonObject.put("siteId", data.getSelectedSite().getSite().siteid);
                    jsonObject.put("subtitle", LayerName);
                    jsonObject.put("spid", data.getSelectedSite().getSite().CustomerName);
                    jsonObject.put("totalStores", 1);
                    jsonObject.put("headerTitles", getHeaderTitles(true));
                    jsonObject.put("title", "PIMM&trade; Food Safety Summary");
                    jsonObject.put("header", LayerInidator);
                    jsonObject.put("address", SCMTool.getCompleteAddress(data.getSelectedSite().getSite()));
                    jsonObject.put("nextLvl", corpStructure.layer);
                    jsonObject.put("items", CreateDates());
                    JSONObject total_average = getFSLTotalAverage(currentFSLDaypartSummary);
                    jsonObject.put("average", total_average.getJSONArray("average"));
                    jsonObject.put("total", total_average.getJSONArray("total"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            try {
                jsonObject.put("headers", getFoodSafetyHeader());
                jsonObject.put("siteId",data.getSelectedSite().getSite().siteid);
                jsonObject.put("subtitle",data.getSelectedSite().getSite().sitename);
                jsonObject.put("spid",data.getSelectedSite().getSite().CustomerName);
                jsonObject.put("totalStores",1);
                jsonObject.put("headerTitles",getHeaderTitles(true));
                jsonObject.put("title","PIMM&trade; Food Safety Summary");
                jsonObject.put("header","Details");
                jsonObject.put("address",SCMTool.getCompleteAddress(data.getSelectedSite().getSite()));
                jsonObject.put("nextLvl",6);
                jsonObject.put("items",CreateDates());
                JSONObject total_average = getFSLTotalAverage(currentFSLDaypartSummary);
                jsonObject.put("average",total_average.getJSONArray("average"));
                jsonObject.put("total",total_average.getJSONArray("total"));

            }catch (Exception e){
                e.printStackTrace();
            }

        }

        return jsonObject.toString();
    }

    private JSONArray CreateShiftPerformanceDates(JSONArray header){
        ReportsData reportsData = ReportsData.getInstance();
        JSONArray jsonArray = new JSONArray();
        Calendar start = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        start.setTime(reportsData.getStartDate());
        start.clear(Calendar.HOUR);
        start.clear(Calendar.MINUTE);
        start.clear(Calendar.MILLISECOND);

        Calendar end = SCMTool.getCurrentDateWithTimeZone();

        SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        try {
            JSONObject jsonObject = new JSONObject();
            JSONArray fslCompliant = new JSONArray();
            if (header.length()>0){
                for (int i = 0; i <header.length() ; i++) {
                    fslCompliant.put("--");
                }
            }
            jsonObject.put("isParent",true);
            jsonObject.put("title",format.format(start.getTime()));
            jsonObject.put("fslCompliant",fslCompliant);
            jsonArray.put(jsonObject);

            while (!format.format(start.getTime()).equalsIgnoreCase(format.format(end.getTime()))) {
                start.add(Calendar.DATE,1);

                jsonObject = new JSONObject();
                fslCompliant = new JSONArray();
                if (header.length()>0){
                    for (int i = 0; i <header.length() ; i++) {
                        fslCompliant.put("--");
                    }
                }
                jsonObject.put("isParent",true);
                jsonObject.put("title",format.format(start.getTime()));
                jsonObject.put("fslCompliant",fslCompliant);
                jsonArray.put(jsonObject);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return jsonArray;
    }

    private JSONArray CreateAverageTemplate(JSONArray header){
        JSONArray jsonArray = new JSONArray();
        try {
            if (header.length()>0){
                for (int i = 0; i <header.length() ; i++) {
                    jsonArray.put("--");
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return jsonArray;
    }

    private JSONArray CreateDates(){
        ReportsData reportsData = ReportsData.getInstance();
        JSONArray jsonArray = new JSONArray();
        Calendar start = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        start.setTime(reportsData.getStartDate());
        start.clear(Calendar.HOUR);
        start.clear(Calendar.MINUTE);
        start.clear(Calendar.MILLISECOND);

        Calendar end = SCMTool.getCurrentDateWithTimeZone();

        SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        JSONArray array = getFSLParentItem(currentFSLDaypartSummary,start.getTime());
        if (array.length()>0){
            for (int i = 0; i <array.length() ; i++) {
                try {
                    jsonArray.put(array.getJSONObject(i));
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

        System.out.println("DATE : "+format.format(start.getTime()));

        while (!format.format(start.getTime()).equalsIgnoreCase(format.format(end.getTime()))) {
            start.add(Calendar.DATE,1);
            array = getFSLParentItem(currentFSLDaypartSummary,start.getTime());

            if (array.length()>0){
                for (int i = 0; i <array.length() ; i++) {
                    try {
                        jsonArray.put(array.getJSONObject(i));
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
            System.out.println("DATE : "+format.format(start.getTime()));
        }
        return jsonArray;
    }

    private JSONArray getFSLParentItem(FSLDaypartSummaryDTO daySummary, Date date){

        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            ArrayList<FSLSummary> fsldaylist = daySummary.daySummaryList;
            JSONArray fslCompliant = new JSONArray();
            ArrayList<FSLSummary> ChildSummary = new ArrayList<>();

            boolean found = false;
            if (fsldaylist!=null){
                for (FSLSummary summary : fsldaylist){
                    if (format.format(summary.date).equalsIgnoreCase(format.format(date))){
                        found = true;
                        ArrayList<Integer>total_status = new ArrayList<>();
                        int total_ontime = 0, total_late = 0 , total_complete=0,total_partial = 0,total_incomplete = 0,total_reviewed = 0;

                        if (daySummary.daypartSummaryList!=null){
                            for (FSLSummary daypartSummary :daySummary.daypartSummaryList){
                                if (format.format(daypartSummary.date).equalsIgnoreCase(format.format(date))){

                                    ChildSummary.add(daypartSummary);
                                    Integer onTime = daypartSummary.onTime;
                                    int late = 0;
                                    int complete =0;
                                    int status= daypartSummary.status;
                                    int partial = 0;
                                    int incomplete = 0;

                                    if (onTime !=null){
                                        if (onTime ==0){
                                            late = 1;
                                        }
                                        complete = 1;
                                    }else {
                                        onTime = 0;
                                    }
                                    if (status == 1){
                                        partial = 1;
                                    } else if (status == 0 || status == 3){
                                        incomplete = 1;
                                    }

                                    total_status.add(status);
                                    total_ontime+=onTime;
                                    total_late +=late;
                                    total_complete+=complete;
                                    total_incomplete+=incomplete;
                                    total_partial+=partial;
                                    total_reviewed +=daypartSummary.reviewed;
                                }
                            }
                        }
                        fslCompliant.put(total_ontime+"/"+ChildSummary.size());
                        fslCompliant.put(total_late+"/"+ChildSummary.size());
                        fslCompliant.put(total_complete+"/"+ChildSummary.size());
                        fslCompliant.put(total_partial+"/"+ChildSummary.size());
                        fslCompliant.put(total_incomplete+"/"+ChildSummary.size());
                        if (total_status.size()>0){
                            fslCompliant.put(getStatusValue(total_status.get(0)));
                        }else {
                            fslCompliant.put(getStatusValue(0));
                        }
                        fslCompliant.put(getStringValueOFInt(total_reviewed));


                        if (foodsafety.isChecked()){
                            boolean tempFound = false;
                            if (daySummary.temperaturePerformanceList!=null){
                                for (FSLTemperaturePerformace temp : daySummary.temperaturePerformanceList){
                                    if (format.format(date).equalsIgnoreCase(format.format(temp.timestamp))){
                                        tempFound = true;
                                        fslCompliant.put(Math.round(temp.percentInThreshold)+"%");
                                        fslCompliant.put(temp.totalMetricCount);
                                        fslCompliant.put(Math.round(temp.percentInService)+"%");
                                        fslCompliant.put(getScore(temp.score));
                                        break;
                                    }
                                }
                                if (!tempFound){
                                    for (int i = 0; i <4 ; i++) {
                                        fslCompliant.put("-");
                                    }
                                }
                            }else {
                                for (int i = 0; i <4 ; i++) {
                                    fslCompliant.put("-");
                                }
                            }
                        }else {
                            for (int i = 0; i <4 ; i++) {
                                fslCompliant.put("-");
                            }
                        }

                        jsonObject.put("numOfChild",ChildSummary.size());

                        break;
                    }
                }
                if (!found) {
                    jsonObject.put("isParent",true);
                    for (int i = 0; i <11 ; i++) {
                        fslCompliant.put("-");
                    }
                }
            }

            jsonObject.put("isParent",true);
            jsonObject.put("title",format.format(date));
            jsonObject.put("fslCompliant",fslCompliant);
            jsonArray.put(jsonObject);

            if (ChildSummary.size()>0){

                for (FSLSummary summary : ChildSummary){
                    JSONObject ChildObj = new JSONObject();
                    JSONArray childItems = new JSONArray();

                    Integer onTime = summary.onTime;
                    int late = 0;
                    int complete =0;
                    int status= summary.status;
                    int partial = 0;
                    int incomplete = 0;

                    if (onTime !=null){
                        if (onTime ==0){
                            late = 1;
                        }
                        complete = 1;
                    }else {
                        onTime = 0;
                    }
                    if (status == 1){
                        partial = 1;
                    } else if (status == 0 || status == 3){
                        incomplete = 1;
                    }

                    childItems.put(getStringValueOFInt(onTime));
                    childItems.put(getStringValueOFInt(late));
                    childItems.put(getStringValueOFInt(complete));
                    childItems.put(getStringValueOFInt(partial));
                    childItems.put(getStringValueOFInt(incomplete));
                    childItems.put(getStatusValue(status));
                    childItems.put(getStringValueOFInt(summary.reviewed));

                    for (int i = 0; i <4 ; i++) {
                        childItems.put("-");
                    }
                    ChildObj.put("isParent",false);
                    ChildObj.put("title","Shift: "+summary.daypart);
                    ChildObj.put("fslCompliant",childItems);

                    jsonArray.put(ChildObj);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return jsonArray;
    }

    private JSONObject getFSLTotalAverage(FSLDaypartSummaryDTO daySummary){
        JSONObject jsonObject = new JSONObject();
        ArrayList<Date>dates = new ArrayList<>();
        ReportsData reportsData = ReportsData.getInstance();

        Calendar start = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        start.setTime(reportsData.getStartDate());
        start.clear(Calendar.HOUR);
        start.clear(Calendar.MINUTE);
        start.clear(Calendar.MILLISECOND);

        Calendar end = SCMTool.getCurrentDateWithTimeZone();

        SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        dates.add(start.getTime());
        while (!format.format(start.getTime()).equalsIgnoreCase(format.format(end.getTime()))) {
            start.add(Calendar.DATE,1);
            dates.add(start.getTime());
        }


        ArrayList<Integer>total_status = new ArrayList<>();
        int total_ontime = 0, total_late = 0 , total_complete=0,total_partial = 0,total_incomplete = 0,total_reviewed = 0;
        int totalActiveRows = 0;

        int total_inTemp = 0, total_metric = 0, total_monitored = 0 , total_score = 0;

        for (Date date : dates){
            if (daySummary.daySummaryList!=null){
                for (FSLSummary fslDaySummary : daySummary.daySummaryList){
                    if (format.format(date).equalsIgnoreCase(format.format(fslDaySummary.date))){
                        totalActiveRows++;
                        if (daySummary.daypartSummaryList!=null){
                            for (FSLSummary fsldaypartSummary : daySummary.daypartSummaryList){
                                if (format.format(date).equalsIgnoreCase(format.format(fsldaypartSummary.date))){
                                    Integer onTime = fsldaypartSummary.onTime;
                                    int late = 0;
                                    int complete =0;
                                    int status= fsldaypartSummary.status;
                                    int partial = 0;
                                    int incomplete = 0;

                                    if (onTime !=null){
                                        if (onTime ==0){
                                            late = 1;
                                        }
                                        complete = 1;
                                    }else {
                                        onTime = 0;
                                    }
                                    if (status == 1){
                                        partial = 1;
                                    } else if (status == 0 || status == 3){
                                        incomplete = 1;
                                    }

                                    total_status.add(status);
                                    total_ontime+=onTime;
                                    total_late +=late;
                                    total_complete+=complete;
                                    total_incomplete+=incomplete;
                                    total_partial+=partial;
                                    total_reviewed +=fsldaypartSummary.reviewed;
                                }
                            }
                        }

                        if (daySummary.temperaturePerformanceList!=null){
                            for (FSLTemperaturePerformace temp : daySummary.temperaturePerformanceList){
                                if (format.format(date).equalsIgnoreCase(format.format(temp.timestamp))){
                                    total_inTemp  +=Math.round(temp.percentInThreshold);
                                    total_metric  +=temp.totalMetricCount;
                                    total_monitored  +=Math.round(temp.percentInService);
                                    total_score  +=temp.score;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        try {
            jsonObject.put("total",getTOtal(total_status,total_ontime,total_late,total_complete,total_partial,total_incomplete,total_reviewed,
                    total_inTemp,total_metric,total_monitored,total_score,totalActiveRows,dates));

            jsonObject.put("average",getAverage(total_status,total_ontime,total_late,total_complete,total_partial,total_incomplete,total_reviewed,
                    total_inTemp,total_metric,total_monitored,total_score,totalActiveRows,dates));

        }catch (Exception e){
            e.printStackTrace();
        }


        return jsonObject;
    }

    private JSONArray getTOtal(ArrayList<Integer>total_status,int total_ontime ,int total_late,int total_complete,int total_partial,int total_incomplete,int total_reviewed
            ,int total_inTemp,int total_metric,int total_monitored,int total_score,int totalActiveRows,ArrayList<Date>dates){
        JSONArray total = new JSONArray();

        //ONTIME
        total.put(total_ontime+"/"+total_complete);
        //LATE
        total.put(total_late+"/"+total_complete);
        //Complete
        total.put(total_complete+"/"+totalActiveRows);
        //Partial
        total.put(total_partial+"/"+totalActiveRows);
        //Incomplete
        total.put(total_incomplete+"/"+totalActiveRows);
        //Status
        if(totalActiveRows == dates.size()){
            if (total_status.size()>0){
                Collections.sort(total_status);
                total.put(getStatusValue(total_status.get(0)));
            }else {
                total.put("--");
            }
        }else {
            total.put("--");
        }
        //Reviewed
        total.put(total_reviewed+"/"+totalActiveRows);

        if (foodsafety.isChecked()){
            //In-temp
            if(totalActiveRows == dates.size()){
                total.put(total_inTemp+"%");
            }else {
                total.put("--");
            }
            //Metric
            total.put(total_metric+"/"+total_metric);
            //Monitored
            if(totalActiveRows == dates.size()){
                total.put(total_monitored+"%");
            }else {
                total.put("--");
            }

            //Score
            if(totalActiveRows == dates.size()){
                double totalScore = (double)total_score/totalActiveRows;
                total.put(getScore( (int) Math.round(totalScore)));
            }else {
                total.put("--");
            }
        }else {
            for (int i = 0; i <4 ; i++) {
                total.put("--");
            }
        }



        return total;
    }

    private JSONArray getAverage(ArrayList<Integer>total_status,int total_ontime ,int total_late,int total_complete,int total_partial,int total_incomplete,int total_reviewed
            ,int total_inTemp,int total_metric,int total_monitored,int total_score,int totalActiveRows,ArrayList<Date>dates){
        JSONArray total = new JSONArray();

        //ONTIME
        int ontime = 0 ;
        if (total_ontime !=0 || total_complete!=0){
            ontime = (int)((double)total_ontime/total_complete)*100;
        }
        total.put((ontime+"%"));
        //LATE
        int late = 0 ;
        if (total_late !=0 || total_complete!=0){
            late = (int)((double)total_late/total_complete)*100;
        }
        total.put((late+"%"));
        //Complete
        int Complete = 0 ;
        if (totalActiveRows !=0 || total_complete!=0){
            Complete = (int)((double)total_complete/totalActiveRows)*100;
        }
        total.put((Complete+"%"));
        //Partial
        int Partial = 0 ;
        if (totalActiveRows !=0 || total_partial!=0){
            Partial = (int)((double)total_partial/totalActiveRows)*100;
        }
        total.put((Partial+"%"));
        //Incomplete
        int Incomplete = 0 ;
        if (totalActiveRows !=0 || total_incomplete!=0){
            Incomplete = (int)((double)total_incomplete/totalActiveRows)*100;
        }
        total.put((Incomplete+"%"));

        //Status
        if(totalActiveRows == dates.size()){
            if (total_status.size()>0){
                Collections.sort(total_status);
                total.put(getStatusValue(total_status.get(0)));
            }else {
                total.put("--");
            }
        }else {
            total.put("--");
        }
        //Reviewed
        int Reviewed = 0 ;
        if (totalActiveRows !=0 || total_reviewed!=0){
            Reviewed = (int)((double)total_reviewed/totalActiveRows)*100;
        }
        total.put((Reviewed+"%"));

        if (foodsafety.isChecked()){
            //In-temp
            total.put((Math.round((double) total_inTemp/dates.size())) +"%");
            //Metric
            int Metric = 0 ;
            if (total_metric !=0){
                Metric = (int)((double)total_metric/total_metric)*100;
            }
            total.put((Metric+"%"));
            //Monitored
            total.put((Math.round((double) total_monitored/dates.size())) +"%");
            //Score
            if(totalActiveRows == dates.size()){
                double totalScore = (double)total_score/totalActiveRows;
                total.put(getScore( (int) Math.round(totalScore)));
            }else {
                total.put("--");
            }
        }else {
            for (int i = 0; i <4 ; i++) {
                total.put("--");
            }
        }

        return total;
    }

    private String getStringValueOFInt(int value){
        if (value == 1){
            return "YES";
        }
        else {
            return "No";
        }
    }

    private String getStatusValue(int value){
        if (value == 1){
            return "Partial";
        }
        else if (value == 2){
            return "Complete";
        }
        else {
            return "Incomplete";
        }
    }

    private JSONArray getFoodSafetyHeader(){
        String header = "[\n" +
                "    \"On-Time\",\n" +
                "    \"Late\",\n" +
                "    \"Completed\",\n" +
                "    \"Partial\",\n" +
                "    \"Incomplete\",\n" +
                "    \"Reviewed\",\n" +
                "    \"Score\",\n" +
                "    \"% In-Temp\",\n" +
                "    \"# Sites\",\n" +
                "    \"% Monitored\",\n" +
                "    \"Score\"\n" +
                "  ]";
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(header);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonArray;

    }

    private JSONArray getChecklistHeader(){
        String header = "[\n" +
                "            \"On-Time\",\n" +
                "            \"Late\",\n" +
                "            \"Completed\",\n" +
                "            \"Partial\",\n" +
                "            \"Incomplete\",\n" +
                "            \"Reviewed\",\n" +
                "            \"Score\",\n" +
                "            \"Cust. Sat. %\",\n" +
                "            \"Taste %\",\n" +
                "            \"Friendly %\",\n" +
                "            \"SoS %\"\n" +
                "        ]";
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(header);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonArray;

    }

    private JSONArray getHeaderTitles(boolean FoodSafety){
        String header = "[\n" +
                "    {\n" +
                "      \"cspan\": 7,\n" +
                "      \"title\": \"Food Safety Compliance\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"cspan\": 4,\n" +
                "      \"title\": \"Temperature Mgmt.\"\n" +
                "    }\n" +
                "  ],";

        if (!FoodSafety){
            header = "[\n" +
                    "            {\n" +
                    "                \"cspan\": 7,\n" +
                    "                \"title\": \"Checklist Performance\"\n" +
                    "            },\n" +
                    "            {\n" +
                    "                \"cspan\": 4,\n" +
                    "                \"title\": \"Restaurant Performance\"\n" +
                    "            }\n" +
                    "        ]";
        }

        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(header);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonArray;
    }

    private JSONArray getChecklistTitleHeader(){
        String header = "[\n" +
                "            {\n" +
                "                \"cspan\": 7,\n" +
                "                \"title\": \"Checklist Performance\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"cspan\": 5,\n" +
                "                \"title\": \"Restaurant Performance\"\n" +
                "            }\n" +
                "        ]";
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(header);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonArray;

    }

    private JSONObject items(FSLDaySummary fslDaySummary){

        ArrayList<Object>objects = new ArrayList<>();
        JSONArray jsonArray = new JSONArray();
        String name = null;
        Object selectedCorpStructure =ReportsData.getInstance().getSelectedCorpStructure();
        if (selectedCorpStructure==null){
            for (FSLRegion region : ReportsData.getInstance().getRegions()){
                region.setFslAggregateData(getAggregateDataList(region.getCorpStructure().corpStructureID,fslDaySummary,false));
                jsonArray.put(getFSLITEM(objects,region));
            }
        }else {
            if (selectedCorpStructure instanceof FSLRegion){
                FSLRegion region = (FSLRegion) selectedCorpStructure;
                name= region.getCorpStructure().name;
                for (FSLDivision division : region.getDivisions()){
                    division.setFslAggregateData(getAggregateDataList(division.getCorpStructure().corpStructureID,fslDaySummary,false));
                    jsonArray.put(getFSLITEM(objects,division));
                }
            }else if (selectedCorpStructure instanceof FSLDivision){
                FSLDivision division = (FSLDivision) selectedCorpStructure;
                name= division.getCorpStructure().name;

                for (FSLArea area : division.getAreas()){
                    System.out.println("ITEM : "+area.getCorpStructure().name);
                    area.setFslAggregateData(getAggregateDataList(area.getCorpStructure().corpStructureID,fslDaySummary,false));
                    jsonArray.put(getFSLITEM(objects,area));
                }

            }else if (selectedCorpStructure instanceof FSLArea){
                FSLArea area = (FSLArea) selectedCorpStructure;
                name= area.getCorpStructure().name;
                for (FSLDistrict district : area.getDistricts()){
                    System.out.println("ITEM : "+district.getCorpStructure().name);
                    district.setFslAggregateData(getAggregateDataList(district.getCorpStructure().corpStructureID,fslDaySummary,false));
                    jsonArray.put(getFSLITEM(objects,district));
                }
            }else if (selectedCorpStructure instanceof FSLDistrict){
                FSLDistrict district = (FSLDistrict) selectedCorpStructure;
                name= district.getCorpStructure().name;
                for (FSLSite site : district.getSites()){
                    site.setAggregateSiteDataList(getAggregateDataList(site.siteID,fslDaySummary,true));
                    System.out.println("ITEM : "+site.siteName);
                    jsonArray.put(getFSLITEM(objects,site));
                }
            }
        }

        JSONArray averages = new JSONArray();
        if (objects.size()>0){
            ArrayList<Integer>integers = (ArrayList<Integer>) objects.get(0);
            int size = integers.size();
            for (int i = 0; i <size ; i++) {
                int counter =0;
                int total = 0;
                int average = 0;
                for (int j = 0; j <objects.size() ; j++) {
                    counter++;
                    ArrayList<Integer>data = (ArrayList<Integer>) objects.get(j);
                    total += data.get(i);
                }
                double quotient =  (double)total/counter;
                average = (int)Math.round(quotient);
                if (i == 6 || i == 10){
                    averages.put(getScore(average));
                }else if (i == 8 ){
                    averages.put(total);
                }
                else {
                    averages.put(average+"%");
                }
            }
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("items",jsonArray);
            jsonObject.put("average",averages);
        }catch (Exception e){
            e.printStackTrace();
        }
        return jsonObject;
    }

    private JSONObject getFSLITEM(ArrayList<Object>objects,Object object){

        FSLRegion region  = null;
        FSLArea area  = null;
        FSLDistrict district  = null;
        FSLDivision division  = null;
        FSLSite site = null;

        JSONObject jsonObject = new JSONObject();
        CorpStructure corpStructure = null;
        boolean isSite = false;

        FSLAggregateData aggregateData = null;
        if (object instanceof FSLRegion){
            region = (FSLRegion) object;
            if (region.getFslAggregateData()!=null && region.getFslAggregateData().size()>0){
                aggregateData = region.getFslAggregateData().get(region.getFslAggregateData().size()-1);
            }
            corpStructure = region.getCorpStructure();
        }
        else if (object instanceof FSLDivision){
            division = (FSLDivision) object;
            if (division.getFslAggregateData()!=null && division.getFslAggregateData().size()>0){
                aggregateData = division.getFslAggregateData().get(division.getFslAggregateData().size()-1);
            }
            corpStructure = division.getCorpStructure();
        }
        else if (object instanceof FSLArea){
            area = (FSLArea) object;
            if (area.getFslAggregateData()!=null && area.getFslAggregateData().size()>0){
                aggregateData = area.getFslAggregateData().get(area.getFslAggregateData().size()-1);
            }
            corpStructure = area.getCorpStructure();
        }
        else if (object instanceof FSLDistrict){
            district = (FSLDistrict) object;
            if (district.getFslAggregateData()!=null && district.getFslAggregateData().size()>0){
                aggregateData = district.getFslAggregateData().get(district.getFslAggregateData().size()-1);
            }
            corpStructure = district.getCorpStructure();
        }
        else if (object instanceof FSLSite){
            site = (FSLSite) object;
            if (site.getAggregateSiteDataList()!=null && site.aggregateSiteDataList.size()>0){
                FSLAggregateSiteData siteData = site.getAggregateSiteDataList().get(site.getAggregateSiteDataList().size()-1);
                aggregateData = new FSLAggregateData();
                aggregateData.name = siteData.name;
                aggregateData.corpStructureID = siteData.siteID;
                aggregateData.compliance = siteData.compliance;
                aggregateData.temperature = siteData.temperature;
            }
            isSite = true;
        }

        FSLCompliance compliance = null;
        FSLTemperature temp = null;
        if (aggregateData!=null){
            compliance = aggregateData.compliance;
            temp = aggregateData.temperature;
        }

        int ontime = 0,late = 0,completed= 0,partial= 0,incompelete= 0,reviewed= 0,com_score= 0,inTempPercent= 0
                ,numberOfSites= 0,monitoredPercent= 0,temp_score= 0;

        ArrayList<Integer>intItems = new ArrayList<>();

        if (compliance!=null){
            ontime = getOntimePercentage(compliance.onTime,compliance.total,compliance.partial,compliance.incomplete);
            late = getLatePercentage(compliance.late,compliance.total,compliance.partial,compliance.incomplete);
            completed = getCompletePercentage(compliance.total,compliance.partial,compliance.incomplete);
            partial = getPartialPercentage(compliance.total,compliance.partial);
            incompelete = getIncompletePercentage(compliance.total,compliance.incomplete);
            reviewed = getReviewedPercentage(compliance.reviewed,compliance.total,compliance.partial,compliance.incomplete);
            com_score = compliance.score;
        }

        if(temp!=null){
            inTempPercent = (int)Math.round(temp.inTempPercent);
            numberOfSites = temp.numberOfSites;
            monitoredPercent =  (int)Math.round((temp.monitoredPercent));
            temp_score = temp.score;
        }

        intItems.add(ontime);
        intItems.add(late);
        intItems.add(completed);
        intItems.add(partial);
        intItems.add(incompelete);
        intItems.add(reviewed);
        intItems.add(com_score);

        JSONArray items = new JSONArray();
        items.put(ontime+"%");
        items.put(late+"%");
        items.put(completed+"%");
        items.put(partial+"%");
        items.put(incompelete+"%");
        items.put(reviewed+"%");
        items.put(getScore(com_score));

        if (foodsafety.isChecked()){
            intItems.add(inTempPercent);
            intItems.add(numberOfSites);
            intItems.add(monitoredPercent);
            intItems.add(temp_score);

            items.put(inTempPercent+"%");
            items.put(numberOfSites);
            items.put(monitoredPercent+"%");
            items.put(getScore(temp_score));
        }else {
            intItems.add(0);
            intItems.add(0);
            intItems.add(0);
            intItems.add(0);

            items.put("");
            items.put("");
            items.put("");
            items.put("");
        }

        objects.add(intItems);
        try {
            if (isSite){
                jsonObject.put("id",site.siteID);
                jsonObject.put("title",site.siteName);
                jsonObject.put("numOfStores",numberOfSites);
            }else {
                jsonObject.put("id",corpStructure.corpStructureID);
                jsonObject.put("title",corpStructure.name);
                jsonObject.put("numOfStores",aggregateData.compliance.numberOfSites);
            }
            jsonObject.put("fslCompliant",items);

        }catch (Exception e){
            e.printStackTrace();
        }return jsonObject;
    }

    private Integer getOntimePercentage(double ontime,double total,double partial,double incomplete){
        double partialIncomplete = partial - incomplete;
        double total_partial =  total - partialIncomplete;
        int percentage = 100;
        if (ontime!=0 && total_partial!=0){
            return (int) Math.round((ontime / total_partial) * percentage);
        }else {
            return 0;
        }
    }

    private Integer getLatePercentage(double late,double total,double partial,double incomplete){
        double partialIncomplete = partial - incomplete;
        double total_partial =  total - partialIncomplete;
        int percentage = 100;
        if (late!=0 && total_partial!=0){
            return (int) Math.round((late / total_partial) * percentage);
        }else {
            return 0;
        }
    }

    private Integer getPartialPercentage(double total,double partial){
        double percentage = 100.0;
        if (total!=0 && partial!=0){
            return (int) Math.round((partial/total)* percentage);
        }else {
            return 0;
        }
    }

    private Integer getIncompletePercentage(double total,double incomplete){
        double percentage = 100.0;
        if (total!=0 && incomplete!=0){
            return (int) Math.round(((incomplete/total)* percentage));
        }
        else {
            return 0;
        }
    }

    private Integer getCompletePercentage(double total,double partial,double incomplete){

        double percentage = 100.0;
        double PartialPercentage = getPartialPercentage(total,partial);
        double IncompletePercentage = getIncompletePercentage(total,incomplete);

        if (PartialPercentage!=0 && IncompletePercentage!=0){

            PartialPercentage = (partial/total)* percentage;

            IncompletePercentage = (incomplete/total)* percentage;
            return (int) Math.round((percentage -( IncompletePercentage + PartialPercentage)));
        }else {
            return 0;
        }

    }

    private Integer getReviewedPercentage(double reviewed ,double total,double partial,double incomplete){
        double percentage = 100.0;
        double partial_Total_incomplete = total - (partial +incomplete);
        if (partial_Total_incomplete !=0){
            return (int) Math.round(((reviewed / partial_Total_incomplete) * percentage));
        }else {
            return 0;
        }
    }

    private String getScore(int score){
        if (score == 5){
            return "A";
        } else if (score == 4){
            return "B";
        } else if (score == 3){
            return "C";
        } else if (score == 2){
            return "D";
        } else if (score == 1){
            return "F";
        }
        return "F";
    }

    private ArrayList getAggregateDataList(String corpID,FSLDaySummary fslDaySummary,boolean Site){
        ArrayList arrayList = new ArrayList();
        if (fslDaySummary!=null){
            if (!Site){
                if (fslDaySummary.aggregateDataList!=null){
                    for (FSLAggregateData aggregateData : fslDaySummary.aggregateDataList){
                        if (corpID.equalsIgnoreCase(aggregateData.corpStructureID)){
                            arrayList.add(aggregateData);
                        }
                    }
                }
            }else {
                if (fslDaySummary.aggregateSiteDataList!=null){
                    for (FSLAggregateSiteData aggregateData : fslDaySummary.aggregateSiteDataList){
                        if (corpID.equalsIgnoreCase(aggregateData.siteID)){
                            arrayList.add(aggregateData);
                        }
                    }
                }


            }
        }
        return arrayList;
    }


    private class Webcromeclient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                progressBar.setProgress(newProgress);
            }else {
                progressBar.setProgressTintList(ColorStateList.valueOf(Color.parseColor("#F68725")));
                progressBar.setProgress(newProgress);
            }
            if(newProgress == 100) {
                progressBar.setVisibility(View.GONE);

            }
        }
    }

    private class mywebviewclient extends WebViewClient {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            ClearViewGroup();
            if (applicationReport.ReportName.equalsIgnoreCase("Food Safety Summary")){

                setUPCustomNavigation();
                if (foodsafety.isChecked()){
                    GenerateRuReport(CreateFoodSafetyParams());
                }else {
                    GenerateRuReport(CreateChecklistParams());
                }
            }else if (applicationReport.ReportName.equalsIgnoreCase("PUW Speed Of Service Summary")){
                GenerateRuReport(getSpeedOfServiceSummaryParams());
            }
            else {
                setUPCustomNavigation();
                if (foodsafety.isChecked()){
                    GenerateRuReport(getInspectionParams());
                }else {
                    GenerateRuReport(getEquipmentParams());
                }
                SCMTool.EnableView(foodsafety,1);
                SCMTool.EnableView(checklist,1);
            }
        }
    }

    private String getSpeedOfServiceSummaryParams(){
        JSONObject jsonObject = new JSONObject();
        String headers = "[\"Total Time\",\"# Cars\",\"Total Time\",\"# Cars\",\"Total Time\",\"# Cars\",\"Total Time\",\"# Cars\",\"Total Time\",\"# Cars\",\"Total Time\",\"# Cars\"]";
        String headerTitle = "[{\"cspan\":2,\"title\":\"Daypart 1\"},{\"cspan\":2,\"title\":\"Daypart 2\"},{\"cspan\":2,\"title\":\"Daypart 3\"},{\"cspan\":2,\"title\":\"Daypart 4\"},{\"cspan\":2,\"title\":\"Daypart 5\"},{\"cspan\":2,\"title\":\"Daypart 6\"}]";
        try {
            JSONArray header = new JSONArray(headers);
            JSONArray headerTitles = new JSONArray(headerTitle);

            jsonObject.put("headers",header);
            jsonObject.put("title","PIMM&trade; PUW Speed Of Service Summary");
            jsonObject.put("headerTitles", headerTitles);
            jsonObject.put("siteId", SCMDataManager.getInstance().getSelectedSite().getSite().siteid);
            jsonObject.put("subtitle", SCMDataManager.getInstance().getSelectedSite().getSite().sitename);
            jsonObject.put("spid", SCMDataManager.getInstance().getSelectedSite().getSite().CustomerName);
            jsonObject.put("header", "Details");
            jsonObject.put("totalStores", 1);
            jsonObject.put("address", SCMTool.getCompleteAddress(SCMDataManager.getInstance().getSelectedSite().getSite()));
            jsonObject.put("nextLvl", 6);
            jsonObject.put("items", CreateShiftPerformanceDates(header));
            jsonObject.put("average", CreateAverageTemplate(header));
            jsonObject.put("total", CreateAverageTemplate(header));


        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jsonObject.toString();
    }

    private String getEquipmentParams(){
        JSONObject jsonObject = new JSONObject();
        String headers = "[\"Bluetooth\",\"Equipment\",\"Bluetooth\",\"Equipment\",\"Bluetooth\",\"Equipment\",\"Bluetooth\",\"Equipment\",\"Bluetooth\",\"Equipment\",\"Bluetooth\",\"Equipment\"]";
        String headerTitle = "[{\"cspan\":2,\"title\":\"Daypart 1\"},{\"cspan\":2,\"title\":\"Daypart 2\"},{\"cspan\":2,\"title\":\"Daypart 3\"},{\"cspan\":2,\"title\":\"Daypart 4\"},{\"cspan\":2,\"title\":\"Daypart 5\"},{\"cspan\":2,\"title\":\"Daypart 6\"}]";
        try {
            JSONArray header = new JSONArray(headers);
            JSONArray headerTitles = new JSONArray(headerTitle);

            jsonObject.put("headers",header);
            jsonObject.put("title","PIMM&trade; Shift Performance Summary");
            jsonObject.put("headerTitles", headerTitles);
            jsonObject.put("siteId", SCMDataManager.getInstance().getSelectedSite().getSite().siteid);
            jsonObject.put("subtitle", SCMDataManager.getInstance().getSelectedSite().getSite().sitename);
            jsonObject.put("spid", SCMDataManager.getInstance().getSelectedSite().getSite().CustomerName);
            jsonObject.put("header", "Details");
            jsonObject.put("totalStores", 1);
            jsonObject.put("address", SCMTool.getCompleteAddress(SCMDataManager.getInstance().getSelectedSite().getSite()));
            jsonObject.put("nextLvl", 6);
            jsonObject.put("items", CreateShiftPerformanceDates(header));
            jsonObject.put("average", CreateAverageTemplate(header));
            jsonObject.put("total", CreateAverageTemplate(header));


        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jsonObject.toString();
    }

    private String getInspectionParams(){
        JSONObject jsonObject = new JSONObject();
        String headers = "[\"Food Safety\",\"Checklist\",\"Food Safety\",\"Checklist\",\"Food Safety\",\"Checklist\",\"Food Safety\",\"Checklist\",\"Food Safety\",\"Checklist\",\"Food Safety\",\"Checklist\"]";
        String headerTitle = "[{\"cspan\":2,\"title\":\"Daypart 1\"},{\"cspan\":2,\"title\":\"Daypart 2\"},{\"cspan\":2,\"title\":\"Daypart 3\"},{\"cspan\":2,\"title\":\"Daypart 4\"},{\"cspan\":2,\"title\":\"Daypart 5\"},{\"cspan\":2,\"title\":\"Daypart 6\"}]";
        try {
            JSONArray header = new JSONArray(headers);
            JSONArray headerTitles = new JSONArray(headerTitle);

            jsonObject.put("headers",header);
            jsonObject.put("title","PIMM&trade; Shift Performance Summary");
            jsonObject.put("headerTitles", headerTitles);
            jsonObject.put("siteId", SCMDataManager.getInstance().getSelectedSite().getSite().siteid);
            jsonObject.put("subtitle", SCMDataManager.getInstance().getSelectedSite().getSite().sitename);
            jsonObject.put("spid", SCMDataManager.getInstance().getSelectedSite().getSite().CustomerName);
            jsonObject.put("header", "Details");
            jsonObject.put("totalStores", 1);
            jsonObject.put("address", SCMTool.getCompleteAddress(SCMDataManager.getInstance().getSelectedSite().getSite()));
            jsonObject.put("nextLvl", 6);
            jsonObject.put("items", CreateShiftPerformanceDates(header));
            jsonObject.put("average", CreateAverageTemplate(header));
            jsonObject.put("total", CreateAverageTemplate(header));


        } catch (JSONException e) {
            e.printStackTrace();
        }


        return jsonObject.toString();
    }


    private String CreateChecklistParams(){
        JSONObject jsonObject = new JSONObject();
        boolean isSite = false;
        final SCMDataManager data = SCMDataManager.getInstance();
        if (!data.getReportLevel().equalsIgnoreCase("site")) {
            try {
                Object selectedCorpStructure =ReportsData.getInstance().getSelectedCorpStructure();
                CorpStructure corpStructure = new CorpStructure();
                if (selectedCorpStructure != null) {
                    if (selectedCorpStructure instanceof FSLRegion) {
                        FSLRegion region = (FSLRegion) selectedCorpStructure;
                        corpStructure = region.getCorpStructure();
                    } else if (selectedCorpStructure instanceof FSLDivision) {
                        FSLDivision division = (FSLDivision) selectedCorpStructure;
                        corpStructure = division.getCorpStructure();
                    } else if (selectedCorpStructure instanceof FSLArea) {
                        FSLArea area = (FSLArea) selectedCorpStructure;
                        corpStructure = area.getCorpStructure();
                    } else if (selectedCorpStructure instanceof FSLDistrict) {
                        FSLDistrict district = (FSLDistrict) selectedCorpStructure;
                        corpStructure = district.getCorpStructure();
                    } else if (selectedCorpStructure instanceof FSLSite) {
                        FSLSite site = (FSLSite) selectedCorpStructure;
                        corpStructure = new CorpStructure();
                        corpStructure.corpStructureID = site.siteID;
                        corpStructure.layerName = site.siteName;
                        corpStructure.layer = 6;
                        isSite = true;
                    }
                } else {
                    corpStructure = new CorpStructure();
                    corpStructure.layerName = "Region";
                    corpStructure.layer = 1;
                }
                if (!isSite) {
                    JSONObject items = items(ReportsData.getInstance().getFslDaySummary());
                    jsonObject.put("headers", getChecklistHeader());
                    jsonObject.put("siteId", "87340814-8fc7-4aa0-b321-b004ecdd02d3");
                    jsonObject.put("subtitle", LayerName);
                    jsonObject.put("spid", "Wendy's International Inc.");
                    jsonObject.put("totalStores", ReportsData.getInstance().getFslDaySummary().siteList.size());
                    jsonObject.put("headerTitles", getHeaderTitles(false));
                    jsonObject.put("title", "PIMM&trade; Food Safety Summary");
                    jsonObject.put("header", LayerInidator);
                    jsonObject.put("address", "4049 E. LIVINGSTON AVE., COLUMBUS, OH");
                    jsonObject.put("nextLvl", corpStructure.layer);
                    jsonObject.put("items", items.getJSONArray("items"));
                    jsonObject.put("average", items.getJSONArray("average"));
                } else {
                    jsonObject.put("headers", getChecklistHeader());
                    jsonObject.put("siteId", data.getSelectedSite().getSite().siteid);
                    jsonObject.put("subtitle", LayerName);
                    jsonObject.put("spid", data.getSelectedSite().getSite().CustomerName);
                    jsonObject.put("totalStores", 1);
                    jsonObject.put("headerTitles", getHeaderTitles(false));
                    jsonObject.put("title", "PIMM&trade; Food Safety Summary");
                    jsonObject.put("header", LayerInidator);
                    jsonObject.put("address", SCMTool.getCompleteAddress(data.getSelectedSite().getSite()));
                    jsonObject.put("nextLvl", corpStructure.layer);
                    jsonObject.put("items", CreateDates());
                    JSONObject total_average = getFSLTotalAverage(currentFSLDaypartSummary);
                    jsonObject.put("average", total_average.getJSONArray("average"));
                    jsonObject.put("total", total_average.getJSONArray("total"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            try {
                jsonObject.put("headers", getChecklistHeader());
                jsonObject.put("siteId",data.getSelectedSite().getSite().siteid);
                jsonObject.put("subtitle",data.getSelectedSite().getSite().sitename);
                jsonObject.put("spid",data.getSelectedSite().getSite().CustomerName);
                jsonObject.put("totalStores",1);
                jsonObject.put("headerTitles",getHeaderTitles(false));
                jsonObject.put("title","PIMM&trade; Food Safety Summary");
                jsonObject.put("header","Details");
                jsonObject.put("address",SCMTool.getCompleteAddress(data.getSelectedSite().getSite()));
                jsonObject.put("nextLvl",6);
                jsonObject.put("items",CreateDates());
                JSONObject total_average = getFSLTotalAverage(currentFSLDaypartSummary);
                jsonObject.put("average",total_average.getJSONArray("average"));
                jsonObject.put("total",total_average.getJSONArray("total"));

            }catch (Exception e){
                e.printStackTrace();
            }

        }








        return jsonObject.toString();
    }

    private void setUPCustomNavigation(){
        ReportsData reportsData = ReportsData.getInstance();
        LayoutInflater layoutInflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.navitagion_item, null);
        nav_container.addView(view);
        if (reportsData.getNavigations()!=null){
            nav_container.addView(addTextView(null,false));
            int counter = 0;
            for (Object object : reportsData.getNavigations()){
                counter++;
                if (counter == reportsData.getNavigations().size()){
                    nav_container.addView(addTextView(object,true));
                }else {
                    nav_container.addView(addTextView(object,false));
                }
            }
        }else {
            nav_container.addView(addTextView(null,true));
        }
    }

    private TextView addTextView(final Object object, boolean lastItem){
        final TextView textView = new TextView(getContext());
        textView.setId(View.generateViewId());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.setMarginEnd(dpToPx(1));
        params.topMargin = dpToPx(1);
        params.bottomMargin = dpToPx(1);
        textView.setLayoutParams(params);
        textView.setPadding(dpToPx(5),dpToPx(5),dpToPx(5),dpToPx(5));
        textView.setTextSize(10);
        textView.setGravity(Gravity.CENTER_VERTICAL);
        final CorpStructure[] corpStructure = {null};

        SCMDataManager data = SCMDataManager.getInstance();
        if (!data.getReportLevel().equalsIgnoreCase("site")){

            if (applicationReport.ReportName.equalsIgnoreCase("Food Safety Summary")){
                if (object!=null){
                    if (object instanceof FSLRegion){
                        FSLRegion region = (FSLRegion) object;
                        LayerInidator = "Division";
                        LayerName = region.getCorpStructure().name;
                        textView.setText("Division");
                        System.out.println("NAV : "+region.getCorpStructure().name);
                    }
                    else if (object instanceof FSLDivision){
                        FSLDivision division = (FSLDivision) object;
                        LayerInidator = "Area";
                        LayerName = division.getCorpStructure().name;
                        textView.setText("Area");
                        System.out.println("NAV : "+division.getCorpStructure().name);
                    }
                    else if (object instanceof FSLArea){
                        FSLArea area = (FSLArea) object;
                        LayerInidator = "District";
                        LayerName = area.getCorpStructure().name;
                        textView.setText("District");
                        System.out.println("NAV : "+area.getCorpStructure().name);
                    }
                    else if (object instanceof FSLDistrict){
                        FSLDistrict district = (FSLDistrict) object;
                        LayerInidator = "Store";
                        LayerName = district.getCorpStructure().name;
                        textView.setText("Store");
                        System.out.println("NAV : "+district.getCorpStructure().name);
                    }
                    else if (object instanceof FSLSite){
                        FSLSite site = (FSLSite) object;
                        LayerInidator = "Details";
                        LayerName = site.siteName;
                        textView.setText("Details");
                    }
                }
                else {
                    corpStructure[0] = new CorpStructure();
                    corpStructure[0].layerName = "Region";
                    corpStructure[0].layer = 1;
                    textView.setText("Region");
                    LayerInidator = "Region";
                    LayerName = "Corporate";
                    textView.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.orange));
                    textView.setTextColor(ContextCompat.getColor(getContext(),R.color.white));
                }
            }else {
                corpStructure[0] = new CorpStructure();
                corpStructure[0].layerName = "Details";
                corpStructure[0].layer = 6;
                textView.setText("Details");
                LayerInidator = "Details";
                LayerName = SCMDataManager.getInstance().getSelectedSite().getSite().sitename;
            }
        }else {
            corpStructure[0] = new CorpStructure();
            corpStructure[0].layerName = "Details";
            corpStructure[0].layer = 6;
            textView.setText("Details");
            LayerInidator = "Details";
            LayerName = data.getSelectedSite().getSite().sitename;
        }
        if (lastItem){
            textView.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.orange));
            textView.setTextColor(ContextCompat.getColor(getContext(),R.color.white));
        }else {
            textView.setBackgroundColor(ContextCompat.getColor(getContext(),R.color.white));
            textView.setTextColor(ContextCompat.getColor(getContext(),R.color.black));
        }
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClearViewGroup();

                if (object!=null){
                    if (object instanceof FSLRegion){
                        FSLRegion region = (FSLRegion) object;
                        SearchSelectedItem(region.getCorpStructure().corpStructureID);
                    }
                    else if (object instanceof FSLDivision){
                        FSLDivision division = (FSLDivision) object;
                        SearchSelectedItem(division.getCorpStructure().corpStructureID);
                    }
                    else if (object instanceof FSLArea){
                        FSLArea area = (FSLArea) object;
                        SearchSelectedItem(area.getCorpStructure().corpStructureID);
                    }
                    else if (object instanceof FSLDistrict){
                        FSLDistrict district = (FSLDistrict) object;
                        SearchSelectedItem(district.getCorpStructure().corpStructureID);
                    }
                    else if (object instanceof FSLSite){
                        FSLSite site = (FSLSite) ReportsData.getInstance().getSelectedCorpStructure();
                        SearchSelectedItem(site.siteID);
                    }
                }else {
                    ReportsData.getInstance().setSelectedCorpStructure(null);
                    ReportsData.getInstance().setNavigations(null);
                }
                webView.loadUrl("file:///android_asset/RUReport/index.html");
            }
        });

        return textView;
    }

    private void ClearViewGroup(){
        nav_container.removeAllViews();
    }

    private void GenerateRuReport(String jsonString){
        if (jsonString != null){
            StringBuilder sbldr = new StringBuilder();
            sbldr.append("loadReport(");
            sbldr.append(jsonString);
            sbldr.append(")");
            System.out.println(sbldr);
            webView.evaluateJavascript(sbldr.toString(), null);
        }
    }

    private void Back() {
        requireActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_right,R.anim.slide_right_out)
                .replace(R.id.resturant_fragment_container,
                        new Reports_fragment()).commit();


    }
    @Override
    public void onDetach() {
        super.onDetach();

    }

}