package com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourceManualFragment;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourcesChildList;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourcesGrandChildList;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourcesList;

import java.util.ArrayList;


public class Resources_list_adapter extends BaseExpandableListAdapter {

    final Context context ;
    ArrayList<ResourcesList> parent_lists;
    FragmentActivity fragmentActivity;
    ExpandableListView listView;


    public Resources_list_adapter(Context context, ArrayList<ResourcesList> data, FragmentActivity fragmentActivity) {
        this.context = context;
        this.parent_lists = new ArrayList<>();
        this.parent_lists = data;
        this.fragmentActivity = fragmentActivity;

    }
    @Override
    public void onGroupExpanded(int i) {

    }

    @Override
    public void onGroupCollapsed(int i){

    }

    @Override
    public boolean isEmpty() {
            return false;
    }


    @Override
    public int getGroupCount() {

        return parent_lists.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return parent_lists.get(i).getChildLists().size();
    }

    @Override
    public Object getGroup(int i) {
        return parent_lists.get(i);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return parent_lists.get(groupPosition).getChildLists().get(childPosition);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int position, boolean b, View contentView, ViewGroup parent) {

        ResourcesList parent_data = parent_lists.get(position);
            contentView = LayoutInflater.from(context).inflate(R.layout.status_journal_parent_data, parent, false);
            TextView textView = contentView.findViewById(R.id.rowParentText);
            textView.setText(parent_data.getTitle());

            return contentView;
    }


    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean b, View contentView, final ViewGroup parent) {
        final ResourcesChildList child_data = parent_lists.get(groupPosition).getChildLists().get(childPosition);

            contentView = LayoutInflater.from(context).inflate(R.layout.resources_leftpane_childlist, parent, false);
            final TextView textView = contentView.findViewById(R.id.title);
            final LinearLayout LastlevelContainer = contentView.findViewById(R.id.lastview_container);

            final ArrayList<View>last_view = new ArrayList<>();
            final ArrayList<ResourcesGrandChildList>dailies = child_data.getGrandChildLists();

            textView.setText(child_data.getAppString());

            if (dailies.size()>0){
                for (int i = 0; i <dailies.size() ; i++) {
                    final ResourcesGrandChildList daily = dailies.get(i);
                    View lastview = LayoutInflater.from(context).inflate(R.layout.resources_leftpane_grandchildlist, parent, false);
                    final TextView title = lastview.findViewById(R.id.title);
                    title.setText(daily.getDocumentDTO().name);
                    final int finalI = i;
                    lastview.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                for (ResourcesList parent : parent_lists){
                                    for (ResourcesChildList childList : parent.getChildLists()){
                                        for (ResourcesGrandChildList grandChildList : childList.getGrandChildLists()){
                                           grandChildList.setSelected(false);
                                        }
                                    }
                                }
                                daily.setSelected(true);
                                notifyDataSetChanged();

                                DisplayPDF(fragmentActivity,daily);

                            }
                        });

                        if (daily.isSelected()){
                            lastview.setBackgroundColor(Color.parseColor("#FDA528"));
                        }else {
                            lastview.setBackgroundColor(Color.TRANSPARENT);
                        }

                    LastlevelContainer.addView(lastview);
                    last_view.add(lastview);
                }
            }

            if (child_data.isSelected()){
                LastlevelContainer.setVisibility(View.VISIBLE);
            }else {
                LastlevelContainer.setVisibility(View.GONE);
            }

            contentView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (child_data.isSelected()) {
                        child_data.setSelected(false);
                    }
                    else {
                        child_data.setSelected(true);
                    }
                    notifyDataSetChanged();

                }
            });

        return contentView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }

    private void DisplayPDF(FragmentActivity fragmentActivity, ResourcesGrandChildList grandChildList) {
        fragmentActivity.getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_left,R.anim.slide_left_out)
                .replace(R.id.resturant_fragment_container,
                        new Resources_right_pane_fragment(grandChildList)).commit();
    }


}