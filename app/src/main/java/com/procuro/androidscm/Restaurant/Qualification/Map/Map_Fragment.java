package com.procuro.androidscm.Restaurant.Qualification.Map;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.load.resource.bitmap.CenterInside;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.Qualification.QualificationActivity;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.Qualification.QualificationActivityFragment;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Area;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_District;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Division;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Regional;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static android.content.ContentValues.TAG;



public class Map_Fragment extends Fragment implements OnMapReadyCallback,
        com.google.maps.android.clustering.ClusterManager.OnClusterClickListener<SampleClusterItem>,
        com.google.maps.android.clustering.ClusterManager.OnClusterInfoWindowClickListener<SampleClusterItem>,
        com.google.maps.android.clustering.ClusterManager.OnClusterItemClickListener<SampleClusterItem>,
        com.google.maps.android.clustering.ClusterManager.OnClusterItemInfoWindowClickListener<SampleClusterItem>{

    public static GoogleMap mMap;
    private static final int REQUEST_LOCATION_PERMISSION = 1;
    public static Marker marker;
    public static ArrayList<LatLng> markers = new ArrayList<LatLng>();
    public static LatLngBounds.Builder builder = new LatLngBounds.Builder();

    private SupportMapFragment mapFragment;
    private SampleClusterItem clickedClusterItem;
    private List<SampleClusterItem> clusterItems = new ArrayList<>();

    private Corp_Regional regional;
    private Corp_Division division;
    private Corp_District district;
    private Corp_Area area;
    private CustomSite customSite;
    private View rootView;
    private com.procuro.apimmdatamanagerlib.aPimmDataManager dataManager;
    public static boolean isStore = false;

    public Map_Fragment() {

    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
            rootView = inflater.inflate(R.layout.activity_maps, container,false);

            mapFragment = (SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.map);
            Objects.requireNonNull(mapFragment).getMapAsync(Map_Fragment.this);


        return rootView;

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setCompassEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        getarguments();


    }

    private int checkSelfPermission(String accessFineLocation) {
        return 0;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.v("LOGIN", "ACCESS_FINE_LOCATION is now granted");
                }
                return;
            }
        }
    }


    public void cluster_function(ArrayList<CustomSite>customSites){
        try {
            final com.google.maps.android.clustering.ClusterManager<SampleClusterItem> clusterManager = new com.google.maps.android.clustering.ClusterManager<>(requireContext(), mMap);
            clusterManager.setRenderer(new OwnIconRendered(getContext(), mMap, clusterManager));
            mMap.setOnCameraIdleListener(clusterManager);

            if (customSites!=null){

                for (CustomSite customSite : customSites){
                    LatLng latLng = new LatLng(customSite.getSite().LatLon.Lat,customSite.getSite().LatLon.Lon);
                    BitmapDescriptor icon = null;

                    int height = SCMTool.dpToPx(65);
                    int width = SCMTool.dpToPx(45);

                    BitmapDrawable bitmapdraw= null;

                    if (customSite.getSite()!=null){

                        if (customSite.getSite().os.equalsIgnoreCase("Custom")){
                            icon = SCMTool.getMapIcon(R.drawable.map_store_light_blue,getContext());
                        }
                        else {
                            if (customSite.getSite().AlarmSeverity == 0 ){
                                icon = SCMTool.getMapIcon(R.drawable.map_store_blue,getContext());

                            }else if (customSite.getSite().AlarmSeverity == 3 ){
                                icon = SCMTool.getMapIcon(R.drawable.map_store_green,getContext());


                            }else if (customSite.getSite().AlarmSeverity == 5 ){
                                icon = SCMTool.getMapIcon(R.drawable.map_store_yellow,getContext());

                            }else if (customSite.getSite().AlarmSeverity == 9 ){
                                icon = SCMTool.getMapIcon(R.drawable.map_store_red,getContext());

                            }else {
                                icon = SCMTool.getMapIcon(R.drawable.map_store_gray,getContext());

                            }
                        }
                    }else {
                        icon = SCMTool.getMapIcon(R.drawable.map_store_gray,getContext());

                    }
                    clusterItems.add(new SampleClusterItem(latLng, customSite.getSite().sitename,null,icon,customSite));

                }

                clusterManager.addItems(clusterItems);
                mMap.setOnInfoWindowClickListener(clusterManager);
                mMap.setInfoWindowAdapter(clusterManager.getMarkerManager());
                mMap.setOnMarkerClickListener(clusterManager);
                clusterManager.getMarkerCollection().setInfoWindowAdapter(new MyCustomAdapterForItems(SCMDataManager.getInstance().getCustomSites()));
                clusterManager.setOnClusterClickListener(this);
                clusterManager.setOnClusterInfoWindowClickListener(this);
                clusterManager.setOnClusterItemClickListener(this);
                clusterManager.setOnClusterItemInfoWindowClickListener(this);


                clusterManager
                        .setOnClusterItemClickListener(new com.google.maps.android.clustering.ClusterManager.OnClusterItemClickListener<SampleClusterItem>() {
                            @Override
                            public boolean onClusterItemClick(SampleClusterItem item) {
                                clickedClusterItem = item;
                                return false;
                            }
                        });
                clusterManager.cluster();

                clusterManager.setOnClusterClickListener(new com.google.maps.android.clustering.ClusterManager.OnClusterClickListener<SampleClusterItem>() {

                    @Override
                    public boolean onClusterClick(com.google.maps.android.clustering.Cluster<SampleClusterItem> cluster) {
                        ClusterClick((ArrayList<SampleClusterItem>) cluster.getItems());

                        return false;
                    }
                });
                clusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<SampleClusterItem>() {
                    @Override
                    public boolean onClusterItemClick(SampleClusterItem sampleClusterItem) {
                        Log.d(TAG, "onItemClusterClick");
                        return false;
                    }
                });
            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }


    @Override
    public boolean onClusterClick(com.google.maps.android.clustering.Cluster<SampleClusterItem> cluster) {

        System.out.println(cluster.getPosition());

        return true;
    }

    @Override
    public void onClusterInfoWindowClick(com.google.maps.android.clustering.Cluster<SampleClusterItem> cluster) {

    }

    @Override
    public boolean onClusterItemClick(SampleClusterItem sampleClusterItem) {
        return true;
    }

    @Override
    public void onClusterItemInfoWindowClick(SampleClusterItem sampleClusterItem) {

    }

    static class OwnIconRendered extends DefaultClusterRenderer<SampleClusterItem> {

        private ArrayList<Integer>severities = new ArrayList<>();
        private ArrayList<String> os = new ArrayList<>();
        int color = Color.parseColor("#9AC9E9");

        @Override
        public Cluster<SampleClusterItem> getCluster(Marker marker) {
            return super.getCluster(marker);

        }

        @Override
        protected int getBucket(Cluster<SampleClusterItem> cluster) {
            return cluster.getSize();
        }

        @Override
        protected void onClusterItemRendered(SampleClusterItem clusterItem, Marker marker) {
            super.onClusterItemRendered(clusterItem, marker);
            if (isStore){
                marker.showInfoWindow();
            }
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster<SampleClusterItem> cluster) {
            severities = new ArrayList<>();
            os = new ArrayList<>();

            for (SampleClusterItem item : cluster.getItems()){
                CustomSite customSite = item.getCustomSite();
                if (customSite.getSite()!=null){
                    if (customSite.getSite().os.equalsIgnoreCase("Custom")){
                        os.add("Custom");
                    }else {
                        if (!severities.contains(customSite.getSite().AlarmSeverity)){
                            severities.add(customSite.getSite().AlarmSeverity);
                        }
                    }
                }
            }

            Collections.sort(severities,Collections.<Integer>reverseOrder());
            if (os.contains("Custom")){
                color = Color.parseColor("#9AC9E9");

            }else {

                if (severities.get(0) == 10){
                    color = Color.parseColor("#515151");

                }else if (severities.get(0) == 9){

                    color = Color.parseColor("#D91E25");

                }else if (severities.get(0) == 5){
                    color = Color.parseColor("#ED8030");


                }else if (severities.get(0) == 3){
                    color = Color.parseColor("#337D34");
                }
            }

            getColor(color);

            return cluster.getSize() >= 2;
        }

        @Override
        protected String getClusterText(int bucket) {


            return super.getClusterText(bucket).replace("+", "");
        }
        @Override
        protected int getColor(int clustercolor) {

           clustercolor = color;

            return clustercolor;
        }

        public OwnIconRendered(Context context, GoogleMap map,
                               com.google.maps.android.clustering.ClusterManager<SampleClusterItem> clusterManager) {
            super(context, map, clusterManager);

        }

        @Override
        protected void onBeforeClusterItemRendered(SampleClusterItem item, MarkerOptions markerOptions) {
            markerOptions.icon(item.getIcon());
            markerOptions.snippet(item.getSnippet());
            markerOptions.title(item.getTitle());

            super.onBeforeClusterItemRendered(item, markerOptions);

        }


        @Override
        protected void onBeforeClusterRendered(Cluster<SampleClusterItem> cluster, MarkerOptions markerOptions) {
            super.onBeforeClusterRendered(cluster, markerOptions);

        }
    }

    public class MyCustomAdapterForItems implements GoogleMap.InfoWindowAdapter  {
        private final View myContentsView;
        private TextView name;
        private TextView address;
        private ArrayList<CustomSite>customSites;
        private LinearLayout scorcard_container;


        MyCustomAdapterForItems(ArrayList<CustomSite>customSites) {
            myContentsView = getLayoutInflater().inflate(
                    R.layout.qualification_map_layout, null);

            this.customSites = customSites;

            name = myContentsView.findViewById(R.id.name);
            address = myContentsView.findViewById(R.id.address);
            scorcard_container = myContentsView.findViewById(R.id.scorcard_container);

        }
        @Override
        public View getInfoWindow(final Marker marker) {
         return null;
        }
        @Override
        public View getInfoContents(final Marker marker) {

            for (final CustomSite customSite :customSites){
                if (marker.getTitle().equalsIgnoreCase(customSite.getSite().sitename)){
                    name.setText(marker.getTitle());
                    address.setText(SCMTool.getCompleteAddress(customSite.getSite()));


                    myContentsView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(getContext(), ""+marker.getTitle(), Toast.LENGTH_SHORT).show();

                        }
                    });

                    mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                        @Override
                        public void onInfoWindowClick(Marker marker) {
                            if (QualificationData.getInstance().isAllstore()){
                                QualificationData.getInstance().setSelectedItem(customSite.getSite().sitename);
                                QualificationData.getInstance().setSelectedSite(customSite);
                                QualificationActivityFragment.scoreCard.setChecked(true);
                                QualificationActivityFragment.scoreCard.setEnabled(true);
                                QualificationActivityFragment.scoreCard.setAlpha(1);

                            }else {
                                QualificationData.getInstance().setSelectedItem(customSite.getSite().sitename);
                                QualificationData.getInstance().setSelectedSite(customSite);
                                QualificationActivityFragment.scoreCard.setChecked(true);
                                QualificationActivityFragment.scoreCard.setEnabled(true);
                                QualificationActivityFragment.scoreCard.setAlpha(1);
                                QualificationActivityFragment.userProfile.setEnabled(true);
                                QualificationActivityFragment.userProfile.setAlpha(1);
                                QualificationActivityFragment.storeProfile.setEnabled(true);
                                QualificationActivityFragment.storeProfile.setAlpha(1);
                            }

                        }
                    });
                    break;
                }
            }
            return myContentsView;
        }

    }

    private void getarguments(){

        if (QualificationData.getInstance().isAllstore()){
            QualificationActivityFragment.userProfile.setAlpha(.5f);
            QualificationActivityFragment.storeProfile.setAlpha(.5f);
            QualificationActivityFragment.scoreCard.setAlpha(.5f);
            QualificationActivityFragment.userProfile.setEnabled(false);
            QualificationActivityFragment.storeProfile.setEnabled(false);
            QualificationActivityFragment.scoreCard.setEnabled(false);

            ShowAllstoreMarker(SCMDataManager.getInstance().getCustomSites());

        }
        else if (QualificationData.getInstance().isOwnerShipViewEnabled()) {
            Corp_Regional regional = QualificationData.getInstance().getRegional();
            if (regional!=null){
                if (regional.getCorpStructures().name.equalsIgnoreCase(QualificationData.getInstance().getSelectedItem())){
                    this.regional = regional;
                    QualificationActivityFragment.storeProfile.setEnabled(false);
                    QualificationActivityFragment.storeProfile.setAlpha(.5f);
                    QualificationActivityFragment.scoreCard.setEnabled(false);
                    QualificationActivityFragment.scoreCard.setAlpha(.5f);
                    QualificationActivityFragment.name.setText(this.regional.getCorpStructures().name);
                    ShowRegionLevelMarker(this.regional);

                }else {

                    if (regional.getQualificationDivisions()!=null){
                        for (Corp_Division division : regional.getQualificationDivisions()){
                            if (division.getCorpStructures().name.equalsIgnoreCase(QualificationData.getInstance().getSelectedItem())){
                                this.division = division;

                                QualificationActivityFragment.storeProfile.setEnabled(false);
                                QualificationActivityFragment.storeProfile.setAlpha(.5f);
                                QualificationActivityFragment.scoreCard.setEnabled(false);
                                QualificationActivityFragment. scoreCard.setAlpha(.5f);
                                QualificationActivityFragment.name.setText(this.division.getCorpStructures().name);
                                ShowDivisionLevelMarker(this.division);
                                break;

                            }else {
                                if (division.getQualificationAreas()!=null){

                                    for (Corp_Area area : division.getQualificationAreas()){

                                        if (area.getCorpStructures().name.equalsIgnoreCase(QualificationData.getInstance().getSelectedItem())){
                                            this.area = area;
                                            QualificationActivityFragment.storeProfile.setEnabled(false);
                                            QualificationActivityFragment.storeProfile.setAlpha(.5f);
                                            QualificationActivityFragment.scoreCard.setEnabled(false);
                                            QualificationActivityFragment. scoreCard.setAlpha(.5f);
                                            QualificationActivityFragment. name.setText(this.area.getCorpStructures().name);
                                            ShowAreaLevelMarker(this.area);
                                            break;

                                        }else {

                                            if (area.getQualificationDistricts()!=null){

                                                for (Corp_District district : area.getQualificationDistricts()){

                                                    if (district.getCorpStructures().name.equalsIgnoreCase(QualificationData.getInstance().getSelectedItem())){
                                                        this.district = district;
                                                        QualificationActivityFragment.storeProfile.setEnabled(false);
                                                        QualificationActivityFragment.storeProfile.setAlpha(.5f);
                                                        QualificationActivityFragment.scoreCard.setEnabled(false);
                                                        QualificationActivityFragment. scoreCard.setAlpha(.5f);
                                                        QualificationActivityFragment. name.setText(this.district.getCorpStructures().name);
                                                        ShowDistrictLevelMarker(this.district);
                                                        break;

                                                    }else {
                                                        if (district.getSites()!=null){

                                                            for (CustomSite  customSite  : district.getSites()){
                                                                if (customSite.getSite().sitename.equalsIgnoreCase(QualificationData.getInstance().getSelectedItem())){
                                                                    this.customSite = customSite;
                                                                    QualificationActivityFragment.name.setText(this.customSite.getSite().sitename);
                                                                    QualificationActivityFragment. storeProfile.setEnabled(true);
                                                                    QualificationActivityFragment.storeProfile.setAlpha(1);
                                                                    QualificationActivityFragment.scoreCard.setEnabled(true);
                                                                    QualificationActivityFragment.scoreCard.setAlpha(1);
                                                                    ShowsiteLevelMarker(this.customSite);
                                                                    QualificationData.getInstance().setSelectedSite(customSite);
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
        else if (QualificationData.getInstance().isStateByStateViewEnabled()){
            if(QualificationData.getInstance().getSelectedSite()!=null){
                this.customSite = QualificationData.getInstance().getSelectedSite();
                QualificationActivityFragment.name.setText(this.customSite.getSite().sitename);
                ShowsiteLevelMarker(this.customSite);
                QualificationData.getInstance().setSelectedSite(customSite);
            }
        }
        else {
            this.customSite = SCMDataManager.getInstance().getSelectedSite();
            QualificationActivityFragment.name.setText(this.customSite.getSite().sitename);
            ShowsiteLevelMarker(this.customSite);
            QualificationData.getInstance().setSelectedSite(customSite);
        }

    }

    public  void ShowAllstoreMarker(ArrayList<CustomSite>customSites) {
        try {
            final ArrayList<LatLng>markers = new ArrayList<>();
            ArrayList<CustomSite>sites = new ArrayList<>();

            for (CustomSite customSite :customSites){
                if (customSite.isMerged()){
                    LatLng latLng = new LatLng(customSite.getSite().LatLon.Lat,customSite.getSite().LatLon.Lon);
                    markers.add(latLng);
                    sites.add(customSite);
                }
            }
            cluster_function(sites);
            SCMTool.moveToSelectedMarkers(requireContext(),mMap,markers);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public  void ShowRegionLevelMarker(Corp_Regional region) {
        try {
            final ArrayList<LatLng>markers = new ArrayList<>();
            ArrayList<CustomSite>sites = new ArrayList<>();
            ArrayList<Corp_Division>divisions = region.getQualificationDivisions();
            if (divisions!=null){
                for (Corp_Division division : divisions){
                    if (division.getQualificationAreas()!=null){
                        for (Corp_Area area : division.getQualificationAreas()){
                            if (area.getQualificationDistricts()!=null){
                                for (Corp_District district : area.getQualificationDistricts()){
                                    if (district.getSites()!=null){
                                        for (CustomSite customSite : district.getSites()){
                                            LatLng latLng = new LatLng(customSite.getSite().LatLon.Lat,customSite.getSite().LatLon.Lon);
                                            markers.add(latLng);
                                            sites.add(customSite);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            cluster_function(sites);
            SCMTool.moveToSelectedMarkers(requireContext(),mMap,markers);

        }catch (Exception e){
            e.printStackTrace();

        }




    }

    public  void ShowDivisionLevelMarker(Corp_Division division) {

        final ArrayList<LatLng>markers = new ArrayList<>();
        ArrayList<CustomSite>sites = new ArrayList<>();

        for (Corp_Area area : division.getQualificationAreas()){
            if (area.getQualificationDistricts()!=null){
                for (Corp_District district : area.getQualificationDistricts()){
                    if (district.getSites()!=null){
                        for (CustomSite customSite : district.getSites()){
                            LatLng latLng = new LatLng(customSite.getSite().LatLon.Lat,customSite.getSite().LatLon.Lon);
                            markers.add(latLng);
                            sites.add(customSite);
                        }
                    }
                }
            }
        }

        try {

            cluster_function(sites);
            SCMTool.moveToSelectedMarkers(requireContext(),mMap,markers);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public  void ShowDistrictLevelMarker(Corp_District district) {
        try {
            final ArrayList<LatLng>markers = new ArrayList<>();
            ArrayList<CustomSite>sites = new ArrayList<>();

            if (district.getSites()!=null){
                for (CustomSite customSite : district.getSites()){
                    LatLng latLng = new LatLng(customSite.getSite().LatLon.Lat,customSite.getSite().LatLon.Lon);
                    markers.add(latLng);
                    sites.add(customSite);
                }
            }

            cluster_function(sites);
            SCMTool.moveToSelectedMarkers(requireContext(),mMap,markers);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public  void ShowAreaLevelMarker(Corp_Area area) {
        try {
            final ArrayList<LatLng>markers = new ArrayList<>();
            ArrayList<CustomSite>sites = new ArrayList<>();

            for (Corp_District district : area.getQualificationDistricts()){
                if (district.getSites()!=null){
                    for (CustomSite customSite : district.getSites()){
                        LatLng latLng = new LatLng(customSite.getSite().LatLon.Lat,customSite.getSite().LatLon.Lon);
                        markers.add(latLng);
                        sites.add(customSite);
                    }
                }
            }

            cluster_function(sites);
            SCMTool.moveToSelectedMarkers(requireContext(),mMap,markers);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public  void ShowsiteLevelMarker(final CustomSite customSite) {
        try {
            isStore = true;
            ArrayList<CustomSite>customSites = new ArrayList<>();
            ArrayList<LatLng>markers = new ArrayList<>();

            LatLng latLng = new LatLng(customSite.getSite().LatLon.Lat,customSite.getSite().LatLon.Lon);
            markers.add(latLng);
            customSites.add(customSite);
            final float width = (int)(getResources().getDisplayMetrics().widthPixels*0.017);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, width));
            cluster_function(customSites);




        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public  void ClusterClick(ArrayList<SampleClusterItem> clusterItems) {
        try {
            final ArrayList<LatLng>markers = new ArrayList<>();
            for (SampleClusterItem clusterItem : clusterItems){
                CustomSite customSite = clusterItem.getCustomSite();
                LatLng latLng = new LatLng(customSite.getSite().LatLon.Lat,customSite.getSite().LatLon.Lon);
                markers.add(latLng);
            }

            SCMTool.moveToSelectedMarkers(requireContext(),mMap,markers);
        }catch (Exception e){
            e.printStackTrace();
        }
    }



}