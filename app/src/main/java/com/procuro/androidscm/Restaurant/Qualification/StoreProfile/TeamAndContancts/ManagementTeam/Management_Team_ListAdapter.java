package com.procuro.androidscm.Restaurant.Qualification.StoreProfile.TeamAndContancts.ManagementTeam;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.procuro.androidscm.GlideApp;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.StoreProfileOptions.KitchenCriteria;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.StoreProfileOptions.KitchenDescription;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.StoreProfileOptions.StoreProfileOptionsData;
import com.procuro.androidscm.Restaurant.UserProfile.CustomRole;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileActivity;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileData;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsUserRole;
import com.suke.widget.SwitchButton;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.procuro.androidscm.Restaurant.Qualification.StoreProfile.TeamAndContancts.Qualification_TeamAndContancts_Fragment.district_manager;
import static com.procuro.androidscm.Restaurant.Qualification.StoreProfile.TeamAndContancts.Qualification_TeamAndContancts_Fragment.district_manager_container;


public class Management_Team_ListAdapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<CustomUser> arraylist;
    private FragmentActivity fragmentActivity;


    public Management_Team_ListAdapter(Context context, ArrayList<CustomUser> arraylist, FragmentActivity fragmentActivity) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;
        this.fragmentActivity = fragmentActivity;

    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return arraylist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {

        final CustomUser userRole = arraylist.get(position);
        view = inflater.inflate(R.layout.management_team_list_data, null);

        TextView name = view.findViewById(R.id.name);
        TextView role = view.findViewById(R.id.role);
        TextView phone = view.findViewById(R.id.number);
        TextView email = view.findViewById(R.id.email);
        ImageView icon = view.findViewById(R.id.icon);

        try {
            String fullaneme = userRole.getUser().firstName +" "+ userRole.getUser().lastName;
            name.setText(SCMTool.CheckString(fullaneme,"--"));
            phone.setText(SCMTool.CheckString(userRole.getUser().dayPhone,""));
            email.setText(SCMTool.CheckString(userRole.getUser().username,""));


            String roleName = "--";

            if (userRole.getUser().roles!=null) {
                ArrayList<CustomRole> customRoles = CustomRole.getAllCustomRole(userRole.getUser().roles);
                if (customRoles.size() >= 1) {
                    roleName = (customRoles.get(0).getName());
                }
            }



            if (roleName.equalsIgnoreCase("General Manager")){
                role.setText(roleName);
                GlideApp.with(mContext).asDrawable().load(R.drawable.general_manager).into(icon);
            }
            else if (roleName.equalsIgnoreCase("Support Manager")){
                role.setText(roleName);
                GlideApp.with(mContext).asDrawable().load(R.drawable.support_manager).into(icon);

            }else {
                role.setText(roleName);
                GlideApp.with(mContext).asDrawable().load(R.drawable.general_manager).into(icon);


            }



            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    QualificationData qualificationData = QualificationData.getInstance();
                    Intent intent = new Intent(fragmentActivity, UserProfileActivity.class);
                    UserProfileData profileData = new UserProfileData();
                    profileData.setUserEditEnabled(false);
                    profileData.setCustomSite(qualificationData.getSelectedSite());
                    profileData.setCustomUser(userRole);
                    profileData.setFromQualification(true);
                    profileData.setIsupdate(true);
                    profileData.setPrevActivity("MT");
                    UserProfileData.setInstance(profileData);
                    fragmentActivity.startActivity(intent);

                }
            });


        }catch (Exception e){
            e.printStackTrace();
        }



        return view;
    }
}

