package com.procuro.androidscm.Restaurant.Calendar;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileData;
import com.procuro.androidscm.SCMDataManager;

import java.util.ArrayList;


public class CalendarEventListAdapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<CalendarEvent> events;
    boolean EnableEdit ;

    public CalendarEventListAdapter(Context context, ArrayList<CalendarEvent> events) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.events = events;
    }

    @Override
    public int getCount() {

        return events.size();
    }

    @Override
    public Object getItem(int position) {
        return events.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
        view = inflater.inflate(R.layout.user_sitelist_data, null);
        CalendarEvent event = events.get(position);


        return view;
    }


}

