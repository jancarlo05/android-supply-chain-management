package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_StoreRecord;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.Employee;
import com.procuro.apimmdatamanagerlib.SMSDailyOpsPlanSpeedOfServiceItem;
import com.procuro.apimmdatamanagerlib.SpeedOfServiceItemDaypartItems;

import java.util.ArrayList;


public class Dashboard_StoreRecord_listviewAdapter extends BaseAdapter {

    // Declare Variables

    Context mContext;
    LayoutInflater inflater;
    private ArrayList<SMSDailyOpsPlanSpeedOfServiceItem> arraylist;


    public Dashboard_StoreRecord_listviewAdapter(Context context, ArrayList<SMSDailyOpsPlanSpeedOfServiceItem> arraylist) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;

    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return arraylist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.store_record_child_list_data, null);
            view.setTag(holder);

            SMSDailyOpsPlanSpeedOfServiceItem item = arraylist.get(position);
            TextView name = view.findViewById(R.id.title);
            TextView d1goal,d2goal,d3goal,d4goal,d5goal,d6goal;
            TextView d1actual,d2actual,d3actual,d4actual,d5actual,d6actual;

            d1goal = view.findViewById(R.id.goal_d1);
            d2goal = view.findViewById(R.id.goal_d2);
            d3goal = view.findViewById(R.id.goal_d3);
            d4goal = view.findViewById(R.id.goal_d4);
            d5goal = view.findViewById(R.id.goal_d5);
            d6goal = view.findViewById(R.id.goal_d6);

            d1actual = view.findViewById(R.id.actual_d1);
            d2actual = view.findViewById(R.id.actual_d2);
            d3actual = view.findViewById(R.id.actual_d3);
            d4actual = view.findViewById(R.id.actual_d4);
            d5actual = view.findViewById(R.id.actual_d5);
            d6actual = view.findViewById(R.id.actual_d6);


            name.setText(item.item);

            for (SpeedOfServiceItemDaypartItems daypart : item.daypartItems){

                if (daypart.daypart == 1){
                    if (daypart.goal!=null){
                        if (!daypart.goal.equalsIgnoreCase("null")){
                            d1goal.setText(daypart.goal);
                        }
                    }
                    if (daypart.actual!=null){
                        if (!daypart.actual.equalsIgnoreCase("null")){
                            d1actual.setText(daypart.actual);
                        }
                    }
                }

                if (daypart.daypart == 2){
                    if (daypart.goal!=null){
                        if (!daypart.goal.equalsIgnoreCase("null")){
                            d2goal.setText(daypart.goal);
                        }
                    }
                    if (daypart.actual!=null){
                        if (!daypart.actual.equalsIgnoreCase("null")){
                            d2actual.setText(daypart.actual);
                        }
                    }

                }

                if (daypart.daypart == 3){
                    if (daypart.goal!=null){
                        if (!daypart.goal.equalsIgnoreCase("null")){
                            d3goal.setText(daypart.goal);
                        }
                    }
                    if (daypart.actual!=null){
                        if (!daypart.actual.equalsIgnoreCase("null")){
                            d3actual.setText(daypart.actual);
                        }
                    }

                }

                if (daypart.daypart == 4){
                    if (daypart.goal!=null){
                        if (!daypart.goal.equalsIgnoreCase("null")){
                            d4goal.setText(daypart.goal);
                        }
                    }
                    if (daypart.actual!=null){
                        if (!daypart.actual.equalsIgnoreCase("null")){
                            d4actual.setText(daypart.actual);
                        }
                    }

                }

                if (daypart.daypart == 5){
                    if (daypart.goal!=null){
                        if (!daypart.goal.equalsIgnoreCase("null")){
                            d5goal.setText(daypart.goal);
                        }
                    }
                    if (daypart.actual!=null){
                        if (!daypart.actual.equalsIgnoreCase("null")){
                            d5actual.setText(daypart.actual);
                        }
                    }

                }

                if (daypart.daypart == 6){
                    if (daypart.goal!=null){
                        if (!daypart.goal.equalsIgnoreCase("null")){
                            d6goal.setText(daypart.goal);
                        }
                    }
                    if (daypart.actual!=null){
                        if (!daypart.actual.equalsIgnoreCase("null")){
                            d6actual.setText(daypart.actual);
                        }
                    }
                }
            }

        return view;
    }


}

