package com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.StorePrep;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.R;
import com.procuro.apimmdatamanagerlib.StorePrepData;


public class StorePrep_Breakfast_RecyclerViewAdapter extends RecyclerView.Adapter<StorePrep_Breakfast_RecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private CustomStorePrepData customStorePrepData;

    public StorePrep_Breakfast_RecyclerViewAdapter(Context context, CustomStorePrepData storePrepData) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.customStorePrepData = storePrepData;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.prep_hour_list_data,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final StorePrepData storePrepData = customStorePrepData.storePrepData.get(position);

        holder.time.setText(storePrepData.time);
        holder.employee_count.setText(String.valueOf(storePrepData.totalEmployees));
        holder.position.setText(getPositionsByEmployees(storePrepData.totalEmployees));

        if (customStorePrepData.StoreprepSchedule.equalsIgnoreCase("Breakfast")){
            if (storePrepData.time.equalsIgnoreCase("06:30 AM")){
                CreateGreenMarker(holder.time,holder.indicator,holder.body);

            }else if (storePrepData.time.equalsIgnoreCase("10:30 AM")){
                CreateRedMarker(holder.time,holder.indicator,holder.body);

            }else {
                DefaultMarker(holder.time,holder.indicator,holder.body);
            }

        }else if (customStorePrepData.StoreprepSchedule.equalsIgnoreCase("Lunch")){

          if (storePrepData.time.equalsIgnoreCase("10:30 AM")){
                CreateGreenMarker(holder.time,holder.indicator,holder.body);
            }else {
                DefaultMarker(holder.time,holder.indicator,holder.body);
            }

        }else if (customStorePrepData.StoreprepSchedule.equalsIgnoreCase("Total")){
            DefaultMarker(holder.time,holder.indicator,holder.body);
            TotalDefaultMarker(holder.time,holder.indicator,holder.body,holder.position);
        }




    }

    @Override
    public int getItemCount() {
        return customStorePrepData.storePrepData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView time,position,indicator,employee_count;
        LinearLayout body;

        public MyViewHolder(View itemView) {
            super(itemView);
            time = itemView.findViewById(R.id.time);
            employee_count = itemView.findViewById(R.id.employee_count);
            indicator = itemView.findViewById(R.id.indicator);
            position = itemView.findViewById(R.id.position);
            body = itemView.findViewById(R.id.body);
        }
    }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }


    private void CreateGreenMarker(TextView time,TextView indicator,LinearLayout body){
        indicator.setText("Open");
        indicator.setBackgroundColor(ContextCompat.getColor(mContext,R.color.green));
        time.setBackgroundColor(ContextCompat.getColor(mContext,R.color.green));
        body.setBackgroundColor(ContextCompat.getColor(mContext,R.color.green_alpha));

    }

    private void CreateRedMarker(TextView time,TextView indicator,LinearLayout body){
        indicator.setBackgroundColor(ContextCompat.getColor(mContext,R.color.red));
        indicator.setText("Close");
        time.setBackgroundColor(ContextCompat.getColor(mContext,R.color.red));
        body.setBackgroundColor(ContextCompat.getColor(mContext,R.color.red_alpha));

    }

    private void DefaultMarker(TextView time,TextView indicator,LinearLayout body){
        indicator.setVisibility(View.INVISIBLE);
        time.setBackgroundColor(ContextCompat.getColor(mContext,R.color.black));
        body.setBackgroundColor(ContextCompat.getColor(mContext,R.color.transparent));

    }

    private void TotalDefaultMarker(TextView time,TextView indicator,LinearLayout body,TextView position){
        indicator.setVisibility(View.GONE);
        time.setBackgroundColor(ContextCompat.getColor(mContext,R.color.black));
        body.setBackgroundColor(View.GONE);
        position.setVisibility(View.GONE);

    }

    private String getPositionsByEmployees(int totalEMployee){
        StringBuilder stringBuilder = new StringBuilder();

        if (totalEMployee == 1){
            stringBuilder.append("Supervisor");
        }
        else if (totalEMployee == 2){
            stringBuilder.append("Supervisor");
            stringBuilder.append("\n");
            stringBuilder.append("Crew");
        }
        else if (totalEMployee == 3){
            stringBuilder.append("Supervisor");
            stringBuilder.append("\n");
            stringBuilder.append("Crew,");
            stringBuilder.append("\n");
            stringBuilder.append("Crew");
        }
        else if (totalEMployee == 4){
            stringBuilder.append("Manager");
            stringBuilder.append("\n");
            stringBuilder.append("Lettuce");
            stringBuilder.append("\n");
            stringBuilder.append("Salads");
            stringBuilder.append("\n");
            stringBuilder.append("Open Ops");
        }
        else if (totalEMployee == 5){
            stringBuilder.append("Manager");
            stringBuilder.append("\n");
            stringBuilder.append("Lettuce");
            stringBuilder.append("\n");
            stringBuilder.append("Salads");
            stringBuilder.append("\n");
            stringBuilder.append("Open Ops");
            stringBuilder.append("\n");
            stringBuilder.append("Bacon");
        }else {
            stringBuilder.append("--");
        }

        return stringBuilder.toString();
    }

}
