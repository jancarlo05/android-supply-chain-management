package com.procuro.androidscm.Restaurant.UserProfile;

import android.app.role.RoleManager;

import com.procuro.apimmdatamanagerlib.IFTA_ECM;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class CustomRole {

    private String name;
    private int status;
    private String code;

    public CustomRole() {

    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public CustomRole(String name, int status, String code) {
        this.name = name;
        this.status = status;
        this.code = code;
    }

    public CustomRole(String name, int status) {
        this.name = name;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public static ArrayList<CustomRole> getAllCustomRole(ArrayList<String>roles){
        ArrayList<CustomRole>customRoles = new ArrayList<>();
        if (roles!=null){

            for (String strings: roles){

                if (strings.equalsIgnoreCase("GM".toUpperCase())){
                    customRoles.add(new CustomRole("General Manager",2,"GM"));

                }
                else if (strings.equalsIgnoreCase("Store admin".toUpperCase())){
                    customRoles.add(new CustomRole("Store admin",4,"STORE ADMIN"));

                }
                else if (strings.equalsIgnoreCase("Auditor".toUpperCase())){
                    customRoles.add(new CustomRole("Auditor",7,"AUDITOR"));
                }
                else if (strings.equalsIgnoreCase("Ops leader".toUpperCase())){
                    customRoles.add(new CustomRole("Support Manager",5,"OPS LEADER"));

                }
                else if (strings.equalsIgnoreCase("FSL".toUpperCase())) {
                    customRoles.add(new CustomRole("Inspector",8,"FSL"));
                }
                else if (strings.equalsIgnoreCase("Employee".toUpperCase())) {
                    customRoles.add(new CustomRole("Crew",9,"EMPLOYEE"));

                }else if (strings.equalsIgnoreCase("DM".toUpperCase())) {
                    customRoles.add(new CustomRole("District Manager",1,"DM"));


                }else if (strings.equalsIgnoreCase("RELIEF CREW".toUpperCase())) {
                    customRoles.add(new CustomRole("Relief Crew",10,"RELIEF CREW"));

                }
                else if (strings.equalsIgnoreCase("RELIEF MGR".toUpperCase())) {
                    customRoles.add(new CustomRole("Relief Manager",3,"RELIEF MGR"));

                }
                else if (strings.equalsIgnoreCase("SUPERVISOR".toUpperCase())) {
                    customRoles.add(new CustomRole("Supervisor",6,"SUPERVISOR"));

                }
                else if (strings.equalsIgnoreCase("FORM ADMIN".toUpperCase())) {
                    customRoles.add(new CustomRole("FORM ADMIN",11,"FORM ADMIN"));

                }

            }

            if (customRoles.size()>0){
                Collections.sort(customRoles, new Comparator<CustomRole>() {
                    @Override
                    public int compare(CustomRole o1, CustomRole o2) {
                        return Integer.compare(o1.status, o2.status);
                    }
                });
            }

        }
        return customRoles;
    }


    public static CustomRole convertCodetoTitle(String strings){

        CustomRole customRoles = null;

        if (strings.equalsIgnoreCase("GM".toUpperCase())){
            customRoles = (new CustomRole("General Manager",2,"GM"));

        }
        else if (strings.equalsIgnoreCase("Store admin".toUpperCase())){
            customRoles = (new CustomRole("Store admin",4,"STORE ADMIN"));

        }
        else if (strings.equalsIgnoreCase("Auditor".toUpperCase())){
            customRoles = (new CustomRole("Auditor",7,"AUDITOR"));
        }
        else if (strings.equalsIgnoreCase("Ops leader".toUpperCase())){
            customRoles = (new CustomRole("Support Manager",5,"OPS LEADER"));

        }
        else if (strings.equalsIgnoreCase("FSL".toUpperCase())) {
            customRoles = (new CustomRole("Inspector",8,"FSL"));
        }
        else if (strings.equalsIgnoreCase("Employee".toUpperCase())) {
            customRoles = (new CustomRole("Employee",9,"EMPLOYEE"));

        }else if (strings.equalsIgnoreCase("DM".toUpperCase())) {
            customRoles = (new CustomRole("District Manage",1,"DM"));


        }else if (strings.equalsIgnoreCase("RELIEF CREW".toUpperCase())) {
            customRoles = (new CustomRole("Relief Crew",10,"RELIEF CREW"));

        }
        else if (strings.equalsIgnoreCase("RELIEF MGR".toUpperCase())) {
            customRoles = (new CustomRole("Relief Manager",3,"RELIEF MGR"));

        }
        else if (strings.equalsIgnoreCase("SUPERVISOR".toUpperCase())) {
            customRoles = (new CustomRole("Supervisor",6,"SUPERVISOR"));

        }
        return customRoles;
    }


}
