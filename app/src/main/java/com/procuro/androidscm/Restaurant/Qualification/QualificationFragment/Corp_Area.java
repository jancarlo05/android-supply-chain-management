package com.procuro.androidscm.Restaurant.Qualification.QualificationFragment;

import com.procuro.apimmdatamanagerlib.CorpStructure;

import java.util.ArrayList;

public class Corp_Area {

    CorpStructure corpStructures;
    private ArrayList<Corp_District> qualificationDistricts;
    private boolean selected;

    public Corp_Area() {

    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public Corp_Area(CorpStructure corpStructures, ArrayList<Corp_District> qualificationDistricts) {
        this.corpStructures = corpStructures;
        this.qualificationDistricts = qualificationDistricts;

    }

    public Corp_Area(CorpStructure corpStructures) {
        this.corpStructures = corpStructures;
    }

    public CorpStructure getCorpStructures() {
        return corpStructures;
    }

    public void setCorpStructures(CorpStructure corpStructures) {
        this.corpStructures = corpStructures;
    }

    public ArrayList<Corp_District> getQualificationDistricts() {
        return qualificationDistricts;
    }

    public void setQualificationDistricts(ArrayList<Corp_District> qualificationDistricts) {
        this.qualificationDistricts = qualificationDistricts;
    }
}
