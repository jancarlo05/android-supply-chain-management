package com.procuro.androidscm.Restaurant.DashBoard;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import androidx.fragment.app.DialogFragment;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.DopHuddle.Dashboard_dop_huddle_tasks_listviewAdapter;
import com.procuro.apimmdatamanagerlib.SMSDailyOpsPlanCustomerSatisfactionItem;

import java.util.ArrayList;
import java.util.Objects;

public class Dashboard_dop_huddle_DialogFragment extends DialogFragment {

    public static ArrayList<SMSDailyOpsPlanCustomerSatisfactionItem> items;

    static Dashboard_dop_huddle_DialogFragment newInstance(ArrayList<SMSDailyOpsPlanCustomerSatisfactionItem> item) {
        items = item;
            return new Dashboard_dop_huddle_DialogFragment();
    }
        @Override
        public int getTheme() {
        return R.style.slide_left;
        }

    @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.dashboard_dop_huddle_dfragment, container, false);

//                    int height = (int)(getResources().getDisplayMetrics().heightPixels*0.90);
//                    int width = 320;
//
                    int width = (int)(getResources().getDisplayMetrics().widthPixels*0.90);
                    int height = (int)(getResources().getDisplayMetrics().heightPixels*0.80);
                    Objects.requireNonNull(getDialog().getWindow()).setLayout(width, height);
                    getDialog().setCanceledOnTouchOutside(true);
                    getDialog().getWindow().getAttributes().windowAnimations = R.style.slide_left;
                    view.setFocusableInTouchMode(true);

                    getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    Button cancel = view.findViewById(R.id.back_button);

                    ListView listView = view.findViewById(R.id.listview);

                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dismiss();
                        }
                    });

                    Dashboard_dop_huddle_tasks_listviewAdapter listviewAdapter =
                            new Dashboard_dop_huddle_tasks_listviewAdapter(getContext(),items);
                    listView.setAdapter(listviewAdapter);

        return view;

        }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

}
