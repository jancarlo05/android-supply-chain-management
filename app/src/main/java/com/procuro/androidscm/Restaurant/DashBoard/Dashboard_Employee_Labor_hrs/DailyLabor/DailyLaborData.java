package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.DailyLabor;

import com.procuro.androidscm.Daypart;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_SalePerformance.SalesPerformanceDaypartHeader;
import com.procuro.androidscm.Restaurant.Schema.Dayparts;

import java.util.ArrayList;

public class DailyLaborData {
    Dayparts daypart;
    ArrayList<String>titles;

    ArrayList<SalesPerformanceDaypartHeader> daypartHeaders;

    private ArrayList<Double>forcasts;
    private ArrayList<Double>sales;
    private ArrayList<Double>projectedHrs;
    private ArrayList<Double>scheduledHrs;
    private ArrayList<Double>optimumHrs;
    private ArrayList<Double>laborHrs;
    private ArrayList<Double>commulativeHrs;



    public Dayparts getDaypart() {
        return daypart;
    }

    public void setDaypart(Dayparts daypart) {
        this.daypart = daypart;
    }

    public ArrayList<String> getTitles() {
        return titles;
    }

    public void setTitles(ArrayList<String> titles) {
        this.titles = titles;
    }

    public ArrayList<SalesPerformanceDaypartHeader> getDaypartHeaders() {
        return daypartHeaders;
    }

    public void setDaypartHeaders(ArrayList<SalesPerformanceDaypartHeader> daypartHeaders) {
        this.daypartHeaders = daypartHeaders;
    }

    public ArrayList<Double> getForcasts() {
        return forcasts;
    }

    public void setForcasts(ArrayList<Double> forcasts) {
        this.forcasts = forcasts;
    }

    public ArrayList<Double> getSales() {
        return sales;
    }

    public void setSales(ArrayList<Double> sales) {
        this.sales = sales;
    }

    public ArrayList<Double> getProjectedHrs() {
        return projectedHrs;
    }

    public void setProjectedHrs(ArrayList<Double> projectedHrs) {
        this.projectedHrs = projectedHrs;
    }

    public ArrayList<Double> getScheduledHrs() {
        return scheduledHrs;
    }

    public void setScheduledHrs(ArrayList<Double> scheduledHrs) {
        this.scheduledHrs = scheduledHrs;
    }

    public ArrayList<Double> getOptimumHrs() {
        return optimumHrs;
    }

    public void setOptimumHrs(ArrayList<Double> optimumHrs) {
        this.optimumHrs = optimumHrs;
    }

    public ArrayList<Double> getLaborHrs() {
        return laborHrs;
    }

    public void setLaborHrs(ArrayList<Double> laborHrs) {
        this.laborHrs = laborHrs;
    }

    public ArrayList<Double> getCommulativeHrs() {
        return commulativeHrs;
    }

    public void setCommulativeHrs(ArrayList<Double> commulativeHrs) {
        this.commulativeHrs = commulativeHrs;
    }
}
