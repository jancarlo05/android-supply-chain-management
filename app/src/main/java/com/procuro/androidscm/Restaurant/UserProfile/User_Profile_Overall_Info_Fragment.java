package com.procuro.androidscm.Restaurant.UserProfile;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.service.autofill.UserData;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.CertificationDefinition;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.User;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;


public class User_Profile_Overall_Info_Fragment extends Fragment {

    public static TextView birthdate,state,permissions,pin,fullname,userRole,
    carrier,hiredate,reviewdate;
    private View view;

    public static EditText firstname,lastname,username,employee_id,email,city,address,mobile,zipcode,password;

    public static LinearLayout root;
    public static Date SelectedBirthdate ,SelectedHireDate ,SelectedReviewDate;
    public static boolean PermissionEnableEdit = false;

    private ConstraintLayout bod_container,hiredate_container,reviewDate_container,
            stateContainer,permissionContainer,carrierContainer ;

    private LinearLayout password_text_container;
    


    //skills
    private User_site_allowed_list_adapter adapter;
    private User_Skills_RecyclerViewAdapter recyclerViewAdapter;
    private RecyclerView recyclerView;
    private info.hoang8f.android.segmented.SegmentedGroup infgroup;
    private RadioButton opsSkill,positionSkill,productSkill,mgmtskill;
    private ConstraintLayout siteAllowed_Container;
    public static TextView siteAllowed;
    private String siteTitle = "" ;
    private boolean EnableEdit;
    private User user;


    public User_Profile_Overall_Info_Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
         view = inflater.inflate(R.layout.user_personal_overall_info_fragment, container, false);

        firstname = view.findViewById(R.id.firstname);
        lastname = view.findViewById(R.id.lastname);
        username = view.findViewById(R.id.username);
        employee_id = view.findViewById(R.id.employee_id);
        email = view.findViewById(R.id.email);
        birthdate = view.findViewById(R.id.birthdate);
        pin = view.findViewById(R.id.personal_id_number);
        permissions = view.findViewById(R.id.permision);
        address = view.findViewById(R.id.address);
        city = view.findViewById(R.id.city);
        state = view.findViewById(R.id.state);
        zipcode = view.findViewById(R.id.zipcode);
        mobile = view.findViewById(R.id.mobile);
        carrier = view.findViewById(R.id.carrier);
        hiredate = view.findViewById(R.id.hire_date);
        reviewdate = view.findViewById(R.id.review_date);
        bod_container = view.findViewById(R.id.bod_container);
        hiredate_container = view.findViewById(R.id.hire_date_container);
        reviewDate_container = view.findViewById(R.id.review_date_container);
        stateContainer = view.findViewById(R.id.state_container);
        permissionContainer = view.findViewById(R.id.permission_contanier);
        carrierContainer = view.findViewById(R.id.carrier_container);
        password_text_container = view.findViewById(R.id.password_text_container);
        fullname = view.findViewById(R.id.fullname);
        userRole = view.findViewById(R.id.userRole);
        password = view.findViewById(R.id.password);
        root = view.findViewById(R.id.root);

        mgmtskill = view.findViewById(R.id.mgmtskill);
        recyclerView = view.findViewById(R.id.recyclerView);
        infgroup = view.findViewById(R.id.segmented2);
        opsSkill = view.findViewById(R.id.opsSkill);
        positionSkill = view.findViewById(R.id.position_skills);
        productSkill = view.findViewById(R.id.product_skills);
        siteAllowed_Container = view.findViewById(R.id.site_allowed_container);
        siteAllowed = view.findViewById(R.id.site_allowed);


        getArgument();

        setupOnclicks();

        return view;

    }



    private void setupOnclicks(){

        bod_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayDatePicker(getContext(),bod_container,"bod",Gravity.TOP,birthdate);
            }
        });


        reviewDate_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayDatePicker(getContext(),reviewDate_container,"reviewDate",Gravity.TOP,reviewdate);
            }
        });



        hiredate_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayDatePicker(getContext(),hiredate_container,"hiredate",Gravity.TOP,hiredate);
            }
        });


        pin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayNumpad(getContext(),pin,pin);
            }
        });


        carrierContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayCarrier(getContext(),carrierContainer,"Select Carrier",carrier);
            }
        });

        stateContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayCarrier(getContext(),stateContainer,"Select State",state);

            }
        });

        permissionContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayCarrier(getContext(),permissionContainer,"Select Title",permissions);
            }
        });

        siteAllowed_Container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplaySiteAllowedPopup(getContext(),siteAllowed_Container);
            }
        });

    }

    public void DisplaySiteAllowedPopup(final Context context, View anchor) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.TOP)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .margin(R.dimen.simpletooltip_margin)
                .padding(R.dimen.simpletooltip_padding)
                .transparentOverlay(true)
                .arrowColor(ContextCompat.getColor(context, R.color.dirty_white))
                .contentView(R.layout.site_allowed_layout)
                .focusable(true)
                .build();
        tooltip.show();

        ListView listView = tooltip.findViewById(R.id.listView);
        Button button = tooltip.findViewById(R.id.apply);

        ArrayList<String>filtere_site = getAvailableSitesAllowed();

        adapter = new User_site_allowed_list_adapter(getContext(),filtere_site,EnableEdit);
        listView.setAdapter(adapter);

        try {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DisplaySelectedSite();
                    tooltip.dismiss();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private ArrayList<String> getAvailableSitesAllowed(){
        UserProfileData data = UserProfileData.getInstance();

        ArrayList<String>filtere_site = new ArrayList<>();
        try {
            ArrayList<CustomSite>customSites = SCMDataManager.getInstance().getCustomSites();
            ArrayList<String>roles = new ArrayList<>();

            if (!data.isCreate()){
                if (user.roles!=null){
                    roles = user.roles;
                }
            }
            if (UserProfileData.getInstance().getUpdate_Siteallowed()==null){

                for (CustomSite customSite : customSites){

                    if (roles.contains(customSite.getSite().sitename)){

                        filtere_site.add(customSite.getSite().sitename);

                        System.out.println("ADD: "+customSite.getSite().sitename);
                        if (UserProfileData.getInstance().getSelectedSites()==null){

                            UserProfileData.getInstance().setSelectedSites(new ArrayList<CustomSite>());
                            UserProfileData.getInstance().getSelectedSites().add(customSite);

                        }else {
                            UserProfileData.getInstance().getSelectedSites().add(customSite);
                        }
                    }
                }
                UserProfileData.getInstance().setUpdate_Siteallowed(filtere_site);
                UserProfileData.getInstance().setCurrent_Site_Allowed(new ArrayList<>(filtere_site));

            }else {
                filtere_site = UserProfileData.getInstance().getUpdate_Siteallowed();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        return filtere_site;
    }



    private void InializeEditHandlers(){

        final UserProfileData data = UserProfileData.getInstance();

        firstname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                data.setFirstName(s.toString());

                StringBuilder builder = new StringBuilder();
                builder.append(s.toString());
                builder.append(" ");
                if (data.getLastName()!=null){
                    builder.append(data.getLastName());
                }
                fullname.setText(builder.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        lastname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                data.setLastName(s.toString());

                StringBuilder builder = new StringBuilder();
                if (data.getFirstName()!=null){
                    builder.append(data.getFirstName());
                    builder.append(" ");
                }
                builder.append(s.toString());
                fullname.setText(builder.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                data.setUsername(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });


        employee_id.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                data.setEmployeeID(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        pin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                data.setpID(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                data.setAddress1(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        city.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                data.setCity(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        zipcode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                data.setPostal(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        mobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                data.setDayPhone(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                data.setContactEmail(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        state.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                data.setState(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });


        carrier.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                data.setMobileCarrier(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });


        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                data.setPassword(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    private void getArgument(){

        User user = null;
        Site site = null;

        UserProfileData data = UserProfileData.getInstance();
        if (data.isFromQuickAccess()){
            user =  SCMDataManager.getInstance().getUser();
            site = SCMDataManager.getInstance().getSelectedSite().getSite();
            this.user = user;
            siteAllowed.setText(SCMDataManager.getInstance().getSelectedSite().getSite().sitename);

        }
        else if (data.isFromQualification()){
            user = data.getCustomUser().getUser();
            site = data.getCustomSite().getSite();
            this.user = user;

            CheckSitesAllowed();
        }

        InializeEditHandlers();

        DisplayData(user,site);

        Check_Update_Create_Function(data,user);

    }

    private void setupSkills(){
        try {

            infgroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == opsSkill.getId()){
                        DisplaypOpsSkills();
                    }else if (checkedId == positionSkill.getId()){
                        DisplaypositionSkills();

                    }else if (checkedId == productSkill.getId()){
                        DisplaypProductSkills();
                    }
                    else if (checkedId == mgmtskill.getId()){
                        DisplayManagementSkills();
                    }
                }
            });

            positionSkill.setChecked(true);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void DisplaypositionSkills(){
        //CATEGORY 1
        DisplayData(getSKillsByCategory("0"));

    }

    private void DisplaypProductSkills(){
        //CATEGORY 1
        DisplayData(getSKillsByCategory("1"));
    }

    private void DisplaypOpsSkills(){
        //CATEGORY 2
        DisplayData(getSKillsByCategory("2"));

    }

    private void DisplayManagementSkills(){
        //CATEGORY 3
        DisplayData(getSKillsByCategory("3"));
    }

    private ArrayList<CertificationDefinition> getSKillsByCategory(String category){
        ArrayList<CertificationDefinition> skills = new ArrayList<>();
        ArrayList<CertificationDefinition>certificationDefinitions = SCMDataManager.getInstance().getCertificationDefinitions();

        for (CertificationDefinition certificationDefinition : certificationDefinitions){
            if (certificationDefinition.category.equalsIgnoreCase(category)){
                skills.add(certificationDefinition);
            }
        }
        return  skills;
    }

    private void DisplayData(ArrayList<CertificationDefinition> skills){
        recyclerViewAdapter  = new User_Skills_RecyclerViewAdapter(getContext(),skills,EnableEdit,user);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),3));
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    private void CheckSitesAllowed(){
        UserProfileData data = UserProfileData.getInstance();
        if (data.getCustomSite()!=null){
            if (data.getCustomSite().getSite()!=null){
                siteAllowed.setText(data.getCustomSite().getSite().sitename);
            }else {
                DisplaySelectedSite();
            }
        } else {
            DisplaySelectedSite();
        }
    }

    private void DisplaySelectedSite(){
        UserProfileData data = UserProfileData.getInstance();
        if (data.isCreate()){
            if (data.getSelectedSites()!=null){
                if (data.getSelectedSites().size()>0){
                    siteAllowed.setText(data.getSelectedSites().get(0).getSite().sitename);
                }
            }
        }
    }

    private void DisplayData(User user,Site site){
        UserProfileData data = UserProfileData.getInstance();
        view.setFocusableInTouchMode(true);
        SimpleDateFormat format = new SimpleDateFormat("MMM/dd/yyyy");
        format.setTimeZone(TimeZone.getTimeZone(offset(SCMDataManager.getInstance().getSelectedSite().getSite().effectiveUTCOffset)));
        try {

            fullname.setText(SCMTool.CheckString(user.getFullName(),"--"));

            if (data.getFirstName()==null){
                if (user.firstName != null){
                    firstname.setText(user.firstName);
                }
            }else {
                firstname.setText(data.getFirstName());
            }

            if (data.getLastName()==null){
                if (user.lastName != null){
                    lastname.setText(user.lastName);
                }
            }else {
                lastname.setText(data.getLastName());
            }

            if (data.getUsername()==null){
                if (user.username != null){
                    username.setText(user.username);
                }
            }else {
                username.setText(data.getUsername());
            }

            if (data.getPassword()==null){
                if (user.password != null){
                    password.setText(user.password);
                }
            }else {
                password.setText(data.getPassword());
            }
            password.setEnabled(false);

            if (data.getEmployeeID()==null){
                if (user.employeeID != null) {
                    if (!user.employeeID.equalsIgnoreCase("null")) {
                        employee_id.setText(user.employeeID);
                    }
                }
            }else {
                employee_id.setText(data.getEmployeeID());
            }


            if (data.getPassword()!=null){
                employee_id.setText(data.getPassword());
            }

            if (data.getContactEmail()==null){
                if (user.contactEmail != null){
                    if (!user.contactEmail.equalsIgnoreCase("null")) {
                        email.setText(user.contactEmail);
                    }

                }
            }else {
                email.setText(data.getContactEmail());
            }

            if (data.getDOB()==null){
                if (user.DOB != null){
                    birthdate.setText(format.format(user.DOB));
                }
            }else {
                birthdate.setText(format.format(data.getDOB()));
            }


            if (data.getpID()==null){

                pin.setText(ValidString(user.pID));

            }else {
                pin.setText(data.getpID());
            }

            if (data.getCity()==null){

                city.setText(ValidString(user.city));

            }else {
                city.setText(data.getCity());
            }

            if (data.getState()==null){
                state.setText(ValidString(user.state));

            }else {
                state.setText(data.getState());
            }


            if (data.getPostal()==null){
                zipcode.setText(ValidString(user.postal));

            }else {
                zipcode.setText(data.getPostal());
            }

            if (data.getDayPhone()==null){
                mobile.setText(ValidString(user.dayPhone));

            }else {
                mobile.setText(data.getDayPhone());
            }


            if (data.getHireDate()==null){
                if (user.hireDate != null){
                    hiredate.setText(format.format(user.hireDate));
                }
            }else {
                hiredate.setText(format.format(data.getHireDate()));
            }

            if (data.getReviewDate()==null){
                if (user.reviewDate != null){
                    reviewdate.setText(format.format(user.reviewDate));
                }
            }else {
                reviewdate.setText(format.format(data.getReviewDate()));
            }

            if (data.getMobileCarrier()==null){
                carrier.setText(ValidString(user.mobileCarrier));

            }else {
                carrier.setText(data.getMobileCarrier());
            }


            if (data.getAddress1()==null){
                address.setText(ValidString(user.address1));

            }else {
                address.setText(data.getAddress1());
            }


            if (data.getRole()==null){
                if (user.roles!=null){
                    ArrayList<CustomRole>customRoles = CustomRole.getAllCustomRole(user.roles);

                if (customRoles.size()>0){
                    CustomRole customRole = customRoles.get(0);
                    permissions.setText(customRole.getName());
                    userRole.setText(customRole.getName());
                }
            }
            }else {
                permissions.setText(CustomRole.convertCodetoTitle(data.getRole()).getName());
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void DisableEDit(ViewGroup vg){
        try {
            for (int i = 0; i < vg.getChildCount(); i++){
                View child = vg.getChildAt(i);
                child.setEnabled(false);
                if (child instanceof ViewGroup){
                    DisableEDit((ViewGroup)child);
                }
            }
            SCMTool.DisableView(recyclerView,1);
            SCMTool.DisableView(UserProfileActivityOverAll.editlock,1);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void EnableEDit(ViewGroup vg){

        try {
            vg.setAlpha(1);
            for (int i = 0; i < vg.getChildCount(); i++){
                View child = vg.getChildAt(i);
                child.setEnabled(true);
                if (child instanceof ViewGroup){
                    EnableEDit((ViewGroup)child);
                }
            }

            if (!EnableEdit){
                SCMTool.DisableView(permissionContainer,1);
                SCMTool.DisableView(siteAllowed_Container,1);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void DisplayDatePicker(final Context context, View anchor, final String title, int gravity, final TextView textView) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(gravity)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .arrowColor(ContextCompat.getColor(context,R.color.orange))
                .transparentOverlay(true)
                .contentView(R.layout.date_picker)
                .focusable(true)
                .build();
        tooltip.show();


        final DatePicker datePicker =tooltip.findViewById(R.id.datepicker);
        Button accept = tooltip.findViewById(R.id.apply);


        final SimpleDateFormat dateFormat = new SimpleDateFormat("MMM/dd/yyyy");


        datePicker.setMaxDate(System.currentTimeMillis());
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar date = Calendar.getInstance();

                date.set(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());

                if (title.equalsIgnoreCase("BOD")){
                    UserProfileData.getInstance().setDOB(removeTime(date.getTime()));
                }
               else if (title.equalsIgnoreCase("hiredate")){
                    UserProfileData.getInstance().setHireDate(removeTime(date.getTime()));
                }
                else if (title.equalsIgnoreCase("reviewDate")){
                    UserProfileData.getInstance().setReviewDate(removeTime(date.getTime()));
                }

                textView.setText(dateFormat.format(date.getTime()));

                tooltip.dismiss();
            }
        });

    }

    public void DisplayNumpad(final Context context, View anchor, final TextView textView) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.BOTTOM)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .arrowColor(ContextCompat.getColor(context,R.color.dirty_white))
                .transparentOverlay(true)
                .contentView(R.layout.pin_number_register)
                .focusable(true)
                .build();
        tooltip.show();


        final TextView name,ans1,ans2,ans3,ans4,num1,num2,num3,num4,num5,num6,num7,num8,num9,num0,delete;

        ans1 = tooltip.findViewById(R.id.ans1);
        ans2 = tooltip.findViewById(R.id.ans2);
        ans3 = tooltip.findViewById(R.id.ans3);
        ans4 = tooltip.findViewById(R.id.ans4);
        num0 = tooltip.findViewById(R.id.num0);
        num1 = tooltip.findViewById(R.id.num1);
        num2 = tooltip.findViewById(R.id.num2);
        num3 = tooltip.findViewById(R.id.num3);
        num4 = tooltip.findViewById(R.id.num4);
        num5 = tooltip.findViewById(R.id.num5);
        num6 = tooltip.findViewById(R.id.num6);
        num7 = tooltip.findViewById(R.id.num7);
        num8 = tooltip.findViewById(R.id.num8);
        num9 = tooltip.findViewById(R.id.num9);
        delete = tooltip.findViewById(R.id.delete);

        final StringBuilder answer = new StringBuilder();


        try {

            num1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("1");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            num2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("2");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            num3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("3");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            num4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("4");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            num5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("5");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            num6.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("6");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            num7.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("7");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            num8.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("8");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            num9.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("9");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            num0.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("0");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.toString().length()>0){
                        answer.setLength(answer.length() - 1);
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });


        }catch (Exception e){
            e.printStackTrace();
        }



    }

    private void populateStringInBox(TextView ans1,TextView ans2,TextView ans3,
                                     TextView ans4,String answer,SimpleTooltip tooltip,TextView pim){
        try {
            ans1.setBackgroundResource(R.drawable.empty_circle_black);
            ans2.setBackgroundResource(R.drawable.empty_circle_black);
            ans3.setBackgroundResource(R.drawable.empty_circle_black);
            ans4.setBackgroundResource(R.drawable.empty_circle_black);


            if (answer.length()>0){
                for (int i = 0; i <answer.length() ; i++) {
                    if (i == 0){
                        ans1.setBackgroundResource(R.drawable.full_circle_black);
                    }else if (i == 1){
                        ans2.setBackgroundResource(R.drawable.full_circle_black);
                    }else if (i == 2){
                        ans3.setBackgroundResource(R.drawable.full_circle_black);
                    }else if (i == 3){
                        ans4.setBackgroundResource(R.drawable.full_circle_black);
                    }
                }
            }

            if (answer.length()==4){
                pin.setText(answer);
                tooltip.dismiss();
            }


        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void DisplayCarrier(final Context context, View anchor, final String title, final TextView textView) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.BOTTOM)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .arrowColor(ContextCompat.getColor(context,R.color.white))
                .transparentOverlay(true)
                .contentView(R.layout.custom_picker)
                .focusable(true)
                .build();
        tooltip.show();


        final NumberPicker picker =tooltip.findViewById(R.id.picker);
        TextView pickertitle = tooltip.findViewById(R.id.picker_title);
        Button accept = tooltip.findViewById(R.id.apply);
        pickertitle.setText(title);


        if (title.toLowerCase().contains("Carrier".toLowerCase())){

            final String[] values= {"AT&T","T-Mobile", "Verizon"};

            picker.setDisplayedValues(values);
            picker.setMinValue(0);
            picker.setMaxValue(values.length-1);
            picker.setWrapSelectorWheel(true);
            picker.setValue(values.length/2);


            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserProfileData.getInstance().setMobileCarrier(values[picker.getValue()]);
                    textView.setText(values[picker.getValue()]);
                    tooltip.dismiss();
                }
            });



        }else if (title.toLowerCase().contains("State".toLowerCase())){
            final String[] countries = SCMDataManager.getInstance().getCountries();
            final String[] countrycode = SCMDataManager.getInstance().getCountrycode();


            picker.setDisplayedValues(countries);
            picker.setMinValue(0);
            picker.setMaxValue(countries.length-1);
            picker.setWrapSelectorWheel(false);
            picker.setValue(1);


            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserProfileData.getInstance().setState(countrycode[picker.getValue()]);
                    textView.setText(countrycode[picker.getValue()]);

                    tooltip.dismiss();
                }
            });



        }else if (title.toLowerCase().contains("Title".toLowerCase())){
            final String[] role_title= {"Crew","District Manager","General Manager","Relief Crew","Relief Manager","Supervisor","Support Manager"};
            final String[] role_code= {"EMPLOYEE", "DM","GM","RELIEF CREW","RELIEF MGR","SUPERVISOR","OPS LEADER"};

            picker.setDisplayedValues(role_title);
            picker.setMinValue(0);
            picker.setMaxValue(role_title.length-1);
            picker.setWrapSelectorWheel(true);
            picker.setValue(role_title.length/2);


            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    textView.setText(role_title[picker.getValue()]);
                    userRole.setText(role_title[picker.getValue()]);
                    UserProfileData.getInstance().setRole(role_code[picker.getValue()]);
                    tooltip.dismiss();

                }
            });
        }

    }


    private void Check_Update_Create_Function(UserProfileData data, User user){

        if (data.isIsupdate()){

            if (user.active){

                getCurrentUserRole();

                if (data.isUserEditEnabled()){
                    EnableEDit(root);
                }else {
                    DisableEDit(root);
                }

                SCMTool.DisableView(password,.5f);

            }else {
                EnableEdit= false;
                DisableEDit(root);
                UserProfileActivityOverAll.editlock.setVisibility(View.GONE);
            }

            password_text_container.setVisibility(View.GONE);

        }else if (data.isCreate()){

            EnableEdit= true;
            EnableEDit(root);
            UserProfileActivityOverAll.editlock.setVisibility(View.GONE);
            InitializeSiteAllowedForCreate();
            password.setEnabled(true);
            setupSkills();

        }
    }

    private void InitializeSiteAllowedForCreate(){
        try {
            UserProfileData data = UserProfileData.getInstance();
            QualificationData qualidata = QualificationData.getInstance();
            if (data.getUpdate_Siteallowed()==null){
                data.setUpdate_Siteallowed(new ArrayList<String>());
                data.getUpdate_Siteallowed().add(qualidata.getSelectedSite().getSite().sitename);
                siteAllowed.setText(qualidata.getSelectedSite().getSite().sitename);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private String ValidString(String s){
        String string = "";
        if (s!=null){
            if (!s.equalsIgnoreCase("null")){
                string = s;
            }
        }
        return string;
    }

    private String offset(int value){
        StringBuilder offset = new StringBuilder();
        offset.append("GMT");
        offset.append(value);
        offset.append(":00");

        return offset.toString();
    }

    private  Date removeTime(Date date) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    private void getCurrentUserRole(){

        User currentUser = SCMDataManager.getInstance().getUser();
        if (currentUser.roles!=null){
            ArrayList<CustomRole>customRoles = CustomRole.getAllCustomRole(currentUser.roles);

            if (customRoles.size()>0){
                CustomRole customRole = customRoles.get(0);

                if (customRole.getStatus()>4){

                    SCMTool.DisableView(permissionContainer,1);
                    SCMTool.DisableView(siteAllowed_Container,1);
                    SCMTool.EnableView(recyclerView,1);
                    SCMTool.DisableView(recyclerView,1);


                    EnableEdit = false;
                    if (currentUser.userId.equalsIgnoreCase(user.userId)){
                        SCMTool.EnableView(UserProfileActivityOverAll.editlock,1);
                    }else {
                        SCMTool.DisableView(UserProfileActivityOverAll.edit,1);
                    }
                    setupSkills();
                }else {
                    if (UserProfileData.getInstance().isUserEditEnabled()){
                        EnableEdit = true;
                        SCMTool.EnableView(UserProfileActivityOverAll.delete,1);
                    }else {
                        EnableEdit = false;
                        SCMTool.DisableView(UserProfileActivityOverAll.delete,.5f);
                    }
                }
            }
        }
        setupSkills();
    }


}
