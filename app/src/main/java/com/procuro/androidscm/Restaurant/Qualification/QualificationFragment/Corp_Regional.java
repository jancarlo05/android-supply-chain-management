package com.procuro.androidscm.Restaurant.Qualification.QualificationFragment;

import com.procuro.androidscm.Restaurant.Status.StatusFragment.StoreListFragment.SiteList;
import com.procuro.apimmdatamanagerlib.CorpStructure;

import java.util.ArrayList;

public class Corp_Regional {

    CorpStructure corpStructures;
    ArrayList<Corp_Division> qualificationDivisions;
    boolean selected;
    ArrayList<SiteList>siteLists;

    public ArrayList<SiteList> getSiteLists() {
        return siteLists;
    }

    public void setSiteLists(ArrayList<SiteList> siteLists) {
        this.siteLists = siteLists;
    }

    public Corp_Regional(CorpStructure corpStructures, ArrayList<Corp_Division> qualificationDivisions) {
        this.corpStructures = corpStructures;
        this.qualificationDivisions = qualificationDivisions;
    }

    public Corp_Regional() {

    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public Corp_Regional(CorpStructure corpStructures) {
        this.corpStructures = corpStructures;
    }


    public CorpStructure getCorpStructures() {
        return corpStructures;
    }

    public void setCorpStructures(CorpStructure corpStructures) {
        this.corpStructures = corpStructures;
    }

    public ArrayList<Corp_Division> getQualificationDivisions() {
        return qualificationDivisions;
    }

    public void setQualificationDivisions(ArrayList<Corp_Division> qualificationDivisions) {
        this.qualificationDivisions = qualificationDivisions;
    }
}
