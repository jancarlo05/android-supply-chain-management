package com.procuro.androidscm.Restaurant.Qualification.QualificationFragment;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.procuro.androidscm.Logout_Confirmation_DialogFragment;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.DashboardActivity;
import com.procuro.androidscm.Restaurant.Qualification.QualificationActivityFragment;
import com.procuro.androidscm.Restaurant.Qualification.QualificationSearchAdapters.Qualification_CorpView_OwnerShipView_SearchAdapter;
import com.procuro.androidscm.Restaurant.Qualification.QualificationSearchAdapters.Qualification_StateByState_SearchAdapter;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.QuickAccessMenu.QuickAccessMenu;
import com.procuro.androidscm.Restaurant.SOS.SOSActivity;
import com.procuro.androidscm.Restaurant.SOS.SOSData;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.StoreListFragment.SiteList;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.CorpStructure;
import com.procuro.apimmdatamanagerlib.Site;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;


public class QualificationFragment extends Fragment {


    public static ExpandableListView expandableListView;
    private Button logout,filter,Dashboard, profile, quick_a_menu, sos,Covid;;
    public static ProgressBar progressBar;
    public static TextView  wait,username,allstore,date,no_data_message;
    public static AutoCompleteTextView search;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, MMM dd, yyyy");

    public QualificationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.qualification_fragment, container, false);

        expandableListView = view.findViewById(R.id.expandible_listview);
        logout = view.findViewById(R.id.home);
        search = view.findViewById(R.id.search);
        progressBar = view.findViewById(R.id.progressBar2);
        wait = view.findViewById(R.id.wait);
        filter = view.findViewById(R.id.filter);
        username = view.findViewById(R.id.username);
        allstore = view.findViewById(R.id.all_store);
        date = view.findViewById(R.id.date);
        Dashboard = view.findViewById(R.id.dashboard);
        quick_a_menu = view.findViewById(R.id.quick_a_menu);
        sos = view.findViewById(R.id.sos);
        Covid = view.findViewById(R.id.covid);
        no_data_message = view.findViewById(R.id.no_data_message);

        search.setFocusableInTouchMode(true);
        search.setDropDownVerticalOffset(10);
        search.setDropDownBackgroundResource(R.drawable.rounded_white_bg);


        username.setText(SCMDataManager.getInstance().getUsername());


        setupOnclicks();

        DisplayAllstoreCount();

        DisplaySelectedPerspective(getContext(),getActivity());

        return view;
    }


    private void DisplayAllstoreCount(){
        Site site = SCMDataManager.getInstance().getSelectedSite().getSite();
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        date.setText(dateFormat.format(SCMTool.getDateWithDaypart(calendar.getTime().getTime(),site.effectiveUTCOffset).getTime()));


        if (QualificationData.getInstance().getAllstore_count()==0){
            int counter = 0;
            for (CustomSite customSite : SCMDataManager.getInstance().getCustomSites()){
                if (customSite.isMerged()){
                    counter++;
                }
            }
            allstore.setText("All Stores | ");
            allstore.append(counter + " Stores");
        }else {
            allstore.setText("All Stores | ");
            allstore.append(QualificationData.getInstance().getAllstore_count() + " Stores");
        }

    }

    private void setupOnclicks(){

        Covid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuickAccessMenu.DisplayCovidFragment(getActivity());
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayLogoutConfirmation();
            }
        });

        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayFilterMenu(getContext(),filter,SCMDataManager.getInstance().getQualificationFilterArrayList(),expandableListView);
            }
        });

        allstore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayAllstore();
            }
        });

        Dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), DashboardActivity.class);
                requireActivity().startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
            }
        });

        quick_a_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuickAccessMenu.DisplayQuickAMenu(getContext(), quick_a_menu, getActivity());
            }
        });

        sos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SOSData data = new SOSData();
                data.setSite(SCMDataManager.getInstance().getSelectedSite().getSite());
                SOSData.setInstance(data);
                Intent intent = new Intent(getActivity(), SOSActivity.class);
                requireActivity().startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
            }
        });

    }


    private void DisplayLogoutConfirmation() {
        DialogFragment newFragment = Logout_Confirmation_DialogFragment.newInstance();
        assert getFragmentManager() != null;
        newFragment.show(getActivity().getSupportFragmentManager(), "dialog");
    }

    public static void DisplaySelectedPerspective(Context context , FragmentActivity fragmentActivity){
        QualificationData data = new QualificationData();
        QualificationData.setInstance(data);

        if (SCMDataManager.getInstance().getQualificationFilterArrayList()!=null){

            for (Qualification_Filter filter : SCMDataManager.getInstance().getQualificationFilterArrayList()){
                if (filter.isSelected()){

                    if (filter.getName().equalsIgnoreCase("Ownership View")){
                        no_data_message.setVisibility(View.VISIBLE);
                        DisplayOwnerShip(context,fragmentActivity);
                        wait.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);

                    }else if (filter.getName().equalsIgnoreCase("Corporate View")){
                        no_data_message.setVisibility(View.VISIBLE);
                        DisplayCorpview(context,fragmentActivity);
                        wait.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);

                    }else {
                        no_data_message.setVisibility(View.VISIBLE);
                        DisplaySiteView(context,fragmentActivity);
                        wait.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                    }
                }
            }
        }else {
            DisplayOwnerShip(context,fragmentActivity);
            wait.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
        }

    }

    public static void DisplayOwnerShip(Context context , FragmentActivity fragmentActivity){
        DisplayOwnerShip(SCMDataManager.getInstance().getCustomSites(),SCMDataManager.getInstance().getCorpStructures(),context,fragmentActivity);
    }

    public static ArrayList<Corp_Regional> SiteView(){

        ArrayList<Corp_Chain> qualificationChains = OwnerShipView(SCMDataManager.getInstance().getCustomSites(),SCMDataManager.getInstance().getCorpStructures());
        ArrayList<Corp_Regional> qualificationRegionals = new ArrayList<>();

        for (Corp_Chain qualificationChain : qualificationChains){
            qualificationRegionals.addAll(qualificationChain.getQualificationRegionals());
        }

        ArrayList<Corp_Regional> NewCorpRegionals = new ArrayList<>();

        ArrayList<String>regional_string = new ArrayList<>();
        for (Corp_Regional qualificationRegional : qualificationRegionals){

            if (!regional_string.contains(qualificationRegional.getCorpStructures().name)){
                regional_string.add(qualificationRegional.getCorpStructures().name);

                ArrayList<String>province = new ArrayList<>();
                ArrayList<SiteList>siteLists = new ArrayList<>();

                for (Corp_Division qualificationDivision : qualificationRegional.getQualificationDivisions()){
                    for (Corp_Area qualificationArea : qualificationDivision.getQualificationAreas()){
                        for (Corp_District qualificationDistrict : qualificationArea.getQualificationDistricts()){
                            for (CustomSite site : qualificationDistrict.getSites()){
                                if (!province.contains(site.getSite().Address.Province)){
                                    province.add(site.getSite().Address.Province);
                                    ArrayList<CustomSite>sites = new ArrayList<>();
                                    sites.add(site);
                                    siteLists.add(new SiteList(site.getSite().Address.Province,sites));


                                }else {
                                    for (SiteList siteList1 : siteLists){
                                        if (siteList1.getProvince().equalsIgnoreCase(site.getSite().Address.Province)){
                                            siteList1.getCustomSites().add(site);

                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                qualificationRegional.setSiteLists(siteLists);
                NewCorpRegionals.add(qualificationRegional);

            }else {

                for (Corp_Regional corp_regional : NewCorpRegionals){
                    if (corp_regional.getCorpStructures().name.equalsIgnoreCase(qualificationRegional.corpStructures.name)){


                        ArrayList<String>province = new ArrayList<>();

                        for (SiteList siteList : corp_regional.getSiteLists()){
                            province.add(siteList.getProvince());

                        }

                        for (Corp_Division qualificationDivision : qualificationRegional.getQualificationDivisions()){
                            for (Corp_Area qualificationArea : qualificationDivision.getQualificationAreas()){
                                for (Corp_District qualificationDistrict : qualificationArea.getQualificationDistricts()){

                                    for (CustomSite site : qualificationDistrict.getSites()){
                                        if (!province.contains(site.getSite().Address.Province)){
                                            province.add(site.getSite().Address.Province);
                                            ArrayList<CustomSite>sites = new ArrayList<>();
                                            sites.add(site);
                                            corp_regional.getSiteLists().add(new SiteList(site.getSite().Address.Province,sites));

                                        }else {
                                            for (SiteList siteList1 : corp_regional.getSiteLists()){
                                                if (siteList1.getProvince().equalsIgnoreCase(site.getSite().Address.Province)){
                                                    siteList1.getCustomSites().add(site);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        return NewCorpRegionals;
    }

    public static void DisplaySiteView(Context context , FragmentActivity fragmentActivity){
        ArrayList<Corp_Regional>chains = SiteView();


        if (chains.size()>0){
            for (Corp_Regional corp_regional : chains){
                Collections.sort(corp_regional.getSiteLists(), new Comparator<SiteList>() {
                    @Override
                    public int compare(SiteList o1, SiteList o2) {
                        return o1.getProvince().compareToIgnoreCase(o2.getProvince());
                    }
                });
            }

            Qualification_SiteView_List_adapter adapter = new Qualification_SiteView_List_adapter(context,chains,fragmentActivity);
            expandableListView.setAdapter(adapter);

            for (int i = 0; i <adapter.getGroupCount() ; i++) {
                expandableListView.expandGroup(i);
            }
            expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                    return true;
                }
            });
            no_data_message.setVisibility(View.GONE);

            Qualification_StateByState_SearchAdapter searchadapter = new Qualification_StateByState_SearchAdapter(
                    context, chains,search,expandableListView,fragmentActivity);
            search.setAdapter(searchadapter);
        }




    }

    public static ArrayList<Corp_Chain> OwnerShipView(ArrayList<CustomSite>siteList , ArrayList<CorpStructure>corpStructures){
        ArrayList<Corp_Chain>basechains = new ArrayList<>();
        ArrayList<String>basestringchain = new ArrayList<>();
        boolean iscropviewEnabaled = true;

        if (siteList !=null && corpStructures!=null){

            for (CustomSite site : siteList){
                if (site.getSite().Chain == null || site.getSite().Chain.equalsIgnoreCase("null")){
                    if (!basestringchain.contains(site.getSite().CustomerName)){
                        basestringchain.add(site.getSite().CustomerName);
                        ArrayList<CustomSite>sites = new ArrayList<>();
                        Corp_Chain qualificationChain = new Corp_Chain();
                        qualificationChain.setName(site.getSite().CustomerName);
                        qualificationChain.setSites(sites);
                        sites.add(site);
                        basechains.add(qualificationChain);

                    }else {
                        for (Corp_Chain qualificationChain : basechains){
                            if (qualificationChain.getName().equalsIgnoreCase(site.getSite().CustomerName)){
                                qualificationChain.getSites().add(site);
                            }
                        }
                    }
                }else{
                    if (!basestringchain.contains(site.getSite().Chain)){
                        basestringchain.add(site.getSite().Chain);
                        ArrayList<CustomSite>sites = new ArrayList<>();
                        Corp_Chain qualificationChain = new Corp_Chain();
                        qualificationChain.setName(site.getSite().Chain);
                        qualificationChain.setSites(sites);
                        sites.add(site);
                        basechains.add(qualificationChain);
                    }else {
                        for (Corp_Chain qualificationChain : basechains){
                            if (qualificationChain.getName().equalsIgnoreCase(site.getSite().Chain)){
                                qualificationChain.getSites().add(site);
                            }
                        }
                    }
                }
            }

            if (basechains.size()>1) {
                try {
                    Collections.sort(basechains.subList(1, basechains.size()), new Comparator<Corp_Chain>() {
                        @Override
                        public int compare(Corp_Chain o1, Corp_Chain o2) {
                            return o1.getName().compareToIgnoreCase(o2.getName());
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            populateDistrict(basechains,corpStructures);

            for (Corp_Chain qualificationChain : basechains){
                if (qualificationChain.getQualificationRegionals()==null){
                    iscropviewEnabaled = false;
                    break;
                }else {
                    if (qualificationChain.getQualificationRegionals().size()==0){
                        iscropviewEnabaled = false;
                        break;
                    }
                }
            }

            if (basechains.get(0).getQualificationRegionals().size() >= 1) {
                basechains.get(0).getQualificationRegionals().get(0).setSelected(true);
                if (basechains.get(0).getQualificationRegionals().get(0).getQualificationDivisions().size()>=1){
                    basechains.get(0).getQualificationRegionals().get(0).getQualificationDivisions().get(0).setSelected(true);

                    if (basechains.get(0).getQualificationRegionals().get(0).getQualificationDivisions().get(0).getQualificationAreas().size()>=1){
                        basechains.get(0).getQualificationRegionals().get(0).getQualificationDivisions().get(0).getQualificationAreas().get(0).setSelected(true);

                        if (basechains.get(0).getQualificationRegionals().get(0).getQualificationDivisions().get(0).getQualificationAreas().get(0).getQualificationDistricts().size()>=2){
                            basechains.get(0).getQualificationRegionals().get(0).getQualificationDivisions().get(0).getQualificationAreas().get(0).getQualificationDistricts().get(0).setSelected(true);

                        }
                    }
                }
            }
        }
        if (iscropviewEnabaled){
            if (SCMDataManager.getInstance().getQualificationFilterArrayList() == null){
                ArrayList<Qualification_Filter>filters = new ArrayList<>();
                filters.add(new Qualification_Filter("Corporate View",false));
                filters.add(new Qualification_Filter("Ownership View",true));
                filters.add(new Qualification_Filter("State By State View",false));
                SCMDataManager.getInstance().setQualificationFilterArrayList(filters);
            }
        }else {
            if (SCMDataManager.getInstance().getQualificationFilterArrayList() == null){
                ArrayList<Qualification_Filter>filters = new ArrayList<>();
                filters.add(new Qualification_Filter("Ownership View",true));
                filters.add(new Qualification_Filter("State By State View",false));
                SCMDataManager.getInstance().setQualificationFilterArrayList(filters);
            }
        }

        return basechains;
    }

    public static void populateDistrict(ArrayList<Corp_Chain>basechains, ArrayList<CorpStructure>corpStructures){

        for (Corp_Chain qualificationChain : basechains){

            ArrayList<Corp_District> qualificationDistricts = new ArrayList<>();
            ArrayList<String> districs = new ArrayList<>();

            for (CustomSite site : qualificationChain.getSites()) {

                if (site.getSite().CorpStructureID != null ) {
                    if (!site.getSite().CorpStructureID.equalsIgnoreCase("null")){

                        if (!districs.contains(site.getSite().CorpStructureID)) {
                            districs.add(site.getSite().CorpStructureID);
                            ArrayList<CustomSite> sites = new ArrayList<>();
                            sites.add(site);
                            for (CorpStructure corpStructure : corpStructures){
                                if (corpStructure.corpStructureID.equalsIgnoreCase(site.getSite().CorpStructureID)){
                                    qualificationDistricts.add(new Corp_District(corpStructure, sites,corpStructure.parentID));

                                }
                            }

                        }
                        else {
                            for (Corp_District qualificationDistrict : qualificationDistricts) {
                                if (qualificationDistrict.getCorpStructures().corpStructureID.equalsIgnoreCase(site.getSite().CorpStructureID)) {
                                    qualificationDistrict.getSites().add(site);

                                }
                            }
                        }
                    }
                }
            }

            Collections.sort(qualificationDistricts, new Comparator<Corp_District>() {
                @Override
                public int compare(Corp_District o1, Corp_District o2) {
                    return o1.getCorpStructures().name.compareToIgnoreCase(o2.getCorpStructures().name);
                }
            });


            populateArea(qualificationChain,corpStructures, qualificationDistricts);


        }
    }

    public static void populateArea(Corp_Chain qualificationChain, ArrayList<CorpStructure>corpStructures, ArrayList<Corp_District> qualificationDistricts){

        ArrayList<Corp_Area> qualificationAreas = new ArrayList<>();
        ArrayList<String> arealist = new ArrayList<>();

        for (Corp_District qualificationDistrict : qualificationDistricts) {
            for (CorpStructure corpStructure : corpStructures) {

                if (qualificationDistrict.getCorpStructures().parentID.equalsIgnoreCase(corpStructure.corpStructureID)) {
                    if (!arealist.contains(corpStructure.corpStructureID)) {
                        arealist.add(corpStructure.corpStructureID);
                        ArrayList<Corp_District> districts1 = new ArrayList<>();
                        districts1.add(qualificationDistrict);

                        if (corpStructure.corpStructureID.equalsIgnoreCase(qualificationDistrict.parentid)){
                            qualificationAreas.add(new Corp_Area(corpStructure, districts1));
                        }

                    } else {
                        for (Corp_Area qualificationArea : qualificationAreas) {
                            if (qualificationArea.getCorpStructures().corpStructureID.equalsIgnoreCase(corpStructure.corpStructureID)) {
                                qualificationArea.getQualificationDistricts().add(qualificationDistrict);
                            }
                        }
                    }
                }
            }
        }

        Collections.sort(qualificationAreas, new Comparator<Corp_Area>() {
            @Override
            public int compare(Corp_Area o1, Corp_Area o2) {
                return o1.getCorpStructures().name.compareToIgnoreCase(o2.getCorpStructures().name);
            }
        });

        populateDision(qualificationChain,corpStructures, qualificationAreas);

    }

    public static void populateDision(Corp_Chain qualificationChain, ArrayList<CorpStructure>corpStructures, ArrayList<Corp_Area> qualificationAreas){

        ArrayList<Corp_Division> qualificationDivisions = new ArrayList<>();
        ArrayList<String> divisionlist = new ArrayList<>();
        for (Corp_Area qualificationArea : qualificationAreas) {
            for (CorpStructure corpStructure : corpStructures) {

                if (qualificationArea.getCorpStructures().parentID.equalsIgnoreCase(corpStructure.corpStructureID)) {

                    if (!divisionlist.contains(corpStructure.corpStructureID)) {
                        divisionlist.add(corpStructure.corpStructureID);
                        ArrayList<Corp_Area> areas1 = new ArrayList<>();
                        areas1.add(qualificationArea);

                        if (corpStructure.corpStructureID.equalsIgnoreCase(qualificationArea.corpStructures.parentID)) {
                            qualificationDivisions.add(new Corp_Division(corpStructure, areas1));
                        }
                    }else {
                        for (Corp_Division qualificationDivision : qualificationDivisions) {
                            if (qualificationDivision.getCorpStructures().corpStructureID.equalsIgnoreCase(corpStructure.corpStructureID)) {
                                qualificationDivision.getQualificationAreas().add(qualificationArea);
                            }
                        }
                    }
                }
            }
        }

        Collections.sort(qualificationDivisions, new Comparator<Corp_Division>() {
            @Override
            public int compare(Corp_Division o1, Corp_Division o2) {
                return o1.getCorpStructures().name.compareToIgnoreCase(o2.getCorpStructures().name);
            }
        });



        populateRegion(qualificationChain,corpStructures, qualificationDivisions);
    }

    public static void populateRegion(Corp_Chain qualificationChain, ArrayList<CorpStructure>corpStructures, ArrayList<Corp_Division> qualificationDivisions){

        ArrayList<Corp_Regional> qualificationRegionals = new ArrayList<>();
        ArrayList<String>stringregionals = new ArrayList<>();
        for (Corp_Division qualificationDivision : qualificationDivisions) {

            for (CorpStructure corpStructure : corpStructures) {

                if (qualificationDivision.getCorpStructures().parentID.equalsIgnoreCase(corpStructure.corpStructureID)) {

                    if (!stringregionals.contains(corpStructure.corpStructureID)) {
                        stringregionals.add(corpStructure.corpStructureID);
                        ArrayList<Corp_Division> divisions1 = new ArrayList<>();
                        divisions1.add(qualificationDivision);

                        qualificationRegionals.add(new Corp_Regional(corpStructure, divisions1));

                    } else {
                        for (Corp_Regional qualificationRegional : qualificationRegionals) {
                            if (qualificationRegional.getCorpStructures().corpStructureID.equalsIgnoreCase(corpStructure.corpStructureID)) {
                                qualificationRegional.getQualificationDivisions().add(qualificationDivision);
                            }
                        }
                    }
                }
            }
        }

        Collections.sort(qualificationRegionals, new Comparator<Corp_Regional>() {
            @Override
            public int compare(Corp_Regional o1, Corp_Regional o2) {
                return o1.getCorpStructures().name.compareToIgnoreCase(o2.getCorpStructures().name);
            }
        });


        qualificationChain.setQualificationRegionals(qualificationRegionals);

    }

    public static void DisplayOwnerShip(ArrayList<CustomSite>siteList,ArrayList<CorpStructure>corpStructures,Context context, FragmentActivity fragmentActivity){

        ArrayList<Corp_Chain>chains =OwnerShipView(siteList,corpStructures);

        if (chains.size()>0){

            Qualification_Owner_Corporate_List_adapter adapter = new Qualification_Owner_Corporate_List_adapter(context,chains,fragmentActivity);
            expandableListView.setAdapter(adapter);

            for (int i = 0; i <adapter.getGroupCount() ; i++) {
                expandableListView.expandGroup(i);
            }
            expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                    return true;
                }
            });

            no_data_message.setVisibility(View.GONE);

            Qualification_CorpView_OwnerShipView_SearchAdapter searchadapter = new Qualification_CorpView_OwnerShipView_SearchAdapter(
                    context, chains,search,expandableListView,fragmentActivity);
            search.setAdapter(searchadapter);

        }


    }

    public void DisplayFilterMenu(final Context context, View anchor,ArrayList<Qualification_Filter>filters,
                                  ExpandableListView expandableListView) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.BOTTOM)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .transparentOverlay(true)
                .arrowColor(ContextCompat.getColor(context, R.color.white))
                .contentView(R.layout.qualification_filter_popup)
                .focusable(true)
                .build();
        tooltip.show();

        ListView listView = tooltip.findViewById(R.id.listview);
        Qualification_Filter_Listview_Adapter adapter = new Qualification_Filter_Listview_Adapter(context,filters,expandableListView,
                getActivity(),tooltip);
        listView.setAdapter(adapter);

    }

    private void DisplayAllstore(){
        QualificationData data = new QualificationData();
        data.setAllstore(true);
        QualificationData.setInstance(data);
//        Intent intent = new Intent(getActivity(), QualificationActivity.class);
//        getActivity().startActivity(intent);
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.resturant_fragment_container,
                new QualificationActivityFragment()).commit();
    }

    public static void DisplayCorpview(Context context , FragmentActivity fragmentActivity){
        ArrayList<Corp_Chain>chains = CorpView();

        if (chains.size()>0){
            Qualification_Owner_Corporate_List_adapter adapter = new Qualification_Owner_Corporate_List_adapter(context,chains,fragmentActivity);
            expandableListView.setAdapter(adapter);

            for (int i = 0; i <adapter.getGroupCount() ; i++) {
                expandableListView.expandGroup(i);
            }
            expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                    return true;
                }
            });
            no_data_message.setVisibility(View.GONE);

            Qualification_CorpView_OwnerShipView_SearchAdapter searchadapter = new Qualification_CorpView_OwnerShipView_SearchAdapter(
                    context, chains,search,expandableListView,fragmentActivity);
            search.setAdapter(searchadapter);
        }

    }

    public static ArrayList<Corp_Chain> CorpView(){

        ArrayList<Corp_Chain> qualificationChains = OwnerShipView(SCMDataManager.getInstance().getCustomSites(),SCMDataManager.getInstance().getCorpStructures());
        ArrayList<Corp_Chain> qualificationChainArrayList = new ArrayList<>();
        ArrayList<Corp_Regional> qualificationRegionals = new ArrayList<>();
        ArrayList<String>string = new ArrayList<>();
        ArrayList<String>stringdivision = new ArrayList<>();

        if (qualificationChains.size()>0){
            for (Corp_Chain qualificationChain : qualificationChains){
                for (Corp_Regional qualificationRegional : qualificationChain.getQualificationRegionals()){
                    if (!string.contains(qualificationRegional.corpStructures.name)){
                        string.add(qualificationRegional.corpStructures.name);
                        qualificationRegionals.add(qualificationRegional);
                        for (Corp_Division qualificationDivision1 : qualificationRegional.getQualificationDivisions()){
                            stringdivision.add(qualificationDivision1.corpStructures.name);
                        }
                    }else {
                        for (Corp_Regional qualificationRegional1 : qualificationRegionals){
                            if (qualificationRegional.corpStructures.name.equalsIgnoreCase(qualificationRegional1.corpStructures.name)){
                                for (Corp_Division qualificationDivision : qualificationRegional.getQualificationDivisions()){
                                    if (!stringdivision.contains(qualificationDivision.corpStructures.name)){
                                        stringdivision.add(qualificationDivision.corpStructures.name);
                                        qualificationRegional1.getQualificationDivisions().add(qualificationDivision);
                                    }else {
                                        for (Corp_Division qualificationDivision1 : qualificationRegional1.getQualificationDivisions()){
                                            if (qualificationDivision1.corpStructures.name.equalsIgnoreCase(qualificationDivision.getCorpStructures().name)){
                                                for (Corp_Area qualificationArea : qualificationDivision.getQualificationAreas()){
                                                    qualificationDivision1.getQualificationAreas().add(qualificationArea);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            for (Corp_Regional qualificationRegional : qualificationRegionals){
                Collections.sort(qualificationRegional.getQualificationDivisions(), new Comparator<Corp_Division>() {
                    @Override
                    public int compare(Corp_Division o1, Corp_Division o2) {
                        return o1.getCorpStructures().name.compareToIgnoreCase(o2.getCorpStructures().name);
                    }
                });
            }

            Corp_Chain newQualificationChain = new Corp_Chain();
            newQualificationChain.setName(SCMDataManager.getInstance().getUser().customerName);
            newQualificationChain.setQualificationRegionals(qualificationRegionals);
            qualificationChainArrayList.add(newQualificationChain);
        }
        return qualificationChainArrayList;
    }


}


