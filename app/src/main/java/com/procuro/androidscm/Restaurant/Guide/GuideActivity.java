package com.procuro.androidscm.Restaurant.Guide;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.procuro.androidscm.R;

public class GuideActivity extends AppCompatActivity {

    public  static int GUIDE_CALENDAR = R.id.calendar;
    public  static int GUIDE_AUDIT = R.id.audits;

    private RadioButton audit,calendar;
    private info.hoang8f.android.segmented.SegmentedGroup infoGroup;
    private ImageView imageView;
    private TextView title;
    private Button back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide);

        audit = findViewById(R.id.audits);
        calendar = findViewById(R.id.calendar);
        infoGroup = findViewById(R.id.segmented2);
        imageView = findViewById(R.id.imageview);
        title = findViewById(R.id.title);
        back = findViewById(R.id.back);

        setUpOnclicks();

        try {
            getArgument();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setUpOnclicks(){
        infoGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                try {
                    if (checkedId == audit.getId()){
                        imageView.setImageResource(R.drawable.help_audit_icon_legend);
                    }else if (checkedId == calendar.getId()){
                        imageView.setImageResource(R.drawable.help_audit_legend_calendar);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    private void getArgument()throws Exception{
        GuideData data = GuideData.getInstance();

        if (data.getTitle()!=null){
            title.setText(data.getTitle());
        }

        if (data.getSelected()!=null){

          if (data.getSelected() == GUIDE_AUDIT){

              audit.setChecked(true);

          }else if (data.getSelected() == GUIDE_CALENDAR){

              calendar.setChecked(true);

          }

        }else {
            audit.setChecked(true);
        }
    }
}
