package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_SafetyDataSheets;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourcesChildList;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourcesGrandChildList;
import com.procuro.androidscm.SCMDataManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class Dashboard_SafetyDataSheets extends AppCompatActivity {


    private TextView title;
    private Button back;
    ExpandableListView expandableListView;
    EditText search;
    ArrayList<ResourcesChildList>array_sort = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_safety_data_sheets);

        title = findViewById(R.id.username);
        back = findViewById(R.id.home);
        expandableListView = findViewById(R.id.expandible_listview);
        search = findViewById(R.id.search);

        SetUpSiteInformation();

        setUpOnclicks();

        populateSafetyDataSheets();

        Searchfunction();


    }

    private void setUpOnclicks(){
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    private void populateSafetyDataSheets(){
        ArrayList<ResourcesChildList>safetyDataSheets =SCMDataManager.getInstance().getSafetyDataSheets();

        Collections.sort(safetyDataSheets, new Comparator<ResourcesChildList>() {
            @Override
            public int compare(ResourcesChildList o1, ResourcesChildList o2) {
                return o1.getAppString().compareTo(o2.getAppString());
            }
        });

        Dashboard_SafetyDataSheets_listview_adapter adapter = new Dashboard_SafetyDataSheets_listview_adapter(this,SCMDataManager.getInstance().getSafetyDataSheets(),Dashboard_SafetyDataSheets.this);
        expandableListView.setAdapter(adapter);

        for (int i = 0; i <adapter.getGroupCount() ; i++) {
            if (i <= 1){
                expandableListView.expandGroup(i);
            }
        }
    }

    private void SetUpSiteInformation(){
        try {
            title.setText(SCMDataManager.getInstance().getSelectedSite().getSite().sitename);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void Searchfunction(){

        final ArrayList<ResourcesChildList>traningData = SCMDataManager.getInstance().getSafetyDataSheets();

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int textlength = search.getText().length();
                ArrayList<String>strings = new ArrayList<>();

                if (textlength>0){
                    array_sort.clear();

                    for (ResourcesChildList dashboardTraningData : traningData){
                        boolean isparentCreated = false;
                        ArrayList<ResourcesGrandChildList> trainingVideos = dashboardTraningData.getGrandChildLists();
                        ArrayList<ResourcesGrandChildList> sortedvideo = new ArrayList<>();

                        if (textlength <= dashboardTraningData.getAppString().length()){
                            if (dashboardTraningData.getAppString().toLowerCase().trim().contains(
                                    search.getText().toString().toLowerCase().trim())){
                                if (!strings.contains(dashboardTraningData.getAppString())){
                                    strings.add(dashboardTraningData.getAppString());
                                    array_sort.add(new ResourcesChildList(dashboardTraningData.getAppString(), sortedvideo));
                                }
                            }
                        }

                        for (ResourcesGrandChildList trainingVideo: trainingVideos){
                            if (textlength <= trainingVideo.getDocumentDTO().name.length()){
                                if (trainingVideo.getDocumentDTO().name.toLowerCase().trim().contains(
                                        search.getText().toString().toLowerCase().trim())) {
                                    if (!strings.contains(dashboardTraningData.getAppString())) {
                                        strings.add(dashboardTraningData.getAppString());
                                        sortedvideo.add(trainingVideo);
                                        array_sort.add(new ResourcesChildList(dashboardTraningData.getAppString(), sortedvideo));

                                    }
                                    else {
                                        sortedvideo.add(trainingVideo);
                                    }
                                }

                            }
                        }
                    }
                    Dashboard_SafetyDataSheets_listview_adapter adapter = new Dashboard_SafetyDataSheets_listview_adapter(Dashboard_SafetyDataSheets.this,array_sort,Dashboard_SafetyDataSheets.this);
                    expandableListView.setAdapter(adapter);
                    for (int i = 0; i <adapter.getGroupCount() ; i++) {
                        expandableListView.expandGroup(i);
                    }
                }else {
                    Dashboard_SafetyDataSheets_listview_adapter adapter = new Dashboard_SafetyDataSheets_listview_adapter(Dashboard_SafetyDataSheets.this,traningData,Dashboard_SafetyDataSheets.this);
                    expandableListView.setAdapter(adapter);
                    for (int i = 0; i <adapter.getGroupCount() ; i++) {
                        if (i <= 1){
                            expandableListView.expandGroup(i);
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
