package com.procuro.androidscm.Restaurant.ReportsFragment;

import com.procuro.apimmdatamanagerlib.ApplicationReport;

public class ReportsChildList {

    ApplicationReport applicationReport;

    public ReportsChildList(ApplicationReport applicationReport) {
        this.applicationReport = applicationReport;
    }

    public ApplicationReport getApplicationReport() {
        return applicationReport;
    }

    public void setApplicationReport(ApplicationReport applicationReport) {
        this.applicationReport = applicationReport;
    }
}
