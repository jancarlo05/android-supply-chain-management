package com.procuro.androidscm.Restaurant.ReportsFragment;


import java.util.Date;

public class ReportsDateGrandChildList {
    String Title;
    Date date;
    boolean selected;
    boolean enabled;

    public ReportsDateGrandChildList() {

    }

    public ReportsDateGrandChildList(String title, Date date) {
        Title = title;
        this.date = date;
    }

    public ReportsDateGrandChildList(String title, Date date, boolean selected) {
        Title = title;
        this.date = date;
        this.selected = selected;
    }

    public ReportsDateGrandChildList(String title, Date date, boolean selected, boolean enabled) {
        Title = title;
        this.date = date;
        this.selected = selected;
        this.enabled = enabled;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
