package com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.StoreProfileOptions;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.EquipmenList.EquipmenlistData;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsEquipment;
import com.suke.widget.SwitchButton;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class StoreProfileOptions_Listview_adapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<KitchenCriteria> arraylist;
    private Site site;
    private Button button;
    private String tempCode = StoreProfileOptionsData.getInstance().getKitchentype();

    private String timeExpr = "\\b" +                      //   word boundary
            "(?:[01]?[0-9]|2[0-3])" +    //   match 0-9, 00-09, 10-19, 20-23
            ":[0-5][0-9]" +              //   match ':' then 00-59
            "(?! ?[ap]m)" +              //   not followed by optional space, and 'am' or 'pm'
            "\\b" +                      //   word boundary
            "|" +                          // OR
            "\\b" +                      //   word boundary
            "(?:0?[0-9]|1[0-1])" +       //   match 0-9, 00-09, 10-11
            ":[0-5][0-9]" +              //   match ':' and 00-59
            " ?[ap]m" +                  //   match optional space, and 'am' or 'pm'
            "\\b";                       //   word boundary
    Pattern p = Pattern.compile(timeExpr, Pattern.CASE_INSENSITIVE);


    public StoreProfileOptions_Listview_adapter(Context context, ArrayList<KitchenCriteria> arraylist) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;

    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return arraylist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
            final KitchenCriteria criteria = arraylist.get(position);
            TextView name;

            if (!criteria.isOnOff()){
                view = inflater.inflate(R.layout.store_profile_options_list_text, null);
                TextView description = view.findViewById(R.id.description);


                if (criteria.getCriteria().equalsIgnoreCase("Peak Hour Setting")){
                        description.setText(ReadTimeFromCode(true));

                }
                else if (criteria.getCriteria().equalsIgnoreCase("Dining Room Service Hours")){
                    if (tempCode.contains("NDRS")){
                        description.setText("6:00 AM - 10:00 AM");
                    }else if (tempCode.contains("DRS")){
                        description.setText("6:00 AM - 10:00 AM");
                    }
                    else {
                        description.setText(ReadTimeFromCode(false));
                    }
                }else {
                    for (KitchenDescription desc : criteria.getDescriptions()){

                        if (tempCode.contains(desc.code)){
                            description.setText(desc.description);
                            break;
                        }
                    }
                }

            }
            else {
                view = inflater.inflate(R.layout.store_profile_options_list_switch, null);
                SwitchButton switchButton = view.findViewById(R.id.switch_button);

                for (KitchenDescription desc : criteria.getDescriptions()){
                    if (tempCode.contains(desc.code)){
                        if (desc.description.equalsIgnoreCase("on")){
                            switchButton.setChecked(true);
                        }else {
                            switchButton.setChecked(false);
                        }
                        break;
                    }
                }

                switchButton.setEnabled(false);
                switchButton.setAlpha(.5f);
            }
            name = view.findViewById(R.id.name);
            name.setText(criteria.getCriteria());


        return view;
    }

    private String ReadTimeFromCode(boolean ifPHS){

        String readTime = "--";
        SimpleDateFormat format = new SimpleDateFormat("h:mm a");
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

        String timeCode = tempCode.replace("-"," ");
        timeCode = timeCode.replace("_"," ");

        StringBuilder time = new StringBuilder();
        Matcher m = p.matcher(timeCode);
        int counter = 0;

        try {
            if (! m.find())
                System.out.println("Has no time");
            else
                do {
                    if (ifPHS){
                        if (counter == 0){
                            Date d = dateFormat.parse(m.group());
                            time.append(format.format(d));
                            time.append(" to ");

                        }else if (counter == 1){
                            Date d = dateFormat.parse(m.group());
                            time.append(format.format(d));

                        }
                    }else {
                        if (counter == 2){
                            Date d = dateFormat.parse(m.group());
                            time.append(format.format(d));
                            time.append(" to ");

                        }else if (counter >2){
                            Date d = dateFormat.parse(m.group());
                            time.append(format.format(d));
                        }
                    }
                    counter++;

                } while (m.find());

            if (time.toString().length()>0){
                readTime = time.toString();
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return readTime;
    }

}

