package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Traning.Wendys_U;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Traning.Dashboard_TraningData;
import com.procuro.androidscm.Restaurant.DashBoard.VideoPlayerActivity;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.TrainingDataItems;

import java.util.ArrayList;

public class DashboardTraningWendysU_With_VideoPlayerActivity extends AppCompatActivity {

    TextView title;
    Button back;
    ExpandableListView expandableListView;
    EditText search;
    ArrayList<Dashboard_TraningData>array_sort = new ArrayList<>();

    public static WebView webView;
    ConstraintLayout fullscreen_Toggle,webview_container;
    LinearLayout header,list_container;
    boolean fullscreen = false;
    boolean visibleToggle = false;
    ImageView image_toggle;
    public static ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_traning_wendys_u_activity);

        title = findViewById(R.id.username);
        back = findViewById(R.id.home);
        expandableListView =findViewById(R.id.expandible_listview);
        search = findViewById(R.id.search);

        webView = findViewById(R.id.webView);
        fullscreen_Toggle = findViewById(R.id.fullscreen_Toggle);
        header = findViewById(R.id.header);
        image_toggle = findViewById(R.id.image_toggle);
        back = findViewById(R.id.home);
        progressBar = findViewById(R.id.progresbar);
        list_container = findViewById(R.id.list_container);
        webview_container = findViewById(R.id.webview_container);

        webView.setWebViewClient(new mywebviewclient());
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAllowFileAccess(true);
        webSettings.setAppCacheEnabled(true);
        webSettings.setUseWideViewPort(true);

        title.setText(SCMDataManager.getInstance().getSelectedSite().getSite().sitename);

        setUPOnclicks();
        setupTraning();
        Searchfunction();

        int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            header.setVisibility(View.GONE);
            image_toggle.setBackgroundResource(R.drawable.ic_fullscreen_exit_white_48dp);
            fullscreen = true;
            webview_container.getLayoutParams().height = LinearLayout.LayoutParams.MATCH_PARENT;

        } else {
            header.setVisibility(View.VISIBLE);
            fullscreen = false;
            image_toggle.setBackgroundResource(R.drawable.ic_fullscreen_white_48dp);
            webview_container.getLayoutParams().height = SCMTool.dpToPx(270);
        }

    }

    @SuppressLint("ClickableViewAccessibility")
    private void setUPOnclicks(){

        fullscreen_Toggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TooggleFullscreen();
            }
        });


        webView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ShowToggle();

                return false;
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void TooggleFullscreen(){
        if (!fullscreen){
            EnabledFUllscreen();
            fullscreen = true;
            image_toggle.setBackgroundResource(R.drawable.ic_fullscreen_exit_white_48dp);
            webview_container.getLayoutParams().height = LinearLayout.LayoutParams.MATCH_PARENT;
        }else {
            DisableFullscreen();
            fullscreen = false;
            image_toggle.setBackgroundResource(R.drawable.ic_fullscreen_white_48dp);
            webview_container.getLayoutParams().height = SCMTool.dpToPx(270);

        }
    }

    private void EnabledFUllscreen(){
//        SCREEN_ORIENTATION_LANDSCAPE
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        header.setVisibility(View.GONE);
        image_toggle.setBackgroundResource(R.drawable.ic_fullscreen_exit_white_48dp);
        fullscreen = true;
        list_container.setVisibility(View.GONE);
        webview_container.getLayoutParams().height = LinearLayout.LayoutParams.MATCH_PARENT;



    }

    private void DisableFullscreen(){
        //        SCREEN_ORIENTATION_PORTRAIT
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        header.setVisibility(View.VISIBLE);
        fullscreen = false;
        image_toggle.setBackgroundResource(R.drawable.ic_fullscreen_white_48dp);
        list_container.setVisibility(View.VISIBLE);
        webview_container.getLayoutParams().height = SCMTool.dpToPx(270);


    }
    private void ShowToggle(){
        if (!visibleToggle){
            visibleToggle = true;
            fullscreen_Toggle.setVisibility(View.VISIBLE);

            new CountDownTimer(4000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    fullscreen_Toggle.setVisibility(View.GONE);
                    visibleToggle = false;

                }
            }.start();
        }
    }

    private class mywebviewclient extends WebViewClient {



        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);

            System.out.println("Error: "+error.getDescription());
            System.out.println(error.getErrorCode());
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
            webView.setVisibility(View.VISIBLE);
            ShowToggle();
        }
    }


    @Override
    protected void onDestroy() {
        webView.destroy();
        super.onDestroy();

    }


    private void setupTraning(){
        expandableListView.setDividerHeight(2);
        expandableListView.setGroupIndicator(null);
        ArrayList training = SCMDataManager.getInstance().getDashboardTraningData();
        int counter = 0;
        if (training!=null){
            if (training.size()>0){
                Dashboard_Wendys_U_list_adapter adapter = new Dashboard_Wendys_U_list_adapter(this,training,this);
                expandableListView.setAdapter(adapter);
                for (int i = 0; i <adapter.getGroupCount() ; i++) {
                    Dashboard_TraningData data = (Dashboard_TraningData) training.get(i);
                    if (data.getTrainingDataItems()!=null){
                        if (data.getTrainingDataItems().size()>0){
                            if (counter==0){
                                counter++;
                                webView.loadUrl(data.getTrainingDataItems().get(0).videoURL);
                            }
                        }
                    }
                    expandableListView.expandGroup(i);
                }
            }
        }
    }


    private void Searchfunction(){

        final ArrayList<Dashboard_TraningData>traningData = SCMDataManager.getInstance().getDashboardTraningData();

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int textlength = search.getText().length();
                ArrayList<String>strings = new ArrayList<>();

                if (textlength>0){
                    array_sort.clear();

                    for (Dashboard_TraningData dashboardTraningData : traningData){
                        boolean isparentCreated = false;
                        ArrayList<TrainingDataItems> trainingVideos = dashboardTraningData.getTrainingDataItems();
                        ArrayList<TrainingDataItems> sortedvideo = new ArrayList<>();

                        if (textlength <= dashboardTraningData.getCategory().length()){
                            if (dashboardTraningData.getCategory().toLowerCase().trim().contains(
                                    search.getText().toString().toLowerCase().trim())){
                                if (!strings.contains(dashboardTraningData.getCategory())){
                                    strings.add(dashboardTraningData.getCategory());
                                    array_sort.add(new Dashboard_TraningData(dashboardTraningData.getCategory(), sortedvideo));
                                }
                            }
                        }

                        for (TrainingDataItems trainingVideo: trainingVideos){
                            if (textlength <= trainingVideo.title.length()){
                                if (trainingVideo.title.toLowerCase().trim().contains(
                                        search.getText().toString().toLowerCase().trim())) {
                                    if (!strings.contains(dashboardTraningData.getCategory())) {
                                        strings.add(dashboardTraningData.getCategory());
                                        sortedvideo.add(trainingVideo);
                                        array_sort.add(new Dashboard_TraningData(dashboardTraningData.getCategory(), sortedvideo));

                                    }
                                    else {
                                        sortedvideo.add(trainingVideo);
                                    }
                                }

                            }
                        }
                    }
                    Dashboard_Wendys_U_list_adapter adapter = new Dashboard_Wendys_U_list_adapter(DashboardTraningWendysU_With_VideoPlayerActivity.this,array_sort, DashboardTraningWendysU_With_VideoPlayerActivity.this);
                    expandableListView.setAdapter(adapter);
                    for (int i = 0; i <adapter.getGroupCount() ; i++) {
                        expandableListView.expandGroup(i);
                    }
                }else {
                    Dashboard_Wendys_U_list_adapter adapter = new Dashboard_Wendys_U_list_adapter(DashboardTraningWendysU_With_VideoPlayerActivity.this,traningData, DashboardTraningWendysU_With_VideoPlayerActivity.this);
                    expandableListView.setAdapter(adapter);
                    for (int i = 0; i <adapter.getGroupCount() ; i++) {
                        expandableListView.expandGroup(i);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        if (fullscreen){
            TooggleFullscreen();
        }else {
            finish();
        }
    }
}
