package com.procuro.androidscm.Restaurant.DashBoard;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourceManualFragment.Resources_right_pane_fragment;
import com.procuro.apimmdatamanagerlib.DocumentDTO;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.util.Objects;

import es.voghdev.pdfviewpager.library.RemotePDFViewPager;
import es.voghdev.pdfviewpager.library.adapter.PDFPagerAdapter;
import es.voghdev.pdfviewpager.library.remote.DownloadFile;
import es.voghdev.pdfviewpager.library.util.FileUtil;

public class Dashboard_PdfView_DialogFragment extends DialogFragment implements DownloadFile.Listener {

    public static DocumentDTO documentDTO;
    private Button close ;
    private ProgressBar progressBar;
    private TextView date , wait , title;
    private RemotePDFViewPager remotePDFViewPager;
    private PDFPagerAdapter adapter;
    private LinearLayout root;
    private ConstraintLayout pdf_container;
    private com.procuro.apimmdatamanagerlib.aPimmDataManager dataManager;

    public static Dashboard_PdfView_DialogFragment newInstance(DocumentDTO dto) {
        documentDTO = dto;
        return new Dashboard_PdfView_DialogFragment();
    }
    @Override
        public int getTheme() {
        return R.style.slide_down_up;
        }

    @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.dasboard_pdfview_fragment, container, false);

//                    int height = (int)(getResources().getDisplayMetrics().heightPixels*0.90);
//                    int width = 320;
//
        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.90);
        int height = (int)(getResources().getDisplayMetrics().heightPixels*0.70);


        Objects.requireNonNull(getDialog().getWindow()).setLayout(width, height);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.slide_down_up;
        view.setFocusableInTouchMode(true);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        close = view.findViewById(R.id.back_button);
        title = view.findViewById(R.id.name);
        root =  view.findViewById(R.id.root);
        progressBar = view.findViewById(R.id.progressBar2);
        wait = view.findViewById(R.id.wait);
        pdf_container = view.findViewById(R.id.pdf_container);

        title.setText(documentDTO.name);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        setDownloadButtonListener();



        return view;

        }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }





    protected void setDownloadButtonListener() {
        final Context ctx = getContext();
        final DownloadFile.Listener listener = this;

        dataManager = aPimmDataManager.getInstance();
        String[] strings = dataManager.getAuthHeaderValue().split("Basic ");

        StringBuilder urlString = new StringBuilder();
        urlString.append(dataManager.getBaseUrl());
        urlString.append("Document/Document/GetContent/");
        urlString.append("?documentId=");
        urlString.append(documentDTO.documentId);
        urlString.append("&authorization=").append(strings[1]);

        System.out.println(urlString.toString());

        try {
            remotePDFViewPager = new RemotePDFViewPager(ctx, urlString.toString(), listener);
            remotePDFViewPager.setId(R.id.pdfViewPager);
        }catch (Exception e){
            progressBar.setVisibility(View.INVISIBLE);
            wait.setText("NO PREVIEW AVAILABLE");
        }



    }

    public static void open(Context context) {
        Intent i = new Intent(context, Resources_right_pane_fragment.class);
        context.startActivity(i);

    }

    public void updateLayout() {
        wait.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        pdf_container.addView(remotePDFViewPager,
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);


    }

    @Override
    public void onSuccess(String url, String destinationPath) {
        try {
            adapter = new PDFPagerAdapter(getContext(), FileUtil.extractFileNameFromURL(url));
            remotePDFViewPager.setAdapter(adapter);
            updateLayout();
        }catch (Exception e){
            progressBar.setVisibility(View.INVISIBLE);
            wait.setText("NO PREVIEW AVAILABLE");
        }
    }

    @Override
    public void onFailure(Exception e) {
        e.printStackTrace();
        progressBar.setVisibility(View.INVISIBLE);
        wait.setText("NO PREVIEW AVAILABLE");

    }

    @Override
    public void onProgressUpdate(int progress, int total) {
        System.out.println("PROGRESS: "+progress+"/"+total);

    }
}
