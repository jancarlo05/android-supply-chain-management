package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_CleaningSlider.Daily;

import com.procuro.androidscm.Restaurant.DashBoard.Employee;


import java.util.Date;

public class CleaningPosition {

    String position;
    String task;
    Employee employee;
    Date assigned;
    boolean completed;
    String Employeename;

    public CleaningPosition(String position, String task, boolean completed) {
        this.position = position;
        this.task = task;
        this.completed = completed;
    }

    public CleaningPosition(String position, String task, String employee, Date assigned, boolean completed) {
        this.position = position;
        this.task = task;
        this.Employeename = employee;
        this.assigned = assigned;
        this.completed = completed;
    }

    public String getEmployeename() {
        return Employeename;
    }

    public void setEmployeename(String employeename) {
        Employeename = employeename;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Date getAssigned() {
        return assigned;
    }

    public void setAssigned(Date assigned) {
        this.assigned = assigned;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
