package com.procuro.androidscm.Restaurant.Qualification.QualificationFragment;

import com.procuro.apimmdatamanagerlib.CorpStructure;

import java.util.ArrayList;

public class Corp_Division {

    CorpStructure corpStructures;
    private ArrayList<Corp_Area> qualificationAreas;
    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public Corp_Division() {

    }

    public Corp_Division(CorpStructure corpStructures) {
        this.corpStructures = corpStructures;
    }

    public Corp_Division(CorpStructure corpStructures, ArrayList<Corp_Area> qualificationAreas) {
        this.corpStructures = corpStructures;
        this.qualificationAreas = qualificationAreas;
    }

    public CorpStructure getCorpStructures() {
        return corpStructures;
    }

    public void setCorpStructures(CorpStructure corpStructures) {
        this.corpStructures = corpStructures;
    }

    public ArrayList<Corp_Area> getQualificationAreas() {
        return qualificationAreas;
    }

    public void setQualificationAreas(ArrayList<Corp_Area> qualificationAreas) {
        this.qualificationAreas = qualificationAreas;
    }
}
