package com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.EquipmenList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsEquipment;
import com.suke.widget.SwitchButton;

import java.util.ArrayList;


public class EquipmentList_Listview_Adapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<CustomEquipment> arraylist;
    private Site site;
    private Button button;
    private boolean ifUserHasAccessToEdit = SCMTool.isUserHasAccessToEdit();


    public EquipmentList_Listview_Adapter(Context context, ArrayList<CustomEquipment> arraylist,
                                          Site site,Button button) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;
        this.site = site;
        this.button = button;

    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return arraylist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
            view = inflater.inflate(R.layout.equipment_list_data, null);

            final CustomEquipment customEquipment = arraylist.get(position);
            final SiteSettingsEquipment equipment = customEquipment.getEquipment();

            TextView name = view.findViewById(R.id.name);
            final com.suke.widget.SwitchButton switchButton = view.findViewById(R.id.switch_button);
            name.setText(equipment.equipmentName);

            if (equipment.inUse){
                switchButton.setChecked(true);

            }else {
                switchButton.setChecked(false);
            }

            switchButton.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(SwitchButton view, final boolean isChecked) {
                    equipment.setInUse(site.siteid,isChecked);
                    customEquipment.setUpdated(true);
                    EquipmenlistData.getInstance().setUnsaveEdit(true);
                    button.setAlpha(1);
                    button.setEnabled(true);
//                    setEquipmentIfCheck(equipment,isChecked,switchButton);
                    CheckUpdateForGrillOrDsg(customEquipment,isChecked);
                }
            });

            if (!ifUserHasAccessToEdit){
                SCMTool.DisableView(switchButton,.5f);
            }

        return view;
    }

    private void CheckUpdateForGrillOrDsg(CustomEquipment selected , boolean isChecked){
        String equipName = selected.getEquipment().equipmentName.toLowerCase();
        if (equipName.contains("dsg")) {
            for (CustomEquipment customEquipment : arraylist) {
                if (customEquipment.getEquipment().equipmentName.toLowerCase().contains("grill")){
                    if (isChecked){
                        customEquipment.getEquipment().setInUse(site.siteid,false);
                    }
                    customEquipment.setUpdated(true);
                }
            }
            notifyDataSetChanged();
        }
        else if (equipName.contains("grill")){
            for (CustomEquipment customEquipment : arraylist) {
                if (customEquipment.getEquipment().equipmentName.toLowerCase().contains("dsg")){
                    if (isChecked){
                        customEquipment.getEquipment().setInUse(site.siteid,false);
                    }
                    customEquipment.setUpdated(true);
                }
            }
            notifyDataSetChanged();
        }
    }


    private void setEquipmentIfCheck(final SiteSettingsEquipment equipment, final boolean isChecked, final SwitchButton switchButton){
     try {
         equipment.inUse = isChecked;
         final EquipmenlistData data = EquipmenlistData.getInstance();

         if (data.getSiteSettingsEquipmentsUpdates() == null){
             data.setSiteSettingsEquipmentsUpdates(new ArrayList<SiteSettingsEquipment>());
             equipment.setInUse(site.siteid,isChecked);
             data.getSiteSettingsEquipmentsUpdates().add(equipment);

         }else {
             int counter = 0;
             setSwithStatus(switchButton,false);
             for (int i = 0; i < data.getSiteSettingsEquipmentsUpdates().size(); i++) {
                 SiteSettingsEquipment newEquipment = data.getSiteSettingsEquipmentsUpdates().get(i);
                 counter++;
                 if (newEquipment.equipmentName.equalsIgnoreCase(equipment.equipmentName)){
                     newEquipment.setInUse(site.siteid,isChecked);
                 }else {
                     equipment.setInUse(site.siteid,isChecked);
                     data.getSiteSettingsEquipmentsUpdates().add(equipment);
                 }
                 if (counter == data.getSiteSettingsEquipmentsUpdates().size()){
                     setSwithStatus(switchButton,true);
                 }
             }

         }
         EquipmenlistData.getInstance().setUnsaveEdit(true);
         button.setAlpha(1);
         button.setEnabled(true);
     }catch (Exception e){
         e.printStackTrace();
     }
    }


    private void setSwithStatus(final SwitchButton switchButton, final boolean enabled){
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                float alpha = 1;
                if (!enabled){
                    alpha = 0.5f;
                }
                switchButton.setAlpha(alpha);
                switchButton.setEnabled(enabled);
            }
        });

    }
}

