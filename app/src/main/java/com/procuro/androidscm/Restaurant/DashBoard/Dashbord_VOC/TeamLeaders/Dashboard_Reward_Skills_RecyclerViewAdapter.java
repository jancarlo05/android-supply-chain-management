package com.procuro.androidscm.Restaurant.DashBoard.Dashbord_VOC.TeamLeaders;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.procuro.androidscm.GlideApp;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;


public class Dashboard_Reward_Skills_RecyclerViewAdapter extends RecyclerView.Adapter<Dashboard_Reward_Skills_RecyclerViewAdapter.MyViewHolder> {

    Context mContext;
    LayoutInflater inflater;
    private ArrayList<String> arraylist;

    public Dashboard_Reward_Skills_RecyclerViewAdapter(Context context, ArrayList<String> arraylist) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.reward_skill_layout,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final String employee = arraylist.get(position);
        SimpleDateFormat format = new SimpleDateFormat("EEE MM/dd/yyyy hh:mm a");

        if (employee.equalsIgnoreCase("0")){
            GlideApp.with(mContext).asDrawable().load(R.drawable.dinning_room_orange)
                    .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(holder.icon);
        }else if (employee.equalsIgnoreCase("1")){
            GlideApp.with(mContext).asDrawable().load(R.drawable.front_register_orange)
                    .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(holder.icon);
        }
        else if (employee.equalsIgnoreCase("2")){
            GlideApp.with(mContext).asDrawable().load(R.drawable.fry_station_orange)
                    .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(holder.icon);
        }
        else if (employee.equalsIgnoreCase("3")){
            GlideApp.with(mContext).asDrawable().load(R.drawable.grill_station_orange)
                    .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(holder.icon);
        }
        else if (employee.equalsIgnoreCase("4")){
            GlideApp.with(mContext).asDrawable().load(R.drawable.puw_orange)
                    .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(holder.icon);
        }
        else if (employee.equalsIgnoreCase("5")){
            GlideApp.with(mContext).asDrawable().load(R.drawable.sandwich_station_orange)
                    .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(holder.icon);
        }

        else if (employee.equalsIgnoreCase("6")){
            GlideApp.with(mContext).asDrawable().load(R.drawable.prod_coord_orange)
                    .skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(holder.icon);
        }
        else if (employee.equalsIgnoreCase("7")){
            GlideApp.with(mContext).asDrawable().load(R.drawable.service_coord_orange).into(holder.icon);
        }
        else if (employee.equalsIgnoreCase("8")){
            GlideApp.with(mContext).asDrawable().load(R.drawable.bacon_orange).into(holder.icon);
        }
        else if (employee.equalsIgnoreCase("9")){
            GlideApp.with(mContext).asDrawable().load(R.drawable.beverage_orange).into(holder.icon);
        }
        else if (employee.equalsIgnoreCase("10")){
            GlideApp.with(mContext).asDrawable().load(R.drawable.chili_orange).into(holder.icon);
        }
        else if (employee.equalsIgnoreCase("11")){
            GlideApp.with(mContext).asDrawable().load(R.drawable.foodprep_orange).into(holder.icon);
        }
        else if (employee.equalsIgnoreCase("12")){
            GlideApp.with(mContext).asDrawable().load(R.drawable.fry_station_orange).into(holder.icon);
        }
        else if (employee.equalsIgnoreCase("13")){
            GlideApp.with(mContext).asDrawable().load(R.drawable.salad_orange).into(holder.icon);
        }
        else if (employee.equalsIgnoreCase("14")){
            GlideApp.with(mContext).asDrawable().load(R.drawable.sandwich_station_orange).into(holder.icon);
        }
        else if (employee.equalsIgnoreCase("15")){
            GlideApp.with(mContext).asDrawable().load(R.drawable.productcoordinator_orange).into(holder.icon);
        }
        else if (employee.equalsIgnoreCase("16")){
            GlideApp.with(mContext).asDrawable().load(R.drawable.orientation_orange).into(holder.icon);
        }
        else if (employee.equalsIgnoreCase("17")){
            GlideApp.with(mContext).asDrawable().load(R.drawable.foodsafety_orange).into(holder.icon);
        }
        else if (employee.equalsIgnoreCase("18")){
            GlideApp.with(mContext).asDrawable().load(R.drawable.cleaning_orange).into(holder.icon);
        }
        else if (employee.equalsIgnoreCase("19")){
            GlideApp.with(mContext).asDrawable().load(R.drawable.customer_courtesy_orange).into(holder.icon);
        }
        else if (employee.equalsIgnoreCase("20")){
            GlideApp.with(mContext).asDrawable().load(R.drawable.fryeroperator_orange).into(holder.icon);
        }
        else if (employee.equalsIgnoreCase("21")){
            GlideApp.with(mContext).asDrawable().load(R.drawable.grilloperator_orange).into(holder.icon);
        }
        else if (employee.equalsIgnoreCase("22")){
            GlideApp.with(mContext).asDrawable().load(R.drawable.openclose_orange).into(holder.icon);
        }
        else if (employee.equalsIgnoreCase("23")){
            GlideApp.with(mContext).asDrawable().load(R.drawable.linesetup_orange).into(holder.icon);
        }


    }

    @Override
    public int getItemCount() {
        return arraylist.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView icon;

        public MyViewHolder(View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.icon);
        }
    }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

}
