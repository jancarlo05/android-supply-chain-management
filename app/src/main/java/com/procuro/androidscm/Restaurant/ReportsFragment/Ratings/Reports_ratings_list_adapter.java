package com.procuro.androidscm.Restaurant.ReportsFragment.Ratings;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;


import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsParentList;
import com.procuro.apimmdatamanagerlib.ApplicationReport;

import java.util.ArrayList;


public class Reports_ratings_list_adapter extends BaseExpandableListAdapter {

    final Context context ;
    ArrayList<ReportsParentList> parent_lists;
    FragmentActivity fragmentActivity;
    ExpandableListView listView;


    public Reports_ratings_list_adapter(Context context, ArrayList<ReportsParentList> data, FragmentActivity fragmentActivity, ExpandableListView listView) {
        this.context = context;
        this.parent_lists = new ArrayList<>();
        this.parent_lists = data;
        this.fragmentActivity = fragmentActivity;
        this.listView = listView;

    }
    @Override
    public void onGroupExpanded(int i) {

    }

    @Override
    public void onGroupCollapsed(int i){

    }

    @Override
    public boolean isEmpty() {
            return false;
    }


    @Override
    public int getGroupCount() {

        return parent_lists.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return parent_lists.get(i).getApplicationReports().size();
    }

    @Override
    public Object getGroup(int i) {
        return parent_lists.get(i);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return parent_lists.get(groupPosition).getApplicationReports().get(childPosition);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int position, boolean b, View contentView, ViewGroup parent) {
        ReportsParentList parent_data = parent_lists.get(position);
            contentView = LayoutInflater.from(context).inflate(R.layout.status_journal_parent_data, parent, false);
            TextView textView = contentView.findViewById(R.id.rowParentText);
            textView.setText(parent_data.getTitle());

            return contentView;
    }


    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean b, View contentView, final ViewGroup parent) {
        final ApplicationReport child_data = parent_lists.get(groupPosition).getApplicationReports().get(childPosition);

        contentView = LayoutInflater.from(context).inflate(R.layout.reports_daily, parent, false);

        final TextView title = contentView.findViewById(R.id.title);
        title.setText(child_data.ReportName);
        contentView.setBackgroundResource(R.drawable.onpress_design);

        if (!child_data.UrlTemplate.equalsIgnoreCase("#")){
            title.setAlpha(1f);
            contentView.setEnabled(true);
        }else {
            title.setAlpha(.5f);
            contentView.setEnabled(false);
        }

        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, ""+child_data.ReportName, Toast.LENGTH_SHORT).show();
                notifyDataSetChanged();

            }
        });

        return contentView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }


}