package com.procuro.androidscm.Restaurant.UserProfile;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.EmployeeList.EmployeeListActivity;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.TeamAndContancts.ManagementTeam.ManagementTeamData;
import com.procuro.androidscm.Restaurant.Qualification.UserProfile.Qualification_UserProfile_Regional_Child_level_Fragment;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.TemperatureListFragment.Status_DateRangePicker_DialogFragment;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.androidscm.UnderMaintenance_DialogFragment;
import com.procuro.apimmdatamanagerlib.Certification;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Objects;
import java.util.UUID;

public class UserProfileActivityOverAll extends AppCompatActivity {

    info.hoang8f.android.segmented.SegmentedGroup infogroup;
    public static TextView username;
    public static Button back,save,edit,editlock,delete;
    public static User user;
    public static aPimmDataManager dataManager = aPimmDataManager.getInstance();
    private LinearLayout root;
    private  int update_counter;
    private int current_counter;
    private boolean IsuserRemoved = false;
    private ProgressDialog removeDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile_overall);

        back = findViewById(R.id.home);
        username = findViewById(R.id.username);
        infogroup = findViewById(R.id.segmented2);
        edit = findViewById(R.id.edit);
        save = findViewById(R.id.save);
        editlock = findViewById(R.id.editlock);
        root = findViewById(R.id.root);
        delete = findViewById(R.id.delete);

        getArgument();

        DisplayPersonalInfo();

    }

    private void getArgument(){

        UserProfileData data = UserProfileData.getInstance();

        if (data.isFromQuickAccess()){
            user =  SCMDataManager.getInstance().getUser();
            username.setText(SCMDataManager.getInstance().getUsername());
        }
        else if (data.isFromQualification()){
            user = data.getCustomUser().getUser();
            username.setText(user.username);
        }

        if (data.isIsupdate()){

            if (user.active){
                edit.setAlpha(1);
                edit.setEnabled(true);
            }else {
                edit.setAlpha(.5f);
                edit.setEnabled(false);
                editlock.setEnabled(false);
                editlock.setAlpha(.5f);
            }
        }else if (data.isCreate()){
            data.setUpdate_Siteallowed(new ArrayList<String>());
            data.getUpdate_Siteallowed().add(data.getCustomSite().getSite().sitename);
            Edit();
        }

        setupOnlcicks(user,data.getCustomSite().getSite());
    }


    private void Exit(){
        UserProfileData data = UserProfileData.getInstance();
        String prev = data.getPrevActivity();
        if (prev!=null){
            if (prev.equalsIgnoreCase("UserProfile")){
                Qualification_UserProfile_Regional_Child_level_Fragment.DisplaySiteView(Qualification_UserProfile_Regional_Child_level_Fragment.customSite, UserProfileActivityOverAll.this, UserProfileActivityOverAll.this);


            }if (prev.equalsIgnoreCase("EmployeeList")){
                if (data.isIsupdate()){
                    EmployeeListActivity.SelectPerspective(getApplicationContext(), UserProfileActivityOverAll.this);

                }
            }
        }

        finish();

    }

    private void setupOnlcicks(final User user, final Site site){

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Exit();
            }
        });

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserProfileData.getInstance().setUserEditEnabled(true);
                DisplayPersonalInfo();
                Edit();
            }
        });

        editlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayCHangePasswordPopup();
            }
        });


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                UserProfileData data = UserProfileData.getInstance();

                if (data.isIsupdate()){

                    SaveUpdate();

                }else if (data.isCreate()){

                    SaveNewEmployee();
                }

            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayDeleteConfirmation(user,site);
            }
        });


    }

    private void DisplayCHangePasswordPopup(){
        DialogFragment newFragment = User_Password_Verification_DialogFragment.newInstance();
        assert getFragmentManager() != null;
        newFragment.show(Objects.requireNonNull(this).getSupportFragmentManager(), "dialog");
    }

    private void DisplayPersonalInfo() {
        getSupportFragmentManager().beginTransaction().replace(R.id.user_profile_fragment_container,
                new User_Profile_Overall_Info_Fragment()).commit();
    }

    private void DisplaySkills() {
        getSupportFragmentManager().beginTransaction().replace(R.id.user_profile_fragment_container,
                new User_Skills_Fragment()).commit();
    }

    private void Edit(){
        editlock.setEnabled(true);
        editlock.setAlpha(1f);
        edit.setEnabled(false);
        edit.setAlpha(.5f);
        save.setAlpha(1);
        save.setEnabled(true);

    }
    private void Lock(){
        editlock.setEnabled(false);
        editlock.setAlpha(.5f);
        edit.setEnabled(true);
        edit.setAlpha(1);
        save.setAlpha(.5f);
        save.setEnabled(false);

    }

    private void DisplayUnderMaintenanceMessage() {
        DialogFragment newFragment = UnderMaintenance_DialogFragment.newInstance();
        assert getFragmentManager() != null;
        newFragment.show(getSupportFragmentManager(), "dialog");
    }

    private void SaveNewEmployee(){

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Please Wait");
        progressDialog.show();

        final UserProfileData data = UserProfileData.getInstance();
        User user = data.getCustomUser().getUser();


        try {
            SaveNewBasicInfo(data,user,progressDialog);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void SaveNewBasicInfo(final UserProfileData data, final User user, final ProgressDialog progressDialog){

        progressDialog.setMessage("Creating New Employee");
        ArrayList<Boolean> needtoupdate = new ArrayList<>();

        UUID uuid = UUID.randomUUID();
        user.userId = uuid.toString();
        user.exitReason = User.UserExitReasonEnum.UserExitReason_CurrentlyEmployed;
        user.roles = new ArrayList<>();
        user.old_certificationList = new ArrayList<>();
        user.active = true;

        System.out.println("NEW USER ID : "+user.userId);

        CustomUser CurrentGm = getCurrentGM(progressDialog);

        if (data.getUpdate_Siteallowed()!=null){
            if (data.getUpdate_Siteallowed().size()>0){
                needtoupdate.add(true);
                user.roles.addAll(data.getUpdate_Siteallowed());
            }else {
                needtoupdate.add(false);
                progressDialog.dismiss();
                SCMTool.DisplaySnackbar("Please Input Site's Allowed", root);
            }

        }else {
            needtoupdate.add(false);
            progressDialog.dismiss();
            SCMTool.DisplaySnackbar("Please Input Site's Allowed", root);

        }

        if (data.getRole()!=null){
            if (ValidString(data.getRole())){

                if (CurrentGm !=null){
                    if (!data.getRole().equalsIgnoreCase("Gm")){
                        needtoupdate.add(true);
                        user.roles.add(data.getRole());

                    }else {
                        progressDialog.dismiss();
                        needtoupdate.add(false);
                        StringBuilder name = new StringBuilder();
                        name.append(CurrentGm.getUser().firstName);
                        name.append(" ");
                        name.append(CurrentGm.getUser().lastName);
                        SCMTool.DisplaySnackbar("General Manager is already taken by "+ name, root);
                    }
                }else {
                    needtoupdate.add(true);
                    user.roles.add(data.getRole());
                }

            }else {
                needtoupdate.add(false);
                progressDialog.dismiss();
                SCMTool.DisplaySnackbar("Please Input Permission",root);

            }
        }else {
            needtoupdate.add(false);
            progressDialog.dismiss();
            SCMTool.DisplaySnackbar("Please Input Permission", root);
        }

        if (data.getPassword()!=null){
            if (ValidString(data.getPassword())){
                user.password = data.getPassword();
                needtoupdate.add(true);
            }else {
                needtoupdate.add(false);
                progressDialog.dismiss();
                SCMTool.DisplaySnackbar("Please Input  Password",root);

            }
        }else {
            needtoupdate.add(false);
            progressDialog.dismiss();
            SCMTool.DisplaySnackbar("Please Input  Password",root);

        }

        if (data.getUsername()!=null){
            if (ValidString(data.getUsername())){
                user.username = data.getUsername();
                needtoupdate.add(true);
            }else {
                needtoupdate.add(false);
                progressDialog.dismiss();
                SCMTool.DisplaySnackbar("Please Input  Username",root);


            }
        }else {
            needtoupdate.add(false);
            progressDialog.dismiss();
            SCMTool.DisplaySnackbar("Please Input  Username",root);
        }

        if (data.getLastName()!=null){
            if (ValidString(data.getLastName())){
                user.lastName = data.getLastName();
                needtoupdate.add(true);
            }else {
                needtoupdate.add(false);
                progressDialog.dismiss();
                SCMTool.DisplaySnackbar("Please Input Last Name",root);


            }
        }else {
            needtoupdate.add(false);
            progressDialog.dismiss();
            SCMTool.DisplaySnackbar("Please Input Last Name",root);
        }

        if (data.getFirstName()!=null){
            if (ValidString(data.getFirstName())){
                user.firstName = data.getFirstName();
                needtoupdate.add(true);
            }else {
                needtoupdate.add(false);
                progressDialog.dismiss();
                SCMTool.DisplaySnackbar("Please Input First Name",root);

            }
        }else {
            needtoupdate.add(false);
            progressDialog.dismiss();
            SCMTool.DisplaySnackbar("Please Input First Name",root);

        }


        if (data.getEmployeeID()!=null){
            user.employeeID = data.getEmployeeID();
            needtoupdate.add(true);

        }

        if (data.getContactEmail()!=null){
            user.contactEmail = data.getContactEmail();
            needtoupdate.add(true);

        }

        if (data.getpID()!=null){
            user.pID = data.getpID();
            needtoupdate.add(true);

        }
        if (data.getAddress1()!=null){
            user.address1 = data.getAddress1();
            needtoupdate.add(true);

        }
        if (data.getCity()!=null){
            user.city = data.getCity();
            needtoupdate.add(true);
        }

        if (data.getPostal()!=null){
            user.postal = data.getPostal();
            needtoupdate.add(true);

        }
        if (data.getDayPhone()!=null){
            user.dayPhone = data.getDayPhone();
            needtoupdate.add(true);
        }

        if (data.getDOB()!=null){
            user.DOB = data.getDOB();
            needtoupdate.add(true);
        }

        if (data.getHireDate()!=null){
            user.hireDate = data.getHireDate();
            needtoupdate.add(true);
        }

        if (data.getReviewDate()!=null){
            user.reviewDate = data.getReviewDate();
            needtoupdate.add(true);

        }
        if (data.getState()!=null){
            user.state = data.getState();
            needtoupdate.add(true);
        }

        if (data.getMobileCarrier()!=null){
            user.mobileCarrier = data.getMobileCarrier();
            needtoupdate.add(true);
        }

        if (!needtoupdate.contains(false)){

            if (needtoupdate.contains(true)){
                try {
                    dataManager.createUserRecordForUser(user, new OnCompleteListeners.updateUserRecordWithUserIdListener() {
                        @Override
                        public void updateUserRecordWithUserIdCallback(Boolean status, Error error) {
                            if (error == null){
                                addCertToserver(user);
                                AddNewSiteAllowed(data.getUpdate_Siteallowed(),progressDialog,data,user);
                                System.out.println("createUserRecordForUser : SAVED");
                            }else {
                                SCMTool.DisplaySnackbar("Username is already taken",root);
                                progressDialog.dismiss();
                            }
                        }
                    });
                }catch (Exception e){
                    e.printStackTrace();
                    SCMTool.DisplaySnackbar("ERROR CREATING EMPLOYEE",root);
                    progressDialog.dismiss();
                }
            }
        }else {
            progressDialog.dismiss();
        }
    }


    private void SaveUpdate(){
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Please Wait");
        progressDialog.show();

        UserProfileData data = UserProfileData.getInstance();
        User user = data.getCustomUser().getUser();

        try {
            UpdateBasicInfo(data,user,progressDialog);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void AddNewSiteAllowed( final ArrayList<String>updated, final ProgressDialog progressDialog, final UserProfileData data, final User user){

        update_counter = 0;
        if (updated.size()>0){

            for (final String update : updated){
                update_counter++;

                System.out.println(update);

                ArrayList<CustomSite>customSites = UserProfileData.getInstance().getSelectedSites();

                for (CustomSite site : customSites){

                    if (site.getSite().sitename.equalsIgnoreCase(update)){

                        dataManager.AddUserToStore(user.userId, site.getSite().siteid, new OnCompleteListeners.updateUserRecordWithUserIdListener() {
                            @Override
                            public void updateUserRecordWithUserIdCallback(Boolean status, Error error) {

                                if (error == null){
                                    System.out.println("AddUserToStore Success!");

                                    dataManager.addRoleForUser(user.userId, update.toUpperCase(), new OnCompleteListeners.updateUserRecordWithUserIdListener() {
                                        @Override
                                        public void updateUserRecordWithUserIdCallback(Boolean status, Error error) {
                                            if (error ==null){
                                                System.out.println("addRoleForUser Success!");

                                                if (update_counter == updated.size()){
                                                    UpdateCertList(data,user,progressDialog);
                                                }
                                            }else {
                                                progressDialog.dismiss();
                                                SCMTool.DisplaySnackbar("ERROR SAVING UPDATE",root);
                                            }
                                        }
                                    });

                                }else {
                                    progressDialog.dismiss();
                                    SCMTool.DisplaySnackbar("ERROR SAVING UPDATE",root);
                                }
                            }
                        });

                        AddNewUserLocal(site);
                    }
                }
            }
        }
    }


    private void UpdateBasicInfo(final UserProfileData data, final User user, final ProgressDialog progressDialog){

        progressDialog.setMessage("Saving Update");
        ArrayList<Boolean> needtoupdate = new ArrayList<>();
        CustomUser CurrentGm = getCurrentGM(progressDialog);

        if (data.getUsername()!=null){
            if (ValidString(data.getUsername())){
                user.username = data.getUsername();
                needtoupdate.add(true);
            }else {
                needtoupdate.add(false);
                progressDialog.dismiss();
                SCMTool.DisplaySnackbar("Please Input  Username",root);

            }
        }

        if (data.getLastName()!=null){
            if (ValidString(data.getLastName())){
                user.lastName = data.getLastName();
                needtoupdate.add(true);
            }else {
                needtoupdate.add(false);
                progressDialog.dismiss();
                SCMTool.DisplaySnackbar("Please Input Last Name",root);

            }
        }

        if (data.getFirstName()!=null){
            if (ValidString(data.getFirstName())){
                user.firstName = data.getFirstName();
                needtoupdate.add(true);
            }else {
                needtoupdate.add(false);
                progressDialog.dismiss();
                SCMTool.DisplaySnackbar("Please Input First Name",root);
            }
        }


        if (CurrentGm !=null){
            if (data.getRole()!=null){
                if (!data.getRole().equalsIgnoreCase("Gm")){
                    needtoupdate.add(true);

                }else {
                    if (!CurrentGm.getUser().userId.equalsIgnoreCase(user.userId)){
                        progressDialog.dismiss();
                        needtoupdate.add(false);
                        StringBuilder name = new StringBuilder();
                        name.append(CurrentGm.getUser().firstName);
                        name.append(" ");
                        name.append(CurrentGm.getUser().lastName);
                        SCMTool.DisplaySnackbar("General Manager is already taken by "+ name, root);

                    }
                }
            }
        }

        if (data.getPassword()!=null){
            user.password = data.getPassword();
            needtoupdate.add(true);

        }

        if (data.getEmployeeID()!=null){
            user.employeeID = data.getEmployeeID();
            needtoupdate.add(true);

        }

        if (data.getContactEmail()!=null){
            user.contactEmail = data.getContactEmail();
            needtoupdate.add(true);

        }

        if (data.getpID()!=null){
            user.pID = data.getpID();
            needtoupdate.add(true);

        }
        if (data.getAddress1()!=null){
            user.address1 = data.getAddress1();
            needtoupdate.add(true);

        }
        if (data.getCity()!=null){
            user.city = data.getCity();
            needtoupdate.add(true);
        }

        if (data.getPostal()!=null){
            user.postal = data.getPostal();
            needtoupdate.add(true);

        }
        if (data.getDayPhone()!=null){
            user.dayPhone = data.getDayPhone();
            needtoupdate.add(true);
        }

        if (data.getDOB()!=null){
            user.DOB = data.getDOB();
            needtoupdate.add(true);
        }

        if (data.getHireDate()!=null){
            user.hireDate = data.getHireDate();
            needtoupdate.add(true);
        }

        if (data.getReviewDate()!=null){
            user.reviewDate = data.getReviewDate();
            needtoupdate.add(true);

        }
        if (data.getState()!=null){
            user.state = data.getState();
            needtoupdate.add(true);
        }

        if (data.getMobileCarrier()!=null){
            user.mobileCarrier = data.getMobileCarrier();
            needtoupdate.add(true);

        }

        if (!needtoupdate.contains(false)){

            if (needtoupdate.contains(true)){
                try {
                    dataManager.updateUserRecordWithUserId(user, new OnCompleteListeners.updateUserRecordWithUserIdListener() {
                        @Override
                        public void updateUserRecordWithUserIdCallback(Boolean status, Error error) {
                            if (error == null){
                                UpdateSiteAllowed(data,user,progressDialog);
                                System.out.println("updateUserRecordWithUserId : SAVED");
                            }else {
                                SCMTool.DisplaySnackbar("ERROR SAVING UPDATE",root);
                                progressDialog.dismiss();
                            }
                        }
                    });
                }catch (Exception e){
                    e.printStackTrace();
                    SCMTool.DisplaySnackbar("ERROR SAVING UPDATE",root);
                    progressDialog.dismiss();
                }

            }else {
                UpdateSiteAllowed(data,user,progressDialog);
            }
        }

    }

    private void UpdateSiteAllowed(UserProfileData data,User user,ProgressDialog progressDialog){

        if (data.getUpdate_Siteallowed() != null){
            try {
                ArrayList<String> current_Sites_allowed = new ArrayList<>();
                if (data.getCurrent_Site_Allowed()!=null){
                    current_Sites_allowed = data.getCurrent_Site_Allowed();
                }
                ArrayList<String> update_Sites_allowed = data.getUpdate_Siteallowed();

                AddSiteAllowed(current_Sites_allowed,update_Sites_allowed,user.userId,progressDialog,data,user);

            }catch (Exception e){
                e.printStackTrace();
                SCMTool.DisplaySnackbar("ERROR SAVING DATA",root);
                progressDialog.dismiss();
            }

        }else {
            UpdateCertList(data,user,progressDialog);
        }
    }

    private void AddSiteAllowed(final ArrayList<String>current, final ArrayList<String>updated, final String userID, final ProgressDialog progressDialog, final UserProfileData data, final User user){

        update_counter = 0;
        boolean ItemExist = false;

        if (updated.size()>0){
            
            for (final String update : updated){
                update_counter++;

                if (!current.contains(update)){

                    System.out.println(update);

                    ArrayList<CustomSite>customSites = UserProfileData.getInstance().getSelectedSites();

                    for (CustomSite site : customSites){

                        if (site.getSite().sitename.equalsIgnoreCase(update)){

                            ItemExist = true;

                            dataManager.AddUserToStore(userID, site.getSite().siteid, new OnCompleteListeners.updateUserRecordWithUserIdListener() {
                                @Override
                                public void updateUserRecordWithUserIdCallback(Boolean status, Error error) {

                                    if (error == null){
                                        System.out.println("AddUserToStore Success!");

                                        dataManager.addRoleForUser(userID, update.toUpperCase(), new OnCompleteListeners.updateUserRecordWithUserIdListener() {
                                            @Override
                                            public void updateUserRecordWithUserIdCallback(Boolean status, Error error) {
                                                if (error ==null){
                                                    System.out.println("addRoleForUser Success!");
                                                    current.add(update);

                                                    if (update_counter == updated.size()){
                                                        RemoveSiteAllowed(current,updated,userID,progressDialog,data,user);
                                                    }

                                                }else {
                                                    progressDialog.dismiss();
                                                    SCMTool.DisplaySnackbar("ERROR SAVING UPDATE",root);
                                                }
                                            }
                                        });

                                    }else {
                                        progressDialog.dismiss();
                                        SCMTool.DisplaySnackbar("ERROR SAVING UPDATE",root);
                                    }
                                }
                            });
                        }
                        AddNewUserLocal(site);
                    }
                }
                if (!ItemExist){
                    if (update_counter == updated.size()){
                        System.out.println("RemoveSiteAllowed Start!");
                        RemoveSiteAllowed(current,updated,userID,progressDialog,data,user);
                    }
                }
            }
        }else {
            System.out.println("RemoveSiteAllowed Start!");
            RemoveSiteAllowed(current,updated,userID,progressDialog,data,user);
        }


    }

    private void RemoveSiteAllowed(final ArrayList<String>current, final ArrayList<String>updated, final String userID, final ProgressDialog progressDialog, final UserProfileData data, final User user){
        current_counter = 0;
        if (current.size()>0){
            user.roles.addAll(current);
            for (int i = 0; i <current.size() ; i++) {

                current_counter++;
                final String curr = current.get(i);
                final int finalI = i;

                if (!updated.contains(curr)) {

                    ArrayList<CustomSite>customSites = UserProfileData.getInstance().getSelectedSites();

                    for (final CustomSite site : customSites){

                        if (site.getSite().sitename.equalsIgnoreCase(curr)){

                            dataManager.RemoveUserFromStore(userID, site.getSite().siteid, new OnCompleteListeners.updateUserRecordWithUserIdListener() {
                                @Override
                                public void updateUserRecordWithUserIdCallback(Boolean status, Error error) {
                                    if (error == null){
                                        System.out.println("RemoveUserFromStore Success!");

                                        dataManager.removeRoleForUser(userID,curr , new OnCompleteListeners.updateUserRecordWithUserIdListener() {
                                            @Override
                                            public void updateUserRecordWithUserIdCallback(Boolean status, Error error) {
                                                if (error == null){
                                                    System.out.println("removeRoleForUser Success!");
                                                    current.remove(finalI);
                                                    RemoveUserLocal(site.getSite().sitename);
                                                    RemoveSiteAllowed(current,updated,userID,progressDialog,data,user);
                                                }else {
                                                    progressDialog.dismiss();
                                                    SCMTool.DisplaySnackbar("ERROR SAVING UPDATE",root);
                                                }
                                            }
                                        });
                                    }else {
                                        progressDialog.dismiss();
                                        SCMTool.DisplaySnackbar("ERROR SAVING UPDATE",root);
                                    }
                                }
                            });
                        }
                    }
                    break;
                }
                if (current_counter == current.size()){
                    System.out.println("UpdateCertList Start!");
                    UpdateCertList(data,user,progressDialog);
                }
            }

        }else {
            System.out.println("UpdateCertList Start!");
            UpdateCertList(data,user,progressDialog);
        }


    }

    private void UpdateCertList(final UserProfileData data, final User user, final ProgressDialog progressDialog){

        if (data.getCertificationList()!=null){

            try {
                ArrayList<String> current = user.old_certificationList;
                ArrayList<String> updated = data.getCertificationList();

                AddCert(current,updated,user.userId,data,user,progressDialog);

                Collections.sort(current, new Comparator<String>() {
                    @Override
                    public int compare(String o1, String o2) {
                        return o1.compareTo(o2);
                    }
                });

            }catch (Exception e){
                e.printStackTrace();
                SCMTool.DisplaySnackbar("ERROR SAVING DATA",root);
                progressDialog.dismiss();
            }
        }else {
            updateROle(data,user,progressDialog);
        }

    }

    private void AddCert(final ArrayList<String>current, final ArrayList<String>updated, final String userid, final UserProfileData data, final User user, final ProgressDialog progressDialog){
        System.out.println("AddCert Start!");
        try {
             update_counter = 0;
            if (updated.size()>0){
                user.old_certificationList = updated;
                for (final String update : updated){
                    update_counter++;
                    if (!current.contains(update)){

                        dataManager.AddCertificationToUser(userid, update, new OnCompleteListeners.updateUserRecordWithUserIdListener() {
                            @Override
                            public void updateUserRecordWithUserIdCallback(Boolean status, Error error) {
                                if (error==null){
                                    System.out.println("AddCertificationToUser Success!");
                                    current.add(update);

                                    if (update_counter == updated.size()){
                                        System.out.println("RemoveCert START!");
                                        RemoveCert(current,updated,userid,data,user,progressDialog);
                                    }

                                }else {
                                    progressDialog.dismiss();
                                    SCMTool.DisplaySnackbar("ERROR SAVING UPDATE",root);
                                }
                            }
                        });
                    } else if (update_counter == updated.size()){
                        System.out.println("RemoveCert START!");
                        RemoveCert(current,updated,userid,data,user,progressDialog);
                    }
                }
            }else {
                System.out.println("RemoveCert START!");
                RemoveCert(current,updated,userid,data,user,progressDialog);
            }

        }catch (Exception e){
            e.printStackTrace();
            SCMTool.DisplaySnackbar("ERROR SAVING UPDATE",root);
            progressDialog.dismiss();
        }


    }

    private void RemoveCert(final ArrayList<String>current, final ArrayList<String>updated , final String userid, final UserProfileData data, final User user, final ProgressDialog progressDialog){

        int current_counter = 0;
        if (current.size()>0){
            for (int i = 0; i <current.size() ; i++) {
                String curr = current.get(i);
                current_counter++;
                if (!updated.contains(curr)) {
                    current.remove(i);

                    dataManager.RemoveCertificationFromUser(userid, curr, new OnCompleteListeners.updateUserRecordWithUserIdListener() {
                        @Override
                        public void updateUserRecordWithUserIdCallback(Boolean status, Error error) {
                            if (error==null){
                                System.out.println("RemoveCertificationFromUser Success!");
                                RemoveCert(current,updated,userid,data,user,progressDialog);
                            }else {
                                progressDialog.dismiss();
                                SCMTool.DisplaySnackbar("ERROR SAVING UPDATE",root);
                            }
                        }
                    });
                    break;
                }else if (current_counter == current.size()){
                    updateROle(data,user,progressDialog);
                }

            }
        }else{
            updateROle(data,user,progressDialog);
        }

    }

    private void updateROle(final UserProfileData data, final User user, final ProgressDialog progressDialog){

        if (UserProfileData.getInstance().getRole()!=null){
            try {
                String currentrole = null;
                final String updatedrole = data.getRole();
                ArrayList<CustomRole>customRoles = CustomRole.getAllCustomRole(user.roles);

                if (customRoles.size()>0){
                    currentrole = customRoles.get(0).getCode();

                    final String finalCurrentrole = currentrole;
                    dataManager.removeRoleForUser(user.userId, currentrole, new OnCompleteListeners.updateUserRecordWithUserIdListener() {
                        @Override
                        public void updateUserRecordWithUserIdCallback(Boolean status, Error error) {

                            if (error == null){
                                System.out.println("removeRoleForUser Success!");
                                for (int i = 0; i <user.roles.size() ; i++) {
                                    String role = user.roles.get(i);
                                    if (role.equalsIgnoreCase(finalCurrentrole)){
                                        user.roles.remove(i);
                                        break;
                                    }
                                }

                                dataManager.addRoleForUser(user.userId, updatedrole, new OnCompleteListeners.updateUserRecordWithUserIdListener() {
                                    @Override
                                    public void updateUserRecordWithUserIdCallback(Boolean status, Error error) {
                                        if (error == null){
                                            user.roles.add(updatedrole);
                                            System.out.println("addRoleForUser Success!");
                                            progressDialog.dismiss();

                                            if (IsuserRemoved){
                                                removeDialog.dismiss();
                                                Exit();
                                            }else {
                                                SCMTool.DisplaySnackbar("UPDATES SAVED!",root);
                                                Refresh();
                                            }
                                        }else {
                                            progressDialog.dismiss();
                                            SCMTool.DisplaySnackbar("ERROR SAVING UPDATE",root);
                                        }
                                    }
                                });
                            }else {
                                progressDialog.dismiss();
                                SCMTool.DisplaySnackbar("ERROR SAVING UPDATE",root);
                            }
                        }

                    });
                }else {

                    dataManager.addRoleForUser(user.userId, updatedrole, new OnCompleteListeners.updateUserRecordWithUserIdListener() {
                        @Override
                        public void updateUserRecordWithUserIdCallback(Boolean status, Error error) {
                            if (error == null){
                                ArrayList<String>strings = new ArrayList<>();
                                strings.add(updatedrole);
                                user.roles = strings;
                                System.out.println("addRoleForUser Success!");
                                progressDialog.dismiss();
                                SCMTool.DisplaySnackbar("New Employee Saved!",root);

                                Refresh();

                            }else {
                                progressDialog.dismiss();
                                SCMTool.DisplaySnackbar("ERROR Creating New Employee",root);
                            }
                        }
                    });

                }

            }catch (Exception e){
                e.printStackTrace();
            }

        }else {
            progressDialog.dismiss();
            if (IsuserRemoved){
                removeDialog.dismiss();
                Exit();
            }else {
                SCMTool.DisplaySnackbar("UPDATES SAVED!",root);
                Refresh();
            }
        }


    }

    private void AddNewUserLocal(CustomSite site){
       UserProfileData data = UserProfileData.getInstance();
        if (data.isCreate()){
            CustomSite customSite = QualificationData.getInstance().getSelectedSite();
            if (customSite.getSite().sitename.equalsIgnoreCase(site.getSite().sitename)){
                CustomUser newUser = new CustomUser();
                newUser.setUser(user);
                QualificationData.getInstance().getUsers().add(newUser);
            }
        }
    }

    private void RemoveUserLocal(String sitename){
        try {
            QualificationData data = QualificationData.getInstance();
            UserProfileData userdata = UserProfileData.getInstance();
            ManagementTeamData managementdata = ManagementTeamData.getInstance();
            Site site = data.getSelectedSite().getSite();
            ArrayList<CustomUser>users = new ArrayList<>();
            if (userdata.getPrevActivity().equalsIgnoreCase("MT")){
                users = managementdata.getThriftusers();
            }else {
                users = data.getUsers();

            }
            if (sitename.equalsIgnoreCase(site.sitename)) {
                for (int i = 0; i < users.size(); i++) {
                    CustomUser user = users.get(i);
                    if (user.getUser().userId.equalsIgnoreCase(userdata.getCustomUser().getUser().userId)) {
                        removeDialog = new ProgressDialog(this);
                        removeDialog.setTitle("Please Wait");
                        removeDialog.setMessage("Removing User");
                        removeDialog.show();
                        users.remove(i);
                        IsuserRemoved = true;
                        break;
                    }
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private boolean ValidString(String string){
        boolean valid = false;
        if (string.length()>0){
            valid = true;
        }
        return  valid;
    }

    private CustomUser getCurrentGM(final ProgressDialog progressDialog) {
        CustomUser CurrentGM = null;
        final QualificationData data = QualificationData.getInstance();
        if (data.getUsers() != null) {

            ArrayList<CustomUser> users = data.getUsers();
            for (CustomUser customUser : users) {
                if (customUser.getUser().active){
                    if (customUser.getUser().roles.contains("GM")) {
                        CurrentGM = customUser;
                        break;
                    }
                }
            }
        } else {
            String siteid="";
            if (data.getSelectedSite()!=null){
                 siteid= data.getSelectedSite().getSite().siteid;

            }else {
                siteid= SCMDataManager.getInstance().getSelectedSite().getSite().siteid;
            }
            dataManager = aPimmDataManager.getInstance();
            dataManager.getStoreUsers(siteid, new OnCompleteListeners.getStoreUsersCallbackListener() {
                @Override
                public void getStoreUsersCallback(ArrayList<User> storeUsers, Error error) {
                    if (error == null) {
                        ArrayList<CustomUser>users = new ArrayList<>();
                        for (User user : storeUsers){
                            users.add(new CustomUser(user));
                        }
                        data.setUsers(users);
                        getCurrentGM(progressDialog);

                    } else {
                        progressDialog.dismiss();
                        SCMTool.DisplaySnackbar("ERROR Checking New Employee",root);
                    }
                }
            });

        }
        return CurrentGM;
    }

    private void Refresh(){
        UserProfileData.getInstance().setUserEditEnabled(false);
        UserProfileData.getInstance().setIsupdate(true);
        UserProfileData.getInstance().setCreate(false);
        Lock();
    }


    private void DisplayDeleteConfirmation(final User user, final Site site){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.user_profile_delete_confirmation, null);
        dialogBuilder.setView(view);
        final AlertDialog alertDialog = dialogBuilder.create();
        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.80);
        int height = (int)(getResources().getDisplayMetrics().heightPixels*0.60);
        Objects.requireNonNull(alertDialog.getWindow()).setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
        Objects.requireNonNull(alertDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Objects.requireNonNull(alertDialog.getWindow()).getAttributes().windowAnimations = R.style.slide_down_up;
        alertDialog.show();


        Button cancel = view.findViewById(R.id.cancel);
        Button verify = view.findViewById(R.id.verify);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                DeleteUser(user,site);
            }
        });

    }

    private void DeleteUser(final User user, final Site site){
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please Wait");
        progressDialog.setMessage("Deleting User");
        progressDialog.setCancelable(false);
        progressDialog.show();

        int counter = 0;
    final ArrayList<String>sites = new ArrayList<>();
        for (final String string : user.roles){
            for (CustomSite customSite : SCMDataManager.getInstance().getCustomSites()){
                if (customSite.getSite().sitename.equalsIgnoreCase(string)){
                    counter++;
                    sites.add(string);
                    final int finalCounter = counter;
                    dataManager.RemoveUserFromStore(user.userId, site.siteid, new OnCompleteListeners.updateUserRecordWithUserIdListener() {
                        @Override
                        public void updateUserRecordWithUserIdCallback(Boolean status, Error error) {
                            if (error == null){
                                System.out.println("RemoveUserFromStore Success!");

                                dataManager.removeRoleForUser(user.userId,string , new OnCompleteListeners.updateUserRecordWithUserIdListener() {
                                    @Override
                                    public void updateUserRecordWithUserIdCallback(Boolean status, Error error) {
                                        if (error == null){
                                            System.out.println("remveRoleForUser Success!");

                                            if (finalCounter ==sites.size()){
                                                Exit();
                                                user.active = false;
                                                progressDialog.dismiss();
                                            }
                                        }else {
                                            progressDialog.dismiss();
                                            SCMTool.DisplaySnackbar("ERROR DELETING UPDATE",root);
                                        }
                                    }
                                });
                            }else {
                                progressDialog.dismiss();
                                SCMTool.DisplaySnackbar("ERROR DELETING USER",root);
                            }
                        }
                    });

                }
            }

        }
    }

    private void addCertToserver(User user){
        try {
            UserProfileData data = UserProfileData.getInstance();
            if (data.getNewCertification()!=null){
                for (Certification certification: data.getNewCertification()){
                    try {
                        dataManager.updateStarRatingForUserCertification(user.userId, certification.Certification,certification.StarRating, new OnCompleteListeners.updateUserRecordWithUserIdListener() {
                            @Override
                            public void updateUserRecordWithUserIdCallback(Boolean status, Error error) {
                                if (error ==null){
                                    System.out.println(" addCertToserver Success ");
                                }
                            }
                        });
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
            }
            data.getCustomUser().getUser().certificationList = data.getNewCertification();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

}
