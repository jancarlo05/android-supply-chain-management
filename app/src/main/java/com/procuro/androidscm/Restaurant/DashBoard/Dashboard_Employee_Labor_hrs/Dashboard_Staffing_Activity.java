package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.DailyLabor.Dashboard_DailyLabor_fragment;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.DailySchedule.Dashboard_staffing_daily_fragment;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.PositionPlan.Dashboard_PositionPlan_fragment;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.Position_Coverage.Dashboard_posiiton_coverage_fragment;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.WeeklySchedule.Dashboard_staffing_weekly_fragment;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.WeeklySchedule.WeeklyPickerDate;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.DashboardKitchenData;
import com.procuro.apimmdatamanagerlib.DocumentDTO;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.PimmForm;
import com.procuro.apimmdatamanagerlib.SMSPositioning;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.SiteSales;
import com.procuro.apimmdatamanagerlib.SiteSalesDefault;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.TimeZone;

import static com.procuro.androidscm.Restaurant.DashBoard.Dashboard_SalePerformance.Dashboard_SalesPerformance.ConvertDefaultToSales;

public class Dashboard_Staffing_Activity extends AppCompatActivity {

    info.hoang8f.android.segmented.SegmentedGroup infogroup;
    RadioButton daily,weekly,monthly,position_covered,daily_labor;
    TextView title;
    Button back;
    aPimmDataManager dataManager = aPimmDataManager.getInstance();
    ProgressDialog progressDialog;
    Site site = SCMDataManager.getInstance().getSelectedSite().getSite();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_staffing);

        infogroup = findViewById(R.id.segmented2);
        daily = findViewById(R.id.daily);
        weekly = findViewById(R.id.weekly);
        monthly = findViewById(R.id.monthly);
        title = findViewById(R.id.username);
        back = findViewById(R.id.home);
        position_covered = findViewById(R.id.position_covered);
        daily_labor = findViewById(R.id.daily_labor);

        setupOnclicks();

        title.setText(SCMDataManager.getInstance().getSelectedSite().getSite().sitename);

        CheckScheduleRequirements();

        CreateWeekPickerData();

    }

    private void setupOnclicks(){
        infogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if ( i == daily.getId()){
                    DisplayDaily();
                }else if ( i == weekly.getId()){
                    DisplayWeekly();
                }else if (i == monthly.getId()){
                    DisplayMonthly();
                }else if (i == position_covered.getId()){
                    DisplayPositionCovered();
                }else if (i == daily_labor.getId()){
                    DisplayDailyLabor();
                }
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void DisplayDailyLabor() {
        getSupportFragmentManager().beginTransaction().replace(R.id.cleaning_fragment_container,
                new Dashboard_DailyLabor_fragment()).commit();
    }

    private void DisplayPositionCovered() {
        getSupportFragmentManager().beginTransaction().replace(R.id.cleaning_fragment_container,
                new Dashboard_posiiton_coverage_fragment()).commit();
    }

    private void DisplayDaily() {
        getSupportFragmentManager().beginTransaction().replace(R.id.cleaning_fragment_container,
                new Dashboard_staffing_daily_fragment()).commit();
    }

    private void DisplayWeekly() {
       getSupportFragmentManager().beginTransaction().replace(R.id.cleaning_fragment_container,
                new Dashboard_staffing_weekly_fragment()).commit();
    }

    private void DisplayMonthly() {
        getSupportFragmentManager().beginTransaction().replace(R.id.cleaning_fragment_container,
                new Dashboard_PositionPlan_fragment()).commit();
    }



    private void CreateWeekPickerData(){
        SCMDataManager data = SCMDataManager.getInstance();
        if (data.getWeeklyPickerDates() ==null){
            ArrayList<WeeklyPickerDate>weeklyPickerDates = new ArrayList<>();
            int weeksOfYear = Calendar.getInstance().getActualMaximum(Calendar.WEEK_OF_YEAR);

            for (int i = -getPrevYear(); i <weeksOfYear+getFutureYear(); i++) {
                getStartEndOFWeek(i,weeklyPickerDates);
            }
            data.setWeeklyPickerDates(weeklyPickerDates);
        }
    }

    private int getPrevYear(){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR,-1);
        return calendar.getActualMaximum(Calendar.WEEK_OF_YEAR);
    }

    private int getFutureYear(){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR,+1);
        return calendar.getActualMaximum(Calendar.WEEK_OF_YEAR);
    }

    void getStartEndOFWeek(int enterWeek,ArrayList<WeeklyPickerDate>weeklyPickerDates){

        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy"); // PST`
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        calendar.set(Calendar.WEEK_OF_YEAR, enterWeek);
        ArrayList<Date> Weekdays = new ArrayList<>();
        int delta = -calendar.get(Calendar.DAY_OF_WEEK) + 2; //add 2 if your week start on monday
        calendar.add(Calendar.DAY_OF_MONTH, delta );
        for (int i = 0; i < 7; i++) {
            Weekdays.add(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        String name = formatter.format(Weekdays.get(0))+ " to " +formatter.format(Weekdays.get(Weekdays.size()-1));
        weeklyPickerDates.add(new WeeklyPickerDate(name,Weekdays,enterWeek));

    }

    private void CheckScheduleRequirements(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please Wait");
        progressDialog.setMessage("Downloading Data");
        progressDialog.setCancelable(false);
        progressDialog.show();


        SCMDataManager data = SCMDataManager.getInstance();

        if (data.getSiteSales()==null){
            setUpDailySalesPerformance();
        }
        if (data.getStoreUsers()==null){
            DownloadStoreUser(data.getSelectedSite().getSite());
        }
//        if (data.getPositionForm()==null){
//            Download_getFormDefinitionListForAppId(data.getSelectedSite().getSite());
//        }
        if (data.getDashboardKitchenData()==null){
            Download_DocumentListForReferenceId();
        }else {
            daily_labor.setChecked(true);
            progressDialog.dismiss();
        }


    }
    private void DownloadStoreUser(Site site){
        dataManager.getStoreUsers(site.siteid, new OnCompleteListeners.getStoreUsersCallbackListener() {
            @Override
            public void getStoreUsersCallback(ArrayList<User> storeUsers, Error error) {
                if (error == null){
                    try {
                        SCMDataManager.getInstance().setStoreUsers(SCMTool.ConvertUserToCustomUser(storeUsers));
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });
    }
    private void Download_getFormDefinitionListForAppId(final Site site){
        dataManager.getFormDefinitionListForAppId(new OnCompleteListeners.getFormDefinitionListForAppIdCallbackListener() {
            @Override
            public void getFormDefinitionListForAppIdCallback(ArrayList<PimmForm> pimmFormArrayList, Error error) {
                if (error==null){
                    for (final PimmForm form: pimmFormArrayList){
                        System.out.println("FORM NAME :"+form.name);
                        if (form.name.equalsIgnoreCase("SMS:Positioning")) {
                            Download_SMS_POSITIONING(site.siteid, form.formDefinitionID);
                        }
                    }
                }
            }
        });
    }

    private void Download_DocumentListForReferenceId(){
        final ArrayList<String>strings = new ArrayList<>();

        dataManager.getDocumentListForReferenceId(SCMDataManager.getInstance().getSelectedSite().getSite().siteid, new OnCompleteListeners.getDocumentListForReferenceIdCallbackListener() {
            @Override
            public void getDocumentListForReferenceIdCallback(ArrayList<DocumentDTO> documentDTOArrayList, Error error) {
                if (error == null){
                    for (DocumentDTO documentDTO : documentDTOArrayList){

                        if (documentDTO.category.equalsIgnoreCase("Employee_Position_Optimization_Data")){
                            strings.add(documentDTO.category);
                            System.out.println("Employee_Position_Optimization_Data : START");
                            Download_EMPLOYEE_POSITION_DATA(documentDTO.documentId,documentDTO.category);

                        }
                    }

                }else {
                    daily_labor.setChecked(true);
                    progressDialog.dismiss();
                }
            }
        });

    }

    private void Download_SMS_POSITIONING(String siteID,String formID){

        dataManager.getLatestFormForReferenceId(siteID, formID, new OnCompleteListeners.getLatestFormForReferenceIdCallbackListener() {
            @Override
            public void getLatestFormForReferenceId(PimmForm pimmForm, Error error) {
                if (error == null){
                    try {
                        if (pimmForm!=null){
                            if (pimmForm.StringBody!=null){
                                JSONObject jsonObject = new JSONObject(pimmForm.StringBody);
                                SMSPositioning smsPositioning = new SMSPositioning();
                                smsPositioning.readFromJSONObject(jsonObject);
                                SCMDataManager.getInstance().setPositionForm(smsPositioning);
                                System.out.println("SMS:Positioning: "+jsonObject.toString());

                            }else {
                                daily_labor.setChecked(true);
                                progressDialog.dismiss();
                            }
                        }else {
                            daily_labor.setChecked(true);
                            progressDialog.dismiss();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    daily_labor.setChecked(true);
                    progressDialog.dismiss();
                }
            }
        });
    }
    private void Download_EMPLOYEE_POSITION_DATA(String documentDtoID, String category){

        dataManager.getDocumentContentForDocumentId(documentDtoID, category, new OnCompleteListeners.getDocumentContentForDocumentIdCallbackListener() {
            @Override
            public void getDocumentContentForDocumentIdCallback(Object obj, Error error) {
                if (error ==  null){
                    if (obj !=null){
                        JSONObject jsonObject = (JSONObject) obj;

                        DashboardKitchenData dashboardKitchenData = new DashboardKitchenData();
                        System.out.println("dashboardKitchenData DOWNLOADED");
                        System.out.println(jsonObject);
                        dashboardKitchenData.readFromJSONObject(jsonObject);
                        SCMDataManager.getInstance().setDashboardKitchenData(dashboardKitchenData);

                        daily_labor.setChecked(true);
                        progressDialog.dismiss();
                    }else {
                        daily_labor.setChecked(true);
                        progressDialog.dismiss();
                    }
                }else {
                    daily_labor.setChecked(true);
                    progressDialog.dismiss();
                }
            }
        });

    }

    private void setUpDailySalesPerformance(){

        try {
            Calendar calendar = Calendar.getInstance();
            calendar = SCMTool.getDateWithDaypart(calendar.getTime().getTime(),site.effectiveUTCOffset);
            if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
                calendar.add(Calendar.WEEK_OF_YEAR,-1);
            }
            int WeekOFYear = calendar.get(Calendar.WEEK_OF_YEAR);

            Calendar now = Calendar.getInstance();
            now.setTimeZone(TimeZone.getTimeZone("UTC"));
            now.set(Calendar.WEEK_OF_YEAR, WeekOFYear);
            now.add(Calendar.MINUTE,site.effectiveUTCOffset);

            final ArrayList<Date> Weekdays = new ArrayList<>();
            int delta = -now.get(Calendar.DAY_OF_WEEK) + 2; //add 2 if your week start on monday
            now.add(Calendar.DAY_OF_MONTH, delta );

            for (int i = 0; i < 7; i++) {
                Weekdays.add(now.getTime());
                now.add(Calendar.DAY_OF_MONTH, 1);
            }

            DownloadSalesData(getPrevWeek(calendar),Weekdays, calendar);


        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void DownloadSalesData(ArrayList<Date> prevdates, final ArrayList<Date>dates, final Calendar currendDate){
        final SCMDataManager scm = SCMDataManager.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("EEE MM-dd-yyyy");
        final ArrayList<SiteSales>siteSalesList = new ArrayList<>();
        final ArrayList<SiteSales>prevsiteSalesList = new ArrayList<>();
        final int[] counter = {0};

        for (final Date date : prevdates){
            System.out.println("PREV DATES : "+format.format(date));
            dataManager.getSiteSalesByDate(site.siteid, date, new OnCompleteListeners.getSiteSalesByDateListener() {
                @Override
                public void getSiteSalesByDateCallback(SiteSales siteSales, Error error) {
                    if (error == null){
                        prevsiteSalesList.add(siteSales);
                    }else {
                        SiteSales siteSale = new SiteSales();
                        siteSale.salesDate = date;
                        prevsiteSalesList.add(siteSale);
                    }
                }
            });
        }

        for (final Date date : dates){
            dataManager.getSiteSalesByDate(site.siteid, date, new OnCompleteListeners.getSiteSalesByDateListener() {
                @Override
                public void getSiteSalesByDateCallback(SiteSales siteSales, Error error) {
                    counter[0]++;
                    if (error == null){
                        if (siteSales!=null && siteSales.data!=null){
                            if (siteSales.data.dss!=0){
                                siteSalesList.add(siteSales);
                                if (counter[0] == dates.size()){
                                    SCMDataManager.getInstance().setSiteSales(siteSalesList);
                                    SCMDataManager.getInstance().setPrevSiteSales(prevsiteSalesList);
                                }
                            }
                            else if (siteSales.data.projected!=0){
                                siteSalesList.add(siteSales);
                                if (counter[0] == dates.size()){
                                    SCMDataManager.getInstance().setSiteSales(siteSalesList);
                                    SCMDataManager.getInstance().setPrevSiteSales(prevsiteSalesList);
                                }
                            }
                            else if (siteSales.data.sales!=null && siteSales.data.sales.size()>0){
                                siteSalesList.add(siteSales);
                                if (counter[0] == dates.size()){
                                    SCMDataManager.getInstance().setSiteSales(siteSalesList);
                                    SCMDataManager.getInstance().setPrevSiteSales(prevsiteSalesList);
                                }
                            }else {
                                SiteSales siteSale = new SiteSales();
                                siteSale.salesDate = date;
                                getSiteDefault(siteSalesList,prevsiteSalesList,date,siteSale,counter[0],dates.size());

                            }

                        }else {
                            SiteSales siteSale = new SiteSales();
                            siteSale.salesDate = date;
                            getSiteDefault(siteSalesList,prevsiteSalesList,date,siteSale,counter[0],dates.size());
                        }
                    }else {
                        SiteSales siteSale = new SiteSales();
                        siteSale.salesDate = date;
                        getSiteDefault(siteSalesList,prevsiteSalesList,date,siteSale,counter[0],dates.size());
                    }

                }
            });
        }
    }

    private  void getSiteDefault(final ArrayList<SiteSales>siteSales,
                                 final ArrayList<SiteSales>prevSiteSales,
                                 final Date date, final SiteSales sales,
                                 final int DownloadedItems, final int TotalItems){

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.setTime(date);
        SimpleDateFormat format = new SimpleDateFormat("EEE");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        String StringDayOfWeek = format.format(date);
        int dayOfWeek = 0;

        if (StringDayOfWeek.equalsIgnoreCase("Tue")){
            dayOfWeek = 1;
        }else if (StringDayOfWeek.equalsIgnoreCase("Wed")){
            dayOfWeek = 2;
        }else if (StringDayOfWeek.equalsIgnoreCase("Thu")){
            dayOfWeek = 3;
        }else if (StringDayOfWeek.equalsIgnoreCase("Fri")){
            dayOfWeek = 4;
        }else if (StringDayOfWeek.equalsIgnoreCase("Sat")){
            dayOfWeek = 5;
        }else if (StringDayOfWeek.equalsIgnoreCase("Sun")){
            dayOfWeek = 6;
        }

        aPimmDataManager dataManager = aPimmDataManager.getInstance();
        dataManager.getDefaultSiteSalesForStoreType(dayOfWeek, 1, new OnCompleteListeners.getSiteSalesDefaultListener() {
            @Override
            public void getSiteSalesDefaultCallback(final SiteSalesDefault salesDefault, Error error) {
                if (error == null){
                    sales.data = ConvertDefaultToSales(salesDefault,date,siteSales,prevSiteSales);
                    siteSales.add(sales);
                    if (DownloadedItems == TotalItems){
                        Calendar currentDate = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                        currentDate.setTime(date);
                        SCMDataManager.getInstance().setSiteSales(siteSales);
                        SCMDataManager.getInstance().setPrevSiteSales(prevSiteSales);
                    }
                }else {
                    getSiteDefault(siteSales,prevSiteSales,date,sales,DownloadedItems,TotalItems);
                }
            }
        });
    }

    private ArrayList<Date> getPrevWeek(Calendar currentDate){
        ArrayList<Date> Weekdays = new ArrayList<>();
        try {
            Calendar now = Calendar.getInstance();
            now.setTimeZone(TimeZone.getTimeZone("UTC"));
            now.setTime(currentDate.getTime());
            now.add(Calendar.WEEK_OF_YEAR, -1);


            int delta = -now.get(Calendar.DAY_OF_WEEK) + 2; //add 2 if your week start on monday
            now.add(Calendar.DAY_OF_MONTH, delta );

            for (int i = 0; i < 7; i++) {
                Weekdays.add(now.getTime());
                now.add(Calendar.DAY_OF_MONTH, 1);
            }

            Collections.reverse(Weekdays);

        }catch (Exception e){
            e.printStackTrace();
        }

        return Weekdays;
    }

}
