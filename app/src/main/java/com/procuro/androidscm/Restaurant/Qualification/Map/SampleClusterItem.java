package com.procuro.androidscm.Restaurant.Qualification.Map;

import androidx.annotation.Nullable;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;
import com.procuro.androidscm.Restaurant.CustomSite;


public class SampleClusterItem implements ClusterItem {

    private final LatLng location;
    private String title;
    private String snippet;
    private BitmapDescriptor icon ;
    private CustomSite customSite;

    public SampleClusterItem(LatLng location, String title, String snippet, BitmapDescriptor icon,CustomSite customSite) {
        this.location = location;
        this.title = title;
        this.snippet = snippet;
        this.icon = icon;
        this.customSite  = customSite;
    }

    public BitmapDescriptor getIcon() {
        return icon;
    }

    public void setIcon(BitmapDescriptor icon) {
        this.icon = icon;
    }


    @Override
    public LatLng getPosition() {
        return location;
    }

    public CustomSite getCustomSite() {
        return customSite;
    }

    public void setCustomSite(CustomSite customSite) {
        this.customSite = customSite;
    }

    @Nullable
    @Override
    public String getTitle() {
        return title;
    }

    @Nullable
    @Override
    public String getSnippet() {
        return null;
    }
}