package com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.EquipmenList;

public class EquipmentList {
    String title;
    boolean enabled;


    public EquipmentList(String title, boolean enabled) {
        this.title = title;
        this.enabled = enabled;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
