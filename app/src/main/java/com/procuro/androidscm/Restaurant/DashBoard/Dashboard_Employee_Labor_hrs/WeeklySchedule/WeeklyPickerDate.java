package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.WeeklySchedule;

import java.util.ArrayList;
import java.util.Date;

public class WeeklyPickerDate {

    private String name;
    private ArrayList<Date> weekdays;
    private int weeknumber;

    public WeeklyPickerDate() {
    }

    public WeeklyPickerDate(String name, ArrayList<Date> weekdays, int weeknumber) {
        this.name = name;
        this.weekdays = weekdays;
        this.weeknumber = weeknumber;
    }

    public ArrayList<Date> getWeekdays() {
        return weekdays;
    }

    public void setWeekdays(ArrayList<Date> weekdays) {
        this.weekdays = weekdays;
    }

    public int getWeeknumber() {
        return weeknumber;
    }

    public void setWeeknumber(int weeknumber) {
        this.weeknumber = weeknumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
