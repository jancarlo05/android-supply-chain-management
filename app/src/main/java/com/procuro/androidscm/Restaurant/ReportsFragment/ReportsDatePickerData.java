package com.procuro.androidscm.Restaurant.ReportsFragment;

import java.util.Date;

public class ReportsDatePickerData {

    private boolean selected;
    private Date date ;
    private String description;

    public ReportsDatePickerData(boolean selected, Date date, String description) {
        this.selected = selected;
        this.date = date;
        this.description = description;
    }

    public ReportsDatePickerData(boolean selected, Date date) {
        this.selected = selected;
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
