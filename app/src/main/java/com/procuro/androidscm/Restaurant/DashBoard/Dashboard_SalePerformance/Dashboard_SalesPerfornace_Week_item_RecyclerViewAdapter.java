package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_SalePerformance;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.Daypart;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.Schema.Dayparts;
import com.procuro.androidscm.Restaurant.Schema.Schema;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.SiteSales;
import com.procuro.apimmdatamanagerlib.SiteSalesActual;
import com.procuro.apimmdatamanagerlib.SiteSalesData;
import com.procuro.apimmdatamanagerlib.SiteSalesDaypart;
import com.procuro.apimmdatamanagerlib.SiteSalesDefault;
import com.procuro.apimmdatamanagerlib.SiteSalesDefaultData;
import com.procuro.apimmdatamanagerlib.SiteSalesForecast;
import com.procuro.apimmdatamanagerlib.SiteSalesForecastTime;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;


public class Dashboard_SalesPerfornace_Week_item_RecyclerViewAdapter extends RecyclerView.Adapter<Dashboard_SalesPerfornace_Week_item_RecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<String> arraylist;
    private ArrayList<String>header;
    private SiteSales siteSales;
    private SiteSales prev;
    private ArrayList<Double>forcasts;
    private ArrayList<Double>sales;
    private static DecimalFormat decimalFormat = new DecimalFormat("0.00");



    public Dashboard_SalesPerfornace_Week_item_RecyclerViewAdapter(Context context,
                                                                   ArrayList<String> arraylist,
                                                                   ArrayList<String>header,
                                                                   SiteSales siteSales, SiteSales prev) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;
        this.header = header;
        this.siteSales = siteSales;
        this.prev = prev;
        this.forcasts = new ArrayList<>();
        this.sales = new ArrayList<>();

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.daily_labor_cardview,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final String string = arraylist.get(position);

        holder.name.setText(string);

        ArrayList<String>items = new ArrayList<>();

        if (string.equalsIgnoreCase("Forecast")){
            setUpForecastData(prev,siteSales,items,false);
        }
        else if (string.equalsIgnoreCase("Actual")){
            setUpAcutalSales(prev,siteSales,items,false);
        }
        else if (string.equalsIgnoreCase("(+/-)")){
            ComputeDifference(items);
        }

        Dashboard_SalesPerformance_Week_Child_item_RecyclerViewAdapter adapter = new Dashboard_SalesPerformance_Week_Child_item_RecyclerViewAdapter(mContext,items,string,header.size());
        holder.recyclerView.setLayoutManager(new GridLayoutManager(mContext,header.size()));
        holder.recyclerView.setAdapter(adapter);

        if (position % 2 == 1) {
            holder.cardView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.light_orange2));
        } else {
            holder.cardView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.white));
        }
    }


    private void setUpForecastData(SiteSales prevSales,SiteSales selectedSales,ArrayList<String>items,boolean isYesterday){
       
        double prev = getForcast(prevSales);
        double total = getForcast(selectedSales);
        
        forcasts.add(prev);
        forcasts.add(total);
        
        items.add("$"+decimalFormat.format(prev));
        items.add("$"+decimalFormat.format(total)+getIndicator(siteSales));

        getForcastPerDaypart(selectedSales,items,total);
        
    }

    private void getForcastPerDaypart(SiteSales siteSales,ArrayList<String>items,double total){
         if (siteSales.data!=null){
            SiteSalesData data = siteSales.data;
            if (data.dayparts!=null){
                for (SiteSalesDaypart daypart : data.dayparts){
                    BigDecimal percentage = new BigDecimal("0");
                    for (SiteSalesForecast forecast : daypart.forecast){

                        Calendar end = Calendar.getInstance();
                        end.setTimeZone(TimeZone.getTimeZone("UTC"));
                        end.setTime(forecast.endTime);
                        end.add(Calendar.MINUTE,-1);

                        if (SCMTool.isBetween(daypart.startTime,daypart.endTime,forecast.startTime)){
                            percentage = percentage.add(forecast.percentage);

                        }else if (SCMTool.isBetween(daypart.startTime,daypart.endTime,end.getTime())){
                            percentage = percentage.add(forecast.percentage);

                        }

                    }
                    BigDecimal FORCASTRESULT = new BigDecimal(total).multiply(percentage);
                    items.add("$"+decimalFormat.format(FORCASTRESULT.doubleValue()));
                    forcasts.add(FORCASTRESULT.doubleValue());
                }
            }else {
                for (int i = 0; i <header.size() ; i++) {
                    items.add("$0.00");
                    forcasts.add(0.0);
                }
            }
        }else {
             for (int i = 0; i <header.size() ; i++) {
                 items.add("$0.00");
                 forcasts.add(0.0);
             }
         }
    }

    private double getForcast(SiteSales siteSales){
        if (siteSales.data!=null){
            SiteSalesData data = siteSales.data;
            double forcast =0;
            if (data.dss!=0) {
                forcast = Double.parseDouble(String.valueOf(data.dss));
            }
            else if (data.projected!=0){
                forcast = Double.parseDouble(String.valueOf(data.projected));
            }
            return forcast;
        }
        return 0;
    }
    private String getIndicator(SiteSales siteSales){
        String indidcator = "";
        if (siteSales.data!=null){
            SiteSalesData data = siteSales.data;
            double forcast =0;
            if (data.dss!=0) {
                indidcator = ":DSS";
            }
            else if (data.projected!=0){
                indidcator = ":Projected";
            }
            return indidcator;
        }
        return "";
    }

    private void setUpAcutalSales(SiteSales prevSales,SiteSales selectedSales,ArrayList<String>items,boolean isYesterday){

        double prev = getSales(prevSales);
        double total = getSales(selectedSales);

        sales.add(prev);
        sales.add(total);

        items.add("$"+decimalFormat.format(prev));
        items.add("$"+decimalFormat.format(total));

        getSalesPerDaypart(selectedSales,items);

    }

    private void getSalesPerDaypart(SiteSales siteSales,ArrayList<String>items){
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");

        if (siteSales.data!=null){
            SiteSalesData data = siteSales.data;

            if (data.dayparts!=null && data.sales!=null){


                for (SiteSalesDaypart daypart : data.dayparts){
                    double amounts = 0;


                    System.out.println("daypart :"+daypart.name);
                    System.out.println("daypart Start :"+format.format(daypart.startTime));
                    System.out.println("daypart End :"+format.format(daypart.endTime));

                    for (SiteSalesActual actual : data.sales){

                        Calendar start = Calendar.getInstance();
                        start.setTimeZone(TimeZone.getTimeZone("UTC"));
                        start.setTime(actual.startTime);
                        start.add(Calendar.MINUTE,1);

                        Calendar end = Calendar.getInstance();
                        end.setTimeZone(TimeZone.getTimeZone("UTC"));
                        end.setTime(actual.endTime);
                        end.add(Calendar.MINUTE,-1);

                        if (daypart.name.equalsIgnoreCase("1") || daypart.name.equalsIgnoreCase("2")){

                            if (format.format(actual.startTime).equalsIgnoreCase("10:00")){
                                BigDecimal transition = new BigDecimal(actual.amount).multiply(getDaypartTransition(data.dayparts));

                                if (daypart.name.equalsIgnoreCase("2")){
                                    BigDecimal dp2Transition = new BigDecimal(actual.amount).subtract(transition);
                                    amounts += dp2Transition.doubleValue();

                                    System.out.println("TIME Start : "+format.format(actual.startTime)+ " | " +Double.parseDouble(dp2Transition.toString()) + " | "+
                                            daypart.name +" | "+"TIME END : "+format.format(actual.endTime));
                                }else {
                                    amounts += transition.doubleValue();
                                    System.out.println("TIME Start : "+format.format(actual.startTime)+ " | " +Double.parseDouble(transition.toString()) + " | "+
                                            daypart.name +" | "+"TIME END : "+format.format(actual.endTime));
                                }


                            }
                            else {
                                if (SCMTool.isBetween(daypart.startTime,daypart.endTime,start.getTime())){
                                    if (SCMTool.isBetween(daypart.startTime,daypart.endTime,end.getTime())){
                                        amounts +=actual.amount;
                                        System.out.println("TIME Start : "+format.format(start.getTime())+ " | " +actual.amount + " | "+daypart.name +" | "+"TIME END : "+format.format(end.getTime()));
                                    }
                                }
                            }
                        }else {
                            if (SCMTool.isBetween(daypart.startTime,daypart.endTime,start.getTime())){
                                if (SCMTool.isBetween(daypart.startTime,daypart.endTime,end.getTime())){
                                    amounts +=actual.amount;
                                    System.out.println("TIME Start : "+format.format(start.getTime())+ " | " +actual.amount + " | "+daypart.name +" | "+"TIME END : "+format.format(end.getTime()));
                                }
                            }
                        }
                    }
//                    double roundOff = Math.round(amounts * 100.00) / 100.00;
                    double roundOff = amounts;
                    sales.add(roundOff);
                    items.add("$"+decimalFormat.format(roundOff));
                }
            }else {
                for (int i = 0; i <6 ; i++) {
                    items.add   ("$0.00");
                    sales.add(0.0);
                }
            }
        }else {
            for (int i = 0; i <6 ; i++) {
                items.add("$0.00");
                sales.add(0.0);
            }
        }
    }

    private BigDecimal getDaypartTransition(ArrayList<SiteSalesDaypart>dayparts){

        BigDecimal start = new BigDecimal("0");
        BigDecimal end = new BigDecimal("0");

        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        for (SiteSalesDaypart daypart : dayparts){
            if (daypart.name.equalsIgnoreCase("1")){
                for (SiteSalesForecast forecast : daypart.forecast){
                    if (format.format(forecast.startTime).equalsIgnoreCase("10:00")){
                        start = forecast.percentage;
                        break;
                    }
                }
            }else if (daypart.name.equalsIgnoreCase("2")){
                for (SiteSalesForecast forecast : daypart.forecast){
                    if (format.format(forecast.startTime).equalsIgnoreCase("10:30")){
                        end = forecast.percentage;
                        break;
                    }
                }
            }
        }
        return CreateTransitionPercentage(start,end);
    }

    private BigDecimal CreateTransitionPercentage(BigDecimal decimal10,BigDecimal decimal11){
        BigDecimal bigDecimalTotal = new BigDecimal("0");
        bigDecimalTotal = bigDecimalTotal.add(decimal10);
        bigDecimalTotal = bigDecimalTotal.add(decimal11);
        return decimal10.divide(bigDecimalTotal, 4);
    }

    private double getSales(SiteSales siteSales){
        double sales =0;
        if (siteSales.data!=null){
            SiteSalesData data = siteSales.data;
            if (data.sales!=null){
                for (SiteSalesActual actual : data.sales){
                    sales +=actual.amount;
                }
            }else {
                return sales;
            }
        }
        return sales;
    }

    private void ComputeDifference(ArrayList<String>items){
        double difference = 0;
        for (int i = 0; i <forcasts.size() ; i++) {
            double forecast = forcasts.get(i);
            double sale =sales.get(i);

            boolean isNegative = false;
            difference = Math.round((sale - forecast) * 100.00) / 100.00;

            if (i != 0){
                if (difference<0){
                    difference = difference * -1;
                    items.add("-$"+decimalFormat.format(difference));
                    isNegative = true;
                }else {
                    items.add("$"+decimalFormat.format(difference));
                }

            }else {
                items.add("");
            }

            if (i==1){
                Dashboard_SalesPerformance.forecast.setText("$"+decimalFormat.format(forecast));
                Dashboard_SalesPerformance.actual.setText("$"+decimalFormat.format(sale));

                if (isNegative){
                    Dashboard_SalesPerformance.difference.setText("-$"+decimalFormat.format(difference));
                    Dashboard_SalesPerformance.difference.setTextColor(ContextCompat.getColor(mContext,R.color.red));
                }else {
                    Dashboard_SalesPerformance.difference.setText("$"+decimalFormat.format(difference));
                    Dashboard_SalesPerformance.difference.setTextColor(ContextCompat.getColor(mContext,R.color.black));

                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return arraylist.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        RecyclerView recyclerView;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
             name = itemView.findViewById(R.id.name);
             recyclerView = itemView.findViewById(R.id.recyclerView);
             cardView = itemView.findViewById(R.id.cardview_id);

        }
    }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    private void setupYesterday(){


    }
}
