package com.procuro.androidscm.Restaurant.DashBoard;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;

import com.procuro.androidscm.R;
import com.procuro.androidscm.SCMDataManager;

import java.util.ArrayList;
import java.util.Objects;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

public class Dashboard_Managers_Confirmation_DialogFragment extends DialogFragment {


    private Button close ;
    private ConstraintLayout manager_container;
    private TextView name,ans1,ans2,ans3,ans4,num1,num2,num3,num4,num5,num6,num7,num8,num9,num0,delete;
    private StringBuilder answer = new StringBuilder();
    private ListView listView;
    private Dashboard_Managers_Confirmation_SelectEmployee_list_adapter adapter;
    private LinearLayout numpad_container;
    public static String url;
    public static String title;
    private ArrayList<CustomUser>users = SCMDataManager.getInstance().getStoreUsers();
    private ArrayList<CustomUser>managerOnduty = new ArrayList<>();
    private boolean ManagerAvailabe = false;


    public static Dashboard_Managers_Confirmation_DialogFragment newInstance(String videourl,String videotitle) {
        url = videourl;
        title = videotitle;

        return new Dashboard_Managers_Confirmation_DialogFragment();
    }
    @Override
        public int getTheme() {
        return R.style.slide_down_up;
        }

    @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.managers_confirmation, container, false);

//                    int height = (int)(getResources().getDisplayMetrics().heightPixels*0.90);
//                    int width = 320;
//
        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.95);
        int height = (int)(getResources().getDisplayMetrics().heightPixels*0.95);
        Objects.requireNonNull(getDialog().getWindow()).setLayout(width, height);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.slide_down_up;
        view.setFocusableInTouchMode(true);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        close = view.findViewById(R.id.close);

        manager_container = view.findViewById(R.id.manager_container);
        name = view.findViewById(R.id.name);
        ans1 = view.findViewById(R.id.ans1);
        ans2 = view.findViewById(R.id.ans2);
        ans3 = view.findViewById(R.id.ans3);
        ans4 = view.findViewById(R.id.ans4);
        num0 = view.findViewById(R.id.num0);
        num1 = view.findViewById(R.id.num1);
        num2 = view.findViewById(R.id.num2);
        num3 = view.findViewById(R.id.num3);
        num4 = view.findViewById(R.id.num4);
        num5 = view.findViewById(R.id.num5);
        num6 = view.findViewById(R.id.num6);
        num7 = view.findViewById(R.id.num7);
        num8 = view.findViewById(R.id.num8);
        num9 = view.findViewById(R.id.num9);
        delete = view.findViewById(R.id.delete);
        listView = view.findViewById(R.id.listview);
        numpad_container = view.findViewById(R.id.numpad_container);



        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        setupManagertsOnduty();

        DisplayEmployeeList();

        populateStringInBox();

        setupNumpad();


        manager_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayManagerPopup(getContext(),manager_container,name);
            }
        });

        return view;

        }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }



    private void DisplayEmployeeList(){
        adapter = new Dashboard_Managers_Confirmation_SelectEmployee_list_adapter(getContext(),
                users,getActivity(),numpad_container,ManagerAvailabe);
        listView.setAdapter(adapter);

    }

    private void populateStringInBox(){
        try {
            ans1.setBackgroundResource(R.drawable.empty_circle_black);
            ans2.setBackgroundResource(R.drawable.empty_circle_black);
            ans3.setBackgroundResource(R.drawable.empty_circle_black);
            ans4.setBackgroundResource(R.drawable.empty_circle_black);


            if (answer.toString().length()>0){
                for (int i = 0; i <answer.toString().length() ; i++) {
                    if (i == 0){
                        ans1.setBackgroundResource(R.drawable.full_circle_black);
                    }else if (i == 1){
                        ans2.setBackgroundResource(R.drawable.full_circle_black);
                    }else if (i == 2){
                        ans3.setBackgroundResource(R.drawable.full_circle_black);
                    }else if (i == 3){
                        ans4.setBackgroundResource(R.drawable.full_circle_black);
                    }
                }
            }

            if (answer.length()==4){
                DisplayVideoPlayer();
            }


        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void setupNumpad(){


        try {

            num1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("1");
                        populateStringInBox();
                    }

                }
            });

            num2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("2");
                        populateStringInBox();
                    }
                }
            });

            num3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("3");
                        populateStringInBox();
                    }
                }
            });

            num4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("4");
                        populateStringInBox();
                    }
                }
            });

            num5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("5");
                        populateStringInBox();
                    }
                }
            });

            num6.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("6");
                        populateStringInBox();
                    }
                }
            });

            num7.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("7");
                        populateStringInBox();
                    }
                }
            });

            num8.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("8");
                        populateStringInBox();
                    }
                }
            });

            num9.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("9");
                        populateStringInBox();
                    }
                }
            });

            num0.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("0");
                        populateStringInBox();
                    }
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.toString().length()>0){
                        answer.setLength(answer.length() - 1);
                        populateStringInBox();
                    }
                }
            });


        }catch (Exception e){
            e.printStackTrace();
        }


    }


    private void DisplayVideoPlayer(){
        try {
            Intent intent = new Intent(getContext(), VideoPlayerActivity.class);
            intent.putExtra("Url",url);
            intent.putExtra("name",title);
            getActivity().startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
            dismiss();
        }catch (Exception e){
            e.printStackTrace();

        }

    }


    private void setupManagertsOnduty(){

        try {
            for (CustomUser user : users){
                user.setSelected(false);
                if (user.getEmployeePositionData()!=null){
                    managerOnduty.add(user);
                    ManagerAvailabe = true;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        try {
            if (ManagerAvailabe){
                boolean managerselected = false;

                for (CustomUser user : managerOnduty){
                    if (user.isSelectedmanager()){
                        managerselected = true;
                        name.setText(user.getUser().firstName);
                        name.append(" ");
                        name.append(user.getUser().lastName);
                        break;
                    }
                }

                if (!managerselected){
                    if (managerOnduty.size()>0){
                        managerOnduty.get(0).setSelectedmanager(true);
                        name.setText(managerOnduty.get(0).getUser().firstName);
                        name.append(" ");
                        name.append(managerOnduty.get(0).getUser().lastName);
                    }
                }
            }else {
            name.setText("No Manager(s) assigned");
            }
        }catch (Exception e ){
            e.printStackTrace();
        }


    }


    private void DisplayManagerPopup(final Context context, View anchor, TextView title) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.BOTTOM)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(false)
                .transparentOverlay(false)
                .arrowColor(Color.WHITE)
                .transparentOverlay(false)
                .contentView(R.layout.managers_onduty_popup)
                .focusable(true)
                .build();
        tooltip.show();

        TextView message = tooltip.findViewById(R.id.message);
        ListView listView = tooltip.findViewById(R.id.listView);

        if (managerOnduty.size()>0){
            message.setVisibility(View.GONE);
            Dashboard_Managers_Confirmation_SelectManager_list_adapter adapter = new Dashboard_Managers_Confirmation_SelectManager_list_adapter(context,managerOnduty,title,tooltip);
            listView.setAdapter(adapter);
        }

    }

}
