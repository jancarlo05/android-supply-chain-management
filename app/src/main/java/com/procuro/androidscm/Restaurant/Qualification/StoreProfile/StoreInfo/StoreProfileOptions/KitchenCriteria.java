package com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.StoreProfileOptions;

import java.util.ArrayList;

public class KitchenCriteria {

    private String criteria;
    private boolean OnOff;
    private ArrayList<KitchenDescription>descriptions;

    public KitchenCriteria(String criteria, ArrayList<KitchenDescription> descriptions, boolean OnOff) {
        this.criteria = criteria;
        this.descriptions = descriptions;
        this.OnOff = OnOff;
    }

    public KitchenCriteria(String criteria,ArrayList<KitchenDescription> descriptions) {
        this.criteria = criteria;
        this.descriptions = descriptions;
    }


    public String getCriteria() {
        return criteria;
    }

    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }

    public boolean isOnOff() {
        return OnOff;
    }

    public void setOnOff(boolean onOff) {
        this.OnOff = onOff;
    }

    public ArrayList<KitchenDescription> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(ArrayList<KitchenDescription> descriptions) {
        this.descriptions = descriptions;
    }
}
