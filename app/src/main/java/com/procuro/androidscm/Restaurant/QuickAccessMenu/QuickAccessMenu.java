package com.procuro.androidscm.Restaurant.QuickAccessMenu;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.Calendar.CalendarActivity;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.DashBoard.DashboardActivity;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.Dashboard_Staffing_Activity;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_SalePerformance.Dashboard_SalesPerformance;
import com.procuro.androidscm.Restaurant.EmployeeList.EmployeeListActivity;
import com.procuro.androidscm.Restaurant.Guide.GuideActivity;
import com.procuro.androidscm.Restaurant.Guide.GuideData;
import com.procuro.androidscm.Restaurant.PimmyNews.Covid_DialogFragment;
import com.procuro.androidscm.Restaurant.PimmyNews.Pimmy_News_DialogFragment;
import com.procuro.androidscm.Restaurant.Qualification.QualificationActivity;
import com.procuro.androidscm.Restaurant.Qualification.QualificationActivityFragment;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.Restaurant.Restaurant_home_page;
import com.procuro.androidscm.Restaurant.SOS.SOSActivity;
import com.procuro.androidscm.Restaurant.SOS.SOSData;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileActivityOverAll;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileData;
import com.procuro.androidscm.SCMDataManager;

import java.util.Objects;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

public class QuickAccessMenu {


    public static void DisplayQuickAMenu(final Context context, View anchor, final FragmentActivity fragmentActivity) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.BOTTOM)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .transparentOverlay(false)
                .arrowColor(ContextCompat.getColor(context, R.color.black))
                .transparentOverlay(true)
                .contentView(R.layout.quick_access_menu)
                .focusable(true)
                .build();
        tooltip.show();

        ConstraintLayout calendar,dop,employee_sched,help,news,reports_portal,review_n_verify,
                skpe,sos,storeprofile,temp_check,user_profile,employee_list,hr_sales_labor;

        calendar = tooltip.findViewById(R.id.calendar);
        dop = tooltip.findViewById(R.id.dashboard);
        employee_sched = tooltip.findViewById(R.id.employee_schedule);
        help = tooltip.findViewById(R.id.menu_help);
        news = tooltip.findViewById(R.id.menu_news);
        reports_portal = tooltip.findViewById(R.id.menu_reports_portal);
        review_n_verify = tooltip.findViewById(R.id.menu_review_n_verify);
        skpe = tooltip.findViewById(R.id.menu_skype);
        sos = tooltip.findViewById(R.id.menu_sos);
        storeprofile = tooltip.findViewById(R.id.menu_store_profile);
        temp_check = tooltip.findViewById(R.id.menu_temp_check);
        user_profile = tooltip.findViewById(R.id.menu_user_profile);
        employee_list = tooltip.findViewById(R.id.employee_list);
        hr_sales_labor = tooltip.findViewById(R.id.hr_sales_labor);

        TextView version = tooltip.findViewById(R.id.version);

        //remove
        temp_check.setVisibility(View.GONE);


        //disable
        reports_portal.setEnabled(false);
        reports_portal.setAlpha(.5f);

        version.setText(getBuildNumber(fragmentActivity));


        employee_sched.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SCMDataManager.getInstance().setDashboardKitchenData(null);
                SCMDataManager.getInstance().setStoreUsers(null);
                SCMDataManager.getInstance().setPositionForm(null);
                Intent intent = new Intent(fragmentActivity, Dashboard_Staffing_Activity.class);
                fragmentActivity.startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(fragmentActivity).toBundle());
                tooltip.dismiss();
            }
        });

        user_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(fragmentActivity, UserProfileActivityOverAll.class);
                UserProfileData data = new UserProfileData();
                data.setUserEditEnabled(false);
                data.setCustomSite(SCMDataManager.getInstance().getSelectedSite());
                CustomUser user = new CustomUser(SCMDataManager.getInstance().getUser());
                data.setCustomUser(user);
                data.setFromQuickAccess(true);
                data.setIsupdate(true);
                UserProfileData.setInstance(data);
                fragmentActivity.startActivity(intent);
                tooltip.dismiss();
            }
        });

        dop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(fragmentActivity, DashboardActivity.class);
                Objects.requireNonNull(fragmentActivity).startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(fragmentActivity).toBundle());
                tooltip.dismiss();
            }
        });

        storeprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(fragmentActivity, QualificationActivity.class);
                QualificationData data = new QualificationData();
                data.setSelectedSiteEnabled(true);
                data.setSelectedSite(SCMDataManager.getInstance().getSelectedSite());
                QualificationData.setInstance(data);

                Restaurant_home_page.setCustom(true);
                Restaurant_home_page.navView.setSelectedItemId(Restaurant_home_page.NAV_QUALIFICATIONS);
                fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.resturant_fragment_container,
                        new QualificationActivityFragment()).commit();

//                Objects.requireNonNull(fragmentActivity).startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(fragmentActivity).toBundle());
                tooltip.dismiss();
            }
        });

        sos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SOSData data = new SOSData();
                data.setSite(SCMDataManager.getInstance().getSelectedSite().getSite());
                SOSData.setInstance(data);
                Intent intent = new Intent(fragmentActivity, SOSActivity.class);
                Objects.requireNonNull(fragmentActivity).startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(fragmentActivity).toBundle());
                tooltip.dismiss();
            }
        });

        calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(fragmentActivity, CalendarActivity.class);
                Objects.requireNonNull(fragmentActivity).startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(fragmentActivity).toBundle());
                tooltip.dismiss();
            }
        });

        news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayPimmyNews(fragmentActivity);
                tooltip.dismiss();
            }
        });

        skpe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initiateSkypeUri(fragmentActivity,"");
                tooltip.dismiss();
            }
        });

        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpGuideModule(fragmentActivity);
            }
        });

        employee_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QualificationData data = new QualificationData();
                data.setSelectedSiteEnabled(true);
                data.setSelectedSite(SCMDataManager.getInstance().getSelectedSite());
                data.setUsers(SCMDataManager.getInstance().getStoreUsers());
                QualificationData.setInstance(data);
                Intent intent = new Intent(fragmentActivity, EmployeeListActivity.class);
                Objects.requireNonNull(fragmentActivity).startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(fragmentActivity).toBundle());
                tooltip.dismiss();
            }
        });


        hr_sales_labor.setAlpha(1);
        hr_sales_labor.setEnabled(true);
        hr_sales_labor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(fragmentActivity, Dashboard_SalesPerformance.class);
                fragmentActivity.startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(fragmentActivity).toBundle());
            }
        });

    }


    public static void DisplayPimmyNews(FragmentActivity activity){
        DialogFragment newFragment = Pimmy_News_DialogFragment.newInstance();
        assert activity.getFragmentManager() != null;
        newFragment.show(activity.getSupportFragmentManager(), "dialog");
    }

    public static void DisplayCovidFragment(FragmentActivity activity){
        DialogFragment newFragment = Covid_DialogFragment.newInstance();
        assert activity.getFragmentManager() != null;
        newFragment.show(activity.getSupportFragmentManager(), "dialog");
    }


    public static void initiateSkypeUri(Activity activity, String mySkypeUri) {

        // Make sure the Skype for Android client is installed.
        if (!isSkypeClientInstalled(activity)) {
            goToMarket(activity);
        }else {
            Uri skypeUri = Uri.parse(mySkypeUri);
            Intent myIntent = new Intent(Intent.ACTION_VIEW, skypeUri);
            myIntent.setComponent(new ComponentName("com.skype.raider", "com.skype.raider.Main"));
            myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivity(myIntent);
        }
    }

    public static boolean isSkypeClientInstalled(Activity activity) {

        PackageManager myPackageMgr = activity.getPackageManager();
        try {
            myPackageMgr.getPackageInfo("com.skype.raider", PackageManager.GET_ACTIVITIES);
        }
        catch (PackageManager.NameNotFoundException e) {
            return (false);
        }
        return (true);
    }

    public static void goToMarket(Activity activity) {
        Uri marketUri = Uri.parse("market://details?id=com.skype.raider");
        Intent myIntent = new Intent(Intent.ACTION_VIEW, marketUri);
        myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Objects.requireNonNull(activity).startActivity(myIntent);

    }

    public static void setUpGuideModule(Activity activity){
        GuideData data = GuideData.getInstance();
        data.setSelected(GuideActivity.GUIDE_AUDIT);
        Intent intent = new Intent(activity, GuideActivity.class);
        activity.startActivity(intent);

    }

    public static String getBuildNumber(FragmentActivity fragmentActivity){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Software Build ");
        PackageManager manager = fragmentActivity.getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(fragmentActivity.getPackageName(), PackageManager.GET_ACTIVITIES);
            stringBuilder.append(info.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }
}
