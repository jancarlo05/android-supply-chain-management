package com.procuro.androidscm.Restaurant.DashBoard.Dasboard_Main_Process;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.AllStorePopup.Status_SiteList_DialogFragment;
import com.procuro.androidscm.Restaurant.Calendar.CalendarActivity;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.DashBoard.DashboardActivity;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_CleaningSlider.DashboardCleaningSliderActivity;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_CommunicationNotes.Dashboard_CommunicationNotes;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_CustomerExperience.Dashboard_CustomerExperience;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_CustomerSatifaction.Dashboard_CustomerSatifaction;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.Dashboard_Staffing_Activity;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_SafetyDataSheets.Dashboard_SafetyDataSheets;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_SalePerformance.Dashboard_SalesPerformance;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_StoreRecord.Dashboard_StoreRecord;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Traning.PimmSMS.Dashboard_Pimmsms;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Traning.VTAs.Dashboard_Vtas_WITH_PDFVIER_Activity;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Traning.Wendys_U.DashboardTraningWendysU_With_VideoPlayerActivity;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_cleaning_tutorial_listviewAdapter;
import com.procuro.androidscm.Restaurant.DashBoard.Dashbord_VOC.Dashboard_VOC;
import com.procuro.androidscm.Restaurant.DashBoard.TutorialList;
import com.procuro.androidscm.Restaurant.EmployeeList.EmployeeListActivity;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourcesGrandChildList;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.Restaurant.QuickAccessMenu.QuickAccessMenu;
import com.procuro.androidscm.Restaurant.Restaurant_home_page;
import com.procuro.androidscm.Restaurant.SOS.SOSActivity;
import com.procuro.androidscm.Restaurant.SOS.SOSData;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileActivity;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileActivityOverAll;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileData;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.DocumentDTO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import io.github.douglasjunior.androidSimpleTooltip.OverlayView;
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

import static com.procuro.androidscm.Restaurant.DashBoard.DashboardActivity.CustomerExperience_Dropdown;
import static com.procuro.androidscm.Restaurant.DashBoard.DashboardActivity.Pimmsm;
import static com.procuro.androidscm.Restaurant.DashBoard.DashboardActivity.VTa;
import static com.procuro.androidscm.Restaurant.DashBoard.DashboardActivity.accuracy_container;
import static com.procuro.androidscm.Restaurant.DashBoard.DashboardActivity.cleanliness_container;
import static com.procuro.androidscm.Restaurant.DashBoard.DashboardActivity.customersatisfaction_Dropdown;
import static com.procuro.androidscm.Restaurant.DashBoard.DashboardActivity.friendliness_container;
import static com.procuro.androidscm.Restaurant.DashBoard.DashboardActivity.quiack_a_menu;
import static com.procuro.androidscm.Restaurant.DashBoard.DashboardActivity.sales_performace;
import static com.procuro.androidscm.Restaurant.DashBoard.DashboardActivity.speed_container;
import static com.procuro.androidscm.Restaurant.DashBoard.DashboardActivity.storeRecord;
import static com.procuro.androidscm.Restaurant.DashBoard.DashboardActivity.taste_container;

public class Dashboard_Onclicks {

    public static void InitializeOnclicks(final FragmentActivity fragmentActivity, final Context context){
        try {

            taste_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setupTasteTutorial(fragmentActivity);
                }
            });

            DashboardActivity.friendliness_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setupFriendlinessTutorial(fragmentActivity);
                }
            });

            DashboardActivity.speed_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setupSpeedTutorial(fragmentActivity);
                }
            });

            DashboardActivity.accuracy_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setupAccuracy(fragmentActivity);
                }
            });

            DashboardActivity.cleanliness_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setupCleanlinessTutorial(fragmentActivity);
                }
            });

            DashboardActivity.cleaningGuide.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(fragmentActivity, DashboardCleaningSliderActivity.class);
                    DisplayIntent(intent,fragmentActivity);
                }
            });


            DashboardActivity.communcationNotes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(fragmentActivity, Dashboard_CommunicationNotes.class);
                    DisplayIntent(intent,fragmentActivity);
                }
            });


            DashboardActivity.SafetyDataSheets.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(fragmentActivity, Dashboard_SafetyDataSheets.class);
                    DisplayIntent(intent,fragmentActivity);                }
            });


            DashboardActivity.Training.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(fragmentActivity, DashboardTraningWendysU_With_VideoPlayerActivity.class);
                    DisplayIntent(intent,fragmentActivity);
                }
            });


            DashboardActivity.VOC.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(fragmentActivity, Dashboard_VOC.class);
                    DisplayIntent(intent,fragmentActivity);
                }
            });

            DashboardActivity.staffing_dropdown.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(fragmentActivity, Dashboard_Staffing_Activity.class);
                    DisplayIntent(intent,fragmentActivity);
                }
            });

            DashboardActivity.sitename_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DisplaySiteListPopup(fragmentActivity);
                }
            });

            DashboardActivity.back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragmentActivity.finish();
                }
            });

            storeRecord.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(fragmentActivity, Dashboard_StoreRecord.class);
                    DisplayIntent(intent,fragmentActivity);
                }
            });

            customersatisfaction_Dropdown.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(fragmentActivity, Dashboard_CustomerSatifaction.class);
                    DisplayIntent(intent,fragmentActivity);
                }
            });

            sales_performace.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(fragmentActivity, Dashboard_SalesPerformance.class);
                    DisplayIntent(intent,fragmentActivity);
                }
            });

            CustomerExperience_Dropdown.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(fragmentActivity, Dashboard_CustomerExperience.class);
                    DisplayIntent(intent,fragmentActivity);
                }
            });

            VTa.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(fragmentActivity, Dashboard_Vtas_WITH_PDFVIER_Activity.class);
                    DisplayIntent(intent,fragmentActivity);
                }
            });

            Pimmsm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(fragmentActivity, Dashboard_Pimmsms.class);
                    DisplayIntent(intent,fragmentActivity);
                }
            });


            DashboardActivity.sos.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SOSData data = new SOSData();
                    data.setSite(SCMDataManager.getInstance().getSelectedSite().getSite());
                    SOSData.setInstance(data);
                    Intent intent = new Intent(fragmentActivity, SOSActivity.class);
                    DisplayIntent(intent,fragmentActivity);
                }
            });


            DashboardActivity.quiack_a_menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DisplayQuickAMenu(context,quiack_a_menu,fragmentActivity);
                }
            });

            DashboardActivity.pimmynews.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    QuickAccessMenu.DisplayPimmyNews(fragmentActivity);
                }
            });


        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void DisplayQuickAMenu(final Context context, View anchor, final FragmentActivity fragmentActivity) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.BOTTOM)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .transparentOverlay(false)
                .arrowColor(ContextCompat.getColor(context, R.color.black))
                .transparentOverlay(true)
                .contentView(R.layout.quick_access_menu)
                .focusable(true)
                .build();
        tooltip.show();

        ConstraintLayout calendar,dop,employee_sched,help,news,reports_portal,review_n_verify,skpe,sos,
                storeprofile,temp_check,user_profile,employee_list;

        calendar = tooltip.findViewById(R.id.calendar);
        dop = tooltip.findViewById(R.id.dashboard);
        employee_sched = tooltip.findViewById(R.id.employee_schedule);
        help = tooltip.findViewById(R.id.menu_help);
        news = tooltip.findViewById(R.id.menu_news);
        reports_portal = tooltip.findViewById(R.id.menu_reports_portal);
        review_n_verify = tooltip.findViewById(R.id.menu_review_n_verify);
        skpe = tooltip.findViewById(R.id.menu_skype);
        sos = tooltip.findViewById(R.id.menu_sos);
        storeprofile = tooltip.findViewById(R.id.menu_store_profile);
        temp_check = tooltip.findViewById(R.id.menu_temp_check);
        user_profile = tooltip.findViewById(R.id.menu_user_profile);
        employee_list = tooltip.findViewById(R.id.employee_list);

        //remove
        temp_check.setVisibility(View.GONE);
        dop.setVisibility(View.GONE);

        //disable
        reports_portal.setEnabled(false);
        reports_portal.setAlpha(.5f);

        employee_sched.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(fragmentActivity, Dashboard_Staffing_Activity.class);
                DisplayIntent(intent,fragmentActivity);
                tooltip.dismiss();
            }
        });


        user_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserProfileData data = new UserProfileData();
                data.setUserEditEnabled(false);
                data.setCustomSite(SCMDataManager.getInstance().getSelectedSite());
                CustomUser user = new CustomUser(SCMDataManager.getInstance().getUser());
                data.setCustomUser(user);
                data.setFromQuickAccess(true);
                data.setIsupdate(true);
                UserProfileData.setInstance(data);
                Intent intent = new Intent(fragmentActivity, UserProfileActivityOverAll.class);
                DisplayIntent(intent,fragmentActivity);
                tooltip.dismiss();
            }
        });

        dop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(fragmentActivity, DashboardActivity.class);
                DisplayIntent(intent,fragmentActivity);
                tooltip.dismiss();
            }
        });

        storeprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QualificationData data = new QualificationData();
                data.setSelectedSiteEnabled(true);
                data.setSelectedSite(SCMDataManager.getInstance().getSelectedSite());
                QualificationData.setInstance(data);

                Restaurant_home_page.setCustom(true);
                Restaurant_home_page.navView.setSelectedItemId(Restaurant_home_page.NAV_QUALIFICATIONS);

                fragmentActivity.finish();
                tooltip.dismiss();
            }
        });

        sos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SOSData data = new SOSData();
                data.setSite(SCMDataManager.getInstance().getSelectedSite().getSite());
                SOSData.setInstance(data);
                Intent intent = new Intent(fragmentActivity, SOSActivity.class);
                DisplayIntent(intent,fragmentActivity);
                tooltip.dismiss();
            }
        });

        calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(fragmentActivity, CalendarActivity.class);
                DisplayIntent(intent,fragmentActivity);
                tooltip.dismiss();
            }
        });

        news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuickAccessMenu.DisplayPimmyNews(fragmentActivity);
                tooltip.dismiss();

            }
        });

        skpe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuickAccessMenu.initiateSkypeUri(fragmentActivity,"");
                tooltip.dismiss();
            }
        });
        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuickAccessMenu.setUpGuideModule(fragmentActivity);
            }
        });

        employee_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QualificationData data = new QualificationData();
                data.setSelectedSiteEnabled(true);
                data.setSelectedSite(SCMDataManager.getInstance().getSelectedSite());
                data.setUsers(SCMDataManager.getInstance().getStoreUsers());
                QualificationData.setInstance(data);
                Intent intent = new Intent(fragmentActivity, EmployeeListActivity.class);
                DisplayIntent(intent,fragmentActivity);
                tooltip.dismiss();
            }
        });

    }


    public static void DisplayIntent(Intent intent,FragmentActivity fragmentActivity){
       fragmentActivity.startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(fragmentActivity).toBundle());
    }

    public static void setupTasteTutorial(FragmentActivity fragmentActivity){

        ArrayList<ResourcesGrandChildList> list =SCMDataManager.getInstance().getDashboardTaste();
        SortDocumentByname(list);

        ArrayList<TutorialList>tutorialLists = new ArrayList<>();

        for (ResourcesGrandChildList child : list){
            DocumentDTO documentDTO = child.getDocumentDTO();
            tutorialLists.add(new TutorialList(SCMTool.getFirstWord(documentDTO.name),SCMTool.RemoveFirstWord(documentDTO.name),documentDTO));
        }
        displayTutorialPopup(fragmentActivity,taste_container,"Taste",tutorialLists,fragmentActivity);
    }

    public static void setupFriendlinessTutorial(FragmentActivity fragmentActivity){

        ArrayList<ResourcesGrandChildList>list =SCMDataManager.getInstance().getDashboardFriendliness();

        SortDocumentByname(list);
        ArrayList<TutorialList>tutorialLists = new ArrayList<>();

        for (ResourcesGrandChildList child : list){
            DocumentDTO documentDTO = child.getDocumentDTO();
            tutorialLists.add(new TutorialList(SCMTool.getFirstWord(documentDTO.name),SCMTool.RemoveFirstWord(documentDTO.name),documentDTO));
        }

        displayTutorialPopup(fragmentActivity,friendliness_container,"Friendliness",tutorialLists,fragmentActivity);
    }

    public static void setupSpeedTutorial(FragmentActivity fragmentActivity){
        ArrayList<ResourcesGrandChildList>list =SCMDataManager.getInstance().getDashboardSpeed();

        SortDocumentByname(list);

        ArrayList<TutorialList>tutorialLists = new ArrayList<>();

        for (ResourcesGrandChildList child : list){
            DocumentDTO documentDTO = child.getDocumentDTO();
            tutorialLists.add(new TutorialList(SCMTool.getFirstWord(documentDTO.name),SCMTool.RemoveFirstWord(documentDTO.name),documentDTO));
        }

        displayTutorialPopup(fragmentActivity,speed_container,"Speed",tutorialLists,fragmentActivity);
    }

    public static void setupAccuracy(FragmentActivity fragmentActivity){
        ArrayList<ResourcesGrandChildList>list =SCMDataManager.getInstance().getDashboardAccuracy();

        SortDocumentByname(list);
        ArrayList<TutorialList>tutorialLists = new ArrayList<>();
        for (ResourcesGrandChildList child : list){
            DocumentDTO documentDTO = child.getDocumentDTO();
            tutorialLists.add(new TutorialList(SCMTool.getFirstWord(documentDTO.name),SCMTool.RemoveFirstWord(documentDTO.name),documentDTO));
        }
        displayTutorialPopup(fragmentActivity,accuracy_container,"Accuracy",tutorialLists,fragmentActivity);
    }

    public static void setupCleanlinessTutorial(FragmentActivity fragmentActivity){
        ArrayList<ResourcesGrandChildList>list =SCMDataManager.getInstance().getDashboardCleanliness();

        SortDocumentByname(list);

        ArrayList<TutorialList>tutorialLists = new ArrayList<>();
        for (ResourcesGrandChildList child : list){
            DocumentDTO documentDTO = child.getDocumentDTO();
            tutorialLists.add(new TutorialList(SCMTool.getFirstWord(documentDTO.name),SCMTool.RemoveFirstWord(documentDTO.name),documentDTO));
        }
        displayTutorialPopup(fragmentActivity,cleanliness_container,"Cleanliness",tutorialLists,fragmentActivity);
    }

    public static void displayTutorialPopup(final Context context, LinearLayout anchor,
                                            String title, ArrayList<TutorialList>tutorialLists,FragmentActivity fragmentActivity) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.TOP)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .transparentOverlay(false)
                .arrowColor(Color.WHITE)
                .contentView(R.layout.dashboard_tutorial_list_popup)
                .focusable(true)
                .overlayOffset(0)
                .highlightShape(OverlayView.HIGHLIGHT_SHAPE_RECTANGULAR)
                .build();
        tooltip.show();

        TextView Titles = tooltip.findViewById(R.id.title);
        ListView listView = tooltip.findViewById(R.id.listView);

        Titles.setText(title);

        Dashboard_cleaning_tutorial_listviewAdapter adapter = new Dashboard_cleaning_tutorial_listviewAdapter(context,tutorialLists,title,
                fragmentActivity);
        listView.setAdapter(adapter);

    }

    public static void DisplaySiteListPopup(FragmentActivity fragmentActivity){
        DialogFragment newFragment = Status_SiteList_DialogFragment.newInstance("Dashboard");
        assert fragmentActivity.getFragmentManager() != null;
        newFragment.show(fragmentActivity.getSupportFragmentManager(), "dialog");
    }

    public static void SortDocumentByname(ArrayList<ResourcesGrandChildList>arrayList){
        Collections.sort(arrayList, new Comparator<ResourcesGrandChildList>() {
            @Override
            public int compare(ResourcesGrandChildList o1, ResourcesGrandChildList o2) {
                return o1.getDocumentDTO().name.compareToIgnoreCase(o2.getDocumentDTO().name);
            }
        });
    }
}
