package com.procuro.androidscm.Restaurant.Qualification.UserProfile;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.GlideApp;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.Qualification.QualificationActivity;
import com.procuro.androidscm.Restaurant.Qualification.QualificationActivityFragment;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Area;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_District;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Division;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Regional;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.CorpUser;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class Qualification_UserProfile_Regional_Child_level_Fragment_2 extends Fragment {


    private CircleImageView profilePicture;
    public static TextView category,category_child_title,name,position,number,email,message;
    public static RecyclerView recyclerView;

    public static Corp_Regional regional;
    public static Corp_Division division;
    public static Corp_District district;
    public static Corp_Area area;
    public static CustomSite customSite;
    private Bundle backBundle;
    private Bundle frontBundle;
    public static ProgressDialog progressDialog;
    public static aPimmDataManager dataManager = aPimmDataManager.getInstance();


    public Qualification_UserProfile_Regional_Child_level_Fragment_2() {

        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.qualification_user_profile_region_new_ui, container, false);

        profilePicture = view.findViewById(R.id.circleImageView);
        category = view.findViewById(R.id.category);
        name = view.findViewById(R.id.name);
        position = view.findViewById(R.id.position);
        number = view.findViewById(R.id.phone);
        email = view.findViewById(R.id.email);
        recyclerView = view.findViewById(R.id.recyclerView);
        category_child_title = view.findViewById(R.id.category_child);
        message = view.findViewById(R.id.message);


        getarguments();

        return view;

    }

    private void getarguments(){

        if (QualificationData.getInstance().isOwnerShipViewEnabled()) {

            final Corp_Regional regional = QualificationData.getInstance().getRegional();
            if (regional!=null){
                if (regional.getCorpStructures().name.equalsIgnoreCase(QualificationData.getInstance().getSelectedItem())){
                    this.regional = regional;
                    QualificationActivityFragment.storeProfile.setEnabled(false);
                    QualificationActivityFragment.storeProfile.setAlpha(.5f);
                    QualificationActivityFragment.scoreCard.setEnabled(false);
                    QualificationActivityFragment.scoreCard.setAlpha(.5f);
                    QualificationActivityFragment.name.setText(this.regional.getCorpStructures().name);
                    DisplayRegionalView(this.regional);

                }else {

                    if (regional.getQualificationDivisions()!=null){
                        for (final Corp_Division division : regional.getQualificationDivisions()){
                            if (division.getCorpStructures().name.equalsIgnoreCase(QualificationData.getInstance().getSelectedItem())){
                                this.division = division;

                                QualificationActivityFragment.storeProfile.setEnabled(false);
                                QualificationActivityFragment.storeProfile.setAlpha(.5f);
                                QualificationActivityFragment.scoreCard.setEnabled(false);
                                QualificationActivityFragment. scoreCard.setAlpha(.5f);
                                QualificationActivityFragment.name.setText(this.division.getCorpStructures().name);

                                DisplayDivisionView(this.division);
                                break;

                            }else {
                                if (division.getQualificationAreas()!=null){

                                    for (final Corp_Area area : division.getQualificationAreas()){

                                        if (area.getCorpStructures().name.equalsIgnoreCase(QualificationData.getInstance().getSelectedItem())){
                                            this.area = area;
                                            QualificationActivityFragment.storeProfile.setEnabled(false);
                                            QualificationActivityFragment.storeProfile.setAlpha(.5f);
                                            QualificationActivityFragment.scoreCard.setEnabled(false);
                                            QualificationActivityFragment. scoreCard.setAlpha(.5f);
                                            QualificationActivityFragment. name.setText(this.area.getCorpStructures().name);

                                            DisplayAreaView(this.area);
                                            break;

                                        }else {

                                            if (area.getQualificationDistricts()!=null){

                                                for (final Corp_District district : area.getQualificationDistricts()){

                                                    if (district.getCorpStructures().name.equalsIgnoreCase(QualificationData.getInstance().getSelectedItem())){
                                                        this.district = district;
                                                        QualificationActivityFragment.storeProfile.setEnabled(false);
                                                        QualificationActivityFragment.storeProfile.setAlpha(.5f);
                                                        QualificationActivityFragment.scoreCard.setEnabled(false);
                                                        QualificationActivityFragment. scoreCard.setAlpha(.5f);
                                                        QualificationActivityFragment. name.setText(this.district.getCorpStructures().name);

                                                        DisplayDistrictView(this.district);
                                                        break;

                                                    }else {
                                                        if (district.getSites()!=null){

                                                            for (CustomSite customSite : district.getSites()){
                                                                if (customSite.getSite().sitename.equalsIgnoreCase(QualificationData.getInstance().getSelectedItem())){
                                                                    Qualification_UserProfile_Regional_Child_level_Fragment_2.customSite = customSite;
                                                                    QualificationActivityFragment.name.setText(this.customSite.getSite().sitename);
                                                                    QualificationActivityFragment. storeProfile.setEnabled(true);
                                                                    QualificationActivityFragment.storeProfile.setAlpha(1);
                                                                    QualificationActivityFragment.scoreCard.setEnabled(true);
                                                                    QualificationActivityFragment.scoreCard.setAlpha(1);

                                                                    DisplaySiteView(Qualification_UserProfile_Regional_Child_level_Fragment_2.customSite,getContext(),getActivity());

                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
        else if (QualificationData.getInstance().isStateByStateViewEnabled()){
            if(QualificationData.getInstance().getSelectedSite()!=null){
                customSite = QualificationData.getInstance().getSelectedSite();
                QualificationActivityFragment.name.setText(customSite.getSite().sitename);
                DisplaySiteView(Qualification_UserProfile_Regional_Child_level_Fragment_2.customSite,getContext(),getActivity());

            }
        }
        else {
            customSite = SCMDataManager.getInstance().getSelectedSite();
            QualificationActivityFragment.name.setText(customSite.getSite().sitename);
            DisplaySiteView(Qualification_UserProfile_Regional_Child_level_Fragment_2.customSite,getContext(),getActivity());

        }

    }

    private void DisplayRegionalView(Corp_Regional regional){

        disableStoreProfileAndScoreCard();
        try {
            category.setText(regional.getCorpStructures().name);
            category_child_title.setText("Division");
            CorpUser corpUser = regional.getCorpStructures().corpUser;

            if (corpUser!=null){

                name.setText(corpUser.firstName);
                name.append(" ");
                name.append(corpUser.lastName);

                if (corpUser.firstName.equalsIgnoreCase("Deepak")){
                    if (corpUser.lastName.equalsIgnoreCase("Ajmani")){
                        GlideApp.with(getContext()).asDrawable().load(R.drawable.deepak).into(profilePicture);
                        profilePicture.setPadding(SCMTool.dpToPx(2),SCMTool.dpToPx(2),SCMTool.dpToPx(2),SCMTool.dpToPx(2));
                    }
                }
                position.setText(corpUser.jobTitle);

                if (corpUser.phoneNumber!=null){

                    if (!corpUser.phoneNumber.equalsIgnoreCase("null")){
                        number.setText(corpUser.phoneNumber);
                    }
                }

                if (corpUser.userName!=null){
                    if (!corpUser.userName.equalsIgnoreCase("null")){
                        email.setText(corpUser.userName);
                    }
                }

            }else {
                name.setText("Name : Not Available");
                position.setText("Position: Not Available");
            }

            Qualification_DIvision_RecyclerViewAdapter adapter = new Qualification_DIvision_RecyclerViewAdapter(getContext(),regional.getQualificationDivisions(),getActivity());
            recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
            recyclerView.setAdapter(adapter);
            message.setVisibility(View.GONE);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void DisplayDivisionView(Corp_Division division){

        disableStoreProfileAndScoreCard();

        try {
            category.setText(division.getCorpStructures().name);
            CorpUser corpUser = division.getCorpStructures().corpUser;

            if (corpUser!=null){

                name.setText(corpUser.firstName);
                name.append(" ");
                name.append(corpUser.lastName);

                position.setText(corpUser.jobTitle);

                if (corpUser.phoneNumber!=null){
                    if (!corpUser.phoneNumber.equalsIgnoreCase("null")){
                        number.setText(corpUser.phoneNumber);
                    }
                }
                if (corpUser.userName!=null){
                    if (!corpUser.userName.equalsIgnoreCase("null")){
                        email.setText(corpUser.userName);
                    }
                }
            }else {
                name.setText("Name : Not Available");
                position.setText("Position: Not Available");
            }

            category_child_title.setText("Area");
            Qualification_Area_RecyclerViewAdapter adapter = new Qualification_Area_RecyclerViewAdapter(getContext(),
                    division.getQualificationAreas(),getActivity());
            recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
            recyclerView.setAdapter(adapter);
            message.setVisibility(View.GONE);
        }catch (Exception e){
            e.printStackTrace();
        }



    }

    private void DisplayAreaView(Corp_Area area){

        disableStoreProfileAndScoreCard();

        try {
            category.setText(area.getCorpStructures().name);
            CorpUser corpUser = area.getCorpStructures().corpUser;

            if (corpUser!=null){

                name.setText(corpUser.firstName);
                name.append(" ");
                name.append(corpUser.lastName);

                position.setText(corpUser.jobTitle);

                if (corpUser.phoneNumber!=null){
                    if (!corpUser.phoneNumber.equalsIgnoreCase("null")){
                        number.setText(corpUser.phoneNumber);
                    }
                }

                if (corpUser.userName!=null){
                    if (!corpUser.userName.equalsIgnoreCase("null")){
                        email.setText(corpUser.userName);
                    }
                }

            }else {
                name.setText("Name : Not Available");
                position.setText("Position: Not Available");
            }

            category_child_title.setText("District");
            Qualification_District_RecyclerViewAdapter adapter = new Qualification_District_RecyclerViewAdapter(
                    getContext(),area.getQualificationDistricts(),getActivity(),frontBundle);
            recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
            recyclerView.setAdapter(adapter);
            message.setVisibility(View.GONE);

        }catch (Exception e){
            e.printStackTrace();
        }



    }

    private void DisplayDistrictView(Corp_District district){
        disableStoreProfileAndScoreCard();
        try {
            category.setText(district.getCorpStructures().name);
            CorpUser corpUser = district.getCorpStructures().corpUser;

            if (corpUser!=null){

                name.setText(corpUser.firstName);
                name.append(" ");
                name.append(corpUser.lastName);

                position.setText(corpUser.jobTitle);

                if (corpUser.phoneNumber!=null){
                    if (!corpUser.phoneNumber.equalsIgnoreCase("null")){
                        number.setText(corpUser.phoneNumber);
                    }
                }

                if (corpUser.userName!=null){
                    if (!corpUser.userName.equalsIgnoreCase("null")){
                        email.setText(corpUser.userName);
                    }
                }

            }else {
                name.setText("Name : Not Available");
                position.setText("Position: Not Available");
            }

            category_child_title.setText("Store");

            Qualification_CustomSite_RecyclerViewAdapter adapter = new Qualification_CustomSite_RecyclerViewAdapter(
                    getContext(),district.getSites(),getActivity(),frontBundle);
            recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
            recyclerView.setAdapter(adapter);
            message.setVisibility(View.GONE);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void DisplaySiteView(CustomSite customSite,Context context,FragmentActivity activity){
        try {

            enableStoreProfileAndScoreCard();
            category.setText(customSite.getSite().sitename);

            if (QualificationData.getInstance().getUsers()==null){
                getStoreUsers(context,activity);

            }else {
                FilterEmployees(QualificationData.getInstance().getUsers(),context,activity);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public static void getStoreUsers(final Context context, final FragmentActivity activity){
        QualificationData.getInstance().setSelectedSite(customSite);
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Please Wait");
        progressDialog.setMessage("Downloading Data");
        progressDialog.setCancelable(false);
        progressDialog.show();

        dataManager = aPimmDataManager.getInstance();
        dataManager.getStoreUsers(customSite.getSite().siteid, new OnCompleteListeners.getStoreUsersCallbackListener() {
            @Override
            public void getStoreUsersCallback(ArrayList<User> storeUsers, Error error) {
                if (error == null){
                    QualificationData.getInstance().setUsers(SCMTool.ConvertUserToCustomUser(storeUsers));
                    FilterEmployees(QualificationData.getInstance().getUsers(),context,activity);
                    message.setVisibility(View.GONE);
                    progressDialog.cancel();
                }else {
                    progressDialog.cancel();
                    name.setText("Name : Not Available");
                    position.setText("Position : Not Available");
                    category_child_title.setText("No Employee Available");
                }
            }
        });
    }


    public static void FilterEmployees(ArrayList<CustomUser>users, Context context, FragmentActivity activity){
        try {
            ArrayList<CustomUser> ActiveUsers = SCMTool.getActiveCustomUser(users);

            CustomUser manager = null;
            ArrayList<CustomUser>employees = new ArrayList<>();
            if (ActiveUsers.size()>0){
                category_child_title.setText(ActiveUsers.size()+ " Employees");
                for (CustomUser user : ActiveUsers){

                    System.out.println("User : "+user.getUser().firstName);
                    boolean managerExist = false;

                    if (user.getUser().roles!=null){
                        System.out.println("Role : "+user.getUser().roles);
                        for (String role : user.getUser().roles){
                            if (role.toLowerCase().contains("gm")){
                                manager = user;
                                managerExist = true;
                            }else if (role.toLowerCase().contains("manager")){
                                manager = user;
                                managerExist = true;
                            }else if (role.toLowerCase().contains("general manager")){
                                manager = user;
                                managerExist = true;
                            }
                        }
                        if (!managerExist){
                            employees.add(user);
                        }
                    }
                }
                if (manager!=null){
                    name.setText(manager.getUser().firstName);
                    name.append(" ");
                    name.append(manager.getUser().lastName);

                    position.setText("General Manager");
                    if (manager.getUser().cellPhone!=null){
                        if (!manager.getUser().cellPhone.equalsIgnoreCase("null")){
                            number.setText(manager.getUser().cellPhone);
                        }else {
                            number.setText("Not Available");
                        }

                    }
                    if (manager.getUser().contactEmail!=null){
                        if (!manager.getUser().contactEmail.equalsIgnoreCase("null")){
                            email.setText(manager.getUser().contactEmail);
                        }else {
                            email.setText("Not Available");
                        }

                    }
                }else {
                    name.setText("Name : Not Available");
                    position.setText("Position : Not Available");
                }

                Qualification_User_RecyclerViewAdapter adapter = new Qualification_User_RecyclerViewAdapter(context,employees,activity,customSite);
                recyclerView.setLayoutManager(new GridLayoutManager(context,2));
                recyclerView.setAdapter(adapter);
                message.setVisibility(View.GONE);

            }else {
                name.setText("Name : Not Available");
                position.setText("Position : Not Available");
                category_child_title.setText("No Employee Available");
                message.setVisibility(View.VISIBLE);
            }
        }catch (Exception e){
         e.printStackTrace();
        }
    }

    private void disableStoreProfileAndScoreCard(){
        try {
            QualificationActivityFragment.storeProfile.setAlpha(.5f);
            QualificationActivityFragment.storeProfile.setEnabled(false);
            QualificationActivityFragment.scoreCard.setAlpha(.5f);
            QualificationActivityFragment.scoreCard.setEnabled(false);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void enableStoreProfileAndScoreCard(){
        try {
            QualificationActivityFragment.storeProfile.setAlpha(1);
            QualificationActivityFragment.storeProfile.setEnabled(true);
            QualificationActivityFragment.scoreCard.setAlpha(1);
            QualificationActivityFragment.scoreCard.setEnabled(true);

        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
