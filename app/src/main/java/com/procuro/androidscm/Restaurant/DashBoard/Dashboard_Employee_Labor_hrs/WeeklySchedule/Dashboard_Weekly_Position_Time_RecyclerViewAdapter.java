package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.WeeklySchedule;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.apimmdatamanagerlib.DashboardKitchenData;
import com.procuro.apimmdatamanagerlib.PositionDataItems;
import com.procuro.apimmdatamanagerlib.SMSPositioning;
import com.procuro.apimmdatamanagerlib.SMSPositioningEmployeeScheduleAssignment;
import com.procuro.apimmdatamanagerlib.SMSPositioningEmployeeScheduleAssignmentPositions;
import com.procuro.apimmdatamanagerlib.Schedule_Business_Site_Plan;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;


public class Dashboard_Weekly_Position_Time_RecyclerViewAdapter extends RecyclerView.Adapter<Dashboard_Weekly_Position_Time_RecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<Date> arraylist;
    private CustomUser customUser;

    public Dashboard_Weekly_Position_Time_RecyclerViewAdapter(Context context, ArrayList<Date> arraylist, CustomUser customUser) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;
        this.customUser = customUser;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.weekly_sched_time_list_data,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {

        final Date selectedDate = arraylist.get(position);

        DisplayPositionCovered(customUser,holder.start_time,holder.end_time,selectedDate,holder.position);
    }

    private void DisplayPositionCovered(CustomUser user, TextView startTime, TextView endTIme,Date seletedDate,TextView position){
        try {
            SimpleDateFormat timeformat = new SimpleDateFormat("hh:mm");
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM/dd/yy");
            timeformat.setTimeZone(TimeZone.getTimeZone("UTC"));
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            if (user.getSchedules()!=null){
                if (user.getSchedules().size()>0){

                    Collections.sort(user.getSchedules(), new Comparator<Schedule_Business_Site_Plan>() {
                        @Override
                        public int compare(Schedule_Business_Site_Plan o1, Schedule_Business_Site_Plan o2) {
                            return o1.startTime.compareTo(o2.startTime);
                        }
                    });

                    for (Schedule_Business_Site_Plan sched : user.getSchedules()){
                        if (dateFormat.format(sched.date).equalsIgnoreCase(dateFormat.format(seletedDate))){

                            if (sched.startTime!=null){
                                startTime.setText(timeformat.format(sched.startTime));
                            }

                            if (sched.endTime!=null){
                                endTIme.setText(timeformat.format(sched.endTime));
                            }

                            position.setText(String.valueOf(sched.position));

                            findPositonColorInKitchenLayout(position,sched.position);

                            break;
                        }
                    }
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return arraylist.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView start_time , end_time, position;


        public MyViewHolder(View itemView) {
            super(itemView);
            start_time = itemView.findViewById(R.id.start_time);
            end_time = itemView.findViewById(R.id.end_time);
            position = itemView.findViewById(R.id.position);

        }
    }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    private TextView  CreatePositionBackground(TextView imageView, String backgroundColor) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setCornerRadii(new float[] { dpToPx(5), dpToPx(5), dpToPx(5), dpToPx(5), dpToPx(5), dpToPx(5), dpToPx(5), dpToPx(5) });
        shape.setColor(Color.parseColor(backgroundColor));
        shape.setStroke(1, Color.BLACK);
        imageView.setBackground(shape);
        return imageView;
    }


    private void findPositonColorInKitchenLayout(TextView holder,int position){
        try {
            DashboardKitchenData dashboardKitchenData = SCMDataManager.getInstance().getDashboardKitchenData();
            ArrayList<PositionDataItems> positionDataItems = dashboardKitchenData.positionDataItems;
            if (positionDataItems!=null){

                for (PositionDataItems dataItems : positionDataItems){
                    if (position == dataItems.position){
                        CreatePositionBackground(holder,"#"+dataItems.color);
                        break;
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }


}
