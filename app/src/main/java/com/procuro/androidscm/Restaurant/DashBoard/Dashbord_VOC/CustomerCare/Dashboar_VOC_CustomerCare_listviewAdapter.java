package com.procuro.androidscm.Restaurant.DashBoard.Dashbord_VOC.CustomerCare;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;


import com.procuro.androidscm.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;


public class Dashboar_VOC_CustomerCare_listviewAdapter extends BaseAdapter {

    // Declare Variables
    Context mContext;
    LayoutInflater inflater;
    private ArrayList<CustomerCareListData> arraylist;
    FragmentActivity fragmentActivity;

    public Dashboar_VOC_CustomerCare_listviewAdapter(Context context, ArrayList<CustomerCareListData> arraylist, FragmentActivity fragmentActivity) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;
        this.fragmentActivity = fragmentActivity;
    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return arraylist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.dashboard_voc_customer_care_list_data, null);
            view.setTag(holder);
            final CustomerCareListData customerCareListData = arraylist.get(position);

            TextView caseNumber = view.findViewById(R.id.caseNumber);
            TextView caseType = view.findViewById(R.id.caseType);
            TextView category = view.findViewById(R.id.category);
            TextView categoryType = view.findViewById(R.id.categoryType);
            TextView dateTime = view.findViewById(R.id.dateTime);
            TextView picture = view.findViewById(R.id.picture);
            TextView status = view.findViewById(R.id.status);
            LinearLayout divider = view.findViewById(R.id.divider);

            SimpleDateFormat format = new SimpleDateFormat("M/dd/yyyy h:mm a");

            caseNumber.setText(String.valueOf(customerCareListData.getCaseNumber()));
            caseType.setText(customerCareListData.getCaseType());
            category.setText(customerCareListData.getCategory());
            categoryType.setText(customerCareListData.getCategoryType());
            dateTime.setText(format.format(customerCareListData.getDate()));
            status.setText(customerCareListData.getStatus());

            if (position == arraylist.size()-1){
                divider.setVisibility(View.GONE);
            }else {
                divider.setVisibility(View.VISIBLE);
            }



        return view;
    }


}

