package com.procuro.androidscm.Restaurant.ReportsFragment;

import java.util.ArrayList;
import java.util.Date;

public class ReportsDateChildList {

    private String title;
    private Date startdate;
    private Date enddate;
    private int weekofyear;
    private ArrayList<ReportsDateGrandChildList>dailyArrayList;

    private boolean selected;
    private boolean enabled;


    public ReportsDateChildList() {

    }


    public ReportsDateChildList(String title, Date startdate, Date enddate, int weekofyear, ArrayList<ReportsDateGrandChildList> dailyArrayList, boolean selected) {
        this.title = title;
        this.startdate = startdate;
        this.enddate = enddate;
        this.weekofyear = weekofyear;
        this.dailyArrayList = dailyArrayList;
        this.selected = selected;
    }

    public ReportsDateChildList(String title, Date startdate, Date enddate, int weekofyear, ArrayList<ReportsDateGrandChildList> dailyArrayList, boolean selected, boolean enabled) {
        this.title = title;
        this.startdate = startdate;
        this.enddate = enddate;
        this.weekofyear = weekofyear;
        this.dailyArrayList = dailyArrayList;
        this.selected = selected;
        this.enabled = enabled;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public ArrayList<ReportsDateGrandChildList> getDailyArrayList() {
        return dailyArrayList;
    }

    public void setDailyArrayList(ArrayList<ReportsDateGrandChildList> dailyArrayList) {
        this.dailyArrayList = dailyArrayList;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public int getWeekofyear() {
        return weekofyear;
    }

    public void setWeekofyear(int weekofyear) {
        this.weekofyear = weekofyear;
    }
}
