package com.procuro.androidscm.Restaurant.DashBoard;

import java.util.ArrayList;

public class SkillsAndPosition {
    String title;
    int icon;
    ArrayList<Employee>employees;
    ArrayList<SkillsAndPositionDescription>workDescription;

    public SkillsAndPosition(String title, int icon, ArrayList<Employee> employees, ArrayList<SkillsAndPositionDescription> workDescription) {
        this.title = title;
        this.icon = icon;
        this.employees = employees;
        this.workDescription = workDescription;
    }

    public SkillsAndPosition(String title, int icon, ArrayList<SkillsAndPositionDescription> workDescription) {
        this.title = title;
        this.icon = icon;
        this.workDescription = workDescription;
    }



    public SkillsAndPosition(String title, int icon) {
        this.title = title;
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public ArrayList<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(ArrayList<Employee> employees) {
        this.employees = employees;
    }

    public ArrayList<SkillsAndPositionDescription> getWorkDescription() {
        return workDescription;
    }

    public void setWorkDescription(ArrayList<SkillsAndPositionDescription> workDescription) {
        this.workDescription = workDescription;
    }
}
