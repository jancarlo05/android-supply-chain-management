package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_SalePerformance;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.SiteSales;
import com.procuro.apimmdatamanagerlib.SiteSalesActual;
import com.procuro.apimmdatamanagerlib.SiteSalesData;
import com.procuro.apimmdatamanagerlib.SiteSalesDaypart;
import com.procuro.apimmdatamanagerlib.SiteSalesForecast;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;


public class Dashboard_SalesPerfornace_Daypart_item_RecyclerViewAdapter extends RecyclerView.Adapter<Dashboard_SalesPerfornace_Daypart_item_RecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<String> items;
    private ArrayList<SalesPerformanceDaypartHeader>header;
    private SiteSales siteSales;
    private ArrayList<Double>forcasts;
    private ArrayList<Double>sales;
    private static DecimalFormat decimalFormat = new DecimalFormat("0.00");
    private double totalForcast;
    private String daypartSelected;
    private boolean isFromDefault;



    public Dashboard_SalesPerfornace_Daypart_item_RecyclerViewAdapter(Context context,
                                                                      ArrayList<String> items,
                                                                      ArrayList<SalesPerformanceDaypartHeader>header,
                                                                      SiteSales siteSales,String daypartSelected) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.items = items;
        this.header = header;
        this.siteSales = siteSales;
        this.forcasts = new ArrayList<>();
        this.sales = new ArrayList<>();
        totalForcast = getForcast(siteSales);
        this.daypartSelected = daypartSelected;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.daily_labor_cardview,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final String string = items.get(position);

        holder.name.setText(string);

        ArrayList<String>items = new ArrayList<>();

        if (string.equalsIgnoreCase("Forecast")){
            getForcastPerDaypart(siteSales,items,daypartSelected);
        }
        else if (string.equalsIgnoreCase("Hourly Sales")){
            getSalesPerDaypart(siteSales,items);
        }
        else if (string.equalsIgnoreCase("(+/-)")){
            ComputeDifference(items);
        }

        Dashboard_SalesPerformance_Week_Child_item_RecyclerViewAdapter adapter = new Dashboard_SalesPerformance_Week_Child_item_RecyclerViewAdapter(mContext,items,string,header.size());
        holder.recyclerView.setLayoutManager(new GridLayoutManager(mContext,header.size()));
        holder.recyclerView.setAdapter(adapter);

        if (position % 2 == 1) {
            holder.cardView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.light_orange2));
        } else {
            holder.cardView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.white));
        }
    }



    private void getForcastPerDaypart(SiteSales siteSales,ArrayList<String>items,String daypartName){

         if (siteSales.data!=null){

            SiteSalesData data = siteSales.data;

            if (data.dayparts!=null){

                for (SiteSalesDaypart daypart : data.dayparts){

                    if (daypart.name.equalsIgnoreCase(daypartName)){

                        for (SalesPerformanceDaypartHeader daypartHeader : header){

                            BigDecimal percentage = new BigDecimal("0");

                            for (SiteSalesForecast forecast : daypart.forecast){

                                if (daypart.name.equalsIgnoreCase("1")|| daypart.name.equalsIgnoreCase("2")){

                                    if (daypartHeader.getName().equalsIgnoreCase("10-11")){
                                        percentage = getDaypartTransitionPercentage(data.dayparts,data.isFromDefault,daypartHeader);
                                    }else {
                                        if (data.isFromDefault){
                                            if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.startTime)){
                                                if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.endTime)){
                                                    percentage = percentage.add(forecast.percentage);
                                                }
                                            }
                                        }else {
                                            if (isInsideDaypart(daypartHeader,forecast)){
                                                percentage = percentage.add(forecast.percentage);
                                            }
                                        }
                                    }
                                }
                                else {
                                    if (data.isFromDefault){
                                        if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.startTime)){
                                            if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.endTime)){
                                                percentage = percentage.add(forecast.percentage);
                                            }
                                        }
                                    }else {
                                        if (isInsideDaypart(daypartHeader,forecast)){
                                            percentage = percentage.add(forecast.percentage);
                                        }
                                    }
                                }
                            }

                            BigDecimal FORCASTRESULT = new BigDecimal(totalForcast).multiply(percentage);
                            items.add("$"+decimalFormat.format(FORCASTRESULT.doubleValue()));
                            forcasts.add(FORCASTRESULT.doubleValue());
                        }
                        break;
                    }
                }
            }else {
                for (int i = 0; i <header.size() ; i++) {
                    items.add("$0.00");
                    forcasts.add(0.0);
                }
            }
        }else {
             for (int i = 0; i <header.size(); i++) {
                 items.add("$0.00");
                 forcasts.add(0.0);
             }
         }
    }

    private void getSalesPerDaypart(SiteSales siteSales,ArrayList<String>items){
        SimpleDateFormat format = new SimpleDateFormat("H:mm");

        if (siteSales.data!=null){
            SiteSalesData data = siteSales.data;
            if (data.dayparts!=null && data.sales!=null){
                for (SalesPerformanceDaypartHeader daypartHeader : header){
                    double amount = 0.00;
                    for (SiteSalesActual actual : data.sales){
                        if (daypartHeader.getStart().equalsIgnoreCase(format.format(actual.startTime))){
                            if (daypartHeader.getEnd().equalsIgnoreCase(format.format(actual.endTime))){
                                amount += actual.amount;

                            }
                        }
                    }
                    sales.add(amount);
                    items.add("$"+decimalFormat.format(amount));
                }

            }else {
                for (int i = 0; i <header.size() ; i++) {
                    items.add   ("$0.00");
                    sales.add(0.0);
                }
            }
        }else {
            for (int i = 0; i <header.size() ; i++) {
                items.add("$0.00");
                sales.add(0.0);
            }
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        RecyclerView recyclerView;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
             name = itemView.findViewById(R.id.name);
             recyclerView = itemView.findViewById(R.id.recyclerView);
             cardView = itemView.findViewById(R.id.cardview_id);

        }
    }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    private boolean isInsideDaypart(SalesPerformanceDaypartHeader daypartHeader,SiteSalesForecast forecast){
        SimpleDateFormat format = new SimpleDateFormat("H:mm");
        String[] header = daypartHeader.getStart().split(":");
        String headerHr = header[0];
        String headerMinute = header[1];
        String header30Minutes = "30";

        String[] forecastName = format.format(forecast.startTime).split(":");
        String forecastHr = forecastName[0];
        String foreMinute = forecastName[1];

        if (headerHr.equalsIgnoreCase(forecastHr)){
            return foreMinute.equalsIgnoreCase(header30Minutes) || foreMinute.equalsIgnoreCase(headerMinute);
        }

        return false;
    }

    private double getForcast(SiteSales siteSales){
        try {
            if (siteSales.data!=null){
                SiteSalesData data = siteSales.data;
                double forcast =0;
                if (data.dss!=0) {
                    forcast = Double.parseDouble(String.valueOf(data.dss));
                }
                else if (data.projected!=0){
                    forcast = Double.parseDouble(String.valueOf(data.projected));
                }
                return forcast;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    private void ComputeDifference(ArrayList<String>items){
        double difference = 0;
        for (int i = 0; i <forcasts.size() ; i++) {
            double forecast = forcasts.get(i);
            double sale =sales.get(i);

            difference = Math.round((sale - forecast) * 100.00) / 100.00;

            if (difference<0){
                difference = difference * -1;
                items.add("-$"+decimalFormat.format(difference));
            }else {
                items.add("$"+decimalFormat.format(difference));
            }
        }
    }
    private BigDecimal getDaypartTransitionPercentage(ArrayList<SiteSalesDaypart>dayparts,boolean isFromDefault,SalesPerformanceDaypartHeader daypartHeader ){

        BigDecimal percentage = new BigDecimal("0");
        SimpleDateFormat format = new SimpleDateFormat("H:mm");
        for (SiteSalesDaypart daypart : dayparts){
            if (daypart.name.equalsIgnoreCase("1")){
                for (SiteSalesForecast forecast : daypart.forecast){
                    if (!isFromDefault){
                        if (format.format(forecast.startTime).equalsIgnoreCase("10:00")){
                            percentage = percentage.add(forecast.percentage);
                            break;
                        }
                    }else {
                        if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.startTime)){
                            if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.endTime)){
                                percentage = percentage.add(forecast.percentage);
                                break;
                            }
                        }
                    }

                }
            }else if (daypart.name.equalsIgnoreCase("2")){
                for (SiteSalesForecast forecast : daypart.forecast){
                    if (!isFromDefault){
                        if (format.format(forecast.startTime).equalsIgnoreCase("10:30")){
                            percentage = percentage.add(forecast.percentage);
                            break;
                        }
                    }else {
                        if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.startTime)){
                            if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.endTime)){
                                percentage = percentage.add(forecast.percentage);
                                break;
                            }
                        }
                    }
                }
            }
        }
        System.out.println("getDaypartTransitionPercentage : "+percentage);
        return percentage;
    }

}
