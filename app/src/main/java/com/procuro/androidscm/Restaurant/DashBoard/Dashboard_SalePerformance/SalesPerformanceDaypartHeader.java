package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_SalePerformance;

import java.util.Date;

public class SalesPerformanceDaypartHeader {

    private String name;
    private boolean isTransition;
    private String start;
    private String end ;
    private Date startDate;
    private Date endDate;

    public SalesPerformanceDaypartHeader(String name, boolean isTransition, String start, String end, Date startDate, Date endDate) {
        this.name = name;
        this.isTransition = isTransition;
        this.start = start;
        this.end = end;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public SalesPerformanceDaypartHeader(String name, boolean isTransition, String start, String end) {
        this.name = name;
        this.isTransition = isTransition;
        this.start = start;
        this.end = end;
    }

    public SalesPerformanceDaypartHeader(String name, String start, String end) {
        this.name = name;
        this.start = start;
        this.end = end;
    }

    public SalesPerformanceDaypartHeader(String name, String start, String end, Date startDate, Date endDate) {
        this.name = name;
        this.start = start;
        this.end = end;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public SalesPerformanceDaypartHeader(String name) {
        this.name = name;
    }

    public SalesPerformanceDaypartHeader(String name, boolean isTransition) {
        this.name = name;
        this.isTransition = isTransition;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isTransition() {
        return isTransition;
    }

    public void setTransition(boolean transition) {
        isTransition = transition;
    }
}
