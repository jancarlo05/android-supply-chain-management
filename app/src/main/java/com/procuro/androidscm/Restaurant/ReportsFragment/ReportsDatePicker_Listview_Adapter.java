package com.procuro.androidscm.Restaurant.ReportsFragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_PdfView_DialogFragment;
import com.procuro.androidscm.Restaurant.DashBoard.TutorialList;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.DocumentDTO;
import com.procuro.apimmdatamanagerlib.Site;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;


public class ReportsDatePicker_Listview_Adapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private FragmentActivity fragmentActivity;
    private ArrayList<ReportsDatePickerData>arraylist;
    private Site site = SCMDataManager.getInstance().getSelectedSite().getSite();

    public ReportsDatePicker_Listview_Adapter(Context context,  ArrayList<ReportsDatePickerData>arraylist) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;


    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return arraylist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        holder = new ViewHolder();
        view = inflater.inflate(R.layout.reports_date_picker_list_data, null);
        view.setTag(holder);

        final ReportsDatePickerData pickerData = arraylist.get(position);

        final TextView name = view.findViewById(R.id.name);
        final TextView date = view.findViewById(R.id.date);
        ImageView indicator = view.findViewById(R.id.indicator);
        final DatePicker datePicker = view.findViewById(R.id.datepicker);
        ConstraintLayout title_container = view.findViewById(R.id.title_container);

        final SimpleDateFormat format = new SimpleDateFormat("MMM d, yyyy");

        name.setText(pickerData.getDescription());
        date.setText(format.format(pickerData.getDate()));

        if (pickerData.getDescription().equalsIgnoreCase("start")){
            Calendar calendar = SCMTool.getDateWithDaypart(new Date().getTime(),site.effectiveUTCOffset);
            datePicker.setMaxDate(calendar.getTime().getTime());

        }else {
            Calendar calendar =  SCMTool.getDateWithDaypart(new Date().getTime(),site.effectiveUTCOffset);
            calendar.add(Calendar.DATE,1);
            datePicker.setMinDate(calendar.getTime().getTime());
        }

        final Calendar cal = new GregorianCalendar();
        cal.setTime(pickerData.getDate());
        cal.setTimeZone(TimeZone.getTimeZone("UTC"));


        final int[] xxday = {cal.get(Calendar.DATE)};
        final int[] xxmonth = {cal.get(Calendar.MONTH)};
        final int[] xxyear = {cal.get(Calendar.YEAR)};

        datePicker.init(xxyear[0], xxmonth[0], xxday[0], new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                cal.set(year,monthOfYear,dayOfMonth);;
                cal.clear(Calendar.HOUR);
                cal.clear(Calendar.MINUTE);

                pickerData.setDate(cal.getTime());
                date.setText(format.format(pickerData.getDate()));
            }
        });

        if (pickerData.isSelected()){
            date.setTextColor(ContextCompat.getColor(mContext,R.color.red));
            indicator.setRotation(180);
            datePicker.setVisibility(View.VISIBLE);
        }else {
            date.setTextColor(ContextCompat.getColor(mContext,R.color.black));
            indicator.setRotation(270);
            datePicker.setVisibility(View.GONE);
        }

        title_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (ReportsDatePickerData data : arraylist){
                    data.setSelected(false);
                }
                pickerData.setSelected(true);
                notifyDataSetChanged();
            }
        });

        return view;
    }


    private void DisplaySiteListPopup(DocumentDTO dto){
        DialogFragment newFragment = Dashboard_PdfView_DialogFragment.newInstance(dto);
        assert fragmentActivity.getFragmentManager() != null;
        newFragment.show(fragmentActivity.getSupportFragmentManager(), "dialog");
    }

}

