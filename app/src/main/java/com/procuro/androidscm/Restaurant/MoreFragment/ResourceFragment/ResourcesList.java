package com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment;

import java.util.ArrayList;

public class ResourcesList {

    String title;
    String Category;
    ArrayList<ResourcesChildList>childLists;

    public ResourcesList(String category) {
        Category = category;
    }

    public ResourcesList(String title, ArrayList<ResourcesChildList> childLists) {
        this.title = title;
        this.childLists = childLists;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<ResourcesChildList> getChildLists() {
        return childLists;
    }

    public void setChildLists(ArrayList<ResourcesChildList> childLists) {
        this.childLists = childLists;
    }
}
