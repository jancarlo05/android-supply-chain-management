package com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourceTrainingVideos;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourcesGrandChildList;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.Resources_left_pane_fragment;
import com.procuro.androidscm.Restaurant.QuickAccessMenu.QuickAccessMenu;


public class Resource_Video_PlayerFragment extends Fragment  {

    VideoEnabledWebView mWebView;
    ProgressBar progressBar;

    private Button back,quick_a_menu,Covid;

    ResourcesGrandChildList grandChildList;
    public Resource_Video_PlayerFragment(ResourcesGrandChildList grandChildList) {
        this.grandChildList = grandChildList;
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.resource_video_player, container, false);
        TextView title = rootView.findViewById(R.id.username);

        back = rootView.findViewById(R.id.home);
        title.setText(grandChildList.getDocumentDTO().name);
        quick_a_menu = rootView.findViewById(R.id.quick_a_menu);
        Covid = rootView.findViewById(R.id.covid);


        mWebView = (VideoEnabledWebView)rootView.findViewById(R.id.webView);
        progressBar = rootView.findViewById(R.id.progressbar);
        progressBar.setVisibility(View.VISIBLE);
        mWebView.setWebViewClient(new Browser_home());
        mWebView.setWebChromeClient(new MyChrome());
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAllowFileAccess(true);
        webSettings.setAppCacheEnabled(true);

        setupOnclicks();

        loadWebsite();

        return rootView;
    }

    private void setupOnclicks(){
        Covid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuickAccessMenu.DisplayCovidFragment(getActivity());
            }
        });

        quick_a_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuickAccessMenu.DisplayQuickAMenu(getContext(),quick_a_menu,getActivity());
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Back();
            }
        });
    }


    private void Back() {
        getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_right,R.anim.slide_right_out)
                .replace(R.id.resturant_fragment_container,
                        new Resources_left_pane_fragment(R.id.training_videos)).commit();
    }

    private class mywebviewclient extends WebViewClient {
        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);

            System.out.println("Error: "+error.getDescription());
            System.out.println(error.getErrorCode());
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.INVISIBLE);

        }
    }

    private void loadWebsite() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getApplication().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            mWebView.loadUrl(grandChildList.getDocumentDTO().url);
        } else {
            mWebView.setVisibility(View.GONE);
        }
    }

    class Browser_home extends WebViewClient {

        Browser_home() {
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            progressBar.setVisibility(View.GONE);
            super.onPageFinished(view, url);

        }
    }

    private class MyChrome extends WebChromeClient {

        private View mCustomView;
        private WebChromeClient.CustomViewCallback mCustomViewCallback;
        protected FrameLayout mFullscreenContainer;
        private int mOriginalOrientation;
        private int mOriginalSystemUiVisibility;

        MyChrome() {}

        public Bitmap getDefaultVideoPoster()
        {
            if (mCustomView == null) {
                return null;
            }
            return BitmapFactory.decodeResource(getContext().getResources(), 2130837573);
        }

        public void onHideCustomView()
        {
            ((FrameLayout)getActivity().getWindow().getDecorView()).removeView(this.mCustomView);
            this.mCustomView = null;
            getActivity().getWindow().getDecorView().setSystemUiVisibility(this.mOriginalSystemUiVisibility);
            getActivity().setRequestedOrientation(this.mOriginalOrientation);
            this.mCustomViewCallback.onCustomViewHidden();
            this.mCustomViewCallback = null;
        }

        public void onShowCustomView(View paramView, WebChromeClient.CustomViewCallback paramCustomViewCallback)
        {
            if (this.mCustomView != null)
            {
                onHideCustomView();
                return;
            }
            this.mCustomView = paramView;
            this.mOriginalSystemUiVisibility = getActivity().getWindow().getDecorView().getSystemUiVisibility();
            this.mOriginalOrientation = getActivity().getRequestedOrientation();
            this.mCustomViewCallback = paramCustomViewCallback;
            ((FrameLayout)getActivity().getWindow().getDecorView()).addView(this.mCustomView, new FrameLayout.LayoutParams(-1, -1));
            getActivity().getWindow().getDecorView().setSystemUiVisibility(3846);
        }
    }

}