package com.procuro.androidscm.Restaurant.Status.StatusFragment.StoreListFragment;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Keep;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.diegocarloslima.fgelv.lib.FloatingGroupExpandableListView;
import com.diegocarloslima.fgelv.lib.WrapperExpandableListAdapter;
import com.procuro.androidscm.Logout_Confirmation_DialogFragment;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.SCMDataManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@Keep

public class StatusStoreListFragment extends Fragment {

    private Status_StoreListFragment_adapter adapter;
    private ExpandableListView expandableListView;
    private ArrayList<SiteList>array_sort = new ArrayList<>();
    private WrapperExpandableListAdapter wrapper;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, MMM dd, yyyy");
    private TextView date;
    private Button back;

    public StatusStoreListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.status_storelist_fragment, container, false);
        expandableListView = view.findViewById(R.id.expandible_listview);
        date = view.findViewById(R.id.date);
        back = view.findViewById(R.id.home);

        setupOnclicks();

        date.setText(dateFormat.format(new Date()));
        DisplaySiteList(view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void setupOnclicks(){
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DisplayLogoutConfirmation();

            }
        });

    }


    private void DisplaySiteList(View view){

        final ArrayList<SiteList>storeParentLists = SCMDataManager.getInstance().getSiteLists();

        LinearLayout search_container = view.findViewById(R.id.search_container);
        final EditText search = view.findViewById(R.id.search);
        expandableListView = (FloatingGroupExpandableListView)view.findViewById(R.id.expandible_listview);
        search.setFocusableInTouchMode(true);
        search_container.setFocusableInTouchMode(true);

        adapter = new Status_StoreListFragment_adapter(getContext(),storeParentLists,getActivity());
        wrapper = new WrapperExpandableListAdapter(adapter);
        expandableListView.setAdapter(wrapper);


        for (int i = 0; i < wrapper.getGroupCount() ; i++) {
            expandableListView.expandGroup(i);
        }

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                return true; // This way the expander cannot be collapsed
            }
        });


        search.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int textlength = search.getText().length();
                array_sort.clear();
                for (int i = 0; i < storeParentLists.size(); i++) {
                    boolean isparentCreated = false;
                    SiteList parentList = storeParentLists.get(i);
                    ArrayList<CustomSite> storeLists = parentList.getCustomSites();
                    ArrayList<CustomSite> sortedStorelist = new ArrayList<>();
                    for (int j = 0; j < storeLists.size(); j++) {
                        if (textlength <= storeLists.get(j).getSite().SiteName.length()) {
                            if (storeLists.get(j).getSite().SiteName.toLowerCase().trim().contains(
                                    search.getText().toString().toLowerCase().trim())) {
                                if (!isparentCreated) {
                                    sortedStorelist.add(storeLists.get(j));
                                    array_sort.add(new SiteList(parentList.getProvince(), sortedStorelist));
                                    isparentCreated = true;
                                }
                                else {
                                    sortedStorelist.add(storeLists.get(j));
                                }
                            }
                        }
                    }
                }
                adapter = new Status_StoreListFragment_adapter(getContext(), array_sort,getActivity());
                wrapper = new WrapperExpandableListAdapter(adapter);
                expandableListView.setAdapter(wrapper);

                for (int i = 0; i < wrapper.getGroupCount() ; i++) {
                    expandableListView.expandGroup(i);
                }
                expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                    @Override
                    public boolean onGroupClick(ExpandableListView parent, View v,
                                                int groupPosition, long id) {
                        return true; // This way the expander cannot be collapsed
                    }
                });
            }
        });
    }

    private void DisplayLogoutConfirmation() {
        DialogFragment newFragment = Logout_Confirmation_DialogFragment.newInstance();
        assert getFragmentManager() != null;
        newFragment.show(getActivity().getSupportFragmentManager(), "dialog");
    }


}
