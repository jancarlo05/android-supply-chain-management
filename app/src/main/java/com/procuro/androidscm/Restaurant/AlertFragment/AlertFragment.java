package com.procuro.androidscm.Restaurant.AlertFragment;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.procuro.androidscm.Logout_Confirmation_DialogFragment;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.Calendar.CalendarActivity;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.PimmyNews.Pimmy_News_DialogFragment;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.Restaurant.AllStorePopup.Status_SiteList_DialogFragment;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.Qualification_Store_Profile_Fragment;
import com.procuro.androidscm.Restaurant.QuickAccessMenu.QuickAccessMenu;
import com.procuro.androidscm.Restaurant.SOS.SOSActivity;
import com.procuro.androidscm.Restaurant.Qualification.QualificationActivity;
import com.procuro.androidscm.Restaurant.SOS.SOSData;
import com.procuro.androidscm.Restaurant.Schema.Dayparts;
import com.procuro.androidscm.Restaurant.Schema.Schema;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileActivity;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileData;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.Site;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;


public class AlertFragment extends Fragment {

    SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, MMM dd, yyyy");

    private Button Covid,profile,logout,quick_a_menu,sos;
    private LinearLayout dropdownContainer;
    private TextView titleheader ,storeName,date;


    public AlertFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.alerts_fragment, container, false);

        dropdownContainer = view.findViewById(R.id.dropdown_container);
        storeName = view.findViewById(R.id.storename);
        titleheader = view.findViewById(R.id.username);
        date = view.findViewById(R.id.date);
        Covid = view.findViewById(R.id.covid);
        logout = view.findViewById(R.id.home);
        quick_a_menu = view.findViewById(R.id.quick_a_menu);
        sos  =view.findViewById(R.id.sos);

        DisplayStorename(storeName,titleheader);

        setupOnclicks();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void setupOnclicks(){
        dropdownContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DisplaySiteListPopup();
            }
        });

        Covid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuickAccessMenu.DisplayCovidFragment(getActivity());
            }
        });
        quick_a_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuickAccessMenu.DisplayQuickAMenu(getContext(),quick_a_menu,getActivity());
            }
        });


        sos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SOSData data = new SOSData();
                data.setSite(SCMDataManager.getInstance().getSelectedSite().getSite());
                SOSData.setInstance(data);
                Intent intent = new Intent(getActivity(), SOSActivity.class);
                Objects.requireNonNull(getActivity()).startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
            }
        });


        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayLogoutConfirmation();
            }
        });

    }

    private void DisplayStorename(TextView textView , TextView title){
        Site site = SCMDataManager.getInstance().getSelectedSite().getSite();

        textView.setText(site.sitename);
        title.setText("aPIMM"+'\u2122'+ " - Restaurants");

        Schema schema = Schema.getInstance();
        Calendar currentDate = Calendar.getInstance();
        currentDate.setTimeZone(TimeZone.getTimeZone("UTC"));
        currentDate.add(Calendar.MINUTE,site.effectiveUTCOffset);

        SimpleDateFormat format = new SimpleDateFormat("EEEE MMM dd, yyyy");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        Dayparts daypart = schema.getDayparts().get(0);
        Calendar start = SCMTool.setCalendarDaypart(currentDate.getTime(),daypart.start);

        Calendar end = SCMTool.setCalendarDaypart(currentDate.getTime(),daypart.end);
        end.add(Calendar.MINUTE,-1);

        if (currentDate.getTime().compareTo(start.getTime())<0) {
            currentDate.add(Calendar.DATE,-1);
            date.setText(format.format(currentDate.getTime()));

        }else {
            date.setText(format.format(currentDate.getTime()));
        }
    }

    private void DisplaySiteListPopup(){
        DialogFragment newFragment = Status_SiteList_DialogFragment.newInstance("alerts");
        assert getFragmentManager() != null;
        newFragment.show(getActivity().getSupportFragmentManager(), "dialog");
    }

    private void DisplayLogoutConfirmation() {
        DialogFragment newFragment = Logout_Confirmation_DialogFragment.newInstance();
        assert getFragmentManager() != null;
        newFragment.show(getActivity().getSupportFragmentManager(), "dialog");
    }



}
