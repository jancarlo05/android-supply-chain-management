package com.procuro.androidscm.Restaurant.DashBoard.DopHuddle;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;


import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.procuro.androidscm.GlideApp;
import com.procuro.androidscm.R;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.SMSDailyOpsPlanCustomerSatisfactionItem;

import java.util.ArrayList;


public class Dashboard_dop_huddle_skills_and_position_listviewAdapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<SMSDailyOpsPlanCustomerSatisfactionItem> arraylist;
    public FragmentActivity fragmentActivity;


    public Dashboard_dop_huddle_skills_and_position_listviewAdapter(Context context, ArrayList<SMSDailyOpsPlanCustomerSatisfactionItem> arraylist, FragmentActivity fragmentActivity) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;
        this.fragmentActivity = fragmentActivity;
    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return arraylist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.dashboard_dop_huddle_skills_and_position_child_layout, null);
            view.setTag(holder);

            SMSDailyOpsPlanCustomerSatisfactionItem item = arraylist.get(position);

            ImageView icon = view.findViewById(R.id.icon);
            TextView title = view.findViewById(R.id.title);
            TextView task = view.findViewById(R.id.tasks);


            task.setText(SCMTool.CheckString(item.task,""));


            if (item.position.equalsIgnoreCase("Ops Leader")){
                GlideApp.with(mContext).asDrawable().load(R.drawable.dashboard_ops_leader).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
            }
            else if (item.position.equalsIgnoreCase("Support Manager")){
                GlideApp.with(mContext).asDrawable().load(R.drawable.dashboard_support_managers).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
            }
            else if (item.position.equalsIgnoreCase("DR Team")){
                GlideApp.with(mContext).asDrawable().load(R.drawable.front_register_orange).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
            }
            else if (item.position.equalsIgnoreCase("PUW Team")){
                GlideApp.with(mContext).asDrawable().load(R.drawable.puw_orange).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
            }
            else if (item.position.equalsIgnoreCase("Fries")){
                GlideApp.with(mContext).asDrawable().load(R.drawable.fry_station_orange).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
            }
            else if (item.position.equalsIgnoreCase("Grill")){
                GlideApp.with(mContext).asDrawable().load(R.drawable.grill_station_orange).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
            }
            else if (item.position.equalsIgnoreCase("Sandwiches")){
                GlideApp.with(mContext).asDrawable().load(R.drawable.sandwich_station_orange).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
            }
            else if (item.position.equalsIgnoreCase("LTO")){
                GlideApp.with(mContext).asDrawable().load(R.drawable.dashboard_lto).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
            }


            title.setText(item.position);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(fragmentActivity, Dashboard_DopHuddle_Activity.class);
                    fragmentActivity.startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(fragmentActivity).toBundle());
                }
            });

            return view;
    }

}

