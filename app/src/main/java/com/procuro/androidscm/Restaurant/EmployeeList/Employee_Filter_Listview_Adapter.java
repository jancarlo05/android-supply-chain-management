package com.procuro.androidscm.Restaurant.EmployeeList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;

import java.util.ArrayList;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;


public class Employee_Filter_Listview_Adapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<EmployeeFilter> list;
    private RecyclerView recyclerView;
    private FragmentActivity fragmentActivity;
    private SimpleTooltip tooltip;
    EmployeeList_User_RecyclerViewAdapter adapter;
    private TextView message;
    private ArrayList<CustomUser>searchUser;
    private EditText search;

    public Employee_Filter_Listview_Adapter(Context context, ArrayList<EmployeeFilter> list, RecyclerView recyclerView,
                                            FragmentActivity fragmentActivity, SimpleTooltip tooltip, TextView message,
                                            ArrayList<CustomUser>searchUser,EditText search) {
        this.mContext = context;
        this.inflater = LayoutInflater.from(mContext);
        this.list = list;
        this.recyclerView = recyclerView;
        this.fragmentActivity = fragmentActivity;
        this.tooltip = tooltip;
        this.message = message;
        this.searchUser =searchUser;
        this.search = search;

    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
            view = inflater.inflate(R.layout.qualification_filter_listview, null);

            final EmployeeFilter filter = list.get(position);

            TextView name = view.findViewById(R.id.name);
            ImageView icon = view.findViewById(R.id.selected);
            name.setText(filter.getName());


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (EmployeeFilter filters : list){
                        filters.setSelected(false);
                    }
                    filter.setSelected(true);

                    EmployeeListActivity.SelectPerspective(mContext,fragmentActivity);

                    notifyDataSetChanged();

                    tooltip.dismiss();


                }
            });

        if (filter.isSelected()){
            icon.setVisibility(View.VISIBLE);
        }else {
            icon.setVisibility(View.INVISIBLE);
        }

        return view;
    }


}

