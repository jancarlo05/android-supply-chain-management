package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.WeeklySchedule;

import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.SMSPositioning;
import com.procuro.apimmdatamanagerlib.SMSPositioningEmployeeScheduleAssignment;
import com.procuro.apimmdatamanagerlib.SMSPositioningEmployeeScheduleAssignmentPositions;
import com.procuro.apimmdatamanagerlib.Schedule_Business_Site_Plan;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

import static com.procuro.androidscm.SCMTool.DaypartTimeToDate;


public class Dashboard_staffing_weekly_fragment extends Fragment {

    private Dashboard_Weekly_Sched_RecyclerViewAdapter adapter;
    private Dashboard_Weekdays_RecyclerViewAdapter weekdays_adapter;
    RecyclerView listView;
    private RecyclerView weekdays,total_employees,total_hours_list;
    private TextView date,message;
    private ConstraintLayout datepicker;
    private ProgressBar progressbar;

    public Dashboard_staffing_weekly_fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dashboard_header_staffing_weekly_sched_fragment, container, false);

        listView = view.findViewById(R.id.listview);
        weekdays = view.findViewById(R.id.weekdays);
        date = view.findViewById(R.id.date);
        datepicker = view.findViewById(R.id.date_container);
        total_hours_list = view.findViewById(R.id.total_hours);
        total_employees = view.findViewById(R.id.total_employees);
        progressbar = view.findViewById(R.id.progressbar);
        message = view.findViewById(R.id.message);


        Site site = SCMDataManager.getInstance().getSelectedSite().getSite();
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar = SCMTool.getDateWithDaypart(calendar.getTime().getTime(),site.effectiveUTCOffset);
        if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
            calendar.add(Calendar.WEEK_OF_YEAR,-1);
        }
        GenerateWeekDays(calendar.getTime(),true);


        datepicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayCarrier(getContext(),datepicker);
            }
        });

        return view;
    }

    private void CheckDefaultSched(Date date){
        SCMDataManager scmDataManager = SCMDataManager.getInstance();
        ArrayList<Date>Weekdays = getWeekdays(date);
        if (scmDataManager.getWeeklySchedule() == null){
            DownloadSchedule(Weekdays,true);
        }else {
            PopulateData(scmDataManager.getWeeklySchedule(),Weekdays);
        }
    }

    private ArrayList<Date> getWeekdays(Date date){

        Calendar now = Calendar.getInstance();
        now.setTimeZone(TimeZone.getTimeZone("UTC"));
        now.setTime(date);

        ArrayList<Date>Weekdays = new ArrayList<>();
        int delta = -now.get(Calendar.DAY_OF_WEEK) + 2; //add 2 if your week start on monday
        now.add(Calendar.DAY_OF_MONTH, delta );
        for (int i = 0; i < 7; i++) {
            Weekdays.add(now.getTime());
            now.add(Calendar.DAY_OF_MONTH, 1);
        }

        return Weekdays;
    }


    private void GenerateWeekDays(Date selectedDate,boolean isDefault){
        try {
            ShowLoadDing(true);
            ArrayList<Date>Weekdays = getWeekdays(selectedDate);
            SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            date.setText(format.format(Weekdays.get(0)));
            date.append(" to ");
            date.append(format.format(Weekdays.get(Weekdays.size()-1)));

            weekdays_adapter = new Dashboard_Weekdays_RecyclerViewAdapter(getContext(),Weekdays);
            weekdays.setLayoutManager(new GridLayoutManager(getContext(),7));
            weekdays.setAdapter(weekdays_adapter);

            if (isDefault){
                CheckDefaultSched(selectedDate);
            }else {
                DownloadSchedule(Weekdays,false);
            }

        }catch (Exception e){
         e.printStackTrace();
        }
    }

    private void ShowLoadDing(boolean show){
        try {
            if (show){
                progressbar.setVisibility(View.VISIBLE);
                message.setVisibility(View.VISIBLE);
                listView.setVisibility(View.INVISIBLE);
            }else {
                progressbar.setVisibility(View.GONE);
                message.setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void DownloadSchedule(final ArrayList<Date>Weekdays, final boolean isDefault){
        aPimmDataManager dataManager = aPimmDataManager.getInstance();
        Site site = SCMDataManager.getInstance().getSelectedSite().getSite();

        dataManager.getSchedulesByDate(site.siteid, Weekdays.get(0), Weekdays.get(Weekdays.size()-1), new OnCompleteListeners.getEmployeeScheduleListener() {
            @Override
            public void getEmployeeScheduleCallback(ArrayList<Schedule_Business_Site_Plan> schedules, Error error) {
                if (isDefault){
                    SCMDataManager.getInstance().setWeeklySchedule(schedules);
                }
                PopulateData(schedules,Weekdays);
            }
        });
    }

    private void PopulateData(ArrayList<Schedule_Business_Site_Plan>schedules,ArrayList<Date>Weekdays){

        ArrayList<CustomUser>customUsers = SCMTool.getActiveCustomUser(SCMDataManager.getInstance().getStoreUsers());
        Collections.sort(customUsers, new Comparator<CustomUser>() {
            @Override
            public int compare(CustomUser o1, CustomUser o2) {
                return o1.getUser().firstName.compareToIgnoreCase(o2.getUser().firstName);
            }
        });

        for (CustomUser user : customUsers){
            ArrayList<Schedule_Business_Site_Plan>filteredSchedules = new ArrayList<>();
            if (schedules!=null){
                for (Schedule_Business_Site_Plan sched : schedules){
                    if (user.getUser().userId.equalsIgnoreCase(sched.userID)){
                        filteredSchedules.add(sched);
                    }
                }
            }
            user.setSchedules(filteredSchedules);
        }

        try {
            adapter = new Dashboard_Weekly_Sched_RecyclerViewAdapter(getContext(), customUsers,Weekdays);
            listView.setLayoutManager(new GridLayoutManager(getContext(),1));
            listView.setAdapter(adapter);

        }catch (Exception e){
            e.printStackTrace();
        }


        ShowLoadDing(false);

        GenerateTotals(Weekdays,customUsers);

    }

    private void GenerateTotals(ArrayList<Date>dates,ArrayList<CustomUser>users){
        try {
            ArrayList<String>total_employee = new ArrayList<>();
            ArrayList<String>total_hours = new ArrayList<>();

            SMSPositioning smsPositionings = SCMDataManager.getInstance().getPositionForm();
            ArrayList<SMSPositioningEmployeeScheduleAssignment> assignments = smsPositionings.employeeScheduleAssignmentList;

            SimpleDateFormat timeformat = new SimpleDateFormat("hh:mm");
            SimpleDateFormat dateFormat= new SimpleDateFormat("EEE MMM/dd/yy");
            timeformat.setTimeZone(TimeZone.getTimeZone("UTC"));
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

            for (Date selectedDate : dates){
                long totalStart = 0;
                long total_end = 0;
                int totalEmployee = 0;

                for (CustomUser user : users){
                    boolean hasSchedule = false;
                    if (user.getSchedules()!=null){
                        for (Schedule_Business_Site_Plan sched : user.getSchedules()){
                            if (dateFormat.format(selectedDate).equalsIgnoreCase(dateFormat.format(sched.date))){

                                final SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
                                timeFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));

                                Calendar startTime = DaypartTimeToDate(timeFormatter.format(sched.startTime), selectedDate);
                                Calendar endTime = DaypartTimeToDate(timeFormatter.format(sched.endTime), selectedDate);

                                totalStart += startTime.getTime().getTime();
                                total_end += endTime.getTime().getTime();

                                hasSchedule = true;
                            }
                        }
                    }
                    if (hasSchedule){
                        totalEmployee++;
                    }
                }


                long diff = total_end - totalStart;
                int diffhours = (int) (diff / (60 * 60 * 1000));
                int diffmin = (int) (diff / (60 * 1000)% 60);

                total_hours.add(String.format("%02d", diffhours)+":"+String.format("%02d", diffmin));
                total_employee.add(String.valueOf(totalEmployee));
            }

            DispayTotal(total_employee,total_employees);
            DispayTotal(total_hours,total_hours_list);


        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static String getResult(int total) {
        int minutes = total % 60;
        int hours = ((total - minutes) / 60) % 24;
        return String.format("%02d:%02d", hours, minutes);
    }




    private void DispayTotal(ArrayList<String>total,RecyclerView recyclerView){
        Dashboard_Weekly_Sched_Total_RecyclerViewAdapter adapter = new Dashboard_Weekly_Sched_Total_RecyclerViewAdapter(getContext(),total);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),7));
        recyclerView.setAdapter(adapter);
    }

    public void DisplayCarrier(final Context context, View anchor) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.BOTTOM)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .arrowColor(ContextCompat.getColor(context,R.color.white))
                .transparentOverlay(true)
                .contentView(R.layout.weekly_picker)
                .focusable(true)
                .build();
        tooltip.show();

        final NumberPicker picker =tooltip.findViewById(R.id.picker);
        Button accept = tooltip.findViewById(R.id.apply);

        String[] values= null;
        final SCMDataManager data = SCMDataManager.getInstance();
        ArrayList<WeeklyPickerDate>weeklyPickerDates = data.getWeeklyPickerDates();
        int currentWeek = Calendar.getInstance().get(Calendar.WEEK_OF_YEAR);
        int currenyYear = Calendar.getInstance().get(Calendar.YEAR);

        if (weeklyPickerDates!=null){
             values = new String[weeklyPickerDates.size()];

            for (int i = 0; i < weeklyPickerDates.size(); i++) {
                WeeklyPickerDate weekdate = weeklyPickerDates.get(i);
                values[i] = weekdate.getName();

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(weekdate.getWeekdays().get(0));
                int year = calendar.get(Calendar.YEAR);
                if (year== currenyYear){
                    if (weekdate.getWeeknumber() == currentWeek){
                        currentWeek = i;
                    }
                }
            }
            picker.setDisplayedValues(values);
            picker.setMinValue(0);
            picker.setMaxValue(values.length-1);
            picker.setValue(currentWeek);

        }else {
            Toast.makeText(context, "NULL", Toast.LENGTH_SHORT).show();
        }

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<Date>Weekdays = data.getWeeklyPickerDates().get(picker.getValue()).getWeekdays();
                GenerateWeekDays(Weekdays.get(Weekdays.size()/2),false);
                tooltip.dismiss();
            }
        });
    }



}
