package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Traning.VTAs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourceManualFragment.Resources_right_pane_fragment;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourcesChildList;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourcesGrandChildList;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.apimmdatamanagerlib.DocumentDTO;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.util.ArrayList;

import es.voghdev.pdfviewpager.library.RemotePDFViewPager;
import es.voghdev.pdfviewpager.library.adapter.PDFPagerAdapter;
import es.voghdev.pdfviewpager.library.remote.DownloadFile;
import es.voghdev.pdfviewpager.library.util.FileUtil;

public class Dashboard_Vtas_WITH_PDFVIER_Activity extends AppCompatActivity implements  DownloadFile.Listener {



    private Button back;
    ExpandableListView expandableListView;
    EditText search;
    ArrayList<ResourcesChildList>array_sort = new ArrayList<>();


    public static DocumentDTO documentDTO;
    public static ProgressBar progressBar;
    public static TextView date , wait , title;
    public static RemotePDFViewPager remotePDFViewPager;
    public static PDFPagerAdapter adapter;
    public static ConstraintLayout pdf_container;
    private com.procuro.apimmdatamanagerlib.aPimmDataManager dataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_traning_vta_activity);

        title = findViewById(R.id.username);
        back = findViewById(R.id.home);
        expandableListView = findViewById(R.id.expandible_listview);
        search = findViewById(R.id.search);
        progressBar = findViewById(R.id.progressBar2);
        wait = findViewById(R.id.wait);
        pdf_container = findViewById(R.id.pdf_container);


        SetUpSiteInformation();

        setUpOnclicks();

        populateSafetyDataSheets();

        Searchfunction();


    }

    @SuppressLint("ClickableViewAccessibility")
    private void setUpOnclicks(){
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        search.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                return false;
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            }
        });

    }


    private void populateSafetyDataSheets(){
        SCMDataManager data = SCMDataManager.getInstance();
        if (data.getVtas()!=null){
            Dashboard_Vta_listview_adapter adapter = new Dashboard_Vta_listview_adapter(this,data.getVtas(), Dashboard_Vtas_WITH_PDFVIER_Activity.this);
            expandableListView.setAdapter(adapter);
            for (int i = 0; i <adapter.getGroupCount() ; i++) {
                ResourcesChildList childList = data.getVtas().get(i);
                if (childList.getGrandChildLists()!=null){
                    if (childList.getGrandChildLists().size()>0){
                        expandableListView.expandGroup(i);
                        setDownloadButtonListener(childList.getGrandChildLists().get(0).getDocumentDTO(),this);
                        break;
                    }
                }
            }

        }
    }

    private void SetUpSiteInformation(){
        try {
            title.setText(SCMDataManager.getInstance().getSelectedSite().getSite().sitename);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void Searchfunction(){

        final ArrayList<ResourcesChildList>traningData = SCMDataManager.getInstance().getVtas();

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int textlength = search.getText().length();
                ArrayList<String>strings = new ArrayList<>();

                if (textlength>0){
                    array_sort.clear();

                    for (ResourcesChildList dashboardTraningData : traningData){
                        boolean isparentCreated = false;
                        ArrayList<ResourcesGrandChildList> trainingVideos = dashboardTraningData.getGrandChildLists();
                        ArrayList<ResourcesGrandChildList> sortedvideo = new ArrayList<>();

                        if (textlength <= dashboardTraningData.getAppString().length()){
                            if (dashboardTraningData.getAppString().toLowerCase().trim().contains(
                                    search.getText().toString().toLowerCase().trim())){
                                if (!strings.contains(dashboardTraningData.getAppString())){
                                    strings.add(dashboardTraningData.getAppString());
                                    array_sort.add(new ResourcesChildList(dashboardTraningData.getAppString(), sortedvideo));
                                }
                            }
                        }

                        for (ResourcesGrandChildList trainingVideo: trainingVideos){
                            if (textlength <= trainingVideo.getDocumentDTO().name.length()){
                                if (trainingVideo.getDocumentDTO().name.toLowerCase().trim().contains(
                                        search.getText().toString().toLowerCase().trim())) {
                                    if (!strings.contains(dashboardTraningData.getAppString())) {
                                        strings.add(dashboardTraningData.getAppString());
                                        sortedvideo.add(trainingVideo);
                                        array_sort.add(new ResourcesChildList(dashboardTraningData.getAppString(), sortedvideo));

                                    }
                                    else {
                                        sortedvideo.add(trainingVideo);
                                    }
                                }

                            }
                        }
                    }
                    Dashboard_Vta_listview_adapter adapter = new Dashboard_Vta_listview_adapter(Dashboard_Vtas_WITH_PDFVIER_Activity.this,array_sort, Dashboard_Vtas_WITH_PDFVIER_Activity.this);
                    expandableListView.setAdapter(adapter);
                    for (int i = 0; i <adapter.getGroupCount() ; i++) {
                        expandableListView.expandGroup(i);
                    }
                }else {
                    Dashboard_Vta_listview_adapter adapter = new Dashboard_Vta_listview_adapter(Dashboard_Vtas_WITH_PDFVIER_Activity.this,traningData, Dashboard_Vtas_WITH_PDFVIER_Activity.this);
                    expandableListView.setAdapter(adapter);
                    for (int i = 0; i <adapter.getGroupCount() ; i++) {
                        if (i <= 1){
                            expandableListView.expandGroup(i);
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void setDownloadButtonListener(DocumentDTO documentDTO, final Context context) {
        progressBar.setVisibility(View.VISIBLE);
        wait.setVisibility(View.VISIBLE);
        wait.setText("Please Wait");

         DownloadFile.Listener listener = this;

        aPimmDataManager dataManager = aPimmDataManager.getInstance();
        String[] strings = dataManager.getAuthHeaderValue().split("Basic ");

        StringBuilder urlString = new StringBuilder();
        urlString.append(dataManager.getBaseUrl());
        urlString.append("Document/Document/GetContent/");
        urlString.append("?documentId=");
        urlString.append(documentDTO.documentId);
        urlString.append("&authorization=").append(strings[1]);

        System.out.println(urlString.toString());

        try {
            remotePDFViewPager = new RemotePDFViewPager(context, urlString.toString(), listener);
            remotePDFViewPager.setId(R.id.pdfViewPager);
        }catch (Exception e){
            progressBar.setVisibility(View.INVISIBLE);
            wait.setText("NO PREVIEW AVAILABLE");
        }

    }

    public static void open(Context context) {
        Intent i = new Intent(context, Resources_right_pane_fragment.class);
        context.startActivity(i);

    }

    public static void updateLayout() {
        wait.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        remotePDFViewPager.setVisibility(View.VISIBLE);
        pdf_container.addView(remotePDFViewPager);
    }

    @Override
    public void onSuccess(String url, String destinationPath) {

        try {
            adapter = new PDFPagerAdapter(this, FileUtil.extractFileNameFromURL(url));
            remotePDFViewPager.setAdapter(adapter);
            updateLayout();

        }catch (Exception e){
            progressBar.setVisibility(View.INVISIBLE);
            wait.setText("NO PREVIEW AVAILABLE");
        }
    }

    @Override
    public void onFailure(Exception e) {
        e.printStackTrace();
        progressBar.setVisibility(View.INVISIBLE);
        wait.setText("NO PREVIEW AVAILABLE");

    }

    @Override
    public void onProgressUpdate(int progress, int total) {
        System.out.println("PROGRESS: "+progress+"/"+total);

    }

}