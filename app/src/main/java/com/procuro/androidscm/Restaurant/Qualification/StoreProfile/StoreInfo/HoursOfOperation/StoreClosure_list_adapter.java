package com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.HoursOfOperation;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.Calendar.CalendarEvent;
import com.procuro.androidscm.SCMTool;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import io.github.douglasjunior.androidSimpleTooltip.OverlayView;
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;


public class StoreClosure_list_adapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<CalendarEvent> arraylist;

    public StoreClosure_list_adapter(Context context, ArrayList<CalendarEvent> arraylist) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;
    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return arraylist.size();
    }

    @Override
    public Object getItem(int position) {
        return arraylist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
        view = inflater.inflate(R.layout.store_closure_list_data, null);

        final CalendarEvent event = arraylist.get(position);
        TextView name = view.findViewById(R.id.name);
        TextView start = view.findViewById(R.id.start);
        TextView end = view.findViewById(R.id.end);

        SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");

        try {
            name.setText(SCMTool.CheckString(event.getName(),"--"));

            if (event.getStartTime()!=null){
                start.setText(format.format(event.getStartTime()));
            }else {
                start.setText("--");
            }

            if (event.getEndTime()!=null){
                end.setText(format.format(event.getEndTime()));
            }else {
                end.setText("--");
            }


        }catch (Exception e){
            e.printStackTrace();
        }

        return view;
    }


    public void DisplayTimePicker(final Context context, View anchor,final TextView textView) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.BOTTOM)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .arrowColor(ContextCompat.getColor(context, R.color.white))
                .transparentOverlay(false)
                .contentView(R.layout.time_picker)
                .focusable(true)
                .overlayOffset(0)
                .highlightShape(OverlayView.HIGHLIGHT_SHAPE_RECTANGULAR)
                .build();
        tooltip.show();

        final TimePicker timePicker = tooltip.findViewById(R.id.datepicker);
        Button accept = tooltip.findViewById(R.id.apply);


        final SimpleDateFormat dateFormat = new SimpleDateFormat("h:mm a");

        timePicker.setIs24HourView(false);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar date = Calendar.getInstance();
                textView.setText(SCMTool.getTime(timePicker));
                tooltip.dismiss();
            }
        });

    }

}

