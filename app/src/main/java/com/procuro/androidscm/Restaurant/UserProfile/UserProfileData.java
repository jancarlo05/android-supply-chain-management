package com.procuro.androidscm.Restaurant.UserProfile;

import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.apimmdatamanagerlib.Certification;

import java.util.ArrayList;
import java.util.Date;

public class UserProfileData {

    private static UserProfileData instance = new UserProfileData();

    private CustomSite customSite;
    private CustomUser customUser;
    private boolean fromQualification;
    private boolean fromQuickAccess;
    private boolean isUserEditEnabled;
    private String prevActivity;
    private boolean isupdate;
    private boolean isCreate;
    private boolean fromThirft;


    //edit profile data temporary storage
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String postal;
    private String employeeID;
    private String pID;
    private String cellPhone;
    private String dayPhone;
    private Date hireDate;
    private Date DOB;
    private String role;
    private String contactEmail;
    private String mobileCarrier;
    private Date reviewDate;
    private ArrayList<String>certificationList;
    private ArrayList<String>roles;
    private ArrayList<String> update_Siteallowed;
    private ArrayList<String> current_Site_Allowed;
    private ArrayList<CustomSite>selectedSites;
    private ArrayList<Certification>newCertification;

    public ArrayList<Certification> getNewCertification() {
        return newCertification;
    }

    public void setNewCertification(ArrayList<Certification> newCertification) {
        this.newCertification = newCertification;
    }

    public boolean isFromThirft() {
        return fromThirft;
    }

    public void setFromThirft(boolean fromThirft) {
        this.fromThirft = fromThirft;
    }

    public boolean isIsupdate() {
        return isupdate;
    }

    public void setIsupdate(boolean isupdate) {
        this.isupdate = isupdate;
    }

    public boolean isCreate() {
        return isCreate;
    }

    public void setCreate(boolean create) {
        isCreate = create;
    }

    public String getPrevActivity() {
        return prevActivity;
    }

    public void setPrevActivity(String prevActivity) {
        this.prevActivity = prevActivity;
    }

    public ArrayList<CustomSite> getSelectedSites() {
        return selectedSites;
    }

    public void setSelectedSites(ArrayList<CustomSite> selectedSites) {
        this.selectedSites = selectedSites;
    }

    public ArrayList<String> getCurrent_Site_Allowed() {
        return this.current_Site_Allowed;
    }

    public void setCurrent_Site_Allowed(ArrayList<String> current_Site_Allowed) {
        this.current_Site_Allowed = current_Site_Allowed;
    }

    public ArrayList<String> getUpdate_Siteallowed() {
        return update_Siteallowed;
    }

    public void setUpdate_Siteallowed(ArrayList<String> update_Siteallowed) {
        this.update_Siteallowed = update_Siteallowed;
    }

    public static UserProfileData getInstance() {
        if(instance == null) {
            instance = new UserProfileData();
        }
        return instance;
    }


    public ArrayList<String> getRoles() {
        return roles;
    }

    public void setRoles(ArrayList<String> roles) {
        this.roles = roles;
    }

    public boolean isUserEditEnabled() {
        return isUserEditEnabled;
    }

    public void setUserEditEnabled(boolean userEditEnabled) {
        isUserEditEnabled = userEditEnabled;
    }

    public static void setInstance(UserProfileData instance) {
        UserProfileData.instance = instance;
    }

    public CustomSite getCustomSite() {
        return customSite;
    }

    public void setCustomSite(CustomSite customSite) {
        this.customSite = customSite;
    }

    public CustomUser getCustomUser() {
        return customUser;
    }

    public void setCustomUser(CustomUser customUser) {
        this.customUser = customUser;
    }

    public boolean isFromQualification() {
        return fromQualification;
    }

    public void setFromQualification(boolean fromQualification) {
        this.fromQualification = fromQualification;
    }

    public boolean isFromQuickAccess() {
        return fromQuickAccess;
    }

    public void setFromQuickAccess(boolean fromQuickAccess) {
        this.fromQuickAccess = fromQuickAccess;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostal() {
        return postal;
    }

    public void setPostal(String postal) {
        this.postal = postal;
    }

    public String getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(String employeeID) {
        this.employeeID = employeeID;
    }

    public String getpID() {
        return pID;
    }

    public void setpID(String pID) {
        this.pID = pID;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getDayPhone() {
        return dayPhone;
    }

    public void setDayPhone(String dayPhone) {
        this.dayPhone = dayPhone;
    }

    public Date getHireDate() {
        return hireDate;
    }

    public void setHireDate(Date hireDate) {
        this.hireDate = hireDate;
    }

    public Date getDOB() {
        return DOB;
    }

    public void setDOB(Date DOB) {
        this.DOB = DOB;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getMobileCarrier() {
        return mobileCarrier;
    }

    public void setMobileCarrier(String mobileCarrier) {
        this.mobileCarrier = mobileCarrier;
    }

    public Date getReviewDate() {
        return reviewDate;
    }

    public void setReviewDate(Date reviewDate) {
        this.reviewDate = reviewDate;
    }

    public ArrayList<String> getCertificationList() {
        return certificationList;
    }

    public void setCertificationList(ArrayList<String> certificationList) {
        this.certificationList = certificationList;
    }
}
