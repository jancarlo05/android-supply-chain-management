package com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.EquipmenList;

import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsEquipment;

import java.util.ArrayList;

public class EquipmenlistData {

    private ArrayList<SiteSettingsEquipment>siteSettingsEquipments;
    private boolean UnsaveEdit;
    private ArrayList<SiteSettingsEquipment>siteSettingsEquipmentsUpdates;
    private boolean probeUpdated;

    private static EquipmenlistData instance = new EquipmenlistData();

    public static EquipmenlistData getInstance() {
        if(instance == null) {
            instance = new EquipmenlistData();
        }
        return instance;
    }

    public boolean isProbeUpdated() {
        return probeUpdated;
    }

    public void setProbeUpdated(boolean probeUpdated) {
        this.probeUpdated = probeUpdated;
    }

    public ArrayList<SiteSettingsEquipment> getSiteSettingsEquipmentsUpdates() {
        return siteSettingsEquipmentsUpdates;
    }

    public void setSiteSettingsEquipmentsUpdates(ArrayList<SiteSettingsEquipment> siteSettingsEquipmentsUpdates) {
        this.siteSettingsEquipmentsUpdates = siteSettingsEquipmentsUpdates;
    }

    public boolean isUnsaveEdit() {
        return UnsaveEdit;
    }

    public void setUnsaveEdit(boolean unsaveEdit) {
        UnsaveEdit = unsaveEdit;
    }

    public ArrayList<SiteSettingsEquipment> getSiteSettingsEquipments() {
        return siteSettingsEquipments;
    }

    public void setSiteSettingsEquipments(ArrayList<SiteSettingsEquipment> siteSettingsEquipments) {
        this.siteSettingsEquipments = siteSettingsEquipments;
    }

    public static void setInstance(EquipmenlistData instance) {
        EquipmenlistData.instance = instance;
    }

    public ArrayList<CustomEquipment> getEquipmentByArea(String area){
        ArrayList<CustomEquipment> equipments = new ArrayList<>();

        if (this.siteSettingsEquipments!=null){
            for (SiteSettingsEquipment equipment : siteSettingsEquipments){
                System.out.println("DESCRIPTION : "+equipment.equipmentName);
                if (equipment.area.equalsIgnoreCase(area)){
                    equipments.add(new CustomEquipment(equipment));
                }
            }
        }
        return equipments;
    }
}
