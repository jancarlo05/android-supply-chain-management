package com.procuro.androidscm.Restaurant.Qualification.QualificationFragment;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.procuro.androidscm.GlideApp;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.Qualification.QualificationActivity;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.Qualification.QualificationActivityFragment;
import com.procuro.androidscm.SCMTool;

import java.util.ArrayList;

public class Qualification_Owner_Corporate_List_adapter extends BaseExpandableListAdapter implements ExpandableListAdapter {

    private Context context;
    private ArrayList<Corp_Chain> qualificationChains;
    private FragmentActivity fragmentActivity;

    public Qualification_Owner_Corporate_List_adapter(Context context,
                                                      ArrayList<Corp_Chain> qualificationChains,
                                                      FragmentActivity fragmentActivity) {
        this.qualificationChains = qualificationChains;
        this.context = context;
        this.fragmentActivity = fragmentActivity;
    }

    @Override
    public int getGroupCount() {
        return qualificationChains.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return qualificationChains.get(groupPosition).qualificationRegionals.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return qualificationChains.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return qualificationChains.get(groupPosition).getQualificationRegionals().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        Corp_Chain qualificationChain = qualificationChains.get(groupPosition);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.qualification_chain_level, parent, false);
        TextView name = contentView.findViewById(R.id.name);
        TextView count = contentView.findViewById(R.id.childcount);

        int childcount = 0;



        for (Corp_Regional regional : qualificationChain.getQualificationRegionals()){
            for (Corp_Division division : regional.getQualificationDivisions()){
                for (Corp_Area area : division.getQualificationAreas()){
                    for (Corp_District district : area.getQualificationDistricts()){
                        for (CustomSite site : district.getSites()){
                            childcount++;
                        }
                    }
                }
            }
        }

        name.setText(qualificationChain.getName());
        count.setText(String.valueOf(childcount));

        if (qualificationChain.getQualificationRegionals()==null){
            contentView = new LinearLayout(context);

        }else {
            if (qualificationChain.getQualificationRegionals().size()==0){
                contentView = new LinearLayout(context);

            }
        }
       return contentView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final Corp_Regional qualificationRegional = qualificationChains.get(groupPosition).getQualificationRegionals().get(childPosition);
        final Corp_Chain qualificationChain = qualificationChains.get(groupPosition);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.qualificaton_region_level, parent, false);

        TextView name = contentView.findViewById(R.id.name);
        ImageView icon   = contentView.findViewById(R.id.icon);
        final LinearLayout linearLayout = contentView.findViewById(R.id.linearLayout);
        final LinearLayout contianer = contentView.findViewById(R.id.container);
        TextView count = contentView.findViewById(R.id.childcount);

        int childcount = 0;


        for (Corp_Division qualificationDivision : qualificationRegional.getQualificationDivisions()){
            for (Corp_Area qualificationArea : qualificationDivision.getQualificationAreas()){
                for (Corp_District qualificationDistrict : qualificationArea.getQualificationDistricts()){
                    for (CustomSite site: qualificationDistrict.getSites()){
                        childcount++;
                    }
                }
            }
        }

        name.setText(qualificationRegional.corpStructures.name);
        count.setText(String.valueOf(childcount));


        if (qualificationRegional.getCorpStructures().name.toUpperCase().contains("UNITED STATES")){
            GlideApp.with(context).load(R.drawable.region_usa).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
        }
        else  if (qualificationRegional.getCorpStructures().name.toUpperCase().contains("PAFICIC")){
            GlideApp.with(context).load(R.drawable.region_paficic).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
        }
        else  if (qualificationRegional.getCorpStructures().name.toUpperCase().contains("CANADA")){
            GlideApp.with(context).load(R.drawable.region_canada).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
        }
        else  if (qualificationRegional.getCorpStructures().name.toUpperCase().contains("LATIN AMERICA")){
            GlideApp.with(context).load(R.drawable.region_latin_america).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
        }
        else  if (qualificationRegional.getCorpStructures().name.toUpperCase().contains("AMEA")){
            GlideApp.with(context).load(R.drawable.region_emea).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
        }


        for (int j = 0; j < qualificationRegional.getQualificationDivisions().size() ; j++) {
            Corp_Division qualificationDivision = qualificationRegional.getQualificationDivisions().get(j);
            getDIvistionVIew(contianer, qualificationDivision,parent,qualificationRegional);

        }

        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (qualificationRegional.isSelected()){
                    qualificationRegional.setSelected(false);
                    Hide(contianer,linearLayout);
                }else {
                    qualificationRegional.setSelected(true);
                    Show(contianer,linearLayout);
                }

//                setUpQualificationData(fragmentActivity,qualificationRegional,qualificationRegional.corpStructures.name,null);
            }
        });

        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (qualificationRegional.isSelected()){
                    qualificationRegional.setSelected(false);
                    Hide(contianer,linearLayout);
                }else {
                    qualificationRegional.setSelected(true);
                    Show(contianer,linearLayout);
                }
            }
        });

        if (qualificationRegional.isSelected()){
            Show(contianer,linearLayout);
        }else {
            Hide(contianer,linearLayout);
        }


        return contentView;
    }


    private void getDIvistionVIew(LinearLayout container,
                                  final Corp_Division qualificationDivision,
                                  ViewGroup parent,final Corp_Regional regional){
        View contentView = LayoutInflater.from(context).inflate(R.layout.qualificaton_division_level, parent, false);

        TextView name = contentView.findViewById(R.id.name);
        ImageView icon   = contentView.findViewById(R.id.icon);
        final LinearLayout linearLayout = contentView.findViewById(R.id.linearLayout);
        final LinearLayout contianer = contentView.findViewById(R.id.container);
        TextView count = contentView.findViewById(R.id.childcount);

        int counter = 0;

        for (Corp_Area qualificationArea : qualificationDivision.getQualificationAreas()){
            for (Corp_District qualificationDistrict : qualificationArea.getQualificationDistricts()){
                for (CustomSite site : qualificationDistrict.getSites()){
                    counter++;
                }
            }
        }

        for (Corp_Area qualificationArea : qualificationDivision.getQualificationAreas()){

            getAreaView(contianer, qualificationArea,parent,regional);
        }

        count.setText(String.valueOf(counter));
        name.setText(qualificationDivision.corpStructures.name);
        GlideApp.with(context).load(R.drawable.division_icon).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);


        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (qualificationDivision.isSelected()){
                    qualificationDivision.setSelected(false);
                    Hide(contianer,linearLayout);
                }else {
                    qualificationDivision.setSelected(true);
                    Show(contianer,linearLayout);
                }

//                setUpQualificationData(fragmentActivity,regional,qualificationDivision.corpStructures.name,null);
            }
        });


        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (qualificationDivision.isSelected()){
                    qualificationDivision.setSelected(false);
                    Hide(contianer,linearLayout);
                }else {
                    qualificationDivision.setSelected(true);
                    Show(contianer,linearLayout);
                }
            }
        });

        if (qualificationDivision.isSelected()){
            Show(contianer,linearLayout);
        }else {
            Hide(contianer,linearLayout);
        }

        container.addView(contentView);
    }

    private void getAreaView(LinearLayout container, final Corp_Area qualificationArea,
                             ViewGroup parent,
                             final Corp_Regional regional) {

        View contentView = LayoutInflater.from(context).inflate(R.layout.qualificaton_area_level, parent, false);

        TextView name = contentView.findViewById(R.id.name);
        ImageView icon   = contentView.findViewById(R.id.icon);
        final LinearLayout linearLayout = contentView.findViewById(R.id.linearLayout);
        final LinearLayout contianer = contentView.findViewById(R.id.container);
        TextView count = contentView.findViewById(R.id.childcount);

        int counter = 0;
        boolean isCompany = true;

        for (Corp_District qualificationDistrict : qualificationArea.getQualificationDistricts()){
            for (CustomSite site : qualificationDistrict.getSites()){
                counter++;
            }
        }

        for (Corp_District qualificationDistrict : qualificationArea.getQualificationDistricts()){
            getDistrictView(contianer, qualificationDistrict,parent,regional);
        }

        count.setText(String.valueOf(counter));
        name.setText(qualificationArea.corpStructures.name);
        GlideApp.with(context).load(R.drawable.area_icon).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);


        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (qualificationArea.isSelected()){
                    qualificationArea.setSelected(false);
                    Hide(contianer,linearLayout);
                }else {
                    qualificationArea.setSelected(true);
                    Show(contianer,linearLayout);
                }
//                setUpQualificationData(fragmentActivity,regional,qualificationArea.corpStructures.name,null);
            }
        });

        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (qualificationArea.isSelected()){
                    qualificationArea.setSelected(false);
                    Hide(contianer,linearLayout);
                }else {
                    qualificationArea.setSelected(true);
                    Show(contianer,linearLayout);
                }

            }
        });


        if (qualificationArea.isSelected()){
            Show(contianer,linearLayout);
        }else {
            Hide(contianer,linearLayout);
        }

        container.addView(contentView);
    }


    private void getDistrictView(LinearLayout container, final Corp_District qualificationDistrict,
                                 ViewGroup parent, final Corp_Regional regional){

        View contentView = LayoutInflater.from(context).inflate(R.layout.qualificaton_district_level, parent, false);
        TextView name = contentView.findViewById(R.id.name);
        ImageView icon   = contentView.findViewById(R.id.icon);
        final LinearLayout linearLayout = contentView.findViewById(R.id.linearLayout);
        final LinearLayout contianer = contentView.findViewById(R.id.container);
        TextView count = contentView.findViewById(R.id.childcount);

        int counter = 0;
        boolean isCompany = false;

        for (CustomSite site : qualificationDistrict.getSites()){
            if (site.getSite().Chain==null){
                isCompany = true;
            }else {
                if (site.getSite().Chain.equalsIgnoreCase("null")){
                    isCompany = true;
                }
            }
            counter++;
        }

        for (CustomSite site : qualificationDistrict.getSites()){
            getSiteView(contianer,site,parent,regional);
        }

        count.setText(String.valueOf(counter));
        name.setText(qualificationDistrict.corpStructures.name);

        if (isCompany){
            GlideApp.with(context).load(R.drawable.company_area).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
        }else {
            GlideApp.with(context).load(R.drawable.franchise_area).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
        }

        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (qualificationDistrict.isSelected()){
                    qualificationDistrict.setSelected(false);
                    Hide(contianer,linearLayout);
                }else {
                    qualificationDistrict.setSelected(true);
                    Show(contianer,linearLayout);
                }
//                setUpQualificationData(fragmentActivity,regional,qualificationDistrict.getCorpStructures().name,null);
            }
        });


        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (qualificationDistrict.isSelected()){
                    qualificationDistrict.setSelected(false);
                    Hide(contianer,linearLayout);
                }else {
                    qualificationDistrict.setSelected(true);
                    Show(contianer,linearLayout);
                }
            }
        });

        if (qualificationDistrict.isSelected()){
            Show(contianer,linearLayout);
        }else {
            Hide(contianer,linearLayout);
        }




        container.addView(contentView);
    }

    private void getSiteView(LinearLayout container, final CustomSite site, ViewGroup parent,
                             final Corp_Regional regional){


        View contentView = LayoutInflater.from(context).inflate(R.layout.qualificaton_site_level, parent, false);
        TextView name = contentView.findViewById(R.id.name);
        ImageView icon   = contentView.findViewById(R.id.icon);
        TextView address = contentView.findViewById(R.id.address);
        contentView.setBackgroundResource(R.drawable.onpress_design);

        if (site.getSite()!=null){

            if (site.getSite().os.equalsIgnoreCase("Custom")){

                GlideApp.with(context).load(R.drawable.store_light_blue).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);

            }else {
                if (site.getSite().AlarmSeverity == 0 ){
                    GlideApp.with(context).load(R.drawable.store_blue).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
                }
                else if (site.getSite().AlarmSeverity == 3 ){
                    GlideApp.with(context).load(R.drawable.store_green).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
                }
                else if (site.getSite().AlarmSeverity == 5 ){
                    GlideApp.with(context).load(R.drawable.store_yellow).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
                }
                else if (site.getSite().AlarmSeverity == 9 ){
                    GlideApp.with(context).load(R.drawable.store_red).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
                }
                else {
                    GlideApp.with(context).load(R.drawable.store_gray).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(icon);
                }
            }

        }else {
            GlideApp.with(context).load(R.drawable.store_gray).into(icon);
        }


        address.setText(SCMTool.getCompleteAddress(site.getSite()));
        name.setText(site.getSite().SiteName);

        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setUpQualificationData(fragmentActivity,regional,site.getSite().sitename,site);

            }
        });



        container.addView(contentView);
    }

    private void setUpQualificationData(FragmentActivity fragmentActivity, Corp_Regional regionals,String selectedName,CustomSite site) {
        QualificationData data = new QualificationData();
        data.setSelectedItem(selectedName);
        if (site!=null){
            data.setSelectedSite(site);
        }
        data.setRegional(regionals);
        data.setOwnerShipViewEnabled(true);
        QualificationData.setInstance(data);
//        Intent intent = new Intent(fragmentActivity, QualificationActivity.class);
//        fragmentActivity.startActivity(intent);
        fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.resturant_fragment_container,
                new QualificationActivityFragment()).commit();
    }

    private void Hide(LinearLayout container,LinearLayout indicator){
        container.setVisibility(View.GONE);
        indicator.setRotation(0);
    }

    private void Show(LinearLayout container,LinearLayout indicator){
        container.setVisibility(View.VISIBLE);
        indicator.setRotation(180);
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }


}
