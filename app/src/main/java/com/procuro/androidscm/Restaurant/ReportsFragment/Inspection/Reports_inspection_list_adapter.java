package com.procuro.androidscm.Restaurant.ReportsFragment.Inspection;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsData;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsDateChildList;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsDateGrandChildList;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsInspectionType;
import com.procuro.androidscm.Restaurant.ReportsFragment.ReportsParentList;
import com.procuro.androidscm.Restaurant.Schema.Days;
import com.procuro.androidscm.Restaurant.Schema.FormItems;
import com.procuro.androidscm.Restaurant.Schema.Forms;
import com.procuro.androidscm.Restaurant.Schema.Schema;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.ApplicationReport;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmthriftservices.services.NamedServices;
import com.procuro.apimmthriftservices.services.auditForm.FSLDataActionItem;
import com.procuro.apimmthriftservices.services.auditForm.FSLDataAttachment;
import com.procuro.apimmthriftservices.services.auditForm.FSLDataFSLFormsClient;
import com.procuro.apimmthriftservices.services.auditForm.FSLDataForm;
import com.procuro.apimmthriftservices.services.auditForm.FSLDataItem;
import com.procuro.apimmthriftservices.services.auditForm.FSLDataNote;
import com.procuro.apimmthriftservices.services.auditForm.FSLDataReading;

import org.apache.thrift.TException;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;


public class Reports_inspection_list_adapter extends BaseExpandableListAdapter {

    private final Context context ;
    private ArrayList<ReportsParentList> parent_lists;
    private FragmentActivity fragmentActivity;
    private ExpandableListView listView;



    public Reports_inspection_list_adapter(Context context, ArrayList<ReportsParentList> data, FragmentActivity fragmentActivity, ExpandableListView listView) {
        this.context = context;
        this.parent_lists = new ArrayList<>();
        this.parent_lists = data;
        this.fragmentActivity = fragmentActivity;
        this.listView = listView;
    }
    @Override
    public void onGroupExpanded(int i) {

    }
    @Override
    public void onGroupCollapsed(int i){

    }
    @Override
    public boolean isEmpty() {
            return false;
    }

    @Override
    public int getGroupCount() {
        int size = 0;
        if (SCMDataManager.getInstance().getReportLevel().equalsIgnoreCase("site")){
            size = parent_lists.size();
        }else {
            size = 1;
        }
        return size ;
    }

    @Override
    public int getChildrenCount(int i) {
        int  size;
        if (i == 0){
            size  = parent_lists.get(i).getApplicationReports().size();
        }else {
            size = parent_lists.get(i).getWeeklyArrayList().size();
        }
        return size;
    }

    @Override
    public Object getGroup(int i) {
        return parent_lists.get(i);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return parent_lists.get(groupPosition).getWeeklyArrayList().get(childPosition);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int position, boolean b, View contentView, ViewGroup parent) {

        ReportsParentList parent_data = parent_lists.get(position);
            contentView = LayoutInflater.from(context).inflate(R.layout.status_journal_parent_data, parent, false);
            TextView textView = contentView.findViewById(R.id.rowParentText);
            textView.setText(parent_data.getTitle());


            return contentView;
    }


    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean b, View contentView, final ViewGroup parent) {

        if (groupPosition ==0){
            ApplicationReport applicationReport = parent_lists.get(groupPosition).getApplicationReports().get(childPosition);
            contentView= LayoutInflater.from(context).inflate(R.layout.reports_daily, parent, false);

            DisplayInspectionPerformance(contentView,applicationReport,parent);

        }

        else {
            contentView = LayoutInflater.from(context).inflate(R.layout.reports_weekly, parent, false);
            ReportsDateChildList child_data = parent_lists.get(groupPosition).getWeeklyArrayList().get(childPosition);
            DisplayDetailInspectionReport(contentView,child_data,parent,groupPosition,childPosition);

        }

        return contentView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }


    private void DisplayInspectionPerformance(View contentView, final ApplicationReport applicationReport,ViewGroup parent){

        TextView title = contentView.findViewById(R.id.title);
        title.setText(applicationReport.ReportName);
        contentView.setBackgroundResource(R.drawable.onpress_design);

        if (applicationReport.UrlTemplate.equalsIgnoreCase("#")){
            contentView.setEnabled(false);
            contentView.setAlpha(.5f);
        }else {
            contentView.setAlpha(1f);
            contentView.setEnabled(true);
        }

        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SCMDataManager.getInstance().setSelectedApplicationReport(applicationReport);
                DisplayApplicationReport(fragmentActivity,applicationReport);                }
        });

    }


    private void DisplayDetailInspectionReport(View contentView,final ReportsDateChildList child_data, ViewGroup parent,
                                               final int groupPosition,int childPosition){

        final TextView textView = contentView.findViewById(R.id.title);
        final LinearLayout LastlevelContainer = contentView.findViewById(R.id.last_level_container);
        final ImageView indicator = contentView.findViewById(R.id.arrow_indicator);


        final ArrayList<View>last_view = new ArrayList<>();
        final ArrayList<ReportsDateGrandChildList>dailies = child_data.getDailyArrayList();
        textView.setText(child_data.getTitle());

        boolean weeklyActive = false;
        if (dailies.size()>0){
            for (int i = 0; i < dailies.size(); i++) {
                ReportsDateGrandChildList daily =dailies.get(i);
                if (daily.isEnabled()){
                    weeklyActive = true;
                    break;
                }
            }
        }
        if (dailies.size()>0){
            for (int i = 0; i < dailies.size(); i++) {
                ReportsDateGrandChildList daily =dailies.get(i);
                getDaily(daily,child_data,parent,last_view,LastlevelContainer,i,weeklyActive);
            }
        }

        if (child_data.isSelected()){
            LastlevelContainer.setVisibility(View.VISIBLE);
            indicator.setRotation(180f);
        }else {
            LastlevelContainer.setVisibility(View.GONE);
        }

        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (child_data.isSelected()){
                    child_data.setSelected(false);
                }else {
                    for (ReportsDateChildList childList:parent_lists.get(groupPosition).getWeeklyArrayList()){
                        childList.setSelected(false);
                    }
                    child_data.setSelected(true);
                }
                notifyDataSetChanged();
            }
        });

    }


    private void getDaily(final ReportsDateGrandChildList daily, final ReportsDateChildList child_data, ViewGroup parent,
                          ArrayList<View>last_view , LinearLayout LastlevelContainer,int position,boolean weeklyActive){

        View lastview = LayoutInflater.from(context).inflate(R.layout.reports_daily, parent, false);
        lastview.setBackgroundResource(R.drawable.onpress_design);
        final TextView title = lastview.findViewById(R.id.title);
        title.setText(daily.getTitle());



        if (position == 0){
            if (!weeklyActive){
               SCMTool.DisableView(lastview,.5f);
            }
            lastview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SCMDataManager.getInstance().setSelectedReportsDateGrandChild(daily);
                    SCMDataManager.getInstance().setSeletedReportsDateChildList(child_data);
                    ClearReportsInstance(SCMDataManager.getInstance().getSelectedSite());
                    InitializedWeeklyForm(child_data);
                    DisplayWeekly(fragmentActivity);
                }
            });

        }else {
            if (!daily.isEnabled()){
                lastview.setEnabled(false);
                lastview.setAlpha(.5f);
            }
            lastview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SCMDataManager.getInstance().setSelectedReportsDateGrandChild(daily);
                    SCMDataManager.getInstance().setSeletedReportsDateChildList(child_data);
                    ClearReportsInstance(SCMDataManager.getInstance().getSelectedSite());
                    ReportsData.getInstance().setCurrentInspectionType(getChecklistFormByDate(daily.getDate()));
                    DisplayDaily(fragmentActivity,daily,child_data);

                }
            });

        }
        LastlevelContainer.addView(lastview);
        last_view.add(lastview);
    }

    public static void ClearReportsInstance(CustomSite customSite){
        ReportsData OldData = ReportsData.getInstance();
        ReportsData newData = new ReportsData();
        newData.setStartDate(OldData.getStartDate());
        newData.setEndDate(OldData.getEndDate());
        newData.setDetailInspectionReport(OldData.getDetailInspectionReport());
        ReportsData.setInstance(newData);
        ReportsData.getInstance().setSite(customSite);
    }

    private void DisplayWeekly(final FragmentActivity fragmentActivity) {
        fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.resturant_fragment_container,
                new Reports_right_pane_weekly_fragment()).commit();
    }

    public static void DisplayDaily(FragmentActivity fragmentActivity,ReportsDateGrandChildList grandChildList,ReportsDateChildList childList) {
        fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.resturant_fragment_container,
                new Reports_right_pane_daily_fragment()).commit();
    }

    private void DisplayApplicationReport(FragmentActivity fragmentActivity,ApplicationReport applicationReport) {
        fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.resturant_fragment_container,
                new Reports_Inspection_RuReport()).commit();
    }


    private void InitializedWeeklyForm(final ReportsDateChildList child_data){

        ArrayList<ReportsInspectionType> reportsInspectionTypes = new ArrayList<>();
        for (int i = 1; i <child_data.getDailyArrayList().size() ; i++) {
            ReportsDateGrandChildList grandChildList = child_data.getDailyArrayList().get(i);
            reportsInspectionTypes.add(getFormsInCurrentDay(grandChildList.getDate()));
        }
        ReportsData reportsData = ReportsData.getInstance();
        reportsData.setReportsInspectionTypes(reportsInspectionTypes);

    }

    public static ReportsInspectionType  getFormsInCurrentDay(Date date){

        SimpleDateFormat format = new SimpleDateFormat("EEE");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        ArrayList<String>ChecklistSections = new ArrayList<>();
        ArrayList<String>FoodSafetySections = new ArrayList<>();
        ArrayList<Days> foodsafety = new ArrayList<>();
        ArrayList<Days> checklist = new ArrayList<>();

        for (Days days :Schema.getInstance().getDays()) {

            if (format.format(date).equalsIgnoreCase(days.day)){

                for (Forms form : Schema.getInstance().getForms()){

                    if (form.formName.equalsIgnoreCase(days.formName)){

                        if (days.section.contains("Cleaning Tasks")){

                        }else if (days.section.contains("Operations")){

                        }else if (days.section.contains("_Audit")){

                        }else if (days.section.contains("Checklist")){

                            if (!ChecklistSections.contains(days.section)) {
                                ChecklistSections.add(days.section);
                                days.setForms(new ArrayList<Forms>());
                                form.setDayparts(days.dayparts);
                                days.getForms().add(form);
                                checklist.add(days);
                            } else {
                                for (Days days1 : checklist){
                                    if (days.section.equalsIgnoreCase(days1.section)){
                                        form.setDayparts(days.dayparts);
                                        days1.getForms().add(form);
                                    }
                                }
                            }

                        }else {
                            if (!FoodSafetySections.contains(days.section)) {
                                FoodSafetySections.add(days.section);
                                days.setForms(new ArrayList<Forms>());
                                form.setDayparts(days.dayparts);
                                days.getForms().add(form);
                                foodsafety.add(days);

                            } else {
                                for (Days days1 : foodsafety){
                                    if (days.section.equalsIgnoreCase(days1.section)){
                                        form.setDayparts(days.dayparts);
                                        days1.getForms().add(form);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

//        reportsInspectionTypes.add(new ReportsInspectionType(date,foodsafety,checklist));

        return new ReportsInspectionType(date,foodsafety,checklist);

    }

    public static ReportsInspectionType  getChecklistFormByDate(Date date){

        SimpleDateFormat format = new SimpleDateFormat("EEE");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        ArrayList<String>ChecklistSections = new ArrayList<>();
        ArrayList<Days> checklists = new ArrayList<>();

        for (Days days :Schema.getInstance().getDays()) {

            if (format.format(date).equalsIgnoreCase(days.day)){

                for (Forms form : Schema.getInstance().getForms()){

                    if (form.formName.equalsIgnoreCase(days.formName)){

                        if (days.section.contains("Checklist")){
                            if (!ChecklistSections.contains(days.section)) {
                                ChecklistSections.add(days.section);
                                days.setForms(new ArrayList<Forms>());
                                form.setDayparts(days.dayparts);
                                days.getForms().add(form);
                                checklists.add(days);
                            } else {
                                for (Days days1 : checklists){
                                    if (days.section.equalsIgnoreCase(days1.section)){
                                        form.setDayparts(days.dayparts);
                                        days1.getForms().add(form);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


//        reportsInspectionTypes.add(new ReportsInspectionType(date,foodsafety,checklist));

        return new ReportsInspectionType(date,new ArrayList<Days>(),checklists);

    }



}