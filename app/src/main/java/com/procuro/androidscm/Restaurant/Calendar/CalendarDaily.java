package com.procuro.androidscm.Restaurant.Calendar;

import java.util.Date;

public class CalendarDaily {
    Date date;
    String EventTitle;
    int EventIcon;

    public CalendarDaily(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getEventTitle() {
        return EventTitle;
    }

    public void setEventTitle(String eventTitle) {
        EventTitle = eventTitle;
    }

    public int getEventIcon() {
        return EventIcon;
    }

    public void setEventIcon(int eventIcon) {
        EventIcon = eventIcon;
    }
}
