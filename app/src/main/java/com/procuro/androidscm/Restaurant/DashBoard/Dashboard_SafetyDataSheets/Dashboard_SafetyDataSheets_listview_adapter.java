package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_SafetyDataSheets;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

import com.procuro.androidscm.GlideApp;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_PdfView_DialogFragment;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourcesChildList;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourcesGrandChildList;
import com.procuro.apimmdatamanagerlib.DocumentDTO;

import java.util.ArrayList;


public class Dashboard_SafetyDataSheets_listview_adapter extends BaseExpandableListAdapter {

    private final Context context ;
    private ArrayList<ResourcesChildList> safetydata;
    private FragmentActivity fragmentActivity;

    public Dashboard_SafetyDataSheets_listview_adapter(Context context, ArrayList<ResourcesChildList> safetydata , FragmentActivity fragmentActivity) {
        this.context = context;
        this.safetydata = safetydata;
        this.fragmentActivity = fragmentActivity;

    }

    @Override
    public int getGroupCount() {
        return safetydata.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return safetydata.get(i).getGrandChildLists().size();
    }

    @Override
    public Object getGroup(int i) {
        return safetydata.get(i);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return safetydata.get(groupPosition).getGrandChildLists().get(childPosition);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int position, boolean b, View contentView, ViewGroup parent) {

        ResourcesChildList data = safetydata.get(position);
        contentView = LayoutInflater.from(context).inflate(R.layout.dashboard_training_category_level, parent,false);

        TextView name = contentView.findViewById(R.id.name);
        TextView childcount = contentView.findViewById(R.id.childcount);

        name.setText(data.getAppString());

        if (data.getGrandChildLists()!=null){
            childcount.setText(String.valueOf(data.getGrandChildLists().size()));
        }else {
            childcount.setText("0");
        }

        return contentView;
    }


    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean b, View contentView, final ViewGroup parent) {
        final ResourcesGrandChildList item = safetydata.get(groupPosition).getGrandChildLists().get(childPosition);
        contentView = LayoutInflater.from(context).inflate(R.layout.dashboard_safety_datasheets_child_data, parent, false);
        TextView name = contentView.findViewById(R.id.title);
        ImageView icon = contentView.findViewById(R.id.icon);



        name.setText(item.getDocumentDTO().name);

        if (item.getDocumentDTO().docType.toLowerCase().contains("pdf")){
            GlideApp.with(context).asDrawable().load(R.drawable.pdf_icon).into(icon);
        }else {
            GlideApp.with(context).asDrawable().load(R.drawable.video_player).into(icon);
        }

        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplaySiteListPopup(item.getDocumentDTO());
            }
        });

        return contentView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }


    private void DisplaySiteListPopup(DocumentDTO dto){
        DialogFragment newFragment = Dashboard_PdfView_DialogFragment.newInstance(dto);
        assert fragmentActivity.getFragmentManager() != null;
        newFragment.show(fragmentActivity.getSupportFragmentManager(), "dialog");
    }
}