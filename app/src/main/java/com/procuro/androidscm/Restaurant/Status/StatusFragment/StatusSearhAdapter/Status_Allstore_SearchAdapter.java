package com.procuro.androidscm.Restaurant.Status.StatusFragment.StatusSearhAdapter;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ExpandableListView;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.procuro.androidscm.GlideApp;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSensor;
import com.procuro.androidscm.Restaurant.Qualification.QualificationSearchAdapters.SearchDetails;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.Status.StatusActivity;
import com.procuro.androidscm.Restaurant.Status.StatusActivityFragment;
import com.procuro.androidscm.Restaurant.Status.StatusData;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.CustomPimmInstance;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.StatusChain;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.StatusFragment;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.Status_All_Store_List_adapter;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.PimmInstance;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Status_Allstore_SearchAdapter extends ArrayAdapter<SearchDetails> {


    private AutoCompleteTextView autoCompleteTextView;
    private ExpandableListView expandableListView;
    private FragmentActivity fragmentActivity;

    private ArrayList<StatusChain> chains ;
    private ArrayList<StatusChain>SelectedAllStore = new ArrayList<>();
    private boolean populated = false;
    public static ProgressDialog progressDialog ;

    private com.procuro.apimmdatamanagerlib.aPimmDataManager dataManager;
    private Context context;
    private ViewGroup parent;

    public Status_Allstore_SearchAdapter(@NonNull Context context, @NonNull ArrayList<StatusChain> chains,
                                         AutoCompleteTextView autoCompleteTextView,
                                         ExpandableListView expandableListView, FragmentActivity fragmentActivity) {
        super(context, 0, new ArrayList<SearchDetails>());
        this.chains = new ArrayList<>(chains);
        this.autoCompleteTextView = autoCompleteTextView;
        this.expandableListView = expandableListView;
        this.fragmentActivity = fragmentActivity;
        progressDialog = new  ProgressDialog(getContext());
        this.context = context;

    }


    @Nullable
    @Override
    public SearchDetails getItem(int position) {
        return super.getItem(position);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return ChainFilter;
    }

    @SuppressLint("ViewHolder")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        convertView = LayoutInflater.from(getContext()).inflate(
                R.layout.drop_down_view, parent, false);

        final SearchDetails searchDetails = getItem(position);
        TextView category_name = convertView.findViewById(R.id.category_name);
        TextView description = convertView.findViewById(R.id.description);
        String title = "";
        this.parent = parent;


        category_name.setText(searchDetails.getName());
        description.setText(searchDetails.getDescription());


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                populated = true;
                hide();
                autoCompleteTextView.setText(searchDetails.getName());
                autoCompleteTextView.dismissDropDown();

                Status_All_Store_List_adapter adapter = new Status_All_Store_List_adapter(getContext(),getSeclectedChain(searchDetails),fragmentActivity,false);
                expandableListView.setAdapter(adapter);

                for (int i = 0; i <adapter.getGroupCount() ; i++) {
                    expandableListView.expandGroup(i);
                }
            }
        });

        return convertView;
    }



    @Override
    public int getPosition(@Nullable SearchDetails item) {
        return super.getPosition(item);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    private Filter ChainFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();
            ArrayList<SearchDetails> searchDetails = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {

                if (populated){
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {

                            Status_All_Store_List_adapter adapter = new Status_All_Store_List_adapter(getContext(),chains,fragmentActivity,false);
                            expandableListView.setAdapter(adapter);
                            for (int i = 0; i <adapter.getGroupCount() ; i++) {
                                expandableListView.expandGroup(i);
                            }
                        }
                    });
                    populated = false;
                }

            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                SearchChain(chains,searchDetails,filterPattern);
            }

            results.values = searchDetails;
            results.count = searchDetails.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            addAll((ArrayList<SearchDetails>) results.values);
            notifyDataSetChanged();
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return super.convertResultToString(resultValue);
        }


    };


    private void SearchChain (ArrayList<StatusChain> chains,ArrayList<SearchDetails>searchDetails,String filterpattern){
        boolean isContain = false;
        if (chains!=null){
            for (StatusChain chain : chains){

                if (chain.getName().toLowerCase().contains(filterpattern)){
                    for (SearchDetails details : searchDetails){
                        if (details.getName().equalsIgnoreCase(chain.getName())){
                            isContain = true;
                            break;
                        }
                    }
                    if (!isContain){
                        searchDetails.add(new SearchDetails(chain.getName(),"Chain"));
                    }

                    SearchStore(chain.getCustomSites(),searchDetails,filterpattern);
                }else {

                    SearchStore(chain.getCustomSites(),searchDetails,filterpattern);
                }
            }
        }
    }

    private void SearchStore (ArrayList<CustomSite>sites , ArrayList<SearchDetails>searchDetails, String filterpattern){
        boolean isContain = false;
        if (sites!=null){
            for (CustomSite site : sites){

                if (site.getSite().sitename.toLowerCase().contains(filterpattern)) {
                    for (SearchDetails details : searchDetails) {
                        if (details.getName().equalsIgnoreCase(site.getSite().sitename)) {
                            isContain = true;
                            break;
                        }
                    }
                    if (!isContain) {
                        searchDetails.add(new SearchDetails(site.getSite().sitename, "Store"));
                    }
                }
            }
        }
    }

    private ArrayList<StatusChain> getSeclectedChain(SearchDetails details){

        SelectedAllStore= new ArrayList<>();
        ArrayList<StatusChain>allStoreChain = new ArrayList<>(chains);

        try {
            for (StatusChain chain : allStoreChain){
                if (chain.getName().equalsIgnoreCase(details.getName())){
                    SelectedAllStore.add(chain);
                }else {

                    StatusChain chain1 = new StatusChain();
                    chain1.setName(chain.getName());
                    chain1.setCustomSites(getSelectedStore(chain.getCustomSites(),details));
                    if (chain1.getCustomSites().size()>0){
                        SelectedAllStore.add(chain1);
                    }
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }


        return SelectedAllStore;
    }

    private ArrayList<CustomSite> getSelectedStore(ArrayList<CustomSite>sites,SearchDetails details){
        ArrayList<CustomSite>SelectedSite = new ArrayList<>();
        ArrayList<CustomSite>CustomSite = new ArrayList<>(sites);
        try {
            for (CustomSite site : CustomSite){
                if (site.getSite().sitename.equalsIgnoreCase(details.getName())){
//                    SensorCategoryView(site);
                    SelectedSite.add(site);
                    break;
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return SelectedSite;
    }

    public void hide() {
        View view = fragmentActivity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) fragmentActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void SensorCategoryView(final CustomSite site){
//        final ProgressDialog progressDialog = new ProgressDialog(getContext());
//        progressDialog.setCancelable(true);
//        progressDialog.setTitle("Please Wait");
//        progressDialog.setMessage("Downloading Data");
//        progressDialog.show();

//        dataManager = aPimmDataManager.getInstance();
//        if (site.getPimmDevice()!=null) {
//            dataManager.getInstanceListForSiteId(site.getSite().DeviceID, new OnCompleteListeners.getInstanceListForSiteIdCallbackListener() {
//                @Override
//                public void getInstanceListForSiteIdCallback(ArrayList<PimmInstance> pimmInstanceArrayList, Error error) {
//                    if (error == null) {
//                        if (pimmInstanceArrayList.size() > 0) {
//                            FilterObjectNames(pimmInstanceArrayList,site);
//                            progressDialog.dismiss();
//                        }else {
//                            setInstanceForNoTemperature(site);
//                            progressDialog.dismiss();
//                        }
//                    }else {
//                        setInstanceForNoTemperature(site);
//                        progressDialog.dismiss();
//                    }
//                }
//            });
//        }
    }

    private void FilterObjectNames(ArrayList<PimmInstance> temperatureLists,CustomSite customSite){
        ArrayList<String>objectname = new ArrayList<>();
        ArrayList<CustomSensor>objects=  new ArrayList<>();
        for (PimmInstance newObjects: temperatureLists){
            if (!objectname.contains(newObjects.objectName)){
                objectname.add(newObjects.objectName);
                objects.add(new CustomSensor(newObjects.objectName));
                System.out.println("Description: "+newObjects.description);
            }
        }

        PopulateInstanceByObjectName(temperatureLists,objects,customSite);
    }

    private void PopulateInstanceByObjectName(ArrayList<PimmInstance>pimmInstances,
                                              ArrayList<CustomSensor>newObjects,CustomSite customSite){
        for (PimmInstance instance: pimmInstances){
            for (CustomSensor newObject: newObjects){
                if (instance.objectName.equalsIgnoreCase(newObject.getTitle())){
                    if (newObject.getObject() == null){
                        ArrayList<PimmInstance>newObs = new ArrayList<>();
                        newObs.add(instance);
                        newObject.setObject(newObs);
                    }else {
                        newObject.getObject().add(instance);
                    }
                }
            }
        }
        PopulateInstanceByCoolsersAndFreezers(newObjects,customSite);
    }

    private void PopulateInstanceByCoolsersAndFreezers(ArrayList<CustomSensor>sensors,CustomSite customSite){

        for (CustomSensor sensor : sensors){
            if (sensor.getTitle().equalsIgnoreCase("Temperature")) {

                ArrayList<CustomPimmInstance> customPimmInstances = new ArrayList<>();
                ArrayList<String> titles = new ArrayList<>();
                if (sensor.getObject() != null) {
                    for (PimmInstance instance : sensor.getObject()) {
                        if (instance.description.toLowerCase().contains("cooler")) {
                            if (!titles.contains("Coolers")) {
                                titles.add("Coolers");
                                ArrayList<PimmInstance> instances = new ArrayList<>();
                                instances.add(instance);
                                customPimmInstances.add(new CustomPimmInstance("Coolers", instances));

                            } else {
                                for (CustomPimmInstance pimmInstance : customPimmInstances) {
                                    if (pimmInstance.getTitle().equalsIgnoreCase("Coolers")) {
                                        pimmInstance.getInstances().add(instance);
                                    }
                                }
                            }
                        } else if (instance.description.toLowerCase().contains("freezer")) {

                            if (!titles.contains("Freezers")) {
                                titles.add("Freezers");
                                ArrayList<PimmInstance> instances = new ArrayList<>();
                                instances.add(instance);
                                customPimmInstances.add(new CustomPimmInstance("Freezers", instances));


                            } else {
                                for (CustomPimmInstance pimmInstance : customPimmInstances) {
                                    if (pimmInstance.getTitle().equalsIgnoreCase("Freezers")) {
                                        pimmInstance.getInstances().add(instance);
                                        System.out.println("Freezer: "+instance.description);
                                    }
                                }
                            }
                        }
                    }
                }


                Collections.sort(customPimmInstances, new Comparator<CustomPimmInstance>() {
                    @Override
                    public int compare(CustomPimmInstance o1, CustomPimmInstance o2) {
                        return o1.getTitle().compareToIgnoreCase(o2.getTitle());
                    }
                });

                for (CustomPimmInstance pimmInstance : customPimmInstances){
                    getTemperatureLevel(pimmInstance,sensors,customSite);
                    break;
                }
            }
        }
    }

    private void getTemperatureLevel(final CustomPimmInstance pimmInstance, ArrayList<CustomSensor>customSensors,CustomSite customSite){

        if (pimmInstance.getInstances()!=null){
            for (PimmInstance sensors :  pimmInstance.getInstances()){
                setInstanceForTemperature(customSite,sensors);
                break;
            }
        }
    }

    private void setInstanceForTemperature(CustomSite site, PimmInstance pimmInstance){

        StatusData data = new StatusData();
        data.setSelectedSite(site);
        data.setAllStoreEnabled(true);
        data.setSelectedSite(site);
        data.setPimmDevice(site.getPimmDevice());
        data.setPimmInstance(pimmInstance);
        data.setTemperatureViewEnabled(true);
        StatusData.setInstance(data);
//        Intent intent = new Intent(fragmentActivity, StatusActivity.class);
//        fragmentActivity.startActivity(intent);
        fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.resturant_fragment_container,
                new StatusActivityFragment()).commit();

    }

    private void setInstanceForNoTemperature(CustomSite site){

        StatusData data = new StatusData();
        data.setSelectedSite(site);
        data.setAllStoreEnabled(true);
        data.setSelectedSite(site);
        data.setPimmDevice(site.getPimmDevice());
        data.setTemperatureViewEnabled(true);
        StatusData.setInstance(data);
//        Intent intent = new Intent(fragmentActivity, StatusActivity.class);
//        fragmentActivity.startActivity(intent);
        fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.resturant_fragment_container,
                new StatusActivityFragment()).commit();
    }




}
