package com.procuro.androidscm.Restaurant.Qualification.UserProfile;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.GlideApp;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.Qualification.QualificationActivity;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Area;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_District;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Division;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Regional;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.CorpUser;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class Qualification_UserProfile_Regional_Child_level_Fragment extends Fragment {


    private CircleImageView profilePicture;
    public static TextView category_name,category,category_child_title,name,position,number,email,message;
    private Button back;
    public static RecyclerView recyclerView;

    public static Corp_Regional regional;
    public static Corp_Division division;
    public static Corp_District district;
    public static Corp_Area area;
    public static CustomSite customSite;
    private Bundle backBundle;
    private Bundle frontBundle;
    public static ProgressDialog progressDialog;
    public static com.procuro.apimmdatamanagerlib.aPimmDataManager dataManager = aPimmDataManager.getInstance();


    public Qualification_UserProfile_Regional_Child_level_Fragment() {

        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.qualification_user_profile_region_child_level, container, false);

        profilePicture = view.findViewById(R.id.circleImageView);
        category_name = view.findViewById(R.id.textView10);
        category = view.findViewById(R.id.category);
        name = view.findViewById(R.id.name);
        position = view.findViewById(R.id.position);
        number = view.findViewById(R.id.number);
        email = view.findViewById(R.id.email);
        back = view.findViewById(R.id.back);
        recyclerView = view.findViewById(R.id.recyclerView);
        category_child_title = view.findViewById(R.id.category_child);
        message = view.findViewById(R.id.message);


        getarguments();

        return view;

    }

    private void getarguments(){

        if (QualificationData.getInstance().isOwnerShipViewEnabled()) {

            final Corp_Regional regional = QualificationData.getInstance().getRegional();
            if (regional!=null){
                if (regional.getCorpStructures().name.equalsIgnoreCase(QualificationData.getInstance().getSelectedItem())){
                    this.regional = regional;
                    QualificationActivity.storeProfile.setEnabled(false);
                    QualificationActivity.storeProfile.setAlpha(.5f);
                    QualificationActivity.scoreCard.setEnabled(false);
                    QualificationActivity.scoreCard.setAlpha(.5f);
                    QualificationActivity.name.setText(this.regional.getCorpStructures().name);
                    DisplayRegionalView(this.regional);
                    back.setVisibility(View.INVISIBLE);

                }else {

                    if (regional.getQualificationDivisions()!=null){
                        for (final Corp_Division division : regional.getQualificationDivisions()){
                            if (division.getCorpStructures().name.equalsIgnoreCase(QualificationData.getInstance().getSelectedItem())){
                                this.division = division;

                                QualificationActivity.storeProfile.setEnabled(false);
                                QualificationActivity.storeProfile.setAlpha(.5f);
                                QualificationActivity.scoreCard.setEnabled(false);
                                QualificationActivity. scoreCard.setAlpha(.5f);
                                QualificationActivity.name.setText(this.division.getCorpStructures().name);

                                back.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        QualificationData.getInstance().setSelectedItem(regional.getCorpStructures().name);
                                        Fragment fragment = new Qualification_UserProfile_Regional_Child_level_Fragment();
                                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.qualification_store_profile,
                                                fragment).commit();
                                    }
                                });

                                DisplayDivisionView(this.division);
                                break;

                            }else {
                                if (division.getQualificationAreas()!=null){

                                    for (final Corp_Area area : division.getQualificationAreas()){

                                        if (area.getCorpStructures().name.equalsIgnoreCase(QualificationData.getInstance().getSelectedItem())){
                                            this.area = area;
                                            QualificationActivity.storeProfile.setEnabled(false);
                                            QualificationActivity.storeProfile.setAlpha(.5f);
                                            QualificationActivity.scoreCard.setEnabled(false);
                                            QualificationActivity. scoreCard.setAlpha(.5f);
                                            QualificationActivity. name.setText(this.area.getCorpStructures().name);
                                            back.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    QualificationData.getInstance().setSelectedItem(division.getCorpStructures().name);
                                                    Fragment fragment = new Qualification_UserProfile_Regional_Child_level_Fragment();
                                                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.qualification_store_profile,
                                                            fragment).commit();
                                                }
                                            });

                                            DisplayAreaView(this.area);
                                            break;

                                        }else {

                                            if (area.getQualificationDistricts()!=null){

                                                for (final Corp_District district : area.getQualificationDistricts()){

                                                    if (district.getCorpStructures().name.equalsIgnoreCase(QualificationData.getInstance().getSelectedItem())){
                                                        this.district = district;
                                                        QualificationActivity.storeProfile.setEnabled(false);
                                                        QualificationActivity.storeProfile.setAlpha(.5f);
                                                        QualificationActivity.scoreCard.setEnabled(false);
                                                        QualificationActivity. scoreCard.setAlpha(.5f);
                                                        QualificationActivity. name.setText(this.district.getCorpStructures().name);
                                                        back.setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View v) {
                                                                QualificationData.getInstance().setSelectedItem(area.getCorpStructures().name);
                                                                Fragment fragment = new Qualification_UserProfile_Regional_Child_level_Fragment();
                                                                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.qualification_store_profile,
                                                                        fragment).commit();
                                                            }
                                                        });

                                                        DisplayDistrictView(this.district);
                                                        break;

                                                    }else {
                                                        if (district.getSites()!=null){

                                                            for (CustomSite customSite : district.getSites()){
                                                                if (customSite.getSite().sitename.equalsIgnoreCase(QualificationData.getInstance().getSelectedItem())){
                                                                    Qualification_UserProfile_Regional_Child_level_Fragment.customSite = customSite;
                                                                    QualificationActivity.name.setText(this.customSite.getSite().sitename);
                                                                    QualificationActivity. storeProfile.setEnabled(true);
                                                                    QualificationActivity.storeProfile.setAlpha(1);
                                                                    QualificationActivity.scoreCard.setEnabled(true);
                                                                    QualificationActivity.scoreCard.setAlpha(1);
                                                                    back.setOnClickListener(new View.OnClickListener() {
                                                                        @Override
                                                                        public void onClick(View v) {
                                                                            QualificationData.getInstance().setSelectedItem(district.getCorpStructures().name);
                                                                            QualificationData.getInstance().setUsers(null);
                                                                            Fragment fragment = new Qualification_UserProfile_Regional_Child_level_Fragment();
                                                                            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.qualification_store_profile,
                                                                                    fragment).commit();
                                                                        }
                                                                    });
                                                                    DisplaySiteView(Qualification_UserProfile_Regional_Child_level_Fragment.customSite,getContext(),getActivity());

                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
        else if (QualificationData.getInstance().isStateByStateViewEnabled()){
            if(QualificationData.getInstance().getSelectedSite()!=null){
                customSite = QualificationData.getInstance().getSelectedSite();
                QualificationActivity.name.setText(customSite.getSite().sitename);
                back.setVisibility(View.INVISIBLE);
                DisplaySiteView(Qualification_UserProfile_Regional_Child_level_Fragment.customSite,getContext(),getActivity());

            }
        }
        else {
            customSite = SCMDataManager.getInstance().getSelectedSite();
            QualificationActivity.name.setText(customSite.getSite().sitename);
            back.setVisibility(View.INVISIBLE);
            DisplaySiteView(Qualification_UserProfile_Regional_Child_level_Fragment.customSite,getContext(),getActivity());

        }

    }

    private void DisplayRegionalView(Corp_Regional regional){

        disableStoreProfileAndScoreCard();
        try {
            category_name.setText(regional.getCorpStructures().name);
            category.setText(regional.getCorpStructures().layerName);
            category_child_title.setText("Division");
            CorpUser corpUser = regional.getCorpStructures().corpUser;

            if (corpUser!=null){

                name.setText(corpUser.firstName);
                name.append(" ");
                name.append(corpUser.lastName);

                if (corpUser.firstName.equalsIgnoreCase("Deepak")){
                    if (corpUser.lastName.equalsIgnoreCase("Ajmani")){
                        GlideApp.with(getContext()).asDrawable().load(R.drawable.deepak).into(profilePicture);
                    }
                }else {
                    GlideApp.with(getContext()).asDrawable().load(R.drawable.user).into(profilePicture);
                }

                position.setText(corpUser.jobTitle);

                if (corpUser.phoneNumber!=null){
                    if (!corpUser.phoneNumber.equalsIgnoreCase("null")){
                        number.setText(corpUser.phoneNumber);
                    }
                }

                if (corpUser.userName!=null){
                    if (!corpUser.userName.equalsIgnoreCase("null")){
                        email.setText(corpUser.userName);
                    }
                }

            }else {
                name.setText("Name : Not Available");
                position.setText("Position: Not Available");
            }

            Qualification_DIvision_RecyclerViewAdapter adapter = new Qualification_DIvision_RecyclerViewAdapter(getContext(),regional.getQualificationDivisions(),getActivity());
            recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
            recyclerView.setAdapter(adapter);
            message.setVisibility(View.GONE);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void DisplayDivisionView(Corp_Division division){

        disableStoreProfileAndScoreCard();

        try {
            category_name.setText(division.getCorpStructures().name);
            category.setText(division.getCorpStructures().layerName);
            CorpUser corpUser = division.getCorpStructures().corpUser;

            if (corpUser!=null){

                name.setText(corpUser.firstName);
                name.append(" ");
                name.append(corpUser.lastName);

                position.setText(corpUser.jobTitle);

                if (corpUser.phoneNumber!=null){
                    if (!corpUser.phoneNumber.equalsIgnoreCase("null")){
                        number.setText(corpUser.phoneNumber);
                    }
                }
                if (corpUser.userName!=null){
                    if (!corpUser.userName.equalsIgnoreCase("null")){
                        email.setText(corpUser.userName);
                    }
                }
            }else {
                name.setText("Name : Not Available");
                position.setText("Position: Not Available");
            }

            category_child_title.setText("Area");
            Qualification_Area_RecyclerViewAdapter adapter = new Qualification_Area_RecyclerViewAdapter(getContext(),
                    division.getQualificationAreas(),getActivity());
            recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
            recyclerView.setAdapter(adapter);
            message.setVisibility(View.GONE);
        }catch (Exception e){
            e.printStackTrace();
        }



    }

    private void DisplayAreaView(Corp_Area area){

        disableStoreProfileAndScoreCard();

        try {
            category_name.setText(area.getCorpStructures().name);
            category.setText(area.getCorpStructures().layerName);
            CorpUser corpUser = area.getCorpStructures().corpUser;

            if (corpUser!=null){

                name.setText(corpUser.firstName);
                name.append(" ");
                name.append(corpUser.lastName);

                position.setText(corpUser.jobTitle);

                if (corpUser.phoneNumber!=null){
                    if (!corpUser.phoneNumber.equalsIgnoreCase("null")){
                        number.setText(corpUser.phoneNumber);
                    }
                }

                if (corpUser.userName!=null){
                    if (!corpUser.userName.equalsIgnoreCase("null")){
                        email.setText(corpUser.userName);
                    }
                }

            }else {
                name.setText("Name : Not Available");
                position.setText("Position: Not Available");
            }

            category_child_title.setText("District");
            Qualification_District_RecyclerViewAdapter adapter = new Qualification_District_RecyclerViewAdapter(
                    getContext(),area.getQualificationDistricts(),getActivity(),frontBundle);
            recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
            recyclerView.setAdapter(adapter);
            message.setVisibility(View.GONE);

        }catch (Exception e){
            e.printStackTrace();
        }



    }

    private void DisplayDistrictView(Corp_District district){
        disableStoreProfileAndScoreCard();
        try {
            category_name.setText(district.getCorpStructures().name);
            category.setText(district.getCorpStructures().layerName);
            CorpUser corpUser = district.getCorpStructures().corpUser;

            if (corpUser!=null){

                name.setText(corpUser.firstName);
                name.append(" ");
                name.append(corpUser.lastName);

                position.setText(corpUser.jobTitle);

                if (corpUser.phoneNumber!=null){
                    if (!corpUser.phoneNumber.equalsIgnoreCase("null")){
                        number.setText(corpUser.phoneNumber);
                    }
                }

                if (corpUser.userName!=null){
                    if (!corpUser.userName.equalsIgnoreCase("null")){
                        email.setText(corpUser.userName);
                    }
                }

            }else {
                name.setText("Name : Not Available");
                position.setText("Position: Not Available");
            }

            category_child_title.setText("Site");

            Qualification_CustomSite_RecyclerViewAdapter adapter = new Qualification_CustomSite_RecyclerViewAdapter(
                    getContext(),district.getSites(),getActivity(),frontBundle);
            recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
            recyclerView.setAdapter(adapter);
            message.setVisibility(View.GONE);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void DisplaySiteView(CustomSite customSite,Context context,FragmentActivity activity){
        try {

            enableStoreProfileAndScoreCard();
            category_name.setText(customSite.getSite().sitename);
            category.setText("Store");

            if (QualificationData.getInstance().getUsers()==null){
                getStoreUsers(context,activity);

            }else {
                FilterEmployees(QualificationData.getInstance().getUsers(),context,activity);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public static void getStoreUsers(final Context context, final FragmentActivity activity){
        QualificationData.getInstance().setSelectedSite(customSite);
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Please Wait");
        progressDialog.setMessage("Downloading Data");
        progressDialog.setCancelable(false);
        progressDialog.show();

        dataManager = aPimmDataManager.getInstance();
        dataManager.getStoreUsers(customSite.getSite().siteid, new OnCompleteListeners.getStoreUsersCallbackListener() {
            @Override
            public void getStoreUsersCallback(ArrayList<User> storeUsers, Error error) {
                if (error == null){
                    QualificationData.getInstance().setUsers(SCMTool.ConvertUserToCustomUser(storeUsers));
                    FilterEmployees(QualificationData.getInstance().getUsers(),context,activity);
                    message.setVisibility(View.GONE);
                    progressDialog.cancel();
                }else {
                    progressDialog.cancel();
                    name.setText("Name : Not Available");
                    position.setText("Position : Not Available");
                    category_child_title.setText("No Employee Available");
                }
            }
        });
    }


    public static void FilterEmployees(ArrayList<CustomUser>users, Context context, FragmentActivity activity){
        try {
            ArrayList<CustomUser> ActiveUsers = SCMTool.getActiveCustomUser(users);

            CustomUser manager = null;
            ArrayList<CustomUser>employees = new ArrayList<>();
            if (ActiveUsers.size()>0){
                category_child_title.setText(ActiveUsers.size()+ " Employees");
                for (CustomUser user : ActiveUsers){

                    System.out.println("User : "+user.getUser().firstName);
                    boolean managerExist = false;

                    if (user.getUser().roles!=null){
                        System.out.println("Role : "+user.getUser().roles);
                        for (String role : user.getUser().roles){
                            if (role.toLowerCase().contains("gm")){
                                manager = user;
                                managerExist = true;
                            }else if (role.toLowerCase().contains("manager")){
                                manager = user;
                                managerExist = true;
                            }else if (role.toLowerCase().contains("general manager")){
                                manager = user;
                                managerExist = true;
                            }
                        }
                        if (!managerExist){
                            employees.add(user);
                        }
                    }
                }
                if (manager!=null){
                    name.setText(manager.getUser().firstName);
                    name.append(" ");
                    name.append(manager.getUser().lastName);

                    position.setText("General Manager");
                    if (manager.getUser().cellPhone!=null){
                        if (!manager.getUser().cellPhone.equalsIgnoreCase("null")){
                            number.setText(manager.getUser().cellPhone);
                        }else {
                            number.setText("Not Available");
                        }

                    }
                    if (manager.getUser().contactEmail!=null){
                        if (!manager.getUser().contactEmail.equalsIgnoreCase("null")){
                            email.setText(manager.getUser().contactEmail);
                        }else {
                            email.setText("Not Available");
                        }

                    }
                }else {
                    name.setText("Name : Not Available");
                    position.setText("Position : Not Available");
                }

                Qualification_User_RecyclerViewAdapter adapter = new Qualification_User_RecyclerViewAdapter(context,employees,activity,customSite);
                recyclerView.setLayoutManager(new GridLayoutManager(context,2));
                recyclerView.setAdapter(adapter);
                message.setVisibility(View.GONE);

            }else {
                name.setText("Name : Not Available");
                position.setText("Position : Not Available");
                category_child_title.setText("No Employee Available");
                message.setVisibility(View.VISIBLE);
            }
        }catch (Exception e){
         e.printStackTrace();
        }
    }

    private void disableStoreProfileAndScoreCard(){
        try {
            QualificationActivity.storeProfile.setAlpha(.5f);
            QualificationActivity.storeProfile.setEnabled(false);
            QualificationActivity.scoreCard.setAlpha(.5f);
            QualificationActivity.scoreCard.setEnabled(false);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void enableStoreProfileAndScoreCard(){
        try {
            QualificationActivity.storeProfile.setAlpha(1);
            QualificationActivity.storeProfile.setEnabled(true);
            QualificationActivity.scoreCard.setAlpha(1);
            QualificationActivity.scoreCard.setEnabled(true);

        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
