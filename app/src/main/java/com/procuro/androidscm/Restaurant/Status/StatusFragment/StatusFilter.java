package com.procuro.androidscm.Restaurant.Status.StatusFragment;

public class StatusFilter {

    String name;
    boolean selected;

    public StatusFilter(String title) {
        this.name = title;
    }

    public StatusFilter(String title, boolean selected) {
        this.name = title;
        this.selected = selected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
