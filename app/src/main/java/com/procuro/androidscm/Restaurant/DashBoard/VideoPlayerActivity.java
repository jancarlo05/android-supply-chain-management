package com.procuro.androidscm.Restaurant.DashBoard;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.SCMTool;

public class VideoPlayerActivity extends AppCompatActivity {

    WebView webView;
    ConstraintLayout fullscreen_Toggle;
    LinearLayout header;
    boolean fullscreen = false;
    boolean visibleToggle = false;
    ImageView image_toggle;
    Button back;
    TextView name;
    ProgressBar progressBar;

    @SuppressLint({"ClickableViewAccessibility", "SetJavaScriptEnabled"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);

        webView = findViewById(R.id.webView);
        fullscreen_Toggle = findViewById(R.id.fullscreen_Toggle);
        header = findViewById(R.id.header);
        image_toggle = findViewById(R.id.image_toggle);
        back = findViewById(R.id.home);
        name = findViewById(R.id.title_header);
        progressBar = findViewById(R.id.progresbar);

        webView.setWebViewClient(new mywebviewclient());
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAllowFileAccess(true);
        webSettings.setAppCacheEnabled(true);
        webSettings.setUseWideViewPort(true);

        Bundle bundle = getIntent().getExtras();

        assert bundle != null;
        if(bundle.getString("Url")!= null) {
            webView.loadUrl(bundle.getString("Url"));
            name.setText(bundle.getString("name"));
        }


        int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            header.setVisibility(View.GONE);
            image_toggle.setBackgroundResource(R.drawable.ic_fullscreen_exit_white_48dp);
            fullscreen = true;
            webView.getLayoutParams().height = LinearLayout.LayoutParams.MATCH_PARENT;

        } else {
            header.setVisibility(View.VISIBLE);
            fullscreen = false;
            image_toggle.setBackgroundResource(R.drawable.ic_fullscreen_white_48dp);
            webView.getLayoutParams().height = SCMTool.dpToPx(250);
        }


        setUPOnclicks();


    }

    @SuppressLint("ClickableViewAccessibility")
    private void setUPOnclicks(){

        fullscreen_Toggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TooggleFullscreen();
            }
        });


        webView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ShowToggle();

                return false;
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void TooggleFullscreen(){
        if (!fullscreen){
            EnabledFUllscreen();
            fullscreen = true;
            image_toggle.setBackgroundResource(R.drawable.ic_fullscreen_exit_white_48dp);
            webView.getLayoutParams().height = LinearLayout.LayoutParams.MATCH_PARENT;
        }else {
            DisableFullscreen();
            fullscreen = false;
            image_toggle.setBackgroundResource(R.drawable.ic_fullscreen_white_48dp);
            webView.getLayoutParams().height = SCMTool.dpToPx(250);

        }
    }



    private void EnabledFUllscreen(){
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        header.setVisibility(View.GONE);
        image_toggle.setBackgroundResource(R.drawable.ic_fullscreen_exit_white_48dp);
        fullscreen = true;
        webView.getLayoutParams().height = LinearLayout.LayoutParams.MATCH_PARENT;


    }

    private void DisableFullscreen(){
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        header.setVisibility(View.VISIBLE);
        fullscreen = false;
        image_toggle.setBackgroundResource(R.drawable.ic_fullscreen_white_48dp);
        webView.getLayoutParams().height = SCMTool.dpToPx(250);


    }


    private void ShowToggle(){
        if (!visibleToggle){
            visibleToggle = true;
            fullscreen_Toggle.setVisibility(View.VISIBLE);
            new CountDownTimer(4000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    fullscreen_Toggle.setVisibility(View.GONE);
                    visibleToggle = false;

                }
            }.start();
        }
    }

    private class mywebviewclient extends WebViewClient {
        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);

            System.out.println("Error: "+error.getDescription());
            System.out.println(error.getErrorCode());
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
            ShowToggle();
        }
    }



    @Override
    protected void onDestroy() {
        webView.destroy();
        super.onDestroy();



    }


}
