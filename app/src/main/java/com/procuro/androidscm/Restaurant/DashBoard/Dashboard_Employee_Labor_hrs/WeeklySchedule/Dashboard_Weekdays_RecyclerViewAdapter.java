package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.WeeklySchedule;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;


public class Dashboard_Weekdays_RecyclerViewAdapter extends RecyclerView.Adapter<Dashboard_Weekdays_RecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<Date> arraylist;

    public Dashboard_Weekdays_RecyclerViewAdapter(Context context, ArrayList<Date> arraylist) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.weekdays_list_data,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final Date days = arraylist.get(position);
        SimpleDateFormat day = new SimpleDateFormat("EEE");
        SimpleDateFormat month = new SimpleDateFormat("MM/dd");
        day.setTimeZone(TimeZone.getTimeZone("UTC"));
        month.setTimeZone(TimeZone.getTimeZone("UTC"));

        holder.days.setText(day.format(days));
        holder.month.setText(month.format(days));



    }

    @Override
    public int getItemCount() {
        return arraylist.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView days,month;


        public MyViewHolder(View itemView) {
            super(itemView);
            days = itemView.findViewById(R.id.days);
            month = itemView.findViewById(R.id.month);

        }
    }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    private TextView  CreatePositionBackground(TextView imageView, String backgroundColor) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setCornerRadii(new float[] { dpToPx(5), dpToPx(5), dpToPx(5), dpToPx(5), 0, 0, 0, 0 });
        shape.setColor(Color.parseColor(backgroundColor));
        shape.setStroke(1, Color.BLACK);
        imageView.setBackground(shape);
        return imageView;
    }



}
