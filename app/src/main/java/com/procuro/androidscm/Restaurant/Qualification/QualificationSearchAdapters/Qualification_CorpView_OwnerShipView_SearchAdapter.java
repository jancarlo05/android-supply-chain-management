package com.procuro.androidscm.Restaurant.Qualification.QualificationSearchAdapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ExpandableListView;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Area;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Chain;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_District;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Division;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Regional;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Qualification_Owner_Corporate_List_adapter;
import com.procuro.androidscm.Restaurant.CustomSite;

import java.util.ArrayList;

public class Qualification_CorpView_OwnerShipView_SearchAdapter extends ArrayAdapter<SearchDetails> {

    private ArrayList<Corp_Chain> corpchain ;
    private AutoCompleteTextView autoCompleteTextView;
    private ExpandableListView expandableListView;
    private FragmentActivity fragmentActivity;
    private ArrayList<Corp_Chain>SelectedCorpchain = new ArrayList<>();
    private boolean populated = false;

    public Qualification_CorpView_OwnerShipView_SearchAdapter(@NonNull Context context, @NonNull ArrayList<Corp_Chain> corp_chains,
                                                              AutoCompleteTextView autoCompleteTextView,
                                                              ExpandableListView expandableListView,FragmentActivity fragmentActivity) {
        super(context, 0, new ArrayList<SearchDetails>());
        this.corpchain = new ArrayList<>(corp_chains);
        this.autoCompleteTextView = autoCompleteTextView;
        this.expandableListView = expandableListView;
        this.fragmentActivity = fragmentActivity;

    }


    @Nullable
    @Override
    public SearchDetails getItem(int position) {
        return super.getItem(position);
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return ChainFilter;
    }

    @SuppressLint("ViewHolder")
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        convertView = LayoutInflater.from(getContext()).inflate(
                R.layout.drop_down_view, parent, false);

        final SearchDetails searchDetails = getItem(position);
        TextView category_name = convertView.findViewById(R.id.category_name);
        TextView description = convertView.findViewById(R.id.description);
        String title = "";


        try {

            category_name.setText(searchDetails.getName());
            description.setText(searchDetails.getDescription());


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    populated = true;
                    hide();
                    autoCompleteTextView.setText(searchDetails.getName());
                    autoCompleteTextView.dismissDropDown();
                    Qualification_Owner_Corporate_List_adapter adapter = new Qualification_Owner_Corporate_List_adapter(getContext(),getSelectedChain(searchDetails),fragmentActivity);
                    expandableListView.setAdapter(adapter);

                    for (int i = 0; i <adapter.getGroupCount() ; i++) {
                        expandableListView.expandGroup(i);
                    }


                }
            });



        }catch (Exception e){
            e.printStackTrace();
        }


        return convertView;
    }



    @Override
    public int getPosition(@Nullable SearchDetails item) {
        return super.getPosition(item);
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    private Filter ChainFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();
            ArrayList<SearchDetails> searchDetails = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {

                if (populated){
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            Qualification_Owner_Corporate_List_adapter adapter = new Qualification_Owner_Corporate_List_adapter(getContext(),corpchain,fragmentActivity);
                            expandableListView.setAdapter(adapter);

                            for (int i = 0; i <adapter.getGroupCount() ; i++) {
                                expandableListView.expandGroup(i);
                            }
                        }
                    });
                    populated = false;
                }

            } else {

                boolean isContain = false;

                String filterPattern = constraint.toString().toLowerCase().trim();
                for (Corp_Chain item : corpchain) {
                    if (item.getName().toLowerCase().contains(filterPattern)) {
                        for (SearchDetails details : searchDetails){
                            if (details.getName().equalsIgnoreCase(item.getName())){
                                isContain = true;
                                break;
                            }
                        }
                        if (!isContain){
                            searchDetails.add(new SearchDetails(item.getName(),"Chain"));
                        }

                        SearchRegion(item.getQualificationRegionals(),searchDetails,filterPattern);
                    }else {
                        SearchRegion(item.getQualificationRegionals(),searchDetails,filterPattern);
                    }
                }
            }

            results.values = searchDetails;
            results.count = searchDetails.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            addAll((ArrayList<SearchDetails>) results.values);
            notifyDataSetChanged();
        }

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return super.convertResultToString(resultValue);
        }


    };


    private void SearchRegion (ArrayList<Corp_Regional> regionals,ArrayList<SearchDetails>searchDetails,String filterpattern){
        boolean isContain = false;
        if (regionals!=null){
            for (Corp_Regional regional : regionals){
                if (regional.getCorpStructures().name.toLowerCase().contains(filterpattern)){
                    for (SearchDetails details : searchDetails){
                        if (details.getName().equalsIgnoreCase(regional.getCorpStructures().name)){
                            isContain = true;
                            break;
                        }
                    }
                    if (!isContain){
                        searchDetails.add(new SearchDetails(regional.getCorpStructures().name,"Region"));
                    }

                    SearchDivision(regional.getQualificationDivisions(),searchDetails,filterpattern);
                }else {

                    SearchDivision(regional.getQualificationDivisions(),searchDetails,filterpattern);
                }
            }
        }
    }

    private void SearchDivision (ArrayList<Corp_Division>divisions ,ArrayList<SearchDetails>searchDetails,String filterpattern){
        boolean isContain = false;
        if (divisions!=null){
            for (Corp_Division division : divisions){
                if (division.getCorpStructures().name.toLowerCase().contains(filterpattern)){
                    for (SearchDetails details : searchDetails){
                        if (details.getName().equalsIgnoreCase(division.getCorpStructures().name)){
                            isContain = true;
                            break;
                        }
                    }
                    if (!isContain){
                        searchDetails.add(new SearchDetails(division.getCorpStructures().name,"Division"));
                    }
                    SearchArea(division.getQualificationAreas(),searchDetails,filterpattern);
                }else {
                    SearchArea(division.getQualificationAreas(),searchDetails,filterpattern);
                }
            }

        }
    }

    private void SearchArea (ArrayList<Corp_Area>areas ,ArrayList<SearchDetails>searchDetails,String filterpattern){
        boolean isContain = false;
        if (areas!=null){
            for (Corp_Area area : areas){
                if (area.getCorpStructures().name.toLowerCase().contains(filterpattern)){

                    for (SearchDetails details : searchDetails){
                        if (details.getName().equalsIgnoreCase(area.getCorpStructures().name)){
                            isContain = true;
                            break;
                        }
                    }
                    if (!isContain){
                        searchDetails.add(new SearchDetails(area.getCorpStructures().name,"Area"));
                    }

                    SearchDistrict(area.getQualificationDistricts(),searchDetails,filterpattern);
                }else {
                    SearchDistrict(area.getQualificationDistricts(),searchDetails,filterpattern);

                }
            }

        }
    }

    private void SearchDistrict (ArrayList<Corp_District>districts ,ArrayList<SearchDetails>searchDetails,String filterpattern){
        boolean isContain = false;
        if (districts!=null){
            for (Corp_District district : districts){
                if (district.getCorpStructures().name.toLowerCase().contains(filterpattern)){

                    for (SearchDetails details : searchDetails){
                        if (details.getName().equalsIgnoreCase(district.getCorpStructures().name)){
                            isContain = true;
                            break;
                        }
                    }
                    if (!isContain){
                        searchDetails.add(new SearchDetails(district.getCorpStructures().name,"District"));
                    }

                    SearchSite(district.getSites(),searchDetails,filterpattern);
                }else {
                    SearchSite(district.getSites(),searchDetails,filterpattern);
                }
            }

        }
    }

    private void SearchSite (ArrayList<CustomSite>sites ,ArrayList<SearchDetails>searchDetails,String filterpattern){
        boolean isContain = false;
        if (sites!=null){
            for (CustomSite site : sites) {
                if (site.getSite().sitename.toLowerCase().contains(filterpattern)) {

                    for (SearchDetails details : searchDetails){
                        if (details.getName().equalsIgnoreCase(site.getSite().sitename)){
                            isContain = true;
                            break;
                        }
                    }

                    if (!isContain){
                        searchDetails.add(new SearchDetails(site.getSite().sitename, "Store"));
                    }
                }
            }
        }
    }

    private ArrayList<Corp_Chain> getSelectedChain(SearchDetails details){

        SelectedCorpchain = new ArrayList<>();

        if (corpchain!=null){

            for (Corp_Chain corp_chain : corpchain){
                if (corp_chain.getName().equalsIgnoreCase(details.getName())){

                    if (corp_chain.getQualificationRegionals()!=null){
                        for (Corp_Regional regional : corp_chain.getQualificationRegionals()){
                            regional.setSelected(true);

                            if (regional.getQualificationDivisions()!=null){
                                for (Corp_Division division : regional.getQualificationDivisions()){

                                    if (regional.getQualificationDivisions().size()==1){
                                        division.setSelected(true);
                                    }else {
                                        division.setSelected(false);
                                    }

                                    if (division.getQualificationAreas()!=null){

                                        for (Corp_Area area : division.getQualificationAreas()){

                                            if (division.getQualificationAreas().size() == 1){
                                                area.setSelected(true);
                                            }else {
                                                area.setSelected(false);
                                            }

                                            if (area.getQualificationDistricts()!=null){
                                                for (Corp_District district : area.getQualificationDistricts()){
                                                    if (area.getQualificationDistricts().size()== 1){
                                                        district.setSelected(true);
                                                    }else {
                                                        district.setSelected(false);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    SelectedCorpchain.add(corp_chain);
                }else {
                    Corp_Chain corp_chain1 = new Corp_Chain();
                    corp_chain1.setName(corp_chain.getName());
                    corp_chain1.setSites(corp_chain.getSites());
                    corp_chain1.setQualificationRegionals(getSelectedRegion(corp_chain1,corp_chain.getQualificationRegionals(),details));
                }
            }
        }

        return SelectedCorpchain;
    }

    private ArrayList<Corp_Regional> getSelectedRegion(Corp_Chain chain,ArrayList<Corp_Regional>regionals,SearchDetails details){

        ArrayList<Corp_Regional>SelectedRegion = new ArrayList<>();
        ArrayList<Corp_Regional>corpRegionals = new ArrayList<>(regionals);

        try {
            for (Corp_Regional regional : corpRegionals){
                if (regional.getCorpStructures().name.equalsIgnoreCase(details.getName())){
                    regional.setSelected(true);

                    if (regional.getQualificationDivisions()!=null){

                        for (Corp_Division division : regional.getQualificationDivisions()){

                            if (regional.getQualificationDivisions().size()==1){
                                division.setSelected(true);
                            }else {
                                division.setSelected(false);
                            }

                            if (division.getQualificationAreas()!=null){

                                for (Corp_Area area : division.getQualificationAreas()){

                                    if (division.getQualificationAreas().size() == 1){
                                        area.setSelected(true);
                                    }else {
                                        area.setSelected(false);
                                    }

                                    if (area.getQualificationDistricts()!=null){
                                        for (Corp_District district : area.getQualificationDistricts()){
                                            if (area.getQualificationDistricts().size()== 1){
                                                district.setSelected(true);
                                            }else {
                                                district.setSelected(false);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    SelectedRegion.add(regional);
                    SelectedCorpchain.add(chain);
                }else {

                    Corp_Regional regional1 = new Corp_Regional();
                    regional1.setCorpStructures(regional.getCorpStructures());
                    regional1.setSelected(true);
                    regional1.setSiteLists(regional.getSiteLists());
                    regional1.setQualificationDivisions(getSelectedDivision(chain,regional.getQualificationDivisions(),details));
                    if (regional1.getQualificationDivisions().size()>0){
                        SelectedRegion.add(regional1);
                    }

                }
            }


        }catch (Exception e){
            e.printStackTrace();
        }


        return SelectedRegion;
    }

    private ArrayList<Corp_Division> getSelectedDivision(Corp_Chain chain,ArrayList<Corp_Division>divisions,SearchDetails details){
        ArrayList<Corp_Division>SelectedDivision = new ArrayList<>();
        ArrayList<Corp_Division>Corp_Division = new ArrayList<>(divisions);
        try {
            for (Corp_Division division : Corp_Division){
                if (division.getCorpStructures().name.equalsIgnoreCase(details.getName())){
                    division.setSelected(true);

                    if (division.getQualificationAreas()!=null){

                        for (Corp_Area area : division.getQualificationAreas()){

                            if (division.getQualificationAreas().size() == 1){
                                area.setSelected(true);
                            }else {
                                area.setSelected(false);
                            }

                            if (area.getQualificationDistricts()!=null){
                                for (Corp_District district : area.getQualificationDistricts()){
                                    if (area.getQualificationDistricts().size()== 1){
                                        district.setSelected(true);
                                    }else {
                                        district.setSelected(false);
                                    }
                                }
                            }
                        }
                    }

                    SelectedDivision.add(division);
                    SelectedCorpchain.add(chain);
                }else {

                    Corp_Division division1 = new Corp_Division();
                    division1.setSelected(true);
                    division1.setCorpStructures(division.getCorpStructures());
                    division1.setQualificationAreas(getSelectedArea(chain,division.getQualificationAreas(),details));
                    if (division1.getQualificationAreas().size()>0){
                        SelectedDivision.add(division1);
                    }

                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return SelectedDivision;
    }


    private ArrayList<Corp_Area> getSelectedArea(Corp_Chain chain,ArrayList<Corp_Area>areas,SearchDetails details){

        ArrayList<Corp_Area>SelectedArea = new ArrayList<>();
        ArrayList<Corp_Area>Corp_Area = new ArrayList<>(areas);
        try {
            for (Corp_Area area : Corp_Area){
                if (area.getCorpStructures().name.equalsIgnoreCase(details.getName())){
                    area.setSelected(true);

                    if (area.getQualificationDistricts()!=null){
                        for (Corp_District district : area.getQualificationDistricts()){
                            if (area.getQualificationDistricts().size()== 1){
                                district.setSelected(true);
                            }else {
                                district.setSelected(false);
                            }
                        }
                    }

                    SelectedArea.add(area);
                    SelectedCorpchain.add(chain);
                }else {

                    Corp_Area area1 = new Corp_Area();
                    area1.setSelected(true);
                    area1.setCorpStructures(area.getCorpStructures());
                    area1.setQualificationDistricts(getSelectedDistrict(chain,area.getQualificationDistricts(),details));
                    if (area1.getQualificationDistricts().size()>0){
                        SelectedArea.add(area1);
                    }

                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return SelectedArea;
    }


    private ArrayList<Corp_District> getSelectedDistrict(Corp_Chain chain,ArrayList<Corp_District>districts,SearchDetails details){
        ArrayList<Corp_District>SelectedDistrict = new ArrayList<>();
        ArrayList<Corp_District>Corp_District = new ArrayList<>(districts);
        try {
            for (Corp_District district : Corp_District){
                if (district.getCorpStructures().name.equalsIgnoreCase(details.getName())){
                    district.setSelected(true);

                    SelectedDistrict.add(district);
                    SelectedCorpchain.add(chain);
                }else {
                    Corp_District district1 = new Corp_District();
                    district1.setSelected(true);
                    district1.setCorpStructures(district.getCorpStructures());
                    district1.setSites(getSelectedStore(chain,district.getSites(),details));
                    if (district1.getSites().size()>0){
                        SelectedDistrict.add(district1);
                        SelectedCorpchain.add(chain);
                    }
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return SelectedDistrict;
    }


    private ArrayList<CustomSite> getSelectedStore(Corp_Chain chain,ArrayList<CustomSite>sites,SearchDetails details){
        ArrayList<CustomSite>SelectedSite = new ArrayList<>();
        ArrayList<CustomSite>CustomSite = new ArrayList<>(sites);
        try {
            for (CustomSite site : CustomSite){
                if (site.getSite().sitename.equalsIgnoreCase(details.getName())){
                    SelectedSite.add(site);
                    System.out.println("SITE NAME : "+site.getSite().sitename);

                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return SelectedSite;
    }


    public void hide() {
        View view = fragmentActivity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) fragmentActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
