package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.DailyLabor;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.Daypart;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.DashBoard.DashboardActivity;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.DailySchedule.Dashboard_daily_sched_listviewAdapter;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.WeeklySchedule.WeeklyPickerDate;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_SalePerformance.Dashboard_SalesPerformance;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_SalePerformance.SalesPerformanceDaypartHeader;
import com.procuro.androidscm.Restaurant.Schema.Dayparts;
import com.procuro.androidscm.Restaurant.Schema.Schema;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileData;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.HoursOfOperation;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.SMSPositioning;
import com.procuro.apimmdatamanagerlib.SMSPositioningEmployeeScheduleAssignment;
import com.procuro.apimmdatamanagerlib.SMSPositioningEmployeeScheduleAssignmentPositions;
import com.procuro.apimmdatamanagerlib.Schedule_Business_Site_Plan;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.SiteSales;
import com.procuro.apimmdatamanagerlib.SiteSalesActual;
import com.procuro.apimmdatamanagerlib.SiteSalesData;
import com.procuro.apimmdatamanagerlib.SiteSalesDaypart;
import com.procuro.apimmdatamanagerlib.SiteSalesForecast;
import com.procuro.apimmdatamanagerlib.StorePrepData;
import com.procuro.apimmdatamanagerlib.StorePrepSchedule;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;


public class Dashboard_DailyLabor_fragment extends Fragment {

    private Dashboard_DailyLabor_Daypart_Header_RecyclerViewAdapter timeadapter;
    public static RecyclerView listView;
    public static RecyclerView time;
    public static TextView daypart,total_difference,total_actual,total_forecast;
    private ConstraintLayout datepicker;
    private Button sales_performance;
    private Site site = SCMDataManager.getInstance().getSelectedSite().getSite();
    private ArrayList<CustomUser>customUsers;
    private SiteSales siteSales;
    private com.procuro.apimmdatamanagerlib.aPimmDataManager dataManager = aPimmDataManager.getInstance();
    public static ProgressDialog progressDialog;
    private double totalForcast ;
    private ArrayList<DailyLaborData>dailyLaborDataArrayList;

    public Dashboard_DailyLabor_fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dashboard_daily_labor_fragment, container, false);

        listView = view.findViewById(R.id.listview);
        time = view.findViewById(R.id.weekdays);
        daypart = view.findViewById(R.id.date);
        datepicker = view.findViewById(R.id.date_container);
        sales_performance = view.findViewById(R.id.sales_performance);
        total_difference = view.findViewById(R.id.total_difference);
        total_forecast = view.findViewById(R.id.total_forecast);
        total_actual = view.findViewById(R.id.total_actual);

        getDailyLabor(getContext(),false);

        setUpOnclicks();

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (progressDialog!=null){
            progressDialog.dismiss();
        }
    }

    public static void getDailyLabor(Context context, boolean isScore){
        Site site = SCMDataManager.getInstance().getSelectedSite().getSite();
        Calendar currentDate = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        currentDate = SCMTool.getDateWithDaypart(currentDate.getTime().getTime(),site.effectiveUTCOffset);

        CheckSchedule(currentDate.getTime(),context,isScore);
    }

    private static void CheckSchedule(Date date,Context context,boolean isScore){
        if (!isScore){
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Please Wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
        if (SCMDataManager.getInstance().getDailySchedule()!=null){
            DisplayDaily(SCMDataManager.getInstance().getDailySchedule(),date,isScore,context);

        }else {
            DownloadSelectedSchedule(date,context,isScore);

        }
    }

    public static void DownloadSelectedSchedule(final Date date, final Context context, final boolean isScore){
        Site site = SCMDataManager.getInstance().getSelectedSite().getSite();
        aPimmDataManager dataManager = aPimmDataManager.getInstance();
        dataManager.getSchedulesByDate(site.siteid, date, date, new OnCompleteListeners.getEmployeeScheduleListener() {
            @Override
            public void getEmployeeScheduleCallback(ArrayList<Schedule_Business_Site_Plan> schedules, Error error) {
                if (error == null){
                    SCMDataManager.getInstance().setDailySchedule(schedules);
                }
                DisplayDaily(schedules,date,isScore,context);
            }
        });
    }


    public static void DisplayDaily(ArrayList<Schedule_Business_Site_Plan>schedules,Date date,boolean isScore,Context context){
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM/dd/yy");
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            ArrayList<CustomUser> customUsers = SCMTool.getActiveCustomUser(SCMDataManager.getInstance().getStoreUsers());
            if (schedules!=null){
                for (CustomUser user : customUsers) {
                    user.setSchedules(new ArrayList<Schedule_Business_Site_Plan>());
                    for (Schedule_Business_Site_Plan sched: schedules){
                        if (dateFormat.format(sched.date).equalsIgnoreCase(dateFormat.format(date))){
                            if (user.getUser().userId.equalsIgnoreCase(sched.userID)) {
                                user.getSchedules().add(sched);
                            }
                        }
                    }
                }
            }
            getSalesPerforance(SCMDataManager.getInstance().getSiteSales(),date,customUsers,isScore,context);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    public static void getSalesPerforance(ArrayList<SiteSales> siteSales,Date curretDay,ArrayList<CustomUser>customUsers,boolean isScore,Context context){
        SimpleDateFormat format = new SimpleDateFormat("EEE");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        Dayparts acutalDaypart = SCMTool.getActualCurrentDaypartStart();
        try {

            for (SiteSales siteSale : siteSales){
                if (format.format(siteSale.salesDate).equalsIgnoreCase(format.format(curretDay))){
                    GenerateLaborTracker(siteSale,customUsers,context,isScore);
                    if (!isScore){
                        try {
                            if (progressDialog!=null && progressDialog.isShowing()){
                                progressDialog.dismiss();
                            }
                            daypart.setText(acutalDaypart.title);
                            DisplayTotals(siteSale,context);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                    break;
                }

            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void DisplayTotals(SiteSales siteSales,Context context){
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        double forecast = 0;
        double actual = 0;
        double total = 0;

        try {
            if (siteSales!=null){
                if (siteSales.data!=null){

                    if (siteSales.data.dss!=0){
                        forecast = siteSales.data.dss;
                    }else if (siteSales.data.projected!=0){
                        forecast = siteSales.data.projected;
                    }

                    if (siteSales.data.sales!=null && siteSales.data.sales.size()>0){
                        for (SiteSalesActual actuals : siteSales.data.sales){
                            actual +=actuals.amount;
                        }
                    }
                }
            }

            total = actual - forecast;
            if (total!=0){
                if (total<0){
                    total = total * -1;
                    total_difference.setText("-$"+decimalFormat.format(total));
                    total_difference.setTextColor(ContextCompat.getColor(context,R.color.red));
                }else {
                    total_difference.setText("$"+decimalFormat.format(total));
                }
            }
            total_forecast.setText("$"+decimalFormat.format(forecast));
            total_actual.setText("$"+decimalFormat.format(actual));
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    private void setUpOnclicks(){
        sales_performance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplaySalesPerformance();
            }
        });

        datepicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayCarrier(getContext(),datepicker);
            }
        });

    }

    public static ArrayList<SalesPerformanceDaypartHeader> CreateDaypart1( SiteSales siteSales){
        ArrayList<SalesPerformanceDaypartHeader>times = new ArrayList<>();

        Calendar start = SCMTool.ISOTimeToDate("4:00",siteSales.salesDate);
        Calendar end = SCMTool.ISOTimeToDate("5:00",siteSales.salesDate);

        times.add(new SalesPerformanceDaypartHeader("4-5","4:00","5:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("5:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("6:00",siteSales.salesDate);

        times.add(new SalesPerformanceDaypartHeader("5-6","5:00","6:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("6:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("7:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("6-7","6:00","7:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("7:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("8:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("7-8","7:00","8:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("8:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("9:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("8-9","8:00","9:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("9:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("10:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("9-10","9:00","10:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("10:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("11:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("10-11",true,"10:00","11:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("4:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("11:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("Total","4:00","11:00",start.getTime(),end.getTime()));

        return times;
    }

    public static ArrayList<SalesPerformanceDaypartHeader>  CreateDaypart2( SiteSales siteSales){
        ArrayList<SalesPerformanceDaypartHeader>times = new ArrayList<>();

        Calendar start = SCMTool.ISOTimeToDate("10:00",siteSales.salesDate);
        Calendar end = SCMTool.ISOTimeToDate("11:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("10-11",true,"10:00","11:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("11:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("12:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("11-12","11:00","12:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("12:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("13:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("12-1","12:00","13:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("13:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("14:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("1-2","13:00","14:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("10:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("14:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("Total","10:00","14:00",start.getTime(),end.getTime()));

        return times;
    }

    public static ArrayList<SalesPerformanceDaypartHeader>  CreateDaypart3( SiteSales siteSales){

        ArrayList<SalesPerformanceDaypartHeader>times = new ArrayList<>();

        Calendar start = SCMTool.ISOTimeToDate("14:00",siteSales.salesDate);
        Calendar end = SCMTool.ISOTimeToDate("15:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("2-3","14:00","15:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("15:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("16:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("3-4","15:00","16:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("16:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("17:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("4-5","16:00","17:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("14:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("17:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("Total","14:00","17:00",start.getTime(),end.getTime()));

        return times;

    }

    public static ArrayList<SalesPerformanceDaypartHeader>  CreateDaypart4( SiteSales siteSales){
        ArrayList<SalesPerformanceDaypartHeader>times = new ArrayList<>();

        Calendar start = SCMTool.ISOTimeToDate("17:00",siteSales.salesDate);
        Calendar end = SCMTool.ISOTimeToDate("18:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("5-6","17:00","18:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("18:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("19:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("6-7","18:00","19:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("19:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("20:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("7-8","19:00","20:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("17:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("20:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("Total","17:00","20:00",start.getTime(),end.getTime()));

        return times;
    }

    public static ArrayList<SalesPerformanceDaypartHeader>  CreateDaypart5( SiteSales siteSales){
        ArrayList<SalesPerformanceDaypartHeader>times = new ArrayList<>();

        Calendar start = SCMTool.ISOTimeToDate("20:00",siteSales.salesDate);
        Calendar end = SCMTool.ISOTimeToDate("21:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("8-9","20:00","21:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("21:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("22:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("9-10","21:00","22:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("20:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("22:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("Total","20:00","22:00",start.getTime(),end.getTime()));


        return times;

    }

    public static ArrayList<SalesPerformanceDaypartHeader>  CreateDaypart6( SiteSales siteSales){
        ArrayList<SalesPerformanceDaypartHeader>times = new ArrayList<>();

        Calendar start = SCMTool.ISOTimeToDate("22:00",siteSales.salesDate);
        Calendar end = SCMTool.ISOTimeToDate("23:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("10-11","22:00","23:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("23:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDatePlus1("0:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("11-12","23:00","0:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDatePlus1("0:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDatePlus1("1:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("12-1","0:00","1:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDatePlus1("1:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDatePlus1("2:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("1-2","1:00","2:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDatePlus1("2:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDatePlus1("3:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("2-3","2:00","3:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDatePlus1("3:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDatePlus1("4:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("3-4","3:00","4:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("22:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDatePlus1("4:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("Total","22:00","4:00",start.getTime(),end.getTime()));

        return times;

    }

    public void DisplayCarrier(final Context context, View anchor) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.BOTTOM)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .arrowColor(ContextCompat.getColor(context, R.color.orange))
                .transparentOverlay(true)
                .contentView(R.layout.custom_picker)
                .focusable(true)
                .build();
        tooltip.show();


        final NumberPicker picker = tooltip.findViewById(R.id.picker);
        TextView pickertitle = tooltip.findViewById(R.id.picker_title);
        Button accept = tooltip.findViewById(R.id.apply);

        final String[] values = {"Daypart 1","Daypart 2","Daypart 3","Daypart 4","Daypart 5","Daypart 6"};
        final String[] dayparts = {"1","2","3","4","5","6"};

        picker.setDisplayedValues(values);
        picker.setMinValue(0);
        picker.setMaxValue(values.length - 1);
        picker.setWrapSelectorWheel(true);
        picker.setValue(values.length / 2);


        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                daypart.setText(values[picker.getValue()]);
                FindSelectedDaypart(dayparts[picker.getValue()],context,false);

                tooltip.dismiss();
            }
        });
    }

    private void DisplaySalesPerformance() {
        Intent intent = new Intent(getActivity(), Dashboard_SalesPerformance.class);
        startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
        getActivity().finish();
    }


    ///Daily Labor Process

    public static void GenerateLaborTracker(SiteSales siteSales,ArrayList<CustomUser>customUsers,Context context,boolean isScore){

        Dayparts acutalDaypart = SCMTool.getActualCurrentDaypartStart();
        double totalForcast = getForcast(siteSales);

        ArrayList<DailyLaborData> dailyLaborDataArrayList = new ArrayList<>();
        ArrayList<Dayparts>dayparts = Schema.getInstance().getDayparts();
        ArrayList<String>items = CreateItems();

        for (Dayparts daypart : dayparts){

            DailyLaborData dailyLaborData = new DailyLaborData();
            dailyLaborData.setDaypart(daypart);

            ArrayList<SalesPerformanceDaypartHeader>headers = getDayparyHeaders(siteSales,daypart.name);
            dailyLaborData.setDaypartHeaders(headers);

            for (String string : items){
                if (string.equalsIgnoreCase("Projected Sales")){
                    dailyLaborData.setForcasts(getForcastPerDaypart(siteSales,daypart.name,headers,totalForcast));
                }
                else if (string.equalsIgnoreCase("Actual Sales")){
                    dailyLaborData.setSales(getSalesPerDaypart(siteSales,headers));
                }
                else if (string.equalsIgnoreCase("Projected Hrs")){
                    dailyLaborData.setProjectedHrs(getProjectedHrPerDaypart(headers,daypart.name,dailyLaborData));
                }
                else if (string.equalsIgnoreCase("Scheduled Hrs")){
                    dailyLaborData.setScheduledHrs(getScheduledHrs(headers,daypart,siteSales.salesDate,customUsers));
                }
                else if (string.equalsIgnoreCase("Optimum Hrs")){
                    dailyLaborData.setOptimumHrs(getOptimumHrs(daypart.name,dailyLaborData));
                }
                else if (string.equalsIgnoreCase("Labor Hrs +/-")){
                    dailyLaborData.setLaborHrs(getLaborHrs(dailyLaborData));
                }
                else if (string.equalsIgnoreCase("Cumulative +/-")){
                    dailyLaborData.setCommulativeHrs(getCommulative(dailyLaborData,dailyLaborDataArrayList));
                }
                dailyLaborDataArrayList.add(dailyLaborData);
            }
        }
        SCMDataManager.getInstance().setDailyLaborDataArrayList(dailyLaborDataArrayList);
        FindSelectedDaypart(acutalDaypart.name,context,isScore);
    }

    public static void FindSelectedDaypart(String title,Context context,boolean isScore){

        final DecimalFormat decimalFormat = new DecimalFormat("0.00");
        ArrayList<DailyLaborData>dailyLaborDataArrayList = SCMDataManager.getInstance().getDailyLaborDataArrayList();
        if (dailyLaborDataArrayList!=null){
            for (final DailyLaborData dailyLaborData : dailyLaborDataArrayList){
                if (dailyLaborData.getDaypart().name.equalsIgnoreCase(title)){


                    if (!isScore){
                        ArrayList<String>items = CreateItems();
                        Dashboard_DailyLabor_Daypart_Header_RecyclerViewAdapter timeadapter = new Dashboard_DailyLabor_Daypart_Header_RecyclerViewAdapter(context,dailyLaborData.getDaypartHeaders());
                        time.setLayoutManager(new GridLayoutManager(context,dailyLaborData.getDaypartHeaders().size()));
                        time.setAdapter(timeadapter);

                        Dashboard_New_DailyLabor_Daypart_item_RecyclerViewAdapter adapter = new  Dashboard_New_DailyLabor_Daypart_item_RecyclerViewAdapter(context,
                                items,dailyLaborData);
                        listView.setLayoutManager(new GridLayoutManager(context,1));
                        listView.setAdapter(adapter);
                    }else {
                        Handler handler = new Handler(Looper.getMainLooper());
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                ArrayList<Double>Commulative = dailyLaborData.getCommulativeHrs();
                                ArrayList<Double>LaborHrs = dailyLaborData.getLaborHrs();
                                if (Commulative!=null){
                                    double commu = Commulative.get(Commulative.size()-1);

                                    if (commu != -0.1 && commu!= 0.0){
                                        if((commu-(int)commu)!=0){
                                            DashboardActivity.staff_peak_actual.setText(decimalFormat.format(commu));
                                        }
                                        else{
                                            DashboardActivity.staff_peak_actual.setText(String.valueOf((int)commu));
                                        }

                                    }else {
                                        DashboardActivity.staff_peak_actual.setText("--");
                                    }
                                }

                                if (LaborHrs!=null){
                                    double labor = LaborHrs.get(LaborHrs.size()-1);
                                    if (labor != -0.1 && labor!= 0.0){
                                        if((labor-(int)labor)!=0){
                                            DashboardActivity.stff_peak_required.setText(decimalFormat.format(labor));
                                        }
                                        else{
                                            DashboardActivity.stff_peak_required.setText(String.valueOf((int)labor));
                                        }
                                    }else {
                                        DashboardActivity.stff_peak_required.setText("--");
                                    }
                                }
                            }
                        });
                    }
                    break;
                }
            }
        }
    }

    public static ArrayList<String> CreateItems(){
        ArrayList<String>strings = new ArrayList<>();
        strings.add("Projected Sales");
        strings.add("Actual Sales");
        strings.add("Projected Hrs");
        strings.add("Scheduled Hrs");
        strings.add("Optimum Hrs");
        strings.add("Labor Hrs +/-");
        strings.add("Cumulative +/-");

        return strings;
    }

    public static ArrayList<SalesPerformanceDaypartHeader> getDayparyHeaders(SiteSales siteSales,String daypartSelected){
        ArrayList<SalesPerformanceDaypartHeader>times = null;
        if (daypartSelected.equalsIgnoreCase("1")){
            times = CreateDaypart1(siteSales);
        }
        else if (daypartSelected.equalsIgnoreCase("2")){
            times = CreateDaypart2(siteSales);
        }
        else if (daypartSelected.equalsIgnoreCase("3")){
            times = CreateDaypart3(siteSales);
        }
        else if (daypartSelected.equalsIgnoreCase("4")){
            times = CreateDaypart4(siteSales);
        }
        else if (daypartSelected.equalsIgnoreCase("5")){
            times = CreateDaypart5(siteSales);
        }
        else if (daypartSelected.equalsIgnoreCase("6")){
            times = CreateDaypart6(siteSales);
        }

        return times;
    }

    public static ArrayList<Double> getCommulative(DailyLaborData laborData,ArrayList<DailyLaborData>dailyLaborDataArrayList){
        ArrayList<Double>laborHrs = laborData.getLaborHrs();
        ArrayList<Double>commulativeHrs = new ArrayList<>();
        boolean startFound = false;
        double prevCummulitive = 0;

        for (int i = 0; i <laborHrs.size()-1 ; i++) {
            double labor = laborHrs.get(i);
            double total = 0;
            if (!startFound){
                if (!laborData.getDaypart().name.equalsIgnoreCase("1")){
                    for (int j = 0; j <dailyLaborDataArrayList.size() ; j++) {
                        DailyLaborData dailyLaborData = dailyLaborDataArrayList.get(j);
                        if (dailyLaborData.getDaypart().name.equalsIgnoreCase(laborData.getDaypart().name)){
                            DailyLaborData PrevLaborData = dailyLaborDataArrayList.get(j-1);
                            if (PrevLaborData.getCommulativeHrs()!=null && PrevLaborData.getCommulativeHrs().size()>0){
                                double prevCommu = PrevLaborData.getCommulativeHrs().get(PrevLaborData.getCommulativeHrs().size()-1);
                                if (prevCommu!=-0.1 && prevCommu != 0.0 && labor!=-0.1 && labor != 0.0){
                                    startFound = true;
                                    if (PrevLaborData.getDaypart().name.equalsIgnoreCase("1")){
                                        commulativeHrs.add(prevCommu);
                                    }else {
                                        total = labor +  prevCommu;
                                        commulativeHrs.add(total);
                                    }
                                }
                                else {
                                    commulativeHrs.add(-0.1);
                                }
                            }else {
                                commulativeHrs.add(-0.1);
                            }
                            break;
                        }
                    }
                }else {
                    if (labor!=-0.1 && labor != 0.0){
                        startFound = true;
                        total = labor;
                        commulativeHrs.add(total);
                    }
                    else {
                        commulativeHrs.add(-0.1);
                    }
                }
            }else {
                double prev = commulativeHrs.get(commulativeHrs.size()-1);
                if (prev!= -0.1 && labor !=-0.1){
                    total = labor + prev;
                    commulativeHrs.add(total);
                }
                else {
                    commulativeHrs.add(-0.1);
                }
            }
        }
        double total = commulativeHrs.get(commulativeHrs.size()-1);
        if (total!=-0.1 && total !=0.0){
            commulativeHrs.add(total);
        }else {
            ArrayList<Double>reverseList = new ArrayList<>(commulativeHrs);
            Collections.reverse(reverseList);
            boolean found = false;
            for (double reverse : reverseList){
                if (reverse!=-0.1 && reverse != 0.0){
                    found = true;
                    System.out.println("Reverse : "+reverse);
                    commulativeHrs.add(reverse);
                    break;
                }
            }
            if (!found){
                if (dailyLaborDataArrayList.size()>0){
                    for (int i = dailyLaborDataArrayList.size() - 1; i >= 0; i--) {
                        DailyLaborData data = dailyLaborDataArrayList.get(i);
                        ArrayList<Double>prevCummulative = data.getCommulativeHrs();
                        if (prevCummulative!=null){
                            if (prevCummulative.size()>0){
                                double finalPrev = prevCummulative.get(prevCummulative.size()-1);
                                if (finalPrev!=-0.1){
                                    found = true;
                                    commulativeHrs.add(finalPrev);
                                    break;
                                }
                            }
                        }

                    }
                    if (!found){
                        commulativeHrs.add(-0.1);
                    }
                }else {
                    commulativeHrs.add(-0.1);
                }
            }
        }
        return commulativeHrs;
    }

    public static ArrayList<Double> getLaborHrs(DailyLaborData laborData){
        ArrayList<Double>optimumHrs = laborData.getOptimumHrs();
        ArrayList<Double>scheduledHrs = laborData.getScheduledHrs();
        ArrayList<Double>sales = laborData.getSales();
        ArrayList<Double>laborHrs = new ArrayList<>();

        for (int i = 0; i <optimumHrs.size()-1 ; i++) {
            double sale = sales.get(i);
            double optimum = optimumHrs.get(i);
            double schedule = scheduledHrs.get(i);

            if (sale!=-0.1 && sale != 0.0){
                if (optimum!=-0.1 && schedule!=-0.1){
                    double result = schedule -optimum;
                    laborHrs.add(result);
                }else {
                    laborHrs.add(-0.1);
                }
            }else {
                laborHrs.add(-0.1);
            }
        }

        double total = 0;
        boolean hasValue = false;
        for (double labor : laborHrs){
            if (labor!=-0.1){
                total += labor;
            }else {
                total += 0;
            }
        }
        laborHrs.add(total);

        return laborHrs;
    }

    public static ArrayList<Double> getOptimumHrs(String daypartSelected,DailyLaborData laborData){
        ArrayList<Double> projectedHrs = laborData.getProjectedHrs();
        ArrayList<Double> sales = laborData.getSales();
        ArrayList<Double> optimumHrs = new ArrayList<>();
        if (daypartSelected.equalsIgnoreCase("1") ){
            optimumHrs.addAll(projectedHrs);

        }else if (daypartSelected.equalsIgnoreCase("2") ){
            double schedule = projectedHrs.get(0);
            if (schedule!=-0.1){
                optimumHrs.add(schedule);
            }else {
                optimumHrs.add(-0.1);
            }
            for (int i = 1; i < sales.size()-1; i++) {
                double sale = sales.get(i);
                if (sale!=-0.1 && sale !=0.0){
                    optimumHrs.add(getCPH(sale));
                }else {
                    optimumHrs.add(-0.1);
                }
            }

            double total = 0;
            for (double optimum : optimumHrs){
                if (optimum!=-0.1){
                    total += optimum;
                }else {
                    total += 0;
                }
            }
            optimumHrs.add(total);

        }else {
            for (int i = 0; i < sales.size()-1; i++) {
                double sale = sales.get(i);
                if (sale!=-0.1 && sale !=0.0){
                    optimumHrs.add(getCPH(sale));
                }else {
                    optimumHrs.add(-0.1);
                }
            }
            double total = 0;
            for (double optimum : optimumHrs){
                if (optimum!=-0.1){
                    total += optimum;
                }else {
                    total += 0;
                }
            }
            optimumHrs.add(total);
        }
        return optimumHrs;
    }

    public static ArrayList<Double> getScheduledHrs(ArrayList<SalesPerformanceDaypartHeader>header,Dayparts daypart,Date date,ArrayList<CustomUser>customUsers){

        ArrayList<Double>scheduledHrs = new ArrayList<>();

        final SimpleDateFormat dayformat = new SimpleDateFormat("EEE MMM/dd/yy");
        dayformat.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat format = new SimpleDateFormat("H:mm");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));


        SimpleDateFormat sampleFOrmat = new SimpleDateFormat("EEE MMM/dd/yy hh:mm a");
        sampleFOrmat.setTimeZone(TimeZone.getTimeZone("UTC"));

        for (int i = 0; i <header.size()-1 ; i++) {
            SalesPerformanceDaypartHeader daypartHeader  = header.get(i);
            double total = 0;

            Calendar daypartStart = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            daypartStart.setTime(daypartHeader.getStartDate());

            Calendar daypartEnd = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            daypartEnd.setTime(daypartHeader.getEndDate());


            for (CustomUser user : customUsers){

                long totalDiff = 0 ,totalHr  = 0, totalMinute =0;

                long total_hours = 0;
                long totalMinutes = 0;

                for (Schedule_Business_Site_Plan sched : user.getSchedules()){

                    long  startDiff = 0;
                    long  endDiff = 0 ;

                    if (dayformat.format(sched.date).equalsIgnoreCase(dayformat.format(date))){

                        String stringName = format.format(sched.startTime);
                        Calendar schedStart = SCMTool.ISOTimeToDate(stringName,sched.startTime);
                        Calendar schedEnd = SCMTool.ISOTimeToDate(format.format(sched.endTime),sched.endTime);
                        schedEnd.add(Calendar.MINUTE,-1);

                        Calendar schedEndTime = SCMTool.ISOTimeToDate(format.format(sched.endTime),sched.endTime);

                        if (SCMTool.isBetween(schedStart.getTime(),schedEnd.getTime(),daypartStart.getTime())
                                && SCMTool.isBetweenEqual(schedStart.getTime(),schedEndTime.getTime(),daypartEnd.getTime())){

                            if (schedEnd.getTime().getTime() - schedStart.getTime().getTime() >0){
                                startDiff += daypartStart.getTime().getTime() - schedStart.getTime().getTime();
                                endDiff += daypartEnd.getTime().getTime() - schedStart.getTime().getTime();
                            }


//                        System.out.println(" ALL "+user.getUser().getFullName() + " | "+dayparts.title + " "+
//                                dateFormat.format(schedStart.getTime())+" | "+dateFormat.format(schedEnd.getTime())+ " | "+dateFormat.format(daypartStart.getTime())+ " | "+dateFormat.format(daypartEnd.getTime()) +" | "+sched.position);
                        }

                        else if (SCMTool.isBetween(daypartStart.getTime(),daypartEnd.getTime(),schedStart.getTime())){
                            if (schedEnd.getTime().getTime() - schedStart.getTime().getTime() >0){
                                endDiff +=  daypartEnd.getTime().getTime()- schedStart.getTime().getTime();
                            }
//                        System.out.println(" START "+user.getUser().getFullName() + " | "+dayparts.title + " "+
//                                dateFormat.format(schedStart.getTime())+" | "+dateFormat.format(schedEnd.getTime())+ " | "+dateFormat.format(daypartStart.getTime())+ " | "+dateFormat.format(daypartEnd.getTime())+" | "+sched.position);

                        }
                        else if (SCMTool.isBetween(daypartStart.getTime(),daypartEnd.getTime(),schedEnd.getTime())){
                            double difference = 0;
                            if (schedEnd.getTime().getTime() - schedStart.getTime().getTime() >0){
                                schedEnd.add(Calendar.MINUTE,1);
                                endDiff +=  schedEnd.getTime().getTime() - daypartStart.getTime().getTime();

                            }
//                        System.out.println(" END "+user.getUser().getFullName() + " | "+dayparts.title + " "+
//                                dateFormat.format(schedStart.getTime())+" | "+dateFormat.format(schedEnd.getTime())+ " | "+dateFormat.format(daypartStart.getTime())+ " | "+dateFormat.format(daypartEnd.getTime())+" | "+sched.position + " | ");
                        }

                        totalDiff = endDiff - startDiff;
                        int Minute = (int) totalDiff / (60 * 1000) % 60;
                        totalHr = (int) (totalDiff / (60 * 60 * 1000));
                        double finalminute = Minute/60.0;
                        total += totalHr +finalminute;

//                        System.out.println("USER : "+user.getUser().getFullName() + " | "+totalHr +" : "+Minute + "PERCENTAGE : "+finalminute +" TOTAL: "+total+" | "+daypartHeader.getName());
                    }
                }
            }
            scheduledHrs.add(total);
        }

        double total = 0;
        for (double schedule : scheduledHrs){
            total += schedule;
        }
        scheduledHrs.add(total);

        return scheduledHrs;
    }

    public static ArrayList<Double> getProjectedHrPerDaypart(ArrayList<SalesPerformanceDaypartHeader>header,String daypartSelected,DailyLaborData dailyData){
        ArrayList<Double>forecast = dailyData.getForcasts();
        ArrayList<Double>projectedHrs = new ArrayList<>();
        HoursOfOperation hoursOfOperation = SCMDataManager.getInstance().getHoursOfOperation();
        for (int i = 0; i <header.size()-1 ; i++) {
            SalesPerformanceDaypartHeader daypartHeader = header.get(i);
            if (hoursOfOperation!=null){
                StorePrepSchedule schedule = hoursOfOperation.storePrepSchedule;
                if (schedule!=null){
                    double total =0;
                    if (daypartSelected.equalsIgnoreCase("1")){
                        if (schedule.breakfast!=null && schedule.breakfast.size()>0 && schedule.lunch!=null && schedule.lunch.size()>0){
                            ArrayList<StorePrepData>totalStorePrepData = CreateTotal(schedule.breakfast,schedule.lunch);
                            for (StorePrepData totalData : totalStorePrepData){
                                String time = totalData.time.replace(" AM","");
                                String[]hr  = daypartHeader.getStart().split(":");
                                if (time.contains(hr[0])){
                                    if (time.contains(":30")){
                                        total += totalData.totalEmployees;
                                    }else {
                                        total += totalData.totalEmployees;
                                    }
                                }
                            }
                        }
                    }else if (daypartSelected.equalsIgnoreCase("2")){
                        if (daypartHeader.getName().equalsIgnoreCase("10-11")){
                            if (schedule.breakfast!=null && schedule.breakfast.size()>0 && schedule.lunch!=null && schedule.lunch.size()>0){
                                ArrayList<StorePrepData>totalStorePrepData = CreateTotal(schedule.breakfast,schedule.lunch);
                                for (StorePrepData totalData : totalStorePrepData){
                                    String time = totalData.time.replace(" AM","");
                                    String[]hr  = daypartHeader.getStart().split(":");
                                    if (time.contains(hr[0])){
                                        if (time.contains(":30")){
                                            total += totalData.totalEmployees;
                                        }else {
                                            total += totalData.totalEmployees;
                                        }
                                    }
                                }
                            }
                        }else {
                            total = getCPH(forecast.get(i));
                        }
                    }else {
                        double forecastItem = forecast.get(i);
                        if (forecastItem!=0.0){
                            total = getCPH(forecastItem);
                        }else {
                            total = 0;
                        }
                    }
                    double result = 0;

                    if (total!=0){
                        if (daypartHeader.getStart().equalsIgnoreCase("6:00") || daypartHeader.getStart().equalsIgnoreCase("10:00")){
                            result = total/2;
                        }else {
                            result = total;
                        }
                    }
                    projectedHrs.add(result);
                }
            }
        }

        double total =0;
        for (double projected : projectedHrs){
            total += projected;
        }
        projectedHrs.add(total);
        return projectedHrs;
    }

    public static double getCPH(double item){

        if (item >=0 && item<=100){
            return 4;
        }else if (item >=100 && item<250){
            return 5;
        }else if (item >=250 && item<400){
            return 6;
        }else if (item >=400 && item<550){
            return 7;
        }else if (item >=550 && item<700){
            return 8;
        }else if (item >=700 && item<850){
            return 9;
        }else if (item >=850 && item<1000){
            return 10;
        }else if (item >=1000 && item<1150){
            return 11;
        }else if (item >=1150 && item<1300){
            return 12;
        }else if (item >=1300 && item<1450){
            return 13;
        }else {
            return 14;
        }

    }

    public static ArrayList<StorePrepData>CreateTotal(ArrayList<StorePrepData>breakfast,ArrayList<StorePrepData>lunch){
        ArrayList<StorePrepData>totaldata = new ArrayList<>();

        for (StorePrepData bfast : breakfast){
            for (StorePrepData lu : lunch){
                if (bfast.time.equalsIgnoreCase(lu.time)){
                    StorePrepData newData = new StorePrepData();
                    newData.time = bfast.time;
                    newData.totalEmployees = bfast.totalEmployees+lu.totalEmployees;
                    newData.positions = bfast.positions;
                    totaldata.add(newData);
                }
            }
        }

        return totaldata;
    }

    public static ArrayList<Double> getForcastPerDaypart(SiteSales siteSales,String daypartName,ArrayList<SalesPerformanceDaypartHeader>header,double totalForcast){
        ArrayList<Double>forcasts = new ArrayList<>();
        if (siteSales.data!=null){
            SiteSalesData data = siteSales.data;
            if (data.dayparts!=null){
                for (SiteSalesDaypart daypart : data.dayparts){
                    if (daypart.name.equalsIgnoreCase(daypartName)){

                        for (int i = 0; i <header.size()-1 ; i++) {
                            SalesPerformanceDaypartHeader daypartHeader = header.get(i);
                            BigDecimal percentage = new BigDecimal("0");
                            for (SiteSalesForecast forecast : daypart.forecast){

                                if (daypart.name.equalsIgnoreCase("1")|| daypart.name.equalsIgnoreCase("2")){
                                    if (daypartHeader.getName().equalsIgnoreCase("10-11")){
                                        percentage = getDaypartTransitionPercentage(data.dayparts,data.isFromDefault,daypartHeader);
                                    }else {
                                        if (data.isFromDefault){
                                            if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.startTime)){
                                                if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.endTime)){
                                                    percentage = percentage.add(forecast.percentage);
                                                }
                                            }
                                        }else {
                                            if (isInsideDaypart(daypartHeader,forecast)){
                                                percentage = percentage.add(forecast.percentage);
                                            }
                                        }
                                    }
                                }
                                else {
                                    if (data.isFromDefault){
                                        if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.startTime)){
                                            if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.endTime)){
                                                percentage = percentage.add(forecast.percentage);
                                            }
                                        }
                                    }else {
                                        if (isInsideDaypart(daypartHeader,forecast)){
                                            percentage = percentage.add(forecast.percentage);
                                        }
                                    }
                                }
                            }

//                            System.out.println(" NAME : "+daypartHeader.getName() +" | "+percentage);
                            BigDecimal FORCASTRESULT = new BigDecimal(totalForcast).multiply(percentage);
                            forcasts.add(FORCASTRESULT.doubleValue());
                        }
                        break;
                    }
                }
            }else {
                for (int i = 0; i <header.size()-1 ; i++) {
                    forcasts.add(-0.1);
                }
            }
        }else {
            for (int i = 0; i <header.size()-1; i++) {
                forcasts.add(-0.1);
            }
        }
        double total = 0;
        for (double forecast : forcasts){
            if (forecast == -0.1){
                total += 0;
            }else {
                total += forecast;
            }
        }
        forcasts.add(total);

        return forcasts;
    }

    public static ArrayList<Double> getSalesPerDaypart(SiteSales siteSales,ArrayList<SalesPerformanceDaypartHeader>header){
        ArrayList<Double>sales = new ArrayList<>();
        SimpleDateFormat format = new SimpleDateFormat("H:mm");

        if (siteSales.data!=null){
            SiteSalesData data = siteSales.data;

            if (data.dayparts!=null && data.sales!=null && data.sales.size()>0){

                for (int i = 0; i < header.size()-1; i++) {
                    SalesPerformanceDaypartHeader daypartHeader = header.get(i);
                    double amount = 0.0;
                    for (SiteSalesActual actual : data.sales){
                        if (daypartHeader.getStart().equalsIgnoreCase(format.format(actual.startTime))){
                            if (daypartHeader.getEnd().equalsIgnoreCase(format.format(actual.endTime))){
                                amount += actual.amount;
                            }
                        }
                    }
                    sales.add(amount);
                }

            }else {
                for (int i = 0; i <header.size()-1 ; i++) {
                    sales.add(-0.1);
                }
            }
        }else {
            for (int i = 0; i <header.size()-1; i++) {
                sales.add(-0.1);
            }
        }
        double total = 0;
        for (double sale : sales){
            if (sale == -0.1){
                total += 0;
            }else {
                total += sale;
            }
        }
        sales.add(total);
        return sales;
    }

    public static double getForcast(SiteSales siteSales){
        try {
            if (siteSales.data!=null){
                SiteSalesData data = siteSales.data;
                double forcast =0;
                if (data.dss!=0) {
                    forcast = Double.parseDouble(String.valueOf(data.dss));
                }
                else if (data.projected!=0){
                    forcast = Double.parseDouble(String.valueOf(data.projected));
                }
                return forcast;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    public static boolean isInsideDaypart(SalesPerformanceDaypartHeader daypartHeader,SiteSalesForecast forecast){
        SimpleDateFormat format = new SimpleDateFormat("H:mm");
        String[] header = daypartHeader.getStart().split(":");
        String headerHr = header[0];
        String headerMinute = header[1];
        String header30Minutes = "30";

        String[] forecastName = format.format(forecast.startTime).split(":");
        String forecastHr = forecastName[0];
        String foreMinute = forecastName[1];

        if (headerHr.equalsIgnoreCase(forecastHr)){
            return foreMinute.equalsIgnoreCase(header30Minutes) || foreMinute.equalsIgnoreCase(headerMinute);
        }
        return false;
    }

    public static BigDecimal getDaypartTransitionPercentage(ArrayList<SiteSalesDaypart>dayparts,boolean isFromDefault,SalesPerformanceDaypartHeader daypartHeader ){

        BigDecimal percentage = new BigDecimal("0");
        SimpleDateFormat format = new SimpleDateFormat("H:mm");
        for (SiteSalesDaypart daypart : dayparts){
            if (daypart.name.equalsIgnoreCase("1")){
                for (SiteSalesForecast forecast : daypart.forecast){
                    if (!isFromDefault){
                        if (format.format(forecast.startTime).equalsIgnoreCase("10:00")){
                            percentage = percentage.add(forecast.percentage);
                            break;
                        }
                    }else {
                        if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.startTime)){
                            if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.endTime)){
                                percentage = percentage.add(forecast.percentage);
                                break;
                            }
                        }
                    }

                }
            }else if (daypart.name.equalsIgnoreCase("2")){
                for (SiteSalesForecast forecast : daypart.forecast){
                    if (!isFromDefault){
                        if (format.format(forecast.startTime).equalsIgnoreCase("10:30")){
                            percentage = percentage.add(forecast.percentage);
                            break;
                        }
                    }else {
                        if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.startTime)){
                            if (SCMTool.isBetween(daypartHeader.getStartDate(),daypartHeader.getEndDate(),forecast.endTime)){
                                percentage = percentage.add(forecast.percentage);
                                break;
                            }
                        }
                    }
                }
            }
        }
//        System.out.println("getDaypartTransitionPercentage : "+percentage);
        return percentage;
    }


}
