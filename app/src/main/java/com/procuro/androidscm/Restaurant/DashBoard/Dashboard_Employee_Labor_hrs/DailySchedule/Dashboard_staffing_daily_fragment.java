package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.DailySchedule;

import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.StoreProfileOptions.StoreProfileOptionsData;
import com.procuro.androidscm.Restaurant.Schema.Dayparts;
import com.procuro.androidscm.Restaurant.Schema.Schema;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileData;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.DashboardKitchenData;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.SMSPositioning;
import com.procuro.apimmdatamanagerlib.SMSPositioningEmployeeScheduleAssignment;
import com.procuro.apimmdatamanagerlib.SMSPositioningEmployeeScheduleAssignmentPositions;
import com.procuro.apimmdatamanagerlib.Schedule_Business_Site_Plan;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

import static com.procuro.androidscm.SCMTool.DaypartTimeToDate;
import static com.procuro.androidscm.SCMTool.getStringCurrentDaypart;


public class Dashboard_staffing_daily_fragment extends Fragment {

    private ListView listView;
    private TextView date,peak_hour,required,actual;
    private ConstraintLayout date_container;
    private String timeExpr = "\\b" +                      //   word boundary
            "(?:[01]?[0-9]|2[0-3])" +    //   match 0-9, 00-09, 10-19, 20-23
            ":[0-5][0-9]" +              //   match ':' then 00-59
            "(?! ?[ap]m)" +              //   not followed by optional space, and 'am' or 'pm'
            "\\b" +                      //   word boundary
            "|" +                          // OR
            "\\b" +                      //   word boundary
            "(?:0?[0-9]|1[0-1])" +       //   match 0-9, 00-09, 10-11
            ":[0-5][0-9]" +              //   match ':' and 00-59
            " ?[ap]m" +                  //   match optional space, and 'am' or 'pm'
            "\\b";                       //   word boundary
    Pattern p = Pattern.compile(timeExpr, Pattern.CASE_INSENSITIVE);

    private ProgressBar progressbar;
    private TextView message;

    public Dashboard_staffing_daily_fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dashboard_header_staffing_daily_sched_fragment, container, false);
        listView = view.findViewById(R.id.listView);
        date = view.findViewById(R.id.date);
        peak_hour = view.findViewById(R.id.peak_hour);
        date_container =view.findViewById(R.id.date_container);
        progressbar = view.findViewById(R.id.progressbar);
        message = view.findViewById(R.id.message);
        required = view.findViewById(R.id.required);
        actual = view.findViewById(R.id.actual);

//        CheckDaypart();

        setUpDefaultView();

        setupOnclicks();

        DisplayPeakHour();

        return view;
    }

    private void DisplayActuals(int actuals){
        try {
            actual.setText(String.valueOf(actuals));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void DisplayRequired(Date date){
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE");
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            int totalPosition = SCMTool.getSiteSettingsConfigurationForCurrentDay(getStringCurrentDaypart(),dateFormat.format(date));
            required.setText(String.valueOf(totalPosition));
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void DownloadSelectedSchedule(final Date date, final boolean isDefault){

        SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        this.date.setText(format.format(date));

        progressbar.setVisibility(View.VISIBLE);
        message.setVisibility(View.VISIBLE);
        listView.setVisibility(View.INVISIBLE);

        Site site = SCMDataManager.getInstance().getSelectedSite().getSite();
        aPimmDataManager dataManager = aPimmDataManager.getInstance();
        dataManager.getSchedulesByDate(site.siteid, date, date, new OnCompleteListeners.getEmployeeScheduleListener() {
            @Override
            public void getEmployeeScheduleCallback(ArrayList<Schedule_Business_Site_Plan> schedules, Error error) {
                if (isDefault){
                    SCMDataManager.getInstance().setDailySchedule(schedules);
                }
                setupNewDailySched(date,schedules);
            }
        });
    }

    private void DisplayPeakHour(){
        try {
            peak_hour.setText(ReadTimeFromCode());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setupOnclicks(){
        date_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayDatePicker(getContext(),date_container);
            }
        });
    }

    private void setUpDefaultView() {
        progressbar.setVisibility(View.VISIBLE);
        message.setVisibility(View.VISIBLE);
        listView.setVisibility(View.INVISIBLE);
        Site site = SCMDataManager.getInstance().getSelectedSite().getSite();
        Calendar currentDate = Calendar.getInstance();
        currentDate = SCMTool.getDateWithDaypart(currentDate.getTime().getTime(), site.effectiveUTCOffset);

        if (SCMDataManager.getInstance().getDailySchedule()==null){
            DownloadSelectedSchedule(currentDate.getTime(),true);
        }else {
            setupNewDailySched(currentDate.getTime(),SCMDataManager.getInstance().getDailySchedule());
        }
    }

    public void DisplayDatePicker(final Context context, View anchor) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.BOTTOM)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .arrowColor(ContextCompat.getColor(context,R.color.white))
                .transparentOverlay(true)
                .contentView(R.layout.date_picker)
                .focusable(true)
                .build();
        tooltip.show();


        final DatePicker datePicker =tooltip.findViewById(R.id.datepicker);
        Button accept = tooltip.findViewById(R.id.apply);

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar date = Calendar.getInstance();
                date.setTimeZone(TimeZone.getTimeZone("UTC"));
                date.set(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());
                DownloadSelectedSchedule(date.getTime(),false);
                tooltip.dismiss();
            }
        });


    }

    private void setupNewDailySched(Date currentDate, ArrayList<Schedule_Business_Site_Plan>schedules){
        SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        this.date.setText(format.format(currentDate));

        ArrayList<CustomUser>customUsers = getActiveAndFilteredAUsers();

        for (CustomUser user : customUsers){
            ArrayList<Schedule_Business_Site_Plan>filteredSchedules = new ArrayList<>();
            if (schedules!=null){
                for (Schedule_Business_Site_Plan sched : schedules){
                    if (user.getUser().userId.equalsIgnoreCase(sched.userID)){
                        filteredSchedules.add(sched);
                    }
                }
            }
            user.setSchedules(filteredSchedules);
        }

        progressbar.setVisibility(View.GONE);
        message.setVisibility(View.GONE);
        listView.setVisibility(View.VISIBLE);

        Dashboard_New_daily_sched_listviewAdapter adapter = new Dashboard_New_daily_sched_listviewAdapter(getContext(),customUsers,getActivity(),currentDate);
        listView.setAdapter(adapter);

        DisplayRequired(currentDate);

        DisplayActuals(getPositionCovered(customUsers,currentDate));

    }

    public static int getPositionCovered(ArrayList<CustomUser>customUsers,Date date){

        Dayparts daypart = SCMTool.getActualCurrentDaypartStart();
        Calendar start = SCMTool.DaypartTimeToDate(daypart.start, date);
        Calendar end = SCMTool.DaypartTimeToDate(daypart.end, date);
        if (daypart.name.equalsIgnoreCase("6")) {
            end.add(Calendar.DATE, 1);
        }
        int employee = 0;
        final SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM/dd/yy");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        for (CustomUser user : customUsers){
            for (Schedule_Business_Site_Plan sched : user.getSchedules()){
                if (dateFormat.format(sched.date).equalsIgnoreCase(dateFormat.format(date))){

                    final SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm");
                    timeFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));

                    Calendar startTime = DaypartTimeToDate(timeFormatter.format(sched.startTime), date);
                    Calendar endTime = DaypartTimeToDate(timeFormatter.format(sched.endTime), date);

                    if (SCMTool.isBetween(startTime.getTime(),endTime.getTime(),start.getTime())){
                        employee++;
                    }else if (SCMTool.isBetween(startTime.getTime(),endTime.getTime(),end.getTime())){

                        employee++;
                    }
                }
            }
        }
        return employee;
    }


    private ArrayList<CustomUser> getActiveAndFilteredAUsers(){

        ArrayList<CustomUser>customUsers = SCMTool.getActiveCustomUser(SCMDataManager.getInstance().getStoreUsers());

        Collections.sort(customUsers, new Comparator<CustomUser>() {
            @Override
            public int compare(CustomUser o1, CustomUser o2) {
                return o1.getUser().firstName.compareToIgnoreCase(o2.getUser().firstName);
            }
        });

        return customUsers;
    }


    @Override
    public void onStart() {
        super.onStart();

    }
    public static boolean isBetween(final Date min, final Date max, final Date date){
        return date.after(min) && date.before(max);
    }

    private String ReadTimeFromCode(){

        DashboardKitchenData dashboardKitchenData = SCMDataManager.getInstance().getDashboardKitchenData();
        String tempCode = dashboardKitchenData.kitchenTemplateCode;
        String readTime = "--";
        SimpleDateFormat format = new SimpleDateFormat("h:mm a");
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        String timeCode = tempCode.replace("-"," ");
        timeCode = timeCode.replace("_"," ");

        StringBuilder time = new StringBuilder();
        Matcher m = p.matcher(timeCode);
        int counter = 0;
        try {
            if (! m.find())
                System.out.println("Has no time");
            else
                do {
                    if (counter == 0){
                        Date d = dateFormat.parse(m.group());
                        time.append(format.format(d));
                        time.append(" to ");

                    }else if (counter == 1){
                        Date d = dateFormat.parse(m.group());
                        time.append(format.format(d));

                    }
                    counter++;

                } while (m.find());

            if (time.toString().length()>0){
                readTime = time.toString();
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return readTime;
    }

}
