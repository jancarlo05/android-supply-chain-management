package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_CommunicationNotes;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.apimmdatamanagerlib.SMSCommunicationNotes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Dashboard_CommunicationNotes extends AppCompatActivity {

    private TextView title,notes_message;
    private Button back;
    private ProgressBar progressBar;
    private ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_communication_notes);

        title = findViewById(R.id.username);
        listView = findViewById(R.id.listView);
        back = findViewById(R.id.home);
        notes_message = findViewById(R.id.notes_message);

        setUpOnclicks();

        setUpSiteInformation();

    }

    private void setUpOnclicks(){
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setUpSiteInformation(){
        try {
            title.setText(SCMDataManager.getInstance().getSelectedSite().getSite().SiteName);
            if (SCMDataManager.getInstance().getSmsCommunicationNotes()!=null){
                if (SCMDataManager.getInstance().getSmsCommunicationNotes().size()>0){
                    Collections.sort(SCMDataManager.getInstance().getSmsCommunicationNotes(), new Comparator<SMSCommunicationNotes>() {
                        @Override
                        public int compare(SMSCommunicationNotes o1, SMSCommunicationNotes o2) {
                            if (o1.timestamp == null || o2.timestamp == null)
                                return 0;
                            return o2.timestamp.compareTo(o1.timestamp);
                        }
                    });
                    DashboardCommunicatonNotesListAdapter adapter = new DashboardCommunicatonNotesListAdapter(this,SCMDataManager.getInstance().getSmsCommunicationNotes());
                    listView.setAdapter(adapter);
                    notes_message.setVisibility(View.GONE);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }





    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
