package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_SalePerformance;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.Dashboard_Staffing_Activity;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.SiteSales;
import com.procuro.apimmdatamanagerlib.SiteSalesActual;
import com.procuro.apimmdatamanagerlib.SiteSalesData;
import com.procuro.apimmdatamanagerlib.SiteSalesDaypart;
import com.procuro.apimmdatamanagerlib.SiteSalesDefault;
import com.procuro.apimmdatamanagerlib.SiteSalesDefaultData;
import com.procuro.apimmdatamanagerlib.SiteSalesForecast;
import com.procuro.apimmdatamanagerlib.SiteSalesForecastTime;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;

public class Dashboard_SalesPerformance extends AppCompatActivity {

    TextView title,date;
    Button back,laborHrsButton;
    RadioButton mon,tue,wed,thu,fri,sat,sun,dp1,dp2,dp3,dp4,dp5,dp6;
    public static info.hoang8f.android.segmented.SegmentedGroup days_segmented_group,daypart_time_segmented_grp;
    RecyclerView week_daypart_header,week_daypart_item,daypart_time_item,daypart_time_header;
    Site site = SCMDataManager.getInstance().getSelectedSite().getSite();
    ProgressDialog progressDialog;
    public static TextView forecast,actual,difference;

    aPimmDataManager dataManager = aPimmDataManager.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_sales_performance);


        title = findViewById(R.id.username);
        back = findViewById(R.id.home);
        days_segmented_group = findViewById(R.id.days_segmented_group);
        date = findViewById(R.id.date);
        week_daypart_header = findViewById(R.id.week_daypart_header);
        week_daypart_item = findViewById(R.id.week_daypart_item);
        daypart_time_item = findViewById(R.id.daypart_time_item);
        daypart_time_header = findViewById(R.id.daypart_time_header);
        daypart_time_segmented_grp = findViewById(R.id.daypart_time_segmented_grp);
        laborHrsButton = findViewById(R.id.laborHrsButton);

        mon = findViewById(R.id.mon);
        tue = findViewById(R.id.tue);
        wed = findViewById(R.id.wed);
        thu = findViewById(R.id.thu);
        fri = findViewById(R.id.fri);
        sat = findViewById(R.id.sat);
        sun = findViewById(R.id.sun);
        dp1 = findViewById(R.id.dp1);
        dp2 = findViewById(R.id.dp2);
        dp3 = findViewById(R.id.dp3);
        dp4 = findViewById(R.id.dp4);
        dp5 = findViewById(R.id.dp5);
        dp6 = findViewById(R.id.dp6);

        forecast = findViewById(R.id.forecast);
        actual = findViewById(R.id.actual);
        difference = findViewById(R.id.difference);


        SetupOnclicks();
        title.setText(site.sitename);

        Calendar calendar = Calendar.getInstance();
        calendar = SCMTool.getDateWithDaypart(calendar.getTime().getTime(),site.effectiveUTCOffset);
        if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
            calendar.add(Calendar.WEEK_OF_YEAR,-1);
        }
        int week = calendar.get(Calendar.WEEK_OF_YEAR);
        CreateSegmentedWeekOfYear(week,calendar);

    }


    private void CreateSegmentedWeekOfYear(int WeekOFYear, final Calendar currentDate){
        try {
            Site site = SCMDataManager.getInstance().getSelectedSite().getSite();

            final SimpleDateFormat dateFormat = new SimpleDateFormat("EEE");

            SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
            format.setTimeZone(TimeZone.getTimeZone("UTC"));

            Calendar now = Calendar.getInstance();
            now.setTimeZone(TimeZone.getTimeZone("UTC"));
            now.set(Calendar.WEEK_OF_YEAR, WeekOFYear);
            now.add(Calendar.MINUTE,site.effectiveUTCOffset);

            ArrayList<Date> Weekdays = new ArrayList<>();
            int delta = -now.get(Calendar.DAY_OF_WEEK) + 2; //add 2 if your week start on monday
            now.add(Calendar.DAY_OF_MONTH, delta );

            for (int i = 0; i < 7; i++) {
                Weekdays.add(now.getTime());
                now.add(Calendar.DAY_OF_MONTH, 1);
            }

            Date start = Weekdays.get(0);
            Date end = Weekdays.get(Weekdays.size()-1);

            date.setText(format.format(start));
            date.append(" to ");
            date.append(format.format(end));

            Calendar prevDate = Calendar.getInstance();
            prevDate.setTime(start);
            prevDate.setTimeZone(TimeZone.getTimeZone("UTC"));
            prevDate.add(Calendar.DATE,-1);



            final SCMDataManager scm = SCMDataManager.getInstance();
            if (scm.getSiteSales()!=null){

                days_segmented_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        RadioButton radioButton = group.findViewById(checkedId);
                        setUpSiteSalesByDate(radioButton.getText().toString(),scm.getSiteSales(),scm.getPrevSiteSales());
                    }
                });

                for (int j = 0; j <days_segmented_group.getChildCount() ; j++) {
                    RadioButton radioButton = (RadioButton) days_segmented_group.getChildAt(j);
                    if (radioButton.getText().toString().equalsIgnoreCase(dateFormat.format(currentDate.getTime()))){
                        radioButton.setChecked(true);
                    }
                }

            }else {
                DownloadSalesData(getPrevWeek(currentDate),Weekdays,currentDate.getTime());
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void DownloadSalesData(ArrayList<Date> prevdates, final ArrayList<Date>dates, final Date curretDate){
        final SimpleDateFormat format = new SimpleDateFormat("EEE");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please Wait");
        progressDialog.setMessage("Downloading Data");
        progressDialog.setCancelable(false);
        progressDialog.show();

        final SiteSales[] prevSiteSales = {null};
        final ArrayList<SiteSales>siteSalesList = new ArrayList<>();
        final ArrayList<SiteSales>prevsiteSalesList = new ArrayList<>();
        final int[] counter = {0};

        for (final Date date : prevdates){
            dataManager.getSiteSalesByDate(site.siteid, date, new OnCompleteListeners.getSiteSalesByDateListener() {
                @Override
                public void getSiteSalesByDateCallback(SiteSales siteSales, Error error) {
                    if (error == null){
                        prevsiteSalesList.add(siteSales);
                    }else {
                        SiteSales siteSale = new SiteSales();
                        siteSale.salesDate = date;
                        prevsiteSalesList.add(siteSale);
                    }
                }
            });
        }

        for (final Date date : dates){
            dataManager.getSiteSalesByDate(site.siteid, date, new OnCompleteListeners.getSiteSalesByDateListener() {
                @Override
                public void getSiteSalesByDateCallback(SiteSales siteSales, Error error) {
                    counter[0]++;
                    if (error == null){
                        if (siteSales!=null && siteSales.data!=null){
                            siteSalesList.add(siteSales);
                            if (siteSales.data.dss!=0){
                                if (counter[0] == dates.size()){
                                    DisplaySelectedData(siteSalesList,prevsiteSalesList,curretDate);
                                    SCMDataManager.getInstance().setSiteSales(siteSalesList);
                                    SCMDataManager.getInstance().setPrevSiteSales(prevsiteSalesList);
                                }
                            }
                            else if (siteSales.data.projected!=0){
                                if (counter[0] == dates.size()){
                                    DisplaySelectedData(siteSalesList,prevsiteSalesList,curretDate);
                                    SCMDataManager.getInstance().setSiteSales(siteSalesList);
                                    SCMDataManager.getInstance().setPrevSiteSales(prevsiteSalesList);;
                                }
                            }
                            else if (siteSales.data.sales!=null && siteSales.data.sales.size()>0){
                                if (counter[0] == dates.size()){
                                    DisplaySelectedData(siteSalesList,prevsiteSalesList,curretDate);
                                    SCMDataManager.getInstance().setSiteSales(siteSalesList);
                                    SCMDataManager.getInstance().setPrevSiteSales(prevsiteSalesList);
                                }

                            }else {
                                SiteSales siteSale = new SiteSales();
                                siteSale.salesDate = date;
                                getSiteDefault(siteSalesList,prevsiteSalesList,date,siteSale,counter[0],dates.size());
                            }
                        }else {
                            SiteSales siteSale = new SiteSales();
                            siteSale.salesDate = date;
                            getSiteDefault(siteSalesList,prevsiteSalesList,date,siteSale,counter[0],dates.size());
                        }
                    }
                    else {
                        SiteSales siteSale = new SiteSales();
                        siteSale.salesDate = date;
                        getSiteDefault(siteSalesList,prevsiteSalesList,date,siteSale,counter[0],dates.size());
                    }
                }
            });
        }
    }

    private void DisplaySelectedData(final ArrayList<SiteSales>siteSales, final ArrayList<SiteSales>prevSiteSales, Date currentDate){
        try {

            Date date = SCMTool.getCurrentDateWithTimeZone().getTime();
            SCMDataManager.getInstance().setPrevSiteSales(siteSales);
            SCMDataManager.getInstance().setSiteSales(prevSiteSales);
            final SimpleDateFormat format = new SimpleDateFormat("EEE");
            days_segmented_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    RadioButton radioButton = group.findViewById(checkedId);
                    setUpSiteSalesByDate(radioButton.getText().toString(),siteSales,prevSiteSales);
                }
            });

            for (int j = 0; j <days_segmented_group.getChildCount() ; j++) {
                RadioButton radioButton = (RadioButton) days_segmented_group.getChildAt(j);
                if (radioButton.getText().toString().equalsIgnoreCase(format.format(date))){
                    radioButton.setChecked(true);
                }
            }
            if (progressDialog!=null){
                if (progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setUpSiteSalesByDate(String curretDay, ArrayList<SiteSales>siteSales,ArrayList<SiteSales> prevSiteSales){
        SiteSales prev = prevSiteSales.get(0);
        SimpleDateFormat format = new SimpleDateFormat("EEE");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        ArrayList<SiteSales>sales = new ArrayList<>(siteSales);


        for (int i = 0; i <sales.size() ; i++) {
            SiteSales siteSale = sales.get(i);

            if (format.format(siteSale.salesDate).equalsIgnoreCase(curretDay)){
                if (i==0){
                    prev = CheckPrevSiteSales(prevSiteSales);
                }
                else{
                    prev = getPrevSiteSalesInCurrentWeek(siteSales,prevSiteSales,siteSale.salesDate);
                }
                CreateWeekTitleHeader(siteSale,prev);
                break;
            }
        }
    }

    public static SiteSales getPrevSiteSalesInCurrentWeek(ArrayList<SiteSales>siteSales,
                                                          ArrayList<SiteSales>prevSiteSales,Date selectedDate){

        Collections.sort(siteSales, new Comparator<SiteSales>() {
            @Override
            public int compare(SiteSales o1, SiteSales o2) {
                return o1.salesDate.compareTo(o2.salesDate);
            }
        });

        SimpleDateFormat format = new SimpleDateFormat("EEEE, MM dd YYYY");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        SiteSales prev = null;
        for (int i = 0; i <siteSales.size() ; i++) {
            SiteSales sales = siteSales.get(i);
            if (sales.salesDate.compareTo(selectedDate)<0){
                if (sales.data!=null){
                    if (sales.data.dss!=0 || sales.data.projected!=0){
                        prev = sales;
                    }
                }

            }
        }
        if (prev!=null){
            return prev;
        }else {
          return CheckPrevSiteSales(prevSiteSales);
        }
    }

    private void CreateWeekTitleHeader(SiteSales siteSale,SiteSales prev ){
        ArrayList<String>header = new ArrayList<>();
        header.add("Yesterday");
        header.add("Total");
        header.add("DP1");
        header.add("DP2");
        header.add("DP3");
        header.add("DP4");
        header.add("DP5");
        header.add("DP6");
        DisplayHeader(header,week_daypart_header);


        ArrayList<String>items = new ArrayList<>();
        items.add("Forecast");
        items.add("Actual");
        items.add("(+/-)");
        DisplayItems(header,items,week_daypart_item,siteSale,prev);


        setUPdayparts(siteSale);

    }

    private void SetupOnclicks(){
        try {
            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            laborHrsButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Dashboard_SalesPerformance.this, Dashboard_Staffing_Activity.class);
                    startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(Dashboard_SalesPerformance.this).toBundle());
                    finish();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setUPdayparts(final SiteSales siteSales){

        daypart_time_segmented_grp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == dp1.getId()){
                    CreateDaypart1(siteSales);
                }else if (checkedId == dp2.getId()){
                    CreateDaypart2(siteSales);
                }else if (checkedId == dp3.getId()){
                    CreateDaypart3(siteSales);
                }else if (checkedId == dp4.getId()){
                    CreateDaypart4(siteSales);
                }else if (checkedId == dp5.getId()){
                    CreateDaypart5(siteSales);
                }else if (checkedId == dp6.getId()){
                    CreateDaypart6(siteSales);
                }
            }
        });
        dp2.setChecked(true);
        dp1.setChecked(true);
    }

    private void CreateDaypart1( SiteSales siteSales){
        ArrayList<SalesPerformanceDaypartHeader>times = new ArrayList<>();

        Calendar start = SCMTool.ISOTimeToDate("4:00",siteSales.salesDate);
        Calendar end = SCMTool.ISOTimeToDate("5:00",siteSales.salesDate);

        times.add(new SalesPerformanceDaypartHeader("4-5","4:00","5:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("5:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("6:00",siteSales.salesDate);

        times.add(new SalesPerformanceDaypartHeader("5-6","5:00","6:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("6:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("7:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("6-7","6:00","7:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("7:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("8:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("7-8","7:00","8:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("8:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("9:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("8-9","8:00","9:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("9:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("10:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("9-10","9:00","10:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("10:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("11:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("10-11",true,"10:00","11:00",start.getTime(),end.getTime()));


        DisplayDaypartHeader(times,daypart_time_header);

        ArrayList<String>items = getDaypartItem();
        DisplayDaypartItems(times,items,daypart_time_item,siteSales,"1");

    }

    private void CreateDaypart2( SiteSales siteSales){
        ArrayList<SalesPerformanceDaypartHeader>times = new ArrayList<>();

        Calendar start = SCMTool.ISOTimeToDate("10:00",siteSales.salesDate);
        Calendar end = SCMTool.ISOTimeToDate("11:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("10-11",true,"10:00","11:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("11:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("12:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("11-12","11:00","12:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("12:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("13:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("12-1","12:00","13:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("13:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("14:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("1-2","13:00","14:00",start.getTime(),end.getTime()));

        DisplayDaypartHeader(times,daypart_time_header);

        ArrayList<String>items = getDaypartItem();
        DisplayDaypartItems(times,items,daypart_time_item,siteSales,"2");

    }

    private void CreateDaypart3( SiteSales siteSales){

        ArrayList<SalesPerformanceDaypartHeader>times = new ArrayList<>();

        Calendar start = SCMTool.ISOTimeToDate("14:00",siteSales.salesDate);
        Calendar end = SCMTool.ISOTimeToDate("15:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("2-3","14:00","15:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("15:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("16:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("3-4","15:00","16:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("16:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("17:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("4-5","16:00","17:00",start.getTime(),end.getTime()));

        DisplayDaypartHeader(times,daypart_time_header);

        ArrayList<String>items = getDaypartItem();
        DisplayDaypartItems(times,items,daypart_time_item,siteSales,"3");

    }

    private void CreateDaypart4( SiteSales siteSales){
        ArrayList<SalesPerformanceDaypartHeader>times = new ArrayList<>();

        Calendar start = SCMTool.ISOTimeToDate("17:00",siteSales.salesDate);
        Calendar end = SCMTool.ISOTimeToDate("18:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("5-6","17:00","18:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("18:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("19:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("6-7","18:00","19:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("19:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("20:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("7-8","19:00","20:00",start.getTime(),end.getTime()));

        DisplayDaypartHeader(times,daypart_time_header);

        ArrayList<String>items = getDaypartItem();
        DisplayDaypartItems(times,items,daypart_time_item,siteSales,"4");
    }

    private void CreateDaypart5( SiteSales siteSales){
        ArrayList<SalesPerformanceDaypartHeader>times = new ArrayList<>();

        Calendar start = SCMTool.ISOTimeToDate("20:00",siteSales.salesDate);
        Calendar end = SCMTool.ISOTimeToDate("21:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("8-9","20:00","21:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("21:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDate("22:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("9-10","21:00","22:00",start.getTime(),end.getTime()));

        DisplayDaypartHeader(times,daypart_time_header);

        ArrayList<String>items = getDaypartItem();
        DisplayDaypartItems(times,items,daypart_time_item,siteSales,"5");

    }

    private void CreateDaypart6( SiteSales siteSales){
        ArrayList<SalesPerformanceDaypartHeader>times = new ArrayList<>();

        Calendar start = SCMTool.ISOTimeToDate("22:00",siteSales.salesDate);
        Calendar end = SCMTool.ISOTimeToDate("23:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("10-11","22:00","23:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDate("23:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDatePlus1("0:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("11-12","23:00","0:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDatePlus1("0:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDatePlus1("1:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("12-1","0:00","1:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDatePlus1("1:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDatePlus1("2:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("1-2","1:00","2:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDatePlus1("2:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDatePlus1("3:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("2-3","2:00","3:00",start.getTime(),end.getTime()));

        start = SCMTool.ISOTimeToDatePlus1("3:00",siteSales.salesDate);
        end = SCMTool.ISOTimeToDatePlus1("4:00",siteSales.salesDate);
        times.add(new SalesPerformanceDaypartHeader("3-4","3:00","4:00",start.getTime(),end.getTime()));

        DisplayDaypartHeader(times,daypart_time_header);

        ArrayList<String>items = getDaypartItem();
        DisplayDaypartItems(times,items,daypart_time_item,siteSales,"6");

    }

    private void DisplayDaypartHeader(ArrayList<SalesPerformanceDaypartHeader>header,RecyclerView recyclerView){
        Dashboard_SalesPerformance_Daypart_Header_RecyclerViewAdapter adapter = new Dashboard_SalesPerformance_Daypart_Header_RecyclerViewAdapter(this,header);
        recyclerView.setLayoutManager(new GridLayoutManager(this,header.size()));
        recyclerView.setAdapter(adapter);

    }

    private void DisplayDaypartItems(ArrayList<SalesPerformanceDaypartHeader>header,ArrayList<String>item,RecyclerView recyclerView,
                              SiteSales siteSales ,String daypartSeleted){
        Dashboard_SalesPerfornace_Daypart_item_RecyclerViewAdapter adapter = new Dashboard_SalesPerfornace_Daypart_item_RecyclerViewAdapter(this,item,header,siteSales,daypartSeleted);
        recyclerView.setLayoutManager(new GridLayoutManager(this,1));
        recyclerView.setAdapter(adapter);


    }

    private void DisplayHeader(ArrayList<String>header,RecyclerView recyclerView){
        Dashboard_SalesPerformance_Week_Header_RecyclerViewAdapter adapter = new Dashboard_SalesPerformance_Week_Header_RecyclerViewAdapter(this,header);
        recyclerView.setLayoutManager(new GridLayoutManager(this,header.size()));
        recyclerView.setAdapter(adapter);

    }

    private void DisplayItems(ArrayList<String>time,ArrayList<String>item,RecyclerView recyclerView,
                              SiteSales siteSales,SiteSales prev ){

        Dashboard_SalesPerfornace_Week_item_RecyclerViewAdapter adapter = new Dashboard_SalesPerfornace_Week_item_RecyclerViewAdapter(this,item,time,siteSales,prev);
        recyclerView.setLayoutManager(new GridLayoutManager(this,1));
        recyclerView.setAdapter(adapter);
    }

    private ArrayList<Date> getPrevWeek(Calendar currentDate){
        ArrayList<Date> Weekdays = new ArrayList<>();
        try {
            Calendar now = Calendar.getInstance();
            now.setTimeZone(TimeZone.getTimeZone("UTC"));
            now.setTime(currentDate.getTime());
            now.add(Calendar.WEEK_OF_YEAR, -1);


            int delta = -now.get(Calendar.DAY_OF_WEEK) + 2; //add 2 if your week start on monday
            now.add(Calendar.DAY_OF_MONTH, delta );

            for (int i = 0; i < 7; i++) {
                Weekdays.add(now.getTime());
                now.add(Calendar.DAY_OF_MONTH, 1);
            }

            Collections.reverse(Weekdays);

        }catch (Exception e){
            e.printStackTrace();
        }

        return Weekdays;
    }

    public static SiteSales CheckPrevSiteSales(ArrayList<SiteSales>siteSales){

        for (SiteSales prevsiteSale: siteSales){
            if (prevsiteSale!=null){
                if (prevsiteSale.data!=null){
                    if (prevsiteSale.data.dss!=0){
                        return prevsiteSale;
                    }else if (prevsiteSale.data.projected!=0){
                        return prevsiteSale;
                    }
                }
            }
        }
        return new SiteSales();
    }

    private ArrayList<String> getDaypartItem(){
        ArrayList<String>items = new ArrayList<>();
        items.add("Forecast");
        items.add("Hourly Sales");
        items.add("(+/-)");
        return items;
    }

    public static SiteSalesData ConvertDefaultToSales(SiteSalesDefault salesDefault, Date date,
                                                ArrayList<SiteSales>siteSales,ArrayList<SiteSales>prevSiteSales){
        SiteSalesData data = new SiteSalesData();

        SiteSales sales = getPrevSiteSalesInCurrentWeek(siteSales,prevSiteSales,date);

        if (sales.data!=null){
            if (sales.data.sales!=null && sales.data.sales.size()>0){
                double amount = 0;
                for (SiteSalesActual actual : sales.data.sales){
                    amount +=actual.amount;
                }
                data.projected = (int)amount;
            }
            else if (sales.data.dss !=0){
                data.projected = sales.data.dss;
            }
            else if (sales.data.projected !=0){
                data.projected = sales.data.projected;
            }
        }

        ArrayList<SiteSalesDaypart>dayparts = new ArrayList<>();
        dayparts.add(getDaypart1(date,salesDefault));
        dayparts.add(getDaypart2(date,salesDefault));
        dayparts.add(getDaypart3(date,salesDefault));
        dayparts.add(getDaypart4(date,salesDefault));
        dayparts.add(getDaypart5(date,salesDefault));
        dayparts.add(getDaypart6(date,salesDefault));
        data.dayparts = dayparts;
        data.isFromDefault = true;
        return data;
    }

    public static SiteSalesDaypart getDaypart1(Date date, SiteSalesDefault salesDefault){

        SiteSalesDaypart salesDaypart = new SiteSalesDaypart();

        Calendar start = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        start.setTime(date);
        start.set(Calendar.HOUR,4);
        start.set(Calendar.MINUTE,0);
        start.set(Calendar.SECOND,0);
        start.set(Calendar.MILLISECOND,0);
        start.set(Calendar.AM_PM,Calendar.AM);

        Calendar end = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        end.setTime(date);
        end.set(Calendar.HOUR,10);
        end.set(Calendar.MINUTE,30);
        end.set(Calendar.SECOND,0);
        end.set(Calendar.MILLISECOND,0);
        end.set(Calendar.AM_PM,Calendar.AM);

        salesDaypart.startTime = start.getTime();
        salesDaypart.endTime = end.getTime();
        salesDaypart.name= "1";
        salesDaypart.forecast = getDaypartForcastByDaypart(date,salesDefault,start.getTime(),end.getTime());

        return salesDaypart;
    }

    public static SiteSalesDaypart getDaypart2(Date date, SiteSalesDefault salesDefault){

        SiteSalesDaypart salesDaypart = new SiteSalesDaypart();

        Calendar start = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        start.setTime(date);
        start.set(Calendar.HOUR,10);
        start.set(Calendar.MINUTE,30);
        start.set(Calendar.SECOND,0);
        start.set(Calendar.MILLISECOND,0);
        start.set(Calendar.AM_PM,Calendar.AM);

        Calendar end = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        end.setTime(date);
        end.set(Calendar.HOUR,2);
        end.set(Calendar.MINUTE,0);
        end.set(Calendar.SECOND,0);
        end.set(Calendar.MILLISECOND,0);
        end.set(Calendar.AM_PM,Calendar.PM);

        salesDaypart.startTime = start.getTime();
        salesDaypart.endTime = end.getTime();
        salesDaypart.name= "2";
        salesDaypart.forecast = getDaypartForcastByDaypart(date,salesDefault,start.getTime(),end.getTime());

        return salesDaypart;
    }

    public static SiteSalesDaypart getDaypart3(Date date, SiteSalesDefault salesDefault){

        SiteSalesDaypart salesDaypart = new SiteSalesDaypart();

        Calendar start = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        start.setTime(date);
        start.set(Calendar.HOUR,2);
        start.set(Calendar.MINUTE,0);
        start.set(Calendar.SECOND,0);
        start.set(Calendar.MILLISECOND,0);
        start.set(Calendar.AM_PM,Calendar.PM);

        Calendar end = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        end.setTime(date);
        end.set(Calendar.HOUR,5);
        end.set(Calendar.MINUTE,0);
        end.set(Calendar.SECOND,0);
        end.set(Calendar.MILLISECOND,0);
        end.set(Calendar.AM_PM,Calendar.PM);

        salesDaypart.startTime = start.getTime();
        salesDaypart.endTime = end.getTime();
        salesDaypart.name= "3";
        salesDaypart.forecast = getDaypartForcastByDaypart(date,salesDefault,start.getTime(),end.getTime());

        return salesDaypart;
    }

    public static SiteSalesDaypart getDaypart4(Date date, SiteSalesDefault salesDefault){

        SiteSalesDaypart salesDaypart = new SiteSalesDaypart();

        Calendar start = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        start.setTime(date);
        start.set(Calendar.HOUR,5);
        start.set(Calendar.MINUTE,0);
        start.set(Calendar.SECOND,0);
        start.set(Calendar.MILLISECOND,0);
        start.set(Calendar.AM_PM,Calendar.PM);

        Calendar end = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        end.setTime(date);
        end.set(Calendar.HOUR,8);
        end.set(Calendar.MINUTE,0);
        end.set(Calendar.SECOND,0);
        end.set(Calendar.MILLISECOND,0);
        end.set(Calendar.AM_PM,Calendar.PM);

        salesDaypart.startTime = start.getTime();
        salesDaypart.endTime = end.getTime();
        salesDaypart.name= "4";
        salesDaypart.forecast = getDaypartForcastByDaypart(date,salesDefault,start.getTime(),end.getTime());

        return salesDaypart;
    }

    public static SiteSalesDaypart getDaypart5(Date date, SiteSalesDefault salesDefault){

        SiteSalesDaypart salesDaypart = new SiteSalesDaypart();

        Calendar start = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        start.setTime(date);
        start.set(Calendar.HOUR,8);
        start.set(Calendar.MINUTE,0);
        start.set(Calendar.SECOND,0);
        start.set(Calendar.MILLISECOND,0);
        start.set(Calendar.AM_PM,Calendar.PM);

        Calendar end = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        end.setTime(date);
        end.set(Calendar.HOUR,10);
        end.set(Calendar.MINUTE,0);
        end.set(Calendar.SECOND,0);
        end.set(Calendar.MILLISECOND,0);
        end.set(Calendar.AM_PM,Calendar.PM);

        salesDaypart.startTime = start.getTime();
        salesDaypart.endTime = end.getTime();
        salesDaypart.name= "5";
        salesDaypart.forecast = getDaypartForcastByDaypart(date,salesDefault,start.getTime(),end.getTime());

        return salesDaypart;
    }

    public static SiteSalesDaypart getDaypart6(Date date, SiteSalesDefault salesDefault){

        SiteSalesDaypart salesDaypart = new SiteSalesDaypart();

        Calendar start = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        start.setTime(date);
        start.set(Calendar.HOUR,10);
        start.set(Calendar.MINUTE,0);
        start.set(Calendar.SECOND,0);
        start.set(Calendar.MILLISECOND,0);
        start.set(Calendar.AM_PM,Calendar.PM);

        Calendar end = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        end.setTime(date);
        end.add(Calendar.DATE,1);
        end.set(Calendar.HOUR,4);
        end.set(Calendar.MINUTE,0);
        end.set(Calendar.SECOND,0);
        end.set(Calendar.MILLISECOND,0);
        end.set(Calendar.AM_PM,Calendar.AM);

        salesDaypart.startTime = start.getTime();
        salesDaypart.endTime = end.getTime();
        salesDaypart.name= "6";
        salesDaypart.forecast = getDaypartForcastByDaypart(date,salesDefault,start.getTime(),end.getTime());

        return salesDaypart;
    }

    public static ArrayList<SiteSalesForecast> getDaypartForcastByDaypart(Date date, SiteSalesDefault salesDefault,Date start,Date end){

        SimpleDateFormat format = new SimpleDateFormat("EEE MM dd yyyy H:mm a");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        ArrayList<SiteSalesForecast>forecasts = new ArrayList<>();

        if (salesDefault!=null){
            if (salesDefault.data!=null){
                for (SiteSalesDefaultData data : salesDefault.data){
                    for (SiteSalesForecastTime time : data.forecast){

                        Calendar forecastStart = SCMTool.ISOTimeToDate(time.startTime,date);
                        Calendar forecastEnd = SCMTool.ISOTimeToDate(time.endTime,date);

                        String startName = format.format(forecastStart.getTime());

                        if (startName.equalsIgnoreCase("1:00") || startName.equalsIgnoreCase("1:30")
                                || startName.equalsIgnoreCase("2:00") || startName.equalsIgnoreCase("2:30")
                                || startName.equalsIgnoreCase("3:00") || startName.equalsIgnoreCase("3:30")
                                || startName.equalsIgnoreCase("0:00") || startName.equalsIgnoreCase("0:30")){

                            forecastStart.add(Calendar.DATE,1);
                            forecastEnd.add(Calendar.DATE,1);
                        }

                        SiteSalesForecast salesForecast = new SiteSalesForecast();
                        salesForecast.percentage = time.percentage;
                        salesForecast.startTime = forecastStart.getTime();
                        salesForecast.endTime = forecastEnd.getTime();

                        if (SCMTool.isBetween(start,end,forecastStart.getTime())){
                            if (SCMTool.isBetween(start,end,forecastEnd.getTime())){
                                forecasts.add(salesForecast);
                            }
                        }
                    }
                }
            }
        }

        Collections.sort(forecasts, new Comparator<SiteSalesForecast>() {
            @Override
            public int compare(SiteSalesForecast o1, SiteSalesForecast o2) {
                return o1.startTime.compareTo(o2.startTime);
            }
        });

        return forecasts;
    }

    private  void getSiteDefault(final ArrayList<SiteSales>siteSales,
                                      final ArrayList<SiteSales>prevSiteSales,
                                      final Date date, final SiteSales sales,
                                      final int DownloadedItems, final int TotalItems){

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.setTime(date);
        SimpleDateFormat format = new SimpleDateFormat("EEE");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        String StringDayOfWeek = format.format(date);
        int dayOfWeek = 0;

        if (StringDayOfWeek.equalsIgnoreCase("Tue")){
            dayOfWeek = 1;
        }else if (StringDayOfWeek.equalsIgnoreCase("Wed")){
            dayOfWeek = 2;
        }else if (StringDayOfWeek.equalsIgnoreCase("Thu")){
            dayOfWeek = 3;
        }else if (StringDayOfWeek.equalsIgnoreCase("Fri")){
            dayOfWeek = 4;
        }else if (StringDayOfWeek.equalsIgnoreCase("Sat")){
            dayOfWeek = 5;
        }else if (StringDayOfWeek.equalsIgnoreCase("Sun")){
            dayOfWeek = 6;
        }

       aPimmDataManager dataManager = aPimmDataManager.getInstance();
        dataManager.getDefaultSiteSalesForStoreType(dayOfWeek, 1, new OnCompleteListeners.getSiteSalesDefaultListener() {
            @Override
            public void getSiteSalesDefaultCallback(final SiteSalesDefault salesDefault, Error error) {
                if (error == null){
                  sales.data = ConvertDefaultToSales(salesDefault,date,siteSales,prevSiteSales);
                  siteSales.add(sales);

                  if (DownloadedItems == TotalItems){
                      DisplaySelectedData(siteSales,prevSiteSales,date);
                      SCMDataManager.getInstance().setSiteSales(siteSales);
                      SCMDataManager.getInstance().setPrevSiteSales(prevSiteSales);
                  }
                }else {
                    getSiteDefault(siteSales,prevSiteSales,date,sales,DownloadedItems,TotalItems);
                }
            }
        });
    }

}
