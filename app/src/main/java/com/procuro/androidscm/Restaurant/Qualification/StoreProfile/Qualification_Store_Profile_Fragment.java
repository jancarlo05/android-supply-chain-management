package com.procuro.androidscm.Restaurant.Qualification.StoreProfile;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.fragment.app.Fragment;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.Qualification.QualificationActivity;
import com.procuro.androidscm.Restaurant.Qualification.QualificationActivityFragment;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.EquipmenList.EquipmenlistData;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.FormsList.FormListData;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.HoursOfOperation.HoursOfOpetationData;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.Qualification_StoreInfo_Fragment;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.StoreProfileOptions.StoreProfileOptionsData;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.TeamAndContancts.ManagementTeam.ManagementTeamData;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.TeamAndContancts.Qualification_TeamAndContancts_Fragment;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.DocumentDTO;
import com.procuro.apimmdatamanagerlib.HoursOfOperation;
import com.procuro.apimmdatamanagerlib.IFTA_ECM;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;
import com.procuro.apimmthriftservices.services.NamedServices;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsConfiguration;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsEquipment;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsSiteConfig;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsUserRole;

import org.apache.thrift.TException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Objects;


public class Qualification_Store_Profile_Fragment extends Fragment {


    private info.hoang8f.android.segmented.SegmentedGroup infogrp;
    private RadioButton store_profile,teams;



    public Qualification_Store_Profile_Fragment() {

        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.qualifications_store_profile_fragment, container, false);

        infogrp = view.findViewById(R.id.segmented2);
        store_profile = view.findViewById(R.id.store_profile);
        teams = view.findViewById(R.id.teams);

        setupOnclicks();

        DownloadDataRequiements();

        return view;
    }


    private void DownloadDataRequiements(){
        ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("Please Wait");
        progressDialog.setMessage("Downloading Data");
        progressDialog.setCancelable(false);
        progressDialog.show();

        try {

            QualificationData data = QualificationData.getInstance();
            CustomSite site = data.getSelectedSite();

            DownloadHoursOfOperationData(site.getSite().siteid);

            QualificationActivityFragment.name.setText(site.getSite().sitename);

            if (data.getUsers() !=null){
                if (EquipmenlistData.getInstance().getSiteSettingsEquipments()==null){
                    startSiteSettingsEquipement(site.getSite(),progressDialog);
                }else {
                    progressDialog.dismiss();
                    store_profile.setChecked(true);
                }
            }else {
                getStoreUsers(site.getSite(),progressDialog);
            }


        }catch (Exception e){
            e.printStackTrace();
            progressDialog.dismiss();
        }

    }

    private void setupOnclicks(){

        infogrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == store_profile.getId()){
                    Display_StoreInfo();
                }else if (checkedId == teams.getId()){
                    Display_Teams();
                }
            }
        });
    }

    public void Display_StoreInfo() {
        Fragment fragment = new Qualification_StoreInfo_Fragment();
        requireActivity().getSupportFragmentManager().beginTransaction().replace(R.id.store_profile_fragment_container, fragment).commit();
    }

    private void Display_Teams() {
        Fragment fragment = new Qualification_TeamAndContancts_Fragment();
        requireActivity().getSupportFragmentManager().beginTransaction().replace(R.id.store_profile_fragment_container, fragment).commit();
    }

    private void getStoreUsers(final Site site, final ProgressDialog progressDialog){
        aPimmDataManager dataManager = aPimmDataManager.getInstance();
        dataManager.getStoreUsers(site.siteid, new OnCompleteListeners.getStoreUsersCallbackListener() {
            @Override
            public void getStoreUsersCallback(ArrayList<User> storeUsers, Error error) {
                if (error == null) {
                    QualificationData.getInstance().setUsers(SCMTool.ConvertUserToCustomUser(storeUsers));
                    DownloadTriftApis(progressDialog,site);
                }else {
                    DownloadTriftApis(progressDialog,site);
                }
            }
        });
    }

    private void DownloadTriftApis(ProgressDialog progressDialog,Site site){
        startSiteSettingsEquipement(site,progressDialog);
    }


    private void startSiteSettingsEquipement(final Site site, final ProgressDialog progressDialog) {
        Log.i("Thrift ","startSiteSettingsEquipement start");
        try {
            final String siteId = site.siteid;


            final NamedServices ns = NamedServices.getInstance();
            if (ns.pimmServer!=null){
                final SiteSettingsSiteConfig config = ns.siteSettingsClient;

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        ArrayList equipments = null;
                        try {
                            equipments = config.getSiteEquipment(siteId);

                            if (equipments!=null){
                                if (equipments.size()>0){
                                    ArrayList<SiteSettingsEquipment>newEquipment = (ArrayList<SiteSettingsEquipment>)equipments;
                                    EquipmenlistData data = new EquipmenlistData();
                                    data.setSiteSettingsEquipments(newEquipment);
                                    EquipmenlistData.setInstance(data);
//                                    SCMTool.DismissDialogInThrift(progressDialog,store_profile);

                                    startGetSiteProperties(site,progressDialog);

                                }else {
                                    getDefaultSiteSettingEquipment(site,progressDialog);
                                }
                            }

                        } catch (TException e) {
                            e.printStackTrace();
                            SCMTool.DismissDialogInThrift(progressDialog,store_profile);
                        }
                    }
                }).start();

            }else {
                Log.i("Thrift ","SERVER NULL ");
                SCMTool.DismissDialogInThrift(progressDialog,store_profile);
            }

            Log.i("Thrift ","startSiteSettingsEquipement End");

        }catch (TException e){
            e.printStackTrace();
            SCMTool.DismissDialogInThrift(progressDialog,store_profile);
        }

    }

    private void getDefaultSiteSettingEquipment(final Site site, final ProgressDialog progressDialog) {
        Log.i("Thrift ","startSiteSettingsEquipement start");
        try {
            final String siteId = site.siteid;
            final String customerId = site.CustomerId;

            final NamedServices ns = NamedServices.getInstance();
            if (ns.pimmServer!=null){
                final SiteSettingsSiteConfig config = ns.siteSettingsClient;

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        ArrayList equipments = null;
                        try {

                            equipments = config.getEquipment(customerId);
                            if (equipments!=null){
                                if (equipments.size()>0){
                                    ArrayList<SiteSettingsEquipment>newEquipment = (ArrayList<SiteSettingsEquipment>)equipments;

                                    for (SiteSettingsEquipment equipment : newEquipment){
                                        if (equipment.isDefault){
                                            equipment.inUse = true;
                                            equipment.setInUse(site.siteid,true);
                                        }
                                    }

                                    EquipmenlistData data = new EquipmenlistData();
                                    data.setSiteSettingsEquipments(newEquipment);
                                    EquipmenlistData.setInstance(data);

                                    saveDefaultSiteSettingEquipment(progressDialog,newEquipment);

                                }
                            }

                        } catch (TException e) {
                            e.printStackTrace();
                            SCMTool.DismissDialogInThrift(progressDialog,store_profile);
                        }
                    }
                }).start();

            }else {
                Log.i("Thrift ","SERVER NULL ");
                SCMTool.DismissDialogInThrift(progressDialog,store_profile);
            }

            Log.i("Thrift ","startSiteSettingsEquipement End");

        }catch (TException e){
            e.printStackTrace();
            SCMTool.DismissDialogInThrift(progressDialog,store_profile);
        }

    }

    private void saveDefaultSiteSettingEquipment(final ProgressDialog progressDialog, final ArrayList arrayList) {
        Log.i("Thrift ","saveDefaultSiteSettingEquipment start");
        try {
            final NamedServices ns = NamedServices.getInstance();
            if (ns.pimmServer!=null){
                final SiteSettingsSiteConfig config = ns.siteSettingsClient;

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            boolean updateSiteEquipment = config.updateSiteEquipment(arrayList);
                            if (updateSiteEquipment){
                                Log.i("Thrift","UPDATE SUCCESS");

                            }else {
                                Log.i("Thrift","UPDATE FAILED");
                            }
                            SCMTool.DismissDialogInThrift(progressDialog,store_profile);

                        } catch (TException e) {
                            e.printStackTrace();
                            SCMTool.DismissDialogInThrift(progressDialog,store_profile);
                        }
                    }
                }).start();

            }else {
                Log.i("Thrift ","SERVER NULL ");
                SCMTool.DismissDialogInThrift(progressDialog,store_profile);
            }

            Log.i("Thrift ","saveDefaultSiteSettingEquipment End");

        }catch (TException e){
            e.printStackTrace();
            SCMTool.DismissDialogInThrift(progressDialog,store_profile);
        }

    }

    private void startGetSiteProperties(final Site site, final ProgressDialog progressDialog) {
        Log.i("Thrift ","startGetSiteProperties start");
        try {
            final String siteId = site.siteid;

            final NamedServices ns = NamedServices.getInstance();
            if (ns.pimmServer!=null){
                final SiteSettingsSiteConfig config = ns.siteSettingsClient;

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        ArrayList sitesettingconfig = null;
                        ArrayList users = null;
                        try {
                            sitesettingconfig = config.getSiteProperties(siteId).configSettings;
                            users = config.getSiteProperties(siteId).users;

                            if (users!=null){
                                if (users.size()>0){
                                    ManagementTeamData data = new ManagementTeamData();
                                    data.setUserRoles(users);
                                    ManagementTeamData.setInstance(data);

                                    getUserDto(users);

                                }
                            }

                            if (sitesettingconfig!=null){
                                if (sitesettingconfig.size()>0){

                                    for (int i = 0; i <sitesettingconfig.size() ; i++) {
                                        SiteSettingsConfiguration siteconfig = (SiteSettingsConfiguration)sitesettingconfig.get(i);
                                        System.out.println("Name: "+siteconfig.name + " VALUE : "+siteconfig.value);
                                        if (siteconfig.name.equalsIgnoreCase("SMS:StoreConfiguration")){
                                            StoreProfileOptionsData data = new StoreProfileOptionsData();
                                            data.setKitchentype(siteconfig.value);
                                            StoreProfileOptionsData.setInstance(data);
                                        }
                                    }
                                    FormListData data = new FormListData();
                                    data.setSiteSettingsConfigurations(sitesettingconfig);
                                    FormListData.setInstance(data);
                                    SCMTool.DismissDialogInThrift(progressDialog,store_profile);
                                }else {
                                    getDefaultSiteSettingEquipment(site,progressDialog);
                                }
                            }

                        } catch (TException e) {
                            e.printStackTrace();
                            SCMTool.DismissDialogInThrift(progressDialog,store_profile);
                        }
                    }
                }).start();

            }else {
                Log.i("Thrift ","SERVER NULL ");
                SCMTool.DismissDialogInThrift(progressDialog,store_profile);
            }

            Log.i("Thrift ","startSiteSettingsEquipement End");

        }catch (TException e){
            e.printStackTrace();
            SCMTool.DismissDialogInThrift(progressDialog,store_profile);
        }
    }

    private void DownloadHoursOfOperationData(String siteid) {
        aPimmDataManager dataManager = aPimmDataManager.getInstance();
        dataManager.getDocumentListForReferenceId(siteid, new OnCompleteListeners.getDocumentListForReferenceIdCallbackListener() {
            @Override
            public void getDocumentListForReferenceIdCallback(ArrayList<DocumentDTO> documentDTOArrayList, Error error) {
                if (error == null) {
                    if (documentDTOArrayList != null) {
                        for (DocumentDTO documentDTO : documentDTOArrayList) {
                            if (documentDTO.category.equalsIgnoreCase("Hours_Of_Operation")) {
                                getDocumentContentForDocumentId(documentDTO.documentId,documentDTO.category);
                                break;
                            }
                        }
                    }
                }
            }
        });
    }

    private void getDocumentContentForDocumentId(String documentID,String category){
        aPimmDataManager dataManager = aPimmDataManager.getInstance();
        dataManager.getDocumentContentForDocumentId(documentID, category, new OnCompleteListeners.getDocumentContentForDocumentIdCallbackListener() {
            @Override
            public void getDocumentContentForDocumentIdCallback(Object Object, Error error) {
                if (error ==null){
                    try {
                        HoursOfOpetationData data =  new HoursOfOpetationData();
                        JSONObject jsonObject = (JSONObject) Object;

                        HoursOfOperation hoursOfOperation = new HoursOfOperation();
                        hoursOfOperation.readFromJSONObject(jsonObject);
                        data.setHoursOfOperation(hoursOfOperation);

                        HoursOfOpetationData.setInstance(data);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });
    }
    public void wrtieFileOnInternalStorage(String title,String sBody){
        System.out.println("wrtieFileOnInternalStorage: START");
        String root = Environment.getExternalStorageDirectory().toString();
        File file = new File(root);
        if(!file.exists()){
            file.mkdir();
        }else {
            file.delete();
        }
        try{
            File gpxfile = new File(file, title);
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();

            BufferedReader br = new BufferedReader(new FileReader(gpxfile));
            StringBuilder stringBuilder= new StringBuilder();
            String st;
            while ((st = br.readLine()) != null){
                stringBuilder.append(st);
            }

//            String s = stringBuilder.toString().replaceAll("[^\\x20-\\x7E]", "");
//            System.out.println(s);
//            JSONObject jsonObject = new JSONObject(s);
//            System.out.println(jsonObject);
        }catch (Exception e){
            e.printStackTrace();

        }
    }

    private void getUserDto(final ArrayList users){
        final ManagementTeamData managementTeamData =  ManagementTeamData.getInstance();
        final ArrayList<User>ThriftUsers = new ArrayList<>();
        final ArrayList<String>UsersID = new ArrayList<>();
        final int[] counter = {0};
        for (int i = 0; i <users.size() ; i++) {
            SiteSettingsUserRole user = (SiteSettingsUserRole) users.get(i);
            if (!UsersID.contains(user.userId)){
                UsersID.add(user.userId);
                System.out.println("THRIFT USER: "+user.description());
                aPimmDataManager dataManager = aPimmDataManager.getInstance();
                dataManager.getUserRecordForUserId(user.userId, new OnCompleteListeners.getUserRecordForUserIdListener() {
                    @Override
                    public void getUserRecordForUserId(User user, Error error) {
                        if (error == null) {
                            counter[0]++;
                            ThriftUsers.add(user);
                            if (counter[0] == UsersID.size()) {
                                ArrayList<CustomUser> customUsers = SCMTool.ConvertUserToCustomUser(ThriftUsers);
                                managementTeamData.setThriftusers(customUsers);
                            }
                        }
                    }
                });
            }

        }
    }
}
