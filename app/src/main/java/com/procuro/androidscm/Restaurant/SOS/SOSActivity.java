package com.procuro.androidscm.Restaurant.SOS;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Telephony;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_SafetyDataSheets.Dashboard_SafetyDataSheets;
import com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreProfile_Emergency_listviewAdapter;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.PimmInstance;
import com.procuro.apimmdatamanagerlib.PropertyValue;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.SiteSettings;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.util.ArrayList;
import java.util.Objects;

public class SOSActivity extends AppCompatActivity {

    Button edit,back,safety_data_sheets;
    TextView Name,Phone,Address;
    EditText fire_dept,police_dept,rescue_squad,hospital,franchise,district_manager,director_area_ops,loss_prevention,ems_safety,acs,securty_desk;
    boolean enableEdit = false;
    com.procuro.apimmdatamanagerlib.aPimmDataManager dataManager = aPimmDataManager.getInstance();
    LinearLayout root;
    private String temp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sos_activity);

        back = findViewById(R.id.home);
        edit = findViewById(R.id.edit);
        Name = findViewById(R.id.name);
        Phone = findViewById(R.id.phone);
        Address = findViewById(R.id.address);
        safety_data_sheets = findViewById(R.id.safety_datasheets);

        fire_dept = findViewById(R.id.fire_dept);
        police_dept = findViewById(R.id.police_dept);
        rescue_squad = findViewById(R.id.rescue_squad);
        hospital = findViewById(R.id.hospital);
        franchise = findViewById(R.id.franchise);
        district_manager = findViewById(R.id.district_manager);
        director_area_ops = findViewById(R.id.director_are_ops);
        loss_prevention = findViewById(R.id.loss_prevention);
        ems_safety = findViewById(R.id.ems_safety);
        acs = findViewById(R.id.acs);
        securty_desk = findViewById(R.id.securty_desk);
        root = findViewById(R.id.root);

        DisplaySiteInfo();

        setUpOnclicks();

        CheckSitePropertyValues();

        DisableEdit();
    }

    private void setUpOnclicks(){
        try {
            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SCMTool.hideKeyboard(SOSActivity.this);
                    finish();
                }
            });

            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!enableEdit){
                        EnableEdit();
                        enableEdit =true;
                    }else {
                        InitializeSave();
                        enableEdit = false;
                    }

                }
            });

            safety_data_sheets.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(SOSActivity.this, Dashboard_SafetyDataSheets.class);
                    Objects.requireNonNull(SOSActivity.this).startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(SOSActivity.this).toBundle());

                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void setupEditHandlers(){
        try {

            AddTextWatcher(fire_dept,"fire_dept");

            AddTextWatcher(police_dept,"police_dept");

            AddTextWatcher(rescue_squad,"rescue_squad");

            AddTextWatcher(hospital,"hospital");

            AddTextWatcher(franchise,"franchise");

            AddTextWatcher(district_manager,"district_manager");

            AddTextWatcher(director_area_ops,"director_area_ops");

            AddTextWatcher(loss_prevention,"loss_prevention");

            AddTextWatcher(ems_safety,"ems_safety");

            AddTextWatcher(acs,"acs");

            AddTextWatcher(securty_desk,"security_desk");

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void DisplaySiteInfo(){
        try {
            SOSData data = SOSData.getInstance();
            Site site = data.getSite();

            Name.setText(SCMTool.CheckString(site.sitename,"--"));
            Phone.append(SCMTool.CheckString(site.Contact.PhoneNumber,"--"));
            Address.append(SCMTool.getCompleteAddress(site));

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void InitializedSitePropertyValue(ArrayList<PropertyValue>siteSettings){

        final ArrayList<PropertyValue> franchise = new ArrayList<>();
        final ArrayList<PropertyValue> district_manager = new ArrayList<>();
        final ArrayList<PropertyValue> director_are = new ArrayList<>();
        final ArrayList<PropertyValue> loss_prevention = new ArrayList<>();
        final ArrayList<PropertyValue> ems = new ArrayList<>();
        final ArrayList<PropertyValue> acs = new ArrayList<>();
        final ArrayList<PropertyValue> scurity_desk = new ArrayList<>();
        final ArrayList<PropertyValue> fire_dept = new ArrayList<>();
        final ArrayList<PropertyValue> police_dept = new ArrayList<>();
        final ArrayList<PropertyValue> resque_squad = new ArrayList<>();
        final ArrayList<PropertyValue> hospital = new ArrayList<>();

        try {
            if (siteSettings!=null){
                for (PropertyValue propertyValue : siteSettings){
                    System.out.println("PROPERTY: "+propertyValue.property);
                    System.out.println("VALUE: "+propertyValue.value);

                    if (propertyValue!=null){
                        if (propertyValue.property.equalsIgnoreCase("FM:EC:Police")){
                            police_dept.add(propertyValue);
                            System.out.println("Police : "+propertyValue.value);
                        }
                        else if (propertyValue.property.equalsIgnoreCase("FM:EC:Fire")){
                            fire_dept.add(propertyValue);
                            System.out.println("Fire : "+propertyValue.value);
                        }
                        else if (propertyValue.property.equalsIgnoreCase("FM:EC:RescueSquad")){
                            resque_squad.add(propertyValue);
                            System.out.println("RescueSquad : "+propertyValue.value);
                        }
                        else if (propertyValue.property.equalsIgnoreCase("FM:EC:Hospital")){
                            hospital.add(propertyValue);
                            System.out.println("Hospital : "+propertyValue.value);
                        }

                        else if (propertyValue.property.equalsIgnoreCase("FM:EC:Franchise")){
                            System.out.println("Hospital : "+propertyValue.value);
                            franchise.add(propertyValue);
                        }
                        else if (propertyValue.property.equalsIgnoreCase("FM:EC:DistrictManager")){
                            System.out.println("DistrictManager : "+propertyValue.value);
                            district_manager.add(propertyValue);
                        }

                        else if (propertyValue.property.equalsIgnoreCase("FM:EC:DirectorAreaOps")){
                            System.out.println("DirectorAreaOps : "+propertyValue.value);
                            director_are.add(propertyValue);
                        }

                        else if (propertyValue.property.equalsIgnoreCase("FM:EC:LossPrevention")){
                            System.out.println("LossPrevention : "+propertyValue.value);
                            loss_prevention.add(propertyValue);
                        }

                        else if (propertyValue.property.equalsIgnoreCase("FM:EC:EMSSafetyMgmt")){
                            System.out.println("EMSSafetyMgmt : "+propertyValue.value);
                            ems.add(propertyValue);
                        }
                        else if (propertyValue.property.equalsIgnoreCase("FM:EC:AutomatedCommSystem(ACS)")){
                            System.out.println("AutomatedCommSystem : "+propertyValue.value);
                            acs.add(propertyValue);
                        }

                        else if (propertyValue.property.equalsIgnoreCase("FM:EC:SecurityDesk")){
                            System.out.println("SecurityDesk : "+propertyValue.value);
                            scurity_desk.add(propertyValue);
                        }

                    }
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }


        DisplayPropertyValueContacts(franchise,district_manager,director_are,
                loss_prevention,ems,acs,scurity_desk,fire_dept,police_dept,resque_squad,hospital);

    }

    private void DownloadSitePropertyValues(String siteID){
        final SOSData data = SOSData.getInstance();
        dataManager.getSiteSettingsForSiteId(siteID, new OnCompleteListeners.getSiteSettingsForSiteIdListener() {
            @Override
            public void getSiteSettingsForSiteId(SiteSettings siteSettings, Error error) {
                if (error == null){
                    InitializedSitePropertyValue(siteSettings.settings);
                    data.setPropertyValues(siteSettings.settings);
                }
            }
        });
    }

    private void CheckSitePropertyValues(){
        SOSData data = SOSData.getInstance();
        if (data.getPropertyValues()==null){
            DownloadSitePropertyValues(data.getSite().siteid);
        }else {
            InitializedSitePropertyValue(data.getPropertyValues());
        }
    }

    private void DisplayPropertyValueContacts(ArrayList<PropertyValue> franchise,
                                              ArrayList<PropertyValue> district_manager,
                                              ArrayList<PropertyValue> director_are,
                                              ArrayList<PropertyValue> loss_prevention,
                                              ArrayList<PropertyValue> ems,
                                              ArrayList<PropertyValue> acs,
                                              ArrayList<PropertyValue> scurity_desk,
                                              ArrayList<PropertyValue> fire_dept,
                                              ArrayList<PropertyValue> police_dept,
                                              ArrayList<PropertyValue> resque_squad,
                                              ArrayList<PropertyValue> hospital){
        try {
            SOSData data = SOSData.getInstance();

            this.fire_dept.setText(getContact(fire_dept,data.getFire_dept()));

            this.police_dept.setText(getContact(police_dept,data.getPolice_dept()));

            this.rescue_squad.setText(getContact(resque_squad,data.getRescue_squad()));

            this.hospital.setText(getContact(hospital,data.getHospital()));

            this.franchise.setText(getContact(franchise,data.getFranchise()));

            this.district_manager.setText(getContact(district_manager,data.getDistrict_manager()));

            this.director_area_ops.setText(getContact(director_are,data.getDirector_area_ops()));

            this.loss_prevention.setText(getContact(loss_prevention,data.getLoss_prevention()));

            this.ems_safety.setText(getContact(ems,data.getEms()));

            this.acs.setText(getContact(acs,data.getAcs()));

            this.securty_desk.setText(getContact(scurity_desk,data.getSecurity_desk()));

            setupEditHandlers();


        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private void EnableEdit(){
        edit.setText("Save");

        fire_dept.setEnabled(true);
        fire_dept.setAlpha(1);

        police_dept.setEnabled(true);
        police_dept.setAlpha(1);

        rescue_squad.setEnabled(true);
        rescue_squad.setAlpha(1);

        hospital.setEnabled(true);
        hospital.setAlpha(1);

        franchise.setEnabled(true);
        franchise.setAlpha(1);

        district_manager.setEnabled(true);
        district_manager.setAlpha(1);

        director_area_ops.setEnabled(true);
        director_area_ops.setAlpha(1);

        loss_prevention.setEnabled(true);
        loss_prevention.setAlpha(1);

        ems_safety.setEnabled(true);
        ems_safety.setAlpha(1);

        acs.setEnabled(true);
        acs.setAlpha(1);

        securty_desk.setEnabled(true);
        securty_desk.setAlpha(1);

    }

    private void DisableEdit(){

        fire_dept.setEnabled(false);
        fire_dept.setAlpha(.5f);

        police_dept.setEnabled(false);
        police_dept.setAlpha(.5f);

        rescue_squad.setEnabled(false);
        rescue_squad.setAlpha(.5f);

        hospital.setEnabled(false);
        hospital.setAlpha(.5f);

        franchise.setEnabled(false);
        franchise.setAlpha(.5f);

        district_manager.setEnabled(false);
        district_manager.setAlpha(.5f);

        director_area_ops.setEnabled(false);
        director_area_ops.setAlpha(.5f);

        loss_prevention.setEnabled(false);
        loss_prevention.setAlpha(.5f);

        ems_safety.setEnabled(false);
        ems_safety.setAlpha(.5f);

        acs.setEnabled(false);
        acs.setAlpha(.5f);

        securty_desk.setEnabled(false);
        securty_desk.setAlpha(.5f);

    }

    private String ValidString(String s){
        String data = "---";
        if (s !=null){
            if (!s.equalsIgnoreCase("") && !s.equalsIgnoreCase("null")){
                data = s;
            }
        }
        return  data;
    }

    private String AddTextWatcher(EditText editText , final String params){
        final SOSData data = SOSData.getInstance();
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (params.equalsIgnoreCase("fire_dept")){
                        data.setFire_dept(s.toString());
                    }
                    else if (params.equalsIgnoreCase("police_dept")){
                        data.setPolice_dept(s.toString());
                    }
                    else if (params.equalsIgnoreCase("rescue_squad")){
                        data.setRescue_squad(s.toString());
                    }
                    else if (params.equalsIgnoreCase("hospital")){
                        data.setHospital(s.toString());
                    }
                    else if (params.equalsIgnoreCase("franchise")){
                        data.setFranchise(s.toString());
                    }
                    else if (params.equalsIgnoreCase("district_manager")){
                        data.setDistrict_manager(s.toString());
                    }
                    else if (params.equalsIgnoreCase("director_area_ops")){
                        data.setDirector_area_ops(s.toString());
                    }
                    else if (params.equalsIgnoreCase("loss_prevention")){
                        data.setLoss_prevention(s.toString());
                    }
                    else if (params.equalsIgnoreCase("ems_safety")){
                        data.setEms(s.toString());
                    }
                    else if (params.equalsIgnoreCase("acs")){
                        data.setAcs(s.toString());
                    }
                    else if (params.equalsIgnoreCase("security_desk")){
                        data.setSecurity_desk(s.toString());
                    }
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return temp;
    }

    private String getContact(ArrayList<PropertyValue> propertyValues , String updated){
        String string = "";
        if (updated==null){
            if (propertyValues.size()>0){
                string = ValidString(propertyValues.get(propertyValues.size()-1).value);
            }
        }else {
            string = updated;
        }

        return string;
    }

    private void InitializeSave(){
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please Wait");
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Saving Updates");
        progressDialog.show();

        SOSData data = SOSData.getInstance();
        final Site site = SOSData.getInstance().getSite();
        ArrayList<PropertyValue>UpdateValues = new ArrayList<>();

        if (data.getFire_dept()!=null){
            PropertyValue propertyValue = new PropertyValue();
            propertyValue.property = "FM:EC:Fire";
            propertyValue.value = data.getFire_dept();
            UpdateValues.add(propertyValue);

            updatePropertValueInLocal(propertyValue);

            data.setFire_dept(null);
        }

        if (data.getPolice_dept()!=null){
            PropertyValue propertyValue = new PropertyValue();
            propertyValue.property = "FM:EC:Police";
            propertyValue.value = data.getPolice_dept();
            UpdateValues.add(propertyValue);
            updatePropertValueInLocal(propertyValue);
            data.setPolice_dept(null);
        }

        if (data.getRescue_squad()!=null){
            PropertyValue propertyValue = new PropertyValue();
            propertyValue.property = "FM:EC:RescueSquad";
            propertyValue.value = data.getRescue_squad();
            UpdateValues.add(propertyValue);
            updatePropertValueInLocal(propertyValue);
            data.setRescue_squad(null);
        }

        if (data.getHospital()!=null){
            PropertyValue propertyValue = new PropertyValue();
            propertyValue.property = "FM:EC:Hospital";
            propertyValue.value = data.getHospital();
            UpdateValues.add(propertyValue);
            updatePropertValueInLocal(propertyValue);
            data.setHospital(null);
        }

        if (data.getFranchise()!=null){
            PropertyValue propertyValue = new PropertyValue();
            propertyValue.property = "FM:EC:Franchise";
            propertyValue.value = data.getFranchise();
            UpdateValues.add(propertyValue);
            updatePropertValueInLocal(propertyValue);
            data.setFranchise(null);
        }

        if (data.getDistrict_manager()!=null){
            PropertyValue propertyValue = new PropertyValue();
            propertyValue.property = "FM:EC:DistrictManager";
            propertyValue.value = data.getDistrict_manager();
            UpdateValues.add(propertyValue);
            updatePropertValueInLocal(propertyValue);
            data.setDistrict_manager(null);
        }

        if (data.getDirector_area_ops()!=null){
            PropertyValue propertyValue = new PropertyValue();
            propertyValue.property = "FM:EC:DirectorAreaOps";
            propertyValue.value = data.getDirector_area_ops();
            UpdateValues.add(propertyValue);
            updatePropertValueInLocal(propertyValue);
            data.setDirector_area_ops(null);
        }

        if (data.getLoss_prevention()!=null){
            PropertyValue propertyValue = new PropertyValue();
            propertyValue.property = "FM:EC:LossPrevention";
            propertyValue.value = data.getLoss_prevention();
            UpdateValues.add(propertyValue);
            updatePropertValueInLocal(propertyValue);
            data.setLoss_prevention(null);
        }

        if (data.getEms()!=null){
            PropertyValue propertyValue = new PropertyValue();
            propertyValue.property = "FM:EC:EMSSafetyMgmt";
            propertyValue.value = data.getEms();
            UpdateValues.add(propertyValue);
            updatePropertValueInLocal(propertyValue);
            data.setEms(null);
        }

        if (data.getAcs()!=null){
            PropertyValue propertyValue = new PropertyValue();
            propertyValue.property = "FM:EC:AutomatedCommSystem(ACS)";
            propertyValue.value = data.getAcs();
            UpdateValues.add(propertyValue);
            updatePropertValueInLocal(propertyValue);
            data.setAcs(null);
        }

        if (data.getSecurity_desk()!=null){
            PropertyValue propertyValue = new PropertyValue();
            propertyValue.property = "FM:EC:SecurityDesk";
            propertyValue.value = data.getSecurity_desk();
            UpdateValues.add(propertyValue);
            updatePropertValueInLocal(propertyValue);
            data.setSecurity_desk(null);
        }

        int ctr = 0;
        if (UpdateValues.size()>0){
            for (PropertyValue updated : UpdateValues){
                ctr++;
                Save(site.siteid,updated.property,updated.value,ctr,UpdateValues.size(),progressDialog);
            }
        }else {
            edit.setText("Edit");
            DisableEdit();
            progressDialog.dismiss();
        }
    }

    private void Save(String siteid , String Property , String Value, final int ctr , final int total, final ProgressDialog progressDialog){

        dataManager.setSiteProperty(siteid, Property, Value, new OnCompleteListeners.updateUserRecordWithUserIdListener() {
            @Override
            public void updateUserRecordWithUserIdCallback(Boolean status, Error error) {
                if (error == null){
                    if (ctr ==total){
                        SCMTool.DisplaySnackbar("Updates Saved",root);
                        edit.setText("Edit");
                        DisableEdit();
                        progressDialog.dismiss();
                    }
                }else {
                    Log.e("SOS ACTIVITY","setSiteProperty : "+error);
                }
            }
        });
    }

    private void updatePropertValueInLocal(PropertyValue propertyValue){
        try {
            SOSData data = SOSData.getInstance();
            if (data.getPropertyValues()!=null){
                for (int i = 0; i <data.getPropertyValues().size() ; i++) {
                    PropertyValue pv = data.getPropertyValues().get(i);
                    if (pv.property.equalsIgnoreCase(propertyValue.property)){
                        pv.value = propertyValue.value;
                        break;
                    }
                }

            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
