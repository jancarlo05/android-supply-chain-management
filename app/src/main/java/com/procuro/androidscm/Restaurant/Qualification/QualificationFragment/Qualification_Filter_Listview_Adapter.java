package com.procuro.androidscm.Restaurant.Qualification.QualificationFragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.apimmdatamanagerlib.CorpStructure;

import java.util.ArrayList;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

import static com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationFragment.DisplaySelectedPerspective;
import static com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationFragment.search;


public class Qualification_Filter_Listview_Adapter extends BaseAdapter {

    // Declare Variables

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<Qualification_Filter> list;
    private ExpandableListView expandableListView;
    private FragmentActivity fragmentActivity;
    private SimpleTooltip tooltip;
    private ArrayList<CustomSite>customSites;
    private ArrayList<CorpStructure>corpStructures;

    public Qualification_Filter_Listview_Adapter(Context context, ArrayList<Qualification_Filter> list, ExpandableListView expandableListView,
                                                 FragmentActivity fragmentActivity,SimpleTooltip tooltip) {
        this.mContext = context;
        this.inflater = LayoutInflater.from(mContext);
        this.list = list;
        this.expandableListView = expandableListView;
        this.fragmentActivity = fragmentActivity;
        this.tooltip = tooltip;

    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    public View getView(final int position, View view, ViewGroup parent) {
            view = inflater.inflate(R.layout.qualification_filter_listview, null);
            final Qualification_Filter filter = list.get(position);

            TextView name = view.findViewById(R.id.name);
            ImageView icon = view.findViewById(R.id.selected);
            name.setText(filter.getName());


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (Qualification_Filter filters : list){
                        filters.setSelected(false);
                    }
                    filter.setSelected(true);

                    DisplaySelectedPerspective(mContext,fragmentActivity);
                    search.setText("");
                    tooltip.dismiss();
                }
            });

        if (filter.isSelected()){
            icon.setVisibility(View.VISIBLE);
        }else {
            icon.setVisibility(View.INVISIBLE);
        }



        return view;
    }


}

