package com.procuro.androidscm.Restaurant.DashBoard;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextClock;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.procuro.androidscm.Daypart;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.DashBoard.Dasboard_Main_Process.Dashboard_Onclicks;
import com.procuro.androidscm.Restaurant.DashBoard.Dasboard_Main_Process.Dashboard_Process;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Traning.Dashboard_TraningData;
import com.procuro.androidscm.Restaurant.DashBoard.DopHuddle.Dashboard_dop_huddle_skills_and_position_listviewAdapter;
import com.procuro.androidscm.Restaurant.DashBoard.KitchenMetric.DashboardKitchenMetricActivity;
import com.procuro.androidscm.Restaurant.Schema.Dayparts;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.DashboardKitchenData;
import com.procuro.apimmdatamanagerlib.DocumentDTO;
import com.procuro.apimmdatamanagerlib.HuddleTeam;
import com.procuro.apimmdatamanagerlib.KitchenDataItems;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.PimmForm;
import com.procuro.apimmdatamanagerlib.PimmInstance;
import com.procuro.apimmdatamanagerlib.PositionDataItems;
import com.procuro.apimmdatamanagerlib.SMSCleaningDaily;
import com.procuro.apimmdatamanagerlib.SMSCleaningMonthly;
import com.procuro.apimmdatamanagerlib.SMSCleaningWeekly;
import com.procuro.apimmdatamanagerlib.SMSCommunicationNotes;
import com.procuro.apimmdatamanagerlib.SMSDailyOpsPlan;
import com.procuro.apimmdatamanagerlib.SMSDashboard;
import com.procuro.apimmdatamanagerlib.SMSPositioning;
import com.procuro.apimmdatamanagerlib.SMSPositioningEmployeePositionData;
import com.procuro.apimmdatamanagerlib.Schedule_Business_Site_Plan;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.SiteSales;
import com.procuro.apimmdatamanagerlib.SiteSalesActual;
import com.procuro.apimmdatamanagerlib.SiteSalesDefault;
import com.procuro.apimmdatamanagerlib.TrainingData;
import com.procuro.apimmdatamanagerlib.TrainingDataItems;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import static com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.DailyLabor.Dashboard_DailyLabor_fragment.getDailyLabor;
import static com.procuro.androidscm.Restaurant.DashBoard.Dashboard_SalePerformance.Dashboard_SalesPerformance.ConvertDefaultToSales;
import static com.procuro.androidscm.SCMTool.getStringCurrentDaypart;

public class DashboardActivity extends AppCompatActivity {


    com.procuro.apimmdatamanagerlib.aPimmDataManager dataManager = aPimmDataManager.getInstance();

    //Header
    public static LinearLayout staff_peak_actual_container, staff_peak_required_container,
            sales_forecast_container,sales_actual_container,cee_container,fee_container,
            osat_container,zod_container,sitename_container;

    public static TextView staff_peak_actual ,stff_peak_required,
            sales_forecast,sales_actual,number_of_cars,
            cee,fee,osat, zod,selectedstore,taste,friendliness,speed,
            accurcacy,cleanliness,positions_covered_value,ops_leader,support_managers,message,total_positions;

    public static  LinearLayout staffing_dropdown,sales_performace,storeRecord,customersatisfaction_Dropdown,
            CustomerExperience_Dropdown;


    //Tutorial
   public static LinearLayout taste_container,friendliness_container,speed_container,accuracy_container,
            cleanliness_container;

    //Sliders
    public static Button cleaningGuide,communcationNotes,SafetyDataSheets,Training,VOC,VTa,Pimmsm,Scorecard,
            quiack_a_menu,sos,back;

    public static ListView listView;
    Dashboard_dop_huddle_skills_and_position_listviewAdapter listviewAdapter;
    Site site;
    public static ImageView kitchen_layout,pimmynews;
    public static RelativeLayout kitchenLayoutContainer;
    boolean isFGPopulated = false;
    ProgressDialog dialog;

    private TextClock textClock;
    private TextView daypart,currentDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);

        quiack_a_menu = findViewById(R.id.quick_a_menu);
        sos  = findViewById(R.id.sos);
        number_of_cars = findViewById(R.id.numberofcars);
        cleaningGuide = findViewById(R.id.cleaning_slider);
        communcationNotes = findViewById(R.id.communication_notes_slider);
        SafetyDataSheets = findViewById(R.id.safety_datasheets);
        Training = findViewById(R.id.training);
        VOC = findViewById(R.id.voc);
        kitchen_layout = findViewById(R.id.kitchen_layout);
        kitchenLayoutContainer = findViewById(R.id.kitchen_layout_container);

        staff_peak_actual_container = findViewById(R.id.staffing_actual_container);
        staff_peak_required_container = findViewById(R.id.staffing_required_container);
        sales_forecast_container = findViewById(R.id.sales_forecast_container);
        sales_actual_container = findViewById(R.id.sales_actual_container);
        cee_container = findViewById(R.id.cee_container);
        fee_container = findViewById(R.id.fee_container);
        osat_container = findViewById(R.id.osat_container);
        zod_container = findViewById(R.id.zod_container);

        staff_peak_actual = findViewById(R.id.staffing_actual);
        stff_peak_required =findViewById(R.id.staffing_required);
        sales_forecast =findViewById(R.id.sales_forecast);
        sales_actual =findViewById(R.id.sales_actual);
        cee = findViewById(R.id.cee);
        fee = findViewById(R.id.fee);
        osat = findViewById(R.id.osat);
        zod = findViewById(R.id.zod);


        listView = findViewById(R.id.listView);

        taste_container = findViewById(R.id.taste_container);
        friendliness_container = findViewById(R.id.friendliness_container);
        speed_container = findViewById(R.id.speed_container);
        accuracy_container = findViewById(R.id.accuracy_container);
        cleanliness_container = findViewById(R.id.cleanliness_container);
        
        taste = findViewById(R.id.taste_value);
        friendliness = findViewById(R.id.friendliness_value);
        speed = findViewById(R.id.speed_value);
        accurcacy = findViewById(R.id.accuracy_value);
        cleanliness = findViewById(R.id.cleanliness_value);

        back = findViewById(R.id.home);
        selectedstore = findViewById(R.id.username);
        sitename_container = findViewById(R.id.title_holder);
        staffing_dropdown = findViewById(R.id.staffing_Dropdown);
        storeRecord = findViewById(R.id.store_record_Dropdown);
        customersatisfaction_Dropdown = findViewById(R.id.customer_sat_Dropdown);
        CustomerExperience_Dropdown = findViewById(R.id.customer_expi_drop_down);
        sales_performace = findViewById(R.id.sales_performance_Dropdown);
        positions_covered_value = findViewById(R.id.positions_covered_value);

        ops_leader = findViewById(R.id.ops_leader);
        support_managers = findViewById(R.id.support_managers);
        VTa = findViewById(R.id.vtas);
        Pimmsm = findViewById(R.id.pimm_sms);
        pimmynews = findViewById(R.id.pimmynews);
        message = findViewById(R.id.message);

        total_positions = findViewById(R.id.total_positions);
        textClock = findViewById(R.id.textclock);
        daypart = findViewById(R.id.daypart);
        currentDate = findViewById(R.id.currentDate);


        dialog = new ProgressDialog(this);
        dialog.setTitle("Please Wait");
        dialog.setMessage("Downloading Data");
        dialog.setCancelable(false);
        dialog.show();

        Dashboard_Process.DisplayTextClock(currentDate, daypart);

        downloadSiteResources();

        Dashboard_Onclicks.InitializeOnclicks(this,this);

        selectedstore.setText(SCMDataManager.getInstance().getSelectedSite().getSite().SiteName);

        SCMTool.animateJump(pimmynews);
    }

    private void setupTextClock() {

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    private void downloadSiteResources(){
        try {
            site = SCMDataManager.getInstance().getSelectedSite().getSite();

            setupTextClock();

            Download_Scores();

            DownloadEmployeeSchedule();

//            SCMDataManager.DownloadHoursOfOperationData(site.siteid);

            setUpDailySalesPerformance();

            SensorCategoryView();

            Download_getSMSCommunicationNotesForSite();

            Download_getFormDefinitionListForAppId();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void DownloadEmployeeSchedule(){
        dataManager = aPimmDataManager.getInstance();
        Calendar currentDate = SCMTool.getDateWithDaypart(new Date().getTime(),site.effectiveUTCOffset);
        dataManager.getSchedulesByDate(site.siteid, currentDate.getTime(), currentDate.getTime(), new OnCompleteListeners.getEmployeeScheduleListener() {
            @Override
            public void getEmployeeScheduleCallback(ArrayList<Schedule_Business_Site_Plan> schedules, Error error) {
                if (error == null){
                    SCMDataManager.getInstance().setDailySchedule(schedules);
                }
            }
        });
    }

    private void Download_DopForm(final String id, final String formid){
        if (SCMDataManager.getInstance().getDailyOpsPlanForm() == null){
            dataManager.getLatestFormForReferenceId(id, formid, new OnCompleteListeners.getLatestFormForReferenceIdCallbackListener() {
                @Override
                public void getLatestFormForReferenceId(PimmForm pimmForm, Error error) {
                    if (error == null){
                        try {
                            JSONObject jsonObject = new JSONObject(pimmForm.StringBody);
                            SMSDailyOpsPlan opsPlan = new SMSDailyOpsPlan();
                            opsPlan.readFromJSONObject(jsonObject);
                            SCMDataManager.getInstance().setDailyOpsPlanForm(opsPlan);
                            Dashboard_Process.DisplayDopHuddle(opsPlan.customerSatisfaction.customerSatisfactionItems, DashboardActivity.this, DashboardActivity.this);
                            DisplayManagersOnduty(opsPlan);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Download_DopForm(id,formid);
                        }
                    }
                }
            });
        }else {
            Dashboard_Process.DisplayDopHuddle(SCMDataManager.getInstance().getDailyOpsPlanForm().customerSatisfaction.customerSatisfactionItems, DashboardActivity.this, DashboardActivity.this);
            DisplayManagersOnduty(SCMDataManager.getInstance().getDailyOpsPlanForm());
            dataManager.getLatestFormForReferenceId(id, formid, new OnCompleteListeners.getLatestFormForReferenceIdCallbackListener() {
                @Override
                public void getLatestFormForReferenceId(PimmForm pimmForm, Error error) {
                    if (error == null){
                        try {
                            JSONObject jsonObject = new JSONObject(pimmForm.StringBody);
                            SMSDailyOpsPlan opsPlan = new SMSDailyOpsPlan();
                            opsPlan.readFromJSONObject(jsonObject);
                            SCMDataManager.getInstance().setDailyOpsPlanForm(opsPlan);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }

    }


    private void Download_getSMSCommunicationNotesForSite(){

        if (SCMDataManager.getInstance().getSmsCommunicationNotes() == null){
            dataManager.getSMSCommunicationNotesForSite(site.siteid, new OnCompleteListeners.getSMSCommunicationNotesForSiteCallbackListener() {
                @Override
                public void getSMSCommunicationNotesForSiteWithCallback(ArrayList<SMSCommunicationNotes> smsCommunicationNotes, Error error) {
                    if (error == null){
                        SCMDataManager.getInstance().setSmsCommunicationNotes(smsCommunicationNotes);
                    }
                }
            });
        }
    }

    private void Download_getFormDefinitionListForAppId(){
        ArrayList<PimmForm>PimmFormList = SCMDataManager.getInstance().getPimmFormArrayList();
        if (PimmFormList == null){
            dataManager.getFormDefinitionListForAppId(new OnCompleteListeners.getFormDefinitionListForAppIdCallbackListener() {
                @Override
                public void getFormDefinitionListForAppIdCallback(ArrayList<PimmForm> pimmFormArrayList, Error error) {
                    if (error == null){
                        SCMDataManager.getInstance().setPimmFormArrayList(pimmFormArrayList);
                        int counter = 0;
                        for (final PimmForm form: pimmFormArrayList){
                            counter++;
                            System.out.println("FORM : "+form.name);
                            if (form.name.equalsIgnoreCase("SMS:Positioning")) {
                                Download_SMS_POSITIONING(site.siteid, form.formDefinitionID);
                            }
                            if (form.name.equalsIgnoreCase("SMS:Cleaning:Daily")){
                                Download_SMS_CLEANING_DAILY(site.siteid,form.formDefinitionID);
                            }
                            if (form.name.equalsIgnoreCase("SMS:Cleaning:Weekly")){
                                System.out.println("SMS:Cleaning:Weekly START");
                                Download_SMS_CLEANING_WEEKLY(site.siteid,form.formDefinitionID);
                            }
                            if (form.name.equalsIgnoreCase("SMS:Cleaning:Monthly")){
                                System.out.println("SMS:Cleaning:Monthly START");
                                Download_SMS_CLEANING_MONTHLY(site.siteid,form.formDefinitionID);
                            }
                            if (form.name.equalsIgnoreCase("SMS:DailyOpsPlan")){
                                System.out.println("SMS:DailyOpsPlan START");
                                Download_DopForm(site.siteid, form.formDefinitionID);
                            }
                        }
                    }
                }
            });
        }
        else {
            int counter = 0;
            for (final PimmForm form: PimmFormList){
                if (form.name.equalsIgnoreCase("SMS:Positioning")) {
                    Download_SMS_POSITIONING(site.siteid, form.formDefinitionID);
                }
                if (form.name.equalsIgnoreCase("SMS:Cleaning:Daily")){
                    Download_SMS_CLEANING_DAILY(site.siteid,form.formDefinitionID);
                }
                if (form.name.equalsIgnoreCase("SMS:Cleaning:Weekly")){
                    System.out.println("SMS:Cleaning:Weekly START");
                    Download_SMS_CLEANING_WEEKLY(site.siteid,form.formDefinitionID);
                }
                if (form.name.equalsIgnoreCase("SMS:Cleaning:Monthly")){
                    System.out.println("SMS:Cleaning:Monthly START");
                    Download_SMS_CLEANING_MONTHLY(site.siteid,form.formDefinitionID);
                }
                if (form.name.equalsIgnoreCase("SMS:DailyOpsPlan")){
                    System.out.println("SMS:DailyOpsPlan START");
                    Download_DopForm(site.siteid, form.formDefinitionID);
                }
            }
        }
    }

    private void Download_DocumentListForReferenceId(){
        final ArrayList<String>strings = new ArrayList<>();
        ArrayList<DocumentDTO>dtoArrayList = SCMDataManager.getInstance().getDocumentDTOArrayList();
        if (dtoArrayList==null){
            dataManager.getDocumentListForReferenceId(SCMDataManager.getInstance().getSelectedSite().getSite().siteid, new OnCompleteListeners.getDocumentListForReferenceIdCallbackListener() {
                @Override
                public void getDocumentListForReferenceIdCallback(ArrayList<DocumentDTO> documentDTOArrayList, Error error) {
                    if (error == null){
                        for (DocumentDTO documentDTO : documentDTOArrayList){
//                        if (documentDTO.category.equalsIgnoreCase("Employee_Position_Data")){
//                            System.out.println("Download_EMPLOYEE_POSITION_DATA : START");
//                            Download_EMPLOYEE_POSITION_DATA(documentDTO.documentId,documentDTO.category);
//                        }
                            if (documentDTO.category.equalsIgnoreCase("Employee_Position_Optimization_Data")){
                                strings.add(documentDTO.category);
                                System.out.println("Employee_Position_Optimization_Data : START");
                                Download_EMPLOYEE_POSITION_DATA(documentDTO.documentId,documentDTO.category);
                            }
                            else if (documentDTO.category.equalsIgnoreCase("Training_Data")){
                                Download_TRAINING_DATA(documentDTO.documentId,documentDTO.category);
                            }

                        }

                        if (!strings.contains("Employee_Position_Optimization_Data")){
                            dialog.dismiss();
                        }
                    }
                }
            });
        }else {
            for (DocumentDTO documentDTO : dtoArrayList){
                System.out.println("CATEGORY : "+documentDTO.category);

//                        if (documentDTO.category.equalsIgnoreCase("Employee_Position_Data")){
//                            System.out.println("Download_EMPLOYEE_POSITION_DATA : START");
//                            Download_EMPLOYEE_POSITION_DATA(documentDTO.documentId,documentDTO.category);
//
//                        }
                if (documentDTO.category.equalsIgnoreCase("Employee_Position_Optimization_Data")){
                    strings.add(documentDTO.category);
                    System.out.println("Employee_Position_Optimization_Data : START");
                    Download_EMPLOYEE_POSITION_DATA(documentDTO.documentId,documentDTO.category);

                }else if (documentDTO.category.equalsIgnoreCase("Training_Data")){
                    Download_TRAINING_DATA(documentDTO.documentId,documentDTO.category);
                }

            }
            if (!strings.contains("Employee_Position_Optimization_Data")){
                dialog.dismiss();
            }
        }
    }

    private void PopulateTraningData(TrainingData trainingData){
        ArrayList<Dashboard_TraningData>traning = new ArrayList<>();
        for (String category : trainingData.categories){
            traning.add(new Dashboard_TraningData(category));
        }
        for (Dashboard_TraningData dashtraining : traning){
            for (TrainingDataItems dataItems : trainingData.trainingDataItems){
                if (dashtraining.getCategory().equalsIgnoreCase(dataItems.category)){
                    if (dashtraining.getTrainingDataItems()!=null){
                        dashtraining.getTrainingDataItems().add(dataItems);
                    }else {
                        ArrayList<TrainingDataItems>items = new ArrayList<>();
                        items.add(dataItems);
                        dashtraining.setTrainingDataItems(items);
                    }
                }else {
                    if (dashtraining.getTrainingDataItems()==null){
                        ArrayList<TrainingDataItems>items = new ArrayList<>();
                        dashtraining.setTrainingDataItems(items);
                    }
                }
            }
        }
        SCMDataManager.getInstance().setDashboardTraningData(traning);
    }

    private void getStoreUsers(){

        dataManager.getStoreUsers(site.siteid, new OnCompleteListeners.getStoreUsersCallbackListener() {
            @Override
            public void getStoreUsersCallback(ArrayList<User> storeUsers, Error error) {
                if (error == null){
                    try {
                        SCMDataManager.getInstance().setStoreUsers(SCMTool.ConvertUserToCustomUser(storeUsers));
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
            }
        });
    }

    private void Download_SMS_CLEANING_MONTHLY(String siteID,String formID){

        dataManager.getLatestFormForReferenceId(siteID, formID, new OnCompleteListeners.getLatestFormForReferenceIdCallbackListener() {
            @Override
            public void getLatestFormForReferenceId(PimmForm pimmForm, Error error) {
                if (error == null){
                    try {
                        JSONObject jsonObject = new JSONObject(pimmForm.StringBody);
                        SMSCleaningMonthly monthly = new SMSCleaningMonthly();
                        monthly.readFromJSONObject(jsonObject);
                        SCMDataManager.getInstance().setCleaningMonthlyForm(monthly);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void Download_SMS_CLEANING_WEEKLY(String siteID,String formID){
        dataManager.getLatestFormForReferenceId(siteID, formID, new OnCompleteListeners.getLatestFormForReferenceIdCallbackListener() {
            @Override
            public void getLatestFormForReferenceId(PimmForm pimmForm, Error error) {
                if (error == null){
                    try {
                        if (pimmForm!=null){
                            JSONObject jsonObject = new JSONObject(pimmForm.StringBody);
                            SMSCleaningWeekly cleaningForm = new SMSCleaningWeekly();
                            cleaningForm.readFromJSONObject(jsonObject);
                            SCMDataManager.getInstance().setCleaningWeeklyForm(cleaningForm);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    private void Download_SMS_CLEANING_DAILY(String siteID,String formID){
        dataManager.getLatestFormForReferenceId(siteID, formID, new OnCompleteListeners.getLatestFormForReferenceIdCallbackListener() {
            @Override
            public void getLatestFormForReferenceId(PimmForm pimmForm, Error error) {
                if (error == null){
                    try {
                        if (pimmForm!=null){
                            JSONObject jsonObject = new JSONObject(pimmForm.StringBody);
                            SMSCleaningDaily cleaningForm = new SMSCleaningDaily();
                            cleaningForm.readFromJSONObject(jsonObject);
                            SCMDataManager.getInstance().setCleaningDailyForm(cleaningForm);
                            System.out.println("SMS:Cleaning:Daily: "+jsonObject.toString());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void Download_SMS_POSITIONING(String siteID,String formID){
        System.out.println("SMS:Positioning: START ");
        if (SCMDataManager.getInstance().getPositionForm() == null){
            System.out.println("SMS:Positioning: NULL ");
            dataManager.getLatestFormForReferenceId(siteID, formID, new OnCompleteListeners.getLatestFormForReferenceIdCallbackListener() {
                @Override
                public void getLatestFormForReferenceId(PimmForm pimmForm, Error error) {
                    if (error == null){
                        try {
                            if (pimmForm!=null){
                                if (pimmForm.StringBody!=null){
                                    JSONObject jsonObject = new JSONObject(pimmForm.StringBody);
                                    SMSPositioning smsPositioning = new SMSPositioning();
                                    smsPositioning.readFromJSONObject(jsonObject);
                                    SCMDataManager.getInstance().setPositionForm(smsPositioning);

                                    Dashboard_Process.DisplayScores(smsPositioning,DashboardActivity.this);
                                    Download_DocumentListForReferenceId();
//                                    Download_DocumentListForReferenceId();

                                }else {
                                    Dashboard_Process.DisplayScores(null,DashboardActivity.this);
                                    Download_DocumentListForReferenceId();
                                    message.setText("No Kitchen Data");
                                    dialog.dismiss();
                                }
                            }else {
                                Dashboard_Process.DisplayScores(null,DashboardActivity.this);
                                Download_DocumentListForReferenceId();
                                message.setText("No Kitchen Data");
                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Dashboard_Process.DisplayScores(null,DashboardActivity.this);
                            Download_DocumentListForReferenceId();
                        }
                    }
                    else {
                        Dashboard_Process.DisplayScores(null,DashboardActivity.this);
                        Download_DocumentListForReferenceId();
                        message.setText("No Kitchen Data");
                        dialog.dismiss();
                    }
                }
            });
        }else {
            System.out.println("SMS:Positioning: NOT NULL ");
            //Display Old Data
            Dashboard_Process.DisplayScores(SCMDataManager.getInstance().getPositionForm(),
                    DashboardActivity.this);
            Download_DocumentListForReferenceId();

            // Get New Data
            dataManager.getLatestFormForReferenceId(siteID, formID, new OnCompleteListeners.getLatestFormForReferenceIdCallbackListener() {
                @Override
                public void getLatestFormForReferenceId(PimmForm pimmForm, Error error) {
                    if (error == null) {
                        if (pimmForm!=null){
                            if (pimmForm.StringBody!=null){
                                try {
                                    JSONObject jsonObject = new JSONObject(pimmForm.StringBody);
                                    SMSPositioning smsPositioning = new SMSPositioning();
                                    smsPositioning.readFromJSONObject(jsonObject);
                                    SCMDataManager.getInstance().setPositionForm(smsPositioning);
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }
            });
        }
    }

    private void Download_EMPLOYEE_POSITION_DATA(String documentDtoID, String category){

        if (SCMDataManager.getInstance().getDashboardKitchenData() == null){
            dataManager.getDocumentContentForDocumentId(documentDtoID, category, new OnCompleteListeners.getDocumentContentForDocumentIdCallbackListener() {
                @Override
                public void getDocumentContentForDocumentIdCallback(Object obj, Error error) {
                    if (error ==  null){
                        if (obj !=null){
                            JSONObject jsonObject = (JSONObject) obj;
                            DashboardKitchenData dashboardKitchenData = new DashboardKitchenData();
                            System.out.println("dashboardKitchenData DOWNLOADED");
                            System.out.println(jsonObject);
                            dashboardKitchenData.readFromJSONObject(jsonObject);
                            SCMDataManager.getInstance().setDashboardKitchenData(dashboardKitchenData);
                            Dashboard_Process.setUPKitchenLayout(dashboardKitchenData,DashboardActivity.this,DashboardActivity.this);
                            message.setVisibility(View.GONE);
                            dialog.cancel();
                        }else {
                            dialog.cancel();
                        }
                    }else {
                        dialog.cancel();
                    }
                }
            });
        }else {
            Dashboard_Process.setUPKitchenLayout(SCMDataManager.getInstance().getDashboardKitchenData() ,DashboardActivity.this,DashboardActivity.this);
            message.setVisibility(View.GONE);
            dialog.cancel();
            dataManager.getDocumentContentForDocumentId(documentDtoID, category, new OnCompleteListeners.getDocumentContentForDocumentIdCallbackListener() {
                @Override
                public void getDocumentContentForDocumentIdCallback(Object obj, Error error) {
                    if (error ==  null){
                        if (obj !=null){
                            JSONObject jsonObject = (JSONObject) obj;
                            DashboardKitchenData dashboardKitchenData = new DashboardKitchenData();
                            System.out.println("dashboardKitchenData DOWNLOADED");
                            System.out.println(jsonObject);
                            dashboardKitchenData.readFromJSONObject(jsonObject);
                            SCMDataManager.getInstance().setDashboardKitchenData(dashboardKitchenData);
                        }
                    }
                }
            });
        }


    }

    private void Download_TRAINING_DATA(String documentDtoID,String category){

        if (SCMDataManager.getInstance().getDashboardTraningData() == null){
            dataManager.getDocumentContentForDocumentId(documentDtoID, category, new OnCompleteListeners.getDocumentContentForDocumentIdCallbackListener() {
                @Override
                public void getDocumentContentForDocumentIdCallback(Object obj, Error error) {
                    if (error ==  null){
                        if (obj !=null){
                            try {
                                System.out.println("Training_Data: "+obj.toString());
                                JSONObject jsonObject = (JSONObject) obj;
                                TrainingData trainingData = new TrainingData();
                                trainingData.readFromJSONObject(jsonObject);
                                PopulateTraningData(trainingData);
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });
        }
    }

    private void Download_Scores(){
        dataManager.getSMSDashboardScoresForSite(site.siteid, new OnCompleteListeners.getSMSDashboardWithCallbackListener() {
            @Override
            public void getSMSDashboardWithCallback(SMSDashboard smsDashboard, Error error) {
                if (error == null){
                    SCMDataManager.getInstance().setSmsDashboard(smsDashboard);
                }
            }
        });

    }

    private void DisplayManagersOnduty(SMSDailyOpsPlan dopFOrm){
        try {
            int shift = 1;
            Dayparts daypart = SCMTool.getActualCurrentDaypartStart();
            if (!"1,2,3".contains(daypart.name)){
                shift = 2;
            }
            StringBuilder opsLeader = new StringBuilder();
            StringBuilder suppManagers = new StringBuilder();
            if (dopFOrm!=null){
                if (dopFOrm.forecastingAndStaffing!=null){
                    if (dopFOrm.forecastingAndStaffing.huddleTeams!=null){
                        for (HuddleTeam huddleTeam : dopFOrm.forecastingAndStaffing.huddleTeams){
                            if (huddleTeam!=null){
                                if (shift == huddleTeam.shift){
                                    boolean support1 = false;
                                    if (huddleTeam.opsLeader!=null && !huddleTeam.opsLeader.equalsIgnoreCase("null")){
                                        opsLeader.append(huddleTeam.opsLeader);
                                    }
                                    if (huddleTeam.supportManager1!=null && !huddleTeam.supportManager1.equalsIgnoreCase("null")){
                                        support1 = true;
                                        suppManagers.append(huddleTeam.supportManager1);
                                    }
                                    if (huddleTeam.supportManager2!=null && !huddleTeam.supportManager2.equalsIgnoreCase("null")){
                                        if (support1){
                                            suppManagers.append(", ");
                                        }
                                        suppManagers.append(huddleTeam.supportManager2);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
                try {
                    ops_leader.setText(opsLeader.toString());
                    support_managers.setText(suppManagers.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void wrtieFileOnInternalStorage(String title,String sBody){


        System.out.println("wrtieFileOnInternalStorage: START");
        String root = Environment.getExternalStorageDirectory().toString();
        File file = new File(root);
        if(!file.exists()){
            file.mkdir();
        }else {
            file.delete();
        }
        try{
            File gpxfile = new File(file, title);
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();

            BufferedReader br = new BufferedReader(new FileReader(gpxfile));
            StringBuilder stringBuilder= new StringBuilder();
            String st;
            while ((st = br.readLine()) != null){
                stringBuilder.append(st);
            }

//            String s = stringBuilder.toString().replaceAll("[^\\x20-\\x7E]", "");
//            System.out.println(s);
//            JSONObject jsonObject = new JSONObject(s);
//            System.out.println(jsonObject);



        }catch (Exception e){
            e.printStackTrace();

        }
    }

    private void SensorCategoryView(){
        final CustomSite site = SCMDataManager.getInstance().getSelectedSite();
        dataManager = aPimmDataManager.getInstance();
        dataManager.getInstanceListForSiteId(site.getSite().DeviceID, new OnCompleteListeners.getInstanceListForSiteIdCallbackListener() {
            @Override
            public void getInstanceListForSiteIdCallback(ArrayList<PimmInstance> pimmInstanceArrayList, Error error) {
                if (error == null) {
                    if (pimmInstanceArrayList.size() > 0) {
                        site.setPimmInstances(pimmInstanceArrayList);
                    }
                }
            }
        });
    }
    private void setUpDailySalesPerformance(){
        try {
            Calendar calendar = Calendar.getInstance();
            calendar = SCMTool.getDateWithDaypart(calendar.getTime().getTime(),site.effectiveUTCOffset);
            if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
                calendar.add(Calendar.WEEK_OF_YEAR,-1);
            }
            int WeekOFYear = calendar.get(Calendar.WEEK_OF_YEAR);

            Site site = SCMDataManager.getInstance().getSelectedSite().getSite();

            Calendar now = Calendar.getInstance();
            now.setTimeZone(TimeZone.getTimeZone("UTC"));
            now.set(Calendar.WEEK_OF_YEAR, WeekOFYear);
            now.add(Calendar.MINUTE,site.effectiveUTCOffset);

            final ArrayList<Date> Weekdays = new ArrayList<>();
            int delta = -now.get(Calendar.DAY_OF_WEEK) + 2; //add 2 if your week start on monday
            now.add(Calendar.DAY_OF_MONTH, delta );

            for (int i = 0; i < 7; i++) {
                Weekdays.add(now.getTime());
                now.add(Calendar.DAY_OF_MONTH, 1);
            }

            DownloadSalesData(getPrevWeek(calendar),Weekdays, calendar);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void DownloadSalesData(ArrayList<Date> prevdates, final ArrayList<Date>dates, final Calendar currendDate){

        final SCMDataManager scm = SCMDataManager.getInstance();
        final SimpleDateFormat format = new SimpleDateFormat("EEE MM-dd-yyyy");
        final ArrayList<SiteSales>siteSalesList = new ArrayList<>();
        final ArrayList<SiteSales>prevsiteSalesList = new ArrayList<>();
        final int[] counter = {0};

        if (SCMDataManager.getInstance().getPrevSiteSales() == null && SCMDataManager.getInstance().getSiteSales() == null ){

            for (final Date date : prevdates){
                System.out.println("PREV DATES : "+format.format(date));
                dataManager.getSiteSalesByDate(site.siteid, date, new OnCompleteListeners.getSiteSalesByDateListener() {
                    @Override
                    public void getSiteSalesByDateCallback(SiteSales siteSales, Error error) {
                        if (error == null){
                            prevsiteSalesList.add(siteSales);
                        }else {
                            SiteSales siteSale = new SiteSales();
                            siteSale.salesDate = date;
                            prevsiteSalesList.add(siteSale);
                        }
                    }
                });
            }
            for (final Date date : dates){
                dataManager.getSiteSalesByDate(site.siteid, date, new OnCompleteListeners.getSiteSalesByDateListener() {
                    @Override
                    public void getSiteSalesByDateCallback(SiteSales siteSales, Error error) {
                        counter[0]++;
                        if (error == null){
                            if (siteSales!=null && siteSales.data!=null){
                                if (siteSales.data.dss!=0){
                                    siteSalesList.add(siteSales);
                                    if (counter[0] == dates.size()){
                                        DisplaySalesPerforanceScore(siteSalesList);
                                        SCMDataManager.getInstance().setSiteSales(siteSalesList);
                                        SCMDataManager.getInstance().setPrevSiteSales(prevsiteSalesList);
                                    }
                                }
                                else if (siteSales.data.projected!=0){
                                    siteSalesList.add(siteSales);
                                    if (counter[0] == dates.size()){
                                        DisplaySalesPerforanceScore(siteSalesList);
                                        SCMDataManager.getInstance().setSiteSales(siteSalesList);
                                        SCMDataManager.getInstance().setPrevSiteSales(prevsiteSalesList);
                                    }
                                }
                                else if (siteSales.data.sales!=null && siteSales.data.sales.size()>0){
                                    siteSalesList.add(siteSales);
                                    if (counter[0] == dates.size()){
                                        DisplaySalesPerforanceScore(siteSalesList);
                                        SCMDataManager.getInstance().setSiteSales(siteSalesList);
                                        SCMDataManager.getInstance().setPrevSiteSales(prevsiteSalesList);
                                    }
                                }else {
                                    SiteSales siteSale = new SiteSales();
                                    siteSale.salesDate = date;
                                    getSiteDefault(siteSalesList,prevsiteSalesList,date,siteSale,counter[0],dates.size());

                                }

                            }else {
                                SiteSales siteSale = new SiteSales();
                                siteSale.salesDate = date;
                                getSiteDefault(siteSalesList,prevsiteSalesList,date,siteSale,counter[0],dates.size());
                            }
                        }
                        else {
                            SiteSales siteSale = new SiteSales();
                            siteSale.salesDate = date;
                            getSiteDefault(siteSalesList,prevsiteSalesList,date,siteSale,counter[0],dates.size());
                        }
                    }
                });
            }
        }
        else {
            DisplaySalesPerforanceScore(SCMDataManager.getInstance().getSiteSales());
            for (final Date date : prevdates){
                System.out.println("PREV DATES : "+format.format(date));
                dataManager.getSiteSalesByDate(site.siteid, date, new OnCompleteListeners.getSiteSalesByDateListener() {
                    @Override
                    public void getSiteSalesByDateCallback(SiteSales siteSales, Error error) {
                        if (error == null){
                            prevsiteSalesList.add(siteSales);
                        }else {
                            SiteSales siteSale = new SiteSales();
                            siteSale.salesDate = date;
                            prevsiteSalesList.add(siteSale);
                        }
                    }
                });
            }

            for (final Date date : dates){
                dataManager.getSiteSalesByDate(site.siteid, date, new OnCompleteListeners.getSiteSalesByDateListener() {
                    @Override
                    public void getSiteSalesByDateCallback(SiteSales siteSales, Error error) {
                        counter[0]++;
                        if (error == null){
                            if (siteSales!=null && siteSales.data!=null){
                                if (siteSales.data.dss!=0){
                                    siteSalesList.add(siteSales);
                                    if (counter[0] == dates.size()){
                                        SCMDataManager.getInstance().setSiteSales(siteSalesList);
                                        SCMDataManager.getInstance().setPrevSiteSales(prevsiteSalesList);
                                    }
                                }
                                else if (siteSales.data.projected!=0){
                                    siteSalesList.add(siteSales);
                                    if (counter[0] == dates.size()){
                                        SCMDataManager.getInstance().setSiteSales(siteSalesList);
                                        SCMDataManager.getInstance().setPrevSiteSales(prevsiteSalesList);
                                    }
                                }
                                else if (siteSales.data.sales!=null && siteSales.data.sales.size()>0){
                                    siteSalesList.add(siteSales);
                                    if (counter[0] == dates.size()){
                                        SCMDataManager.getInstance().setSiteSales(siteSalesList);
                                        SCMDataManager.getInstance().setPrevSiteSales(prevsiteSalesList);
                                    }
                                }else {
                                    SiteSales siteSale = new SiteSales();
                                    siteSale.salesDate = date;
                                    getSiteDefault(siteSalesList,prevsiteSalesList,date,siteSale,counter[0],dates.size());

                                }

                            }else {
                                SiteSales siteSale = new SiteSales();
                                siteSale.salesDate = date;
                                getSiteDefault(siteSalesList,prevsiteSalesList,date,siteSale,counter[0],dates.size());
                            }
                        }
                        else {
                            SiteSales siteSale = new SiteSales();
                            siteSale.salesDate = date;
                            getSiteDefault(siteSalesList,prevsiteSalesList,date,siteSale,counter[0],dates.size());
                        }
                    }
                });
            }
        }
    }

    private  void getSiteDefault(final ArrayList<SiteSales>siteSales,
                                 final ArrayList<SiteSales>prevSiteSales,
                                 final Date date, final SiteSales sales,
                                 final int DownloadedItems, final int TotalItems){

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.setTime(date);
        SimpleDateFormat format = new SimpleDateFormat("EEE");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        String StringDayOfWeek = format.format(date);
        int dayOfWeek = 0;

        if (StringDayOfWeek.equalsIgnoreCase("Tue")){
            dayOfWeek = 1;
        }else if (StringDayOfWeek.equalsIgnoreCase("Wed")){
            dayOfWeek = 2;
        }else if (StringDayOfWeek.equalsIgnoreCase("Thu")){
            dayOfWeek = 3;
        }else if (StringDayOfWeek.equalsIgnoreCase("Fri")){
            dayOfWeek = 4;
        }else if (StringDayOfWeek.equalsIgnoreCase("Sat")){
            dayOfWeek = 5;
        }else if (StringDayOfWeek.equalsIgnoreCase("Sun")){
            dayOfWeek = 6;
        }

        aPimmDataManager dataManager = aPimmDataManager.getInstance();
        dataManager.getDefaultSiteSalesForStoreType(dayOfWeek, 1, new OnCompleteListeners.getSiteSalesDefaultListener() {
            @Override
            public void getSiteSalesDefaultCallback(final SiteSalesDefault salesDefault, Error error) {
                if (error == null){
                    sales.data = ConvertDefaultToSales(salesDefault,date,siteSales,prevSiteSales);
                    siteSales.add(sales);
                    if (DownloadedItems == TotalItems){
                        Calendar currentDate = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                        currentDate.setTime(date);
                        DisplaySalesPerforanceScore(siteSales);
                        SCMDataManager.getInstance().setSiteSales(siteSales);
                        SCMDataManager.getInstance().setPrevSiteSales(prevSiteSales);
                    }
                }else {
                    getSiteDefault(siteSales,prevSiteSales,date,sales,DownloadedItems,TotalItems);
                }
            }
        });
    }

    private ArrayList<Date> getPrevWeek(Calendar currentDate){
        ArrayList<Date> Weekdays = new ArrayList<>();
        try {
            Calendar now = Calendar.getInstance();
            now.setTimeZone(TimeZone.getTimeZone("UTC"));
            now.setTime(currentDate.getTime());
            now.add(Calendar.WEEK_OF_YEAR, -1);


            int delta = -now.get(Calendar.DAY_OF_WEEK) + 2; //add 2 if your week start on monday
            now.add(Calendar.DAY_OF_MONTH, delta );

            for (int i = 0; i < 7; i++) {
                Weekdays.add(now.getTime());
                now.add(Calendar.DAY_OF_MONTH, 1);
            }

            Collections.reverse(Weekdays);

        }catch (Exception e){
            e.printStackTrace();
        }

        return Weekdays;
    }

    private void DisplaySalesPerforanceScore(ArrayList<SiteSales> siteSales){
        SCMDataManager.getInstance().setSiteSales(siteSales);
        Calendar curretDay = SCMTool.getDateWithDaypart(new Date().getTime(),site.effectiveUTCOffset);
        SimpleDateFormat format = new SimpleDateFormat("EEE");
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            for (int i = 0; i <siteSales.size() ; i++) {
                SiteSales siteSale = siteSales.get(i);
                if (format.format(siteSale.salesDate).equalsIgnoreCase(format.format(curretDay.getTime()))){

                    if (siteSale.data!=null){
                        if (siteSale.data.dss!=0){
                            sales_forecast.setText("$"+decimalFormat.format(siteSale.data.dss));
                        }else if (siteSale.data.projected!=0){
                            sales_forecast.setText("$"+decimalFormat.format(siteSale.data.projected));
                        }else {
                            sales_forecast.setText("$0.00");
                        }

                        if (siteSale.data.sales!=null){
                            if (siteSale.data.sales.size()>0){
                                double amount =0;
                                for (SiteSalesActual actual : siteSale.data.sales){
                                    amount += actual.amount;
                                }
                                sales_actual.setText("$"+decimalFormat.format(amount));
                            }else {
                                sales_actual.setText("$0.00");
                            }
                        }else {
                            sales_actual.setText("$0.00");
                        }
                    } else {
                        sales_actual.setText("$0.00");
                        sales_forecast.setText("$0.00");
                    }
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            getDailyLabor(DashboardActivity.this,true);
                        }
                    }).start();

                    break;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
