package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_CleaningSlider;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.procuro.androidscm.Daypart;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_CleaningSlider.Daily.CleaningGuideList;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_CleaningSlider.Daily.CleaningPosition;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_CleaningSlider.Daily.Dashboard_Daily_Fragment;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_CleaningSlider.Monthly.Dashboard_Monthly_Fragment;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_CleaningSlider.Weekly.Dashboard_Weekly_Fragment;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_cleaning_tutorial_listviewAdapter;
import com.procuro.androidscm.Restaurant.DashBoard.TutorialList;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourcesGrandChildList;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.DocumentDTO;
import com.procuro.apimmdatamanagerlib.SMSCleaningDaily;
import com.procuro.apimmdatamanagerlib.SMSCleaningMonthly;
import com.procuro.apimmdatamanagerlib.SMSCleaningWeekly;
import com.procuro.apimmdatamanagerlib.SMSItem;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import io.github.douglasjunior.androidSimpleTooltip.OverlayView;
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

public class DashboardCleaningSliderActivity extends AppCompatActivity {

    private info.hoang8f.android.segmented.SegmentedGroup infogroup;
    private RadioButton daily, weekly, monthly;
    private ConstraintLayout backroom, customerview, product, services;
    private Button back;
    private TextView selectedname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_cleaning_slider);

        infogroup = findViewById(R.id.segmented2);
        daily = findViewById(R.id.daily);
        weekly = findViewById(R.id.weekly);
        monthly = findViewById(R.id.monthly);
        backroom = findViewById(R.id.backroom);
        customerview = findViewById(R.id.customerview);
        product = findViewById(R.id.production);
        services = findViewById(R.id.services);
        back = findViewById(R.id.home);
        selectedname = findViewById(R.id.username);

        selectedname.setText(SCMDataManager.getInstance().getSelectedSite().getSite().SiteName);

        GenerateDaypart();

        SetupOnclicks();

        populateCleaningData();

    }


    private void SetupOnclicks() {

        infogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == daily.getId()) {
                    DisplayDaily();
                } else if (i == weekly.getId()) {
                    DisplayWeekly();
                } else if (i == monthly.getId()) {
                    DisplayMonthly();
                }
            }
        });

        backroom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setUpBackroom();
            }
        });

        customerview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setUpCustomerview();
            }
        });

        product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setUpProduction();
            }
        });

        services.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setUpServices();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void GenerateDaypart(){

        ArrayList<Daypart>dayparts = new ArrayList<>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss a");

        Date currentDate = new Date();

        Calendar d1start = Calendar.getInstance();
        d1start.setTime(currentDate);
        d1start.set(Calendar.HOUR,4);
        d1start.set(Calendar.MINUTE,0);
        d1start.set(Calendar.SECOND,0);
        d1start.set(Calendar.AM_PM,Calendar.AM);

        Calendar d1end = Calendar.getInstance();
        d1end.setTime(currentDate);
        d1end.set(Calendar.HOUR,10);
        d1end.set(Calendar.MINUTE,59);
        d1end.set(Calendar.SECOND,59);
        d1end.set(Calendar.AM_PM,Calendar.AM);

        dayparts.add(new Daypart(1,"Daypart 1", d1start.getTime(),d1end.getTime()));

        System.out.println("DAYPART 1 : "+dateFormat.format(d1start.getTime())+" - "+dateFormat.format(d1end.getTime()));

        Calendar d2start = Calendar.getInstance();
        d2start.setTime(currentDate);
        d2start.set(Calendar.HOUR,11);
        d2start.set(Calendar.MINUTE,0);
        d2start.set(Calendar.SECOND,0);
        d2start.set(Calendar.AM_PM,Calendar.AM);

        Calendar d2end = Calendar.getInstance();
        d2end.setTime(currentDate);
        d2end.set(Calendar.HOUR,1);
        d2end.set(Calendar.MINUTE,59);
        d2end.set(Calendar.SECOND,59);
        d2end.set(Calendar.AM_PM,Calendar.PM);
        dayparts.add(new Daypart(2,"Daypart 2", d2start.getTime(),d2end.getTime()));

        System.out.println("DAYPART 2 : "+dateFormat.format(d2start.getTime())+" - "+dateFormat.format(d2end.getTime()));

        Calendar d3start = Calendar.getInstance();
        d3start.setTime(currentDate);
        d3start.set(Calendar.HOUR,2);
        d3start.set(Calendar.MINUTE,0);
        d3start.set(Calendar.SECOND,0);
        d3start.set(Calendar.AM_PM,Calendar.PM);

        Calendar d3end = Calendar.getInstance();
        d3end.setTime(currentDate);
        d3end.set(Calendar.HOUR,4);
        d3end.set(Calendar.MINUTE,59);
        d3end.set(Calendar.SECOND,59);
        d3end.set(Calendar.AM_PM,Calendar.PM);
        dayparts.add(new Daypart(3,"Daypart 3", d3start.getTime(),d3end.getTime()));

        System.out.println("DAYPART 3 : "+dateFormat.format(d3start.getTime())+" - "+dateFormat.format(d3end.getTime()));

        Calendar d4start = Calendar.getInstance();
        d4start.setTime(currentDate);
        d4start.set(Calendar.HOUR,5);
        d4start.set(Calendar.MINUTE,0);
        d4start.set(Calendar.SECOND,0);
        d4start.set(Calendar.AM_PM,Calendar.PM);

        Calendar d4end = Calendar.getInstance();
        d4end.setTime(currentDate);
        d4end.set(Calendar.HOUR,7);
        d4end.set(Calendar.MINUTE,59);
        d4end.set(Calendar.SECOND,59);
        d4end.set(Calendar.AM_PM,Calendar.PM);
        dayparts.add(new Daypart(4,"Daypart 4", d4start.getTime(),d4end.getTime()));

        System.out.println("DAYPART 4 : "+dateFormat.format(d4start.getTime())+" - "+dateFormat.format(d4end.getTime()));


        Calendar d5start = Calendar.getInstance();
        d5start.setTime(currentDate);
        d5start.set(Calendar.HOUR,8);
        d5start.set(Calendar.MINUTE,0);
        d5start.set(Calendar.SECOND,0);
        d5start.set(Calendar.AM_PM,Calendar.PM);

        Calendar d5end = Calendar.getInstance();
        d5end.setTime(currentDate);
        d5end.set(Calendar.HOUR,9);
        d5end.set(Calendar.MINUTE,59);
        d5end.set(Calendar.SECOND,59);
        d5end.set(Calendar.AM_PM,Calendar.PM);
        dayparts.add(new Daypart(5,"Daypart 5", d5start.getTime(),d5end.getTime()));

        System.out.println("DAYPART 5 : "+dateFormat.format(d5start.getTime())+" - "+dateFormat.format(d5end.getTime()));


        Calendar d6start = Calendar.getInstance();
        d6start.setTime(currentDate);
        d6start.set(Calendar.HOUR,10);
        d6start.set(Calendar.MINUTE,0);
        d6start.set(Calendar.SECOND,0);
        d6start.set(Calendar.AM_PM,Calendar.PM);

        Calendar d6end = Calendar.getInstance();
        d6end.setTime(currentDate);
        d6end.set(Calendar.HOUR,3);
        d6end.set(Calendar.MINUTE,59);
        d6end.set(Calendar.SECOND,59);
        d6end.set(Calendar.AM_PM,Calendar.AM);
        d6end.add(Calendar.DATE,1);
        dayparts.add(new Daypart(6,"Daypart 6", d6start.getTime(),d6end.getTime()));
        System.out.println("DAYPART 6 : "+dateFormat.format(d6start.getTime())+" - "+dateFormat.format(d6end.getTime()));

        SCMDataManager.getInstance().setDayparts(dayparts);


    }

    private void setUpBackroom() {

        ArrayList<ResourcesGrandChildList>list =SCMDataManager.getInstance().getDashboardbackroom();

        Collections.sort(list, new Comparator<ResourcesGrandChildList>() {
            @Override
            public int compare(ResourcesGrandChildList o1, ResourcesGrandChildList o2) {
                return o1.getDocumentDTO().name.compareToIgnoreCase(o2.getDocumentDTO().name);
            }
        });

        ArrayList<TutorialList>tutorialLists = new ArrayList<>();

        for (ResourcesGrandChildList child : list){
            DocumentDTO documentDTO = child.getDocumentDTO();

            String s = documentDTO.name;

            String[] arr = s.split(" ");

            String id = "";
            StringBuilder name = new StringBuilder();

            for (int i = 0; i <arr.length ; i++) {
                if (i == 0){
                    id = arr[i];
                }else if (i >= 2){
                    if (i == 2){
                        name.append(arr[i]);
                    }else {
                        name.append(" ").append(arr[i]);
                    }
                }
            }
            tutorialLists.add(new TutorialList(id,name.toString().replace("-",""),documentDTO));

        }

        displayTutorialPopup(this, backroom, "Backroom & Exterior", tutorialLists);

    }

    private void setUpCustomerview() {
        ArrayList<ResourcesGrandChildList>list =SCMDataManager.getInstance().getDashboardcustomerview();

        Collections.sort(list, new Comparator<ResourcesGrandChildList>() {
            @Override
            public int compare(ResourcesGrandChildList o1, ResourcesGrandChildList o2) {
                return o1.getDocumentDTO().name.compareToIgnoreCase(o2.getDocumentDTO().name);
            }
        });

        ArrayList<TutorialList>tutorialLists = new ArrayList<>();

        for (ResourcesGrandChildList child : list){
            DocumentDTO documentDTO = child.getDocumentDTO();

            String s = documentDTO.name;

            String[] arr = s.split(" ");

            String id = "";
            StringBuilder name = new StringBuilder();

            for (int i = 0; i <arr.length ; i++) {
                if (i == 0){
                    id = arr[i];
                }else if (i >= 2){
                    if (i == 2){
                        name.append(arr[i]);
                    }else {
                        name.append(" ").append(arr[i]);
                    }
                }
            }
            tutorialLists.add(new TutorialList(id,name.toString().replace("-",""),documentDTO));

        }
        displayTutorialPopup(this, customerview, "Customer View", tutorialLists);

    }

    private void setUpProduction() {
        ArrayList<ResourcesGrandChildList>list =SCMDataManager.getInstance().getDashboardproduction();

        Collections.sort(list, new Comparator<ResourcesGrandChildList>() {
            @Override
            public int compare(ResourcesGrandChildList o1, ResourcesGrandChildList o2) {
                return o1.getDocumentDTO().name.compareToIgnoreCase(o2.getDocumentDTO().name);
            }
        });

        ArrayList<TutorialList>tutorialLists = new ArrayList<>();

        for (ResourcesGrandChildList child : list){
            DocumentDTO documentDTO = child.getDocumentDTO();

            String s = documentDTO.name;

            String[] arr = s.split(" ");

            String id = "";
            StringBuilder name = new StringBuilder();

            for (int i = 0; i <arr.length ; i++) {
                if (i == 0){
                    id = arr[i];
                }else if (i >= 2){
                    if (i == 2){
                        name.append(arr[i]);
                    }else {
                        name.append(" ").append(arr[i]);
                    }
                }
            }
            tutorialLists.add(new TutorialList(id,name.toString().replace("-",""),documentDTO));

        }
        displayTutorialPopup(this, product, "Production", tutorialLists);

    }

    private void setUpServices() {
        ArrayList<ResourcesGrandChildList>list =SCMDataManager.getInstance().getDashboardservices();

        Collections.sort(list, new Comparator<ResourcesGrandChildList>() {
            @Override
            public int compare(ResourcesGrandChildList o1, ResourcesGrandChildList o2) {
                return o1.getDocumentDTO().name.compareToIgnoreCase(o2.getDocumentDTO().name);
            }
        });

        ArrayList<TutorialList>tutorialLists = new ArrayList<>();

        for (ResourcesGrandChildList child : list){
            DocumentDTO documentDTO = child.getDocumentDTO();

            String s = documentDTO.name;

            String[] arr = s.split(" ");

            String id = "";
            StringBuilder name = new StringBuilder();

            for (int i = 0; i <arr.length ; i++) {
                if (i == 0){
                    id = arr[i];
                }else if (i >= 2){
                    if (i == 2){
                        name.append(arr[i]);
                    }else {
                        name.append(" ").append(arr[i]);
                    }
                }
            }
            tutorialLists.add(new TutorialList(id,name.toString().replace("-",""),documentDTO));

        }
        displayTutorialPopup(this, services, "Services", tutorialLists);

    }

    public void displayTutorialPopup(final Context context, ConstraintLayout anchor, String title, ArrayList<TutorialList> tutorialLists) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.TOP)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .arrowColor(Color.WHITE)
                .transparentOverlay(false)
                .highlightShape(OverlayView.HIGHLIGHT_SHAPE_RECTANGULAR)
                .overlayOffset(5)
                .contentView(R.layout.dashboard_tutorial_list_popup)
                .focusable(true)
                .build();
        tooltip.show();

        TextView Titles = tooltip.findViewById(R.id.title);
        ListView listView = tooltip.findViewById(R.id.listView);

        Titles.setText(title);

        Dashboard_cleaning_tutorial_listviewAdapter adapter = new Dashboard_cleaning_tutorial_listviewAdapter(context, tutorialLists, title, this);
        listView.setAdapter(adapter);

    }

    private void DisplayDaily() {
        getSupportFragmentManager().beginTransaction().replace(R.id.cleaning_fragment_container,
                new Dashboard_Daily_Fragment()).commit();
    }


    private void populateCleaningData() {

        if (SCMDataManager.getInstance().getCleaningDailyForm() != null) {
            SMSCleaningDaily dailyform = SCMDataManager.getInstance().getCleaningDailyForm();
            ArrayList<CleaningGuideList> guideDailies = new ArrayList<>();

            ArrayList<SMSItem> shift1 = dailyform.shift1Items;
            ArrayList<SMSItem> shift2 = dailyform.shift2Items;


            String CurrentDaypart = SCMTool.getCurrentDaypart();

            if (shift1 != null) {
                ArrayList<CleaningPosition> cleaningPosition1 = new ArrayList<>();
                for (SMSItem item : shift1) {
                    cleaningPosition1.add(new CleaningPosition(item.position, item.task, item.employee, item.assignedDate, item.completed));
                }

                if (CurrentDaypart.contains("Daypart 1") || CurrentDaypart.contains("Daypart 2") || CurrentDaypart.contains("Daypart 3") ){
                    guideDailies.add(new CleaningGuideList("Daypart 1 - 3", true, cleaningPosition1));
                }else {
                    guideDailies.add(new CleaningGuideList("Daypart 1 - 3", false, cleaningPosition1));
                }
            }

            if (shift2 != null) {
                ArrayList<CleaningPosition> cleaningPosition2 = new ArrayList<>();
                for (SMSItem item : shift2) {
                    cleaningPosition2.add(new CleaningPosition(item.position, item.task, item.employee, item.assignedDate, item.completed));
                }

                if (CurrentDaypart.contains("Daypart 4") || CurrentDaypart.contains("Daypart 5") || CurrentDaypart.contains("Daypart 6") ){
                    guideDailies.add(new CleaningGuideList("Daypart 4 - 6", true, cleaningPosition2));
                }else {
                    guideDailies.add(new CleaningGuideList("Daypart 4 - 6", false, cleaningPosition2));
                }
            }

            SCMDataManager.getInstance().setCleaningGuideDailies(guideDailies);
            daily.setChecked(true);
            populateWeeklyCleaningData();
        }


    }

    private void populateWeeklyCleaningData(){

        if (SCMDataManager.getInstance().getCleaningGuideWeekly() == null){
            ArrayList<CleaningGuideList>cleaningGuideLists = new ArrayList<>();
            if (SCMDataManager.getInstance().getCleaningWeeklyForm() !=null){
                SMSCleaningWeekly weeklyform = SCMDataManager.getInstance().getCleaningWeeklyForm();
                ArrayList<SMSItem>customerViewItems = weeklyform.customerViewItems;
                ArrayList<SMSItem>serviceItems = weeklyform.serviceItems;
                ArrayList<SMSItem>productionItems = weeklyform.productionItems;
                ArrayList<SMSItem>backroomItems = weeklyform.backroomItems;

                if (customerViewItems!=null){
                    ArrayList<CleaningPosition> customerview = new ArrayList<>();
                    for (SMSItem item : customerViewItems){
                        customerview.add(new CleaningPosition(item.position, item.task, item.employee, item.assignedDate, item.completed));
                    }
                    cleaningGuideLists.add(new CleaningGuideList("Customer View",true,false, customerview));
                }

                if (serviceItems!=null){
                    ArrayList<CleaningPosition> Services = new ArrayList<>();
                    for (SMSItem item : serviceItems){
                        Services.add(new CleaningPosition(item.position, item.task, item.employee, item.assignedDate, item.completed));
                    }
                    cleaningGuideLists.add(new CleaningGuideList("Service",false,false, Services));
                }

                if (productionItems!=null){
                    ArrayList<CleaningPosition> Production = new ArrayList<>();
                    for (SMSItem item : productionItems){
                        Production.add(new CleaningPosition(item.position, item.task, item.employee, item.assignedDate, item.completed));
                    }
                    cleaningGuideLists.add(new CleaningGuideList("Production",false,false, Production));
                }

                if (backroomItems!=null){
                    ArrayList<CleaningPosition> Backroom = new ArrayList<>();
                    for (SMSItem item : backroomItems){
                        Backroom.add(new CleaningPosition(item.position, item.task, item.employee, item.assignedDate, item.completed));
                    }
                    cleaningGuideLists.add(new CleaningGuideList("Backroom",false,false, Backroom));
                }
            }
            SCMDataManager.getInstance().setCleaningGuideWeekly(cleaningGuideLists);
            populateMonthlyCleaningData();
        }

    }

    private void populateMonthlyCleaningData(){

        if (SCMDataManager.getInstance().getCleaningMonthly() == null){
            ArrayList<CleaningGuideList>cleaningGuideLists = new ArrayList<>();

            if (SCMDataManager.getInstance().getCleaningMonthlyForm() !=null){
                SMSCleaningMonthly weeklyform = SCMDataManager.getInstance().getCleaningMonthlyForm();

                ArrayList<SMSItem>customerViewItems = weeklyform.customerViewItems;
                ArrayList<SMSItem>serviceItems = weeklyform.serviceItems;
                ArrayList<SMSItem>productionItems = weeklyform.productionItems;
                ArrayList<SMSItem>backroomItems = weeklyform.backroomItems;

                if (customerViewItems!=null){
                    ArrayList<CleaningPosition> customerview = new ArrayList<>();
                    for (SMSItem item : customerViewItems){
                        customerview.add(new CleaningPosition(item.position, item.task, item.employee, item.assignedDate, item.completed));
                    }
                    cleaningGuideLists.add(new CleaningGuideList("Customer View",true,false, customerview));
                }

                if (serviceItems!=null){
                    ArrayList<CleaningPosition> Services = new ArrayList<>();
                    for (SMSItem item : serviceItems){
                        Services.add(new CleaningPosition(item.position, item.task, item.employee, item.assignedDate, item.completed));
                    }
                    cleaningGuideLists.add(new CleaningGuideList("Service",false,false, Services));
                }

                if (productionItems!=null){
                    ArrayList<CleaningPosition> Production = new ArrayList<>();
                    for (SMSItem item : productionItems){
                        Production.add(new CleaningPosition(item.position, item.task, item.employee, item.assignedDate, item.completed));
                    }
                    cleaningGuideLists.add(new CleaningGuideList("Production",false,false, Production));
                }

                if (backroomItems!=null){
                    ArrayList<CleaningPosition> Backroom = new ArrayList<>();
                    for (SMSItem item : backroomItems){
                        Backroom.add(new CleaningPosition(item.position, item.task, item.employee, item.assignedDate, item.completed));
                    }
                    cleaningGuideLists.add(new CleaningGuideList("Backroom",false,false, Backroom));
                }
            }
            SCMDataManager.getInstance().setCleaningMonthly(cleaningGuideLists);

        }

    }

    private void DisplayWeekly() {
        getSupportFragmentManager().beginTransaction().replace(R.id.cleaning_fragment_container,
                new Dashboard_Weekly_Fragment()).commit();
    }
    private void DisplayMonthly() {
       getSupportFragmentManager().beginTransaction().replace(R.id.cleaning_fragment_container,
                new Dashboard_Monthly_Fragment()).commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


}
