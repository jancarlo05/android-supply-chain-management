package com.procuro.androidscm.Restaurant.AllStorePopup;

import android.app.ActivityOptions;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.google.gson.Gson;
import com.procuro.androidscm.GlideApp;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.DashboardActivity;
import com.procuro.androidscm.Restaurant.AlertFragment.AlertFragment;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationFragment;
import com.procuro.androidscm.Restaurant.ReportsFragment.Reports_fragment;
import com.procuro.androidscm.Restaurant.Restaurant_home_page;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.StoreListFragment.SiteList;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.TemperatureListFragment.StatusSensorFragment;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.Site;

import java.util.ArrayList;


public class Status_StoreListDialogFragment_adapter extends BaseExpandableListAdapter {

    final Context context ;
    ArrayList<SiteList> parent_lists;
    TextView storeName;
    FragmentActivity fragmentActivity;
    Dialog dialogFragment;
    String fragment;

    public Status_StoreListDialogFragment_adapter(Context context, ArrayList<SiteList> data , FragmentActivity fragmentActivity,Dialog dialogFragment,String fragment) {
        this.context = context;
        this.parent_lists = data;
        this.fragmentActivity = fragmentActivity;
        this.dialogFragment = dialogFragment;
        this.fragment = fragment;

    }

    @Override
    public int getGroupCount() {
        return parent_lists.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return parent_lists.get(i).getCustomSites().size();
    }

    @Override
    public Object getGroup(int i) {
        return parent_lists.get(i);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return parent_lists.get(groupPosition).getCustomSites().get(childPosition);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int position, boolean b, View contentView, ViewGroup parent) {
        SiteList parentList = parent_lists.get(position);
        contentView = LayoutInflater.from(context).inflate(R.layout.status_storelist_parent_data, parent,false);

        TextView name = contentView.findViewById(R.id.name);
        name.setText(parentList.getProvince());

        return contentView;
    }


    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean b, View contentView, final ViewGroup parent) {
        final CustomSite childlist = parent_lists.get(groupPosition).getCustomSites().get(childPosition);
        contentView = LayoutInflater.from(context).inflate(R.layout.status_storelist_child_data_with_scorecard, parent, false);

        ImageView icon = contentView.findViewById(R.id.icon);
        ImageView scorecardContainer = contentView.findViewById(R.id.score_container);
        TextView score = contentView.findViewById(R.id.score);
        TextView name = contentView.findViewById(R.id.name);
        contentView.setBackgroundResource(R.drawable.onpress_design);

        if (childlist.getSite()!=null){
            if (childlist.getSite().AlarmSeverity == 0 ){
                GlideApp.with(context).load(R.drawable.store_blue).into(icon);
            }else if (childlist.getSite().AlarmSeverity == 3 ){
                GlideApp.with(context).load(R.drawable.store_green).into(icon);
            }else if (childlist.getSite().AlarmSeverity == 5 ){
                GlideApp.with(context).load(R.drawable.store_yellow).into(icon);
            }else if (childlist.getSite().AlarmSeverity == 9 ){
                GlideApp.with(context).load(R.drawable.store_red).into(icon);
            }else {
                GlideApp.with(context).load(R.drawable.store_gray).into(icon);
                icon.setAlpha(.5f);
            }
        }else {
            GlideApp.with(context).load(R.drawable.store_gray).into(icon);
            icon.setAlpha(.5f);
        }
        GlideApp.with(context).load(R.drawable.empty_circle_orange).into(scorecardContainer);
        score.setText("");
        name.setText(childlist.getSite().SiteName);

        contentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SCMDataManager.getInstance().setSelectedSite(childlist);
                SCMDataManager.getInstance().setReportsSelectedSite(null);
                SCMDataManager.getInstance().setReportLevel("Site");
                setSharedpref(fragmentActivity);
                LoadActivity(fragmentActivity,childlist);
                dialogFragment.dismiss();
            }
        });

        scorecardContainer.setVisibility(View.GONE);

        return contentView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }


    private void LoadActivity(FragmentActivity fragmentActivity,CustomSite site) {
        if (fragment.equalsIgnoreCase("status")) {
            DownloadInitialSiteRequirements(site.getSite(),true);
            StatusSensorFragment fragment = new StatusSensorFragment();
            fragmentActivity.getSupportFragmentManager().beginTransaction()
                    .replace(R.id.resturant_fragment_container,
                            fragment).commit();

        }else if (fragment.equalsIgnoreCase("alerts")){
            DownloadInitialSiteRequirements(site.getSite(),true);
            fragmentActivity.getSupportFragmentManager().beginTransaction()
                    .replace(R.id.resturant_fragment_container,
                            new AlertFragment()).commit();

        }else if (fragment.equalsIgnoreCase("reports")){
            DownloadInitialSiteRequirements(site.getSite(),true);
            fragmentActivity.getSupportFragmentManager().beginTransaction()
                    .replace(R.id.resturant_fragment_container,
                            new Reports_fragment()).commit();
        }
        else if (fragment.equalsIgnoreCase("Dashboard")){
            DownloadInitialSiteRequirements(site.getSite(),false);
            fragmentActivity.finish();
            Intent intent = new Intent(fragmentActivity, DashboardActivity.class);
            fragmentActivity.startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(fragmentActivity).toBundle());

        }else if (fragment.equalsIgnoreCase("Qualifications")){
            SCMDataManager.getInstance().setSelectedSite(site);
            setSharedpref(fragmentActivity);
            DownloadInitialSiteRequirements(site.getSite(),true);
            SCMDataManager.getInstance().setReportsSelectedSite(null);
            SCMDataManager.getInstance().setReportLevel("Site");
            displayStoreTemperature(fragmentActivity,site);
        }
    }

    private void displayStoreTemperature(FragmentActivity fragmentActivity, CustomSite storeList) {
        fragmentActivity.getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_left,R.anim.slide_left_out)
                .replace(R.id.resturant_fragment_container,
                        new QualificationFragment()).commit();
    }

    private void setSharedpref(FragmentActivity fragmentActivity){
        SharedPreferences mypref = PreferenceManager.getDefaultSharedPreferences(fragmentActivity);
        SharedPreferences.Editor prefsEditor = mypref.edit();
        Gson gson = new Gson();
        String json = gson.toJson(SCMDataManager.getInstance());
        prefsEditor.putString("SCMDataManager", json);
        prefsEditor.commit();
    }

    private void DownloadInitialSiteRequirements(Site site,boolean hasProgressDialog){
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Please Wait");
        progressDialog.setMessage("Downloading Data");
        progressDialog.show();
        if (hasProgressDialog){
            Restaurant_home_page.DownloadInitialData(site,progressDialog);
        }else {
            Restaurant_home_page.DownloadInitialData(site,null);
        }
        SCMDataManager.getInstance().setSiteSales(null);
        SCMDataManager.getInstance().setPrevSiteSales(null);
        SCMDataManager.getInstance().setPositionForm(null);
        SCMDataManager.getInstance().setCleaningDailyForm(null);
        SCMDataManager.getInstance().setCleaningWeeklyForm(null);
        SCMDataManager.getInstance().setCleaningMonthly(null);
        SCMDataManager.getInstance().setDailyOpsPlanForm(null);
        SCMDataManager.getInstance().setSmsCommunicationNotes(null);
        SCMDataManager.getInstance().setDashboardKitchenData(null);
        SCMDataManager.getInstance().setSmsDashboard(null);
    }

}