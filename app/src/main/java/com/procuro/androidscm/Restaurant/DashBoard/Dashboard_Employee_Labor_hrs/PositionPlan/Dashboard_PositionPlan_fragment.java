package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.PositionPlan;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.procuro.androidscm.Daypart;
import com.procuro.androidscm.GlideApp;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.DashBoard.DashboardActivity;
import com.procuro.androidscm.Restaurant.Schema.Dayparts;
import com.procuro.androidscm.Restaurant.Schema.Schema;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.DashboardKitchenData;
import com.procuro.apimmdatamanagerlib.KitchenDataItems;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.PositionDataItems;
import com.procuro.apimmdatamanagerlib.SMSPositioning;
import com.procuro.apimmdatamanagerlib.SMSPositioningEmployeeScheduleAssignment;
import com.procuro.apimmdatamanagerlib.SMSPositioningEmployeeScheduleAssignmentPositions;
import com.procuro.apimmdatamanagerlib.Schedule_Buisness;
import com.procuro.apimmdatamanagerlib.Schedule_Business_Site_Plan;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

import static com.procuro.androidscm.Restaurant.DashBoard.Dasboard_Main_Process.Dashboard_Process.DisplayAdditionalKitchenComponent;
import static com.procuro.androidscm.Restaurant.DashBoard.Dasboard_Main_Process.Dashboard_Process.DisplayKitchenComponents;
import static com.procuro.androidscm.Restaurant.DashBoard.Dasboard_Main_Process.Dashboard_Process.DisplayPositions;
import static com.procuro.androidscm.Restaurant.DashBoard.Dasboard_Main_Process.Dashboard_Process.getPositionCovered;
import static com.procuro.androidscm.Restaurant.DashBoard.Dasboard_Main_Process.Dashboard_Process.getpositionDataItems;
import static com.procuro.androidscm.SCMTool.DaypartTimeToDate;
import static com.procuro.androidscm.SCMTool.getStringCurrentDaypart;


public class Dashboard_PositionPlan_fragment extends Fragment {

    public  static TextView required_position , daypart_name , daypart_time,switch_button_indicator_closed,switch_button_indicator_open;
    public static ImageView kitchen_layout;
    public static RelativeLayout kitchenLayoutContainer;
    public static RadioButton dr_open ,dr_close;
    public static boolean isFGPopulated = false;
    public static ListView listView;
    public static Dashboard_Position_plan_position_listviewAdapter adapter;
    public static RecyclerView recyclerView;
    public static TextView date;
    public static ConstraintLayout date_container;
    public static View view;
    public static boolean isDatepicerUse;
    public static info.hoang8f.android.segmented.SegmentedGroup infogroup;
    public static int id;
    public static com.suke.widget.SwitchButton dinningnRoom ;
    public static ProgressDialog progressDialog;
    public Dashboard_PositionPlan_fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dashboard_header_staffing_monthly_sched_fragment, container, false);
        required_position = view.findViewById(R.id.required_position);
        daypart_name = view.findViewById(R.id.daypart_name);
        daypart_time = view.findViewById(R.id.daypart_time);

        kitchen_layout = view.findViewById(R.id.kitchen_layout);
        kitchenLayoutContainer = view.findViewById(R.id.kitchen_layout_container);
        listView = view.findViewById(R.id.listView);
        recyclerView = view.findViewById(R.id.recyclerView);

        date = view.findViewById(R.id.date);
        date_container =view.findViewById(R.id.date_container);
        infogroup = view.findViewById(R.id.segmented2);
        dr_close = view.findViewById(R.id.dr_close);
        dr_open = view.findViewById(R.id.dr_open);

        dinningnRoom = view.findViewById(R.id.switch_button);
        switch_button_indicator_closed = view.findViewById(R.id.switch_button_indicator_closed);
        switch_button_indicator_open = view.findViewById(R.id.switch_button_indicator_open);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Please Wait");
        progressDialog.setCancelable(false);

        Site site = SCMDataManager.getInstance().getSelectedSite().getSite();
        Calendar currentDate = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        currentDate = SCMTool.getDateWithDaypart(currentDate.getTime().getTime(),site.effectiveUTCOffset);


        CheckSChedules(currentDate.getTime(),true);

        setupOnclicks();

        return view;
    }

    private void CheckSChedules(Date date,boolean isFromDefaut){
        progressDialog.show();
        SCMDataManager scm = SCMDataManager.getInstance();
        if (isFromDefaut){
            if (scm.getDailySchedule()!=null){
                setupData(date,getEmployeeSchedule(scm.getDailySchedule(),date));
            }else {
                DownloadSelectedSchedule(date,isFromDefaut);
            }
        }else {
            DownloadSelectedSchedule(date,isFromDefaut);
        }
    }

    private void DownloadSelectedSchedule(final Date date, final boolean isDefault){

        SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        Site site = SCMDataManager.getInstance().getSelectedSite().getSite();
        aPimmDataManager dataManager = aPimmDataManager.getInstance();
        dataManager.getSchedulesByDate(site.siteid, date, date, new OnCompleteListeners.getEmployeeScheduleListener() {
            @Override
            public void getEmployeeScheduleCallback(ArrayList<Schedule_Business_Site_Plan> schedules, Error error) {
                if (isDefault){
                    SCMDataManager.getInstance().setDailySchedule(schedules);
                }
                setupData(date,getEmployeeSchedule(schedules,date));
            }
        });
    }

    private ArrayList<CustomUser>getEmployeeSchedule(ArrayList<Schedule_Business_Site_Plan>schedules,Date date){
        ArrayList<CustomUser>users = SCMTool.getActiveCustomUser(SCMDataManager.getInstance().getStoreUsers());
        SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd,yyyy");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        if (schedules!=null){
            for (CustomUser user : users){
                ArrayList<Schedule_Business_Site_Plan>FilteredSchedules = new ArrayList<>();
                for (Schedule_Business_Site_Plan sched : schedules){
                    if (format.format(date).equalsIgnoreCase(format.format(sched.date))){
                        if (user.getUser().userId.equalsIgnoreCase(sched.userID)){
                            FilteredSchedules.add(sched);
                        }
                    }
                }
                user.setSchedules(FilteredSchedules);
            }
        }

        return users;
    }

    private void setupOnclicks(){
        date_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayDatePicker(getContext(),date_container);
            }
        });

        infogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                id = checkedId;
            }
        });
    }

    private void setupData(Date date,ArrayList<CustomUser>users){

        Schema schema = Schema.getInstance();
        SimpleDateFormat dateformat = new SimpleDateFormat("EEE");
        dateformat.setTimeZone(TimeZone.getTimeZone("UTC"));

        ArrayList<PositionPlanData>PositionPlanData = new ArrayList<>();

        Dayparts CurrentDaypart = SCMTool.getActualCurrentDaypartStart();

        for(Dayparts daypart: schema.getDayparts()){
            boolean selected = false;

            if (CurrentDaypart.name.equalsIgnoreCase(daypart.name)){
                selected = true;
            }

            Calendar start = DaypartTimeToDate(daypart.start, date);
            Calendar end = DaypartTimeToDate(daypart.end, date);

            if (daypart.name.equalsIgnoreCase("6")) {
                end.add(Calendar.DATE, 1);
            }

            int RequiredPosition = SCMTool.getSiteSettingsConfigurationForCurrentDay(daypart.name,dateformat.format(date));
            if (RequiredPosition == 0){
                RequiredPosition = 4;
            }
            PositionPlanData.add(new PositionPlanData(daypart,selected,start.getTime(),end.getTime(),RequiredPosition,date));
        }

        setUpAndDisplayDaypart(date,PositionPlanData,users);
    }


    private void setUpAndDisplayDaypart(Date currentDate, ArrayList<PositionPlanData>positionPlanData,ArrayList<CustomUser>customUsers){
        try {

            Dashboard_Position_plan_daypart_RecyclerViewAdapter adapter = new Dashboard_Position_plan_daypart_RecyclerViewAdapter(getContext(),
                    positionPlanData,currentDate,getActivity(),
                    customUsers);
            recyclerView.setLayoutManager(new GridLayoutManager(getContext(),3));
            recyclerView.setAdapter(adapter);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void DisplayDaily(Date currentDate,int RequiredPosition,
                                    Context context , Activity activity,
                                    PositionPlanData positionPlanData,
                                    ArrayList<CustomUser>users){

        SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        date.setText(format.format(currentDate));
        try {

            setUPKitchenLayout(SCMDataManager.getInstance().getDashboardKitchenData(),
                    RequiredPosition,context,users,positionPlanData);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void setUPKitchenLayout(DashboardKitchenData dashboardKitchenData,
                                          int PositionCovered, Context context,ArrayList<CustomUser>users,
                                          PositionPlanData positionPlanData){

        kitchenLayoutContainer.removeAllViews();
        kitchen_layout = new ImageView(context);
        kitchen_layout.setId(R.id.kitchen_layout);
        kitchen_layout.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        kitchenLayoutContainer.addView(kitchen_layout);

        boolean isFGPopulated = false;
        double multipler = SCMTool.multiplier(dashboardKitchenData.kitchenWidth,context);


        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        Site site = SCMDataManager.getInstance().getSelectedSite().getSite();

        String currentDaypart = SCMTool.getStringCurrentDaypart();

        kitchenLayoutContainer.getLayoutParams().width = SCMTool.ApplyMultiplier(dashboardKitchenData.kitchenWidth,multipler);
        kitchenLayoutContainer.getLayoutParams().height = SCMTool.ApplyMultiplier(dashboardKitchenData.kitchenHeight,multipler);

        Glide.with(context)
                .load(dashboardKitchenData.kitchenImageData)
                .override(SCMTool.ApplyMultiplier(dashboardKitchenData.kitchenWidth,multipler),
                        SCMTool.ApplyMultiplier(dashboardKitchenData.kitchenHeight,multipler))
                .into(kitchen_layout);

        for (KitchenDataItems kitchenDataItems : dashboardKitchenData.kitchenDataItems) {

            if (!kitchenDataItems.imageType.equalsIgnoreCase("FREEZER")) {
                ImageView imageView = new ImageView(context);
                imageView.setId(View.generateViewId());
                RelativeLayout.LayoutParams newParams = new RelativeLayout.LayoutParams(
                        SCMTool.ApplyMultiplier(kitchenDataItems.width, multipler), SCMTool.ApplyMultiplier(kitchenDataItems.height, multipler));
                newParams.setMarginStart(SCMTool.ApplyMultiplier(kitchenDataItems.xcoordinate, multipler));
                newParams.topMargin = SCMTool.ApplyMultiplier(kitchenDataItems.ycoordinate, multipler);
                imageView.setLayoutParams(newParams);

                int image = SCMTool.getDashboardPositionImage(kitchenDataItems.imageType, dashboardKitchenData.kitchenTemplateCode, isFGPopulated);
                GlideApp.with(context).load(image).into(imageView);
                kitchenLayoutContainer.addView(imageView);
            }
        }


        ArrayList<PositionDataItems>dashboard_position_data = getpositionDataItems(dashboardKitchenData,currentDaypart,dinningnRoom.isChecked());

        DisplayPositions(dashboardKitchenData,dashboard_position_data,context,PositionCovered);

        DisplayPositionList(getFilterdPositionItems(dashboard_position_data,PositionCovered),context,positionPlanData,users);

        try {
            if (progressDialog.isShowing()){
                progressDialog.dismiss();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void DisplayPositions(DashboardKitchenData dashboardKitchenData,ArrayList<PositionDataItems> positionDataItems,Context context,int totalPosition ){

        double multipler = SCMTool.multiplier(dashboardKitchenData.kitchenWidth,context);

        for (PositionDataItems dataItems: positionDataItems){
            if (totalPosition == dataItems.totalEmployees){

                TextView imageView = new TextView(context);
                imageView.setId(View.generateViewId());
                RelativeLayout.LayoutParams newParams = new RelativeLayout.LayoutParams(
                        SCMTool.ApplyMultiplier(dataItems.width,multipler),  SCMTool.ApplyMultiplier(dataItems.height,multipler));
                newParams.setMarginStart( SCMTool.ApplyMultiplier(dataItems.xcoordinate,multipler));
                newParams.topMargin =  SCMTool.ApplyMultiplier(dataItems.ycoordinate,multipler);
                imageView.setLayoutParams(newParams);

                imageView.setText(String.valueOf(dataItems.position));
                imageView.setAllCaps(false);
                imageView.setGravity(Gravity.CENTER);
                imageView.setTextSize(9);
                imageView.setTextColor(Color.BLACK);

                kitchenLayoutContainer.addView(SCMTool.CreateDashboardPositionBackground(imageView,"#"+dataItems.color));
            }
        }
    }



    public static ArrayList<PositionDataItems> getpositionDataItems(DashboardKitchenData dashboardKitchenData,String CurrentDaypart,boolean iSDinningRoomOpen){
        ArrayList<PositionDataItems>items = new ArrayList<>();

        if (CurrentDaypart.toLowerCase().equalsIgnoreCase("1")){
            if (iSDinningRoomOpen){
                items = dashboardKitchenData.breakfastPositionDataItems_DROpen;
            }else {
                items = dashboardKitchenData.breakfastPositionDataItems_DRClosed;
            }
        }else {
            if (iSDinningRoomOpen){
                items = dashboardKitchenData.positionDataItems;
            }else {
                items = dashboardKitchenData.lateNightPositionDataItems;
            }
        }

        return items;
    }

    public static void DisplayPositionList(ArrayList<PositionDataItems>PositionList,
                                           Context context,PositionPlanData positionPlanData,ArrayList<CustomUser>users){
        try {
            adapter = new Dashboard_Position_plan_position_listviewAdapter(context,PositionList,users,positionPlanData);
            listView.setAdapter(adapter);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static ArrayList<PositionDataItems> getFilterdPositionItems(ArrayList<PositionDataItems>PositionLis,int PositionCovered){
        ArrayList<PositionDataItems>positionPlanData = new ArrayList<>();
        ArrayList<Integer>integers = new ArrayList<>();
        for (PositionDataItems dataItems: PositionLis){
            if (dataItems.position >0){
                if (PositionCovered == dataItems.totalEmployees){
                    if (!integers.contains(dataItems.position)){
                        integers.add(dataItems.position);
                        positionPlanData.add(dataItems);
                    }
                }
            }
        }
        return positionPlanData;
    }


    public void DisplayDatePicker(final Context context, View anchor) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.BOTTOM)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .arrowColor(ContextCompat.getColor(context,R.color.white))
                .transparentOverlay(true)
                .contentView(R.layout.date_picker)
                .focusable(true)
                .build();
        tooltip.show();


        final DatePicker datePicker =tooltip.findViewById(R.id.datepicker);
        Button accept = tooltip.findViewById(R.id.apply);
        final Site site  = SCMDataManager.getInstance().getSelectedSite().getSite();
        SimpleDateFormat format = new SimpleDateFormat("EEE,MMM dd yyyy");
        Calendar currentDay = Calendar.getInstance();
        currentDay.setTimeZone(TimeZone.getTimeZone("UTC"));
        currentDay.add(Calendar.MINUTE,site.effectiveUTCOffset);


        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar date = Calendar.getInstance();
                date.setTimeZone(TimeZone.getTimeZone("UTC"));
                date.set(Calendar.YEAR,datePicker.getYear());
                date.set(Calendar.MONTH,datePicker.getMonth());
                date.set(Calendar.DATE,datePicker.getDayOfMonth());
                date.add(Calendar.MINUTE,site.effectiveUTCOffset);
                isDatepicerUse = true;


                CheckSChedules(date.getTime(),false);
                tooltip.dismiss();
            }
        });

    }




}
