package com.procuro.androidscm.Restaurant.Schema;

import com.procuro.androidscm.SCMDataManager;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class Schema {

    private String jsonSchema;

    private ArrayList<Forms>forms;
    private ArrayList<Days>days;
    private ArrayList<Actions>actions;
    private ArrayList<Dayparts>dayparts;

    private ArrayList<Days>foodsafety;
    private ArrayList<Days>checklist;

    private static Schema instance = new Schema();

    public static Schema getInstance() {
        if(instance == null) {
            instance = new Schema();
        }
        return instance;
    }

    public String getJsonSchema() {
        return jsonSchema;
    }

    public void setJsonSchema(String jsonSchema) {
        this.jsonSchema = jsonSchema;
    }

    public static void setInstance(Schema instance) {
        Schema.instance = instance;
    }

    public ArrayList<Days> getFoodsafety() {
        return foodsafety;
    }

    public void setFoodsafety(ArrayList<Days> foodsafety) {
        this.foodsafety = foodsafety;
    }

    public ArrayList<Days> getChecklist() {
        return checklist;
    }

    public void setChecklist(ArrayList<Days> checklist) {
        this.checklist = checklist;
    }

    public ArrayList<Forms> getForms() {
        return forms;
    }

    public void setForms(ArrayList<Forms> forms) {
        this.forms = forms;
    }

    public ArrayList<Days> getDays() {
        return days;
    }

    public void setDays(ArrayList<Days> days) {
        this.days = days;
    }

    public ArrayList<Actions> getActions() {
        return actions;
    }

    public void setActions(ArrayList<Actions> actions) {
        this.actions = actions;
    }

    public ArrayList<Dayparts> getDayparts() {
        return dayparts;
    }

    public void setDayparts(ArrayList<Dayparts> dayparts) {
        this.dayparts = dayparts;
    }


    public static void getFormsInCurrentDay(Date date){
        SimpleDateFormat format = new SimpleDateFormat("EEE");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));


        ArrayList<String>sections = new ArrayList<>();
        ArrayList<Days> foodsafety = new ArrayList<>();
        ArrayList<Days> checklist = new ArrayList<>();

        for (Days days :Schema.getInstance().getDays()) {

            if (format.format(date).equalsIgnoreCase(days.day)){

                for (Forms form : Schema.getInstance().getForms()){

                    if (form.formName.equalsIgnoreCase(days.formName)){

                        if (days.section.contains("Cleaning Tasks")){

                        }else if (days.section.contains("Operations")){

                        }else if (days.section.contains("_Audit")){

                        }else if (days.section.contains("Checklist")){
                            if (!sections.contains(days.section)) {
                                sections.add(days.section);
                                days.setForms(new ArrayList<Forms>());
                                days.getForms().add(form);
                                checklist.add(days);
                                Schema.getInstance().setChecklist(checklist);
                            } else {
                                for (Days days1 : Schema.getInstance().getChecklist()){
                                    if (days.section.equalsIgnoreCase(days1.section)){
                                        days1.getForms().add(form);
                                    }
                                }
                            }

                        }else {
                            if (!sections.contains(days.section)) {
                                sections.add(days.section);
                                days.setForms(new ArrayList<Forms>());
                                days.getForms().add(form);
                                foodsafety.add(days);
                                System.out.println("DAY FORM: "+form.formName);
                                Schema.getInstance().setFoodsafety(foodsafety);
                            } else {
                                for (Days days1 : Schema.getInstance().getFoodsafety()){
                                    if (days.section.equalsIgnoreCase(days1.section)){
                                        days1.getForms().add(form);
                                        System.out.println("DAY FORM: "+form.formName);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }



}
