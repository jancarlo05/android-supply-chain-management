package com.procuro.androidscm.Restaurant.DashBoard;

import com.procuro.apimmdatamanagerlib.DocumentDTO;

public class TutorialList {

    private String id;
    private String path;
    private String title;
    private String type;
    private DocumentDTO documentDTO;


    public TutorialList(String id, String title, DocumentDTO documentDTO) {
        this.id = id;
        this.title = title;
        this.documentDTO = documentDTO;
    }

    public TutorialList(String id, String path, String title, String type) {
        this.id = id;
        this.path = path;
        this.title = title;
        this.type = type;
    }



    public DocumentDTO getDocumentDTO() {
        return documentDTO;
    }

    public void setDocumentDTO(DocumentDTO documentDTO) {
        this.documentDTO = documentDTO;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
