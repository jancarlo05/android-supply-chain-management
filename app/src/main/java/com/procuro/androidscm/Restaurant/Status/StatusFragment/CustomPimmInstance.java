package com.procuro.androidscm.Restaurant.Status.StatusFragment;

import com.procuro.apimmdatamanagerlib.PimmInstance;

import java.util.ArrayList;

public class CustomPimmInstance {

    private String title;
    private ArrayList<PimmInstance> instances;

    public CustomPimmInstance(String title, ArrayList<PimmInstance> instances) {
        this.title = title;
        this.instances = instances;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<PimmInstance> getInstances() {
        return instances;
    }

    public void setInstances(ArrayList<PimmInstance> instances) {
        this.instances = instances;
    }
}
