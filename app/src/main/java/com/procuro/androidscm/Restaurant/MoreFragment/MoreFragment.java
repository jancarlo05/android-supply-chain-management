package com.procuro.androidscm.Restaurant.MoreFragment;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.procuro.androidscm.Logout_Confirmation_DialogFragment;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourcesChildList;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourcesGrandChildList;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourcesList;
import com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.Resources_left_pane_fragment;
import com.procuro.androidscm.Restaurant.QuickAccessMenu.QuickAccessMenu;
import com.procuro.androidscm.Restaurant.SOS.SOSActivity;
import com.procuro.androidscm.Restaurant.SOS.SOSData;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.apimmdatamanagerlib.DocumentDTO;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Objects;


public class MoreFragment extends Fragment {

    private com.procuro.apimmdatamanagerlib.aPimmDataManager dataManager;
    private ArrayList<ResourcesList> resourcesLists = new ArrayList<>();
    private ArrayList<ResourcesList> TrainingVideos = new ArrayList<>();
    private ProgressDialog progressDialog ;
    private LinearLayout resources_container;

    private Button logout,quick_a_menu,sos,Covid;
    private TextView titleheader;

    public MoreFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.more_fragment, container, false);
        resources_container = view.findViewById(R.id.resources_container);

        titleheader = view.findViewById(R.id.username);
        logout = view.findViewById(R.id.home);
        quick_a_menu = view.findViewById(R.id.quick_a_menu);
        sos  =view.findViewById(R.id.sos);
        Covid = view.findViewById(R.id.covid);

        setupOnclicks();

        DislplayUserName();

        return view;
    }

    private void DislplayUserName(){
        try {

            User user = SCMDataManager.getInstance().getUser();
            titleheader.setText(user.username);

        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private void DisplayResources() {
            getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_left,R.anim.slide_left_out)
                    .replace(R.id.resturant_fragment_container,
                            new Resources_left_pane_fragment(R.id.resource_manual)).commit();
    }


    private void setupOnclicks(){

        Covid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuickAccessMenu.DisplayCovidFragment(getActivity());
            }
        });

        quick_a_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuickAccessMenu.DisplayQuickAMenu(getContext(),quick_a_menu,getActivity());
            }
        });

        resources_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DisplayResources();
            }
        });




        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayLogoutConfirmation();
            }
        });

    }

    private void DisplayLogoutConfirmation() {
        DialogFragment newFragment = Logout_Confirmation_DialogFragment.newInstance();
        assert getFragmentManager() != null;
        newFragment.show(getActivity().getSupportFragmentManager(), "dialog");
    }



    private void DowndloadResources(){

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setTitle("Please Wait");
        progressDialog.setMessage("Downloading Resources");
        progressDialog.setCancelable(false);
        progressDialog.show();

        dataManager = aPimmDataManager.getInstance();
        dataManager.getDocumentListForReferenceId(SCMDataManager.getInstance().getSelectedSite().getSite().CustomerId, new OnCompleteListeners.getDocumentListForReferenceIdCallbackListener() {
            @Override
            public void getDocumentListForReferenceIdCallback(ArrayList<DocumentDTO> documentDTOArrayList, Error error) {
                if (error == null) {
                    FilterCleaningGuides(documentDTOArrayList);
                }
            }
        });
    }

    private void FilterCleaningGuides(final ArrayList<DocumentDTO>documentDTOS){
        ArrayList<ResourcesGrandChildList>backroom = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>CustomerView = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>Production = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>Service = new ArrayList<>();

        for (DocumentDTO dto : documentDTOS){
            if (dto.appString.contains("Backroom_")){
                backroom.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("Customer_View")){
                CustomerView.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("Production")){
                Production.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("Service")){
                Service.add(new ResourcesGrandChildList(dto));
            }
        }

        ArrayList<ResourcesChildList>CleaningGuides = new ArrayList<>();
        CleaningGuides.add(new ResourcesChildList("Backroom",backroom));
        CleaningGuides.add(new ResourcesChildList("Customer View",CustomerView));
        CleaningGuides.add(new ResourcesChildList("Production",Production));
        CleaningGuides.add(new ResourcesChildList("Service",Service));

        resourcesLists.add(new ResourcesList("Cleaning Guides",CleaningGuides));

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {

                FilterFoodSafety(documentDTOS);
            }
        });
    }

    private void FilterFoodSafety(final ArrayList<DocumentDTO>documentDTOS){

        ArrayList<ResourcesGrandChildList>instruction = new ArrayList<>();

        for (DocumentDTO dto : documentDTOS){
            if (dto.category.contains("Resources")){
                instruction.add(new ResourcesGrandChildList(dto));
            }
        }

        Collections.sort(instruction, new Comparator<ResourcesGrandChildList>() {
            @Override
            public int compare(ResourcesGrandChildList o1, ResourcesGrandChildList o2) {
                return o1.getDocumentDTO().name.compareToIgnoreCase(o2.getDocumentDTO().name);
            }
        });


        ArrayList<ResourcesChildList>FoodSafety = new ArrayList<>();
        FoodSafety.add(new ResourcesChildList("Instruction Manuals",instruction));

        resourcesLists.add(new ResourcesList("Food Safety",FoodSafety));


        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                FilterPlaybook(documentDTOS);
            }
        });

    }

    private void FilterPlaybook(final ArrayList<DocumentDTO>documentDTOS){

        ArrayList<ResourcesGrandChildList>introduction = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>taste = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>friendliness = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>speed = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>accuracy = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>cleanliness = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>core = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>welearn = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>instructions = new ArrayList<>();

        for (DocumentDTO dto : documentDTOS){
            if (dto.appString.contains("Introduction")){
                introduction.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("Taste")){
                taste.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("Friendliness")){
                friendliness.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("Speed")){
                speed.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("Accuracy")){
                accuracy.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("Cleanliness")){
                cleanliness.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("Core_Operations")){
                core.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("We_Learn")){
                welearn.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("Instructions_And_Forms")){
                instructions.add(new ResourcesGrandChildList(dto));
            }
        }

        SCMDataManager.getInstance().setDashboardTaste(taste);
        SCMDataManager.getInstance().setDashboardFriendliness(friendliness);
        SCMDataManager.getInstance().setDashboardSpeed(speed);
        SCMDataManager.getInstance().setDashboardAccuracy(accuracy);
        SCMDataManager.getInstance().setDashboardCleanliness(cleanliness);

        ArrayList<ResourcesChildList>PlayBook = new ArrayList<>();
        PlayBook.add(new ResourcesChildList("Introduction",introduction));
        PlayBook.add(new ResourcesChildList("Taste",taste));
        PlayBook.add(new ResourcesChildList("Friendliness",friendliness));
        PlayBook.add(new ResourcesChildList("Speed",speed));
        PlayBook.add(new ResourcesChildList("Accuracy",accuracy));
        PlayBook.add(new ResourcesChildList("Cleanliness",cleanliness));
        PlayBook.add(new ResourcesChildList("Core Operations",core));
        PlayBook.add(new ResourcesChildList("We Learn",welearn));
        PlayBook.add(new ResourcesChildList("Instructions And Forms",instructions));

        resourcesLists.add(new ResourcesList("PlayBook",PlayBook));

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {

                FilterSafetyDataSheets(documentDTOS);
            }
        });
    }

    private void FilterSafetyDataSheets(final ArrayList<DocumentDTO>documentDTOS){

        ArrayList<ResourcesGrandChildList>airCarbonGasses = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>FireExtinguishants = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>CleaningProducts = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>Disinfectants = new ArrayList<>();

        for (DocumentDTO dto : documentDTOS){
            if (dto.appString.contains("Air_Carbon_Gasses")){
                airCarbonGasses.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("Fire_Extinguishants")){
                FireExtinguishants.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("Cleaning_Products")){
                CleaningProducts.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("Disinfectants")){
                Disinfectants.add(new ResourcesGrandChildList(dto));
            }
        }


        Collections.sort(airCarbonGasses, new Comparator<ResourcesGrandChildList>() {
            @Override
            public int compare(ResourcesGrandChildList o1, ResourcesGrandChildList o2) {
                return o1.getDocumentDTO().name.compareToIgnoreCase(o2.getDocumentDTO().name);
            }
        });

        Collections.sort(CleaningProducts, new Comparator<ResourcesGrandChildList>() {
            @Override
            public int compare(ResourcesGrandChildList o1, ResourcesGrandChildList o2) {
                return o1.getDocumentDTO().name.compareToIgnoreCase(o2.getDocumentDTO().name);
            }
        });

        Collections.sort(Disinfectants, new Comparator<ResourcesGrandChildList>() {
            @Override
            public int compare(ResourcesGrandChildList o1, ResourcesGrandChildList o2) {
                return o1.getDocumentDTO().name.compareToIgnoreCase(o2.getDocumentDTO().name);
            }
        });

        ArrayList<ResourcesChildList>SafetyDataSheets = new ArrayList<>();
        SafetyDataSheets.add(new ResourcesChildList("Air Carbon Gasses",airCarbonGasses));
        SafetyDataSheets.add(new ResourcesChildList("Fire Extinguishants",FireExtinguishants));
        SafetyDataSheets.add(new ResourcesChildList("Cleaning Products",CleaningProducts));
        SafetyDataSheets.add(new ResourcesChildList("Disinfectants",Disinfectants));
        resourcesLists.add(new ResourcesList("Safety Data Sheets",SafetyDataSheets));



        for (ResourcesList resourcesList: resourcesLists){
            Log.d("Title", resourcesList.getTitle());
            for (ResourcesChildList childList: resourcesList.getChildLists()){
                Log.d("AppString", childList.getAppString());
                for (ResourcesGrandChildList grandChildList: childList.getGrandChildLists()){
                    Log.d("Name", grandChildList.getDocumentDTO().name);
                }
            }
        }

        SCMDataManager.getInstance().setResourcesLists(resourcesLists);
        FilterByInspectionModule(documentDTOS);
    }

    private void FilterByInspectionModule(final ArrayList<DocumentDTO>documentDTOS){

        ArrayList<ResourcesGrandChildList>Inspection_Module = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>PIMM_DOP_Install = new ArrayList<>();
        ArrayList<ResourcesGrandChildList>PIMM_SMS_Intro = new ArrayList<>();
        for (DocumentDTO dto : documentDTOS){
            if (dto.appString.contains("Inspection_Module")){
                Inspection_Module.add(new ResourcesGrandChildList(dto));
            }else if (dto.appString.contains("PIMM_DOP_Install")){
                PIMM_DOP_Install.add(new ResourcesGrandChildList(dto));

            }else if (dto.appString.contains("PIMM_SMS_Intro")){
                PIMM_SMS_Intro.add(new ResourcesGrandChildList(dto));
            }
        }

        Collections.sort(PIMM_DOP_Install, new Comparator<ResourcesGrandChildList>() {
            @Override
            public int compare(ResourcesGrandChildList o1, ResourcesGrandChildList o2) {
                return o1.getDocumentDTO().name.compareToIgnoreCase(o2.getDocumentDTO().name);
            }
        });

        ArrayList<ResourcesChildList>PIMMSMS = new ArrayList<>();
        PIMMSMS.add(new ResourcesChildList("Inspection Module",Inspection_Module));
        PIMMSMS.add(new ResourcesChildList("PIMM DOP Install",PIMM_DOP_Install));
        PIMMSMS.add(new ResourcesChildList("PIMM SMS Intro",PIMM_SMS_Intro));
        TrainingVideos.add(new ResourcesList("PIMM SMS",PIMMSMS));

        SCMDataManager.getInstance().setTrainingVideos(TrainingVideos);

        progressDialog.dismiss();

    }


}

