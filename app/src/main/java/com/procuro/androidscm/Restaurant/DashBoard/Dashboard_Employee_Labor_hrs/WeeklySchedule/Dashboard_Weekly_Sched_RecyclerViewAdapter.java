package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.WeeklySchedule;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.Dashboard_Sched_Skills_RecyclerViewAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;


public class Dashboard_Weekly_Sched_RecyclerViewAdapter extends RecyclerView.Adapter<Dashboard_Weekly_Sched_RecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<CustomUser> arraylist;
    private ArrayList<Date> dates;
    private Dashboard_Sched_Skills_RecyclerViewAdapter skill_adapter;
    private Dashboard_Weekly_Position_Time_RecyclerViewAdapter start_time_adapter;

    public Dashboard_Weekly_Sched_RecyclerViewAdapter(Context context, ArrayList<CustomUser> arraylist,ArrayList<Date>dates) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;
        this.dates = dates;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.weekly_sched_cardview,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final CustomUser employee = arraylist.get(position);
        SimpleDateFormat format = new SimpleDateFormat("hh:mm a");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        try {
            holder.name.setText(employee.getUser().getFullName());
        }catch (Exception e){
            e.printStackTrace();
        }

        DisplaySKills(employee,holder.skills,holder.message);

        DisplayEmployeePositionAndTime(employee,holder.start_time);

        if (position % 2 == 1) {
            holder.cardView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.light_orange2));
        } else {
            holder.cardView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.white));
        }
    }

    private void DisplayEmployeePositionAndTime(CustomUser employee,RecyclerView start_time){
        try {
            start_time_adapter = new Dashboard_Weekly_Position_Time_RecyclerViewAdapter(mContext,dates,employee);
            start_time.setLayoutManager(new GridLayoutManager(mContext,7));
            start_time.setAdapter(start_time_adapter);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void DisplaySKills(CustomUser employee,RecyclerView skills,TextView message){
        try {
            if (employee.getUser().certificationList !=null){
                if (employee.getUser().certificationList.size()>0){
                    skill_adapter = new Dashboard_Sched_Skills_RecyclerViewAdapter(mContext,employee.getUser().certificationList);
                    skills.setLayoutManager(new GridLayoutManager(mContext,8));
                    skills.setAdapter(skill_adapter);
                    message.setVisibility(View.GONE);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return arraylist.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name,message;
        RecyclerView skills,start_time;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
             name = itemView.findViewById(R.id.name);
             start_time = itemView.findViewById(R.id.start_time);
             skills = itemView.findViewById(R.id.skills);
             cardView = itemView.findViewById(R.id.cardview_id);
             message = itemView.findViewById(R.id.skill_message);

        }
    }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    private TextView  CreatePositionBackground(TextView imageView, String backgroundColor) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setCornerRadii(new float[] { dpToPx(5), dpToPx(5), dpToPx(5), dpToPx(5), 0, 0, 0, 0 });
        shape.setColor(Color.parseColor(backgroundColor));
        shape.setStroke(1, Color.BLACK);
        imageView.setBackground(shape);
        return imageView;
    }

    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(hasStableIds);
    }
}
