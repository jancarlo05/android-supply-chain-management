package com.procuro.androidscm.Restaurant;

import com.procuro.androidscm.Restaurant.Status.StatusFragment.CustomPimmInstance;
import com.procuro.apimmdatamanagerlib.PimmInstance;

import java.util.ArrayList;

public class CustomSensor {
    boolean selected;
    String Title;
    ArrayList<PimmInstance>object;
    ArrayList<CustomPimmInstance> customPimmInstances;


    public CustomSensor(boolean selected, String title, ArrayList<PimmInstance> object) {
        this.selected = selected;
        Title = title;
        this.object = object;
    }


    public CustomSensor(String title, ArrayList<CustomPimmInstance> customPimmInstances) {
        Title = title;
        this.customPimmInstances = customPimmInstances;
    }

    public ArrayList<CustomPimmInstance> getCustomPimmInstances() {
        return customPimmInstances;
    }

    public void setCustomPimmInstances(ArrayList<CustomPimmInstance> customPimmInstances) {
        this.customPimmInstances = customPimmInstances;
    }

    public CustomSensor(String title) {
        Title = title;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public ArrayList<PimmInstance> getObject() {
        return object;
    }

    public void setObject(ArrayList<PimmInstance> object) {
        this.object = object;
    }
}
