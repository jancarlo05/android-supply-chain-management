package com.procuro.androidscm.Restaurant.Status.Map;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.CustomSensor;
import com.procuro.androidscm.Restaurant.Qualification.Map.Map_Fragment;
import com.procuro.androidscm.Restaurant.Qualification.Map.SampleClusterItem;
import com.procuro.androidscm.Restaurant.Qualification.QualificationActivity;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Area;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_District;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Division;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.Corp_Regional;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.Restaurant.Status.StatusActivity;
import com.procuro.androidscm.Restaurant.Status.StatusActivityFragment;
import com.procuro.androidscm.Restaurant.Status.StatusData;
import com.procuro.androidscm.Restaurant.Status.StatusFragment.CustomPimmInstance;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.PimmInstance;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import static android.content.ContentValues.TAG;


public class Status_Map_Fragment extends Fragment implements OnMapReadyCallback,
        ClusterManager.OnClusterClickListener<SampleClusterItem>,
        ClusterManager.OnClusterInfoWindowClickListener<SampleClusterItem>,
        ClusterManager.OnClusterItemClickListener<SampleClusterItem>,
        ClusterManager.OnClusterItemInfoWindowClickListener<SampleClusterItem>{

    public static GoogleMap mMap;
    private static final int REQUEST_LOCATION_PERMISSION = 1;
    public static Marker marker;
    public static ArrayList<LatLng> markers = new ArrayList<LatLng>();
    public static LatLngBounds.Builder builder = new LatLngBounds.Builder();


    int ZoomControl_id = 0x1;
    int MyLocation_button_id = 0x2;
    private SupportMapFragment mapFragment;
    private SampleClusterItem clickedClusterItem;
    private Button change_map;
    private List<SampleClusterItem> clusterItems = new ArrayList<>();

    private Corp_Regional regional;
    private Corp_Division division;
    private Corp_District district;
    private Corp_Area area;
    private CustomSite customSite;
    View rootView;
    com.procuro.apimmdatamanagerlib.aPimmDataManager dataManager;
    public static boolean isStore;

    public Status_Map_Fragment() {

    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
            rootView = inflater.inflate(R.layout.activity_maps, container,false);

            mapFragment = (SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.map);
            Objects.requireNonNull(mapFragment).getMapAsync(Status_Map_Fragment.this);




        return rootView;

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setCompassEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        getarguments();


    }

    private int checkSelfPermission(String accessFineLocation) {
        return 0;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.v("LOGIN", "ACCESS_FINE_LOCATION is now granted");
                }
                return;
            }
        }
    }


    public void create_marckers(){

        for (CustomSite customSite : SCMDataManager.getInstance().getCustomSites()){
            System.out.println(customSite.getSite().LatLon.Lat);
            System.out.println(customSite.getSite().LatLon.Lon);
            int map_icon = 0;
            LatLng latLng = new LatLng(customSite.getSite().LatLon.Lat,customSite.getSite().LatLon.Lon);
            map_icon = R.drawable.network_map;
            marker = mMap.addMarker(new MarkerOptions()
            .position(latLng)
            .title(customSite.getSite().sitename)
            .icon(BitmapDescriptorFactory.fromResource(map_icon)));{
            }
        }

        for (int j = 0; j < markers.size(); j++) {
            builder.include(markers.get(j));
        }

}

    @Override
    public boolean onClusterClick(Cluster<SampleClusterItem> cluster) {
        return false;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<SampleClusterItem> cluster) {

    }

    @Override
    public boolean onClusterItemClick(SampleClusterItem sampleClusterItem) {
        return false;
    }

    @Override
    public void onClusterItemInfoWindowClick(SampleClusterItem sampleClusterItem) {


    }

    class OwnIconRendered extends DefaultClusterRenderer<SampleClusterItem> {

        private ArrayList<Integer>severities = new ArrayList<>();
        private ArrayList<String> os = new ArrayList<>();
        int color = Color.parseColor("#9AC9E9");


        @Override
        public Cluster<SampleClusterItem> getCluster(Marker marker) {
            return super.getCluster(marker);
        }
        @Override
        protected int getBucket(Cluster<SampleClusterItem> cluster) {


            return cluster.getSize();
        }

        @Override
        protected void onClusterItemRendered(SampleClusterItem clusterItem, Marker marker) {
            super.onClusterItemRendered(clusterItem, marker);
            if (isStore){
                marker.showInfoWindow();
            }
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster<SampleClusterItem> cluster) {
            severities = new ArrayList<>();
            os = new ArrayList<>();

            for (SampleClusterItem item : cluster.getItems()){
                CustomSite customSite = item.getCustomSite();
                if (customSite.getSite()!=null){
                    if (customSite.getSite().os.equalsIgnoreCase("Custom")){
                        os.add("Custom");
                    }else {
                        if (!severities.contains(customSite.getSite().AlarmSeverity)){
                            severities.add(customSite.getSite().AlarmSeverity);
                        }
                    }
                }
            }

            System.out.println("os: "+os);
            System.out.println("severity: "+severities);
            Collections.sort(severities,Collections.<Integer>reverseOrder());

            if (os.contains("Custom")){
                color = Color.parseColor("#9AC9E9");
            }else {
                if (severities.get(0) == 10){
                    color = Color.parseColor("#515151");

                }else if (severities.get(0) == 9){

                    color = Color.parseColor("#D91E25");

                }else if (severities.get(0) == 5){
                    color = Color.parseColor("#ED8030");


                }else if (severities.get(0) == 3){
                    color = Color.parseColor("#337D34");
                }
            }

            getColor(color);

            return cluster.getSize() >= 2;
        }

        @Override
        protected String getClusterText(int bucket) {


            return super.getClusterText(bucket).replace("+", "");
        }
        @Override
        protected int getColor(int clustercolor) {

            clustercolor = color;

            return clustercolor;
        }

        public OwnIconRendered(Context context, GoogleMap map,
                               com.google.maps.android.clustering.ClusterManager<SampleClusterItem> clusterManager) {
            super(context, map, clusterManager);

        }

        @Override
        protected void onBeforeClusterItemRendered(SampleClusterItem item, MarkerOptions markerOptions) {
            markerOptions.icon(item.getIcon());
            markerOptions.snippet(item.getSnippet());
            markerOptions.title(item.getTitle());

            super.onBeforeClusterItemRendered(item, markerOptions);

        }

        @Override
        protected void onBeforeClusterRendered(Cluster<SampleClusterItem> cluster, MarkerOptions markerOptions) {
            super.onBeforeClusterRendered(cluster, markerOptions);


//            severities = new ArrayList<>();
//            os = new ArrayList<>();
//
//            for (SampleClusterItem item : cluster.getItems()){
//                CustomSite customSite = item.getCustomSite();
//                if (customSite.getPimmDevice()!=null){
//                    if (customSite.getPimmDevice().os.equalsIgnoreCase("Custom")){
//                        os.add("Custom");
//                    }else {
//                        if (!severities.contains(customSite.getPimmDevice().severity)){
//                            severities.add(customSite.getPimmDevice().severity);
//                        }
//                    }
//
//                }
//            }
//
//
//            System.out.println("os: "+os);
//            System.out.println("severity: "+severities);
//            Collections.sort(severities,Collections.<Integer>reverseOrder());
//
//
//
//            int color = Color.parseColor("#9AC9E9");
//
//            if (os.contains("Custom")){
//                color = Color.parseColor("#9AC9E9");
//
//            }else {
//
//                if (severities.get(0) == 10){
//                    color = Color.parseColor("#515151");
//
//                }else if (severities.get(0) == 9){
//
//                    color = Color.parseColor("#D91E25");
//
//                }else if (severities.get(0) == 5){
//                    color = Color.parseColor("#ED8030");
//
//
//                }else if (severities.get(0) == 3){
//                    color = Color.parseColor("#337D34");
//                }
//            }
//
//
//
//
//            int cluster_size = cluster.getSize();
//
//            int size = dpToPx(10);
//            int box_size = dpToPx(50);
//
//            if (cluster_size<10){
//                size = dpToPx(7);
//                box_size = dpToPx(55);
//            }
//            else if (cluster_size <50){
//                size = dpToPx(7);
//                box_size = dpToPx(55);
//            }else if (cluster_size <1000){
//                size = dpToPx(6);
//                box_size = dpToPx(60);
//
//            }else {
//                size = dpToPx(5);
//                box_size = dpToPx(60);
//
//            }
//
//
//
//            Button textView = new Button(getContext());
//            textView.setId(View.generateViewId());
//            textView.setTextSize(size);
//            textView.setGravity(Gravity.CENTER |Gravity.CENTER_HORIZONTAL);
//            textView.setText(String.valueOf(cluster_size));
//            textView.setLayoutParams(new LinearLayout.LayoutParams(box_size,box_size));
//
//            GradientDrawable shape = new GradientDrawable();
//            shape.setShape(GradientDrawable.OVAL);
//            shape.setColor(color);
//            shape.setStroke(5, ContextCompat.getColor(getContext(),R.color.white));
//            textView.setBackground(shape);
//
//            Bitmap bitmap = Bitmap.createBitmap(box_size,box_size,
//                    Bitmap.Config.ARGB_8888);
//            Canvas canvas = new Canvas(bitmap);
//
//            textView.layout(0, 0, box_size, box_size);
//            textView.setGravity(Gravity.CENTER);
//            textView.draw(canvas);
//            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bitmap));
        }
    }



    public class MyCustomAdapterForItems implements GoogleMap.InfoWindowAdapter {
        private final View myContentsView;
        private TextView name;
        private TextView address;
        private ArrayList<CustomSite>customSites;


        MyCustomAdapterForItems(ArrayList<CustomSite>customSites) {
            myContentsView = getLayoutInflater().inflate(
                    R.layout.qualification_map_layout, null);

            this.customSites = customSites;

            name = myContentsView.findViewById(R.id.name);
            address = myContentsView.findViewById(R.id.address);

        }
        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }
        @Override
        public View getInfoContents(final Marker marker) {

            for (final CustomSite customSite :customSites){
                if (marker.getTitle().equalsIgnoreCase(customSite.getSite().sitename)){
                    name.setText(marker.getTitle());
                    address.setText(SCMTool.getCompleteAddress(customSite.getSite()));

                    myContentsView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(getContext(), ""+marker.getTitle(), Toast.LENGTH_SHORT).show();

                        }
                    });


                    mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                        @Override
                        public void onInfoWindowClick(Marker marker) {
                            if (StatusData.getInstance().isAllstore()){
                                StatusData.getInstance().setSelectedItem(customSite.getSite().sitename);
                                StatusData.getInstance().setSelectedSite(customSite);
                                StatusActivityFragment.scoreCard.setChecked(true);
                                StatusActivityFragment.scoreCard.setEnabled(true);
                                StatusActivityFragment.scoreCard.setAlpha(1);
                                StatusActivityFragment.chart.setEnabled(true);
                                StatusActivityFragment.chart.setAlpha(1);
                                StatusActivityFragment.name.setText(customSite.getSite().sitename);

                            }else {
                                StatusData.getInstance().setSelectedItem(customSite.getSite().sitename);
                                StatusData.getInstance().setSelectedSite(customSite);
                                StatusActivityFragment.scoreCard.setChecked(true);
                                StatusActivityFragment.scoreCard.setEnabled(true);
                                StatusActivityFragment.scoreCard.setAlpha(1);
                                StatusActivityFragment.chart.setEnabled(true);
                                StatusActivityFragment.chart.setAlpha(1);
                            }

                        }
                    });

                    break;
                }
            }
            return myContentsView;
        }
    }

    private void getarguments(){

        if (StatusData.getInstance().isAllstore()){
            StatusActivityFragment.name.setText("");
            StatusActivityFragment.chart.setAlpha(.5f);
            StatusActivityFragment.chart.setEnabled(false);
            StatusActivityFragment.storeOptions.setAlpha(.5f);
            StatusActivityFragment.scoreCard.setAlpha(.5f);
            StatusActivityFragment.storeOptions.setEnabled(false);
            StatusActivityFragment.scoreCard.setEnabled(false);

            ShowAllstoreMarker(SCMDataManager.getInstance().getCustomSites());

        }else if (StatusData.getInstance().isOwnerShipViewEnabled()) {
            StatusActivityFragment.chart.setEnabled(false);
            StatusActivityFragment.chart.setAlpha(.5f);
            Corp_Regional regional = StatusData.getInstance().getRegional();

            if (regional!=null){

                if (regional.getCorpStructures().name.equalsIgnoreCase(StatusData.getInstance().getSelectedItem())){

                    this.regional = regional;
                    ShowRegionLevelMarker(this.regional);
                    StatusActivityFragment.name.setText(this.regional.getCorpStructures().name);

                }else {

                    if (regional.getQualificationDivisions()!=null){

                        for (Corp_Division division : regional.getQualificationDivisions()){

                            if (division.getCorpStructures().name.equalsIgnoreCase(StatusData.getInstance().getSelectedItem())){
                                this.division = division;
                                ShowDivisionLevelMarker(this.division);
                                StatusActivityFragment.name.setText(this.division.getCorpStructures().name);
                                break;

                            }else {
                                if (division.getQualificationAreas()!=null){

                                    for (Corp_Area area : division.getQualificationAreas()){

                                        if (area.getCorpStructures().name.equalsIgnoreCase(StatusData.getInstance().getSelectedItem())){

                                            this.area = area;
                                            ShowAreaLevelMarker(this.area);
                                            StatusActivityFragment.name.setText(this.area.getCorpStructures().name);
                                            break;

                                        }else {

                                            if (area.getQualificationDistricts()!=null){

                                                for (Corp_District district : area.getQualificationDistricts()){

                                                    if (district.getCorpStructures().name.equalsIgnoreCase(StatusData.getInstance().getSelectedItem())){

                                                        this.district = district;
                                                        ShowDistrictLevelMarker(this.district);
                                                        StatusActivityFragment.name.setText(this.district.getCorpStructures().name);

                                                        break;

                                                    }else {
                                                        if (district.getSites()!=null){

                                                            for (CustomSite customSite : district.getSites()){

                                                             if (customSite.getSite().sitename.equalsIgnoreCase(StatusData.getInstance().getSelectedItem())){
                                                                 this.customSite = customSite;
                                                                 StatusActivityFragment.chart.setEnabled(true);
                                                                 StatusActivityFragment.chart.setAlpha(1);
                                                                 ShowsiteLevelMarker(this.customSite);
                                                                 StatusActivityFragment.scoreCard.setEnabled(true);
                                                                 StatusActivityFragment.scoreCard.setAlpha(1);
                                                                 SensorCategoryView(this.customSite);
                                                                 StatusActivityFragment.name.setText(this.customSite.getSite().sitename);

                                                                 break;
                                                             }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else if (StatusData.getInstance().isAllStoreEnabled()){
            if(StatusData.getInstance().getSelectedSite()!=null){
                this.customSite = StatusData.getInstance().getSelectedSite();
                ShowsiteLevelMarker(this.customSite);
                StatusActivityFragment.name.setText(this.customSite.getSite().sitename);
                StatusActivityFragment.scoreCard.setEnabled(true);
                StatusActivityFragment.scoreCard.setAlpha(1);
                SensorCategoryView(this.customSite);
            }
        }
        else if (StatusData.getInstance().isStateByStateViewEnabled()){
            if(StatusData.getInstance().getSelectedSite()!=null){
                this.customSite = StatusData.getInstance().getSelectedSite();
                ShowsiteLevelMarker(this.customSite);
                StatusActivityFragment.name.setText(this.customSite.getSite().sitename);
                StatusActivityFragment.scoreCard.setEnabled(true);
                StatusActivityFragment.scoreCard.setAlpha(1);
                SensorCategoryView(this.customSite);
            }
        }
        else {
            this.customSite = SCMDataManager.getInstance().getSelectedSite();
            ShowsiteLevelMarker(this.customSite);
            StatusActivityFragment.name.setText(this.customSite.getSite().sitename);
            StatusActivityFragment.scoreCard.setEnabled(true);
            StatusActivityFragment.scoreCard.setAlpha(1);
            SensorCategoryView(this.customSite);
        }

    }

    public  void ShowAllstoreMarker(ArrayList<CustomSite>customSites) {
        try {
            final ArrayList<LatLng>markers = new ArrayList<>();
            ArrayList<CustomSite>sites = new ArrayList<>();

            for (CustomSite customSite :customSites){
                if (customSite.isMerged()){
                    LatLng latLng = new LatLng(customSite.getSite().LatLon.Lat,customSite.getSite().LatLon.Lon);
                    markers.add(latLng);
                    sites.add(customSite);
                }
            }
            cluster_function(sites);
            SCMTool.moveToSelectedMarkers(requireContext(),mMap,markers);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public  void ShowRegionLevelMarker(Corp_Regional region) {
        try {
            final ArrayList<LatLng>markers = new ArrayList<>();
            ArrayList<CustomSite>sites = new ArrayList<>();
            ArrayList<Corp_Division>divisions = region.getQualificationDivisions();
            if (divisions!=null){
                for (Corp_Division division : divisions){
                    if (division.getQualificationAreas()!=null){
                        for (Corp_Area area : division.getQualificationAreas()){
                            if (area.getQualificationDistricts()!=null){
                                for (Corp_District district : area.getQualificationDistricts()){
                                    if (district.getSites()!=null){
                                        for (CustomSite customSite : district.getSites()){
                                            LatLng latLng = new LatLng(customSite.getSite().LatLon.Lat,customSite.getSite().LatLon.Lon);
                                            markers.add(latLng);
                                            sites.add(customSite);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            cluster_function(sites);
            SCMTool.moveToSelectedMarkers(requireContext(),mMap,markers);

        }catch (Exception e){
            e.printStackTrace();

        }




    }

    public  void ShowDivisionLevelMarker(Corp_Division division) {

        final ArrayList<LatLng>markers = new ArrayList<>();
        ArrayList<CustomSite>sites = new ArrayList<>();

        for (Corp_Area area : division.getQualificationAreas()){
            if (area.getQualificationDistricts()!=null){
                for (Corp_District district : area.getQualificationDistricts()){
                    if (district.getSites()!=null){
                        for (CustomSite customSite : district.getSites()){
                            LatLng latLng = new LatLng(customSite.getSite().LatLon.Lat,customSite.getSite().LatLon.Lon);
                            markers.add(latLng);
                            sites.add(customSite);
                        }
                    }
                }
            }
        }

        try {

            cluster_function(sites);
            SCMTool.moveToSelectedMarkers(requireContext(),mMap,markers);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public  void ShowDistrictLevelMarker(Corp_District district) {
        try {
            final ArrayList<LatLng>markers = new ArrayList<>();
            ArrayList<CustomSite>sites = new ArrayList<>();

            if (district.getSites()!=null){
                for (CustomSite customSite : district.getSites()){
                    LatLng latLng = new LatLng(customSite.getSite().LatLon.Lat,customSite.getSite().LatLon.Lon);
                    markers.add(latLng);
                    sites.add(customSite);
                }
            }

            cluster_function(sites);
            SCMTool.moveToSelectedMarkers(requireContext(),mMap,markers);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public  void ShowAreaLevelMarker(Corp_Area area) {
        try {
            final ArrayList<LatLng>markers = new ArrayList<>();
            ArrayList<CustomSite>sites = new ArrayList<>();

            for (Corp_District district : area.getQualificationDistricts()){
                if (district.getSites()!=null){
                    for (CustomSite customSite : district.getSites()){
                        LatLng latLng = new LatLng(customSite.getSite().LatLon.Lat,customSite.getSite().LatLon.Lon);
                        markers.add(latLng);
                        sites.add(customSite);
                    }
                }
            }

            cluster_function(sites);
            SCMTool.moveToSelectedMarkers(requireContext(),mMap,markers);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public  void ShowsiteLevelMarker(final CustomSite customSite) {
        try {

            ArrayList<CustomSite>customSites = new ArrayList<>();
            ArrayList<LatLng>markers = new ArrayList<>();

            LatLng latLng = new LatLng(customSite.getSite().LatLon.Lat,customSite.getSite().LatLon.Lon);
            markers.add(latLng);
            customSites.add(customSite);
            final float width = (int)(getResources().getDisplayMetrics().widthPixels*0.017);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, width));
            isStore = true;
            cluster_function(customSites);


        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public  void ClusterClick(ArrayList<SampleClusterItem> clusterItems) {
        try {
            final ArrayList<LatLng>markers = new ArrayList<>();
            for (SampleClusterItem clusterItem : clusterItems){
                CustomSite customSite = clusterItem.getCustomSite();
                LatLng latLng = new LatLng(customSite.getSite().LatLon.Lat,customSite.getSite().LatLon.Lon);
                markers.add(latLng);

            }

            SCMTool.moveToSelectedMarkers(requireContext(),mMap,markers);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void cluster_function(ArrayList<CustomSite>customSites){
        try {
            final com.google.maps.android.clustering.ClusterManager<SampleClusterItem> clusterManager = new com.google.maps.android.clustering.ClusterManager<>(requireContext(), mMap);
            clusterManager.setRenderer(new Status_Map_Fragment.OwnIconRendered(getContext(), mMap, clusterManager));
            mMap.setOnCameraIdleListener(clusterManager);

            if (customSites!=null){
                for (CustomSite customSite : customSites){
                    LatLng latLng = new LatLng(customSite.getSite().LatLon.Lat,customSite.getSite().LatLon.Lon);
                    BitmapDescriptor icon = null;

                    int height = SCMTool.dpToPx(65);
                    int width = SCMTool.dpToPx(45);

                    BitmapDrawable bitmapdraw= null;

                    if (customSite.getSite()!=null){
                        if (customSite.getSite().os.equalsIgnoreCase("Custom")){
                            icon = SCMTool.getMapIcon(R.drawable.map_store_light_blue,getContext());
                        }
                        else {
                            if (customSite.getSite().AlarmSeverity == 0 ){
                                icon = SCMTool.getMapIcon(R.drawable.map_store_blue,getContext());

                            }else if (customSite.getSite().AlarmSeverity == 3 ){
                                icon = SCMTool.getMapIcon(R.drawable.map_store_green,getContext());

                            }else if (customSite.getSite().AlarmSeverity == 5 ){
                                icon = SCMTool.getMapIcon(R.drawable.map_store_yellow,getContext());

                            }else if (customSite.getSite().AlarmSeverity == 9 ){
                                icon = SCMTool.getMapIcon(R.drawable.map_store_red,getContext());

                            }else {
                                icon = SCMTool.getMapIcon(R.drawable.map_store_gray,getContext());

                            }
                        }
                    }else {
                        icon = SCMTool.getMapIcon(R.drawable.map_store_gray,getContext());

                    }
                    clusterItems.add(new SampleClusterItem(latLng, customSite.getSite().sitename,null,icon,customSite));

                }

                clusterManager.addItems(clusterItems);
                mMap.setOnInfoWindowClickListener(clusterManager);
                mMap.setInfoWindowAdapter(clusterManager.getMarkerManager());
                mMap.setOnMarkerClickListener(clusterManager);
                clusterManager.getMarkerCollection().setInfoWindowAdapter(new Status_Map_Fragment.MyCustomAdapterForItems(SCMDataManager.getInstance().getCustomSites()));
                clusterManager.setOnClusterClickListener(this);
                clusterManager.setOnClusterInfoWindowClickListener(this);
                clusterManager.setOnClusterItemClickListener(this);
                clusterManager.setOnClusterItemInfoWindowClickListener(this);

                clusterManager
                        .setOnClusterItemClickListener(new com.google.maps.android.clustering.ClusterManager.OnClusterItemClickListener<SampleClusterItem>() {
                            @Override
                            public boolean onClusterItemClick(SampleClusterItem item) {
                                clickedClusterItem = item;
                                return false;
                            }
                        });
                clusterManager.cluster();

                clusterManager.setOnClusterClickListener(new com.google.maps.android.clustering.ClusterManager.OnClusterClickListener<SampleClusterItem>() {

                    @Override
                    public boolean onClusterClick(com.google.maps.android.clustering.Cluster<SampleClusterItem> cluster) {
                        ClusterClick((ArrayList<SampleClusterItem>) cluster.getItems());

                        return false;
                    }
                });
                clusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<SampleClusterItem>() {
                    @Override
                    public boolean onClusterItemClick(SampleClusterItem sampleClusterItem) {
                        Log.d(TAG, "onItemClusterClick");
                        return false;
                    }
                });
            }


        }catch (Exception e){
            e.printStackTrace();
        }


    }

    private void SensorCategoryView(final CustomSite site){

        dataManager = aPimmDataManager.getInstance();
        if (site.getPimmDevice()!=null) {
            dataManager.getInstanceListForSiteId(site.getPimmDevice().deviceID, new OnCompleteListeners.getInstanceListForSiteIdCallbackListener() {
                @Override
                public void getInstanceListForSiteIdCallback(ArrayList<PimmInstance> pimmInstanceArrayList, Error error) {
                    if (error == null) {
                        if (pimmInstanceArrayList.size() > 0) {
                            FilterObjectNames(pimmInstanceArrayList,site);
                        }else {
                            SCMTool.DisableView(StatusActivityFragment.chart,.5f);
                            StatusData.getInstance().setSelectedSite(site);
                            StatusData.getInstance().setPimmDevice(site.getPimmDevice());
                        }
                    }else {
                        SCMTool.DisableView(StatusActivityFragment.map,.5f);
                        StatusData.getInstance().setSelectedSite(site);
                        StatusData.getInstance().setPimmDevice(site.getPimmDevice());
                    }
                }
            });
        }
    }

    private void FilterObjectNames(ArrayList<PimmInstance> temperatureLists,CustomSite customSite){
        ArrayList<String>objectname = new ArrayList<>();
        ArrayList<CustomSensor>objects=  new ArrayList<>();
        for (PimmInstance newObjects: temperatureLists){
            if (!objectname.contains(newObjects.objectName)){
                objectname.add(newObjects.objectName);
                objects.add(new CustomSensor(newObjects.objectName));
                System.out.println("Description: "+newObjects.description);
            }
        }

        PopulateInstanceByObjectName(temperatureLists,objects,customSite);
    }

    private void PopulateInstanceByObjectName(ArrayList<PimmInstance>pimmInstances,
                                              ArrayList<CustomSensor>newObjects,CustomSite customSite){
        for (PimmInstance instance: pimmInstances){
            for (CustomSensor newObject: newObjects){
                if (instance.objectName.equalsIgnoreCase(newObject.getTitle())){
                    if (newObject.getObject() == null){
                        ArrayList<PimmInstance>newObs = new ArrayList<>();
                        newObs.add(instance);
                        newObject.setObject(newObs);
                    }else {
                        newObject.getObject().add(instance);
                    }
                }
            }
        }
        PopulateInstanceByCoolsersAndFreezers(newObjects,customSite);
    }

    private void PopulateInstanceByCoolsersAndFreezers(ArrayList<CustomSensor>sensors,CustomSite customSite){

        for (CustomSensor sensor : sensors){
            if (sensor.getTitle().equalsIgnoreCase("Temperature")) {

                ArrayList<CustomPimmInstance> customPimmInstances = new ArrayList<>();
                ArrayList<String> titles = new ArrayList<>();
                if (sensor.getObject() != null) {
                    for (PimmInstance instance : sensor.getObject()) {
                        if (instance.description.toLowerCase().contains("cooler")) {
                            if (!titles.contains("Coolers")) {
                                titles.add("Coolers");
                                ArrayList<PimmInstance> instances = new ArrayList<>();
                                instances.add(instance);
                                customPimmInstances.add(new CustomPimmInstance("Coolers", instances));

                            } else {
                                for (CustomPimmInstance pimmInstance : customPimmInstances) {
                                    if (pimmInstance.getTitle().equalsIgnoreCase("Coolers")) {
                                        pimmInstance.getInstances().add(instance);
                                    }
                                }
                            }
                        } else if (instance.description.toLowerCase().contains("freezer")) {

                            if (!titles.contains("Freezers")) {
                                titles.add("Freezers");
                                ArrayList<PimmInstance> instances = new ArrayList<>();
                                instances.add(instance);
                                customPimmInstances.add(new CustomPimmInstance("Freezers", instances));
                                System.out.println("Freezer: "+instance.description);

                            } else {
                                for (CustomPimmInstance pimmInstance : customPimmInstances) {
                                    if (pimmInstance.getTitle().equalsIgnoreCase("Freezers")) {
                                        pimmInstance.getInstances().add(instance);
                                        System.out.println("Freezer: "+instance.description);
                                    }
                                }
                            }
                        }
                    }
                }


                Collections.sort(customPimmInstances, new Comparator<CustomPimmInstance>() {
                    @Override
                    public int compare(CustomPimmInstance o1, CustomPimmInstance o2) {
                        return o1.getTitle().compareToIgnoreCase(o2.getTitle());
                    }
                });

                for (CustomPimmInstance pimmInstance : customPimmInstances){
                    getTemperatureLevel(pimmInstance,sensors,customSite);
                    break;
                }
            }
        }
    }

    private void getTemperatureLevel(final CustomPimmInstance pimmInstance, ArrayList<CustomSensor>customSensors,CustomSite customSite){

        if (pimmInstance.getInstances()!=null){
            for (PimmInstance sensors :  pimmInstance.getInstances()){
                setInstanceForTemperature(customSite,sensors);
                System.out.println("sensors: "+sensors.description);
                break;
            }
        }
    }

    private void setInstanceForTemperature(CustomSite site, PimmInstance pimmInstance){
        StatusData.getInstance().setSelectedSite(site);
        StatusData.getInstance().setPimmDevice(site.getPimmDevice());
        StatusData.getInstance().setPimmInstance(pimmInstance);
    }
}