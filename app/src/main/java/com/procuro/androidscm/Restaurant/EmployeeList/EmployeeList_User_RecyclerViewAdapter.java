package com.procuro.androidscm.Restaurant.EmployeeList;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_Employee_Labor_hrs.Dashboard_Sched_Skills_RecyclerViewAdapter;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.UserProfile.CustomRole;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileActivity;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileActivityOverAll;
import com.procuro.androidscm.Restaurant.UserProfile.UserProfileData;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.apimmdatamanagerlib.Certification;
import com.procuro.apimmdatamanagerlib.CertificationDefinition;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import static com.procuro.androidscm.Restaurant.EmployeeList.EmployeeListActivity.Employee_Schedule_Report;
import static com.procuro.androidscm.Restaurant.EmployeeList.EmployeeListActivity.Employee_Schedule_Summary;
import static com.procuro.androidscm.Restaurant.EmployeeList.EmployeeListActivity.setUpEmployeeReport;


public class EmployeeList_User_RecyclerViewAdapter extends RecyclerView.Adapter<EmployeeList_User_RecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<CustomUser> arraylist;
    private FragmentActivity fragmentActivity;
    private CustomSite customSite;
    private Dashboard_Sched_Skills_RecyclerViewAdapter adapter;
    private ArrayList<String>positionSKills = new ArrayList<>();

    public EmployeeList_User_RecyclerViewAdapter(Context context, ArrayList<CustomUser> arraylist,
                                                 FragmentActivity fragmentActivity, CustomSite customSite) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = arraylist;
        this.fragmentActivity = fragmentActivity;
        this.customSite = customSite;
        positionSKills = getPositionSkills();

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.employeelist_employee_cardview,parent,false);

        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position, @NonNull List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final CustomUser user = arraylist.get(position);

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        holder.name.setText(user.getUser().firstName);
        holder.name.append(" ");
        holder.name.append(user.getUser().lastName);


        if (user.getUser().username!=null){
            if (!user.getUser().username.equalsIgnoreCase("null")){
                holder.username.setText(user.getUser().username);
            }
        }
        if (user.getUser().hireDate!=null){

            holder.startdate.setText(dateFormat.format(user.getUser().hireDate));
        }

        if (user.getUser().DOB!=null){

            holder.birthday.setText(dateFormat.format(user.getUser().DOB));
        }

        if (user.getUser().reviewDate!=null){
            holder.review_date.setText(dateFormat.format(user.getUser().reviewDate));
        }

        if (user.getUser().certificationList !=null){
            if (user.getUser().certificationList.size()>0){

                ArrayList<Certification>FilteredPositions = new ArrayList<>();
                for (Certification certification : user.getUser().certificationList){
                    if (positionSKills.contains(certification.Certification)){
                        FilteredPositions.add(certification);
                    }
                }
                if (FilteredPositions.size()>0){
                    adapter = new Dashboard_Sched_Skills_RecyclerViewAdapter(mContext,FilteredPositions);
                    holder.skills.setLayoutManager(new GridLayoutManager(mContext,8));
                    holder.skills.setAdapter(adapter);
                    holder.message.setVisibility(View.GONE);
                    holder.skills.setVisibility(View.VISIBLE);
                }
            }else {
                holder.skills.setVisibility(View.INVISIBLE);
                holder.message.setVisibility(View.VISIBLE);
            }
        }
        else {
            holder.message.setVisibility(View.VISIBLE);
            holder.skills.setVisibility(View.INVISIBLE);
        }

        if (user.getUser().roles!=null){
            ArrayList<CustomRole>customRoles = CustomRole.getAllCustomRole(user.getUser().roles);
            if (customRoles.size()>=1){
                holder.permission.setText(customRoles.get(0).getName());
            }
        }


        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(fragmentActivity, UserProfileActivityOverAll.class);
                UserProfileData data = new UserProfileData();
                data.setUserEditEnabled(false);
                data.setCustomSite(customSite);
                data.setCustomUser(user);
                data.setFromQualification(true);
                data.setIsupdate(true);
                data.setPrevActivity("EmployeeList");
                UserProfileData.setInstance(data);
                fragmentActivity.startActivity(intent);
            }
        });

        if (position % 2 == 1) {
            holder.cardView.setBackgroundColor(Color.parseColor("#E6E6EA"));
        } else {
            holder.cardView.setBackgroundColor(ContextCompat.getColor(mContext,R.color.white));
        }

        if (!user.getUser().active){
            holder.cardView.setAlpha(.5F);
        }

        setUpEmployeeReport(user,holder.employee_report,Employee_Schedule_Report,fragmentActivity);

    }

    @Override
    public int getItemCount() {
        return arraylist.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name,username,permission,startdate,message,birthday,review_date;
        RecyclerView skills;
        CardView cardView;
        ImageView employee_report;

        public MyViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            username = itemView.findViewById(R.id.username);
            permission = itemView.findViewById(R.id.permision);
            startdate = itemView.findViewById(R.id.start_date);
            message = itemView.findViewById(R.id.message);
            birthday = itemView.findViewById(R.id.birtdate);
            review_date = itemView.findViewById(R.id.review_date);
            skills = itemView.findViewById(R.id.skills);
            cardView = itemView.findViewById(R.id.cardview_id);
            employee_report = itemView.findViewById(R.id.employee_report);
        }
    }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    private ArrayList<String>getPositionSkills(){
        ArrayList<String>skills = new ArrayList<>();
        ArrayList<CertificationDefinition>certifications = SCMDataManager.getInstance().getCertificationDefinitions();
        if (certifications!=null){
            for (CertificationDefinition certification : certifications){
                if (certification.category.equalsIgnoreCase("0")){
                    skills.add(certification.certification);
                }
            }
        }
        return skills;
    }


}
