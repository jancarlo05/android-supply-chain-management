package com.procuro.androidscm.Restaurant.PimmyNews;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;

import com.procuro.androidscm.R;

import java.util.Objects;

public class Covid_DialogFragment extends DialogFragment {

    RadioButton dcd,cny;
    private Button close ;
    private ProgressBar progressBar;
    private TextView date , wait , title;
    private WebView webView;
    info.hoang8f.android.segmented.SegmentedGroup infogroup;
    ConstraintLayout loading_container;

    public static Covid_DialogFragment newInstance() {
        return new Covid_DialogFragment();
    }
    @Override
        public int getTheme() {
        return R.style.slide_down_up;
        }

    @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.covid_fragment, container, false);

//                    int height = (int)(getResources().getDisplayMetrics().heightPixels*0.90);
//                    int width = 320;
//
        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.99);
        int height = (int)(getResources().getDisplayMetrics().heightPixels*.90);


        Objects.requireNonNull(getDialog().getWindow()).setLayout(width, height);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.slide_down_up;
        view.setFocusableInTouchMode(true);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        webView = view.findViewById(R.id.webview);
        close = view.findViewById(R.id.back_button);
        loading_container = view.findViewById(R.id.loading_container);
        progressBar = view.findViewById(R.id.progressBar2);
        wait = view.findViewById(R.id.wait);
        infogroup = view.findViewById(R.id.segmented2);
        dcd = view.findViewById(R.id.cdcp);
        cny = view.findViewById(R.id.covid_near_you);

        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDomStorageEnabled(true);
        settings.setSupportZoom(true);
        settings.supportZoom();
        settings.setDisplayZoomControls(false);
        settings.setBuiltInZoomControls(true);
        webView.setWebChromeClient(new Webcromeclient());
        webView.setWebViewClient(new mywebviewclient());

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        setupOnlicks();

        return view;

        }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    private void setupOnlicks(){
        infogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == dcd.getId()){
                    webView.loadUrl("https://www.cdc.gov/coronavirus/2019-ncov/index.html");
                    loading_container.setVisibility(View.VISIBLE);
                }else {
                    loading_container.setVisibility(View.VISIBLE);
                    webView.loadUrl("https://covidnearyou.org/?__cf_chl_captcha_tk__=c9108d5d29dc94cb56dd14d7480add354ba3410e-1587601526-0-ARUjb61xPpzyEZtH2JZWOfl7XC3ltM3XXKqtAVv6MgJN5Z0vJGeFLkGFJ23MfRHTW3B7TU8uvfF3aWKGoPg5Nt4o2T9Ol77dr7s_FVpG6SWBbYYgPu_0d_neIt-gsO4xNS1vy3Y4wk9Qwnek4zLbFR8XGze6XnJnTIfwylM3EfPYyvTqB9zm3aFDZ3qMA7JIm4GQ4xlO_8zJZoX4lK17vxmufsnCEV-hTnQS22XrypGptofpogbp1Rh78j8jH8NSAIWHtD6Uq_LU0NDpnx77pgY7TS4q4r2fLYa2cicyjwfDeA8kXOA2c-k5NEx0Q-gMjB_Va5rKmuv1g_ZeH2Pfe0ERvCJ9h97uJt2L435U68Ue0LIfhkwLu_gpWBVVqmqwyy2E0iMRpQj2Ahc0O7kp4vchL41R2lPHKHhbOy1fO-e7h5tW6_s1ZQLwIwqrbDQqsI31_qlliUv-QUUs1lX63pQa2Pa-D93-GKg3Jla00M1uP22TDMyU1pVXAhiVjV1yskqb39Zrw4zmZP2HDODBZNjmPwsXpjuDLI8gJ0PQjn1D#!/");
                }

            }
        });

        dcd.setChecked(true);
    }

    private class Webcromeclient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);


        }
    }

    private class mywebviewclient extends WebViewClient {

        @Override
        public void onPageFinished(WebView view, String url) {
            loading_container.setVisibility(View.GONE);
            super.onPageFinished(view, url);

        }
    }




}
