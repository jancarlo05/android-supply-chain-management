package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_CleaningSlider.Monthly;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import androidx.fragment.app.Fragment;

import com.diegocarloslima.fgelv.lib.FloatingGroupExpandableListView;
import com.diegocarloslima.fgelv.lib.WrapperExpandableListAdapter;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_CleaningSlider.Weekly.Dashboard_Cleaning_Weekly_ListviewAdapter;
import com.procuro.androidscm.SCMDataManager;


public class Dashboard_Monthly_Fragment extends Fragment {


    public Dashboard_Monthly_Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dashboard_cleaning_weekly_fragment, container, false);

        ExpandableListView expandableListView = view.findViewById(R.id.expandible_listview);

        Dashboard_Cleaning_Weekly_ListviewAdapter listviewAdapter = new Dashboard_Cleaning_Weekly_ListviewAdapter(getContext(),
                SCMDataManager.getInstance().getCleaningMonthly(),expandableListView);
        expandableListView.setAdapter(listviewAdapter);
        expandableListView.setDividerHeight(0);
        expandableListView.setGroupIndicator(null);

        return view;
    }

}
