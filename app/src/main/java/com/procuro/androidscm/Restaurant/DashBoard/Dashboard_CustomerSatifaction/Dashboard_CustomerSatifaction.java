package com.procuro.androidscm.Restaurant.DashBoard.Dashboard_CustomerSatifaction;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.os.Bundle;
import android.system.Os;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.procuro.androidscm.R;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.androidscm.SCMTool;
import com.procuro.apimmdatamanagerlib.SMSDashboard;
import com.procuro.apimmdatamanagerlib.SMSPositioning;

public class Dashboard_CustomerSatifaction extends AppCompatActivity {

     TextView taste,friendliness,speed,
            accurcacy,cleanliness,LikletoRecommend,LikelytoReturn,ExperienceProblem,problemResolution,
             ZoneDefection,Osat,title,Zod,CEE,FE;
     Button back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_customer_satifaction);

        taste = findViewById(R.id.taste_value);
        friendliness = findViewById(R.id.friendliness_value);
        speed = findViewById(R.id.speed_value);
        accurcacy = findViewById(R.id.accuracy_value);
        cleanliness = findViewById(R.id.cleanliness_value);

        LikletoRecommend = findViewById(R.id.likelyltorecommend_value);
        LikelytoReturn = findViewById(R.id.likelytoreturn_value);
        ExperienceProblem = findViewById(R.id.experience_problem_value);
        problemResolution = findViewById(R.id.problem_resolution_value);
        ZoneDefection = findViewById(R.id.zone_of_defection_value);
        Osat = findViewById(R.id.osat);
        Zod = findViewById(R.id.zod);
        CEE = findViewById(R.id.cee);
        FE = findViewById(R.id.fe);



        title = findViewById(R.id.username);
        back = findViewById(R.id.home);

        setupOnclicks();

        setupDisplayData();

    }


    private void setupOnclicks(){
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void setupDisplayData(){

        title.setText(SCMDataManager.getInstance().getSelectedSite().getSite().sitename);

        SMSPositioning smsPositioning = SCMDataManager.getInstance().getPositionForm();
        SMSDashboard scores = SCMDataManager.getInstance().getSmsDashboard();

        if (smsPositioning!=null){
            try {

                SCMTool.setGradeColors(CEE,SCMTool.CheckString(smsPositioning.customerExperience_CEE,"--"),this);
                SCMTool.setGradeColors(FE,SCMTool.CheckString(smsPositioning.customerExperience_FEE,"--"),this);

            }catch (Exception e){
                e.printStackTrace();
            }
        }
        if (scores!=null){
            SCMTool.setGradeColors(Zod,SCMTool.CheckString(scores.zod+"%","--"),this);
            SCMTool.setGradeColors(Osat,SCMTool.CheckString(scores.osat+"%","--"),this);

            LikletoRecommend.setText(SCMTool.CheckString(scores.likelyRecommend+"%","--"));
            LikelytoReturn.setText(SCMTool.CheckString(scores.likelyReturn+"%","--"));
            ExperienceProblem.setText(SCMTool.CheckString(scores.problemScore+"%","--"));
            problemResolution.setText(SCMTool.CheckString(scores.resolutionScore+"%","--"));
            ZoneDefection.setText(SCMTool.CheckString(scores.zod+"%","--"));

            SCMTool.setGradeColors(taste,SCMTool.CheckString(scores.tasteScore+"%","--"),this);
            SCMTool.setGradeColors(friendliness,SCMTool.CheckString(scores.friendlinessScore+"%","--"),this);
            SCMTool.setGradeColors(speed,SCMTool.CheckString(scores.speedScore+"%","--"),this);
            SCMTool.setGradeColors(accurcacy,SCMTool.CheckString(scores.accuracyScore+"%","--"),this);
            SCMTool.setGradeColors(cleanliness,SCMTool.CheckString(scores.cleanlinessScore+"%","--"),this);

        }
    }

    private TextView ScoreColors(TextView textView , String value){
        textView.setText(value);

        double valuecolor = Double.parseDouble(value.replace("%",""));

        if(valuecolor > 0 && valuecolor < 65)
        {
            textView.setTextColor(ContextCompat.getColor(this, R.color.red));
        }
        else if(valuecolor > 74)
        {
            textView.setTextColor(ContextCompat.getColor(this, R.color.green));
        }
        else
        {
            textView.setTextColor(ContextCompat.getColor(this, R.color.orange));
        }
        return  textView;
    }
    private TextView OsatColors(TextView textView, double value){
        textView.setText("OSAT: "+value+"%");

        if(value > 0 && value < 65)
        {
            textView.setBackgroundResource(R.drawable.rounded_red_bg);
        }
        else if(value > 74)
        {
            textView.setBackgroundResource(R.drawable.rounded_green_bg);
        }
        else
        {
            textView.setBackgroundResource(R.drawable.rounded_orange_bg);
        }

        return textView;
    }

}
