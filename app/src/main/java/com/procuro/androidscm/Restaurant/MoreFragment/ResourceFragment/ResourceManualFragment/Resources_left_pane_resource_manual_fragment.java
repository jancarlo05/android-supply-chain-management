package com.procuro.androidscm.Restaurant.MoreFragment.ResourceFragment.ResourceManualFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import androidx.fragment.app.Fragment;

import com.diegocarloslima.fgelv.lib.FloatingGroupExpandableListView;
import com.diegocarloslima.fgelv.lib.WrapperExpandableListAdapter;
import com.procuro.androidscm.R;
import com.procuro.androidscm.SCMDataManager;

public class Resources_left_pane_resource_manual_fragment extends Fragment {


    public Resources_left_pane_resource_manual_fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.resource_left_pane_resource_manual_fragment, container, false);

        ExpandableListView expandableListView = rootView.findViewById(R.id.expandible_listview);

        Resources_list_adapter resources_list_adapter = new Resources_list_adapter(getContext(), SCMDataManager.getInstance().getResourcesLists(),getActivity());
        expandableListView.setAdapter(resources_list_adapter);


        for (int i = 0; i < resources_list_adapter.getGroupCount(); i++) {
            expandableListView.expandGroup(i);
        }
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                return true; // This way the expander cannot be collapsed
            }
        });

        return rootView;
    }


}