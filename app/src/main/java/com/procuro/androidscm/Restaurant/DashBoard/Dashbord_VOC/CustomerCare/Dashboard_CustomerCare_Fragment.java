package com.procuro.androidscm.Restaurant.DashBoard.Dashbord_VOC.CustomerCare;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import com.procuro.androidscm.R;
import com.procuro.androidscm.SCMDataManager;

import java.util.ArrayList;
import java.util.Date;


public class Dashboard_CustomerCare_Fragment extends Fragment {

    Dashboar_VOC_CustomerCare_listviewAdapter adapter;
    ListView listView;


    public Dashboard_CustomerCare_Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dashboard_voc_customer_care_fragment, container, false);
        listView = view.findViewById(R.id.listView);
        populateCustomerCareListData();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    private void populateCustomerCareListData(){

            ArrayList<CustomerCareListData>listData = new ArrayList<>();

            listData.add(new CustomerCareListData(00417462,"","Product",
                    "Hamburger",new Date(),null,"Resolve by Field",
                    "John Doe","555 898-1212","jd.email.com","text","anytime",
                    "TEXT MESSAGE: Customer said, \"We are having dinner at Wendy's\" Evergreen Al. As a teenager I worked for Wendys and were taught to always greet the customers. There was about to 5-6 staffs no one in line and not one of the employees said hello. Several of the tables are dirty. Our food was good but customer service was terrible. I thought you all should be aware. Rae"));

            listData.add(new CustomerCareListData(01571222,"","Product",
                    "Fries",new Date(),null,"Closed - Resolved",
                    "John Doe","555 898-1212","jd.email.com","text","anytime",
                    "TEXT MESSAGE: Customer said, \"We are having dinner at Wendy's\" Evergreen Al. As a teenager I worked for Wendys and were taught to always greet the customers. There was about to 5-6 staffs no one in line and not one of the employees said hello. Several of the tables are dirty. Our food was good but customer service was terrible. I thought you all should be aware. Rae"));


            listData.add(new CustomerCareListData(1223321,"","Service",
                    "Employee",new Date(),null,"Closed - Resolved",
                    "John Doe","555 898-1212","jd.email.com","text","anytime",
                    "TEXT MESSAGE: Customer said, \"We are having dinner at Wendy's\" Evergreen Al. As a teenager I worked for Wendys and were taught to always greet the customers. There was about to 5-6 staffs no one in line and not one of the employees said hello. Several of the tables are dirty. Our food was good but customer service was terrible. I thought you all should be aware. Rae"));


            listData.add(new CustomerCareListData(02212345,"","Facilities",
                    "Outdoor",new Date(),null,"Closed - Resolved",
                    "John Doe","555 898-1212","jd.email.com","text","anytime",
                    "TEXT MESSAGE: Customer said, \"We are having dinner at Wendy's\" Evergreen Al. As a teenager I worked for Wendys and were taught to always greet the customers. There was about to 5-6 staffs no one in line and not one of the employees said hello. Several of the tables are dirty. Our food was good but customer service was terrible. I thought you all should be aware. Rae"));


            adapter = new Dashboar_VOC_CustomerCare_listviewAdapter(getContext(),listData,getActivity());
            listView.setAdapter(adapter);



    }
}
