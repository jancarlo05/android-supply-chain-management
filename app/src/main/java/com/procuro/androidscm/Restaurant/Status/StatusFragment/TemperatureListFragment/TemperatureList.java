package com.procuro.androidscm.Restaurant.Status.StatusFragment.TemperatureListFragment;

import com.procuro.apimmdatamanagerlib.PimmInstance;

import java.util.ArrayList;

public class TemperatureList {
    private String Name;
    private double temperture;
    private String signal;
    private double battery;
    private int color;
    private boolean selected;
    private ArrayList<PimmInstance>pimmInstances;

    public TemperatureList(String name, double temperture, String signal, double battery, int color, boolean selected) {
        Name = name;
        this.temperture = temperture;
        this.signal = signal;
        this.battery = battery;
        this.color = color;
        this.selected = selected;
    }

    public TemperatureList(ArrayList<PimmInstance> pimmInstances) {
        this.pimmInstances = pimmInstances;
    }

    public ArrayList<PimmInstance> getPimmInstances() {
        return pimmInstances;
    }

    public void setPimmInstances(ArrayList<PimmInstance> pimmInstances) {
        this.pimmInstances = pimmInstances;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public double getTemperture() {
        return temperture;
    }

    public void setTemperture(double temperture) {
        this.temperture = temperture;
    }

    public String getSignal() {
        return signal;
    }

    public void setSignal(String signal) {
        this.signal = signal;
    }

    public double getBattery() {
        return battery;
    }

    public void setBattery(double battery) {
        this.battery = battery;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
