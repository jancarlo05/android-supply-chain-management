package com.procuro.androidscm.Restaurant.UserProfile;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.DashBoard.Dashboard_cleaning_tutorial_listviewAdapter;
import com.procuro.androidscm.Restaurant.DashBoard.TutorialList;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.User;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;


public class User_Profile_Info_Fragment extends Fragment {

    public static TextView birthdate,state,permissions,pin,
    carrier,hiredate,reviewdate;
    private View view;

    public static EditText firstname,lastname,username,employee_id,email,city,address,mobile,zipcode,password;

    public static LinearLayout root;
    public static Date SelectedBirthdate ,SelectedHireDate ,SelectedReviewDate;
    public static boolean PermissionEnableEdit = false;

    private ConstraintLayout bod_container,hiredate_container,reviewDate_container,
            stateContainer,permissionContainer,carrierContainer;


    private LinearLayout password_text_container;


    public User_Profile_Info_Fragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
         view = inflater.inflate(R.layout.user_personal_info_fragment, container, false);

        firstname = view.findViewById(R.id.firstname);
        lastname = view.findViewById(R.id.lastname);
        username = view.findViewById(R.id.username);
        employee_id = view.findViewById(R.id.employee_id);
        email = view.findViewById(R.id.email);
        birthdate = view.findViewById(R.id.birthdate);
        pin = view.findViewById(R.id.personal_id_number);
        permissions = view.findViewById(R.id.permision);
        address = view.findViewById(R.id.address);
        city = view.findViewById(R.id.city);
        state = view.findViewById(R.id.state);
        zipcode = view.findViewById(R.id.zipcode);
        mobile = view.findViewById(R.id.mobile);
        carrier = view.findViewById(R.id.carrier);
        hiredate = view.findViewById(R.id.hire_date);
        reviewdate = view.findViewById(R.id.review_date);
        bod_container = view.findViewById(R.id.bod_container);
        hiredate_container = view.findViewById(R.id.hire_date_container);
        reviewDate_container = view.findViewById(R.id.review_date_container);
        stateContainer = view.findViewById(R.id.state_container);
        permissionContainer = view.findViewById(R.id.permission_contanier);
        carrierContainer = view.findViewById(R.id.carrier_container);
        password_text_container = view.findViewById(R.id.password_text_container);

        password = view.findViewById(R.id.password);
        root = view.findViewById(R.id.root);


        getArgument();

        CheckCurrentUserRole();

        setupOnclicks();

        return view;

    }


    private void setupOnclicks(){

        bod_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayDatePicker(getContext(),bod_container,"bod",Gravity.TOP,birthdate);
            }
        });


        reviewDate_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayDatePicker(getContext(),reviewDate_container,"reviewDate",Gravity.TOP,reviewdate);
            }
        });



        hiredate_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayDatePicker(getContext(),hiredate_container,"hiredate",Gravity.TOP,hiredate);
            }
        });


        pin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayNumpad(getContext(),pin,pin);
            }
        });


        carrierContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayCarrier(getContext(),carrierContainer,"Select Carrier",carrier);
            }
        });

        stateContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayCarrier(getContext(),stateContainer,"Select State",state);

            }
        });

        permissionContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DisplayCarrier(getContext(),permissionContainer,"Select Permission",permissions);
            }
        });





    }

    private void InializeEditHandlers(){

        final UserProfileData data = UserProfileData.getInstance();

        firstname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                data.setFirstName(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        lastname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                data.setLastName(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                data.setUsername(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });


        employee_id.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                data.setEmployeeID(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        pin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                data.setpID(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                data.setAddress1(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        city.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                data.setCity(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        zipcode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                data.setPostal(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        mobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                data.setDayPhone(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        email.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                data.setContactEmail(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        state.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                data.setState(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });


        carrier.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                data.setMobileCarrier(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });


        password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                data.setPassword(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    private void getArgument(){

        User user = null;
        Site site = null;


        UserProfileData data = UserProfileData.getInstance();
        if (data.isFromQuickAccess()){
            user =  SCMDataManager.getInstance().getUser();
            site = SCMDataManager.getInstance().getSelectedSite().getSite();
        }
        else if (data.isFromQualification()){
            user = data.getCustomUser().getUser();
            site = data.getCustomSite().getSite();

        }

        Check_Update_Create_Function(data,user);

        DisplayData(user,site);

        InializeEditHandlers();

    }

    private void DisplayData(User user,Site site){
        UserProfileData data = UserProfileData.getInstance();
        view.setFocusableInTouchMode(true);
        SimpleDateFormat format = new SimpleDateFormat("MMM/dd/yyyy");
        format.setTimeZone(TimeZone.getTimeZone(offset(SCMDataManager.getInstance().getSelectedSite().getSite().effectiveUTCOffset)));
        try {

            if (data.getFirstName()==null){
                if (user.firstName != null){
                    firstname.setText(user.firstName);
                }
            }else {
                firstname.setText(data.getFirstName());
            }


            if (data.getLastName()==null){
                if (user.lastName != null){
                    lastname.setText(user.lastName);
                }
            }else {
                lastname.setText(data.getLastName());
            }

            if (data.getUsername()==null){
                if (user.username != null){
                    username.setText(user.username);
                }
            }else {
                username.setText(data.getUsername());
            }


            if (data.getEmployeeID()==null){
                if (user.employeeID != null) {
                    if (!user.employeeID.equalsIgnoreCase("null")) {
                        employee_id.setText(user.employeeID);
                    }
                }
            }else {
                employee_id.setText(data.getEmployeeID());
            }


            if (data.getPassword()!=null){
                employee_id.setText(data.getPassword());
            }

            if (data.getContactEmail()==null){
                if (user.contactEmail != null){
                    if (!user.contactEmail.equalsIgnoreCase("null")) {
                        email.setText(user.contactEmail);
                    }

                }
            }else {
                email.setText(data.getContactEmail());
            }

            if (data.getDOB()==null){
                if (user.DOB != null){
                    birthdate.setText(format.format(user.DOB));
                }
            }else {
                birthdate.setText(format.format(data.getDOB()));
            }


            if (data.getpID()==null){

                pin.setText(ValidString(user.pID));

            }else {
                pin.setText(data.getpID());
            }

            if (data.getCity()==null){

                city.setText(ValidString(user.city));

            }else {
                city.setText(data.getCity());
            }

            if (data.getState()==null){
                state.setText(ValidString(user.state));

            }else {
                state.setText(data.getState());
            }


            if (data.getPostal()==null){
                zipcode.setText(ValidString(user.postal));

            }else {
                zipcode.setText(data.getPostal());
            }

            if (data.getDayPhone()==null){
                mobile.setText(ValidString(user.dayPhone));

            }else {
                mobile.setText(data.getDayPhone());
            }


            if (data.getHireDate()==null){
                if (user.hireDate != null){
                    hiredate.setText(format.format(user.hireDate));
                }
            }else {
                hiredate.setText(format.format(data.getHireDate()));
            }

            if (data.getReviewDate()==null){
                if (user.reviewDate != null){
                    reviewdate.setText(format.format(user.reviewDate));
                }
            }else {
                reviewdate.setText(format.format(data.getReviewDate()));
            }

            if (data.getMobileCarrier()==null){
                carrier.setText(ValidString(user.mobileCarrier));

            }else {
                carrier.setText(data.getMobileCarrier());
            }


            if (data.getAddress1()==null){
                address.setText(ValidString(user.address1));

            }else {
                address.setText(data.getAddress1());
            }


            if (data.getRole()==null){
                if (user.roles!=null){
                    ArrayList<CustomRole>customRoles = CustomRole.getAllCustomRole(user.roles);
                if (customRoles.size()>=1){
                    permissions.setText(customRoles.get(0).getName());

                }
            }
            }else {
                permissions.setText(CustomRole.convertCodetoTitle(data.getRole()).getName());
            }


        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void DisableEDit(ViewGroup vg){
        try {
            root.setAlpha(.5f);
            for (int i = 0; i < vg.getChildCount(); i++){
                View child = vg.getChildAt(i);
                child.setEnabled(false);
                if (child instanceof ViewGroup){
                    DisableEDit((ViewGroup)child);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void EnableEDit(ViewGroup vg){

        try {
            vg.setAlpha(1);
            for (int i = 0; i < vg.getChildCount(); i++){
                View child = vg.getChildAt(i);
                child.setEnabled(true);
                if (child instanceof ViewGroup){
                    EnableEDit((ViewGroup)child);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        if (PermissionEnableEdit){
            permissions.setEnabled(true);
            permissions.setAlpha(1);
        }else {
            permissions.setEnabled(false);
            permissions.setAlpha(.5f);

        }


    }


    private void CheckCurrentUserRole() {
        User user = SCMDataManager.getInstance().getUser();
        if (user != null) {
            if (user.roles != null) {
                ArrayList<CustomRole>customRoles = CustomRole.getAllCustomRole(user.roles);
                if (customRoles.size()>=1){
                    String strings = customRoles.get(0).getCode();

                    if (strings.equalsIgnoreCase("GM".toUpperCase())) {
                        permissions.setEnabled(true);
                        permissions.setAlpha(1);
                        PermissionEnableEdit = true;
                    }
                    else if (strings.equalsIgnoreCase("Store admin".toUpperCase())) {
                        permissions.setEnabled(true);
                        permissions.setAlpha(1);
                        PermissionEnableEdit = true;
                    } else if (strings.equalsIgnoreCase("Auditor".toUpperCase())) {
                        permissions.setEnabled(false);
                        permissions.setAlpha(.5f);
                    } else if (strings.equalsIgnoreCase("Ops leader".toUpperCase())) {
                        permissions.setEnabled(false);
                        permissions.setAlpha(.5f);
                    } else if (strings.equalsIgnoreCase("FSL".toUpperCase())) {
                        permissions.setEnabled(false);
                        permissions.setAlpha(.5f);
                    } else if (strings.equalsIgnoreCase("Employee".toUpperCase())) {
                        permissions.setEnabled(false);
                        permissions.setAlpha(.5f);
                    } else if (strings.equalsIgnoreCase("DM".toUpperCase())) {
                        permissions.setEnabled(true);
                        permissions.setAlpha(1);
                        PermissionEnableEdit = true;
                    }
                    else if (strings.equalsIgnoreCase("RELIEF MGRr".toUpperCase())) {
                        permissions.setEnabled(true);
                        permissions.setAlpha(1);
                        PermissionEnableEdit = true;
                    }

                }

            }
        }
    }

    public void DisplayDatePicker(final Context context, View anchor, final String title, int gravity, final TextView textView) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(gravity)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .arrowColor(ContextCompat.getColor(context,R.color.orange))
                .transparentOverlay(true)
                .contentView(R.layout.date_picker)
                .focusable(true)
                .build();
        tooltip.show();


        final DatePicker datePicker =tooltip.findViewById(R.id.datepicker);
        Button accept = tooltip.findViewById(R.id.apply);


        final SimpleDateFormat dateFormat = new SimpleDateFormat("MMM/dd/yyyy");


        datePicker.setMaxDate(System.currentTimeMillis());
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar date = Calendar.getInstance();

                date.set(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());

                if (title.equalsIgnoreCase("BOD")){
                    UserProfileData.getInstance().setDOB(removeTime(date.getTime()));
                }
               else if (title.equalsIgnoreCase("hiredate")){
                    UserProfileData.getInstance().setHireDate(removeTime(date.getTime()));
                }
                else if (title.equalsIgnoreCase("reviewDate")){
                    UserProfileData.getInstance().setReviewDate(removeTime(date.getTime()));
                }

                textView.setText(dateFormat.format(date.getTime()));

                tooltip.dismiss();
            }
        });




    }

    public void DisplayNumpad(final Context context, View anchor, final TextView textView) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.BOTTOM)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .arrowColor(ContextCompat.getColor(context,R.color.dirty_white))
                .transparentOverlay(true)
                .contentView(R.layout.pin_number_register)
                .focusable(true)
                .build();
        tooltip.show();


        final TextView name,ans1,ans2,ans3,ans4,num1,num2,num3,num4,num5,num6,num7,num8,num9,num0,delete;

        ans1 = tooltip.findViewById(R.id.ans1);
        ans2 = tooltip.findViewById(R.id.ans2);
        ans3 = tooltip.findViewById(R.id.ans3);
        ans4 = tooltip.findViewById(R.id.ans4);
        num0 = tooltip.findViewById(R.id.num0);
        num1 = tooltip.findViewById(R.id.num1);
        num2 = tooltip.findViewById(R.id.num2);
        num3 = tooltip.findViewById(R.id.num3);
        num4 = tooltip.findViewById(R.id.num4);
        num5 = tooltip.findViewById(R.id.num5);
        num6 = tooltip.findViewById(R.id.num6);
        num7 = tooltip.findViewById(R.id.num7);
        num8 = tooltip.findViewById(R.id.num8);
        num9 = tooltip.findViewById(R.id.num9);
        delete = tooltip.findViewById(R.id.delete);

        final StringBuilder answer = new StringBuilder();


        try {

            num1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("1");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            num2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("2");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            num3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("3");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            num4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("4");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            num5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("5");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            num6.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("6");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            num7.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("7");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            num8.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("8");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            num9.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("9");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            num0.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.length()<4){
                        answer.append("0");
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (answer.toString().length()>0){
                        answer.setLength(answer.length() - 1);
                        populateStringInBox(ans1,ans2,ans3,ans4,answer.toString(),tooltip,textView);
                    }
                }
            });


        }catch (Exception e){
            e.printStackTrace();
        }



    }

    private void populateStringInBox(TextView ans1,TextView ans2,TextView ans3,
                                     TextView ans4,String answer,SimpleTooltip tooltip,TextView pim){
        try {
            ans1.setBackgroundResource(R.drawable.empty_circle_black);
            ans2.setBackgroundResource(R.drawable.empty_circle_black);
            ans3.setBackgroundResource(R.drawable.empty_circle_black);
            ans4.setBackgroundResource(R.drawable.empty_circle_black);


            if (answer.length()>0){
                for (int i = 0; i <answer.length() ; i++) {
                    if (i == 0){
                        ans1.setBackgroundResource(R.drawable.full_circle_black);
                    }else if (i == 1){
                        ans2.setBackgroundResource(R.drawable.full_circle_black);
                    }else if (i == 2){
                        ans3.setBackgroundResource(R.drawable.full_circle_black);
                    }else if (i == 3){
                        ans4.setBackgroundResource(R.drawable.full_circle_black);
                    }
                }
            }

            if (answer.length()==4){
                pin.setText(answer);
                tooltip.dismiss();
            }


        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void DisplayCarrier(final Context context, View anchor, final String title, final TextView textView) {

        final SimpleTooltip tooltip = new SimpleTooltip.Builder(context)
                .anchorView(anchor)
                .gravity(Gravity.TOP)
                .dismissOnOutsideTouch(true)
                .dismissOnInsideTouch(false)
                .showArrow(true)
                .arrowColor(ContextCompat.getColor(context,R.color.orange))
                .transparentOverlay(true)
                .contentView(R.layout.custom_picker)
                .focusable(true)
                .build();
        tooltip.show();


        final NumberPicker picker =tooltip.findViewById(R.id.picker);
        TextView pickertitle = tooltip.findViewById(R.id.picker_title);
        Button accept = tooltip.findViewById(R.id.apply);
        pickertitle.setText(title);


        if (title.toLowerCase().contains("Carrier".toLowerCase())){

            final String[] values= {"AT&T","T-Mobile", "Verizon"};

            picker.setDisplayedValues(values);
            picker.setMinValue(0);
            picker.setMaxValue(values.length-1);
            picker.setWrapSelectorWheel(true);
            picker.setValue(values.length/2);


            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserProfileData.getInstance().setMobileCarrier(values[picker.getValue()]);
                    textView.setText(values[picker.getValue()]);
                    tooltip.dismiss();
                }
            });



        }else if (title.toLowerCase().contains("State".toLowerCase())){
            final String[] countries = SCMDataManager.getInstance().getCountries();
            final String[] countrycode = SCMDataManager.getInstance().getCountrycode();


            picker.setDisplayedValues(countries);
            picker.setMinValue(0);
            picker.setMaxValue(countries.length-1);
            picker.setWrapSelectorWheel(true);
            picker.setValue(countries.length/2);


            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UserProfileData.getInstance().setState(countrycode[picker.getValue()]);
                    textView.setText(countrycode[picker.getValue()]);
                    tooltip.dismiss();
                }
            });



        }else if (title.toLowerCase().contains("permission".toLowerCase())){

            final String[] role_title= {"General Manager","Support Manager", "Auditor","Employee","District Manager","Relief Crew","Relief Manager","Supervisor"};
            final String[] role_code= {"GM","OPS LEADER", "AUDITOR","EMPLOYEE","DM","RELIEF CREW","RELIEF MGR","SUPERVISOR"};

            picker.setDisplayedValues(role_title);
            picker.setMinValue(0);
            picker.setMaxValue(role_title.length-1);
            picker.setWrapSelectorWheel(true);
            picker.setValue(role_title.length/2);


            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    textView.setText(role_title[picker.getValue()]);
                    UserProfileData.getInstance().setRole(role_code[picker.getValue()]);
                    tooltip.dismiss();

                }
            });
        }

    }


    private void DisplayCHangePasswordPopup(final User user){

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.change_password_popop, null);
        dialogBuilder.setView(view);
        final AlertDialog alertDialog = dialogBuilder.create();
        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.90);
        int height = (int)(getResources().getDisplayMetrics().heightPixels*0.60);
        Objects.requireNonNull(alertDialog.getWindow()).setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
        Objects.requireNonNull(alertDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Objects.requireNonNull(alertDialog.getWindow()).getAttributes().windowAnimations = R.style.slide_down_up;
        alertDialog.show();


        final TextView current_password_message,password_text,confirm_password_text,password_match_message;
        final EditText current_password,new_password,confirm_password;
        final Button  confirm,cancel;


        current_password_message = view.findViewById(R.id.current_password_message);
        password_text = view.findViewById(R.id.password_text);
        confirm_password_text = view.findViewById(R.id.confirm_password_text);
        password_match_message = view.findViewById(R.id.password_match_message);

        current_password = view.findViewById(R.id.current_password);
        new_password = view.findViewById(R.id.new_password);
        confirm_password = view.findViewById(R.id.confirm_password);


        confirm = view.findViewById(R.id.confirm);
        cancel = view.findViewById(R.id.cancel);

        current_password_message.setVisibility(View.INVISIBLE);
        password_match_message.setVisibility(View.INVISIBLE);

        password_text.setAlpha(.5f);
        confirm_password_text.setAlpha(.5f);
        confirm_password_text.setAlpha(.5f);
        new_password.setAlpha(.5f);
        confirm_password.setAlpha(.5f);
        new_password.setEnabled(false);
        confirm_password.setEnabled(false);
        confirm.setEnabled(false);
        confirm.setAlpha(.5f);

        current_password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length()>0){
                    current_password_message.setVisibility(View.VISIBLE);

                    if (s.toString().equalsIgnoreCase(SCMDataManager.getInstance().getPassword())){
                        current_password_message.setText("Correct Password");
                        current_password_message.setTextColor(ContextCompat.getColor(getContext(),R.color.green));

                        password_text.setAlpha(1);
                        new_password.setAlpha(1);
                        new_password.setEnabled(true);


                    }else {
                        current_password_message.setText("Password Incorrect");
                        current_password_message.setTextColor(ContextCompat.getColor(getContext(),R.color.red));
                        password_match_message.setVisibility(View.INVISIBLE);
                        password_text.setAlpha(.5f);
                        confirm_password_text.setAlpha(.5f);
                        confirm_password_text.setAlpha(.5f);
                        new_password.setAlpha(.5f);
                        confirm_password.setAlpha(.5f);
                        new_password.setEnabled(false);
                        confirm_password.setEnabled(false);
                        confirm.setEnabled(false);
                        confirm.setAlpha(.5f);
                    }

                }else {
                    password_match_message.setVisibility(View.INVISIBLE);
                    current_password_message.setVisibility(View.INVISIBLE);
                    password_match_message.setVisibility(View.INVISIBLE);
                    password_text.setAlpha(.5f);
                    confirm_password_text.setAlpha(.5f);
                    confirm_password_text.setAlpha(.5f);
                    new_password.setAlpha(.5f);
                    confirm_password.setAlpha(.5f);
                    new_password.setEnabled(false);
                    confirm_password.setEnabled(false);
                    confirm.setEnabled(false);
                    confirm.setAlpha(.5f);
                    confirm.setEnabled(false);
                    confirm.setAlpha(.5f);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        new_password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length()>0){
                    confirm_password_text.setAlpha(1);
                    confirm_password.setEnabled(true);
                    confirm_password.setAlpha(1);

                }else {
                    confirm_password_text.setAlpha(.5f);
                    confirm_password.setEnabled(false);
                    confirm_password.setAlpha(.5f);
                    password_match_message.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        confirm_password.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length()>0){
                    password_match_message.setVisibility(View.VISIBLE);
                    if (s.toString().equalsIgnoreCase(new_password.getText().toString())){
                        password_match_message.setText("Password Match");
                        password_match_message.setTextColor(ContextCompat.getColor(getContext(),R.color.green));

                        confirm.setAlpha(1);
                        confirm.setEnabled(true);


                    }else {
                        password_match_message.setText("Password Not Match");
                        password_match_message.setTextColor(ContextCompat.getColor(getContext(),R.color.red));

                        confirm.setAlpha(.5f);
                        confirm.setEnabled(false);


                    }

                }else {
                    password_match_message.setVisibility(View.INVISIBLE);


                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });


        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserProfileData.getInstance().setPassword(confirm_password.getText().toString());
                alertDialog.dismiss();
            }
        });

    }

    private void Check_Update_Create_Function(UserProfileData data, User user){

        if (data.isIsupdate()){
            password.setVisibility(View.GONE);
            password_text_container.setVisibility(View.GONE);
            if (user.active){
                if (data.isUserEditEnabled()){
                    EnableEDit(root);
                }else {
                    DisableEDit(root);
                }
            }else {
                UserProfileActivity.editlock.setVisibility(View.GONE);
                DisableEDit(root);
            }

        }else if (data.isCreate()){
            UserProfileActivity.editlock.setVisibility(View.GONE);
            password.setVisibility(View.VISIBLE);
            password_text_container.setVisibility(View.VISIBLE);

        }

    }

    private String ValidString(String s){
        String string = "";
        if (s!=null){
            if (!s.equalsIgnoreCase("null")){
                string = s;
            }
        }
        return string;
    }

    private String offset(int value){
        StringBuilder offset = new StringBuilder();
        offset.append("GMT");
        offset.append(value);
        offset.append(":00");

        return offset.toString();
    }

    private  Date removeTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
}
