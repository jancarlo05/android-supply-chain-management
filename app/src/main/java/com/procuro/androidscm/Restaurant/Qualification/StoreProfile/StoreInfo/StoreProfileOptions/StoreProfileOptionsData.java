package com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.StoreProfileOptions;

import com.procuro.apimmdatamanagerlib.DashboardKitchenData;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsConfiguration;

import java.util.ArrayList;

public class StoreProfileOptionsData {
    private static StoreProfileOptionsData instance = new StoreProfileOptionsData();

    private String kitchentype;
    private DashboardKitchenData kitchenData;

    private ArrayList<KitchenCriteria> kitchenCriteria;

    public static StoreProfileOptionsData getInstance() {
        if(instance == null) {
            instance = new StoreProfileOptionsData();
        }
        return instance;
    }


    public ArrayList<KitchenCriteria> getKitchenCriteria() {
        return kitchenCriteria;
    }

    public void setKitchenCriteria(ArrayList<KitchenCriteria> kitchenCriteria) {
        this.kitchenCriteria = kitchenCriteria;
    }

    public String getKitchentype() {
        return kitchentype;
    }

    public void setKitchentype(String kitchentype) {
        this.kitchentype = kitchentype;
    }

    public DashboardKitchenData getKitchenData() {
        return kitchenData;
    }

    public void setKitchenData(DashboardKitchenData kitchenData) {
        this.kitchenData = kitchenData;
    }



    public static void setInstance(StoreProfileOptionsData instance) {
        StoreProfileOptionsData.instance = instance;
    }
}
