package com.procuro.androidscm.Restaurant.Status.StatusFragment.TemperatureListFragment;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.DatePicker;

import androidx.fragment.app.DialogFragment;

import com.procuro.androidscm.R;
import com.procuro.androidscm.Restaurant.AllStorePopup.Status_StoreListDialogFragment_adapter;
import com.procuro.androidscm.SCMDataManager;
import com.procuro.apimmdatamanagerlib.PimmInstance;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class Status_DateRangePicker_DialogFragment extends DialogFragment {

    Status_StoreListDialogFragment_adapter adapters;
    @SuppressLint("StaticFieldLeak")
    public static WebView webView;
    public static PimmInstance instance;

    public static Status_DateRangePicker_DialogFragment newInstance(WebView webViews,PimmInstance temp) {
        webView = webViews;
        instance = temp;

        return new Status_DateRangePicker_DialogFragment();
    }
    @Override
        public int getTheme() {
        return R.style.slide_down_up;
        }

    @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.status_date_range_picker, container, false);

        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.90);
        int height = dpToPx(585);


        Objects.requireNonNull(getDialog().getWindow()).setLayout(width, height);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.slide_down_up;
        view.setFocusableInTouchMode(true);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        final DatePicker start = view.findViewById(R.id.datePicker_start);
        final DatePicker end = view.findViewById(R.id.datePicker_end);
        Button accept = view.findViewById(R.id.apply);
        end.setMaxDate(System.currentTimeMillis());
        start.setMaxDate(System.currentTimeMillis());

        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar startdate = Calendar.getInstance();
                startdate.set(start.getYear(), start.getMonth(), start.getDayOfMonth());

                final Calendar enddate = Calendar.getInstance();
                enddate.set(end.getYear(), end.getMonth(), end.getDayOfMonth());

                webView.loadUrl(DisplayDateRangePicker(instance.instanceId,startdate.getTime(),enddate.getTime()));
                dismiss();
            }
        });

        return view;

        }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    private String DisplayDateRangePicker(final String instanceID, Date start,Date end){

        final String[] link = {""};
        final SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmm");

        int width  = 800;
        int height = 800;

        link[0] ="https://wendys.pimm.us/DesktopModules/EmailChart.aspx?" +
                "instanceid="+instanceID+
                "&username="+ SCMDataManager.getInstance().getUsername()+
                "&password="+SCMDataManager.getInstance().getPassword()+
                "&start="+format.format(start.getTime())+
                "&end="+format.format(end.getTime())+
                "width="+width+
                "&height="+height;

        System.out.println("start: "+format.format(start.getTime()));
        System.out.println("end: "+format.format(end.getTime()));
        return link[0];
    }


}
