package com.procuro.androidscm.Restaurant.Qualification.StoreProfile.StoreInfo.FormsList;

import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsConfiguration;

public class CustomSiteSettingsConfiguration {
    SiteSettingsConfiguration siteSettingsConfiguration;
    boolean updated;


    public CustomSiteSettingsConfiguration(SiteSettingsConfiguration siteSettingsConfiguration) {
        this.siteSettingsConfiguration = siteSettingsConfiguration;
    }

    public CustomSiteSettingsConfiguration(SiteSettingsConfiguration siteSettingsConfiguration, boolean updated) {
        this.siteSettingsConfiguration = siteSettingsConfiguration;
        this.updated = updated;
    }

    public SiteSettingsConfiguration getSiteSettingsConfiguration() {
        return siteSettingsConfiguration;
    }

    public void setSiteSettingsConfiguration(SiteSettingsConfiguration siteSettingsConfiguration) {
        this.siteSettingsConfiguration = siteSettingsConfiguration;
    }

    public boolean isUpdated() {
        return updated;
    }

    public void setUpdated(boolean updated) {
        this.updated = updated;
    }
}
