package com.procuro.androidscm;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.material.snackbar.Snackbar;
import com.google.maps.android.clustering.ClusterManager;
import com.procuro.androidscm.Restaurant.CustomSite;
import com.procuro.androidscm.Restaurant.DashBoard.CustomUser;
import com.procuro.androidscm.Restaurant.Qualification.Map.Map_Fragment;
import com.procuro.androidscm.Restaurant.Qualification.Map.SampleClusterItem;
import com.procuro.androidscm.Restaurant.Qualification.QualificationFragment.QualificationData;
import com.procuro.androidscm.Restaurant.Schema.Dayparts;
import com.procuro.androidscm.Restaurant.Schema.Schema;
import com.procuro.androidscm.Restaurant.UserProfile.CustomRole;
import com.procuro.apimmdatamanagerlib.Certification;
import com.procuro.apimmdatamanagerlib.FormPack;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.PropertyValue;
import com.procuro.apimmdatamanagerlib.Site;
import com.procuro.apimmdatamanagerlib.SiteSettings;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsConfiguration;
import com.procuro.apimmthriftservices.services.siteConfig.SiteSettingsEquipment;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Objects;
import java.util.TimeZone;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static android.content.ContentValues.TAG;
import static com.procuro.androidscm.Restaurant.Login.Login.SCM_FILE_FOLDER;


public class SCMTool {
   public static aPimmDataManager dataManager = aPimmDataManager.getInstance();

   public static boolean isFGPopulatedd;

    public  static String SCM_FILE_FOLDER = ".SCMFiles";
    public  static String root = Environment.getExternalStorageDirectory().toString();


    public static int getYearByDate(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);;

        return  year;
    }

    public static String CheckString(String value,String ifnull){
        if (value!=null){
            if (value.equalsIgnoreCase("null") || value.equalsIgnoreCase("")){
                value = ifnull;
            }
        }else {
            value = ifnull;
        }
        return value;
    }

    public static boolean ValidString(String value){
        boolean valid = false;
        if (value!=null){
            if (!value.equalsIgnoreCase("null") && !value.equalsIgnoreCase("")){
                valid = true;
            }
        }
        return valid;
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int getActiveORinUseCount(ArrayList arrayList){
        int active_count = 0;
        if (arrayList!=null){

            Log.i("SCM TOOL", "object total size : "+ arrayList.size());

            for (int i = 0; i <arrayList.size() ; i++) {
                Object object = arrayList.get(i);
                if (object instanceof User){
                    if (((User) object).active){
                        active_count++;
                    }
                }else if (object instanceof CustomUser){
                    if (((CustomUser) object).getUser().active){
                        active_count++;
                    }
                }else if (object instanceof SiteSettingsEquipment){
                    if (((SiteSettingsEquipment) object).inUse){

                        active_count++;
                    }
                }
            }
        }else {
            Log.i("SCM TOOL", "NULL ARRAY");
        }
        return active_count;
    }


    public static ArrayList<User> getActiveUser(ArrayList<User>users){
        ArrayList<User> newUsers = new ArrayList<>();
        if (users!=null){
            for (User user : users){
                if (user.active){
                    newUsers.add(user);
                }
            }
        }
        return newUsers;
    }

    public static ArrayList<CustomUser> getActiveCustomUser(ArrayList<CustomUser>users){
        ArrayList<CustomUser> newUsers = new ArrayList<>();
        if (users!=null){
            for (CustomUser user : users){
                if (user.getUser().active){
                    newUsers.add(user);
                }
            }
        }
        return newUsers;
    }

    public static ArrayList<CustomUser> ConvertUserToCustomUser(ArrayList<User>users){
        ArrayList<CustomUser> newUsers = new ArrayList<>();
        if (users!=null){
            for (User user : users){
                newUsers.add(new CustomUser(user));
            }
        }
        return newUsers;
    }

    public static void DismissDialogInThrift(final ProgressDialog progressDialog, final Object object){
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
                if (object!=null){
                    if (object instanceof RadioButton){
                        ((RadioButton) object).setChecked(true);
                    }
                    else if (object instanceof Button){
                        ((Button) object).setEnabled(false);
                        ((Button) object).setAlpha(.5f);
                    }
                }
            }
        });
    }

    public static void DisplaySnackbar(String Message,View view){
        try {
            Snackbar snackbar = Snackbar
                    .make(view, Message, Snackbar.LENGTH_LONG);
            snackbar.show();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static CustomUser SearchCustomUserByName(String name,ArrayList<CustomUser>users){
        CustomUser user = null;
        for (CustomUser customUser :users){
            if (name.contains(customUser.getUser().firstName)){
                if (name.contains(customUser.getUser().lastName)){
                    user = customUser;
                }
            }
        }
        return  user;
    }

    public static TextView setGradeColors(TextView textView , String value, Context context){
        textView.setText(value);
        double valuecolor = 0;
        boolean parsable = false;

        Pattern p = Pattern.compile("([0-9])");
        Matcher m = p.matcher(value);

        if(m.find()){
            parsable = true;
        }

        if (parsable){
            valuecolor = Double.parseDouble(value.replace("%",""));
        }else {
            return  textView;

        }
        if(valuecolor > 0 && valuecolor < 65)
        {
            textView.setTextColor(ContextCompat.getColor(context, R.color.red));
        }
        else if(valuecolor > 74)
        {
            textView.setTextColor(ContextCompat.getColor(context, R.color.green));
        }
        else
        {
            textView.setTextColor(ContextCompat.getColor(context, R.color.orange));
        }
        return  textView;
    }
    public static String getFirstWord(String value){

        String[] arr = value.split(" ");
        String firstWord = "";
        StringBuilder name = new StringBuilder();

        if (arr.length>0){
            firstWord = arr[0];
        }

        return  firstWord;
    }

    public static String RemoveFirstWord(String value){

        String[] arr = value.split(" ");
        StringBuilder name = new StringBuilder();

        for (int i = 0; i <arr.length ; i++) {
           if (i >= 2){
                if (i == 2){
                    name.append(arr[i]);
                }else {
                    name.append(" ").append(arr[i]);
                }
            }
        };

        return  name.toString().replace("-","");
    }

    public static int getDashboardPositionImage(String string, String templatecode,boolean isFGPopulated){

        if (!isFGPopulatedd){
            isFGPopulatedd = isFGPopulated;
        }

        int image = 0;

        if (string.equalsIgnoreCase("FRONT REG1")){
            if (templatecode.contains("CCW".toUpperCase())){
                image = R.drawable.dashboard_front_reg;
            }
        }
        else if (string.equalsIgnoreCase("FRONT REG1 CW")){
            if (templatecode.contains("-CW-".toUpperCase())){
                image = R.drawable.dashboard_front_reg;
            }
        }
        else if (string.equalsIgnoreCase("FRONT REG2")){

            if (templatecode.contains("CCW".toUpperCase())) {

                if (templatecode.contains("NK-2".toUpperCase())){

                    image = R.drawable.dashboard_front_reg;
                }else if (templatecode.contains("CK-2".toUpperCase())){

                    image = R.drawable.dashboard_front_reg;

                }else if (templatecode.contains("FSK-2".toUpperCase())){

                    image = R.drawable.dashboard_front_reg;
                }
            }
        }
        else if (string.equalsIgnoreCase("FRONT REG2 CW")){
            if (templatecode.contains("-CW-".toUpperCase())) {
                if (templatecode.contains("NK-2".toUpperCase())){

                    image = R.drawable.dashboard_front_reg;

                }else if (templatecode.contains("CK-2".toUpperCase())){

                    image = R.drawable.dashboard_front_reg;
                }else if (templatecode.contains("FSK-2".toUpperCase())){

                    image = R.drawable.dashboard_front_reg;
                }
            }
        }

        else if (string.equalsIgnoreCase("PU AREA")){
            if (templatecode.contains("-CCW-")){
                image = R.drawable.dashboard_pick_up_area;
            }
        }

        else if (string.equalsIgnoreCase("PU AREA CW")){
            if (templatecode.contains("-CW-".toUpperCase())){
                image = R.drawable.dashboard_pick_up_area;
            }
        }

        else if (string.equalsIgnoreCase("PUW REG3")){

            if (!templatecode.contains("-0-0")){
                image = R.drawable.dashboard_puw_reg;
            }

        }else if (string.equalsIgnoreCase("PUW REG4")){

            if (templatecode.contains("-1-1-2") || templatecode.contains("-1-2-3")){

                image = R.drawable.dashboard_puw_reg;

            }else if (templatecode.contains("-2-1-2")|| templatecode.contains("-2-2-3")){

                image = R.drawable.dashboard_puw_reg;
            }

            else if (templatecode.contains("-3-1-2") || templatecode.contains("-3-2-3")){
                image = R.drawable.dashboard_puw_reg;
            }


        }else if (string.equalsIgnoreCase("PUW REG5")){

            if (templatecode.contains("-1-2-2") || templatecode.contains("-1-2-3")){

                image = R.drawable.dashboard_puw_reg;

            }else if (templatecode.contains("-2-2-2")|| templatecode.contains("-2-2-3")){

                image = R.drawable.dashboard_puw_reg;
            }

            else if (templatecode.contains("-3-2-2") || templatecode.contains("-3-2-3")){
                image = R.drawable.dashboard_puw_reg;
            }

        } else if (string.equalsIgnoreCase("SSD")){


        }else if (string.equalsIgnoreCase("SALAD")){

        }
        else if (string.equalsIgnoreCase("SL1")){
            if (templatecode.contains("NSL".toUpperCase())){
                image = R.drawable.dashboard_tinted;
            }
        }else if (string.equalsIgnoreCase("SL2")){
            if (templatecode.contains("NSL".toUpperCase())){
                image = R.drawable.dashboard_tinted;
            }else if (templatecode.contains("1SL".toUpperCase())){
                image = R.drawable.dashboard_tinted;
            }
        }
        else if (string.equalsIgnoreCase("DSG1")){


        } else if (string.equalsIgnoreCase("HH1")){


        }else if (string.equalsIgnoreCase("DSG2")){

            if (!isFGPopulatedd){
                if (templatecode.contains("1DSG".toUpperCase())){
                    image = R.drawable.dashboard_tinted;

                }else if (templatecode.contains("1FG".toUpperCase())){
                    image = R.drawable.dashboard_tinted;
                }
                isFGPopulatedd = true;
            }
        }
        else if (string.equalsIgnoreCase("HH2")){

            if (!isFGPopulated){
                if (templatecode.contains("1DSG".toUpperCase())){
                    image = R.drawable.dashboard_tinted;

                }else if (templatecode.contains("1FG".toUpperCase())){
                    image = R.drawable.dashboard_tinted;
                }
                isFGPopulatedd = true;
            }

        }
        else if (string.equalsIgnoreCase("CAR1")){
            if (templatecode.contains("1L".toUpperCase())){
                image = R.drawable.dashboard_car1;

            }else if (templatecode.contains("YL".toUpperCase())){
                image = R.drawable.dashboard_car1;
            }

        }else if (string.equalsIgnoreCase("CARTIME1")){
            if (templatecode.contains("1L".toUpperCase())){
                image = R.drawable.dashboard_car_time;

            }else if (templatecode.contains("YL".toUpperCase())){
                image = R.drawable.dashboard_car_time;
            }
        }
        else if (string.equalsIgnoreCase("CAR2")){
            if (templatecode.contains("1L".toUpperCase())){
                image = R.drawable.dashboard_car2;

            }else if (templatecode.contains("YL".toUpperCase())){
                image = R.drawable.dashboard_car2;
            }
        } else if (string.equalsIgnoreCase("CARTIME2")){
            if (templatecode.contains("1L".toUpperCase())){
                image = R.drawable.dashboard_car_time;

            }else if (templatecode.contains("YL".toUpperCase())){
                image = R.drawable.dashboard_car_time;
            }

        }else if (string.equalsIgnoreCase("CAR3")){
            if (templatecode.contains("1L".toUpperCase())){
                image = R.drawable.dashboard_car3_green;

            }else if (templatecode.contains("YL".toUpperCase())){
                image = R.drawable.dashboard_car3_green;
            }

        } else if (string.equalsIgnoreCase("CARTIME3")){
            if (templatecode.contains("1L".toUpperCase())){
                image = R.drawable.dashboard_car_time;

            }else if (templatecode.contains("YL".toUpperCase())){
                image = R.drawable.dashboard_car_time;

            }
        } else if (string.equalsIgnoreCase("CAR4")){
            if (templatecode.contains("YL")){
                image = R.drawable.dashboard_car4;
            }
        }
        else if (string.equalsIgnoreCase("CARTIME4")){
            if (templatecode.contains("YL")){
                image = R.drawable.dashboard_car_time;
            }
        }
        else {
            image = R.drawable.dashboard_tinted;
        }
        return image;
    }

    public static int spToPx(int sp) {
        return (int) (sp * Resources.getSystem().getDisplayMetrics().scaledDensity);
    }

    public static  TextView  CreateDashboardPositionBackground(TextView textView, String backgroundColor) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setCornerRadii(new float[] { dpToPx(5), dpToPx(5), dpToPx(5), dpToPx(5), dpToPx(5), dpToPx(5), dpToPx(5), dpToPx(5)});
        shape.setColor(Color.parseColor(backgroundColor));
        textView.setBackground(shape);
        return textView;
    }

    public static BitmapDescriptor getMapIcon(int drawable,Context context){
        BitmapDescriptor icon = null;
        int height = dpToPx(65);
        int width = dpToPx(45);

        BitmapDrawable bitmapdraw = (BitmapDrawable)context.getResources().getDrawable(drawable,null);
        Bitmap b = bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
        icon =  BitmapDescriptorFactory.fromBitmap(smallMarker);

        return icon;

    }

    public static String getCompleteAddress(Site site){
        StringBuilder address = new StringBuilder();

        if (site.Address.Address2 != null){
            if (!site.Address.Address2.equalsIgnoreCase("null")){
                if (!site.Address.Address2.equalsIgnoreCase("")){
                    address.append(site.Address.Address2);
                }
            }
        }

        if (site.Address.Address1 != null){
            if (!site.Address.Address1.equalsIgnoreCase("null")){
                if (!site.Address.Address1.equalsIgnoreCase("")){
                    address.append(site.Address.Address1);
                }
            }
        }

        if (site.Address.City != null){
            if (!site.Address.City.equalsIgnoreCase("null")){
                if (!site.Address.City.equalsIgnoreCase("")){
                    address.append(", ");
                    address.append(site.Address.City);
                }
            }
        }

        if (site.Address.Province != null){
            if (!site.Address.Province.equalsIgnoreCase("null")){
                if (!site.Address.Province.equalsIgnoreCase("")){
                    address.append(" ");
                    address.append(site.Address.Province);
                }
            }
        }

        if (site.Address.Postal != null){
            if (!site.Address.Postal.equalsIgnoreCase("null")){
                if (!site.Address.Postal.equalsIgnoreCase("")){
                    address.append(", ");
                    address.append(site.Address.Postal);
                }
            }
        }
        return address.toString();
    }

    public static void moveToSelectedMarkers(Context context, final GoogleMap mMap, final ArrayList<LatLng>markers){
        final int width = (int)(context.getResources().getDisplayMetrics().widthPixels*0.15);
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                for (LatLng marker : markers) {
                    builder.include(marker);
                }
                LatLngBounds bounds = builder.build();
                int padding = (int) (width); // offset from edges of the map in pixels
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                mMap.animateCamera(cu);
            }
        });
    }


    public static void ExpandListview(int count , ExpandableListView expandableListView){
        if (count>0){
            for (int i = 0; i <count ; i++) {
                expandableListView.expandGroup(i);
            }
            expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                @Override
                public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                    return true;
                }
            });
        }
    }

    public static void animateJump(final View target){
        try {
            int delay = 380;
            //jum 1

            ObjectAnimator jump1_up = ObjectAnimator.ofFloat( target, "translationY", 0f, -70f );
            jump1_up.setDuration(300);
            jump1_up.start();

            ObjectAnimator jump1_down = ObjectAnimator.ofFloat( target, "translationY", -70f, 0f );
            jump1_down.setDuration(400);
            jump1_down.setStartDelay(delay);
            jump1_down.start();

            ObjectAnimator jump2_up = ObjectAnimator.ofFloat( target, "translationY", 0f, -70f );
            jump2_up.setDuration(500);
            delay += 380;
            jump2_up.setStartDelay(delay);
            jump2_up.start();

            ObjectAnimator jump2_down = ObjectAnimator.ofFloat( target, "translationY", -70f, 0f );
            jump2_down.setDuration(500);
            delay += 550;
            jump2_down.setStartDelay(delay);// 滞空時間を考慮してちょっとあとに落ちてくる
            jump2_down.start();

            jump2_down.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    animateJump(target);
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });

        } catch (Exception e){
            e.printStackTrace();
        }
    }


    public static void unzip(byte[] bytes,Context context) throws IOException {
        System.out.println("unzip start");
//        String root = context.getFilesDir().toString();

        File destDir = new File(root, SCM_FILE_FOLDER);
        if (!destDir.exists()) {
            destDir.mkdirs();
            destDir.setWritable(true);
            destDir.setReadable(true);
            destDir.setExecutable(true);
        }

        ZipInputStream zipIn = new ZipInputStream(new ByteArrayInputStream(bytes));
        ZipEntry entry = zipIn.getNextEntry();
        // iterates over entries in the zip file
        while (entry != null) {
            String filePath = destDir + File.separator + entry.getName();
            if (!entry.isDirectory()) {
                // if the entry is a file, extracts it
                extractFile(zipIn, filePath);

            } else {
                // if the entry is a directory, make the directory
                File dir = new File(filePath);
                dir.mkdir();
            }
            zipIn.closeEntry();
            entry = zipIn.getNextEntry();
        }
        zipIn.close();
    }

    public static void purgeDirectory(File dir) {
        if (dir.exists()){
            for (File file: dir.listFiles()) {
                if (file.isDirectory()){
                    purgeDirectory(file);
                }
                file.delete();
            }
        }
    }

    public static void listData(File dir) {
        for (File file: dir.listFiles()) {
            if (file.isDirectory()){
                System.out.println("DIRECTORY: "+file.getName());
                listData(file);
            }else {
                System.out.println("file: "+file.getName());
            }
        }
    }

    public static void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
        byte[] bytesIn = new byte[1024*1024];
        int read = 0;
        while ((read = zipIn.read(bytesIn)) != -1) {
            bos.write(bytesIn, 0, read);
        }
        bos.close();
    }

    public static void clearAppData(Context context) {
        try {
            Runtime runtime = Runtime.getRuntime();
            runtime.exec("pm clear " + context.getPackageName() + " HERE");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static double multiplier(int def,Context context){
        double multiplier = 0;
        double x = (int)(context.getResources().getDisplayMetrics().widthPixels);
        multiplier = x/def;
        return  multiplier;
    }

    public static int ApplyMultiplier(int value, double mulitiplier){
        value = (int) (value * mulitiplier);
        return value;
    }

    public static String getTime(TimePicker timePicker) {
        int hour = timePicker.getHour();
        int min = timePicker.getMinute();

        return showTime(hour, min);
    }

    public static String showTime(int hour, int min) {
        String format  = "";
        if (hour == 0) {
            hour += 12;
            format = "AM";
        } else if (hour == 12) {
            format = "PM";
        } else if (hour > 12) {
            hour -= 12;
            format = "PM";
        } else {
            format = "AM";
        }
        StringBuilder builder = new StringBuilder();
        builder.append(hour);
        builder.append(":");
        if (min<10){
            builder.append("0");
        }
        builder.append(min);
        builder.append(" ");
        builder.append(format);

        return builder.toString();
    }

    public static void DisableView(View view, float alpha){
        view.setAlpha(alpha);
        view.setEnabled(false);
    }

    public static void EnableView(View view, float alpha){
        view.setAlpha(alpha);
        view.setEnabled(true);
    }

    public static Calendar StringTimeToDate(String time,int offset){
        DateFormat dateFormat = new SimpleDateFormat("hh:mm");
        DateFormat timformat = new SimpleDateFormat("MMM dd, yyyy hh:mm a");
        timformat.setTimeZone(TimeZone.getTimeZone("UTC"));

        Calendar calendar = Calendar.getInstance();
        Calendar daypart = Calendar.getInstance();

        try {
            Date d = dateFormat.parse(time);
            daypart.setTime(d);

            calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            calendar.set(Calendar.HOUR,daypart.get(Calendar.HOUR));
            calendar.set(Calendar.MINUTE,daypart.get(Calendar.MINUTE));
            calendar.set(Calendar.AM_PM,daypart.get(Calendar.AM_PM));
            calendar.add(Calendar.MINUTE,offset);

            int AMPM = daypart.get(Calendar.HOUR);

            System.out.println("SCM TOOL : "+timformat.format(calendar.getTime()));

        }catch (Exception e){
            e.printStackTrace();
        }
        return calendar;
    }

    public static Calendar StringTimeToDateNoOffset(String time){
        DateFormat dateFormat = new SimpleDateFormat("hh:mm");
        DateFormat timformat = new SimpleDateFormat("MMM dd, yyyy hh:mm a");
        timformat.setTimeZone(TimeZone.getTimeZone("UTC"));

        Calendar calendar = Calendar.getInstance();
        Calendar daypart = Calendar.getInstance();
        try {
            Date d = dateFormat.parse(time);
            daypart.setTime(d);

            calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            calendar.set(Calendar.HOUR,daypart.get(Calendar.HOUR));
            calendar.set(Calendar.MINUTE,daypart.get(Calendar.MINUTE));
            calendar.set(Calendar.AM_PM,daypart.get(Calendar.AM_PM));
            calendar.clear(Calendar.SECOND);

            int AMPM = daypart.get(Calendar.HOUR);

            System.out.println("SCM TOOL : "+timformat.format(calendar.getTime()));

        }catch (Exception e){
            e.printStackTrace();
        }
        return calendar;
    }

    public static Calendar setCalendarDaypart(Date date,String time){
        DateFormat dateFormat = new SimpleDateFormat("hh:mm");
        DateFormat timformat = new SimpleDateFormat("MMM dd, yyyy hh:mm a");
        timformat.setTimeZone(TimeZone.getTimeZone("UTC"));

        Calendar daypart = Calendar.getInstance();
        daypart.clear();

        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));

        try {
            Date d = dateFormat.parse(time);
            daypart.setTime(d);

            calendar.setTime(date);
            calendar.set(Calendar.HOUR,daypart.get(Calendar.HOUR));
            calendar.set(Calendar.MINUTE,daypart.get(Calendar.MINUTE));
            calendar.set(Calendar.AM_PM,daypart.get(Calendar.AM_PM));
            calendar.clear(Calendar.SECOND);
            calendar.clear(Calendar.MILLISECOND);


        }catch (Exception e){
            e.printStackTrace();
        }
        return calendar;
    }

    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static String getRequiredPosition(String day , String daypart, Site site){
        String required_position = "0";
        StringBuilder body = new StringBuilder();
        body.append("Config:staffingPeakHourRequired".toLowerCase());
        body.append(daypart);
        body.append("_");
        body.append(day);
        ArrayList<PropertyValue>propertyValues = site.PropertiesList;

        System.out.println("FIND : "+body.toString());

        for (PropertyValue propertyValue : propertyValues){
            if (propertyValue.property.equalsIgnoreCase(body.toString())){
                System.out.println("PROPERTY VALUE : "+propertyValue.property+ " : "+propertyValue.value);
                return propertyValue.value;
            }
        }

        return required_position;
    }

    public static boolean isBetween(final Date min, final Date max, final Date date){
        boolean isbetween = false;

        //AFTER OR EQUAL TO SELECTED DATE
        if (date.compareTo(min)>=0){
            //BEFORE OR EQUAL
            if (date.compareTo(max)<0){
                isbetween = true;
            }
        }

        return isbetween;
    }

    public static boolean isBetweenEqual(final Date min, final Date max, final Date date){
        boolean isbetween = false;

        //AFTER OR EQUAL TO SELECTED DATE
        if (date.compareTo(min)>=0){
            //BEFORE OR EQUAL
            if (date.compareTo(max)<=0){
                isbetween = true;
            }
        }

        return isbetween;
    }

    public static void wrtieFileOnInternalStorage(String title,String sBody){

        System.out.println("wrtieFileOnInternalStorage: START");
        String root = Environment.getExternalStorageDirectory().toString();
        File file = new File(root);
        if(!file.exists()){
            file.mkdir();
        }else {
            file.delete();
        }
        try{
            File gpxfile = new File(file, title);
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();

            BufferedReader br = new BufferedReader(new FileReader(gpxfile));
            StringBuilder stringBuilder= new StringBuilder();
            String st;
            while ((st = br.readLine()) != null){
                stringBuilder.append(st);
            }

//            String s = stringBuilder.toString().replaceAll("[^\\x20-\\x7E]", "");
//            System.out.println(s);
//            JSONObject jsonObject = new JSONObject(s);
//            System.out.println(jsonObject);



        }catch (Exception e){
            e.printStackTrace();

        }
    }


    public static int getSkillIconByCertification(Certification certification){

        if (certification.Certification.equalsIgnoreCase("0")){

          if (certification.StarRating.equalsIgnoreCase("1")){
              return R.drawable.dining_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
              return R.drawable.dining_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
              return R.drawable.dining_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
              return R.drawable.dining_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
              return R.drawable.dining_5;

            }else {
                return R.drawable.dining_room;
            }


        }else if (certification.Certification.equalsIgnoreCase("1")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.frontregister_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.frontregister_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.frontregister_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.frontregister_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.frontregister_5;

            }else {
                return R.drawable.front_register_gray;
            }
        }
        else if (certification.Certification.equalsIgnoreCase("2")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.friesposition_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.friesposition_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.friesposition_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.friesposition_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.friesposition_5;

            }else {
                return R.drawable.friesposition;

            }
        }
        else if (certification.Certification.equalsIgnoreCase("3")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.grill_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.grill_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.grill_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.grill_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.grill_5;

            }else {
                return R.drawable.grill;
            }
        }
        else if (certification.Certification.equalsIgnoreCase("4")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.puwregister_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.puwregister_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.puwregister_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")) {
                return R.drawable.puwregister_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.puwregister_5;

            }else {
                return R.drawable.puwregister;

            }

        }
        else if (certification.Certification.equalsIgnoreCase("5")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.sandwichposition_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.sandwichposition_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.sandwichposition_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.sandwichposition_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.sandwichposition_5;

            }else {
                return R.drawable.sandwichposition;
            }

        }
        else if (certification.Certification.equalsIgnoreCase("6")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.productcoord_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.productcoord_2;
            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.productcoord_3;
            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.productcoord_4;
            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.productcoord_5;
            }else {
                return R.drawable.productcoordinator_gray;
            }

        }
        else if (certification.Certification.equalsIgnoreCase("7")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.servicecoord_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.servicecoord_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.servicecoord_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.servicecoord_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.servicecoord_5;

            }else {
                return R.drawable.servicecoord;

            }


        }
        else if (certification.Certification.equalsIgnoreCase("8")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.bacon_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.bacon_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.bacon_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.bacon_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.bacon_5;

            }else {
                return R.drawable.bacon_gray;

            }


        }
        else if (certification.Certification.equalsIgnoreCase("9")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.beverages_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.beverage_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.beverage_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.beverage_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.beverage_5;

            }else {
                return R.drawable.beverage_gray;
            }

        }
        else if (certification.Certification.equalsIgnoreCase("10")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.chili_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.chili_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.chili_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.chili_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.chili_5;

            }else {
                return R.drawable.chili_gray;

            }

        }
        else if (certification.Certification.equalsIgnoreCase("11")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.foodprep_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.foodprep_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.foodprep_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.foodprep_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.foodprep_5;

            }else {
                return R.drawable.foodprep;
            }

        }
        else if (certification.Certification.equalsIgnoreCase("12")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.fries_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.fries_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.fries_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.fries_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.fries_5;

            }else {
                return R.drawable.fries_gray;

            }
        }
        else if (certification.Certification.equalsIgnoreCase("13")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.salad_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.salad_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.salad_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.salad_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.salad_5;

            }else {
                return R.drawable.salad;

            }

        }
        else if (certification.Certification.equalsIgnoreCase("14")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.sandwich_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.sandwich_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.sandwich_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.sandwich_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.sandwich_5;

            }else {
                return R.drawable.sandwich;

            }

        }
        else if (certification.Certification.equalsIgnoreCase("15")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.newproduct_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.newproduct_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.newproduct_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.newproduct_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.newproduct_5;

            }else {
                return R.drawable.newproduct;

            }

        }
        else if (certification.Certification.equalsIgnoreCase("16")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.welcome_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.welcome_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.welcome_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.welcome_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.welcome_5;

            }else {
                return R.drawable.orientation_gray;

            }

        }
        else if (certification.Certification.equalsIgnoreCase("17")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.foodsafety_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.foodsafety_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.foodsafety_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.foodsafety_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.foodsafety_5;

            }else {
                return R.drawable.foodsafety_gray;
            }

        }
        else if (certification.Certification.equalsIgnoreCase("18")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.cleaning_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.cleaning_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.cleaning_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.cleaning_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.cleaning_5;

            }else {
                return R.drawable.cleaning_gray;

            }

        }
        else if (certification.Certification.equalsIgnoreCase("19")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.cus_cur_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.cus_cur_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")) {
                return R.drawable.cus_cur_3;
            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.cus_cur_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.cus_cur_5;

            }else {
                return R.drawable.customer_courtesy_gray;

            }

        }
        else if (certification.Certification.equalsIgnoreCase("20")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.fryerops_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.fryerops_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.fryerops_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.fryerops_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.fryerops_5;

            }else {
                return R.drawable.fryerops;

            }

        }
        else if (certification.Certification.equalsIgnoreCase("21")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.grillops_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.grillops_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.grillops_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.grillops_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.grillops_5;

            }else {
                return R.drawable.grillops;

            }

        }
        else if (certification.Certification.equalsIgnoreCase("22")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.openclose_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.openclose_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.openclose_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.openclose_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.openclose_5;

            }else {
                return R.drawable.openclose;

            }
        }
        else if (certification.Certification.equalsIgnoreCase("23")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.linesetup_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.linesetup_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.linesetup_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.linesetup_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.linesetup_5;

            }else {
                return R.drawable.linesetup;

            }
        }

        else if (certification.Certification.equalsIgnoreCase("24")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.welcome_1;

            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.welcome_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.welcome_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.welcome_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.welcome_5;

            }else {
                return R.drawable.orientation_gray;

            }
        }

        else if (certification.Certification.equalsIgnoreCase("25")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.foodsafety_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.foodsafety_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.foodsafety_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.foodsafety_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.foodsafety_5;

            }else {
                return R.drawable.foodsafety_gray;
            }

        }

        else if (certification.Certification.equalsIgnoreCase("26")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.foodm_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.foodm_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.foodm_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.foodm_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.foodm_5;

            }else {
                return R.drawable.foodm;
            }

        }

        else if (certification.Certification.equalsIgnoreCase("27")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.bm_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.bm_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.bm_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.bm_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.bm_5;

            }else {
                return R.drawable.bm;
            }

        }

        else if (certification.Certification.equalsIgnoreCase("28")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.coaching_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.coaching_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.coaching_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.coaching_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.coaching_5;

            }else {
                return R.drawable.coaching;
            }

        }

        else if (certification.Certification.equalsIgnoreCase("29")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.talentmgmt_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.talentmgmt_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.talentmgmt_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.talentmgmt_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.talentmgmt_5;

            }else {
                return R.drawable.talentmgmt;
            }

        }

        else if (certification.Certification.equalsIgnoreCase("30")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.labormgmt_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.labormgmt_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.labormgmt_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.labormgmt_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.labormgmt_5;

            }else {
                return R.drawable.labormgmt;
            }

        }

        else if (certification.Certification.equalsIgnoreCase("31")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.opsleader_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.opsleader_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.opsleader_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.opsleader_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.opsleader_5;

            }else {
                return R.drawable.opsleader;
            }

        }

        else if (certification.Certification.equalsIgnoreCase("32")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.positioning_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.positioning_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.positioning_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.positioning_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.positioning_5;

            }else {
                return R.drawable.positioning;
            }

        }

        else if (certification.Certification.equalsIgnoreCase("33")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.training_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.training_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.training_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.training_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.training_5;

            }else {
                return R.drawable.training;
            }

        }

        else if (certification.Certification.equalsIgnoreCase("34")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.cei_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.cei_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.cei_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.cei_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.cei_5;

            }else {
                return R.drawable.cei;
            }

        }

        else if (certification.Certification.equalsIgnoreCase("35")){

            if (certification.StarRating.equalsIgnoreCase("1")){
                return R.drawable.voc_1;
            }
            else if (certification.StarRating.equalsIgnoreCase("2")){
                return R.drawable.voc_2;

            }
            else if (certification.StarRating.equalsIgnoreCase("3")){
                return R.drawable.voc_3;

            }
            else if (certification.StarRating.equalsIgnoreCase("4")){
                return R.drawable.voc_4;

            }
            else if (certification.StarRating.equalsIgnoreCase("5")){
                return R.drawable.voc_5;

            }else {
                return R.drawable.voc;
            }

        }

        return 0;

    }

    public static Calendar getDateWithDaypart(long reading,int effectiveUTCOffset ){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(reading));
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        calendar.add(Calendar.MINUTE,effectiveUTCOffset);

        Schema schema = Schema.getInstance();
        Calendar currentDate = Calendar.getInstance();
        currentDate.setTime(calendar.getTime());
        currentDate.setTimeZone(TimeZone.getTimeZone("UTC"));

        Dayparts daypart = schema.getDayparts().get(0);
        Calendar start = SCMTool.setCalendarDaypart(currentDate.getTime(),daypart.start);

        if (currentDate.getTime().compareTo(start.getTime())<0) {
            calendar.add(Calendar.DATE,-1);
        }

        return calendar;
    }

    public static View getNullView(Context context){
        LinearLayout linearLayout = new LinearLayout(context);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0,0);
        linearLayout.setLayoutParams(params);
        return linearLayout;
    }

    public static void getStoreUsers(final Site site, final ProgressDialog progressDialog){
        try {
            aPimmDataManager dataManager = aPimmDataManager.getInstance();
            dataManager.getStoreUsers(site.siteid, new OnCompleteListeners.getStoreUsersCallbackListener() {
                @Override
                public void getStoreUsersCallback(ArrayList<User> storeUsers, Error error) {
                    if (error == null) {
                        SCMDataManager.getInstance().setStoreUsers(SCMTool.ConvertUserToCustomUser(storeUsers));
                        if (progressDialog!=null){
                            progressDialog.dismiss();
                        }
                    }else {
                        if (progressDialog!=null){
                            progressDialog.dismiss();
                        }
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static boolean isUserHasAccessToEdit(){
        boolean access = false;
        User currentUser = SCMDataManager.getInstance().getUser();
        if (currentUser.roles!=null){
            ArrayList<CustomRole>customRoles = CustomRole.getAllCustomRole(currentUser.roles);
            if (customRoles.size()>=1){
                CustomRole customRole = customRoles.get(0);
                if (customRole.getStatus()>4){
                    return false;
                }else {
                    return true;
                }
            }
        }
        return false;
    }

    public static Calendar DaypartTimeToDate(String time,Date date){
        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        Calendar daypart = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

        try {
            Date d = dateFormat.parse(time);
            daypart.setTime(d);
            calendar.setTime(date);
            calendar.set(Calendar.HOUR,daypart.get(Calendar.HOUR));
            calendar.set(Calendar.MINUTE,daypart.get(Calendar.MINUTE));
            calendar.set(Calendar.SECOND,0);
            calendar.set(Calendar.MILLISECOND,0);
            calendar.set(Calendar.AM_PM,daypart.get(Calendar.AM_PM));
        }catch (Exception e){
            e.printStackTrace();
        }
        return calendar;
    }

    public static String getCurrentDaypart(){

        Site site = SCMDataManager.getInstance().getSelectedSite().getSite();
        ArrayList<Dayparts>dayparts = Schema.getInstance().getDayparts();

        SimpleDateFormat format = new SimpleDateFormat("hh:mm a");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        Calendar currentDate = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        currentDate.add(Calendar.MINUTE,site.effectiveUTCOffset);

        StringBuilder daypartName = new StringBuilder();


        Dayparts daypart1 = dayparts.get(0);
        Dayparts daypart6 = dayparts.get(dayparts.size()-1);

        Calendar daypart1_start = SCMTool.DaypartTimeToDate(daypart1.start,currentDate.getTime());

        Calendar daypart6_start = SCMTool.DaypartTimeToDate(daypart6.start,currentDate.getTime());
        Calendar daypart6_end = SCMTool.DaypartTimeToDate(daypart6.end,currentDate.getTime());

        if (currentDate.compareTo(daypart1_start)<0){
            daypartName.append(daypart6.title);
            daypartName.append(" ");
            daypartName.append(format.format(daypart6_start.getTime()));
            daypartName.append(" - ");
            daypartName.append(format.format(daypart6_end.getTime()));

            return daypartName.toString();
        }

        for(Dayparts daypart: dayparts){
            Calendar start = SCMTool.DaypartTimeToDate(daypart.start,currentDate.getTime());
            Calendar end = SCMTool.DaypartTimeToDate(daypart.end,currentDate.getTime());

            if (daypart.name.equalsIgnoreCase("6")){
                end.set(Calendar.AM_PM,Calendar.AM);
                end.add(Calendar.DATE,1);
            }

            if (SCMTool.isBetween(start.getTime(),end.getTime(),currentDate.getTime())){
                daypartName.append(daypart.title);
                daypartName.append(" ");
                daypartName.append(format.format(start.getTime()));
                daypartName.append(" - ");
                daypartName.append(format.format(end.getTime()));

                return daypartName.toString();
            }
        }

        return "--";

    }

    public static String getStringCurrentDaypart(){

        Site site = SCMDataManager.getInstance().getSelectedSite().getSite();

        ArrayList<Dayparts>dayparts = Schema.getInstance().getDayparts();

        SimpleDateFormat format = new SimpleDateFormat("hh:mm a");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        Calendar currentDate = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        currentDate.add(Calendar.MINUTE,site.effectiveUTCOffset);

        StringBuilder daypartName = new StringBuilder();

        Dayparts daypart1 = dayparts.get(0);
        Dayparts daypart6 = dayparts.get(dayparts.size()-1);
        Calendar daypart1_start = SCMTool.DaypartTimeToDate(daypart1.start,currentDate.getTime());


        if (currentDate.compareTo(daypart1_start)<0){
            return daypart6.name;
        }

        for(Dayparts daypart: dayparts){
            Calendar start = SCMTool.DaypartTimeToDate(daypart.start,currentDate.getTime());
            Calendar end = SCMTool.DaypartTimeToDate(daypart.end,currentDate.getTime());

            if (daypart.name.equalsIgnoreCase("6")){
                end.set(Calendar.AM_PM,Calendar.AM);
                end.add(Calendar.DATE,1);
            }

            if (SCMTool.isBetween(start.getTime(),end.getTime(),currentDate.getTime())){
                daypartName.append(daypart.title);
                daypartName.append(" ");
                daypartName.append(format.format(start.getTime()));
                daypartName.append(" - ");
                daypartName.append(format.format(end.getTime()));

                return daypart.name;
            }
        }

        return "1";

    }

    public static Date getCurrentDaypartStart(Date date){



        ArrayList<Dayparts>dayparts = Schema.getInstance().getDayparts();

        SimpleDateFormat format = new SimpleDateFormat("hh:mm a");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        Calendar currentDate = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        currentDate.setTime(date);

        StringBuilder daypartName = new StringBuilder();

        Dayparts daypart1 = dayparts.get(0);
        Dayparts daypart6 = dayparts.get(dayparts.size()-1);
        Calendar daypart1_start = SCMTool.DaypartTimeToDate(daypart1.start,currentDate.getTime());

        Calendar daypart6_start = SCMTool.DaypartTimeToDate(daypart6.start,currentDate.getTime());
        Calendar daypart6_end = SCMTool.DaypartTimeToDate(daypart6.end,currentDate.getTime());


        if (currentDate.compareTo(daypart1_start)<0){
            return daypart6_start.getTime();
        }

        for(Dayparts daypart: dayparts){
            Calendar start = SCMTool.DaypartTimeToDate(daypart.start,currentDate.getTime());
            Calendar end = SCMTool.DaypartTimeToDate(daypart.end,currentDate.getTime());

            if (daypart.name.equalsIgnoreCase("6")){
                end.set(Calendar.AM_PM,Calendar.AM);
                end.add(Calendar.DATE,1);
            }

            if (SCMTool.isBetween(start.getTime(),end.getTime(),currentDate.getTime())){
                daypartName.append(daypart.title);
                daypartName.append(" ");
                daypartName.append(format.format(start.getTime()));
                daypartName.append(" - ");
                daypartName.append(format.format(end.getTime()));

                return start.getTime();
            }
        }

        return null;

    }

    public static Date getCurrentDaypartEnd(Date date){

        ArrayList<Dayparts>dayparts = Schema.getInstance().getDayparts();

        SimpleDateFormat format = new SimpleDateFormat("hh:mm a");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        Calendar currentDate = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        currentDate.setTime(date);
        StringBuilder daypartName = new StringBuilder();

        Dayparts daypart1 = dayparts.get(0);
        Dayparts daypart6 = dayparts.get(dayparts.size()-1);
        Calendar daypart1_start = SCMTool.DaypartTimeToDate(daypart1.start,currentDate.getTime());

        Calendar daypart6_start = SCMTool.DaypartTimeToDate(daypart6.start,currentDate.getTime());
        Calendar daypart6_end = SCMTool.DaypartTimeToDate(daypart6.end,currentDate.getTime());
        daypart6_end.add(Calendar.MINUTE,-1);

        if (currentDate.compareTo(daypart1_start)<0){
            return daypart6_end.getTime();
        }

        for(Dayparts daypart: dayparts){
            Calendar start = SCMTool.DaypartTimeToDate(daypart.start,currentDate.getTime());
            Calendar end = SCMTool.DaypartTimeToDate(daypart.end,currentDate.getTime());
            end.add(Calendar.MINUTE,-1);

            if (daypart.name.equalsIgnoreCase("6")){
                end.set(Calendar.AM_PM,Calendar.AM);
                end.add(Calendar.DATE,1);
            }

            if (SCMTool.isBetween(start.getTime(),end.getTime(),currentDate.getTime())){
                daypartName.append(daypart.title);
                daypartName.append(" ");
                daypartName.append(format.format(start.getTime()));
                daypartName.append(" - ");
                daypartName.append(format.format(end.getTime()));

                return end.getTime();
            }
        }

        return null;

    }
    public static Dayparts getActualCurrentDaypartStart(){

        Site site = SCMDataManager.getInstance().getSelectedSite().getSite();

        ArrayList<Dayparts>dayparts = Schema.getInstance().getDayparts();

        SimpleDateFormat format = new SimpleDateFormat("hh:mm a");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        Calendar currentDate = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        currentDate.add(Calendar.MINUTE,site.effectiveUTCOffset);

        StringBuilder daypartName = new StringBuilder();

        Dayparts daypart1 = dayparts.get(0);
        Dayparts daypart6 = dayparts.get(dayparts.size()-1);
        Calendar daypart1_start = SCMTool.DaypartTimeToDate(daypart1.start,currentDate.getTime());

        Calendar daypart6_start = SCMTool.DaypartTimeToDate(daypart6.start,currentDate.getTime());
        Calendar daypart6_end = SCMTool.DaypartTimeToDate(daypart6.end,currentDate.getTime());


        if (currentDate.compareTo(daypart1_start)<0){
            return daypart6;
        }

        for(Dayparts daypart: dayparts){
            Calendar start = SCMTool.DaypartTimeToDate(daypart.start,currentDate.getTime());
            Calendar end = SCMTool.DaypartTimeToDate(daypart.end,currentDate.getTime());

            if (daypart.name.equalsIgnoreCase("6")){
                end.set(Calendar.AM_PM,Calendar.AM);
                end.add(Calendar.DATE,1);
            }

            if (SCMTool.isBetween(start.getTime(),end.getTime(),currentDate.getTime())){
                daypartName.append(daypart.title);
                daypartName.append(" ");
                daypartName.append(format.format(start.getTime()));
                daypartName.append(" - ");
                daypartName.append(format.format(end.getTime()));

                return daypart;
            }
        }

        return null;

    }


    public static Date getCurrentDaypartStart(){

        Site site = SCMDataManager.getInstance().getSelectedSite().getSite();

        ArrayList<Dayparts>dayparts = Schema.getInstance().getDayparts();

        SimpleDateFormat format = new SimpleDateFormat("hh:mm a");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        Calendar currentDate = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        currentDate.add(Calendar.MINUTE,site.effectiveUTCOffset);

        StringBuilder daypartName = new StringBuilder();

        Dayparts daypart1 = dayparts.get(0);
        Dayparts daypart6 = dayparts.get(dayparts.size()-1);
        Calendar daypart1_start = SCMTool.DaypartTimeToDate(daypart1.start,currentDate.getTime());

        Calendar daypart6_start = SCMTool.DaypartTimeToDate(daypart6.start,currentDate.getTime());
        Calendar daypart6_end = SCMTool.DaypartTimeToDate(daypart6.end,currentDate.getTime());


        if (currentDate.compareTo(daypart1_start)<0){
            return daypart6_start.getTime();
        }

        for(Dayparts daypart: dayparts){
            Calendar start = SCMTool.DaypartTimeToDate(daypart.start,currentDate.getTime());
            Calendar end = SCMTool.DaypartTimeToDate(daypart.end,currentDate.getTime());

            if (daypart.name.equalsIgnoreCase("6")){
                end.set(Calendar.AM_PM,Calendar.AM);
                end.add(Calendar.DATE,1);
            }

            if (SCMTool.isBetween(start.getTime(),end.getTime(),currentDate.getTime())){
                daypartName.append(daypart.title);
                daypartName.append(" ");
                daypartName.append(format.format(start.getTime()));
                daypartName.append(" - ");
                daypartName.append(format.format(end.getTime()));

                return start.getTime();
            }
        }

        return null;

    }

    public static Date getCurrentDaypartEnd(){

        Site site = SCMDataManager.getInstance().getSelectedSite().getSite();

        ArrayList<Dayparts>dayparts = Schema.getInstance().getDayparts();

        SimpleDateFormat format = new SimpleDateFormat("hh:mm a");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        Calendar currentDate = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        currentDate.add(Calendar.MINUTE,site.effectiveUTCOffset);

        StringBuilder daypartName = new StringBuilder();

        Dayparts daypart1 = dayparts.get(0);
        Dayparts daypart6 = dayparts.get(dayparts.size()-1);
        Calendar daypart1_start = SCMTool.DaypartTimeToDate(daypart1.start,currentDate.getTime());

        Calendar daypart6_start = SCMTool.DaypartTimeToDate(daypart6.start,currentDate.getTime());
        Calendar daypart6_end = SCMTool.DaypartTimeToDate(daypart6.end,currentDate.getTime());
        daypart6_end.add(Calendar.MINUTE,-1);

        if (currentDate.compareTo(daypart1_start)<0){
            return daypart6_end.getTime();
        }

        for(Dayparts daypart: dayparts){
            Calendar start = SCMTool.DaypartTimeToDate(daypart.start,currentDate.getTime());
            Calendar end = SCMTool.DaypartTimeToDate(daypart.end,currentDate.getTime());
            end.add(Calendar.MINUTE,-1);

            if (daypart.name.equalsIgnoreCase("6")){
                end.set(Calendar.AM_PM,Calendar.AM);
                end.add(Calendar.DATE,1);
            }

            if (SCMTool.isBetween(start.getTime(),end.getTime(),currentDate.getTime())){
                daypartName.append(daypart.title);
                daypartName.append(" ");
                daypartName.append(format.format(start.getTime()));
                daypartName.append(" - ");
                daypartName.append(format.format(end.getTime()));

                return end.getTime();
            }
        }

        return null;

    }
    public static Calendar ISOTimeToDate(String time,Date date){
        DateFormat dateFormat = new SimpleDateFormat("H:mm");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        Calendar daypart = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

        try {
            Date d = dateFormat.parse(time);
            daypart.setTime(d);
            calendar.setTime(date);
            calendar.set(Calendar.HOUR,daypart.get(Calendar.HOUR));
            calendar.set(Calendar.MINUTE,daypart.get(Calendar.MINUTE));
            calendar.set(Calendar.SECOND,0);
            calendar.set(Calendar.MILLISECOND,0);
            calendar.set(Calendar.AM_PM,daypart.get(Calendar.AM_PM));
        }catch (Exception e){
            e.printStackTrace();
        }
        return calendar;
    }
    public static Calendar ISOTimeToDatePlus1(String time,Date date){
        DateFormat dateFormat = new SimpleDateFormat("H:mm");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        Calendar daypart = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

        try {
            Date d = dateFormat.parse(time);
            daypart.setTime(d);
            calendar.setTime(date);
            calendar.set(Calendar.HOUR,daypart.get(Calendar.HOUR));
            calendar.set(Calendar.MINUTE,daypart.get(Calendar.MINUTE));
            calendar.set(Calendar.SECOND,0);
            calendar.set(Calendar.MILLISECOND,0);
            calendar.set(Calendar.AM_PM,daypart.get(Calendar.AM_PM));
            calendar.add(Calendar.DATE,1);
        }catch (Exception e){
            e.printStackTrace();
        }
        return calendar;
    }

    public static boolean getDiningRoomStatusPerDaypart(String daypart){
        SiteSettings siteSettings = SCMDataManager.getInstance().getSiteSettings();
        if (siteSettings!=null){
            if (siteSettings.settings!=null){
                String Name = "SMS:DiningRoomDp"+daypart;
                for (PropertyValue propertyValue : siteSettings.settings){
                    if (propertyValue.property.equalsIgnoreCase(Name)){
                        System.out.println("SiteSetting -- "+propertyValue.property + " : "+propertyValue.value);
                        return propertyValue.value.equalsIgnoreCase("1");
                    }
                }
            }
        }
        return true;
    }

    public static Calendar getCurrentDateWithTimeZone(){
        Site site = SCMDataManager.getInstance().getSelectedSite().getSite();
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.add(Calendar.MINUTE,site.effectiveUTCOffset);

        return calendar;
    }

    public static int getSiteSettingsConfigurationForCurrentDay(String daypart,String Day){
        String configName = "Config:staffingPeakHourRequiredDp" + daypart + "_" + Day.toLowerCase();

        ArrayList<SiteSettingsConfiguration>configurations = SCMDataManager.getInstance().getSettingsConfigurations();
        if (configurations!=null){
            for (SiteSettingsConfiguration configuration :configurations){
                if (configuration.name.equalsIgnoreCase(configName)){
                    System.out.println("SELECTED : "+configName+" : "+configuration.value);
                    int value = Integer.parseInt(configuration.value);
                    if (value!=0){
                        return value;
                    }
                    return 4;
                }
            }
        }
        return 0;
    }
}
