package com.procuro.androidscm;

import java.util.Date;

public class Daypart {

    int int_name;
    String string_name;
    Date start_time;
    Date end_time;

    public Daypart(int int_name, String string_name, Date start_time, Date end_time) {
        this.int_name = int_name;
        this.string_name = string_name;
        this.start_time = start_time;
        this.end_time = end_time;
    }

    public int getInt_name() {
        return int_name;
    }

    public void setInt_name(int int_name) {
        this.int_name = int_name;
    }

    public String getString_name() {
        return string_name;
    }

    public void setString_name(String string_name) {
        this.string_name = string_name;
    }

    public Date getStart_time() {
        return start_time;
    }

    public void setStart_time(Date start_time) {
        this.start_time = start_time;
    }

    public Date getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Date end_time) {
        this.end_time = end_time;
    }
}
