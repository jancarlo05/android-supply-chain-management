package com.procuro.androidscm;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.fragment.app.DialogFragment;

import com.procuro.androidscm.Restaurant.Login.Login;

import java.util.Objects;

public class Logout_Confirmation_DialogFragment extends DialogFragment {

    public static Logout_Confirmation_DialogFragment newInstance() {

        return new Logout_Confirmation_DialogFragment();
    }
    @Override
        public int getTheme() {
        return R.style.slide_up_down;
        }

    @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.logout_message, container, false);

//                    int height = (int)(getResources().getDisplayMetrics().heightPixels*0.90);
//                    int width = 320;
//
        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.90);
        int height = (int)(getResources().getDisplayMetrics().heightPixels*0.60);


        Objects.requireNonNull(getDialog().getWindow()).setLayout(width, height);
        getDialog().setCanceledOnTouchOutside(false);
        view.setFocusableInTouchMode(true);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button cancel = view.findViewById(R.id.cancel);
        Button logout = view.findViewById(R.id.logout);


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Login.class);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Objects.requireNonNull(getActivity()).startActivity(intent);
                dismiss();
                getActivity().finish();
            }
        });

        return view; }

    private static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = (int)(getResources().getDisplayMetrics().widthPixels*0.90);
        Window window = Objects.requireNonNull(getDialog()).getWindow();
        assert window != null;
        window.setLayout(width, LinearLayout.LayoutParams.WRAP_CONTENT);
    }

}
