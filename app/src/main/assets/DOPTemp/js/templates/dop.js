(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['dop'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression, alias5=container.lambda;

  return "<!-- PAGE 1 - SANITATION CHECKS & SHELL LIFE -->\n<div class=\"dv-page-container col-md-12 col-xs-12\">\n"
    + ((stack1 = container.invokePartial(partials.header,depth0,{"name":"header","hash":{"class":"dv-first"},"data":data,"indent":"  ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "  \n  <!-- KEY DRIVER -->\n  <div class=\"dv-form col-md-12 col-xs-12 col-md-12 col-xs-12\">\n    <h6 class=\"text-bold\">Key Driver and Associated Play(s)</h6>\n    <table class=\"table table-striped table-bordered col-md-12 col-xs-12 tbl-forms\">\n      <tbody>\n        <tr>\n          <td class=\"width-20 td-title-orange\">TASTE</td>\n          <td class=\"width-20 td-title-orange\">FRIENDLINESS</td>\n          <td class=\"width-20 td-title-orange\">SPEED</td>\n          <td class=\"width-20 td-title-orange\">ACCURACY</td>\n          <td class=\"width-20 td-title-orange\">CLEANLINESS</td>\n        </tr>\n        <tr>\n          <td>&nbsp;"
    + alias4(((helper = (helper = helpers.tastePlayValue || (depth0 != null ? depth0.tastePlayValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tastePlayValue","hash":{},"data":data}) : helper)))
    + "</td>\n          <td>&nbsp;"
    + alias4(((helper = (helper = helpers.friendlinessPLayValue || (depth0 != null ? depth0.friendlinessPLayValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"friendlinessPLayValue","hash":{},"data":data}) : helper)))
    + "</td>\n          <td>&nbsp;"
    + alias4(((helper = (helper = helpers.speedPlayValue || (depth0 != null ? depth0.speedPlayValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"speedPlayValue","hash":{},"data":data}) : helper)))
    + "</td>\n          <td>&nbsp;"
    + alias4(((helper = (helper = helpers.accuracyPlayValue || (depth0 != null ? depth0.accuracyPlayValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"accuracyPlayValue","hash":{},"data":data}) : helper)))
    + "</td>\n          <td>&nbsp;"
    + alias4(((helper = (helper = helpers.cleanlinessPlayValue || (depth0 != null ? depth0.cleanlinessPlayValue : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"cleanlinessPlayValue","hash":{},"data":data}) : helper)))
    + "</td>\n        </tr>\n      </tbody>\n    </table>\n\n    <table class=\"tblnoborder tbl-dt\">\n      <tbody>\n        <tr>\n          <td colspan=\"2\" class=\"width-40 text-left text-bold padding-left-10\">F2 - Dining Table</td>\n          <td class=\"width-20 text-bold\">System</td>\n          <td class=\"width-20 text-bold\">Rush Ready</td>\n        </tr>\n      </tbody>\n    </table>\n  </div>\n\n  <div class=\"dv-form col-md-12 col-xs-12 col-md-12 col-xs-12\">\n    <div class=\"title col-md-12 col-xs-12\"></div>\n    <table class=\"table table-striped table-bordered col-md-12 col-xs-12 tbl-forms\">\n      <thead>\n          <tr>\n            <td colspan=\"2\" class=\"width-40 text-left text-bold dark-shaded\"><!-- Overall Score: --> </td>\n            <td colspan=\"2\" class=\"width-30 text-left text-bold dark-shaded\"> </td> <!-- Forecasting &amp; Staffing: -->\n            <td colspan=\"2\" class=\"width-30 text-left text-bold dark-shaded\">ZOD: "
    + alias4(((helper = (helper = helpers.ZOD || (depth0 != null ? depth0.ZOD : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ZOD","hash":{},"data":data}) : helper)))
    + "</td>\n          </tr>\n          <tr>\n            <td colspan=\"2\">Huddle Team</td>\n            <td colspan=\"2\">Daypart 1 - 3</td>\n            <td colspan=\"2\">Daypart 4 - 6</td>\n          </tr>\n      </thead>\n      <tbody>\n        <tr>\n          <td colspan=\"2\" class=\"text-left\">Ops Leader</td>\n          <td colspan=\"2\">"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$forecastStaff_opsLeader1to3 : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n          <td colspan=\"2\">"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$forecastStaff_opsLeader4to6 : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n        </tr>\n        <tr>\n          <td colspan=\"2\" class=\"text-left\">Support Manager</td>\n          <td colspan=\"2\">"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$forecastStaff_supportManagerOne1to3 : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n          <td colspan=\"2\">"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$forecastStaff_supportManagerOne4to6 : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n        </tr>\n        <tr>\n          <td colspan=\"2\" class=\"text-left\">Support Manager</td>\n          <td colspan=\"2\">"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$forecastStaff_supportManagerTwo1to3 : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n          <td colspan=\"2\">"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$forecastStaff_supportManagerTwo4to6 : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n        </tr>\n        <tr>\n          <td colspan=\"6\" class=\"dark-shaded text-bold\"> Forecasting</td>\n        </tr>\n        <tr>\n          <td class=\"width-15 text-bold\">Forecast</td>\n          <td class=\"width-15 text-bold\">Projected</td>\n          <td class=\"width-15 text-bold\">Actual</td>\n          <td class=\"width-15 text-bold\">(+/-)</td>\n          <td class=\"width-25 text-bold\">Reason</td>\n        </tr>\n        <tr>\n          <td class=\"text-left\">Daily Forecast</td>\n          <td>"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$forecastStaff_forecastDailyProjected : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n          <td>"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$forecastStaff_forecastDailyActual : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n          <td>"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$forecastStaff_forecastDailyVariance : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n          <td>"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$forecastStaff_forecastDailyReason : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n        </tr>\n        <tr>\n          <td class=\"text-left\">Peak Hour</td>\n          <td>"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$forecastStaff_forecastProjected : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n          <td>"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$forecastStaff_forecastActual : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n          <td>"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$forecastStaff_forecastVariance : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n          <td>"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$forecastStaff_forecastReason : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n        </tr>\n        <tr>\n          <td colspan=\"5\" class=\"dark-shaded text-bold\">Staffing</td>\n        </tr>\n        <tr>\n          <td class=\"width-15 text-bold\">Staffing</td>\n          <td class=\"width-15 text-bold\">Required</td>\n          <td class=\"width-15 text-bold\">Actual</td>\n          <td class=\"width-15 text-bold\">(+/-)</td>\n          <td class=\"width-25 text-bold dark-shaded\"></td>\n        </tr>\n        <tr>\n          <td class=\"text-left\">Peak Hour</td>\n          <td>"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$forecastStaff_staffingPeakHourRequired : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n          <td>"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$forecastStaff_staffingPeakHourActual : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n          <td>"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$forecastStaff_staffingPeakHourVariance : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n          <td class=\"dark-shaded\"></td>\n        </tr>\n      </tbody>\n    </table>\n\n    <!-- Service Focus -->\n    <table class=\"table table-striped table-bordered col-md-12 col-xs-12 tbl-forms\">\n      <thead>\n        <tr>\n          <td colspan=\"3\" class=\"text-left text-bold dark-shaded\">Drive-thru Speed of Service Focus</td>\n          <td class=\"text-right text-bold dark-shaded\">SoS Time</td>\n          <td class=\"text-right text-bold dark-shaded\">Record Cars: "
    + alias4(((helper = (helper = helpers.RecordCars || (depth0 != null ? depth0.RecordCars : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"RecordCars","hash":{},"data":data}) : helper)))
    + "</td>\n        </tr>\n        <tr>\n          <td class=\"width-20\">Goal / Actual</td>\n          <td class=\"width-20\">Greet</td>\n          <td class=\"width-20\">Window</td>\n          <td class=\"width-20\">Total Time</td>\n          <td class=\"width-20\"># Cars</td>\n        </tr>\n      </thead>\n      <tbody>\n        <tr>\n          <td class=\"noPadding\">\n            <table class=\"tblnoborder\">\n              <tbody>\n                <tr>\n                  <td rowspan=\"2\">Daypart 2</td>\n                  <td>G</td>\n                </tr>\n                <tr>\n                  <td>A</td>\n                </tr>\n              </tbody>\n            </table>\n          </td>\n          <td class=\"noPadding\">\n            <table class=\"tblnoborder\">\n              <tbody>\n                <tr>\n                  <td class=\"border-bottom-1\">&nbsp;"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$speedOfService_greetDp2GoalGreet : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n                </tr>\n                <tr>\n                  <td>&nbsp;"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$speedOfService_greetDp2ActualGreet : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n                </tr>\n              </tbody>\n            </table>\n          </td>\n          <td class=\"noPadding\">\n            <table class=\"tblnoborder\">\n              <tbody>\n                <tr>\n                  <td class=\"border-bottom-1\">&nbsp;"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$speedOfService_greetDp2GoalWindow : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n                </tr>\n                <tr>\n                  <td>&nbsp;"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$speedOfService_greetDp2ActualWindow : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n                </tr>\n              </tbody>\n            </table>\n          </td>\n          <td class=\"noPadding\">\n            <table class=\"tblnoborder\">\n              <tbody>\n                <tr>\n                  <td class=\"border-bottom-1\">&nbsp;"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$speedOfService_greetDp2GoalTotalTime : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n                </tr>\n                <tr>\n                  <td>&nbsp;"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$speedOfService_greetDp2ActualTotalTime : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n                </tr>\n              </tbody>\n            </table>\n          </td>\n          <td class=\"noPadding\">\n            <table class=\"tblnoborder\">\n              <tbody>\n                <tr>\n                  <td class=\"border-bottom-1\">&nbsp;"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$speedOfService_greetDp2GoalCars : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n                </tr>\n                <tr>\n                  <td>&nbsp;"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$speedOfService_greetDp2ActualCars : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n                </tr>\n              </tbody>\n            </table>\n          </td>\n        </tr>\n        <tr>\n          <td class=\"noPadding\">\n            <table class=\"tblnoborder\">\n              <tbody>\n                <tr>\n                  <td rowspan=\"2\">Daypart 3</td>\n                  <td>G</td>\n                </tr>\n                <tr>\n                  <td>A</td>\n                </tr>\n              </tbody>\n            </table>\n          </td>\n          <td class=\"noPadding\">\n            <table class=\"tblnoborder\">\n              <tbody>\n                <tr>\n                  <td class=\"border-bottom-1\">&nbsp;"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$speedOfService_greetDp3GoalGreet : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n                </tr>\n                <tr>\n                  <td>&nbsp;"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$speedOfService_greetDp3ActualGreet : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n                </tr>\n              </tbody>\n            </table>\n          </td>\n          <td class=\"noPadding\">\n            <table class=\"tblnoborder\">\n              <tbody>\n                <tr>\n                  <td class=\"border-bottom-1\">&nbsp;"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$speedOfService_greetDp3GoalWindow : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n                </tr>\n                <tr>\n                  <td>&nbsp;"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$speedOfService_greetDp3ActualWindow : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n                </tr>\n              </tbody>\n            </table>\n          </td>\n          <td class=\"noPadding\">\n            <table class=\"tblnoborder\">\n              <tbody>\n                <tr>\n                  <td class=\"border-bottom-1\">&nbsp;"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$speedOfService_greetDp3GoalTotalTime : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n                </tr>\n                <tr>\n                  <td>&nbsp;"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$speedOfService_greetDp3ActualTotalTime : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n                </tr>\n              </tbody>\n            </table>\n          </td>\n          <td class=\"noPadding\">\n            <table class=\"tblnoborder\">\n              <tbody>\n                <tr>\n                  <td class=\"border-bottom-1\">&nbsp;"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$speedOfService_greetDp3GoalCars : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n                </tr>\n                <tr>\n                  <td>&nbsp;"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$speedOfService_greetDp3ActualCars : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n                </tr>\n              </tbody>\n            </table>\n          </td>\n        </tr>\n        <tr>\n          <td class=\"noPadding\">\n            <table class=\"tblnoborder\">\n              <tbody>\n                <tr>\n                  <td rowspan=\"2\">Daypart 4</td>\n                  <td>G</td>\n                </tr>\n                <tr>\n                  <td>A</td>\n                </tr>\n              </tbody>\n            </table>\n          </td>\n          <td class=\"noPadding\">\n            <table class=\"tblnoborder\">\n              <tbody>\n                <tr>\n                  <td class=\"border-bottom-1\">&nbsp;"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$speedOfService_greetDp4GoalGreet : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n                </tr>\n                <tr>\n                  <td>&nbsp;"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$speedOfService_greetDp4ActualGreet : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n                </tr>\n              </tbody>\n            </table>\n          </td>\n          <td class=\"noPadding\">\n            <table class=\"tblnoborder\">\n              <tbody>\n                <tr>\n                  <td class=\"border-bottom-1\">&nbsp;"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$speedOfService_greetDp4GoalWindow : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n                </tr>\n                <tr>\n                  <td>&nbsp;"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$speedOfService_greetDp4ActualWindow : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n                </tr>\n              </tbody>\n            </table>\n          </td>\n          <td class=\"noPadding\">\n            <table class=\"tblnoborder\">\n              <tbody>\n                <tr>\n                  <td class=\"border-bottom-1\">&nbsp;"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$speedOfService_greetDp4GoalTotalTime : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n                </tr>\n                <tr>\n                  <td>&nbsp;"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$speedOfService_greetDp4ActualTotalTime : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n                </tr>\n              </tbody>\n            </table>\n          </td>\n          <td class=\"noPadding\">\n            <table class=\"tblnoborder\">\n              <tbody>\n                <tr>\n                  <td class=\"border-bottom-1\">&nbsp;"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$speedOfService_greetDp4GoalCars : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n                </tr>\n                <tr>\n                  <td>&nbsp;"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$speedOfService_greetDp4ActualCars : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n                </tr>\n              </tbody>\n            </table>\n          </td>\n        </tr>\n      </tbody>\n    </table>\n\n    <!-- Huddle Satisfaction Commitments -->\n    <table class=\"table table-striped table-bordered col-md-12 col-xs-12 tbl-forms\">\n      <thead>\n        <tr>\n          <td colspan=\"2\" class=\"width-80\">Huddle / Customer Satisfaction Commitments</td>\n          <td class=\"width-20\">Achieved</td>\n        </tr>\n      </thead>\n      <tbody>\n        <tr>\n          <td class=\"width-20 text-left text-bold\">Ops Leader</td>\n          <td class=\"width-70 text-left\">"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$customerSatisfaction_OpsLeader : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n          <td class=\"width-10\">\n            <img src=\"images/"
    + alias4((helpers.isAchieved || (depth0 && depth0.isAchieved) || alias2).call(alias1,((stack1 = (depth0 != null ? depth0.dp1$customerSatisfaction_OpsLeaderAchieved : depth0)) != null ? stack1.reading : stack1),{"name":"isAchieved","hash":{},"data":data}))
    + "\" onerror=\"this.onerror=null;this.style.display='none';\" width=\"16\" />\n          </td>\n        </tr>\n        <tr>\n          <td class=\"text-left text-bold\">Support Mngr.</td>\n          <td class=\"text-left\">"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$customerSatisfaction_SupportMgr1 : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n          <td>\n            <img src=\"images/"
    + alias4((helpers.isAchieved || (depth0 && depth0.isAchieved) || alias2).call(alias1,((stack1 = (depth0 != null ? depth0.dp1$customerSatisfaction_SupportMgr1Achieved : depth0)) != null ? stack1.reading : stack1),{"name":"isAchieved","hash":{},"data":data}))
    + "\" onerror=\"this.onerror=null;this.style.display='none';\" width=\"16\" />\n          </td>\n        </tr>\n        <tr>\n          <td class=\"text-left text-bold\">Support Mngr.</td>\n          <td class=\"text-left\">"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$customerSatisfaction_SupportMgr2 : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n          <td>\n            <img src=\"images/"
    + alias4((helpers.isAchieved || (depth0 && depth0.isAchieved) || alias2).call(alias1,((stack1 = (depth0 != null ? depth0.dp1$customerSatisfaction_SupportMgr2Achieved : depth0)) != null ? stack1.reading : stack1),{"name":"isAchieved","hash":{},"data":data}))
    + "\" onerror=\"this.onerror=null;this.style.display='none';\" width=\"16\" />\n          </td>\n        </tr>\n        <tr>\n          <td class=\"text-left text-bold\">DR Team</td>\n          <td class=\"text-left\">"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$customerSatisfaction_drTeam : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n          <td>\n            <img src=\"images/"
    + alias4((helpers.isAchieved || (depth0 && depth0.isAchieved) || alias2).call(alias1,((stack1 = (depth0 != null ? depth0.dp1$customerSatisfaction_drTeamAchieved : depth0)) != null ? stack1.reading : stack1),{"name":"isAchieved","hash":{},"data":data}))
    + "\" onerror=\"this.onerror=null;this.style.display='none';\" width=\"16\" />\n          </td>\n        </tr>\n        <tr>\n          <td class=\"text-left text-bold\">PUW Team</td>\n          <td class=\"text-left\">"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$customerSatisfaction_PUWTeam : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n          <td>\n            <img src=\"images/"
    + alias4((helpers.isAchieved || (depth0 && depth0.isAchieved) || alias2).call(alias1,((stack1 = (depth0 != null ? depth0.dp1$customerSatisfaction_PUWTeamAchieved : depth0)) != null ? stack1.reading : stack1),{"name":"isAchieved","hash":{},"data":data}))
    + "\" onerror=\"this.onerror=null;this.style.display='none';\" width=\"16\" />\n          </td>\n        </tr>\n        <tr>\n          <td class=\"text-left text-bold\">Fries</td>\n          <td class=\"text-left\">"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$customerSatisfaction_Fries : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n          <td>\n            <img src=\"images/"
    + alias4((helpers.isAchieved || (depth0 && depth0.isAchieved) || alias2).call(alias1,((stack1 = (depth0 != null ? depth0.dp1$customerSatisfaction_FriesAchieved : depth0)) != null ? stack1.reading : stack1),{"name":"isAchieved","hash":{},"data":data}))
    + "\" onerror=\"this.onerror=null;this.style.display='none';\" width=\"16\" />\n          </td>\n        </tr>\n        <tr>\n          <td class=\"text-left text-bold\">Grill</td>\n          <td class=\"text-left\">"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$customerSatisfaction_Grill : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n          <td>\n            <img src=\"images/"
    + alias4((helpers.isAchieved || (depth0 && depth0.isAchieved) || alias2).call(alias1,((stack1 = (depth0 != null ? depth0.dp1$customerSatisfaction_GrillAchieved : depth0)) != null ? stack1.reading : stack1),{"name":"isAchieved","hash":{},"data":data}))
    + "\" onerror=\"this.onerror=null;this.style.display='none';\" width=\"16\" />\n          </td>\n        </tr>\n        <tr>\n          <td class=\"text-left text-bold\">Sandwiches</td>\n          <td class=\"text-left\">"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$customerSatisfaction_Sandwiches : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n          <td>\n            <img src=\"images/"
    + alias4((helpers.isAchieved || (depth0 && depth0.isAchieved) || alias2).call(alias1,((stack1 = (depth0 != null ? depth0.dp1$customerSatisfaction_SandwichesAchieved : depth0)) != null ? stack1.reading : stack1),{"name":"isAchieved","hash":{},"data":data}))
    + "\" onerror=\"this.onerror=null;this.style.display='none';\" width=\"16\" />\n          </td>\n        </tr>\n        <tr>\n          <td class=\"text-left text-bold\">LTO's</td>\n          <td class=\"text-left\">"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dp1$customerSatisfaction_LTO : depth0)) != null ? stack1.reading : stack1), depth0))
    + "</td>\n          <td>\n            <img src=\"images/"
    + alias4((helpers.isAchieved || (depth0 && depth0.isAchieved) || alias2).call(alias1,(depth0 != null ? depth0.isAchieved : depth0),((stack1 = (depth0 != null ? depth0.dp1$customerSatisfaction_LTOAchieved : depth0)) != null ? stack1.reading : stack1),{"name":"isAchieved","hash":{},"data":data}))
    + "\" onerror=\"this.onerror=null;this.style.display='none';\" width=\"16\" />\n          </td>\n        </tr>\n      </tbody>\n    </table>\n    \n    <div class=\"col-md-12 col-xs-12 dv-comments\"><span class=\"span-comments\">Comments: "
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.dop_note : depth0)) != null ? stack1.value : stack1), depth0))
    + "</span></div>\n  </div>\n\n    <!-- General Manager Signature -->\n      <div class=\"row col-md-12 col-xs-12 dv-gm\">\n          <div class=\"col-md-9 col-xs-7\"></div>\n          <div class=\"col-md-3 col-xs-3 text-center pull-right width-50\">\n              <span class=\"span-gm-label\">General Manager:</span>\n              <div class=\"dv-gm-img\">\n                  <img class=\"margin-right0 img-gm\" src=\"data:image/png;base64,"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.managerVerified : depth0)) != null ? stack1.managerSignature : stack1), depth0))
    + "\" onerror=\"this.onerror=null;this.src='images/not_available.png';\" width=\"100\"/>\n              </div>\n          </div>\n      </div>\n  </div>\n</div>";
},"usePartial":true,"useData":true});
})();