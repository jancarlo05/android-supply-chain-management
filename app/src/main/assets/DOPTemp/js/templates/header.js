(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['header'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<!-- HEADER -->\n<div class=\"col-md-12 col-xs-12 padding-bottom-10 section-header "
    + alias4(((helper = (helper = helpers["class"] || (depth0 != null ? depth0["class"] : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"class","hash":{},"data":data}) : helper)))
    + "\">\n    <div class=\"dv-logo flex col-md-2 col-xs-2\">\n        <div class=\"vertical\">\n            <img src=\"images/"
    + alias4(((helper = (helper = helpers.logo || (depth0 != null ? depth0.logo : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"logo","hash":{},"data":data}) : helper)))
    + ".png\" class=\"logo-left\" onerror=\"this.onerror=null;this.style='display:none;';\">\n            <!-- <img src=\"images/logos/"
    + alias4(((helper = (helper = helpers.statusIcon || (depth0 != null ? depth0.statusIcon : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"statusIcon","hash":{},"data":data}) : helper)))
    + ".png\" class=\"logo-left\" onerror=\"this.onerror=null;this.src='images/logos/generic.jpg';\"> -->\n        </div>\n    </div>\n    <div class=\"col-md-8 col-xs-8 text-center\">\n        <div class=\"header-title\">"
    + alias4(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data}) : helper)))
    + "</div>\n        <div class=\"subtitle-orange\">"
    + alias4(((helper = (helper = helpers.storeName || (depth0 != null ? depth0.storeName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"storeName","hash":{},"data":data}) : helper)))
    + "</div>\n        <div class=\"subtitle\">"
    + alias4(((helper = (helper = helpers.date || (depth0 != null ? depth0.date : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"date","hash":{},"data":data}) : helper)))
    + "</div>\n    </div>\n    <div class=\"dv-logo col-md-2 col-xs-2\">\n        <img class=\"logo-right\" src=\"images/PIMM%20Logo.png\">\n    </div>\n\n    <div class=\"col-md-12 col-xs-12 divider\" style=\"margin-top: 10px;\"></div>\n</div>";
},"useData":true});
})();