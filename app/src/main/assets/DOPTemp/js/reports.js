/*
* Created by Lian Lagundimao
* Created at July 25, 2016
*/

/*
 * Created By Lian Lagundimao
 * July 30, 2015
 */

//  $(document).ready(function() {
    // loadBlankReport({});
    // loadReport({title: 'Daily Operations Plan', storeName: '01174 - 270 AND SAWMILL', date: 'January 11, 2019'}, 'dop');
//  });

 function loadBlankReport(params) {
    try {
        initHelpers();
        var days = ['wednesday', 'thursday', 'friday', 'saturday', 'sunday', 'monday', 'tuesday'];
        var data = {
            dayTitle: ''
        };

        Handlebars.registerPartial('forms', Handlebars.templates['forms'])
        Handlebars.registerPartial('header', Handlebars.templates['header'])
        Handlebars.registerPartial('titlepage', Handlebars.templates['titlepage'])
        
        for (var i=0; i<days.length; i++) {
            data['dayTitle'] = days[i].toUpperCase();
            var html = Handlebars.templates[days[i]](data);

            $("body").append(html);
        }

        return 'success';
    } catch (e) {
        return '[loadBlankReport] Error: ' + e;
    }
 }


function loadReport(params, template) {
    try {
        initHelpers();
        
        data = {
            title: params.title,
            storeName: params.storeName,
            logo: params.logo,
            date: params.date,
            RecordCars: params.RecordCars,
            managerVerified: params.managerVerified,
            LTO: params.LTO,
            tastePlayValue: params.tastePlayValue,
            speedPlayValue: params.speedPlayValue,
            accuracyPlayValue: params.accuracyPlayValue,
            friendlinessPLayValue: params.friendlinessPLayValue,
            cleanlinessPlayValue: params.cleanlinessPlayValue,
            ZOD: params.ZOD
        };


        // tastePlayValue, speedPlayValue, accuracyPlayValue, friendlinessPLayValue, *cleanlinessPlayValue

        var itemCount = 0;
        var initialsCount = 0;
        var opsLeadersCount = 0;
        var commentsCount = 0;

        if (params.items != undefined) {
           itemCount = params.items.length;
        }

        if (params.initials != undefined) {
           initialsCount = params.initials.length;
        }
        
        if (params.opsLeaders != undefined) {
           opsLeadersCount = params.opsLeaders.length;
        }
        
        if (params.comments != undefined) {
           commentsCount = params.comments.length;
        }
        
        
        if (itemCount > 0) {
            for(var i=0; i < itemCount; i++) {
                data[params.items[i].itemName] = {
                    reading: decodeHtml(params.items[i].reading),
                    correctiveAction: params.items[i].correctiveAction,
                    date: params.items[i].dateTaken,
                    time: getTimeFromTimestamp(params.items[i].dateTaken),
                    textDate: getDateFromTimestamp(params.items[i].dateTaken),
                    textReading: params.items[i].textReading
                }
            }
        }
        
        if(initialsCount > 0) {
            for(var i=0; i < initialsCount; i++) {
                data[params.initials[i].initialKey] = {
                    value: params.initials[i].initialValue
                }
            }
        }
        
        if(opsLeadersCount > 0) {
            for(var i=0; i < opsLeadersCount; i++) {
                data[params.opsLeaders[i].opsLeaderKey] = {
                    value: params.opsLeaders[i].opsLeaderValue
                }
            }
        }
        
        if(commentsCount > 0) {
            for(var i=0; i < commentsCount; i++) {
                data[params.comments[i].commentKey] = {
                    value: params.comments[i].commentValue
                }
            }
        }

        Handlebars.registerPartial('header', Handlebars.templates['header']);
        
        var html = Handlebars.templates[template](data);

        $("body").append(html);

        // BUILD LTO Table
        if (data.LTO) {
            var tds = '';
            for (var i=0; i<data.LTO.dayparts.length; i++) {
                tds += ' <td>'+data.LTO.dayparts[i]+'</td>';
            }

            // draw header
            $('.tbl-LTO thead').append('<tr>'
                + ' <td class="width-20">Products</td>'
                + ' <td class="width-120">Standard</td>'
                + tds
                + ' <td class="width-20">Corrective Action</td>'
                + '</tr>'
            );

            for (var i=0; i<data.LTO.products.length; i++) {
                product = data.LTO.products[i];
                var values = '';
                for (var v=0; v<product.values.length; v++) {
                    values += '<td class="text-center">'+ product.values[v] + '</td>';
                }
                
                $('.tbl-LTO tbody').append('<tr>'
                + ' <td>'+ product.productName +'</td>'
                + ' <td class="text-center">'+ product.standard +'</td>'
                + values
                + ' <td class="text-center">'+ product.correctiveActions +'</td>'
                + '</tr>'
            );
            }
        }

        return "success";

    } catch (err) {
        return "loadReport() Error:" + err;
    }
}

function getTimeFromTimestamp(date) {
    try {
      var result = "";

      if (date.length > 0) {
        result = date.split(", ")[1];
      }

      return result;
    } catch(error) {
      return "";
    }
}

function getDateFromTimestamp(date) {
    try {
      var result = "";

      if (date.length > 0) {
        result = date.split(", ")[0];
      }

      return result;
    } catch(error) {
      return "";
    }
}

function decodeHtml(html) {
    var txt = document.createElement("textarea");
    txt.innerHTML = html;

    return txt.value;
}

function test() {
    alert("test");
}

function initHelpers() {
    Handlebars.registerHelper("isAchieved", function(value) {
        if (value === undefined) return;

        if (value === 'N') {
            return 'red_x.png';
        }

        return 'green_check.png';
    });

    Handlebars.registerHelper("renderTable", function(LTOObject) {
        console.log('LTO: ', LTOObject);
        $(function() {
            var table = '<table class="table table-striped table-bordered col-md-12 col-xs-12 tbl-LTO"><thead class="lto-thead"></thead><tbody class="lto-tbody"></tbody></table>';

            var tds = '';
            for (var i=0; i<LTOObject.dayparts.length; i++) {
                tds += ' <td>'+LTOObject.dayparts[i]+'</td>'
            }

            return new Handlebars.SafeString(table);
        });
    })

    Handlebars.registerHelper("getDescription", function(value, defaultValue) {
        console.log('getDescription: ', value, defaultValue)

        return (value === undefined) ? defaultValue : value
    })

    Handlebars.registerHelper("replaceNullValueWithCustomString", function(value, customString) {
        console.log('replaceNullValueWithCustomString: ', value, customString)

        return (value === undefined) ? customString : value
    })

    Handlebars.registerHelper("getTheDelTacoDescription", function (value) {
        console.info("GET THE DEL TACO DESCRIPTION: "+ value);
        
	    return (value == undefined) ? "Crunchy 4.25 oz." : value;
	});
    
    Handlebars.registerHelper("checkEnabledEquipmentTemps", function (value) {
        console.info("equipmentTempsCheckEnabled: "+ value);
        
	    if (value == undefined) 
            return "";
        else if (value == "False")
            return "none";
        else
            return "";
	});
    
    Handlebars.registerHelper("displayCorrectiveActions", function (ca1, ca2, ca3, ca4, ca5) {
        console.info("displayCorrectiveActions: ca1: "+ ca1 +" - ca2: "+ ca2 + " - ca3: "+ ca3 + " - ca4: "+ ca4 + " - ca5: "+ ca5);
        
        var correctiveActions = "";
        var arrCA = [];
        
        if (ca1 != undefined) {
            correctiveActions = correctiveActions + ca1;
        }
        
        if(ca2 != undefined) {
            if(correctiveActions != "") {
                // Check if existing
                if (correctiveActions.indexOf(ca2) == -1) 
                    correctiveActions = correctiveActions.concat(", "+ca2);   
            }
            else
                correctiveActions = correctiveActions + ca2;
        }
        
        if(ca3 != undefined) {
            if(correctiveActions != "") {
                if (correctiveActions.indexOf(ca3) == -1)   
                    correctiveActions = correctiveActions.concat(", "+ca3);
            } else
                correctiveActions = correctiveActions + ca3;
        } 
        
        if(ca4 != undefined) {
            // Check if existing
            if(correctiveActions != "") {
                if (correctiveActions.indexOf(ca4) == -1)   
                    correctiveActions = correctiveActions.concat(", "+ca4);
            } else
                correctiveActions = correctiveActions + ca4;
        }

        if(ca5 != undefined) {
            // Check if existing
            if(correctiveActions != "") {
                if (correctiveActions.indexOf(ca5) == -1)   
                    correctiveActions = correctiveActions.concat(", "+ca5);
            } else
                correctiveActions = correctiveActions + ca5;
        }
        
        console.info("CorrectiveActions: "+ JSON.stringify(correctiveActions));
        
        return correctiveActions;
	});
}


