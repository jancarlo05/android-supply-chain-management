(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['employeesSchedule'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "          <tr>\n            <td class=\"width-30\">\n              <div class=\"text-orange\">Employee Name</div>\n            </td>\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : {},(depth0 != null ? depth0.headerDays : depth0),{"name":"each","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "          </tr>\n";
},"2":function(container,depth0,helpers,partials,data) {
    var alias1=container.lambda, alias2=container.escapeExpression;

  return "                <td class=\"width-10\">\n                  <div class=\"text-orange\">"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "</div>\n                  <div class=\"text-color-555 padding-top-2\">"
    + alias2(alias1((depth0 != null ? depth0.date : depth0), depth0))
    + "</div>\n                </td>\n";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : {},(depth0 != null ? depth0.employees : depth0),{"name":"each","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "            <tr onClick=\"onClickEmployee('"
    + alias2(alias1((depth0 != null ? depth0.userId : depth0), depth0))
    + "')\">\n              <td valign=\"middle\" class=\"text-left\">"
    + alias2(alias1((depth0 != null ? depth0.name : depth0), depth0))
    + "</td>\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : {},(depth0 != null ? depth0.schedules : depth0),{"name":"each","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "            </tr>\n";
},"6":function(container,depth0,helpers,partials,data) {
    var alias1=container.lambda, alias2=container.escapeExpression;

  return "                <td>\n                  <div>"
    + alias2(alias1((depth0 != null ? depth0.timeIn : depth0), depth0))
    + "</div>\n                  <div>"
    + alias2(alias1((depth0 != null ? depth0.timeOut : depth0), depth0))
    + "</div>\n                </td>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : {};

  return "<div class=\"dv-page-container container-fluid col-md-12 col-xs-12 dv-first\">\n"
    + ((stack1 = container.invokePartial(partials.header,depth0,{"name":"header","hash":{"class":"dv-first"},"data":data,"indent":"  ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\n  <div class=\"col-md-12 col-xs-12 padding-bottom-10\">\n    <table class=\"tbl-emp-sched table tbl-salmon col-md-12 col-xs-12 table-responsive\">\n      <thead>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.headerDays : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "      </thead>\n      <tbody>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.employees : depth0),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "      </tbody>\n    </table>\n  </div>\n</div>";
},"usePartial":true,"useData":true});
})();