(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['header'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda;

  return "<div class=\"col-md-12 col-xs-12\">\n  <div class=\"col-md-12 col-xs-12 dv-header-container\">\n    <div class=\"col-md-2 col-xs-2\"></div>\n    <div class=\"col-md-8 col-xs-8\" style=\"text-align: center;\">\n      <div class=\"dv-title\">"
    + ((stack1 = alias1((depth0 != null ? depth0.title : depth0), depth0)) != null ? stack1 : "")
    + "</div>\n      <div class=\"dv-subtitle\">"
    + ((stack1 = alias1((depth0 != null ? depth0.subtitle : depth0), depth0)) != null ? stack1 : "")
    + "</div>\n    </div>\n    <div class=\"col-md-2 col-xs-2 dv-logo dv-logo-right\"></div>\n  </div>\n</div>";
},"useData":true});
})();