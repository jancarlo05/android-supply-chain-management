//$(document).ready(function() {
//  // loadReport({}, 3);
//});

function loadReport(params) {
    try {
        // Register Partials
        Handlebars.registerPartial('header', Handlebars.templates['header']);
        Handlebars.registerPartial('headerDetails', Handlebars.templates['headerDetails']);
        
        // Load template with data
        var templateToUse = (params.header === 'Details') ? 'detail' : 'rollup';
        var html = Handlebars.templates[templateToUse](params);
        
        $("body").append(html);

        return "success";
    } catch (err) {
        return '[RollUpReports] caught an error: ' + err;
    }
}

function sendOnClickAction(id, corpId, title) {
//    callbackAction(id, corpId, title);
    window.AndroidApp.callbackAction(id, corpId, title);
}

function sendOnClickActionForParent(isParent, corpId, title) {
    if(!isParent)
      return;
//    callbackAction('-1', corpId, title);
    window.AndroidApp.callbackAction('-1', corpId, title);
}

function toggleItem(elem, numOfChild, isParent) {

  if(!isParent)
    return;

  var parentNode = elem.parentNode;
  var child = parentNode.nextElementSibling;
  var cval = parentNode.getElementsByClassName('placehold')[0].innerHTML;
  parentNode.getElementsByClassName('placehold')[0].innerHTML = (cval == "-") ?  "+" : "-";
  
  for (var i=0; i < numOfChild; i++) {
    
    
    if(child.className == 'child') {
      child.className = '';
      child.classList.toggle('child-open');
    } else {
      child.className = '';
      child.classList.toggle('child');
    }

    child = child.nextElementSibling;
  }
}


Handlebars.registerHelper("ClassToUse", function(isParent) {
  return isParent ? "parent" : "child";
})

Handlebars.registerHelper("PlaceHoldToUseClass", function(isParent) {
  return isParent ? "placehold" : "";
})

Handlebars.registerHelper("PlaceHoldToUse", function(isParent) {
  return isParent ? "+" : "-";
})