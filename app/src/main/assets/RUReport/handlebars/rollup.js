(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['rollup'] = template({"1":function(container,depth0,helpers,partials,data) {
    var alias1=container.lambda, alias2=container.escapeExpression;

  return "            <td colspan="
    + alias2(alias1((depth0 != null ? depth0.cspan : depth0), depth0))
    + ">"
    + alias2(alias1((depth0 != null ? depth0.title : depth0), depth0))
    + "</td>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "             <td class=\"font10\">"
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "</td>\n";
},"5":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "          <tr onclick=\"sendOnClickAction('"
    + alias2(alias1((depths[1] != null ? depths[1].nextLvl : depths[1]), depth0))
    + "', '"
    + alias2(alias1((depth0 != null ? depth0.id : depth0), depth0))
    + "', '"
    + alias2(alias1((depth0 != null ? depth0.title : depth0), depth0))
    + "')\">\n            <td>"
    + alias2(alias1((depth0 != null ? depth0.title : depth0), depth0))
    + "</td>\n            <td>"
    + alias2(alias1((depth0 != null ? depth0.numOfStores : depth0), depth0))
    + "</td>\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.fslCompliant : depth0),{"name":"each","hash":{},"fn":container.program(6, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "          </tr> \n";
},"6":function(container,depth0,helpers,partials,data) {
    return "              <td>"
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "</td>\n";
},"8":function(container,depth0,helpers,partials,data) {
    return "            <td>"
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "</td>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "<div class=\"dv-page-container col-md-12 col-xs-12\">\n"
    + ((stack1 = container.invokePartial(partials.header,depth0,{"name":"header","hash":{"subtitle":(depth0 != null ? depth0.subtitle : depth0),"title":(depth0 != null ? depth0.title : depth0)},"data":data,"indent":"  ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "  \n  <!-- HEADER DETAILS -->\n"
    + ((stack1 = container.invokePartial(partials.headerDetails,depth0,{"name":"headerDetails","data":data,"indent":"  ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "")
    + "\n  <!-- TABLE CONTENT -->\n  <div class=\"dv-table-content col-md-12 col-xs-12 margin-top-10\">\n    <table class=\"table table-bordered tbl-report col-md-12 col-xs-12\">\n      <thead>\n        <tr>\n          <td rowspan=\"2\">"
    + container.escapeExpression(((helper = (helper = helpers.header || (depth0 != null ? depth0.header : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"header","hash":{},"data":data}) : helper)))
    + "</td>\n          <td rowspan=\"2\"># Stores</td>\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.headerTitles : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "        </tr>\n        <tr>\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.headers : depth0),{"name":"each","hash":{},"fn":container.program(3, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "        </tr>\n      </thead>\n      <tbody>\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.items : depth0),{"name":"each","hash":{},"fn":container.program(5, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n        <tr>\n          <td style=\"border:none;\" colspan=\"15\">&nbsp;</td>\n        </tr>  \n        <tr>\n          <td style=\"border:none;\" colspan=\"15\">&nbsp;</td>\n        </tr>\n      </tbody>\n      <tfoot>\n        <tr>\n          <td>Average</td>\n          <td>&nbsp;</td>\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.average : depth0),{"name":"each","hash":{},"fn":container.program(8, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "        </tr>\n      </tfoot>\n    </table>\n  </div>\n</div>";
},"usePartial":true,"useData":true,"useDepths":true});
})();