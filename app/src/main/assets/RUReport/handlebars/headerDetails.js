(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['headerDetails'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"col-md-12 col-xs-12\">\n  <div class=\"dv-header-details col-md-12 col-xs-12\" style=\"padding: 5px 0px;\">\n    <div class=\"col-md-6 col-xs-6 border-right\">\n      <div class=\"col-md-4 col-xs-4 dv-labels text-left  bordered-bottom\">Company:</div>  \n      <div class=\"col-md-8 col-xs-8 dv-value text-right  bordered-bottom\">"
    + alias4(((helper = (helper = helpers.spid || (depth0 != null ? depth0.spid : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"spid","hash":{},"data":data}) : helper)))
    + "</div>  \n\n      <div class=\"col-md-6 col-xs-4 dv-labels text-left\">Total Number of Stores:</div>  \n      <div class=\"col-md-6 col-xs-8 dv-value text-right\">"
    + alias4(((helper = (helper = helpers.totalStores || (depth0 != null ? depth0.totalStores : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"totalStores","hash":{},"data":data}) : helper)))
    + "</div>  \n    </div>\n    <div class=\"col-md-6 col-xs-6\">\n      <div class=\"col-md-4 col-xs-4 dv-labels text-left\">Corporate Address:</div>  \n      <div class=\"col-md-8 col-xs-8 dv-value text-right\">"
    + alias4(((helper = (helper = helpers.address || (depth0 != null ? depth0.address : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"address","hash":{},"data":data}) : helper)))
    + "</div>  \n    </div>\n  </div>\n</div>";
},"useData":true});
})();